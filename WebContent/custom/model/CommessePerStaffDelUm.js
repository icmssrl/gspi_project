jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.CommessePerStaffDelUm = {
    
    // funzione chiamata da UmScheduling
    read: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            var arrayProjects = model.persistence.Serializer.commessePerStaffDelUm.fromSAPItems(result);
            defer.resolve(arrayProjects);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getCommessePerStaffDelUm(req, fSuccess, fError);
        return defer.promise;
    },
    
    
};