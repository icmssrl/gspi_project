jQuery.sap.declare("model.tabellaSchedulingMaria");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.tabellaSchedulingMaria = {
    readTable: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            var requests = [];
            if (result.results) {
                result.results = model.persistence.Serializer.requestsFinal.fromSAPItems(result).items;
            };
            var table = [];
            if (result && result.results) {
                var res3 = result.results;
                for (var i = 0; i < res3.length; i++) {
                    var obj = {};
                    obj.nomeConsulente = res3[i].nomeConsulente;
                    obj.codiceConsulente = res3[i].cod_consulente;
                    var propNote = "nota" + res3[i].gg;
                    obj[propNote] = res3[i].note;
                    if (res3[i].gg) {
                        var prop = "gg" + res3[i].gg;
                        var propIcon = "ggIcon" + res3[i].gg;
                        var propConflict = "ggConflict" + res3[i].gg;
                    };
                    if (res3[i].nomeCommessa) {
                        obj[prop] = res3[i].nomeCommessa;
                    };
                    /* verifico se ci sono delle giornate con conflitti e in caso metto un'icona di avvertimento */
                    var richiesteContemporanee = _.where(res3, {
                        'gg': res3[i].gg,
                        'cod_consulente': res3[i].cod_consulente
                    });
                    var mezzeRichiesteContemporanee = _.where(richiesteContemporanee, {
                        'ggIcon': "halfDay"
                    });
                    var mezzeRichiesteFerieContemporanee = _.where(richiesteContemporanee, {
                        'ggIcon': "bed"
                    });
                    var nRichiesteContemporanee = richiesteContemporanee.length;
                    var nMezzeRichiesteContemporanee = mezzeRichiesteContemporanee.length + mezzeRichiesteFerieContemporanee.length;

                    if (nMezzeRichiesteContemporanee > 2 || (nRichiesteContemporanee > 1 && nRichiesteContemporanee !== nMezzeRichiesteContemporanee)) {
                        obj[propConflict] = true;
                    } else {
                        obj[propConflict] = false;
                    };

                    if (res3[i].ggIcon) {
                        if (res3[i].ggIcon === "halfDay") {
                            obj[propIcon] = "./custom/img/half3.png";
                        } else if (res3[i].ggIcon === "abroad") {
                            obj[propIcon] = "sap-icon://globe";
                        } else if (res3[i].ggIcon === "not-collaborate") {
                            obj[propIcon] = "sap-icon://offsite-work";
                        }else if (res3[i].ggIcon === "complete-holidays") {
                            obj[propIcon] = "sap-icon://travel-itinerary";
                        } else if (res3[i].ggIcon === "not-confirmed") {
                            obj[propIcon] = "sap-icon://role";
                        } else if (res3[i].ggIcon === "not-customer-and-contacts") {
                            obj[propIcon] = "sap-icon://employee-rejections";
                        } else {
                            obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                        };
                    };
                    if (res3[i].codicePJM) {
                        obj.codicePJM = res3[i].codicePJM;
                    };
                    if (res3[i].toCheck) {
                        var prop = "toCheck" + res3[i].gg;
                        obj[prop] = res3[i].toCheck;
                    };
                    if (res3[i].id_req) {
                        var prop = "req" + res3[i].gg;
                        obj[prop] = res3[i].id_req;
                    };
                    //**
                    table.push(obj);
                };
                var res = [];
                res.push(table);
                defer.resolve(res);
            };
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getTableSchedulingPeriodsMaria(req, fSuccess, fError);
        return defer.promise;
    },

    deleteRequestMaria: function (idReq, state, numero) {
        if (numero) {
            this.numeroRichiesteDaEliminare = numero;
        };
        this.numeroRichiesteDaEliminare--;

        var deferDeleteRequest = Q.defer();

        var fSuccess = function (ris) {
            // faccio ritornare alla funzione di successo solo se tutte le richieste di cambio stato sono andate a buon fine
            if (this.numeroRichiesteDaEliminare === 0) {
                deferDeleteRequest.resolve(ris);
            };
        };
        var fError = function (err) {
            deferDeleteRequest.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteRequestMaria(idReq, state, fSuccess, fError);
        return deferDeleteRequest.promise;
    },

    deleteRequestsFromFinal: function (periodo) {
        var deferDelete = Q.defer();

        var fSuccess = function (ris) {
            deferDelete.resolve(ris);
        };
        var fError = function (err) {
            deferDelete.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteSchedulingPeriodsMaria(periodo, fSuccess, fError);
        return deferDelete.promise;
    },

    readRequests: function (periodo) {
        var deferGet = Q.defer();

        var fSuccess = function (ris) {
            deferGet.resolve(ris);
        };
        var fError = function (err) {
            deferGet.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readSchedulingPeriodsMaria(periodo, fSuccess, fError);
        return deferGet.promise;
    },

    writeRequests: function (requests) {
        /* chiamo in maniera ciclica il servizio per l'inserimento delle richieste e se almeno una non va a buon fine faccio tornare un errore */
        var response = [];
        var deferGet = Q.defer();

        var fSuccess = function (ris) {
            /* ogni volta che un inserimento va a buon fine pusho la risposta dentro l'array. quindi torno al controller solo quando tutte le risposte
            sono arrivate in maniera corretta */
            if (ris.status.indexOf("OK") >= 0) {
                deferGet.resolve(ris);
            };
        };
        var fError = function (err) {
            deferGet.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var request = {};
        request.results = requests;

        model.odata.chiamateOdata.writeSchedulingPeriodsMaria(request, fSuccess, fError);
        return deferGet.promise;
    },

    sendRequest: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.saveEntityToFinal(req, fSuccess, fError);
        return defer.promise;
    },
    
    sendBatchRequest: function (batchReq) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.saveBatchEntityToFinal(batchReq, fSuccess, fError);
        return defer.promise;
    },

    updateNote: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateNoteMaria(req, fSuccess, fError);
        return defer.promise;
    },
    
    readSingoloConsulente: function (req) {
        var defer = Q.defer();
        this.richiesta = req;
//        this.staffCompleteList = staffCompleteList.items;

        var fSuccess = function (result) {
            var requests = [];
            if (result.results) {
                result.results = model.persistence.Serializer.requestsFinal.fromSAPItems(result).items;
            }
            var table = [];
            var arrConsulente = [];
            if (result && result.results) {
                var res3 = _.uniq(result.results, function (item) {
                    return item.id_req;
                });
                res3 = res3.sort(this.confronta.bind(this));
                var resConfirmed = _.where(res3, {
                    stato: 'CONFIRMED',
                    idPeriodoScheduling: this.richiesta.idPeriodo
                });
                var resStandBy = _.where(res3, {
                    stato: 'STANDBY',
                    idPeriodoScheduling: this.richiesta.idPeriodo
                });
                var resSchedulazioni = resConfirmed.concat(resStandBy);
                for (var i = 0; i < res3.length; i++) {
//                    if (res3[i].stato === "CONFIRMED" || res3[i].stato === "STANDBY") {
                        var obj = {};
                        obj.nomeConsulente = res3[i].nomeConsulente;
                        obj.codiceConsulente = res3[i].cod_consulente;
                        obj.nomeSkill = res3[i].nomeSkill;
                        var propNote = "nota" + res3[i].gg;
                        obj[propNote] = res3[i].note;
                        if (res3[i].gg) {
                            var prop = "gg" + res3[i].gg;
                            var propIcon = "ggIcon" + res3[i].gg;
                            var propConflict = "ggConflict" + res3[i].gg;
                            var propReq = "ggReq" + res3[i].gg;
                            var propStato = "ggStato" + res3[i].gg;
                        }
                        if (res3[i].dataReqPeriodo) {
                            obj[propReq] = res3[i].dataReqPeriodo;
                        }
                        if (res3[i].nomeCommessa) {
                            obj[prop] = res3[i].nomeCommessa;
                        }
                        if (res3[i].stato) {
                            obj[propStato] = res3[i].stato;
                        }
                        /* verifico se ci sono delle giornate con conflitti e in caso metto un'icona di avvertimento */
                        var richiesteContemporanee = _.where(resSchedulazioni, {
                            'gg': res3[i].gg,
                            'cod_consulente': res3[i].cod_consulente
                        });
                        var mezzeRichiesteContemporanee = _.where(richiesteContemporanee, {
                            'ggIcon': "halfDay"
                        });
                        var mezzeRichiesteFerieContemporanee = _.where(richiesteContemporanee, {
                            'ggIcon': "bed"
                        });
                        var nRichiesteContemporanee = richiesteContemporanee.length;
                        var nMezzeRichiesteContemporanee = mezzeRichiesteContemporanee.length + mezzeRichiesteFerieContemporanee.length;

                        if (nMezzeRichiesteContemporanee > 2 || (nRichiesteContemporanee > 1 && nRichiesteContemporanee !== nMezzeRichiesteContemporanee)) {
                            obj[propConflict] = true;
                        } else {
                            obj[propConflict] = false;
                        }

                        if (res3[i].ggIcon) {
                            obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                        }
                        if (res3[i].codicePJM) {
                            obj.codicePJM = res3[i].codicePJM;
                        }
                        if (res3[i].cod_consulenteBu) {
                            obj.cod_consulenteBu = res3[i].cod_consulenteBu;
                        }
                        if (res3[i].toCheck) {
                            var prop = "toCheck" + res3[i].gg;
                            obj[prop] = res3[i].toCheck;
                        }
                        if (res3[i].id_req) {
                            var prop = "req" + res3[i].gg;
                            obj[prop] = res3[i].id_req;
                            var propOld = "idReqOld" + res3[i].gg;
                            obj[propOld] = (res3[i].idReqOld !== "") ? parseInt(res3[i].idReqOld) : 0;
                        }
                        //**
                        table.push(obj);

//                    }
                }
                // aggiungo all'array finale tutti i consulenti non schedulati
//                for (var t = 0; t < this.staffCompleteList.length; t++) {
//                    if (!_.find(table, {
//                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff
//                        })) {
//                        table.push({
//                            nomeConsulente: this.staffCompleteList[t].codConsulenteStaff.indexOf("CCG") >= 0 ? this.staffCompleteList[t].descSkill : this.staffCompleteList[t].cognomeConsulente + " " + this.staffCompleteList[t].nomeConsulente,
//                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff,
//                            richiesteCancellate: 0
//                        });
//                    }
//                }
                var res = [];
                res.push(table);
                defer.resolve(res);
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getFinalSingle(req, fSuccess, fError);
        return defer.promise;
    },
    
     updateHolidaySuFinal: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateHolidaySuFinal(req, fSuccess, fError);
        return defer.promise;
    },
};
