jQuery.sap.declare("model.Periods");
jQuery.sap.require("model.Period");
//jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.persistence.Serializer");


model.Periods = (function () {

    Periods = function (data) {
        this.arrayPeriods = [];

        this.getModel = function () {
            var model = new sap.ui.model.json.JSONModel();
            model.setData(this.arrayPeriods);
            return model;
        };

        this.getPeriods = function () {
            var defer = Q.defer();

            var fSuccess = function (result) {
                this.arrayPeriods = model.persistence.Serializer.periods.fromSAPItems(result);
                defer.resolve(this.arrayPeriods);
            };
            var fError = function (err) {
                defer.reject(err);
            };
            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            model.odata.chiamateOdata.getPeriods(fSuccess, fError);
            return defer.promise;
        };

        this.deletePeriod = function (periodCode) {
            var defer = Q.defer();

            var fSuccess = function (result) {
                defer.resolve(result);
            };

            var fError = function (err) {
                defer.reject(err);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            model.odata.chiamateOdata.deletePeriod(periodCode, fSuccess, fError);

            return defer.promise;
        };

        this.addPeriod = function (periodObj) {
            var defer = Q.defer();

            var fSuccess = function (result) {
                defer.resolve(result);
            };

            var fError = function (err) {
                defer.reject(err);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            model.odata.chiamateOdata.addPeriod(periodObj, fSuccess, fError);

            return defer.promise;
        };

        this.updatePeriod = function (periodObj) {
            var defer = Q.defer();

            var fSuccess = function (result) {
                defer.resolve(result);
            };

            var fError = function (err) {
                defer.reject(err);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            model.odata.chiamateOdata.updatePeriod(periodObj, fSuccess, fError);

            return defer.promise;
        };

        this.readPeriod = function (id, year) {
            var defer = Q.defer();

            var path = "(ID_PERIODO=" + id + ",ANNO_PERIODO_SCHEDULAZIONE=" + year + ")";
            var fSuccess = function (result) {
                var period = model.persistence.Serializer.periods.fromOData(result);
                defer.resolve(period);
            };

            var fError = function (err) {
                defer.reject(err);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            model.odata.chiamateOdata.readODataPeriod(path, fSuccess, fError);

            return defer.promise;
        };

        this.getNewInstance = function () {
            var lastPeriod = _.max(this.arrayPeriods.items, function (item) {
                return item.endDate;
            });
            var firstDate = this._getNextAvailableDate(lastPeriod.endDate);
            var lastDate = this._getMonthLastDate(firstDate);
            var reqDate = new Date(lastDate);
            reqDate.setDate(firstDate.getDate() - 7);
            var period = new model.Period();
            period.createInstance(firstDate, lastDate, reqDate);
            return period;

        };
        
        this._getNextAvailableDate = function (lastDate) {
            if (!lastDate) {
                var dataRif = new Date();
                lastDate = new Date(dataRif.getFullYear(), 0, 1);
            }
            var result = new Date(lastDate.getFullYear(), lastDate.getMonth(), lastDate.getDate() + 1);
            while (result.getDay() == 6 || result.getDay() == 0) {
                result.setDate(result.getDate() + 1);
            }
            return result;
        };
        
        this._getMonthLastDate = function (firstDate) {
            var date = new Date(firstDate.getFullYear(), firstDate.getMonth() + 1, 0);
            while (date.getDay() == 6 || date.getDay() == 0) {
                date.setDate(date.getDate() - 1);
            }
            if (date <= firstDate) {
                date = new Date(firstDate.getFullYear(), firstDate.getMonth() + 2, 0);
                while (date.getDay() == 6 || date.getDay() == 0) {
                    date.setDate(date.getDate() - 1);
                }
            }
            while (((date - firstDate) / (1000 * 60 * 60 * 24)) > 35) {
                date.setDate(date.getDate() - 1);
                while (date.getDay() == 6 || date.getDay() == 0) {
                    date.setDate(date.getDate() - 1);
                }
            }
            return date;
        };
        return this;
    };

    return Periods;

})();