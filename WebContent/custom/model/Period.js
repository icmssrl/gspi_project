jQuery.sap.declare("model.Period");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.persistence.Serializer");

model.Period = (function () {
    Period = function (data) {
        this.annoPeriodo;
        this.mesePeriodo;
        this.stato;
        this.idPeriod;
        this.note;

        this.dataInizio;
        this.startDate; //javascript date format

        this.dataFine;
        this.endDate; //javascript date format

        this.dataFineRichieste;
        this.reqEndDate; //javascript date format


        this.getModel = function () {
            var model = new sap.ui.model.json.JSONModel();
            model.setData(this);
            return model;
        };

        this.initialize = function () {
            var date = new Date();

            this.annoPeriodo = date.getFullYear();
            this.mesePeriodo = null;
            this.stato = "APERTO";
            this.idPeriod;
            this.note;
//            this.rilascioPjm;
//            this.rilascioAll;

            this.startDate = null; //javascript date format
            this.dataInizio = null;

            this.endDate = null; //javascript date format
            this.dataFine = null;

            this.reqEndDate = null;
            this.dataFineRichieste = null;
        };

        this.update = function (obj) {
            for (var prop in obj) {
                this[prop] = obj[prop];
            }
            this.endDate = this.endDate ? new Date(this.endDate) : null;
            this.reqEndDate = this.reqEndDate ? new Date(this.reqEndDate) : null;
            this.startDate = this.startDate ? new Date(this.startDate) : null;
            this.checkDate();

            this.path = "(ID_PERIODO=" + this.idPeriod + ",ANNO_PERIODO_SCHEDULAZIONE=" + this.annoPeriodo + ")";
        };

        this.checkDate = function () {
            //check if localeDateString is equals, else reassign it
            if (this.endDate && this.endDate.toLocaleDateString() !== this.dataFine) {
                this.dataFine = this.endDate.toLocaleDateString();
            }

            if (this.startDate && this.startDate.toLocaleDateString() !== this.dataInizio) {
                this.dataInizio = this.startDate.toLocaleDateString();
                this.mesePeriodo = this.startDate.getMonth() + 1;
                this.annoPeriodo = this.startDate.getFullYear();
            }

            if (this.reqEndDate && this.reqEndDate.toLocaleDateString() !== this.dataFineRichieste)
                this.dataFineRichieste = this.reqEndDate.toLocaleDateString();
        };

        this.checkPeriodDate = function (type) //req to xsjs to compare periods
            {
                var defer = Q.defer();
                var req = {};
                req.annoPeriodoSchedulazione = this.annoPeriodo;
                req.date = (type === "START") ? this.startDate : this.endDate;
                req.date = this.dateConverter(req.date);
                if (this.idPeriod)
                    req.idPeriodo = this.idPeriod;

                var fSuccess = function (result) {
                    defer.resolve(result);
                };
            
                fSuccess = _.bind(fSuccess, this);

                var fError = function (err) {
                    defer.reject(err);
                };
            
                fError = _.bind(fError, this);

                model.odata.chiamateOdata.checkDate(req, fSuccess, fError);

                return defer.promise;
            };
        
        this.checkDateConstraints = function () //contraints about period
            {
                var res = {
                    "res": true,
                    "error": []
                };
                if ((this.reqEndDate > this.endDate || this.reqEndDate > this.startDate) && this.reqEndDate && this.endDate && this.startDate) {
                    res.res = false;
                    res.error.push({
                        "errorCode": 1,
                        "errorText": "REQDATE_ERROR"
                    });
                }
                if (this.endDate && this.startDate) {
                    var dateDiffMs = this.endDate - this.startDate;
                    if (dateDiffMs <= 0) {
                        res.res = false;
                        res.error.push({
                            "errorCode": 2,
                            "errorText": "ENDDATE_BEFORE_START"
                        });
                        return res;
                    }

                    var dayDiff = dateDiffMs / (1000 * 60 * 60 * 24);
                    if (dayDiff > 35) {
                        res.res = false;
                        res.error.push({
                            "errorCode": 3,
                            "errorText": "INTERVAL_TOO_BIG"
                        });
                    }
                }
                return res;
            };
        
        this.deleteODataPeriod = function () {
            var defer = Q.defer();

            var fSuccess = function (result) {
                defer.resolve(result);
            };

            var fError = function (err) {
                defer.reject(err);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            model.odata.chiamateOdata.deleteODataPeriod(this.path, fSuccess, fError);

            return defer.promise;
        };

        this.addODataPeriod = function () {
            var defer = Q.defer();

            var fSuccess = function (result) {
                defer.resolve(result);
            };

            var fError = function (err) {
                defer.reject(err);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            var entity = model.persistence.Serializer.periods.toOData(this);
            model.odata.chiamateOdata.createODataPeriod(entity, fSuccess, fError);

            return defer.promise;
        };

        this.createPeriod = function () {
            var defer = Q.defer();

            var fSuccess = function (result) {
                defer.resolve(result);
            };

            var fError = function (err) {
                defer.reject(err);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            
            var numSettimanaStart = this.getNumWeek(this.startDate);
            var numSettimanaEnd = this.getNumWeek(this.endDate);
            var nSettimaneDiff = numSettimanaEnd-numSettimanaStart+1;
            if (nSettimaneDiff < 0) {
                nSettimaneDiff = 52 + nSettimaneDiff;
            }
            
//            var descrizioneMesi = this.getMesi(this.startDate, this.endDate);
            var descrizioneMesi = this.getMesi(this.mesePeriodo);

            var entity = {
                "annoPeriodo": this.annoPeriodo,
                "mesePeriodo": this.mesePeriodo,
                "stato": this.stato,
                "dataInizio": this.dateConverter(this.startDate),
                "dataFine": this.dateConverter(this.endDate),
                "dataFineRichieste": this.dateConverter(this.reqEndDate),
                "note": this.note,
                "rilascioPjm": '',
                "rilascioAll": '',
                "numSettimane": nSettimaneDiff,
                "descrizioneMesi": descrizioneMesi
            };

            model.odata.chiamateOdata.addPeriod(entity, fSuccess, fError);

            return defer.promise;
        };

        this.updatePeriod = function () {
            var defer = Q.defer();

            var fSuccess = function (result) {
                defer.resolve(result);
            };

            var fError = function (err) {
                defer.reject(err);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            
            var numSettimanaStart = this.getNumWeek(this.startDate);
            var numSettimanaEnd = this.getNumWeek(this.endDate);
            var nSettimaneDiff = numSettimanaEnd-numSettimanaStart+1;
            if (nSettimaneDiff < 0) {
                nSettimaneDiff = 52 + nSettimaneDiff;
            }
            
//            var descrizioneMesi = this.getMesi(this.startDate, this.endDate);
            var descrizioneMesi = this.getMesi(this.mesePeriodo);
            
            var entity = {
                "idPeriod": this.idPeriod,
                "annoPeriodo": this.annoPeriodo,
                "mesePeriodo": this.mesePeriodo,
                "stato": this.stato,
                "dataInizio": this.dateConverter(this.startDate),
                "dataFine": this.dateConverter(this.endDate),
                "dataFineRichieste": this.dateConverter(this.reqEndDate),
                "note": this.note,
                "numSettimane": nSettimaneDiff,
                "descrizioneMesi": descrizioneMesi
            };

            model.odata.chiamateOdata.updatePeriod(entity, fSuccess, fError);

            return defer.promise;
        };
        
        this.getNumWeek = function (data) {
            var target = new Date(data.valueOf());
            var dayNr = (data.getDay() + 6) % 7;
            target.setDate(target.getDate() - dayNr + 3);
            var jan4 = new Date(target.getFullYear(), 0, 4);
            var dayDiff = (target - jan4) / 86400000;
            var weekNr = 1 + Math.ceil(dayDiff / 7);
            return weekNr;
        };
        
//        this.getMesi = function (startDate, endDate) {
        this.getMesi = function (mese) {
            var mesi = {0: "Gennaio", 1: "Febbraio", 2: "Marzo", 3: "Aprile", 4: "Maggio", 5: "Giugno", 6: "Luglio", 7: "Agosto", 8: "Settembre", 9: "Ottobre", 10: "Novembre", 11: "Dicembre"};
//            var descrizione = "";
            var descrizione = mesi[mese-1];
//            if (startDate.getMonth() === endDate.getMonth()) {
//                descrizione = mesi[startDate.getMonth()];
//            } else {
//                descrizione = (mesi[startDate.getMonth()] + " - " + mesi[endDate.getMonth()]);
//            }
            return descrizione;
        };

        this.dateConverter = function (d) {
            if (!_.isDate(d))
                return;

            var dateConverted = d.getFullYear() + '-' +
                ('0' + (d.getMonth() + 1)).slice(-2) + '-' +
                ('0' + d.getDate()).slice(-2);
            
            return dateConverted;
        };

        this.createInstance = function (startDate, endDate, reqDate) {
            this.startDate = startDate;
            this.endDate = endDate;
            this.reqEndDate = reqDate;

            this.checkDate();
        };

        this.initialize();

        if (data) {
            this.update(data);
        }

        return this;

    };

    return Period;
})();