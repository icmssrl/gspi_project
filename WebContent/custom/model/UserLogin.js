jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.UserLogin = {
    read: function (alias) {
        var defer = Q.defer();
        var that = this;
        var fSuccess = function (result) {
            this.arrayUsersLogin = model.persistence.Serializer.userLogin.fromSAPItems(result);
            defer.resolve(this.arrayUsersLogin);
        };
        
        var fError = function (err) {
            if (!this.oneTime) {
                this.oneTime = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(that.readConsultants(), that));
            }
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getUserLogin(alias, fSuccess, fError);
        return defer.promise;
    },

    deleteAlias: function (alias) {
        this._deferDeleteUserLogin = Q.defer();

        var fSuccess = function (result) {
            this._deferDeleteUserLogin.resolve(result);
        };

        var fError = function (err) {
            this._deferDeleteUserLogin.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteUserLogin(alias, fSuccess, fError);

        return this._deferDeleteUserLogin.promise;
    },

    add: function (alias, cognomeConsulente, nomeConsulente, password, stato, codConsulente) {
        this._deferAddUserLogin = Q.defer();

        var fSuccess = function (result) {
            this._deferAddUserLogin.resolve(result);
        };

        var fError = function (err) {
            this._deferAddUserLogin.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.addUserLogin(alias, cognomeConsulente, nomeConsulente, password, stato, codConsulente, fSuccess, fError);

        return this._deferAddUserLogin.promise;
    },

    update: function (alias, cognomeConsulente, nomeConsulente, password, stato, alias_OR) {
        this._deferUpdateUserLogin = Q.defer();

        var fSuccess = function (result) {
            this._deferUpdateUserLogin.resolve(result);
        };

        var fError = function (err) {
            this._deferUpdateUserLogin.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateUserLogin(alias, cognomeConsulente, nomeConsulente, password, stato, alias_OR, fSuccess, fError);

        return this._deferUpdateUserLogin.promise;
    },
    
    updateByCodConsulente: function (alias, cognomeConsulente, nomeConsulente, password, stato, codConsulente) {
        this._deferUpdateUserLogin = Q.defer();

        var fSuccess = function (result) {
            this._deferUpdateUserLogin.resolve(result);
        };

        var fError = function (err) {
            this._deferUpdateUserLogin.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateUserLoginByCodConsulente(alias, cognomeConsulente, nomeConsulente, password, stato, codConsulente, fSuccess, fError);

        return this._deferUpdateUserLogin.promise;
    },
    
    deleteByCodConsulente: function (codConsulente, fSuccess, fError) {
        model.odata.chiamateOdata.deleteUserLoginByCodConsulente(codConsulente, fSuccess, fError);
    },
  
};
