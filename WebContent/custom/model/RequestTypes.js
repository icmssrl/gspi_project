jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.RequestTypes = {
    
    readRequestTypes: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (!result && !this.oneTime) {
                this.oneTime = true;
                model.odata.chiamateOdata.chiamataForLoginToHanaSimple()
                    .then(_.bind(this.readRequestType(), this));
            };
            this.arrayRequestType = model.persistence.Serializer.requestType.fromSAPItems(result);
            defer.resolve(this.arrayRequestType);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getRequestTypes(req, fSuccess, fError);
        return defer.promise;
    },
    
    deleteRequestType: function (codicePrioritaCommessa) {
        this._deferDeleteRequestType = Q.defer();

        var fSuccess = function (result) {
            this._deferDeleteRequestType.resolve(result);
        };

        var fError = function (err) {
            this._deferDeleteRequestType.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteRequestType(codicePrioritaCommessa, fSuccess, fError);

        return this._deferDeleteRequestType.promise;
    },

    addRequestType: function (descrizionePrioritaCommessa) {
        this._deferAddRequestType = Q.defer();

        var fSuccess = function (result) {
            this._deferAddRequestType.resolve(result);
        };

        var fError = function (err) {
            this._deferAddRequestType.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.addRequestType(descrizionePrioritaCommessa, fSuccess, fError);

        return this._deferAddRequestType.promise;
    },

    updateRequestType: function (codicePrioritaCommessa, descrizionePrioritaCommessa) {
        this._deferUpdateRequestType = Q.defer();

        var fSuccess = function (result) {
            this._deferUpdateRequestType.resolve(result);
        };

        var fError = function (err) {
            this._deferUpdateRequestType.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateRequestType(codicePrioritaCommessa, descrizionePrioritaCommessa, fSuccess, fError);

        return this._deferUpdateRequestType.promise;
    }
};
