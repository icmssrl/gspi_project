jQuery.sap.declare("model._ConsultantsStatus");

model._ConsultantsStatus = {

    getStauts: function () {
        this._deferConsultantsStatus = Q.defer();

        var fSuccess = function (result) {
            this._consultantsStatus = result;
            this._deferConsultantsStatus.resolve(this._consultantsStatus);
        };

        var fError = function (err) {
            this._consultantsStatus = [];
            this._deferConsultantsStatus.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        $.getJSON("custom/model/statoConsulente.json")
            .success(fSuccess)
            .fail(fError);

        return this._deferConsultantsStatus.promise;
    },
};
