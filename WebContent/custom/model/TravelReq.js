jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.persistence.Serializer");

model.TravelReq = {
    getTravelReq: function () {
        this._deferTravelReq = Q.defer();

        var fSuccess = function (result) {
            this.arrayTravelReq = model.persistence.Serializer.travelReports.fromSAPItems(result);
            this._deferTravelReq.resolve(this.arrayTravelReq);
        };

        var fError = function (err) {
            this._deferTravelReq.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getTravelRequests(fSuccess, fError);

        return this._deferTravelReq.promise;
    },
   confirmTravelReq: function(nomeCognome, cid, societa, cliente, commessa, attivita, elementoWbs, rifatturabile, tipoSpesaCod, tipoSpesaDesc, luogo, dataCheckIn, dataCheckOut, noNotti, dettagliTrasporto, richiesteSpeciali, confermato, noteConferma, dataInserimento, costo, attivitaCod, idRichiesta){
       
       this._deferConfirmReq = Q.defer();
        
        var fSuccess = function(result){
            this._deferConfirmReq.resolve(result);
        };
        
        var fError = function(err){
            this._deferConfirmReq.reject(err);
        }
        
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.confirmTravelRequest(nomeCognome, cid, societa, cliente, commessa, attivita, elementoWbs, rifatturabile, tipoSpesaCod, tipoSpesaDesc, luogo, dataCheckIn, dataCheckOut, noNotti, dettagliTrasporto, richiesteSpeciali, confermato, noteConferma, dataInserimento, costo, attivitaCod, idRichiesta, fSuccess, fError);
        
        return this._deferConfirmReq.promise;
   }
    
};
