jQuery.sap.declare("model.consultantListReq");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("model.persistence.Serializer");

model.consultantListReq = {
    getConsultants: function () {
        var defer = Q.defer();
        var fSuccess = function (result) {
            utils.Busy.hide();
            _.remove(result.d.results, {COD_PROFILO_CONSULENTE: 'CP10000001'});
            var arrayConsultantList = model.persistence.Serializer.consultants.fromSAPItems(result);
            defer.resolve(arrayConsultantList);
        };
        var fError = function (err) {
            utils.Busy.hide();
            defer.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.odata.chiamateOdata.getConsultants(fSuccess, fError);

        return defer.promise;
    }

};
