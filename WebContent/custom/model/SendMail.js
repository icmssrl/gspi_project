model.SendMail = {
    richiestaFerie: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            var res = JSON.parse(result);
            defer.resolve(res);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.sendHolidayMail(req, fSuccess, fError);
        return defer.promise;
    },

};