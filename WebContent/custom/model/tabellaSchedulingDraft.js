model.tabellaSchedulingDraft = {
    // funzione che mi restituisce un JSON formattato per la tabella
    readTableDraft: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            var r = "";
            if(result)
            {
                r = JSON.parse(result)
            }
            defer.resolve(r);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readPjmDraft(req, fSuccess, fError);
        return defer.promise;
    },
    
};