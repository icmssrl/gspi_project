jQuery.sap.declare("model.SingleSchedModRequest");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.SingleSchedModRequest = (function () {

	SingleSchedModRequest = function (serializedData) {
		
		this.idReqModifica = undefined;
        this.idScambio = undefined;
        this.giornoPeriodoModifica = undefined;
        this.noteModifica = undefined;
        this.statoReqModifica = undefined;
        this.dataInserimentoModifica = undefined;
        this.codPjmModifica = undefined;
        this.idPeriodoSchedModifica = undefined;
        this.codAttivitaRichiestaModifica = undefined;
        this.idCommessaModifica = undefined;
        this.dataRichiestaModifica = undefined;
        this.codiceConsulenteModifica = undefined;
        this.nomePjmModifica = undefined;
        this.cognomePjmModifica = undefined;
        this.nomeConsulenteModifica = undefined;
        this.cognomeConsulenteModifica = undefined;
        this.codProfiloConsulenteModifica = undefined;
        this.noteConsulenteModifica = undefined;
        this.statoConsulenteModifica = undefined;
        this.skillConsulenteModifica = undefined;
        this.codBuConsulenteModifica = undefined;
        this.tipoAziendaConsulenteModifica = undefined;
        this.intercompanyConsulenteModifica = undefined;
        this.partTimeConsulenteModifica = undefined;
        this.giornateIntercompanyConsulenteModifica = undefined;
        this.tariffaIntercompanyConsulenteModifica = undefined;
        this.orePartTimeConsulenteModifica = undefined;
        this.nomeCommessaModifica = undefined;
        this.descrizioneCommessaModifica = undefined;
        this.statoCommessaModifica = undefined;
        this.codPJMCommessaModifica = undefined;
        this.codStaffCommessaModifica = undefined;
        this.codBuCommessaModifica = undefined;

        this.getModel = function () {
            var model = new sap.ui.model.json.JSONModel(this);
            return model; 
        };
        
        this.setNewConsultant = function(data){
            this.codiceConsulenteModifica = data.codConsulenteModifica;
            this.nomeConsulenteModifica = data.nomeConsulenteModifica;
            this.cognomeConsulenteModifica = data.cognomeConsulenteModifica;
        };
        
        this.update = function (data) {
            for (var prop in data) {
              this[prop] = data[prop];
            }
        };
          
        if (serializedData) {
            this.update(serializedData);
        }
        
        return this;
    };

    return SingleSchedModRequest;


})();
