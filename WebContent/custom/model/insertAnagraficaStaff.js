jQuery.sap.declare("model.insertAnagraficaStaff");
jQuery.sap.require("model.persistence.Serializer");

model.insertAnagraficaStaff = {
    insert: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if (!_.isArray(req)) {
            model.odata.chiamateOdata.insert(req, fSuccess, fError);
        } else {
            for (var i = 0; i < req.length; i++) {
                model.odata.chiamateOdata.insertAnagraficaStaff(req[i], fSuccess, fError);
            }
        }
        return defer.promise;
    }
};
