jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.TabellaConsulentePeriodoFinal = {
    check: function (idPeriodo) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            var risultato = "";
            if (result.results.length !== 0) {
                risultato = "OK";
            } else {
                risultato = "KO";
            }
            defer.resolve(risultato);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.checkTabellaConsPerFinal(idPeriodo, fSuccess, fError);
        return defer.promise;
    },
    
    write: function (idPeriodo) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.writeTabellaConsPerFinal(idPeriodo, fSuccess, fError);
        return defer.promise;
    },
    
    read: function (idPeriodo) {
        var defer = Q.defer();
            
        var fSuccess = function (result) {
            var res = model.persistence.Serializer.listaConsulentiPeriodo.fromSAPItems(result);
            defer.resolve(res);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.checkTabellaConsPerFinal(idPeriodo, fSuccess, fError);
        return defer.promise;
    },
    
    changeStatus: function (codConsulente, idPeriodo, stato) {
        var defer = Q.defer();
            
        var fSuccess = function (result) {
//            var res = model.persistence.Serializer.listaConsulentiPeriodo.fromSAPItems(result);
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.changeStatusTabellaConsPerFinal(codConsulente, idPeriodo, stato, fSuccess, fError);
        return defer.promise;
    },
};