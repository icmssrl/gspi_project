jQuery.sap.declare("model.WBS");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.persistence.SapSerializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.WBS = {

    readWbsSapHana: function (cid, stato) {
        var defer = Q.defer();
        var arrayWbsSapHana = [];
        var fSuccess = function (result) {
            arrayWbsSapHana.push(model.persistence.Serializer.wbsSapHana.fromSapItems(result));
            defer.resolve(arrayWbsSapHana);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readWbsSapHana(cid, stato, fSuccess, fError);

        return defer.promise;
    },
    
    readCommesseHana: function (cid) {
        var defer = Q.defer();
        var arrayCommesseHana = [];
        var fSuccess = function (result) {
            arrayCommesseHana.push(model.persistence.Serializer.commesseHana.fromSapItems(result));
            defer.resolve(arrayCommesseHana);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readCommesseHana(cid, fSuccess, fError);

        return defer.promise;
    },
    
    readWBS: function (cid) {
        var defer = Q.defer();
        var arraySkills = [];
        var fSuccess = function (result) {
                arraySkills.push(model.persistence.SapSerializer.projectEntity.fromSapItems(result));
            defer.resolve(arraySkills);
        };

        var fError = function (err) {
            if (!this.oneTime) {
                this.oneTime = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(this.readWBS(), this));
            }
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readWBS(cid, fSuccess, fError);

        return defer.promise;
    },
    
    getCommessa: function (cid, fSuccess, fError) {
        this._deferCommessa = Q.defer();
        var commessa = [];
        var fSuccess = function (result) {
            commessa.push(model.persistence.SapSerializer.commessaEntity.fromSapItems(result));
            this._deferCommessa.resolve(commessa);
        };

        var fError = function (err) {
            commessa = [];
            this._deferCommessa.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.odata.chiamateOdata.getCommesse(cid, fSuccess, fError);
        return this._deferCommessa.promise;
    },
    
    getMenuTendina: function (type) {
        this._deferMenuTendina = Q.defer();
        var menuTendina = [];
        var fSuccess = function (result) {
            menuTendina.push(model.persistence.SapSerializer.menuTendinaEntity.fromSapItems(result));
            this._deferMenuTendina.resolve(menuTendina);
        };

        var fError = function (err) {
            menuTendina = [];
            this._deferMenuTendina.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.odata.chiamateOdata.getMenuTendina(type, fSuccess, fError);
        return this._deferMenuTendina.promise;
    },
    
    writeNewWBS: function (wbs) {
        this._deferAddWBS = Q.defer();

        var fSuccess = function (result) {
            this._deferAddWBS.resolve(result);
        };

        var fError = function (err) {
            this._deferAddWBS.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.addWBS(wbs, fSuccess, fError);

        return this._deferAddWBS.promise;
    },
    
    writeToSap: function (data) {
        this.getOdataModel();
        this.modelloOdata = this.getView().getModel().createEntry("CIDSet", data.cidElement);
        this._odataModel.submitChanges(function () {
            console.log("OK");
        }, function (err) {
            console.log(err);
        });
    },
    
    changeStatus: function (data) {
        this._deferUpdateWBS = Q.defer();

        var fSuccess = function (result) {
            this._deferUpdateWBS.resolve(result);
        };

        var fError = function (err) {
            this._deferUpdateWBS.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.updateWBS(data, fSuccess, fError);

        return this._deferUpdateWBS.promise;
    },
    
    checkActivities: function (dataI, dataF, cid) {
        this._deferAttivita = Q.defer();
        
        var fSuccess = function (result) {
            this._attivita = {
                dati: []
            };
            for (var i = 0; i < result.results.length; i++) {
                this._attivita.dati.push(model.persistence.SapSerializer.attivitaEntity.fromSap(result.results[i]));
            }
            this._deferAttivita.resolve(this._attivita);
        };

        var fError = function (err) {
            this._attivita = [];
            this._deferAttivita.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        var dataInizio = dataI;
        var dataFine = dataF;
        model.odata.chiamateOdata.getActivities(dataInizio, dataFine, cid, fSuccess, fError);
        
        return this._deferAttivita.promise;
    }
};
