jQuery.sap.declare("model.tabellaSchedulingFinalAllRead");
//jQuery.sap.require("utils.Busy");
//jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.tabellaSchedulingFinalAllRead = {
    
    readAllRequests: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (result && result !== "") {
                defer.resolve(JSON.parse(result));
            } else {
                defer.resolve();
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readPmFinalSchedulingAll(req, fSuccess, fError);
        return defer.promise;
    },
    
    readSingleProject: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (result && result !== "") {
                defer.resolve(JSON.parse(result));
            } else {
                defer.resolve();
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readAdminProjectRequests(req, fSuccess, fError);
        return defer.promise;
    },
    

};
