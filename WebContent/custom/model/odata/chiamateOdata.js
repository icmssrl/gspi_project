jQuery.sap.declare("model.odata.chiamateOdata");
jQuery.sap.require("icms.Component");

model.odata.chiamateOdata = {
    _serviceUrl: (window.location.origin === 'file://' ?  icms.Component.getMetadata().getConfig().settings.dnsServer : '') + icms.Component.getMetadata().getConfig().settings.serverUrl,
    _serverUrlServices: (window.location.origin === 'file://' ?  icms.Component.getMetadata().getConfig().settings.dnsServer : '') + icms.Component.getMetadata().getConfig().settings.serverUrlServices,
    _serverUrlServicesNew: (window.location.origin === 'file://' ?  icms.Component.getMetadata().getConfig().settings.dnsServer : '') + icms.Component.getMetadata().getConfig().settings.serverUrlServicesNew,
    _serviceUrlSap: icms.Component.getMetadata().getConfig().settings.serverSAP,
    _serviceUrlMenuTendina: icms.Component.getMetadata().getConfig().settings.serverUrlMenuTendina,
    _client: icms.Component.getMetadata().getConfig().settings.client,
    _tokUsr: icms.Component.getMetadata().getConfig().settings.tokUsr,
    _tokPsw: icms.Component.getMetadata().getConfig().settings.tokPsw,

    getOdataModel: function () {
        if (!this._odataModel) {
            this._odataModel = new sap.ui.model.odata.ODataModel(this._serviceUrl, true);
        }
        return this._odataModel;
    },

    getOdataModelSap: function () {
        if (!this._odataSapModel) {
            this._odataSapModel = new sap.ui.model.odata.ODataModel(this._serviceUrlSap, true);
        }
        return this._odataSapModel;
    },

    getOdataModelMenuTendina: function () {
        if (!this._odataModelMenuTendina) {
            this._odataModelMenuTendina = new sap.ui.model.odata.ODataModel(this._serviceUrlMenuTendina, true);
        }
        return this._odataModelMenuTendina;
    },

    chiamataForLoginToHana: function () {
        var tok = this._tokUsr + ':' + this._tokPsw;
        var hash = btoa(tok);
        var auth = "Basic " + hash;
        $.ajaxSetup({
            headers: {
                "Authorization": auth
            }
        });
        var defer = Q.defer();
        var fSuccess = function () {
            defer.resolve();
        };
        var fError = function () {
            defer.reject();
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        $.ajax({
            url: this._serviceUrl,
            type: "GET",
            xhrFields: {
                withCredentials: true
            },
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", auth);
            },
            async: false,
            success: fSuccess,
            error: fError
        });
        return defer.promise;
    },

    //    chiamataForLoginToHanaSimple: function (fSuccess, fError) {
    chiamataForLoginToHanaSimple: function () {
        var tok = this._tokUsr + ':' + this._tokPsw;
        var auth = btoa(tok);

        $.ajaxSetup({
            headers: {
                "Authorization": "Basic " + auth,
                "X-CSFR-TOKEN": "fetch"
            }
        });
    },

    chiamataForSap: function () {
        var defer = Q.defer();

        var fSuccessSap = function () {
            defer.resolve();
        };
        var fErrorSap = function () {
            defer.reject();
        };

        fSuccessSap = _.bind(fSuccessSap, this);
        fErrorSap = _.bind(fErrorSap, this);

        $.ajax({
            type: "GET",
            url: icms.Component.getMetadata().getConfig().settings.serverSAP + "?" + icms.Component.getMetadata().getConfig().settings.client,
            context: this,
            username: "FIORI_FTR",
            password: "F1@ry_f7R",
            success: fSuccessSap,
            error: fErrorSap
        });

        return defer.promise;
    },

    doLoginSAP: function (cid, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "anagraficaUserLoginFromSap.xsjs",
            data: JSON.stringify({
                "cidSap": cid
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    doLogin: function (username, pswCriptata, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "anagraficaUserLogin.xsjs",
            data: JSON.stringify({
                "aliasLogin": username,
                "passwordLogin": pswCriptata
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getTiles: function (req, visible, success, error) {
        var url = "";
        if (visible) {
            url = "tileSet?$filter=" + req + " eq 'X' and VISIBILE eq '" + visible + "'";
        } else {
            url = "tileSet?$filter=" + req + " eq 'X'";
        }
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    updateTiles: function (modello, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "updateTiles.xsjs",
            data: JSON.stringify(modello),
            success: fSuccess,
            error: fError,
            dataType: "json",
            contentType: "application/json"
        });
    },

    getTableSchedulingPeriods: function (req, success, error) {
        $.post(
            this._serverUrlServices + 'letturaScheduling.xsjs',
            JSON.stringify(req), success)
            .fail(error);
    },

    doLoginFromFTR: function (alias, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "anagraficaUserLoginFromSap.xsjs",
            data: JSON.stringify({
                "alias": alias
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readSchedulazioniPerControllo: function (codPjm, codConsulente, idPeriodo, success, error) {
        var url = this._serviceUrl + "periodsRequestSet?$filter=COD_CONSULENTE ne '" + codPjm + "' and ID_PERIODO_SCHEDULING_RIC eq " + idPeriodo + " and COD_CONSULENTE eq '" + codConsulente + "'";
        $.ajax({
            type: "GET",
            url: url,
            success: success,
            error: error,
            dataType: "json",
            contentType: "application/json"
        });
    },

    //    getTableSchedulingPeriodsTrial: function (req, success, error) {
    //        $.post(
    //                this._serverUrlServices + 'letturaSchedulingTrial.xsjs',
    //                JSON.stringify(req), success)
    //            .fail(error);
    //    },

    getTableSchedulingPeriodsTrial: function (req, success, error) {
        //        var url = "newPeriodsPjmSet?$filter=ID_PERIODO_SCHEDULING_RIC eq " + req.idPeriodo + " and COD_STAFF eq '" + req.idStaff + "' and COD_PJM eq '" + req.cod_consulente + "'";
        var url = "";
        if (req.idStaff) {
            //            url = "newPeriodsPjmSet?$filter=COD_STAFF eq '" + req.idStaff + "'";
            url = "newPeriodsPjmSet?$filter=ID_PERIODO_SCHEDULING_RIC eq " + req.idPeriodo + " and COD_STAFF eq '" + req.idStaff + "' and (COD_PJM eq '" + req.cod_consulente + "' or COD_PJM eq 'CC00000000') and STATO_REQ ne 'DELETED'";
        } else {
            url = "allConsultantsPjmSet?$filter=COD_PJM_STAFF eq '" + req.cod_consulente + "'";
            //            url = "newPeriodsPjmSet?$filter=ID_PERIODO_SCHEDULING_RIC eq " + req.idPeriodo + " and COD_PJM eq '" + req.cod_consulente + "'";
        }

        var isFirst = true;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getTableSchedulingAllForUm: function (req, success, error) {
        var url = "newPeriodsPjmSet?$filter=ID_PERIODO_SCHEDULING_RIC eq " + req.idPeriodo + " and COD_STAFF eq '" + req.idStaff + "' and COD_PJM eq '" + req.cod_consulente + "' and ID_COMMESSA eq '" + req.idCommessa + "' and STATO_REQ ne 'DELETED'";
        var isFirst = true;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getTableSchedulingAllConsultantsByPJM: function (req, success, error) {
        //        var url = "allConsultantsPjmSet?$filter=(COD_PJM_STAFF eq '" + req.cod_consulente + "' or COD_PJM eq 'CC00000000')";
        var url = "allConsultantsPjmSet?$filter=ID_PERIODO_SCHEDULING_RIC eq " + req.idPeriodo + " and (COD_PJM_STAFF eq '" + req.cod_consulente + "' or COD_PJM eq 'CC00000000')";

        var isFirst = true;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getTableFerieSchedulingPeriods: function (req, success, error) {
        $.post(
            this._serverUrlServices + 'letturaFerieScheduling.xsjs',
            JSON.stringify(req), success)
            .fail(error);
    },

    getTableFerieSchedulingPeriods2: function (req, success, error) {
        $.post(
            this._serverUrlServices + 'letturaFerieScheduling_L220616.xsjs',
            JSON.stringify(req), success)
            .fail(error);
    },

    getTableFerie: function (req, success, error) {
        var url = "readHolidaySet?$filter=COD_CONSULENTE_BU eq '" + req.cod_bu + "' and ID_COMMESSA eq '" + req.idCommessa + "' and ID_PERIODO_SCHEDULING_RIC eq " + req.idPeriodo;

        var isFirst = true;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getTableSchedulingPeriodsUM: function (req, success, error) {
        var url = "";
        if (req.codCons) {
            // to TEST
            url = "newPeriodsDraft?$filter=STATO_REQ_DRAFT eq 'CONFIRMED' and ID_PERIODO_SCHEDULING_DRAFT eq " + req.idPeriodo + " and COD_STAFF eq '" + req.idStaff + "' and COD_CONSULENTE_DRAFT eq '" + req.codCons + "'";
        } else if (req.buUM) {
            url = "newPeriodsDraft?$filter=CODICE_BU eq '" + req.buUM + "' and ID_PERIODO_SCHEDULING_DRAFT eq " + req.idPeriodo;
        } else {
            url = "newPeriodsDraft?$filter=COD_STAFF eq '" + req.idStaff + "' and ID_PERIODO_SCHEDULING_DRAFT eq " + req.idPeriodo;
        }
        var isFirst = true;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getUmAllConsultantsByBu: function (req, success, error) {
        var url = "";
        //        if(req.codCons) {
        //            // to TEST
        //            url = "newPeriodsDraft?$filter=STATO_REQ_DRAFT eq 'CONFIRMED' and ID_PERIODO_SCHEDULING_DRAFT eq " + req.idPeriodo + " and COD_STAFF eq '" + req.idStaff + "' and COD_CONSULENTE_DRAFT eq '" + req.codCons + "'";
        //        } else {
        url = "allConsultantsUmSet?$filter=COD_CONSULENTE_BU eq '" + req.codBu + "'";
        //        }
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getUmAllConsultantsByBuProject: function (req, success, error) {
        var url = "allConsultantsPerProjectUmSet?$filter=CODICE_BU eq '" + req.umBU + "'";
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    //    getFromDraftByIdReq: function (idReq, fSuccess, fError) {
    //        var url = "draftSchedulingSet?$filter=ID_REQ_DRAFT eq " + idReq;
    //        this.getOdataModel().read(url, {
    //            success: fSuccess,
    //            error: fError,
    //            async: true
    //        });
    //    },

    getDraftSingle: function (req, fSuccess, fError) {
        var url = "newPeriodsDraft?$filter=ID_PERIODO_SCHEDULING_DRAFT eq " + req.idPeriodo + " and COD_CONSULENTE_DRAFT eq '" + req.selectedConsultantsCode + "'";
        this.getOdataModel().read(url, {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    getFinalSingle: function (req, fSuccess, fError) {
        var url = "newPeriodsFinal?$filter=ID_PERIODO_SCHEDULING_FINAL eq " + req.idPeriodo + " and COD_CONSULENTE_FINAL eq '" + req.selectedConsultantsCode + "'";
        this.getOdataModel().read(url, {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    deleteSchedulingPeriodsUM: function (periodo, codBu, fSuccess, fError) {
        $.ajax({
            type: "DEL",
            url: this._serverUrlServices + "serviziDatiTabellaDraftV2.xsjs",
            data: JSON.stringify({
                "periodo": periodo,
                "codBu": codBu
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    deleteRequest: function (idReq, state, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "aggiornamentoStatoDraft.xsjs",
            data: JSON.stringify({
                "statoReqDraft": state,
                "idReqDraft": idReq
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readSchedulingPeriodsUM: function (periodo, codBu, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "serviziDatiTabellaDraftV2.xsjs",
            data: JSON.stringify({
                "periodo": periodo,
                "codBu": codBu
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    //    writeSchedulingPeriodsUM: function (request, fSuccess, fError) {
    //        $.ajax({
    //            type: "POST",
    //            url: this._serverUrlServices + "serviziDatiTabellaDraftV2.xsjs",
    //            data: JSON.stringify(request),
    //            success: fSuccess,
    //            error: fError,
    //            dataType: "json",
    //            contentType: "application/json"
    //        });
    //    },

    getTableSchedulingPeriodsMaria: function (req, success, error) {
        var url = "periodsRequestFinalViewSet?$filter=STATO_REQ_FINAL eq 'CONFIRMED' and ID_PERIODO_SCHEDULING_FINAL eq " + req.idPeriodo;
        var isFirst = true;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getOfficialScheduling: function (req, success, error) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readOfficialSchedulingSingleConsultant.xsjs",
            data: JSON.stringify(req),
            success: success,
            error: error,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getOfficialSchedulingByProject: function (req, success, error) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readOfficialSchedulingSingleProject.xsjs",
            data: JSON.stringify(req),
            success: success,
            error: error,
            contentType: "application/x-www-form-urlencoded"
        });
    },
    //    getOfficialScheduling: function (req, success, error) {
    //        var url = "";
    //        if (req.cod_consulente) {
    //            url = "periodsRequestFinalViewSet?$filter=STATO_REQ_FINAL eq 'CONFIRMED' and COD_CONSULENTE_FINAL eq '" + req.cod_consulente + "' and ID_PERIODO_SCHEDULING_FINAL eq " + req.idPeriodo;
    //        } else {
    //            url = "periodsRequestFinalViewSet?$filter=STATO_REQ_FINAL eq 'CONFIRMED' and ID_PERIODO_SCHEDULING_FINAL eq " + req.idPeriodo;
    //        }
    //        var isFirst = true;
    //        this.getOdataModel().read(url, {
    //            success: success,
    //            error: error,
    //            async: true
    //        });
    //    },

    deleteSchedulingPeriodsMaria: function (periodo, fSuccess, fError) {
        $.ajax({
            type: "DEL",
            url: this._serverUrlServices + "serviziDatiTabellaFinal2.xsjs",
            data: JSON.stringify({
                "periodo": periodo
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    deleteRequestAdmin: function (idReq, state, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "aggiornamentoStatoFinal.xsjs",
            data: JSON.stringify({
                "statoReqFinal": state,
                "idReqFinal": idReq
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readSchedulingPeriodsMaria: function (periodo, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "serviziDatiTabellaFinal2.xsjs",
            data: JSON.stringify({
                "periodo": periodo
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    writeSchedulingPeriodsMaria: function (request, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "serviziDatiTabellaFinal.xsjs",
            data: JSON.stringify(request),
            success: fSuccess,
            error: fError,
            dataType: "json",
            contentType: "application/json"
        });
    },

    saveEntity: function (entity, fSuccess, fError) {
        $.post(
            this._serverUrlServices + 'scritturaRichiestePeriodo.xsjs',
            JSON.stringify(entity), fSuccess)
            .fail(fError);
    },

    saveMultiEntity: function (entity, fSuccess, fError) {
        $.post(
            this._serverUrlServices + 'scritturaMultiRichiestePeriodo.xsjs',
            JSON.stringify(entity), fSuccess)
            .fail(fError);
    },

    getProjects: function (req, success, error) {
        var url = "";
        if (!req) {
            url = "projectsViewWithoutPriority";
        } else if (req !== "anag_projects" && typeof (req) !== "number") {
            url = "projectsPeriodViewSet";
        } else if (typeof (req) === "number") {
            url = "projectsViewWithPriority?$filter=ID_PERIODO_SCHEDULING_PROF eq " + req;
        }

        var isFirst = true;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getProjectsPerPJM: function (req, success, error) {
        var url = "projectsViewWithoutPriority?$filter=COD_PJM_AC eq '" + req.codPjm + "' and STATO eq '" + req.stato + "'";
        var isFirst = true;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getCommessePerStaffDelPjm: function (codPjm, success, error) {
        var url = "readCommessePerPjmSet?$filter=COD_PJM_STAFF eq '" + codPjm + "'";
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getCommessePerStaffDelUm: function (codBu, success, error) {
        var url = "";
        if (!codBu) {
            url = "readCommessePerUmSet";
        } else {
            url = "readCommessePerUmSet?$filter=CODICE_BU eq '" + codBu + "'";
        }
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getNewProjects: function (stato, success, error) {
        var url = "";
        if (stato) {
            url = "projectsViewWithoutPriority?$filter=STATO eq '" + stato + "'";
        } else {
            url = "projectsViewWithoutPriority";
        }

        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    addProjects: function (nomeCommessa, descrizioneCommessa, codPjmAssociato, codiceStaffCommessa, statoCommessa, bu, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "scritturaAnagraficaCommesse.xsjs",
            data: JSON.stringify({
                "nomeCommessa": encodeURI(nomeCommessa),
                "descrizioneCommessa": encodeURI(descrizioneCommessa),
                "codPjmAssociato": codPjmAssociato,
                "codiceStaffCommessa": codiceStaffCommessa,
                "statoCommessa": statoCommessa,
                "bu": bu
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    updateProjects: function (codCommessa, nomeCommessa, descrizioneCommessa, codPjmAssociato, codiceStaffCommessa, statoCommessa, bu, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "scritturaAnagraficaCommesse.xsjs",
            data: JSON.stringify({
                "codCommessa": codCommessa,
                "nomeCommessa": encodeURI(nomeCommessa),
                "descrizioneCommessa": encodeURI(descrizioneCommessa),
                "codPjmAssociato": codPjmAssociato,
                "codiceStaffCommessa": codiceStaffCommessa,
                "statoCommessa": statoCommessa,
                "bu": bu
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    updateAnagStaff: function (codCommessa, codPjmAssociato, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateAnagraficaStaff.xsjs",
            data: JSON.stringify({
                "codCommessa": codCommessa,
                "codPjmAssociato": codPjmAssociato
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    deleteProjects: function (codCommessa, fSuccess, fError) {
        $.ajax({
            type: "DEL",
            url: this._serverUrlServices + "scritturaAnagraficaCommesse.xsjs",
            data: JSON.stringify({
                "codCommessa": codCommessa
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getConsultantsFabio: function (req, success, error) {
        var url = "consultantsSet?$filter=COD_PROFILO_CONSULENTE ne '" + req + "'";
        var isFirst = true;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    delEntity: function (entity, fSuccess, fError) {
        $.post(this._serverUrlServices + 'deleteSchedulingRic.xsjs', JSON.stringify(entity), fSuccess)
            .fail(fError);
    },

    delEntityFerie: function (entity, fSuccess, fError) {
        $.post(this._serverUrlServices + 'deleteSchedulingRicFerie.xsjs', JSON.stringify(entity), fSuccess)
            .fail(fError);
    },

    delMultiEntity: function (entity, fSuccess, fError) {
        $.post(this._serverUrlServices + 'deleteMultiSchedulingRic.xsjs', JSON.stringify(entity), fSuccess)
            .fail(fError);
    },

    getConsultants: function (success, error, filtri) {
        var url = "";
        if (filtri) {
            url = this._serviceUrl + "consultantsViewSet" + filtri;
        } else {
            url = this._serviceUrl + "consultantsViewSet";
        }
        $.ajax({
            type: "GET",
            url: url,
            success: success,
            error: error,
            dataType: "json",
            contentType: "application/json"
        });
    },

    getConsultantsWithSkills: function (success, error, filtri) {
        var url = "";
        if (filtri) {
            url = this._serverUrlServices + "getListConsultantWithSkill.xsjs" + filtri;
        } else {
            url = this._serverUrlServices + "getListConsultantWithSkill.xsjs";
        }
        $.ajax({
            type: "GET",
            url: url,
            success: success,
            error: error
        });
    },

    getConsultantsForPm: function (pm, success, error) {
        var url = "";
        if (pm) {
            url = this._serviceUrl + "consultantsViewSet" + filtri;
        } else {
            url = this._serviceUrl + "consultantsViewSet";
        }
        $.ajax({
            type: "GET",
            url: url,
            success: success,
            error: error,
            dataType: "json",
            contentType: "application/json"
        });
    },

    deleteConsultants: function (codConsulente, fSuccess, fError) {
        $.ajax({
            type: "DEL",
            url: this._serverUrlServices + "scritturaAnagraficaConsulenti.xsjs",
            data: JSON.stringify({
                "codConsulente": codConsulente
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    addConsultants: function (cognomeConsulente, nomeConsulente, codProfiloConsulente, codConsulenteBu, descBu, tipoAzienda, stato, note, ggIntercompany, tariffaIntercompany, intercompany, partTime, orePartTime, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "scritturaAnagraficaConsulenti.xsjs",
            data: JSON.stringify({
                "cognomeConsulente": encodeURI(cognomeConsulente),
                "nomeConsulente": encodeURI(nomeConsulente),
                "codProfiloConsulente": codProfiloConsulente,
                "codConsulenteBu": codConsulenteBu,
                "descrizioneBu": descBu,
                "tipoAzienda": tipoAzienda,
                "statoConsulente": stato,
                "noteConsulente": encodeURI(note),
                "ggIntercompany": ggIntercompany,
                "tariffaIntercompany": tariffaIntercompany,
                "intercompany": intercompany,
                "partTime": partTime,
                "orePartTime": orePartTime
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    updateConsultants: function (codConsulente, cognomeConsulente, nomeConsulente, codProfiloConsulente, codConsulenteBu, descBu, tipoAzienda, stato, note, ggIntercompany, tariffaIntercompany, intercompany, partTime, orePartTime, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "scritturaAnagraficaConsulenti.xsjs",
            data: JSON.stringify({
                "codConsulente": codConsulente,
                "cognomeConsulente": encodeURI(cognomeConsulente),
                "nomeConsulente": encodeURI(nomeConsulente),
                "codProfiloConsulente": codProfiloConsulente,
                "codConsulenteBu": codConsulenteBu,
                "descrizioneBu": descBu,
                "tipoAzienda": tipoAzienda,
                "statoConsulente": stato,
                "noteConsulente": encodeURI(note),
                "ggIntercompany": ggIntercompany,
                "tariffaIntercompany": tariffaIntercompany,
                "intercompany": intercompany,
                "partTime": partTime,
                "orePartTime": orePartTime
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readBu: function (fSuccess, fError) {
        $.ajax({
            type: "GET",
            url: this._serverUrlServices + "scritturaAnagraficaConsulenti.xsjs",
            success: fSuccess,
            error: fError
        });
    },

    getProfiles: function (req, success, error) {
        var url = "profilesSet";
        var isFirst = true;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    deletePriority: function (codicePrioritaCommessa, fSuccess, fError) {
        $.ajax({
            type: "DEL",
            url: this._serverUrlServices + "scritturaAnagraficaPrioritaCommesse.xsjs",
            data: JSON.stringify({
                "codicePrioritaCommessa": codicePrioritaCommessa
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    addPriority: function (descrizionePrioritaCommessa, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "scritturaAnagraficaPrioritaCommesse.xsjs",
            data: JSON.stringify({
                "descrizionePrioritaCommessa": descrizionePrioritaCommessa
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    updatePriority: function (codicePrioritaCommessa, descrizionePrioritaCommessa, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "scritturaAnagraficaPrioritaCommesse.xsjs",
            data: JSON.stringify({
                "codicePrioritaCommessa": codicePrioritaCommessa,
                "descrizionePrioritaCommessa": descrizionePrioritaCommessa
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getPriorities: function (req, success, error) {
        var url = "projectPrioritySet";
        var isFirst = true;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getRequestTypes: function (req, success, error) {
        var url = "requestTypesSet";
        var isFirst = true;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    insertConsultantInStaff: function (entity, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "/insertNewConsultantInStaff.xsjs",
            success: fSuccess,
            error: fError,
            data: JSON.stringify(entity),
            dataType: "json",
            contentType: "application/json"
        });
    },

    updateNote: function (entity, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateScheduling.xsjs",
            success: fSuccess,
            error: fError,
            data: JSON.stringify(entity)

        });
    },

    updateVacationRequest: function (entity, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateVacationRequest.xsjs",
            success: fSuccess,
            error: fError,
            data: JSON.stringify(entity)

        });
    },

    updateVacationBatchRequest: function (batch, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateVacationBatchRequest.xsjs",
            success: fSuccess,
            error: fError,
            data: JSON.stringify(batch)

        });
    },

    updateNoteAdmin: function (entity, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "addNoteMaria.xsjs",
            success: fSuccess,
            error: fError,
            data: JSON.stringify(entity)

        });
    },

    updateNoteUM: function (entity, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "addNoteUM.xsjs",
            success: fSuccess,
            error: fError,
            data: JSON.stringify(entity)

        });
    },

    getProfiloCommessaPeriodoSet: function (success, error) {
        var url = "profiloCommessaPeriodoSet";
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getProfiloCommessaPeriodoByPeriod: function (period, success, error) {
        var url = "profiloCommessaPeriodoSet?$filter=ID_PERIODO_SCHEDULING_PROF eq " + period;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getProfiloCommessaPeriodoByCodCommessa: function (codCommessa, success, error) {
        var url = "profiloCommessaPeriodoSet?$filter=COD_COMMESSA_PERIODO eq '" + codCommessa + "'";
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getProfiloCommessaPeriodoByPeriodStatus: function (periodo, stato, success, error) {
        var url = "profiloCommessaPeriodoSet?$filter=ID_PERIODO_SCHEDULING_PROF eq " + periodo + " and STATO eq '" + stato + "'";
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getProfiloCommessaPeriodoByPeriodCommessa: function (periodo, codCommessa, success, error) {
        var url = "profiloCommessaPeriodoSet?$filter=ID_PERIODO_SCHEDULING_PROF eq " + periodo + " and COD_COMMESSA_PERIODO eq '" + codCommessa + "'";
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    updateProfiloCommessaPeriodoSet: function (codCommessaPeriodo, idPeriod, codPrioritaA, codPrioritaB, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "scritturaProfiloCommessaPeriodo.xsjs",
            data: JSON.stringify({
                "codCommessaPeriodo": codCommessaPeriodo,
                "idPeriodoSchedulingProf": idPeriod,
                "codPrioritaA": codPrioritaA,
                "codPrioritaB": codPrioritaB
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    updateStatoProfiloCommessaPeriodoSet: function (codCommessaPeriodo, idPeriod, stato, codiceStaffCommessa, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "scritturaProfiloCommessaPeriodo.xsjs",
            data: JSON.stringify({
                "codCommessaPeriodo": codCommessaPeriodo,
                "idPeriodoSchedulingProf": idPeriod,
                "stato": stato,
                "codiceStaffCommessa": codiceStaffCommessa
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    setStatoProfiloCommessaPeriodoSet: function (codCommessaPeriodo, stato, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "scritturaProfiloCommessaPeriodo.xsjs",
            data: JSON.stringify({
                "codCommessaPeriodo": codCommessaPeriodo,
                "stato": stato
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    newProfiloCommessaPeriodoSet: function (codCommessaPeriodo, idPeriod, codiceStaffComm, stato, codPrioritaA, codPrioritaB, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "scritturaProfiloCommessaPeriodo.xsjs",
            data: JSON.stringify({
                "codCommessaPeriodo": codCommessaPeriodo,
                "idPeriodoSchedulingProf": idPeriod,
                "codStaffCommessaPeriodo": codiceStaffComm,
                "stato": stato,
                "codPrioritaA": codPrioritaA,
                "codPrioritaB": codPrioritaB,
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    newProfiloCommessaPeriodBatch: function (array, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "scritturaProfiloCommessaPeriodoBatch.xsjs",
            data: JSON.stringify(array),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    deleteODataPeriod: function (periodPath, success, error) {
        var path = "/periodsSet" + periodPath;
        this.getOdataModel().remove(path, {
            success: success,
            error: error
        });

    },

    createODataPeriod: function (entity, success, error) {
        var path = "/periodsSet";
        this.getOdataModel().create(path, entity, {
            success: success,
            error: error
        });
    },

    readODataPeriod: function (path, success, error) {
        var url = "/periodsSet";
        url += path;
        this.getOdataModel().read(url, {
            success: success,
            error: error
        });
    },

    getPeriods: function (fSuccess, fError) {
        $.ajax({
            type: "GET",
            url: this._serviceUrl + "periodsSet",
            success: fSuccess,
            error: fError,
            dataType: "json",
            contentType: "application/json"
        });
    },

    checkDate: function (req, success, error) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "checkDate.xsjs",
            data: JSON.stringify(req),
            success: success,
            error: error,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    addPeriod: function (periodObj, success, error) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "scritturaAnagraficaPeriodi2.xsjs",
            data: JSON.stringify({
                "annoPeriodoSchedulazione": periodObj.annoPeriodo,
                "dataFine": periodObj.dataFine,
                "dataFineRichieste": periodObj.dataFineRichieste,
                "dataInizio": periodObj.dataInizio,
                "mesePeriodo": periodObj.mesePeriodo,
                "stato": periodObj.stato,
                "note": periodObj.note,
                "rilascioPjm": periodObj.rilascioPjm,
                "rilascioAll": periodObj.rilascioAll,
                "numSettimane": periodObj.numSettimane,
                "descrizioneMesi": periodObj.descrizioneMesi
            }),
            success: success,
            error: error,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    updatePeriod: function (periodObj, success, error) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "scritturaAnagraficaPeriodi2.xsjs",
            data: JSON.stringify({
                "annoPeriodoSchedulazione": periodObj.annoPeriodo,
                "dataFine": periodObj.dataFine,
                "dataFineRichieste": periodObj.dataFineRichieste,
                "dataInizio": periodObj.dataInizio,
                "idPeriodo": periodObj.idPeriod,
                "mesePeriodo": periodObj.mesePeriodo,
                "stato": periodObj.stato,
                "note": periodObj.note,
                "numSettimane": periodObj.numSettimane,
                "descrizioneMesi": periodObj.descrizioneMesi
            }),
            success: success,
            error: error,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    updatePeriodToRelease: function (periodObj, success, error) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "scritturaAnagraficaPeriodi2.xsjs",
            data: JSON.stringify({
                "annoPeriodoSchedulazione": periodObj.annoPeriodo,
                "dataFine": periodObj.dataFine,
                "dataFineRichieste": periodObj.dataFineRichieste,
                "dataInizio": periodObj.dataInizio,
                "idPeriodo": periodObj.idPeriod,
                "mesePeriodo": periodObj.mesePeriodo,
                "stato": periodObj.stato,
                "note": periodObj.note,
                "rilascioPjm": periodObj.rilascioPjm,
                "rilascioAll": periodObj.rilascioAll
            }),
            success: success,
            error: error,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getBusinessUnitSet: function (codBu, stato, success, error) {
        var url = "listBuSet";
        if (codBu && !stato) {
            url = url + "?$filter=COD_BU eq '" + codBu + "'";
        } else if (!codBu && stato) {
            url = url + "?$filter=STATO eq '" + stato + "'";
        } else if (codBu && stato) {
            url = url + "?$filter=COD_BU eq '" + codBu + "' and STATO eq '" + stato + "'";
        }
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getBuSet: function (success, error) {
        var url = "listBuSet";

        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },


    getGenericConsultants: function (success, error) {
        $.ajax({
            type: "GET",
            url: this._serviceUrl + "genericConsultantListSet",
            success: success,
            error: error,
            dataType: "json",
            contentType: "application/json"
        });
    },

    getAnagStaffCommesseView: function (codConsulente, success, error) {
        var url = "anagStaffViewSet?$filter=COD_CONSULENTE_STAFF eq '" + codConsulente + "'";
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });

    },

    readStaffCommessaView: function (fSuccess, fError) {
        $.ajax({
            type: "GET",
            url: this._serverUrlServices + "letturaStaffCommessa.xsjs",
            success: fSuccess,
            error: fError
        });
    },

    readStaffSingolaCommessaView: function (codCommessa, fSuccess, fError) {
        //        var url = "anagStaffViewSet?$filter=NOME_COMMESSA eq '" + nomeComeessa + "'";
        if (codCommessa.indexOf("AA") === -1) {
            var url = "anagStaffViewSet?$filter=NOME_COMMESSA eq '" + codCommessa + "'";
        } else {
            var url = "anagStaffViewSet?$filter=COD_COMMESSA eq '" + codCommessa + "'";
        }
        this.getOdataModel().read(url, {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    readStaffCompleteView: function (success, error, filtri) {
        var url = "";
        if (filtri) {
            url = this._serviceUrl + "anagStaffViewSet" + filtri;
        } else {
            url = this._serviceUrl + "anagStaffViewSet";
        }
        $.ajax({
            type: "GET",
            url: url,
            success: success,
            error: error,
            dataType: "json",
            contentType: "application/json"
        });
    },

    getNoStaffProject: function (success, error, filtri) {
        var url = "";
        if (filtri) {
            url = this._serviceUrl + "commesseNoStaff" + filtri;
        } else {
            url = this._serviceUrl + "commesseNoStaff";
        }
        $.ajax({
            type: "GET",
            url: url,
            success: success,
            error: error,
            dataType: "json",
            contentTyp: "application/json"
        });
    },

    addBusinessUnit: function (nomeBU, descrizioneBU, statoBu, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "scritturaAnagraficaBusinessUnit.xsjs",
            data: JSON.stringify({
                "nomeBU": nomeBU,
                "descrizioneBU": descrizioneBU,
                "statoBU": statoBu
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    updateBusinessUnit: function (codiceBU, nomeBU, descrizioneBU, statoBu, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "scritturaAnagraficaBusinessUnit.xsjs",
            data: JSON.stringify({
                "nomeBU": nomeBU,
                "descrizioneBU": descrizioneBU,
                "statoBU": statoBu,
                "codiceBU": codiceBU
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    deleteBusinessUnit: function (codiceBU, fSuccess, fError) {
        $.ajax({
            type: "DEL",
            url: this._serverUrlServices + "scritturaAnagraficaBusinessUnit.xsjs",
            data: JSON.stringify({
                "codiceBU": codiceBU
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getMultipleRequests: function (req, success, error) {
        var url = "periodsRequestViewSet";
        var propString = "";
        var isFirst = true;
        for (var prop in req) {
            if (isFirst) {
                url = url.concat("?$filter=");
                isFirst = false;
            } else {
                url = url.concat(" and ");
            };
            if (typeof (req[prop]) == "string") {
                propString = prop + " eq '" + req[prop] + "'";
            } else {
                propString = prop + " eq " + req[prop];
            };
            url = url.concat(propString);
        }
        $.ajax({
            type: "GET",
            url: this._serviceUrl + "genericConsultantListSet",
            success: success,
            error: error,
            dataType: "json",
            contentType: "application/json"
        });
    },

    addStaffToCommessa: function (codCommessa, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "aggiornamentoCommessaNoStaff.xsjs",
            data: JSON.stringify({
                "codCommessa": codCommessa
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    deleteConsultantFromStaff: function (codStaff, codConsulenteStaff, codCommessa, codPJMStaff, fSuccess, fError) {
        $.ajax({
            type: "DELETE",
            url: this._serverUrlServices + "deleteAnagraficaStaffProgetti.xsjs",
            data: JSON.stringify({
                "codStaff": codStaff,
                "codConsulenteStaff": codConsulenteStaff,
                "codCommessa": codCommessa,
                "codPJMStaff": codPJMStaff
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    insertAnagraficaStaff: function (entity, fSuccess, fError) {
        $.post(
            this._serviceUrl + '/scritturaAnagraficaStaffProgetti.xsjs',
            JSON.stringify(entity), fSuccess)
            .fail(fError);
    },

    readSkill: function (success, error) {
        var url = "skillSet";
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    deleteSkill: function (codSkill, fSuccess, fError) {
        $.ajax({
            type: "DELETE",
            url: this._serverUrlServices + "scritturaSkills_GenericConsultants.xsjs",
            data: JSON.stringify({
                "codiceSkill": codSkill
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    addSkill: function (nomeSkill, descrizioneSkill, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "scritturaSkills_GenericConsultants.xsjs",
            data: JSON.stringify({
                "nomeSkill": nomeSkill,
                "descrizioneSkill": descrizioneSkill
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    updateSkill: function (codSkill, nomeSkill, descrizioneSkill, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "scritturaSkills_GenericConsultants.xsjs",
            data: JSON.stringify({
                "codiceSkill": codSkill,
                "nomeSkill": nomeSkill,
                "descrizioneSkill": descrizioneSkill
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getConsulenteSkill: function (codConsulente, fSuccess, fError) {
        var url = "consulenteSkill?$filter=COD_CONSULENTE eq '";
        var isFirst = true;
        this.getOdataModel().read(url + codConsulente + "'", {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    getConsulenteSkillInBatch: function (batchCodConsulente, fSuccess, fError) {
        //        var url = "consulenteSkill?$filter=";
        var url = "consulenteSkill";
        //        var pieces = "";
        //        for (var i = 0; i < batchCodConsulente.length - 1; i++) {
        //            pieces += "COD_CONSULENTE eq '" + batchCodConsulente[i] + "' or ";
        //        }
        //        var lastPiece = "COD_CONSULENTE eq '" + batchCodConsulente[batchCodConsulente.length - 1] + "'";
        //        var fullPiece = pieces + lastPiece;

        //        this.getOdataModel().read(url + fullPiece, {
        this.getOdataModel().read(url, {
            success: fSuccess,
            error: fError,
            async: true
        });

        ////// FUNZIONA MA NON VA BENE
        //        var batchChanges = [];
        //        for (var i = 0; i < batchCodConsulente.length - 1; i++) {
        //            
        //            batchChanges.push(this.getOdataModel().createBatchOperation("consulenteSkill", "GET", batchCodConsulente[i]));
        //        }
        //        
        //        
        //        this.getOdataModel().addBatchReadOperations(batchChanges);
        //        this.getOdataModel().submitBatch(
        //            fSuccess,
        //            fError,
        //            true
        //        );


    },


    readAllConsulenteSkill: function (fSuccess, fError, filtri) {
        var url;
        if (filtri) {
            url = this._serviceUrl + "consulenteSkill" + filtri;
        } else {
            url = this._serviceUrl + "consulenteSkill";
        }
        $.ajax({
            type: "GET",
            url: url,
            success: fSuccess,
            error: fError,
            dataType: "json",
            contentType: "application/json"
        });
    },

    addSkillToConsultant: function (codConsulente, codSkill, pricipale, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "scritturaConsulenteSkill.xsjs",
            data: JSON.stringify({
                "codConsulente": codConsulente,
                "codSkill": codSkill,
                "principale": pricipale ? "X" : ""
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    deleteSkillToConsultant: function (codConsulente, codSkill, fSuccess, fError) {
        $.ajax({
            type: "DELETE",
            url: this._serverUrlServices + "scritturaConsulenteSkill.xsjs",
            data: JSON.stringify({
                "codConsulente": codConsulente,
                "codSkill": codSkill
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    saveFerie: function (entity, fSuccess, fError) {
        $.post(
            this._serverUrlServices + 'scritturaRichiestaFerie.xsjs',
            JSON.stringify(entity), fSuccess)
            .fail(fError);
    },

    getUserLogin: function (alias, fSuccess, fError) {
        var url = "";
        if (!alias) {
            url = "loginUserSet";
        } else {
            url = "loginUserSet('" + alias + "')";
        }
        this.getOdataModel().read(url, {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    addUserLogin: function (alias, cognomeConsulente, nomeConsulente, password, stato, codConsulente, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "insertAnagraficaLoginUser.xsjs",
            data: JSON.stringify({
                "alias": encodeURI(alias),
                "nomeConsulente": encodeURI(nomeConsulente),
                "cognomeConsulente": encodeURI(cognomeConsulente),
                "password": password,
                "stato": stato,
                "codConsulente": codConsulente
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    deleteUserLogin: function (alias, fSuccess, fError) {
        $.ajax({
            type: "DEL",
            url: this._serverUrlServices + "deleteAnagraficaLoginUser.xsjs",
            data: JSON.stringify({
                "alias": alias
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    updateUserLogin: function (alias, cognomeConsulente, nomeConsulente, password, stato, alias_OR, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateAnagraficaLoginUser.xsjs",
            data: JSON.stringify({
                "alias": alias,
                "nomeConsulente": nomeConsulente,
                "cognomeConsulente": cognomeConsulente,
                "password": password,
                "stato": stato,
                "alias_OR": alias_OR
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },
    
    updateUserLoginByCodConsulente: function (alias, cognomeConsulente, nomeConsulente, password, stato, codConsulente, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateAnagraficaLoginUserByCod.xsjs",
            data: JSON.stringify({
                "alias": alias,
                "nomeConsulente": nomeConsulente,
                "cognomeConsulente": cognomeConsulente,
                "password": password,
                "stato": stato,
                "codConsulente": codConsulente
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    updateTeamLeaderStaff: function (teamLeader, codSkill, idStaff, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateTeamLeaderStaff.xsjs",
            data: JSON.stringify({
                "teamLeader": teamLeader,
                "codSkill": codSkill,
                "idStaff": idStaff
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readOfficialScheduling: function (req, success, error) {
        var url;
        if (req !== "") {
            url = "officialSchedulingView" + req;
        } else {
            url = "officialSchedulingView";
        }
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });

    },

    readCommesseSingoloConsulente: function (codConsulente, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "letturaCommesseConsulente.xsjs",
            data: JSON.stringify({
                "codConsulente": codConsulente
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readDayInFinalTable: function (codConsulente, day, idPeriodo, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "letturaFinalForDay.xsjs",
            data: JSON.stringify({
                "codConsulente": codConsulente,
                "giornoPeriodoFinal": day,
                "idPeriodo": idPeriodo
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    saveEntityToDraft: function (entity, fSuccess, fError) {
        $.post(
            this._serverUrlServices + 'insertTabellaDraft.xsjs',
            JSON.stringify(entity), fSuccess)
            .fail(fError);
    },

    saveEntityToFinal: function (entity, fSuccess, fError) {
        $.post(
            this._serverUrlServices + 'insertTabellaFinal.xsjs',
            JSON.stringify(entity), fSuccess)
            .fail(fError);
    },

    saveBatchEntityToFinal: function (batch, fSuccess, fError) {
        $.post(
            this._serverUrlServices + 'insertBatchTabellaFinal.xsjs',
            JSON.stringify(batch), fSuccess)
            .fail(fError);
    },

    saveBatchFerieFinalDraft: function (batch, fSuccess, fError) {
        $.post(
            this._serverUrlServices + 'insertBatchFerieDraftFinal.xsjs',
            JSON.stringify(batch), fSuccess)
            .fail(fError);
    },

    getRequestVacation: function (codConsulente, idPeriodo, fSuccess, fError) {
        //        var url = "periodsRequestSet?$filter=ID_COMMESSA eq 'AA00000008' and ID_PERIODO_SCHEDULING_RIC eq " + dPeriodo + " and COD_CONSULENTE eq '" + codConsulente + "'";
        var url = "periodsRequestSet?$filter=ID_COMMESSA eq 'AA00000008' and COD_CONSULENTE eq '" + codConsulente + "'";
        var isFirst = true;
        this.getOdataModel().read(url, {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    saveRequestVacation: function (entity, fSuccess, fError) {
        $.post(this._serverUrlServices + 'scritturaRichiestaFerieConsulente.xsjs',
            JSON.stringify(entity), fSuccess)
            .fail(fError);
    },

    deleteEntityToDraft: function (idReq, state, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateTabellaDraft.xsjs",
            data: JSON.stringify({
                "statoReqDraft": state,
                "idReqDraft": idReq
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    deleteEntityToFinal: function (idReq, state, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateTabellaFinal.xsjs",
            data: JSON.stringify({
                "statoReqFinal": state,
                "idReqFinal": idReq
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    deleteDefEntityToDraft: function (idReq, fSuccess, fError) {
        $.ajax({
            type: "DEL",
            url: this._serverUrlServices + "deleteTabellaDraft.xsjs",
            data: JSON.stringify({
                "idReqDraft": idReq
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    deleteDefEntityToFinal: function (idReq, fSuccess, fError) {
        $.ajax({
            type: "DEL",
            url: this._serverUrlServices + "deleteTabellaFinal.xsjs",
            data: JSON.stringify({
                "idReqFinal": idReq
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getRequestListAdmin: function (idReq, fSuccess, fError) {
        var url = {};
        if (idReq) {
            url = "periodsModifyRequestViewSet?$filter=STATO_REQ_MODIFICA eq '" + idReq + "'";
        } else {
            url = "periodsModifyRequestViewSet";
        }

        var isFirst = true;
        this.getOdataModel().read(url, {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    getRequestListVacation: function (req, fSuccess, fError) {
        $.post(this._serverUrlServices + 'letturaRichiesteFerieConsulente.xsjs',
            JSON.stringify(req), fSuccess)
            .fail(fError);
    },

    getSchedulingModifyRequests: function (idPJM, stato, success, error) {
        var url = "";
        if (stato !== "ALL") {
            url = "periodsModifyRequestViewSet?$filter=COD_PJM_MODIFICA eq '" + idPJM + "' and STATO_REQ_MODIFICA eq '" + stato + "'";
        } else {
            url = "periodsModifyRequestViewSet?$filter=COD_PJM_MODIFICA eq '" + idPJM + "'";
        }
        this.getOdataModel().read(url, {
            success: success,
            error: error,
        });
    },

    getSingleSchedulingModifyRequest: function (idReqModifica, success, error) {
        var url = "periodsModifyRequestViewSet(" + idReqModifica + ")";
        this.getOdataModel().read(url, {
            success: success,
            error: error,
        });
    },

    deleteModifyRequest: function (idReqModifica, success, error) {
        var idReqModificaInt = parseInt(idReqModifica);
        $.ajax({
            type: "DEL",
            url: this._serverUrlServices + "deleteSchedModRequest.xsjs",
            data: JSON.stringify({
                "idReqModifica": idReqModifica
            }),
            success: success,
            error: error,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    noteRichiesteModifichePeriodo: function (idReqModifica, note, success, error) {
        var idReqModificaInt = parseInt(idReqModifica);
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateNoteRichiestaPeriodoModifica.xsjs",
            data: JSON.stringify({
                "idReqModifica": idReqModificaInt,
                "noteModifica": note
            }),
            success: success,
            error: error,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    checkRichiesteModifichePeriodo: function (periodo, commessa, stato, fSuccess, fError) {
        var url = "periodsModifyRequestSet?$filter=ID_PERIODO_SCHEDULING_RIC_MODIFICA eq " + periodo + " and COD_COMMESSA_MODIFICA eq '" + commessa + "' and STATO_REQ_MODIFICA eq '" + stato + "'";
        this.getOdataModel().read(url, {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    updateRequestListAdmin: function (idReq, stato, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateRichiestaPeriodoModifica.xsjs",
            data: JSON.stringify({
                "statoReqModifica": stato,
                "idReqModifica": idReq
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    //    scritturaRichiesteModifichePeriodo: function (idScambio, giornoPeriodoModifica, noteModifica, statoReqModifica, dataInserimentoModifica, codPjmModifica, idPeriodoSchedModifica, codAttivitaRichiestaModifica, dataRichiestaModifica, codiceConsulenteModifica, codCommessaModifica, codCommessaOldModifica, fSuccess, fError) {
    scritturaRichiesteModifichePeriodo: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "scritturaRichiesteModifichePeriodo.xsjs",
            data: JSON.stringify({
                "idScambio": req.idScambio,
                "giornoPeriodoModifica": req.giornoPeriodoModifica,
                "noteModifica": req.noteModifica,
                "statoReqModifica": req.statoReqModifica,
                "dataInserimentoModifica": req.dataInserimentoModifica,
                "codPjmModifica": req.codPjmModifica,
                "idPeriodoSchedModifica": req.idPeriodoSchedModifica,
                "codAttivitaRichiestaModifica": req.codAttivitaRichiestaModifica,
                "dataRichiestaModifica": req.dataRichiestaModifica,
                "codiceConsulenteModifica": req.codiceConsulenteModifica,
                "codCommessaModifica": req.codCommessaModifica,
                "codCommessaOldModifica": req.codCommessaOldModifica,
                "idReqOriginale": req.idReqOriginale
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    scritturaRichiesteMultiModifichePeriodo: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "scritturaRichiesteMultiModifichePeriodo.xsjs",
            data: JSON.stringify(req),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    cancellazioneRichiesteMultiModifichePeriodo: function (array, fSuccess, fError) {
        $.ajax({
            type: "DELETE",
            url: this._serverUrlServices + "scritturaRichiesteMultiModifichePeriodo.xsjs",
            data: JSON.stringify(array),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    writeSchedulingPeriodsFinalFromRichiestaModifica: function (request, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "updateTabellaFinalFromRichiestaModifica.xsjs",
            data: JSON.stringify(request),
            success: fSuccess,
            error: fError,
            dataType: "json",
            contentType: "application/json"
        });
    },

    updateId: function (idFirstCosnultant, idScambio, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateIdScambioSchedulingModifyRequest.xsjs",
            data: JSON.stringify({
                "idFirstCosnultant": idFirstCosnultant,
                "idScambio": idScambio
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getCurrentUserCID: function (codConsulente, success, error) {
        var url = "cidConsulentiSet('" + codConsulente + "')";
        this.getOdataModel().read(url, {
            success: success,
            error: error,
        });
    },

    loginToSAPToFillTheWar: function (success, error) {
        var sapServerUrl = "http://war.icms.it/sap/opu/odata/SAP/ZFTR_SRV/";
        var client = "sap-client=100";
        $.ajax({
            type: "GET",
            url: sapServerUrl + "?" + client,
            username: "FIORI_FTR",
            password: "F1@ry_f7R",
            success: success,
            error: error
        });
    },

    getStaffByPJMCode: function (code, success, error) {
        var url = "anagStaffViewSet?$filter=COD_PJM_AC eq '" + code + "'";
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });

    },

    checkForPreviousRequestForConsultant: function (data, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "readSchedulingRequestsForConsultant.xsjs",
            data: JSON.stringify(data),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getAllTableSchedulingPeriods: function (req, success, error) {
        //        var url = "singleConsultantPjmSet?$filter=COD_PJM ne '" + req.codPjm + "' and COD_CONSULENTE eq '" + req.codConsulente + "' and ID_PERIODO_SCHEDULING_RIC eq " + req.idPeriodo;
        var url = "singleConsultantPjmSet?$filter=COD_CONSULENTE eq '" + req.codConsulente + "' and ID_PERIODO_SCHEDULING_RIC eq " + req.idPeriodo;
        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    getConsultantFromSkill: function (skill, success, error) {
        var url = "";
        if (skill) {
            url = this._serviceUrl + "consulenteSkill?$filter=NOME_SKILL eq '" + skill + "'";
        } else {
            url = this._serviceUrl + "consulenteSkill";
        }
        //        var url = this._serviceUrl + "consulenteSkill?$filter=NOME_SKILL eq '" + skill + "'";
        $.ajax({
            type: "GET",
            url: url,
            success: success,
            error: error,
            dataType: "json",
            contentType: "application/json"
        });
    },

    changeCodConsAnagStaff: function (codConsGen, codCons, codCommessa, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "changeCodConsAnagStaff.xsjs",
            data: JSON.stringify({
                "codConsGen": codConsGen,
                "codCons": codCons,
                "codCommessa": codCommessa
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    changeCodConsRequest: function (codConsGen, codCons, codCommessa, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "changeCodConsRequest.xsjs",
            data: JSON.stringify({
                "codConsGen": codConsGen,
                "codCons": codCons,
                "codCommessa": codCommessa
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    changeCodConsDraft: function (codConsGen, codCons, codCommessa, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "changeCodConsDraft.xsjs",
            data: JSON.stringify({
                "codConsGen": codConsGen,
                "codCons": codCons,
                "codCommessa": codCommessa
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    changeCodConsFinal: function (codConsGen, codCons, codCommessa, idPeriodo, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "changeCodConsFinal.xsjs",
            data: JSON.stringify({
                "codConsGen": codConsGen,
                "codCons": codCons,
                "codCommessa": codCommessa,
                "idPeriodo": idPeriodo
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    restoreCodConsAnagStaff: function (codCons, codConsGen, codCommessa, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "restoreCodConsAnagStaff.xsjs",
            data: JSON.stringify({
                "codConsGen": codConsGen,
                "codCons": codCons,
                "codCommessa": codCommessa
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    restoreCodConsDraft: function (codCons, codConsGen, codCommessa, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "restoreCodConsDraft.xsjs",
            data: JSON.stringify({
                "codConsGen": codConsGen,
                "codCons": codCons,
                "codCommessa": codCommessa
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getTravelRequests: function (success, error) {
        //var url = "http://gspidev.icms.it:8000/GSPI/odata/odata.xsodata/ftrRichiesteViaggioSet";
        var url = this._serviceUrl + "ftrRichiesteViaggioSet";
        $.ajax({
            type: "GET",
            url: url,
            success: success,
            error: error,
            dataType: "json",
            contentType: "application/x-www-form-urlencoded"
        });
    },

    confirmTravelRequest: function (nomeCognome, cid, societa, cliente, commessa, attivita, elementoWbs, rifatturabile, tipoSpesaCod, tipoSpesaDesc, luogo, dataCheckIn, dataCheckOut, noNotti, dettagliTrasporto, richiesteSpeciali, confermato, noteConferma, dataInserimento, costo, attivitaCod, idRichiesta, fSuccess, fError) {
        var url = this._serverUrlServices + "scritturaRichiesteViaggioFtr.xsjs";
        //        var url = "http://gspidev.icms.it:8000/GSPI/services/scritturaRichiesteViaggioFtr.xsjs";
        $.ajax({
            type: "PUT",
            url: url,
            data: JSON.stringify({
                "nomeCognome": nomeCognome,
                "cid": cid,
                "societa": societa,
                "cliente": cliente,
                "commessa": commessa,
                "attivita": attivita,
                "elementoWbs": elementoWbs,
                "rifatturabile": rifatturabile,
                "tipoSpesaCod": tipoSpesaCod,
                "tipoSpesaDesc": tipoSpesaDesc,
                "luogo": luogo,
                "dataCheckIn": dataCheckIn,
                "dataCheckOut": dataCheckOut,
                "noNotti": noNotti,
                "dettagliTrasporto": dettagliTrasporto,
                "richiesteSpeciali": richiesteSpeciali,
                "confermato": confermato,
                "noteConferma": noteConferma,
                "dataInserimento": dataInserimento,
                "costo": costo,
                "attivitaCod": attivitaCod,
                "idRichiesta": idRichiesta
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getCid: function (alias, password, success, error) {
        this.getOdataModelSap().read("CIDSet?$filter=ZzftrAlias eq '" + alias + "' and ZzftrPwd eq '" + atob(password) + "'&" + this._client, {
            success: success,
            error: error,
            async: true
        });
    },

    getActivities: function (dataInizio, dataFine, cid, success, error) {
        this.getOdataModelSap().read("TimesheetSet?$filter=Workdate ge datetime'" + dataInizio + "' and Workdate le datetime'" + dataFine + "' and Pernr eq '" + cid + "'&" + this._client, {
            success: success,
            error: error,
            async: true
        });
    },

    readWbsSapHana: function (cid, stato, fSuccess, fError) {
        try {
            var url = "";
            if (stato) {
                url = "wbsSapHanaSet?$filter=CID_CONSULENTE_SAP eq '" + cid + "' and STATO eq '" + stato + "'";
            } else {
                url = "wbsSapHanaSet?$filter=CID_CONSULENTE_SAP eq '" + cid + "'";
            }
            this.getOdataModel().read(url, {
                success: fSuccess,
                error: fError,
                async: true
            });
        } catch (err) {
            console.log(err);
            return fError;
        }
    },

    readCommesseHana: function (cid, fSuccess, fError) {
        this.getOdataModel().read("commessePerConsulente?$filter=COD_CONSULENTE_STAFF eq '" + cid + "'", {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    readWBS: function (cid, fSuccess, fError) {
        this.getOdataModelSap().read("ProjectSet?$filter=Pernr eq '" + cid + "'&" + this._client, {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    getCommesse: function (cid, fSuccess, fError) {
        this.getOdataModelSap().read("WbsSet?$filter=Pernr eq '" + cid + "'&" + this._client, {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    getMenuTendina: function (type, success, error) {
        this.getOdataModelMenuTendina().read("ValuesSet?$filter=Fieldname eq '" + type + "'&" + this._client, {
            success: success,
            error: error,
            async: true
        });
    },

    addWBS: function (wbs, fSuccess, fError) {
        var url = this._serverUrlServices + "insertWBS.xsjs";
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(wbs),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    updateWBS: function (data, fSuccess, fError) {
        var url = this._serverUrlServices + "updateWBS.xsjs";
        $.ajax({
            type: "PUT",
            url: url,
            data: JSON.stringify(data),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    deleteVacationRequest: function (codConsulente, idCommessa, idPeriodoSchedulingRic, giornoPeriodo, fSuccess, fError) {
        var url = this._serverUrlServices + "letturaRichiesteFerieConsulente.xsjs";
        $.ajax({
            type: "DELETE",
            url: url,
            data: JSON.stringify({
                "codConsulente": codConsulente,
                "idCommessa": idCommessa,
                "idPeriodoSchedulingRic": idPeriodoSchedulingRic,
                "giornoPeriodo": giornoPeriodo
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    //    checkTabellaConsPerDraft: function (idPeriodo, codUmPjm, fSuccess, fError) {
    //        this.getOdataModel().read("statoConsPerDraftSet?$filter=ID_PERIODO eq " + idPeriodo + " and COD_BU_PJM eq '" + codUmPjm + "'", {
    //            success: fSuccess,
    //            error: fError,
    //            async: true
    //        });
    //    },

    //    

    checkJoinRichiesteDraftCommesse: function (idPeriodo, fSuccess, fError) {
        this.getOdataModel().read("joinRichiesteDraftCommesseSet?$filter=ID_PERIODO_SCHEDULING_DRAFT eq " + idPeriodo, {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    checkTabellaConsPerDraft: function (idPeriodo, fSuccess, fError) {
        this.getOdataModel().read("statoConsPerDraftSet?$filter=ID_PERIODO eq " + idPeriodo, {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    //    writeTabellaConsPerDraft: function (idPeriodo, codUmPjm, fSuccess, fError) {
    writeTabellaConsPerDraft: function (idPeriodo, listaBu, fSuccess, fError) {
        var url = this._serverUrlServices + "scritturaTabellaConsulentePerdiodoDraft.xsjs";
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify({
                "idPeriodoScheduling": idPeriodo,
                "listaBu": listaBu
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    changeStatusTabellaConsPer: function (codConsulente, idPeriodo, stato, codUmPjm, fSuccess, fError) {
        var url = this._serverUrlServices + "scritturaTabellaConsulentePerdiodoDraft.xsjs";
        var data = {
            "idPeriodoScheduling": idPeriodo,
            "codConsulente": codConsulente,
            "stato": stato,
            "codUmPjm": codUmPjm
        };
        $.ajax({
            type: "PUT",
            url: url,
            data: JSON.stringify(data),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },


    /******************************************Table Final**********************************************/

    checkTabellaConsPerFinal: function (idPeriodo, fSuccess, fError) {
        this.getOdataModel().read("statoConsPerFinalSet?$filter=ID_PERIODO eq " + idPeriodo + "", {
            success: fSuccess,
            error: fError,
            async: true
        });
    },
    writeTabellaConsPerFinal: function (idPeriodo, fSuccess, fError) {
        var url = this._serverUrlServices + "scritturaTabellaConsulentePeriodoFinal.xsjs";
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify({
                "idPeriodoScheduling": idPeriodo

            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },
    changeStatusTabellaConsPerFinal: function (codConsulente, idPeriodo, stato, fSuccess, fError) {
        var url = this._serverUrlServices + "scritturaTabellaConsulentePeriodoFinal.xsjs";
        var data = {
            "idPeriodoScheduling": idPeriodo,
            "codConsulente": codConsulente,
            "stato": stato

        };
        $.ajax({
            type: "PUT",
            url: url,
            data: JSON.stringify(data),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },
    /********************************************************************************************************************/
    getSchedulazioniModifiche: function (req, fSuccess, fError) {
        //        var url = "periodsModifyRequestSet?$filter=ID_PERIODO_SCHEDULING_RIC_MODIFICA eq " + req.idPeriodo + " and COD_COMMESSA_MODIFICA eq '" + req.idCommessa + "' and STATO_REQ_MODIFICA eq 'PROPOSED'";
        var url = "periodsModifyRequestSet?$filter=ID_PERIODO_SCHEDULING_RIC_MODIFICA eq " + req.idPeriodo + " and COD_PJM_MODIFICA eq '" + (req.codPjm ? req.codPjm : req.cod_consulente) + "'";
        this.getOdataModel().read(url, {
            success: fSuccess,
            error: fError,
            async: true
        });
    },

    //	getTravelRequests: function (success, error) {
    //        var url = this._serviceUrl + "ftrRichiesteViaggioSet";
    //        $.ajax({
    //            type: "GET",
    //            url: url,
    //            success: success,
    //            error: error,
    //            dataType: "json",
    //            contentType: "application/x-www-form-urlencoded"
    //        });
    //    },

    //    confirmTravelRequest: function (nomeCognome, cid, societa, cliente, commessa, attivita, elementoWbs, rifatturabile, tipoSpesaCod, tipoSpesaDesc, luogo, dataCheckIn, dataCheckOut, noNotti, dettagliTrasporto, richiesteSpeciali, confermato, noteConferma, dataInserimento, costo, attivitaCod, idRichiesta, fSuccess, fError) {
    //        
    //        var url = this._serverUrlServices + "scritturaRichiesteViaggioFtr.xsjs";
    //        $.ajax({
    //            type: "PUT",
    //            url: url,
    //            data: JSON.stringify({
    //                "nomeCognome": nomeCognome,
    //                "cid": cid,
    //                "societa": societa,
    //                "cliente": cliente,
    //                "commessa": commessa,
    //                "attivita": attivita,
    //                "elementoWbs": elementoWbs,
    //                "rifatturabile": rifatturabile,
    //                "tipoSpesaCod": tipoSpesaCod,
    //                "tipoSpesaDesc": tipoSpesaDesc,
    //                "luogo": luogo,
    //                "dataCheckIn": dataCheckIn,
    //                "dataCheckOut": dataCheckOut,
    //                "noNotti": noNotti,
    //                "dettagliTrasporto": dettagliTrasporto,
    //                "richiesteSpeciali": richiesteSpeciali,
    //                "confermato": confermato,
    //                "noteConferma": noteConferma,
    //                "dataInserimento": dataInserimento,
    //                "costo": costo,
    //                "attivitaCod" : attivitaCod,
    //                "idRichiesta": idRichiesta
    //            }),
    //            success: fSuccess,
    //            error: fError,
    //            contentType: "application/x-www-form-urlencoded"
    //        });
    //    }
    newReadPJMRequest: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readPjmRequest.xsjs",
            data: JSON.stringify({
                "codStaff": req.idStaff,
                "idPeriodo": parseInt(req.idPeriodo),
                "cod_consulente": req.cod_consulente,
                "idCommessa": req.idCommessa,
                "nomeCommessa": req.nomeCommessa
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });

    },

    readPjmAllRequests: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readPjmAllRequests.xsjs",
            data: JSON.stringify({
                "idPeriodo": parseInt(req.idPeriodo),
                "cod_consulente": req.cod_consulente
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readPjmSingleConsultant: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readPjmSingleRequest.xsjs",
            data: JSON.stringify({
                "idPeriodo": parseInt(req.idPeriodo),
                "cod_consulente": req.codConsulente
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readUmProjectRequests: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readUmProjectRequests.xsjs",
            data: JSON.stringify({
                "idPeriodo": parseInt(req.idPeriodo),
                "idCommessa": req.idCommessa,
                "idStaff": req.idStaff ? req.idStaff : "CSC0000031",
                "arrayBu": req.arrayBu,
                "arraySkill": req.arraySkill,
                "arrayConsultants": req.arrayConsultants,
                "nomeCommessa": req.nomeCommessa,
                "conflitti": req.conflitti,
                "umBU": req.umBU,
                "selectedConsultantsCode": req.selectedConsultantsCode,
                "single": req.single
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readUmAllRequests: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readUmAllRequests.xsjs",
            data: JSON.stringify({
                "idPeriodo": parseInt(req.idPeriodo),
                "umBU": req.umBU,
                "arrayConsultants": req.arrayConsultants,
                "arrayBu": req.arrayBu,
                "arraySkill": req.arraySkill,
                "conflitti": req.conflitti,
                "consulentiGenerici": req.consulentiGenerici
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readAdminProjectRequests: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readAdminProjectRequest.xsjs",
            data: JSON.stringify(req),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readAdminSingleConsultant: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readAdminSingleConultant.xsjs",
            data: JSON.stringify(req),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readAdminAllRequests: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readAdminAllRequest.xsjs",
            data: JSON.stringify(req),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getGenConsultantSkill: function (fSuccess, fError) {
        $.ajax({
            type: "GET",
            url: this._serviceUrl + "genericConsultantSkillSet?$filter=TIPO_AZIENDA eq 'NO_COMPANY'",
            success: fSuccess,
            error: fError,
            dataType: "json",
            contentType: "application/json"
        });
    },

    getStatoConsPeriodoDraft: function (periodo, fSuccess, fError) {
        $.ajax({
            type: "GET",
            url: this._serviceUrl + "/statoConsPerDraftSet?$filter=ID_PERIODO eq " + periodo,
            success: fSuccess,
            error: fError,
            dataType: "json",
            contentType: "application/json"
        });
    },

    insertMultiInDraft: function (array, fSuccess, fError) {
        $.post(
            this._serverUrlServices + 'scritturaMultiDraft.xsjs',
            JSON.stringify(array), fSuccess)
            .fail(fError);
    },

    deleteMultiInDraft: function (arrayToDel, fSuccess, fError) {
        $.ajax({
            type: "DEL",
            url: this._serverUrlServices + 'cancellazioneMultiDraft.xsjs',
            data: JSON.stringify(arrayToDel),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    modifyMultiInDraft: function (arrayToMod, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + 'modificaMultiDraft.xsjs',
            data: JSON.stringify(arrayToMod),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    updateStatoMultiRichiesteModifiche: function (array, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + 'updateStatoRichiesteModifica.xsjs',
            data: JSON.stringify(array),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readPjmDraft: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readPjmDraft.xsjs",
            data: JSON.stringify({
                "idPeriodo": parseInt(req.idPeriodo),
                "idCommessa": req.idCommessa,
                "cod_consulente": req.cod_consulente,
                "nomeCommessa": req.nomeCommessa,
                "idStaff": req.codStaff
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readPjmRequestModify: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readPjmRequestModify.xsjs",
            data: JSON.stringify({
                "codStaff": req.codStaff,
                "idPeriodo": parseInt(req.idPeriodo),
                "cod_consulente": req.cod_consulente,
                "idCommessa": req.idCommessa,
                "nomeCommessa": req.nomeCommessa
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readFinalProjectRequests: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readFinalPjmProjectRequest.xsjs",
            data: JSON.stringify({
                "idPeriodo": parseInt(req.idPeriodo),
                "idCommessa": req.idCommessa,
                "idStaff": req.idStaff,
                "nomeCommessa": req.nomeCommessa
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readFinalAllRequests: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readFinalPjmAllRequest.xsjs",
            data: JSON.stringify({
                "idPeriodo": parseInt(req.idPeriodo),
                "codPjm": req.codPjm
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readAnalisiCapacita: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readAnalisiCapacitaRisorse.xsjs",
            data: JSON.stringify({
                "idPeriodo": parseInt(req.idPeriodo)
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readAnalisiDisponibilita: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readAnalisiDisponibilitaRisorse.xsjs",
            data: JSON.stringify({
                "idPeriodo": parseInt(req.idPeriodo)
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    checkConflittiInFinal: function (periodo, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/checkAdminConflicts.xsjs",
            data: JSON.stringify({
                "idPeriodo": parseInt(periodo)
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    insertMultiInFinal: function (array, fSuccess, fError) {
        $.post(
            this._serverUrlServices + 'scritturaMultiFinal.xsjs',
            JSON.stringify(array), fSuccess)
            .fail(fError);
    },

    updateHolidaySuFinal: function (req, fSuccess, fError) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateTabellaFinalFerie.xsjs",
            data: JSON.stringify({
                "codAttivita": req.codAttivita,
                "codConsulente": req.codConsulente,
                "idPeriodoFinal": req.idPeriodoFinal,
                "giornoPeriodoFinal": req.giornoPeriodoFinal,
                "note": req.note ? req.note : ""
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    checkListaCommesseConRichiesteModifica: function (periodo, bu, stato, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/checkUmRequestsModify.xsjs",
            data: JSON.stringify({
                "periodo": parseInt(periodo),
                "bu": bu,
                "stato": stato
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    insertConsultantNote: function (period, consultantCode, umCode, administratorCode, consultantNote, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "scritturaUmAdminNoteConsultantV2.xsjs",
            data: JSON.stringify({
                "idPeriodo": period,
                "codConsulente": consultantCode,
                "codUm": umCode,
                "codAdministrator": administratorCode,
                "note": consultantNote
            }),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getAllPjmFromConsultant: function (param, fSuccess, fError) {
        $.post(
            this._serverUrlServices + 'getAllPjmFromConsultant.xsjs',
            JSON.stringify(param), fSuccess)
            .fail(fError);
    },

    sendHolidayMail: function (param, fSuccess, fError) {
        $.post(
            this._serverUrlServices + 'mail/sendEmail.xsjs',
            JSON.stringify(param), fSuccess)
            .fail(fError);
    },

    checkIsToWork: function (params, success, error) {
        var url = "";
        if (params.codCommessa) {
            url = "umWorkOtherProjectSet?$filter=COD_COMMESSA_PERIODO eq '" + params.codCommessa + "' and ID_PERIODO_SCHEDULING_PROF eq " + params.idPeriodo;
        } else {
            url = "umWorkOtherProjectSet?$filter=ID_PERIODO_SCHEDULING_PROF eq " + params.idPeriodo;
        }

        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    setUmIsToWork: function (params, success, error) {
        $.ajax({
            type: "PUT",
            url: this._serverUrlServices + "insertUmWorkOtherProject.xsjs",
            data: JSON.stringify(params),
            crossDomain: true,
            success: success,
            error: error,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getProjectListWithGenericConsultant: function (param, fSuccess, fError) {
        $.post(
            this._serverUrlServices + 'getProjectListWithGenericConsultant.xsjs',
            JSON.stringify(param), fSuccess)
            .fail(fError);
    },

    getProjectListWithGenericConsultantFinal: function (param, fSuccess, fError) {
        $.post(
            this._serverUrlServices + 'getProjectListWithGenericConsultantFinal.xsjs',
            JSON.stringify(param), fSuccess)
            .fail(fError);
    },

    readFinalIcmsAllRequests: function (req, success, error) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readFinalSchedulingAllRequest.xsjs",
            data: JSON.stringify(req),
            crossDomain: true,
            success: success,
            error: error,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readFinalIcmsProjectRequests: function (req, fSuccess, fError) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "model/readFinalSchedulingProjectRequest.xsjs",
            data: JSON.stringify(req),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    getProjectSeePjmScheduling: function (params, success, error) {
        $.ajax({
            type: "POST",
            url: this._serverUrlServices + "getProjectsByPeriod.xsjs",
            data: JSON.stringify(params),
            crossDomain: true,
            success: success,
            error: error,
            contentType: "application/x-www-form-urlencoded"
        });
    },

    readAllProjects: function (success, error) {
        var url = "projectsViewWithoutPriority?$filter=STATO eq 'APERTO'";

        this.getOdataModel().read(url, {
            success: success,
            error: error,
            async: true
        });
    },

    readUmProjectsByPeriod: function (params, success, error) {
        $.ajax({
            type: "GET",
            url: this._serverUrlServicesNew + "getUmProjectsByPeriod.xsjs?PERIOD=" + params.idPeriod + "&BU=" + params.idBu,
            crossDomain: true,
            success: success,
            error: error,
            contentType: "application/json"
        });
    },

    readAdminProjectsByPeriod: function (params, success, error) {
        $.ajax({
            type: "GET",
            url: this._serverUrlServicesNew + "getAdminProjectsByPeriod.xsjs?PERIOD=" + params.idPeriod,
            crossDomain: true,
            success: success,
            error: error,
            contentType: "application/json"
        });
    },
    
    updateFerieinFinal: function (params) {
    	$.ajax({
            type: "PUT",
            url: this._serverUrlServices + "updateFerieInFinal.xsjs",
            data: JSON.stringify(params),
            crossDomain: true,
            success: function () {},
            error: function () {},
            contentType: "application/x-www-form-urlencoded"
        });
    },
    
    checkIfConsultantIsRequired: function (codConsulente, fSuccess, fError) {
    	$.ajax({
            type: "POST",
            url: this._serverUrlServices + "checkIfConsultantIsRequired.xsjs",
            data: JSON.stringify({codConsulente: codConsulente}),
            crossDomain: true,
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    },
    
    deleteUserLoginByCodConsulente: function (codConsulente, fSuccess, fError) {
    	$.ajax({
            type: "DEL",
            url: this._serverUrlServices + "deleteAnagraficaLoginUser.xsjs",
            data: JSON.stringify({
                "codConsulente": codConsulente
            }),
            success: fSuccess,
            error: fError,
            contentType: "application/x-www-form-urlencoded"
        });
    }

};
