jQuery.sap.declare("model.requestSave");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");


model.requestSave = {
    sendRequest: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.saveEntity(req, fSuccess, fError);
        return defer.promise;
    },

    sendMultiRequest: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.saveMultiEntity(req, fSuccess, fError);
        return defer.promise;
    },

    sendHolidayRequest: function (req) {
        var defer = Q.defer();
        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.saveFerie(req, fSuccess, fError);
        return defer.promise;
    },

    changeStatus: function (entity) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateVacationRequest(entity, fSuccess, fError);
        return defer.promise;
    },

    changeStatusBatchRequest: function (batch) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateVacationBatchRequest(batch, fSuccess, fError);
        return defer.promise;
    },

    updateNote: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateNote(req, fSuccess, fError);
        return defer.promise;
    },

    readDayInFinalTable: function (codConsulente, day, idPeriodo) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            result = JSON.parse(result).results
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readDayInFinalTable(codConsulente, parseInt(day), parseInt(idPeriodo), fSuccess, fError);
        return defer.promise;
    },

    insertConsultantNote: function (period, consultantCode, umCode, administratorCode, consultantNote) {
        var defer = Q.defer();

        var fSuccess = function () {
            defer.resolve();
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.insertConsultantNote(period, consultantCode, umCode, administratorCode, consultantNote, fSuccess, fError);
        return defer.promise;
    }
};
