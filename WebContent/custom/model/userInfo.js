jQuery.sap.declare("model.userInfo");
jQuery.sap.require("utils.LocalStorage");

model.userInfo = {
    _cid: undefined,
    _userCoeff: 0.0,
    _type: "A",
    _carType: undefined,

    getCID: function () {
        this._cid = sessionStorage.getItem("cid");
        return this._cid;
    },
    
    getUserCoeff: function () {
        var cidSet = utils.LocalStorage.getItem("cidSet");
        var coeff = cidSet[0].rimborsoChilometrico;
        if (coeff) {
            this._userCoeff = coeff.replace(",", ".");
        } else {
            this._userCoeff = 0.0;
        }
        return this._userCoeff;
    },
    
    getType: function () {
        return this._type;
    },
    
    getCarType: function () {
        return this._carType;
    }
};
