jQuery.sap.declare("model.tabellaSchedulingOnlyReadPJM");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.tabellaSchedulingOnlyReadPJM = {
    // funzione che mi estrae tutti i consulenti dello staff passato
    readStaffPerCommessaPJM: function (req) {
        var defer = Q.defer();
        this.req = req;

        var fSuccess = function (result) {
        	_.remove(result.d.results, {STATO: 'CHIUSO'});
            this.staffCompleteList = model.persistence.Serializer.readStaffCompleteView.fromSAPItems(result);
            this.staffCompleteList.items = _.uniq(this.staffCompleteList.items, 'codConsulenteStaff');
            defer.resolve(this.staffCompleteList);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var filtri = "";
        if (req.idStaff) {
            filtri = "?$filter=COD_STAFF eq " + "'" + req.idStaff + "'";
        } else {
            filtri = "?$filter=COD_PJM_AC eq " + "'" + req.cod_consulente + "'";
        }
        model.odata.chiamateOdata.readStaffCompleteView(fSuccess, fError, filtri);
        return defer.promise;
    },
    
    // funzione che estrae tutte le schedulazioni di tutti i consulenti (schedulati) nel periodo passato, per codice staff e codPJM
    readTable: function (req, staffCompleteList, commessa, allCommesse) {
        var defer = Q.defer();
        this.pjm = req.cod_consulente;
        this.richiesta = req;
        this.staffCompleteList = staffCompleteList.items;
        this.commessa = commessa;
        this.allCommesse = allCommesse;

        var fSuccess = function (result) {
            if (result.results) {
                result.results = model.persistence.Serializer.requests.fromSAPItems(result).items;
            }
            var table = [];
            if (result && result.results) {
                var res3 = result.results;
                for (var i = 0; i < res3.length; i++) {
                    if (this.richiesta.idPeriodo === res3[i].idPeriodoScheduling) {
                        var obj = {};
                        if (res3[i].cod_consulente.indexOf("CCG") >= 0) {
                            obj.nomeConsulente = res3[i].skill;
                        } else {
                            obj.nomeConsulente = res3[i].nomeConsulente;
                        }
                        obj.codiceConsulente = res3[i].cod_consulente;
//                        if (res3[i].codicePJM === this.pjm || res3[i].cod_commessa === "AA00000008") {
                        if (res3[i].cod_commessa === this.richiesta.idCommessa) {
                            if (res3[i].gg) {
                                var prop = "gg" + res3[i].gg;
                                var propIcon = "ggIcon" + res3[i].gg;
                                var propReq = "ggReq" + res3[i].gg;
                                var propDataIns = "ggIns" + res3[i].gg;
                            }
                            if (res3[i].dataReqPeriodo) {
                                obj[propReq] = (res3[i].dataReqPeriodo).getFullYear() + "-" + ((res3[i].dataReqPeriodo).getMonth() + 1) + "-" + (res3[i].dataReqPeriodo).getDate();
                            }
                            if (res3[i].dataInserimento) {
                                obj[propDataIns] = (res3[i].dataInserimento).getFullYear() + "-" + ((res3[i].dataInserimento).getMonth() + 1) + "-" + (res3[i].dataInserimento).getDate();
                            }
                            if (res3[i].nomeCommessa) {
                                obj[prop] = res3[i].nomeCommessa;
                            }

                            if (res3[i].ggIcon) {
                                obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                            }
                            if (res3[i].codicePJM) {
                                obj.codicePJM = res3[i].codicePJM;
                            }
                            if (res3[i].note) {
                                var prop = "nota" + res3[i].gg;
                                obj[prop] = res3[i].note;
                            }
                            if (res3[i].id_req) {
                                var prop = "req" + res3[i].gg;
                                obj[prop] = res3[i].id_req;
                            }
                            if (res3[i].idStaff) {
                                var prop = "idStaff";
                                obj[prop] = res3[i].idStaff;
                            }
                        }
                        table.push(obj);
                    }
                }

                // aggiungo all'array finale tutti i consulenti non schedulati
                for (var t = 0; t < this.staffCompleteList.length; t++) {
                    if (!_.find(table, {
                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff
                        })) {
                        table.push({
                            nomeConsulente: this.staffCompleteList[t].codConsulenteStaff.indexOf("CCG") >= 0 ? this.staffCompleteList[t].descSkill : this.staffCompleteList[t].cognomeConsulente + " " + this.staffCompleteList[t].nomeConsulente,
                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff
                        });
                    }
                }

                this.table = table;

                var fSuccessMod = function (result) {
                    if (result.results && result.results.length > 0) {
                        result.results = model.persistence.Serializer.requestsModificheDraft.fromSAPItems(result).items;
                    }
                    result.results = _.filter(result.results, {codCommessa:this.richiesta.idCommessa})

                    var ris = _.groupBy(result.results, 'codConsulente');
                    // aggiunge le proposte di modifica nell'array
                    for (var n in ris) {
                        if (_.find(this.table, {
                                codiceConsulente: n
                            })) {
                            var ff = _.where(this.table, {
                                codiceConsulente: n
                            });
                            for (var i = 0; i < ris[n].length; i++) {

                                var gg = ris[n][i].gg;
                                var prop = "gg" + gg;
                                for (var t = 0; t < ff.length; t++) {
                                    if (ff[t][prop] && ris[n][i].codAttivita !== "DELETE" && this.commessa === ff[t][prop]) {
                                        var ic = prop.replace("gg", "ggIcon");
                                        var ins = prop.replace("gg", "ggIns");
                                        var gReq = prop.replace("gg", "ggReq");
                                        var req = prop.replace("gg", "req");
                                        var notaP = prop.replace("gg", "nota");

                                        var nome = _.find(this.staffCompleteList, {
                                            codConsulenteStaff: ris[n][i].codConsulente
                                        }).nomeConsulente;
                                        var cognome = _.find(this.staffCompleteList, {
                                            codConsulenteStaff: ris[n][i].codConsulente
                                        }).cognomeConsulente;

                                        if (ff[t][ic] === 'sap-icon://delete') {
                                            ff[t][prop] = _.find(this.allCommesse, {
                                                codCommessa: ris[n][i].codCommessa
                                            }).nomeCommessa;
                                            ff[t][ic] = "sap-icon://" + ris[n][i].codAttivita;
                                            ff[t][gReq] = ris[n][i].dataReq;
                                            ff[t][req] = "M" + ris[n][i].idReq;
                                            ff[t][notaP] = ris[n][i].noteModifica;
                                        } else {
                                            var obj = {};
                                            obj.codiceConsulente = ris[n][i].codConsulente;
                                            obj.codicePJM = ris[n][i].codPjm;
                                            obj[prop] = this.commessa;
                                            obj[ic] = "sap-icon://" + ris[n][i].codAttivita;
                                            obj[ins] = ris[n][i].dataInserimento;
                                            obj[gReq] = ris[n][i].dataReq;
                                            obj[req] = "M" + ris[n][i].idReq;
                                            obj[notaP] = ris[n][i].noteModifica;
                                            obj.idStaff = this.richiesta.idStaff;
                                            obj.nomeConsulente = cognome + " " + nome;

                                            this.table.push(obj);
                                        }
                                    } else if (ff[t][prop] && ris[n][i].codAttivita === "DELETE") {
                                        if (this.commessa === ff[t][prop]) {
                                            var propIcon = prop.replace("gg", "ggIcon");
                                            var idReq = prop.replace("gg", "req");
                                            ff[t][propIcon] = "sap-icon://delete";
                                            ff[t][idReq] = "M" + ris[n][i].idReq;
                                        } else {
                                            //                                            delete ff[t];
                                            _.remove(this.table, ff[t]);
//                                            _.remove(ff, ff[t]);
//                                        t--;
                                        }
                                    } else {
                                        if (t === ff.length - 1) {
                                            if (location.hash.indexOf("seePjmScheduling") > -1) {
                                                if (ris[n][i].codAttivita === 'DELETE') {
                                                    continue;
                                                }
                                            } else {
                                                if (ris[n][i].codAttivita === 'DELETE' && ris[n][i].statoModifica === 'CONFIRMED') {
                                                    continue;
                                                }
                                            }
                                            
                                            
                                            var ic = prop.replace("gg", "ggIcon");
                                            var ins = prop.replace("gg", "ggIns");
                                            var gReq = prop.replace("gg", "ggReq");
                                            var req = prop.replace("gg", "req");
                                            var notaP = prop.replace("gg", "nota");

                                            var nome = _.find(this.staffCompleteList, {
                                                codConsulenteStaff: ris[n][i].codConsulente
                                            }).nomeConsulente;
                                            var cognome = _.find(this.staffCompleteList, {
                                                codConsulenteStaff: ris[n][i].codConsulente
                                            }).cognomeConsulente;

                                            var obj = {};
                                            obj.codiceConsulente = ris[n][i].codConsulente;
                                            obj.codicePJM = ris[n][i].codPjm;
                                            obj[prop] = this.commessa;
                                            obj[ic] = "sap-icon://" + ris[n][i].codAttivita;
                                            obj[ins] = ris[n][i].dataInserimento;
                                            obj[gReq] = ris[n][i].dataReq;
                                            obj[req] = "M" + ris[n][i].idReq;
                                            obj[notaP] = ris[n][i].noteModifica;
                                            obj.idStaff = this.richiesta.idStaff;
                                            obj.nomeConsulente = cognome + " " + nome;

                                            var con1 = _.find(ff, function (o) {
                                                if (o[req] === obj[req]) {
                                                    return true
                                                }
                                            });
                                            var con2 = _.find(ff, function (o) {
                                                if (o[gReq] === obj[gReq]) {
                                                    return true
                                                }
                                            });
                                            if (!con1 && !con2)
                                                this.table.push(obj);

                                        }
                                    }
                                }
                            }
                        }
                    }
                    var res = [];
                    res.push(this.table);
                    defer.resolve(res);
                };
                var fErrorMod = function (err) {
                    defer.reject(err);
                };

                fSuccessMod = _.bind(fSuccessMod, this);
                fErrorMod = _.bind(fErrorMod, this);

                model.odata.chiamateOdata.getSchedulazioniModifiche(this.richiesta, fSuccessMod, fErrorMod);
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getTableSchedulingPeriodsTrial(req, fSuccess, fError);
        return defer.promise;
    },
};

