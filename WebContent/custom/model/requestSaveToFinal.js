jQuery.sap.declare("model.requestSaveToFinal");

model.requestSaveToFinal = {
    sendRequest: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.saveEntityToFinal(req, fSuccess, fError);
        return defer.promise;
    },
    
    delRequest: function (reqId, state) {
        var deferDelRequest = Q.defer();

        var fSuccess = function (result) {
            deferDelRequest.resolve(result);
        };

        var fError = function (err) {
            deferDelRequest.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteEntityToFinal(reqId, state, fSuccess, fError);
        return deferDelRequest.promise;
    },
    
    delDefRequest: function (reqId) {
        var deferDelRequest = Q.defer();

        var fSuccess = function (result) {
            deferDelRequest.resolve(result);
        };

        var fError = function (err) {
            deferDelRequest.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteDefEntityToFinal(reqId, fSuccess, fError);
        return deferDelRequest.promise;
    },
    
    updateFerieinFinal: function (params) {
    	model.odata.chiamateOdata.updateFerieinFinal(params);
    }
};