if (!model.persistence.Serializer)
    jQuery.sap.require("model.persistence.Serializer");
if (!model.odata.chiamateOdata)
    jQuery.sap.require("model.odata.chiamateOdata");

model.CommessePerStaffDelPjm = {
    // funzione chiamata da PmScheduling che estrae tutte le commesse di tutti i consulenti del PJM
    read: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            var arrayProjects = model.persistence.Serializer.commessePerStaffDelPjm.fromSAPItems(result);
            defer.resolve(arrayProjects);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getCommessePerStaffDelPjm(req, fSuccess, fError);
        return defer.promise;
    },
    
    
};