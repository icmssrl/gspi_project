if (!model.persistence.Serializer)
    jQuery.sap.require("model.persistence.Serializer");
if (!model.odata)
    jQuery.sap.require("model.odata.chiamateOdata");

model.Consultants = {
    // estrae tutti i consulenti di una BU
    readConsultants: function (req) {
        var defer = Q.defer();
        var that = this;
        var fSuccess = function (result) {
            this.arrayConsultants = model.persistence.Serializer.consultants.fromSAPItems(result);
            defer.resolve(this.arrayConsultants);
        };
        
        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

//        model.odata.chiamateOdata.getConsultants(fSuccess, fError, req);
        model.odata.chiamateOdata.getConsultantsCid(req, fSuccess, fError);
        return defer.promise;
    },
    
    
    
    getConsultants: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.arrayConsultants = model.persistence.Serializer.consultants.fromSAPItems(result);
            defer.resolve(this.arrayConsultants.items);
        };

        var fError = function (err) {

            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getConsultantsFabio(req, fSuccess, fError);
        return defer.promise;
    },

    

    readConsultantsForPm: function (pm) {
        var defer = Q.defer();
        var that = this;
        var fSuccess = function (result) {
            this.arrayConsultants = model.persistence.Serializer.consultants.fromSAPItems(result);
            defer.resolve(this.arrayConsultants);
        };
        
        var fError = function (err) {
            if (!this.oneTime) {
                this.oneTime = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(that.readConsultantsForPm(pm), that));
            };
            defer.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getConsultantsForPm(pm, fSuccess, fError);
        return defer.promise;
    },

    deleteConsultant: function (codConsulente) {
        this._deferDeleteConsultants = Q.defer();

        var fSuccess = function (result) {
            this._deferDeleteConsultants.resolve(result);
        };

        var fError = function (err) {
            this._deferDeleteConsultants.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteConsultants(codConsulente, fSuccess, fError);

        return this._deferDeleteConsultants.promise;
    },

    addConsultant: function (cognomeConsulente, nomeConsulente, codProfiloConsulente, codConsulenteBu, descBu, tipoAzienda, stato, note, ggIntercompany, tariffaIntercompany, intercompany, partTime, orePartTime, cid) {
        this._deferAddConsultants = Q.defer();

        var fSuccess = function (result) {
            this._deferAddConsultants.resolve(result);
        };

        var fError = function (err) {
            this._deferAddConsultants.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.addConsultants(cognomeConsulente, nomeConsulente, codProfiloConsulente, codConsulenteBu, descBu, tipoAzienda, stato, note,ggIntercompany, tariffaIntercompany, intercompany, partTime, orePartTime, cid, fSuccess, fError);

        return this._deferAddConsultants.promise;
    },

    updateConsultant: function (codConsulente, cognomeConsulente, nomeConsulente, codProfiloConsulente, codConsulenteBu, descBu, tipoAzienda, stato, note, ggIntercompany, tariffaIntercompany, intercompany, partTime, orePartTime) {
        this._deferUpdateConsultants = Q.defer();

        var fSuccess = function (result) {
            this._deferUpdateConsultants.resolve(result);
        };

        var fError = function (err) {
            this._deferUpdateConsultants.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateConsultants(codConsulente, cognomeConsulente, nomeConsulente, codProfiloConsulente, codConsulenteBu, descBu, tipoAzienda, stato, note, ggIntercompany, tariffaIntercompany, intercompany, partTime, orePartTime, fSuccess, fError);

        return this._deferUpdateConsultants.promise;
    },
  
  getGenericConsultants: function () {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.arrayGenericConsultants = model.persistence.Serializer.genericConsultants.fromSAPItems(result);
            defer.resolve(this.arrayGenericConsultants.items);
        };

        var fError = function (err) {
            this.arrayGenericConsultants = [];
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getGenericConsultants(fSuccess, fError);
        return defer.promise;
    },
    
    getConsultantFromSkill: function (skill, fSuccess, fError) {

        this.arrayConsultants = [];
        var defer = Q.defer();
        
        var fSuccess = function (result) {
            this.arrayConsultants = model.persistence.Serializer.consultants.fromSAPItems(result);
            defer.resolve(this.arrayConsultants.items);
        };

        var fError = function (err) {
            this.arrayConsultants = [];
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getConsultantFromSkill(skill, fSuccess, fError);
        return defer.promise;
    },
};
