jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.declare("model.multipleReqMaria");

model.multipleReqMaria = {
  getMultipleReqMaria: function (req) {
    var defer = Q.defer();

    var fSuccess = function (result) {
      var res = model.persistence.Serializer.multipleReq.fromSAPItems(result);
      defer.resolve(res.items);
    };

    var fError = function (err) {

      defer.reject(err);
    };

    fSuccess = _.bind(fSuccess, this);
    fError = _.bind(fError, this);

    var reqUltima = {};
    reqUltima.COD_CONSULENTE = req.idConsulente;
    reqUltima.GIORNO_PERIODO = parseInt(req.giornoPeriodo);

    model.odata.chiamateOdata.getMultipleRequests(reqUltima, fSuccess, fError);
    return defer.promise;
  },

};