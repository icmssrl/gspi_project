if (!model.persistence.Serializer)
    jQuery.sap.require("model.persistence.Serializer");
if (!model.odata)
    jQuery.sap.require("model.odata.chiamateOdata");

model.BuList = {
    // ritorna solo le BU di uno specifico stato (aperto, chiuso)
    getBuListByStatus: function (stato) {
        var defer = Q.defer();
        var buList = [];
        var fSuccess = function (result) {
            buList = model.persistence.Serializer.getBuList.fromSAPItems(result);
            defer.resolve(buList);
        };

        var fError = function (err) {
            buList = [];
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getBusinessUnitSet(undefined, stato, fSuccess, fError);

        return defer.promise;
    },

    getBuList: function () {
        var defer = Q.defer();
        var buList = [];
        var fSuccess = function (result) {
            buList = model.persistence.Serializer.getBuList.fromSAPItems(result);
            defer.resolve(buList);
        };

        var fError = function (err) {
            buList = [];
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getBuList(fSuccess, fError);

        return defer.promise;
    },

    getBuSet: function () {
        var defer = Q.defer();
        var buList = [];
        var fSuccess = function (result) {
            buList = model.persistence.Serializer.getBuList.fromSAPItems(result);
            defer.resolve(buList.items);
        };

        var fError = function (err) {
            buList = [];
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getBuSet(fSuccess, fError);

        return defer.promise;
    }
};
