jQuery.sap.declare("model.Columns");
jQuery.sap.require("model.i18n");


model.Columns = function () {
  
  newFlexBox = function(num)
  {
    var oMyFlexbox = new sap.m.FlexBox({
      direction:"Column",
      alignItems:"Start",
        items: [
    new sap.m.Button({
            icon: "{pmSchedulingModel>ggIcon"+num+"}",
            press: "handleOpen",
            width: "15px"
          }),
    new sap.m.Text({
            text: "{pmSchedulingModel>gg"+num+"}"
          }),
    new sap.m.Text({
            text: "{pmSchedulingModel>gg"+num+"a}"
          })
  ]
      });
    
    return oMyFlexbox;
  };
  
  

  getColumns = function (nC) {
    var arrayC = [];
    for (var i = 0; i < nC; i++) {
      var flex = _.bind(newFlexBox,this);
      var f = flex(i+1);
      var c = new sap.ui.table.Column({
        label: new sap.ui.commons.Label({
          text: "1"
        }),
        template: f
      });

      arrayC.push(c);
    }
    return arrayC;
  };


  //  getSocietyTile = function(society)
  //  {
  //    return _.find(societyColumns, {society : society});
  //  };

  return {
    getMenu: getColumns

  };



}();