jQuery.sap.declare("model.i18n");
// jQuery.sap.require("utils.error");


model.i18n = {
    
    model : undefined,
    
    _getLocaleText: function (key) {
        return this.getModel("i18n").getProperty(key);
    },
    
    getText: function(key, replace){
      if(_.isString(replace)){
        var tmp = replace;
        replace = [tmp];
      }
      return this.getModel().getResourceBundle().getText(key, replace);
    },
    
    getModel: function() {
        
        if (this.model === undefined) {
            
            this.model = new sap.ui.model.resource.ResourceModel({
                bundleUrl: "custom/i18n/text.properties"
                
            });
            this.T = this.model.getResourceBundle();            
        };
        return this.model;
    },
    
    setModel: function(resourceModel){
        this.model = resourceModel;
    },
    
    getData: function() {
        var o = this.getModel().getObject("/");
        if (o === null) {
            return undefined;
        };
        return o;
    }
};
(function(){
  model.i18n.getModel();
})();