jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.Profiles = {
    getProfiles: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.arrayProfiles = model.persistence.Serializer.consultants.fromSAPItems(result);
            defer.resolve(this.arrayProfiles.items);
        };

        var fError = function (err) {

            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getProfilesFabio(req, fSuccess, fError);
        return defer.promise;
    },
    
    readProfiles: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (!result && !this.oneTime) {
                this.oneTime = true;
                model.odata.chiamateOdata.chiamataForLoginToHanaSimple()
                    .then(_.bind(this.readProfiles(), this));
            };
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getProfiles(req, fSuccess, fError);
        return defer.promise;
    },
};
