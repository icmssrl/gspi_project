

model.PjmByConsultant = {
    getRecords: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            var arrayPjm = JSON.parse(result).results;
            defer.resolve(arrayPjm);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getAllPjmFromConsultant(req, fSuccess, fError);
        return defer.promise;
    },

};