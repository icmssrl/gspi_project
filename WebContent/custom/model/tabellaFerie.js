jQuery.sap.declare("model.tabellaFerie");
//jQuery.sap.require("utils.Busy");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.tabellaFerie = {
    
    readFerie: function (req) {
        var defer = Q.defer();
        this.req = req;

        var fSuccess = function (result) {
            //console.log(result);
//            var result = JSON.parse(result);
            if (result.results) {
                result.results = model.persistence.Serializer.holidayRequest.fromSAPItems(result).items;
            }
            var table = [];
//            var arrConsulente = [];
            if (result && result.results) {
                var res3 = result.results;
                
                for (var i = 0; i < res3.length; i++) {
//                    if (!_.find(arrConsulente, {
//                            cod: res3[i].codConsulente
//                        })) {
//                        if (res3[i].codConsulente.indexOf("CCG") < 0) {
//                            arrConsulente.push({
//                                cod: res3[i].codConsulente,
//                                nome: res3[i].nomeConsulente,
//                                cognome: res3[i].cognomeConsulente
//                            });
//                        }
//                    }
                    
                    if (this.req.idPeriodo === res3[i].idPeriodo && res3[i].codCommessa === "AA00000008") {
                    
                    
                        var obj = {};
                        obj.nomeConsulente = res3[i].cognomeConsulente + " " + res3[i].nomeConsulente;
                        obj.codiceConsulente = res3[i].codConsulente;
                        if (res3[i].giornoPeriodo) {
                            var prop = "gg" + res3[i].giornoPeriodo;
                            var propIcon = "ggIcon" + res3[i].giornoPeriodo;
                        }
                        if (res3[i].nomeCommessa) {
                            obj[prop] = res3[i].nomeCommessa;
                        }

                        if (res3[i].codAttivita) {
//                            if (res3[i].ggIcon === "halfDay") {
//                                obj[propIcon] = "./custom/img/half3.png";
//                            } else if (res3[i].ggIcon === "abroad") {
//                                obj[propIcon] = "sap-icon://globe";
//                            } else if (res3[i].ggIcon === "not-customer-and-contacts") {
//                                obj[propIcon] = "sap-icon://employee-rejections";
//                            } else if (res3[i].ggIcon === "not-collaborate") {
//                                obj[propIcon] = "sap-icon://offsite-work";
//                            } else if (res3[i].ggIcon === "not-confirmed") {
//                                obj[propIcon] = "sap-icon://role";
//                            } else if (res3[i].ggIcon === "complete-holidays") {
                            if (res3[i].codAttivita === "complete-holidays") {
                                obj[propIcon] = "sap-icon://travel-itinerary";
                            } else {
                                obj[propIcon] = "sap-icon://" + res3[i].codAttivita;
                            }
                        }
                        if (res3[i].codPjm) {
                            obj.codicePJM = res3[i].codPjm;
                        }
                        if (res3[i].note) {
                            var prop = "nota" + res3[i].giornoPeriodo;
                            obj[prop] = res3[i].note;
                        }
                        if (res3[i].idReq) {
                            var prop = "req" + res3[i].giornoPeriodo;
                            obj[prop] = res3[i].idReq;
                        }
                        if (res3[i].statoReq) {
                            var prop = "stato" + res3[i].giornoPeriodo;
                            obj[prop] = res3[i].statoReq;
                        }
                        
                        table.push(obj);
                    }
                }
//                for (var t = 0; t < arrConsulente.length; t++) {
//                    if (!_.find(table, {
//                            codiceConsulente: arrConsulente[t].cod
//                        })) {
//                        table.push({
//                            nomeConsulente: arrConsulente[t].cognome + " " + arrConsulente[t].nome,
//                            codiceConsulente: arrConsulente[t].cod
//                        });
//                    }
//                }
                var res = [];
                res.push(table);
//                res.push(result.a1);
                defer.resolve(res);
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getTableFerie(req, fSuccess, fError);
        return defer.promise;
    },

    
};