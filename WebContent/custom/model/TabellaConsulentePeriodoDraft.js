jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.TabellaConsulentePeriodoDraft = {
//    check: function (idPeriodo, codUmPjm) {
    check: function (idPeriodo) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            var risultato = "";
            if (result.results.length !== 0) {
                risultato = "OK";
            } else {
                risultato = "KO";
            }
            defer.resolve(risultato);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

//        model.odata.chiamateOdata.checkTabellaConsPerDraft(idPeriodo, codUmPjm, fSuccess, fError);
        model.odata.chiamateOdata.checkTabellaConsPerDraft(idPeriodo, fSuccess, fError);
        return defer.promise;
    },
    
//    write: function (idPeriodo, codUmPjm) {
    write: function (idPeriodo, listaBU) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

//        model.odata.chiamateOdata.writeTabellaConsPerDraft(idPeriodo, codUmPjm, fSuccess, fError);
        model.odata.chiamateOdata.writeTabellaConsPerDraft(idPeriodo, listaBU, fSuccess, fError);
        return defer.promise;
    },
    
    read: function (idPeriodo, codUmPjm) {
        var defer = Q.defer();
            
        var fSuccess = function (result) {
            var res = model.persistence.Serializer.listaConsulentiPeriodo.fromSAPItems(result);
            defer.resolve(res);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.checkTabellaConsPerDraft(idPeriodo, codUmPjm, fSuccess, fError);
        return defer.promise;
    },
    
    changeStuts: function (codConsulente, idPeriodo, stato, codUmPjm) {
        var defer = Q.defer();
            
        var fSuccess = function (result) {
//            var res = model.persistence.Serializer.listaConsulentiPeriodo.fromSAPItems(result);
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.changeStatusTabellaConsPer(codConsulente, idPeriodo, stato, codUmPjm, fSuccess, fError);
        return defer.promise;
    },
};