if (!model.persistence.Serializer)
    jQuery.sap.require("model.persistence.Serializer");
if (!model.odata)
    jQuery.sap.require("model.odata.chiamateOdata");

model.getStatoConsPeriodoDraft = {

    getStatoConsPeriodoDraft: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            getStatoConsPeriodo = model.persistence.Serializer.getStatoConsPeriodoDraft.fromSAPItems(result);
            defer.resolve(getStatoConsPeriodo);
        }

        var fError = function (err) {
            genericConsSkill = [];
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.getStatoConsPeriodoDraft(req, fSuccess, fError);
        
        return defer.promise;
    }
};