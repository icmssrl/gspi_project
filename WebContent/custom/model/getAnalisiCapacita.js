model.getAnalisiCapacita = {
    // funzione che mi restituisce un JSON formattato per la tabella
    read: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(JSON.parse(result));
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readAnalisiCapacita(req, fSuccess, fError);
        return defer.promise;
    },
    
};