jQuery.sap.declare("model.tabellaScheduling");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.OfficialScheduling");

model.tabellaSchedulingOfficial = {
    readTable: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            var r = "";
            if (result) {
                r = JSON.parse(result);
            }
            defer.resolve(r);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if (req.idCommessa) {
            model.odata.chiamateOdata.getOfficialSchedulingByProject(req, fSuccess, fError);
        } else {
            model.odata.chiamateOdata.getOfficialScheduling(req, fSuccess, fError);
        }
        
        
        return defer.promise;
    },
    
//    readTable: function (req) {
//        var defer = Q.defer();
//
//        var fSuccess = function (result) {
//            var requests = [];
//            if (result && result.results) {
//                result.results = model.persistence.Serializer.requestsFinal.fromSAPItems(result).items;
//            };
//            var table = [];
//            if (result && result.results) {
//                var res3 = result.results;
//                for (var i = 0; i < res3.length; i++) {
//                    var obj = {};
//                    obj.nomeConsulente = res3[i].nomeConsulente;
//                    obj.codiceConsulente = res3[i].cod_consulente;
//                    var propNote = "nota" + res3[i].gg;
//                    obj[propNote] = res3[i].note;
//                    if (res3[i].gg) {
//                        var prop = "gg" + res3[i].gg;
//                        var propIcon = "ggIcon" + res3[i].gg;
//                        var propConflict = "ggConflict" + res3[i].gg;
//                        //
//                        var propReq = "ggReq" + res3[i].gg;
//                        var propComm = "ggComm" + res3[i].gg;
//                    };
//                    if (res3[i].nomeCommessa) {
//                        obj[prop] = res3[i].nomeCommessa;
//                    };
//                    
//                    //
//                    if (res3[i].dataReqPeriodo) {
//                        obj[propReq] = res3[i].dataReqPeriodo;
//                    }
//                    
//                    if (res3[i].idCommessa) {
//                        obj[propComm] = res3[i].idCommessa;
//                    }
//                    
//                    
//                    
//                    
//                    /* verifico se ci sono delle giornate con conflitti e in caso metto un'icona di avvertimento */
//                    var richiesteContemporanee = _.where(res3, {
//                        'gg': res3[i].gg,
//                        'cod_consulente': res3[i].cod_consulente
//                    });
//                    var mezzeRichiesteContemporanee = _.where(richiesteContemporanee, {
//                        'ggIcon': "halfDay"
//                    });
//                    var mezzeRichiesteFerieContemporanee = _.where(richiesteContemporanee, {
//                        'ggIcon': "bed"
//                    });
//                    var nRichiesteContemporanee = richiesteContemporanee.length;
//                    var nMezzeRichiesteContemporanee = mezzeRichiesteContemporanee.length + mezzeRichiesteFerieContemporanee.length;
//
//                    if (nMezzeRichiesteContemporanee > 2 || (nRichiesteContemporanee > 1 && nRichiesteContemporanee !== nMezzeRichiesteContemporanee)) {
//                        obj[propConflict] = true;
//                    } else {
//                        obj[propConflict] = false;
//                    };
//
//                    if (res3[i].ggIcon) {
//                        if (res3[i].ggIcon === "halfDay") {
//                            obj[propIcon] = "./custom/img/half3.png";
//                        } else if (res3[i].ggIcon === "abroad") {
//                            obj[propIcon] = "sap-icon://globe";
//                        }
//                        else if (res3[i].ggIcon === "not-collaborate") {
//                            obj[propIcon] = "sap-icon://offsite-work";
//                        }else if (res3[i].ggIcon === "complete-holidays") {
//                            obj[propIcon] = "sap-icon://travel-itinerary";
//                        } else if (res3[i].ggIcon === "not-confirmed") {
//                            obj[propIcon] = "sap-icon://role";
//                        } else if (res3[i].ggIcon === "not-customer-and-contacts") {
//                            obj[propIcon] = "sap-icon://employee-rejections";
//                        } else {
//                            obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
//                        };
//                    };
//                    if (res3[i].codicePJM) {
//                        obj.codicePJM = res3[i].codicePJM;
//                    };
//                    if (res3[i].toCheck) {
//                        var prop = "toCheck" + res3[i].gg;
//                        obj[prop] = res3[i].toCheck;
//                    };
//                    if (res3[i].id_req) {
//                        var prop = "req" + res3[i].gg;
//                        obj[prop] = res3[i].id_req;
//                    };
//                    //**
//                    table.push(obj);
//                };
//                var res = [];
//                res.push(table);
//                defer.resolve(res);
//            };
//        };
//
//        var fError = function (err) {
//            defer.reject(err);
//        };
//
//        fSuccess = _.bind(fSuccess, this);
//        fError = _.bind(fError, this);
//
//        model.odata.chiamateOdata.getOfficialScheduling(req, fSuccess, fError);
//        return defer.promise;
//    },

    readFerieTable: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            //console.log(result);
            var result = JSON.parse(result);
            var table = [];
            if (result && result.results) {
                var res3 = result.results;
                for (var i = 0; i < res3.length; i++) {
                    var obj = {};
                    obj.nomeConsulente = res3[i].nomeConsulente;
                    obj.codiceConsulente = res3[i].cod_consulente;
                    if (res3[i].gg) {
                        var prop = "gg" + res3[i].gg;
                        var propIcon = "ggIcon" + res3[i].gg;
                    };
                    if (res3[i].commessa) {
                        obj[prop] = res3[i].commessa;
                    };

                    if (res3[i].ggIcon) {
                        if (res3[i].ggIcon === "halfDay") {
                            obj[propIcon] = "./custom/img/half3.png";
                        } else if (res3[i].ggIcon === "abroad") {
                            obj[propIcon] = "sap-icon://globe";
                        } else if (res3[i].ggIcon === "not-customer-and-contacts") {
                            obj[propIcon] = "sap-icon://employee-rejections";
                        } else if (res3[i].ggIcon === "not-collaborate") {
                            obj[propIcon] = "sap-icon://offsite-work";
                        } else if (res3[i].ggIcon === "not-confirmed") {
                            obj[propIcon] = "sap-icon://role";
                        } else if (res3[i].ggIcon === "complete-holidays") {
                            obj[propIcon] = "sap-icon://travel-itinerary";
                        } else {
                            obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                        };
                    };
                    if (res3[i].codicePJM) {
                        obj.codicePJM = res3[i].codicePJM;
                    };
                    if (res3[i].note) {
                        var prop = "nota" + res3[i].gg;
                        obj[prop] = res3[i].note;
                    };
                    if (res3[i].id_req) {
                        var prop = "req" + res3[i].gg;
                        obj[prop] = res3[i].id_req;
                    };
                    if (res3[i].idStaff) {
                        var prop = "idStaff";
                        obj[prop] = res3[i].idStaff;
                    }
                    //**
                    table.push(obj);
                };
                var res = [];
                res.push(table);
                res.push(result.a1);
                defer.resolve(res);
            };
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getTableFerieSchedulingPeriods(req, fSuccess, fError);
        return defer.promise;
    },
};
