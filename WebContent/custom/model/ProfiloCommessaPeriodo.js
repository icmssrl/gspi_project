if (!model.persistence.Serializer)
    jQuery.sap.require("model.persistence.Serializer");
if (!model.odata.chiamateOdata)
    jQuery.sap.require("model.odata.chiamateOdata");

model.ProfiloCommessaPeriodo = {
    // funzione che estrae le priorità di tutte le commesse per ogn periodo
    readRecord: function () {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.arrayRecord = model.persistence.Serializer.profiloCommessaPeriodo.fromSAPItems(result);
            defer.resolve(this.arrayRecord);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getProfiloCommessaPeriodoSet(fSuccess, fError);
        return defer.promise;
    },
    
    readRecordByPeriod: function (period) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.arrayRecord = model.persistence.Serializer.profiloCommessaPeriodo.fromSAPItems(result);
            defer.resolve(this.arrayRecord);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getProfiloCommessaPeriodoByPeriod(period, fSuccess, fError);
        return defer.promise;
    },
    
    readRecordByPeriodCommessa: function (period, codCommessa) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.arrayRecord = model.persistence.Serializer.profiloCommessaPeriodo.fromSAPItems(result);
            defer.resolve(this.arrayRecord);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getProfiloCommessaPeriodoByPeriodCommessa(period, codCommessa, fSuccess, fError);
        return defer.promise;
    },
    
    // inserimento di un nuovo record in tabella
    newRecord: function (codCommessaPeriodo, idPeriod, codiceStaffComm, stato, codPrioritaA, codPrioritaB) {
        this._deferNewRecord = Q.defer();

        var fSuccess = function (result) {
            this._deferNewRecord.resolve(result);
        };

        var fError = function (err) {
            this._deferNewRecord.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.newProfiloCommessaPeriodoSet(codCommessaPeriodo, idPeriod, codiceStaffComm, stato, codPrioritaA, codPrioritaB, fSuccess, fError);
        
        return this._deferNewRecord.promise;
    },

    // aggiornamento dei dati in tabella (codPrioritaA e codPrioritaB)
    updateRecord: function (codCommessaPeriodo, idPeriod, codPrioritaA, codPrioritaB) {
        this._deferUpdateRecord = Q.defer();

        var fSuccess = function (result) {
            this._deferUpdateRecord.resolve(result);
        };

        var fError = function (err) {
            this._deferUpdateRecord.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.updateProfiloCommessaPeriodoSet(codCommessaPeriodo, idPeriod, codPrioritaA, codPrioritaB, fSuccess, fError);
        
        return this._deferUpdateRecord.promise;
    },
    
    readRecordByCodCommessa: function (codCommessa) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.arrayRecord = model.persistence.Serializer.profiloCommessaPeriodo.fromSAPItems(result);
            defer.resolve(this.arrayRecord);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getProfiloCommessaPeriodoByCodCommessa(codCommessa, fSuccess, fError);
        return defer.promise;
    },
    
    changeStato: function (codCommessaPeriodo, idPeriod, stato, codiceStaffCommessa) {
        var deferChangeStato = Q.defer();
        
        var fSuccess = function (result) {
            deferChangeStato.resolve(result);
        };

        var fError = function (err) {
            deferChangeStato.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        if (idPeriod) {
            model.odata.chiamateOdata.updateStatoProfiloCommessaPeriodoSet(codCommessaPeriodo, idPeriod, stato, codiceStaffCommessa, fSuccess, fError);
        } else {
            model.odata.chiamateOdata.setStatoProfiloCommessaPeriodoSet(codCommessaPeriodo, stato, fSuccess, fError);
        }
        
        return deferChangeStato.promise;
    },
    
    readRecordByPeriodStatus: function (periodo, stato) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.arrayRecord = model.persistence.Serializer.profiloCommessaPeriodo.fromSAPItems(result);
            defer.resolve(this.arrayRecord);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getProfiloCommessaPeriodoByPeriodStatus(periodo, stato, fSuccess, fError);
        return defer.promise;
    },
    
    // inserimento di un tutte le commesse
    newRecordsFromNewPeriod: function (array) {
        var df = Q.defer();

        var fSuccess = function (result) {
            df.resolve(result);
        };

        var fError = function (err) {
            df.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.newProfiloCommessaPeriodBatch(array, fSuccess, fError);
        
        return df.promise;
    },
};
