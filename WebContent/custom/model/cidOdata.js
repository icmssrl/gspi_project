jQuery.sap.declare("model.cidOdata");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.persistence.SapSerializer");

model.cidOdata = {
    _cid: {
        dati: []
    },
    _deferCid: Q.defer(),

    getCid: function (user, password) {
        this._deferCid = Q.defer();
        var fSuccess = function (result) {
            this._cid.dati = [];
            for (var i = 0; i < result.results.length; i++) {
                this._cid.dati.push(model.persistence.SapSerializer.cidEntity.fromSap(result.results[i]));
            }
            this._deferCid.resolve(this._cid);
        };

        var fError = function (err) {
            this._cid = [];
            this._deferCid.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.odata.chiamateOdata.getCid(user, password, fSuccess, fError);
        return this._deferCid.promise;
    },

    getConsultantListFromSap: function () {
        var defer = Q.defer();
        var fSuccess = function (result) {
            //serializer
            var consultantList = model.persistence.SapSerializer.consultantList.fromSapItems(result);
            defer.resolve(consultantList);
        };

        var fError = function (err) {

            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.odata.chiamateOdata.getConsultantListFromSap(fSuccess, fError);
        return defer.promise;
    },
};