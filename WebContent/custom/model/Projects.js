if (!model.persistence.Serializer)
    jQuery.sap.require("model.persistence.Serializer");
if (!model.odata.chiamateOdata)
    jQuery.sap.require("model.odata.chiamateOdata");

model.Projects = {
    // funzione chiamata da PmScheduling (OK)
    readProjectsPerPJM: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            var arrayProjects = model.persistence.Serializer.projects.fromSAPItems(result);
            defer.resolve(arrayProjects);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getProjectsPerPJM(req, fSuccess, fError);
        return defer.promise;
    },
    
    
    
    readProjects: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (!result && !this.oneTime) {
                this.oneTime = true;
                model.odata.chiamateOdata.chiamataForLoginToHanaSimple()
                    .then(_.bind(this.readProjects(), this));
            }
            this.arrayProjects = model.persistence.Serializer.projects.fromSAPItems(result);
            defer.resolve(this.arrayProjects);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getProjects(req, fSuccess, fError);
        return defer.promise;
    },
    
    
    // funziona chiamata per leggere le commesse
    newReadProjects: function (stato) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (!result || result === '') {
                defer.reject('No Data');
            } else {
                this.arrayProjects = model.persistence.Serializer.projects.fromSAPItems(result);
                defer.resolve(this.arrayProjects);
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getNewProjects(stato, fSuccess, fError);
        return defer.promise;
    },

    addProject: function (nomeCommessa, descrizioneCommessa, codPjmAssociato, codiceStaffCommessa, statoCommessa, codiceBu) {
        this._deferAddProjects = Q.defer();

        var fSuccess = function (result) {
            this._deferAddProjects.resolve(result);
        };

        var fError = function (err) {
            this._deferAddProjects.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.addProjects(nomeCommessa, descrizioneCommessa, codPjmAssociato, codiceStaffCommessa, statoCommessa, codiceBu, fSuccess, fError);

        return this._deferAddProjects.promise;
    },

    updateProject: function (codCommessa, nomeCommessa, descrizioneCommessa, codPjmAssociato, codiceStaffCommessa, statoCommessa, codiceBu) {
        this._deferUpdateProjects = Q.defer();

        var fSuccess = function (result) {
            this._deferUpdateProjects.resolve(result);
        };

        var fError = function (err) {
            this._deferUpdateProjects.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateProjects(codCommessa, nomeCommessa, descrizioneCommessa, codPjmAssociato, codiceStaffCommessa, statoCommessa, codiceBu, fSuccess, fError);

        return this._deferUpdateProjects.promise;
    },

    deleteProject: function (codCommessa) {
        this._deferDeleteProjects = Q.defer();

        var fSuccess = function (result) {
            this._deferDeleteProjects.resolve(result);
        };

        var fError = function (err) {
            this._deferDeleteProjects.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteProjects(codCommessa, fSuccess, fError);

        return this._deferDeleteProjects.promise;
    },
    
    updateAnagStaff: function (codCommessa, codPjmAssociato) {
        var deferUpdateAnagStaff = Q.defer();

        var fSuccess = function (result) {
            deferUpdateAnagStaff.resolve(result);
        };

        var fError = function (err) {
            deferUpdateAnagStaff.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateAnagStaff(codCommessa, codPjmAssociato, fSuccess, fError);

        return deferUpdateAnagStaff.promise;
    },

    readProjectSeePjmScheduling: function (idPeriodo) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (!result && !this.oneTime) {
                this.oneTime = true;
                model.odata.chiamateOdata.chiamataForLoginToHanaSimple()
                    .then(_.bind(this.readProjectSeePjmScheduling(idPeriodo), this));
            }
            var ris = JSON.parse(result);
            this.arrayProjects = model.persistence.Serializer.projects.fromSAPItems(ris);
            defer.resolve(this.arrayProjects);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        var params = {
            idPeriodo: idPeriodo
        }

        model.odata.chiamateOdata.getProjectSeePjmScheduling(params, fSuccess, fError);
        return defer.promise;
    },

    readAllProjects: function (success, error) {
        var fSuccess = _.bind(function (result) {
            var arrayProjects = model.persistence.Serializer.projects.fromSAPItems(result);
            success(arrayProjects);
        }, this);
        
        model.odata.chiamateOdata.readAllProjects(fSuccess, error);
    },

    readUmProjectsByPeriod: function (params, success, error) {
        var fSuccess = _.bind(function (result) {
            var arrayProjects = model.persistence.Serializer.projects.fromSAPItems(JSON.parse(result));
            success(arrayProjects);
        }, this);
        
        model.odata.chiamateOdata.readUmProjectsByPeriod(params, fSuccess, error);
    },

    readAdminProjectsByPeriod: function (params, success, error) {
        var fSuccess = _.bind(function (result) {
            var arrayProjects = model.persistence.Serializer.projects.fromSAPItems(JSON.parse(result));
            success(arrayProjects);
        }, this);
        
        model.odata.chiamateOdata.readAdminProjectsByPeriod(params, fSuccess, error);
    }
};