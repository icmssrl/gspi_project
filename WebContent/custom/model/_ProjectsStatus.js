jQuery.sap.declare("model._ProjectsStatus");

model._ProjectsStatus = {

    getStauts: function () {
        this._deferProjectsStatus = Q.defer();

        var fSuccess = function (result) {
            this._projectsStatus = result;
            this._deferProjectsStatus.resolve(this._projectsStatus);
        };

        var fError = function (err) {
            this._projectsStatus = [];
            this._deferProjectsStatus.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        $.getJSON("custom/model/statoCommessa.json")
            .success(fSuccess)
            .fail(fError);

        return this._deferProjectsStatus.promise;
    },
};
