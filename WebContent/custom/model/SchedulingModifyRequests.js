jQuery.sap.declare("model.SchedulingModifyRequests");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("model.SingleSchedModRequest");

model.SchedulingModifyRequests = {
//    readModifyRequests: function (idPJM, stato) {
//        var defer = Q.defer();
//        this.requestArray = [];
//        var fSuccess = function (result) {
//            if(result.results.length !== 0) {
//                this.requestArray = model.persistence.Serializer.getPeriodsModifyRequest.fromSAPItems(result);
//            }
//            defer.resolve(this.requestArray);
//        };
//
//        var fError = function (err) {
//            defer.reject(err);
//        };
//
//        fSuccess = _.bind(fSuccess, this);
//        fError = _.bind(fError, this);
//
//        model.odata.chiamateOdata.getSchedulingModifyRequests(idPJM, stato, fSuccess, fError);
//        return defer.promise;
//    },
//    
//    getSingleSchedModRequest : function (idReqModifica) {
//
//        var deferSingle = Q.defer();
//        var idReqModificaInteger = parseInt(idReqModifica);
//        var fSuccess = function (result) {
//        	
//        	var data = model.persistence.Serializer.getPeriodsModifyRequest.fromSAP(result);
//            var request = new model.SingleSchedModRequest(data);
//           
//            if(request)
//            {
//            	deferSingle.resolve(request); //
//            }
//            else
//            {
//              console.log("SchedulingModifyRequests.js -- Single scheduling modify request not found!");
//            }
//            
//        };
//
//        var fError = function (err) {
//        	deferSingle.reject(err);
//        	console.log("SchedulingModifyRequests.js -- Single scheduling modify request failed!");
//        };
//
//        fSuccess = _.bind(fSuccess, this);
//        fError = _.bind(fError, this);
//        
//        model.odata.chiamateOdata.getSingleSchedulingModifyRequest(idReqModificaInteger, fSuccess, fError);
//        return deferSingle.promise;
//    },

//    addModifyRequests: function (data) {
//        this._deferAddModifyRequests = Q.defer();
//
//        var fSuccess = function (result) {
//            this._deferAddModifyRequests.resolve(result);
//        };
//
//        var fError = function (err) {
//            this._deferAddModifyRequests.reject(err);
//        };
//
//        fSuccess = _.bind(fSuccess, this);
//        fError = _.bind(fError, this);
//
//        model.odata.chiamateOdata.addSchedulingModifyRequests(data, fSuccess, fError);
//
//        return this._deferAddModifyRequests.promise;
//    },

//    updateModifyRequests: function (data) {
//        this._deferUpdateModifyRequests= Q.defer();
//
//        var fSuccess = function (result) {
//            this._deferUpdateModifyRequests.resolve(result);
//        };
//
//        var fError = function (err) {
//            this._deferUpdateModifyRequests.reject(err);
//        };
//
//        fSuccess = _.bind(fSuccess, this);
//        fError = _.bind(fError, this);
//
//        model.odata.chiamateOdata.updateModifyRequests(data, fSuccess, fError);
//
//        return this._deferUpdateModifyRequests.promise;
//    },

    deleteSingleSchedModRequest: function (idReq) {
        this._deferDeleteModifyRequests = Q.defer();
        
        var fSuccess = function (result) {
            this._deferDeleteModifyRequests.resolve(result);
        };

        var fError = function (err) {
            this._deferDeleteModifyRequests.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteModifyRequest(idReq, fSuccess, fError);

        return this._deferDeleteModifyRequests.promise;
    },
    
    sendRequest: function (req) {
        this._deferSendModifyRequests = Q.defer();
        
        var fSuccess = function (result) {
            this._deferSendModifyRequests.resolve(result);
        };

        var fError = function (err) {
            this._deferSendModifyRequests.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.scritturaRichiesteModifichePeriodo(req, fSuccess, fError);

        return this._deferSendModifyRequests.promise;
    },
    
    updateNote: function (req, note) {
        this._deferNoteModifyRequests = Q.defer();
        
        var fSuccess = function (result) {
            this._deferNoteModifyRequests.resolve(result);
        };

        var fError = function (err) {
            this._deferNoteModifyRequests.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.noteRichiesteModifichePeriodo(req, note, fSuccess, fError);

        return this._deferNoteModifyRequests.promise;
    },
    
    
    sendMultiRequest: function (req) {
        var _deferMultiRequest = Q.defer();
        
        var fSuccess = function (result) {
            _deferMultiRequest.resolve(result);
        };

        var fError = function (err) {
            _deferMultiRequest.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.scritturaRichiesteMultiModifichePeriodo(req, fSuccess, fError);

        return _deferMultiRequest.promise;
    },
    
    deleteMultiRequest: function (arrayDelete) {
        var _deferMultiRequest = Q.defer();
        
        var fSuccess = function (result) {
            _deferMultiRequest.resolve(result);
        };

        var fError = function (err) {
            _deferMultiRequest.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.cancellazioneRichiesteMultiModifichePeriodo(arrayDelete, fSuccess, fError);

        return _deferMultiRequest.promise;
    },
    
//    updateIdScambio: function (idFirstCosnultant, idScambio) {
//        var defer = Q.defer();
//        
//        var fSuccess = function (result) {
//            defer.resolve(result);
//        };
//        
//        var fError = function (err) {
//            defer.reject(err);
//        };
//
//        fSuccess = _.bind(fSuccess, this);
//        fError = _.bind(fError, this);
//
//        model.odata.chiamateOdata.updateId(idFirstCosnultant, idScambio, fSuccess, fError);
//        return defer.promise;
//    },
    
    
    /* PARTE UM */
    checkValueToUpload: function (periodo, commessa, stato) {
        var deferCheck = Q.defer();
        
        var fSuccess = function (result) {
            deferCheck.resolve(model.persistence.Serializer.getModifyRequest.fromSAPItems(result));
        };

        var fError = function (err) {
            deferCheck.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.checkRichiesteModifichePeriodo(periodo, commessa, stato, fSuccess, fError);

        return deferCheck.promise;
    },
    
    updateStatoMulti: function (arrayId) {
        var deferUpdateStato = Q.defer();
        
        var fSuccess = function (result) {
            deferUpdateStato.resolve(result);
        };

        var fError = function (err) {
            deferUpdateStato.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.updateStatoMultiRichiesteModifiche(arrayId, fSuccess, fError);

        return deferUpdateStato.promise;
    },
    
    //* PJM READ MOFIFY REQUEST JPIN NORMAL REQUESTS
    
    readPjmRequestModify: function (req) {
         var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(JSON.parse(result));
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);


        model.odata.chiamateOdata.readPjmRequestModify(req, fSuccess, fError);
        return defer.promise;
    },
    
    
};
