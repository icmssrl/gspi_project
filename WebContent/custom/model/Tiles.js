jQuery.sap.declare("model.Tiles");
jQuery.sap.require("model.i18n");

model.Tiles = function () {
    TileCollection = [
        {
            "icon": "customer-view",
            "title": model.i18n._getLocaleText("VIEW_NEW_REQUESTS"),
            "url": "pmScheduling",
            "type": ["Project Manager", "Unit Manager", "Administrator", "SuperUser"]
        },
        {
            "icon": "edit",
            "title": model.i18n._getLocaleText("VIEW_MODIFY_REQUESTS"),
            "url": "pmModifyRequest",
            "type": ["Project Manager", "Unit Manager", "Administrator", "SuperUser"]
        },
//        {
//            "icon": "appointment",
//            "title": model.i18n._getLocaleText("VIEW_NEW_REQUESTS_AND_CHANGES"),
//            "url": "pmSchedulingDraft",
//            "type": ["Project Manager", "Unit Manager", "Administrator"]
//        },
        {
            "icon": "inspection",
            "title": model.i18n._getLocaleText("VIEW_SEE_REQUESTS"),
            "url": "seePjmScheduling",
            "type": ["Unit Manager", "Administrator", "SuperUser"]
        },

        {
            "icon": "gantt-bars",
            "title": model.i18n._getLocaleText("VIEW_NEW_REQUESTS_2"),
            "url": "umScheduling",
            "type": ["Administrator", "Unit Manager", "SuperUser"]
        },
        {
            "icon": "request",
            "title": model.i18n._getLocaleText("VIEW_NEW_REQUESTS_2_WRITE"),
            "url": "umSchedulingWrite",
            "type": ["Administrator", "SuperUser"]
        },
        {
            "icon": "save",
            "title": model.i18n._getLocaleText("VIEW_NEW_REQUESTS_3"),
            "url": "administratorScheduling",
            "type": ["Administrator", "SuperUser"]
        },
        {
            "icon": "request",
            "title": model.i18n._getLocaleText("VIEW_NEW_REQUESTS_FINAL_WRITE"),
            "url": "administratorSchedulingWrite",
            "type": ["Administrator", "SuperUser"]
        },
        {
            "icon": "bbyd-active-sales",
            "title": model.i18n._getLocaleText("VIEW_NEW_REQUESTS_4"),
            "url": "administratorSchedulingReadOnly",
            "type": ["Administrator", "Unit Manager", "Project Manager", "SuperUser"]
        },
        {
            "icon": "bbyd-dashboard",
            "title": model.i18n._getLocaleText("VIEW_SCHEDULING_ALL_ICMS"),
            "url": "finalSchedulingAll",
            "type": ["Administrator", "Unit Manager", "Project Manager", "Consultant", "SuperUser", "Ufficio Amministrativo"]
        },
        {
            "icon": "manager-insight",
            "title": model.i18n._getLocaleText("ANALISI_CAPACITA_RISORSE"),
            "url": "analisiCapacitaRisorse",
            "type": ["Administrator", "Unit Manager", "SuperUser"]
        },
        {
            "icon": "horizontal-stacked-chart",
            "title": model.i18n._getLocaleText("ANALISI_DISPONIBILITA_RISORSE"),
            "url": "analisiDisponibilitaRisorse",
            "type": ["Administrator", "Unit Manager", "SuperUser"]
        },
        {
            "icon": "travel-itinerary",
            "title": model.i18n._getLocaleText("HOLIDAYS"),
            "url": "holidaysScheduling",
            "type": ["Unit Manager", "Administrator", "SuperUser"]
        },
        {
            "icon": "create-leave-request",
            "title": model.i18n._getLocaleText("VACATIONREQUEST"),
            "url": "vacationRequestsList",
            "type": ["Project Manager", "Administrator", "Consultant", "Unit Manager", "SuperUser"]
        },
        {
            "icon": "add-contact",
            "title": model.i18n._getLocaleText("CREATE_NEW_USER"),
            "url": "consultantList",
            "type": ["Administrator", "SuperUser"]
        },
        {
            "icon": "add-activity-2",
            "title": model.i18n._getLocaleText("PROJECT"),
            "url": "projectList",
            "type": ["Administrator", "Unit Manager", "SuperUser"]
        },
        {
            "icon": "appointment",
            "title": model.i18n._getLocaleText("PERIOD"),
            "url": "periodList",
            "type": ["Administrator", "SuperUser"]
        },
        {
            "icon": "goalseek",
            "title": model.i18n._getLocaleText("PRIORITY"),
            "url": "priorityList",
            "type": ["Administrator", "SuperUser"]
        },
        {
            "icon": "add-coursebook",
            "title": model.i18n._getLocaleText("REQUEST_TYPE"),
            "url": "requestTypeList",
            "type": ["SuperUser"]
        },
        {
            "icon": "official-service",
            "title": model.i18n._getLocaleText("BUSINESS_UNIT"),
            "url": "businessUnitList",
            "type": ["Administrator", "SuperUser"]
        },
        {
            "icon": "collaborate",
            "title": model.i18n._getLocaleText("ANAG_STAFF"),
            "url": "anagStaffList",
            "type": ["Administrator", "Unit Manager", "SuperUser"]
        },
        {
            "icon": "activity-assigned-to-goal",
            "title": model.i18n._getLocaleText("ANAG_SKILL"),
            "url": "skillList",
            "type": ["Administrator", "SuperUser"]
        },
        {
            "icon": "customer-briefing",
            "title": model.i18n._getLocaleText("USER_ACCOUNT_LOGIN"),
            "url": "userLoginList",
            "type": ["SuperUser"]
        },
        {
            "icon": "activity-individual",
            "title": model.i18n._getLocaleText("OFFICIAL_SCHEDULING"),
            "url": "officialScheduling",
            "type": ["Project Manager", "Administrator", "Consultant", "Unit Manager", "SuperUser"]
        },
//        {
//            "icon": "edit-outside",
//            "title": model.i18n._getLocaleText("APPROVE_MODIFY_REQUEST_PJM"),
//            "url": "modifyRequestsList",
//            "type": ["Project Manager", "Administrator", "Unit Manager"]
//        },
//        {
//            "icon": "activate",
//            "title": model.i18n._getLocaleText("APPROVE_MODIFY_REQUEST"),
//            "url": "ricModAdminList",
//            "type": ["Administrator"]
//        },
//        {
//            "icon": "travel-itinerary",
//            "title": model.i18n._getLocaleText("TRAVEL_REQUEST_LIST"),
//            "url": "travelRequestList",
//            "type": ["Ufficio Amministrativo", "SuperUser"]
//        },
//        {
//            "icon": "calendar",
//            "title": model.i18n._getLocaleText("WAR"),
//            "url": "",
//            "type": ["Project Manager", "Administrator", "Consultant", "Unit Manager"]
//        },
//        {
//            "icon": "hr-approval",
//            "title": model.i18n._getLocaleText("schedulingApprove"),
//            "url": "",
//            "type": ["Project Manager", "Administrator", "Unit Manager"]
//        },
//        {
//            "icon": "action-settings",
//            "title": model.i18n._getLocaleText("settingsWBS"),
//            "url": "settingsWBSList",
//            "type": ["Project Manager", "Administrator", "Unit Manager", "Consultant"]
//        },
    ];

    getTiles = function (profile, companyType) {
        var arrayTiles = [];
        for (var i = 0; i < TileCollection.length; i++) {
            var tile = TileCollection[i];
            var types = tile.type;
            for (var t = 0; t < types.length; t++) {
                if (types[t] === profile) {
                    if (tile.url !== "finalSchedulingAll" || tile.url === "finalSchedulingAll" && profile !== "Consultant" || tile.url === "finalSchedulingAll" && profile === "Consultant" && companyType === "Interni") {
                        arrayTiles.push(tile);
                    }
                }
            }
        }
        return arrayTiles;
    };

    return {
        getMenu: getTiles
    };
}();
