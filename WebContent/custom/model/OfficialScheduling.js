jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.declare("model.OfficialScheduling");

model.OfficialScheduling = {
    getOfficialScheduling: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.arrayScheduling = model.persistence.Serializer.readOfficialScheduling.fromSAPItems(result);
            defer.resolve(this.arrayScheduling);
        };

        //            var fSuccess = function (result) {
        //            var result = JSON.parse(result);
        //            var table = [];
        //            if (result && result.results) {
        //                var res3 = result.results;
        //                for (var i = 0; i < res3.length; i++) {
        //                    var obj = {};
        //                    obj.nomeConsulente = res3[i].nomeConsulente;
        //                    obj.codiceConsulente = res3[i].cod_consulente;
        //                    if (res3[i].gg) {
        //                        var prop = "gg" + res3[i].gg;
        //                        var propIcon = "ggIcon" + res3[i].gg;
        //                    };
        //                    if (res3[i].commessa) {
        //                        obj[prop] = res3[i].commessa;
        //                    };
        //
        //                    if (res3[i].ggIcon) {
        //                        if (res3[i].ggIcon === "halfDay") {
        //                            obj[propIcon] = "./custom/img/half3.png";
        //                        } else if (res3[i].ggIcon === "abroad") {
        //                            obj[propIcon] = "sap-icon://globe";
        //                        } else if (res3[i].ggIcon === "not-customer-and-contacts") {
        //                            obj[propIcon] = "sap-icon://employee-rejections";
        //                        } else if (res3[i].ggIcon === "not-collaborate") {
        //                            obj[propIcon] = "sap-icon://offsite-work";
        //                        } else if (res3[i].ggIcon === "not-confirmed") {
        //                            obj[propIcon] = "sap-icon://role";
        //                        } else if (res3[i].ggIcon === "complete-holidays") {
        //                            obj[propIcon] = "sap-icon://travel-itinerary";
        //                        } else {
        //                            obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
        //                        };
        //                    };
        //                    if (res3[i].codicePJM) {
        //                        obj.codicePJM = res3[i].codicePJM;
        //                    };
        //                    if (res3[i].note) {
        //                        var prop = "nota" + res3[i].gg;
        //                        obj[prop] = res3[i].note;
        //                    };
        //                    if (res3[i].id_req) {
        //                        var prop = "req" + res3[i].gg;
        //                        obj[prop] = res3[i].id_req;
        //                    };
        //                    if (res3[i].idStaff) {
        //                        var prop = "idStaff";
        //                        obj[prop] = res3[i].idStaff;
        //                    }
        //                    //**
        //                    table.push(obj);
        //                };
        //                var res = [];
        //                res.push(table);
        //                res.push(result.a1);
        //                defer.resolve(res);
        //            };
        //        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readOfficialScheduling(req, fSuccess, fError);
        return defer.promise;
    },

    readCommesseSingoloConsulente: function (codConsulente) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readCommesseSingoloConsulente(codConsulente, fSuccess, fError);
        return defer.promise;
    }
}
