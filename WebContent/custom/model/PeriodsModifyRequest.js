jQuery.sap.declare("model.PeriodsModifyRequest");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.PeriodsModifyRequest = {
    getRequestList: function (req) {
        var defer = Q.defer();
        var requestList = [];
        var fSuccess = function (result) {
            if (result) {
                requestList = model.persistence.Serializer.getPeriodsModifyRequest.fromSAPItems(result);
                defer.resolve(requestList);
            } else {
                defer.reject("undefined");
            }
        };

        var fError = function (err) {
            requestList = [];
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getRequestListAdmin(req, fSuccess, fError);

        return defer.promise;
    },

    discardRequest: function (idReq, stato) {
        var defer = Q.defer();
        var requestList = [];
        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            requestList = [];
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateRequestListAdmin(idReq, stato, fSuccess, fError);

        return defer.promise;
    },

    writeSchedulingPeriodsFinal: function (requests) {
        var defer = Q.defer();
        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            requestList = [];
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.writeSchedulingPeriodsFinalFromRichiestaModifica(requests, fSuccess, fError);

        return defer.promise;
    },
    
    getAllRequest: function () {
        var defer = Q.defer();
        var requestList = [];
        var fSuccess = function (result) {
            if (result) {
                requestList = model.persistence.Serializer.getPeriodsModifyRequest.fromSAPItems(result);
                defer.resolve(requestList);
            } else {
                defer.reject("undefined");
            }
        };

        var fError = function (err) {
            requestList = [];
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getRequestListAdmin(undefined, fSuccess, fError);

        return defer.promise;
    },
};