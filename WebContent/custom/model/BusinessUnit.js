jQuery.sap.require("model.odata.chiamateOdata");

model.BusinessUnit = {
    readBu: function (codBu, stato) {
        this._deferBuList = Q.defer();

        var fSuccess = function (result) {
//            this.arrayBu = model.persistence.Serializer.bu.fromSAPItems(result);
            var buList = model.persistence.Serializer.getBuList.fromSAPItems(result);
            this._deferBuList.resolve(buList);
        };

        var fError = function (err) {
            this._deferBuList.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getBusinessUnitSet(codBu, stato,fSuccess, fError);

        return this._deferBuList.promise;
    },
    
    deleteBu: function(codiceBU){
        this._deferDeleteBu = Q.defer();
        
        var fSuccess = function(result){
            this._deferDeleteBu.resolve(result);
        };
        
        var fError = function(err){
            this._deferDeleteBu.reject(err);
        };
        
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.deleteBusinessUnit(codiceBU, fSuccess, fError);
        
        return this._deferDeleteBu.promise;
    },
    
    addBu: function(nomeBu, descrizioneBU, statoBu){
        this._deferAddBu = Q.defer();
        
        var fSuccess = function(result){
            this._deferAddBu.resolve(result);
        };
        
        var fError = function(err){
            this._deferAddBu.reject(err);
        };
        
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.addBusinessUnit(nomeBu, descrizioneBU, statoBu, fSuccess, fError);
        
        return this._deferAddBu.promise;
    },
    
    updateBu: function(nomeBu, descrizioneBU, codiceBU, statoBu){
        this._deferUpdateBu = Q.defer();
        
        var fSuccess = function(result){
            this._deferUpdateBu.resolve(result);
        };
        
        var fError = function(err){
            this._deferUpdateBu.reject(err);
        }
        
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.updateBusinessUnit(codiceBU, nomeBu, descrizioneBU, statoBu, fSuccess, fError);
        
        return this._deferUpdateBu.promise;
    }
};
