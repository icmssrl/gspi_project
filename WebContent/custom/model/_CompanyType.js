jQuery.sap.declare("model._CompanyType");

model._CompanyType = {

    getCompany: function () {
        this._deferCompanyType = Q.defer();

        var fSuccess = function (result) {
            this._companyType = result;
            this._deferCompanyType.resolve(this._companyType);
        };

        var fError = function (err) {
            this._companyType = [];
            this._deferCompanyType.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        $.getJSON("custom/model/companyType.json")
            .success(fSuccess)
            .fail(fError);

        return this._deferCompanyType.promise;
    },
};
