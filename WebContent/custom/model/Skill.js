if (!model.persistence.Serializer)
    jQuery.sap.require("model.persistence.Serializer");
if (!model.odata)
    jQuery.sap.require("model.odata.chiamateOdata");

model.Skill = {
    // funzione che mi estrae tutti gli skill ti tutti i consulenti di uno staff
    getConsulenteSkillSerializedInBatch: function (batchCodConsulente) {
        this._deferConsulenteSkillsBatch = Q.defer();

        var fSuccess = function (result) {
            this.allSkillsForConsultants = model.persistence.Serializer.consulenteSkillNoD.fromSAPItems(result);
            this._deferConsulenteSkillsBatch.resolve(this.allSkillsForConsultants);
        };

        var fError = function (err) {
            this._deferConsulenteSkillsBatch.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getConsulenteSkillInBatch(batchCodConsulente, fSuccess, fError);
        return this._deferConsulenteSkillsBatch.promise;
    },

    // carica tutti gli skill (nel PJM serve per aggiungere un consulente generico)
    readSkills: function () {
        var defer = Q.defer();
        var that = this;
        var fSuccess = function (result) {
            this.arraySkills = model.persistence.Serializer.skills.fromSAPItems(result);
            defer.resolve(this.arraySkills);
        };

        var fError = function (err) {
            if (!this.oneTime) {
                this.oneTime = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(that.readSkills(), that));
            }
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readSkill(fSuccess, fError);

        return defer.promise;
    },

    deleteSkill: function (codSkill) {
        this._deferDeleteSkills = Q.defer();

        var fSuccess = function (result) {
            this._deferDeleteSkills.resolve(result);
        };

        var fError = function (err) {
            this._deferDeleteSkills.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteSkill(codSkill, fSuccess, fError);

        return this._deferDeleteSkills.promise;
    },

    addSkill: function (nomeSkill, descrizioneSkill) {
        this._deferAddSkills = Q.defer();

        var fSuccess = function (result) {
            this._deferAddSkills.resolve(result);
        };

        var fError = function (err) {
            this._deferAddSkills.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.addSkill(nomeSkill, descrizioneSkill, fSuccess, fError);

        return this._deferAddSkills.promise;
    },

    updateSkill: function (codSkill, nomeSkill, descrizioneSkill) {
        this._deferUpdateSkills = Q.defer();

        var fSuccess = function (result) {
            this._deferUpdateSkills.resolve(result);
        };

        var fError = function (err) {
            this._deferUpdateSkills.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateSkill(codSkill, nomeSkill, descrizioneSkill, fSuccess, fError);

        return this._deferUpdateSkills.promise;
    },

    getConsulenteSkill: function (codConsulente) {
        this._deferConsulenteSkill = Q.defer();

        var fSuccess = function (result) {
            this._deferConsulenteSkill.resolve(result);
        };

        var fError = function (err) {
            this._deferConsulenteSkill.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getConsulenteSkill(codConsulente, fSuccess, fError);

        return this._deferConsulenteSkill.promise;
    },

    addSkillToConsultant: function (codConsulente, codSkill, pricipale = false) {
        this._deferAddSkillToConsultant = Q.defer();

        var fSuccess = function (result) {
            this._deferAddSkillToConsultant.resolve(result);
        };

        var fError = function (err) {
            this._deferAddSkillToConsultant.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.addSkillToConsultant(codConsulente, codSkill, pricipale, fSuccess, fError);

        return this._deferAddSkillToConsultant.promise;
    },

    deleteSkillToConsultant: function (codConsulente, codSkill) {
        this._deferDeleteSkillToConsultant = Q.defer();

        var fSuccess = function (result) {
            this._deferDeleteSkillToConsultant.resolve(result);
        };

        var fError = function (err) {
            this._deferDeleteSkillToConsultant.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteSkillToConsultant(codConsulente, codSkill, fSuccess, fError);

        return this._deferDeleteSkillToConsultant.promise;
    },

    getConsulenteSkillSerialized: function (codConsulente) {
        this._deferConsulenteSkill = Q.defer();

        var fSuccess = function (result) {
            this.arraySkill = model.persistence.Serializer.consulenteSkillNoD.fromSAPItems(result);
            this._deferConsulenteSkill.resolve(this.arraySkill);
        };

        var fError = function (err) {
            this._deferConsulenteSkill.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getConsulenteSkill(codConsulente, fSuccess, fError);

        return this._deferConsulenteSkill.promise;

    },


};
