jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.Login = {
    
//    doLogin: function (username, pswCriptata, success, error) {
//        var defer = Q.defer();
//
//        var fSuccess = function (result) {
//            if (!result && !this.oneTime) {
//                this.oneTime = true;
//                model.odata.chiamateOdata.chiamataForLoginToHanaSimple()
//                    .then(_.bind(this.doLogin(), this));
//            };
//            this.arrayLogin = JSON.parse(result).results[0];  
//            if(!this.arrayLogin) {
//                defer.reject("credenzialiErrate");
//            } else {
//                defer.resolve(this.arrayLogin);
//            }
//        };
//
//        var fError = function (err) {
//            defer.reject(err);
//        };
//
//        fSuccess = _.bind(fSuccess, this);
//        fError = _.bind(fError, this);
//
//        model.odata.chiamateOdata.doLogin(username, pswCriptata, fSuccess, fError);
//        return defer.promise;
//    },
    
    /* LOGIN SAP - HANA */
    doLogin: function (cid, success, error) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (!result && !this.oneTime) {
                this.oneTime = true;
                model.odata.chiamateOdata.chiamataForLoginToHanaSimple()
                    .then(_.bind(this.doLoginSAP(), this));
            };
            this.arrayLogin = JSON.parse(result).results;  
            if(!this.arrayLogin) {
                defer.reject("credenzialiErrate");
            } else {
                defer.resolve(this.arrayLogin);
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.doLogin(cid, fSuccess, fError);
        return defer.promise;
    }
};
