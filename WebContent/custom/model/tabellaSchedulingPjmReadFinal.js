jQuery.sap.declare("model.tabellaSchedulingPjmReadFinal");

model.tabellaSchedulingPjmReadFinal = {
    // funzione che legge tutte le richieste di un progetto
    readFinalProjectRequests: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (result && result !== "") {
                defer.resolve(JSON.parse(result));
            } else {
                defer.resolve();
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readFinalProjectRequests(req, fSuccess, fError);
        return defer.promise;
    },
    
    
    readFinalAllRequests: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (result && result !== "") {
                defer.resolve(JSON.parse(result));
            } else {
                defer.resolve();
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readFinalAllRequests(req, fSuccess, fError);
        return defer.promise;
    },
    
    // funzione che legge tutte le richieste
    readAdminAllRequests: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (result && result !== "") {
                defer.resolve(JSON.parse(result));
            } else {
                defer.resolve();
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readAdminAllRequests(req, fSuccess, fError);
        return defer.promise;
    },
    
    // funzione che legge le richieste di un singolo consulente
    readAdminSingleConsultant: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (result && result !== "") {
                defer.resolve(JSON.parse(result));
            } else {
                defer.resolve();
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readAdminSingleConsultant(req, fSuccess, fError);
        return defer.promise;
    },
    
    deleteRequest: function (idReq, state, numero) {
        if (numero) {
            this.numeroRichiesteDaEliminare = numero;
        }
        this.numeroRichiesteDaEliminare--;

        var deferDeleteRequest = Q.defer();

        var fSuccess = function (ris) {
            // faccio ritornare alla funzione di successo solo se tutte le richieste di cambio stato sono andate a buon fine
            if (this.numeroRichiesteDaEliminare === 0) {
                deferDeleteRequest.resolve(ris);
            }
        };
        var fError = function (err) {
            deferDeleteRequest.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteRequestAdmin(idReq, state, fSuccess, fError);
        return deferDeleteRequest.promise;
    },
    
    
    
    
    
    // funzione per la cancellazione delle richieste in scrittura FINAL
    deleteRequestsFromFinal: function (periodo) {
        var deferDelete = Q.defer();

        var fSuccess = function (ris) {
            deferDelete.resolve(ris);
        };
        var fError = function (err) {
            deferDelete.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteSchedulingPeriodsMaria(periodo, fSuccess, fError);
        return deferDelete.promise;
    },
    
    // funzione per inserimento richieste in scrittura FINAL
    readRequests: function (periodo) {
        var deferGet = Q.defer();

        var fSuccess = function (ris) {
            deferGet.resolve(ris);
        };
        var fError = function (err) {
            deferGet.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readSchedulingPeriodsMaria(periodo, fSuccess, fError);
        return deferGet.promise;
    },
    
};
