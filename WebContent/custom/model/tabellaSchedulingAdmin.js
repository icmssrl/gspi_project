jQuery.sap.declare("model.tabellaSchedulingAdmin");

model.tabellaSchedulingAdmin = {
    // funzione che legge tutte le richieste di un progetto
    readAdminProjectRequests: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (result && result !== "") {
                defer.resolve(JSON.parse(result));
            } else {
                defer.resolve();
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readAdminProjectRequests(req, fSuccess, fError);
        return defer.promise;
    },
    
    // funzione che legge tutte le richieste
    readAdminAllRequests: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (result && result !== "") {
                defer.resolve(JSON.parse(result));
            } else {
                defer.resolve();
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readAdminAllRequests(req, fSuccess, fError);
        return defer.promise;
    },
    
    // funzione che legge le richieste di un singolo consulente
    readAdminSingleConsultant: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (result && result !== "") {
                defer.resolve(JSON.parse(result));
            } else {
                defer.resolve();
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readAdminSingleConsultant(req, fSuccess, fError);
        return defer.promise;
    },
    
    deleteRequest: function (idReq, state, numero) {
        if (numero) {
            this.numeroRichiesteDaEliminare = numero;
        }
        this.numeroRichiesteDaEliminare--;

        var deferDeleteRequest = Q.defer();

        var fSuccess = function (ris) {
            // faccio ritornare alla funzione di successo solo se tutte le richieste di cambio stato sono andate a buon fine
            if (this.numeroRichiesteDaEliminare === 0) {
                deferDeleteRequest.resolve(ris);
            }
        };
        var fError = function (err) {
            deferDeleteRequest.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteRequestAdmin(idReq, state, fSuccess, fError);
        return deferDeleteRequest.promise;
    },
    
    
    
    
    
    // funzione per la cancellazione delle richieste in scrittura FINAL
    deleteRequestsFromFinal: function (periodo) {
        var deferDelete = Q.defer();

        var fSuccess = function (ris) {
            deferDelete.resolve(ris);
        };
        var fError = function (err) {
            deferDelete.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteSchedulingPeriodsMaria(periodo, fSuccess, fError);
        return deferDelete.promise;
    },
    
    // funzione per inserimento richieste in scrittura FINAL
    readRequests: function (periodo) {
        var deferGet = Q.defer();

        var fSuccess = function (ris) {
            deferGet.resolve(ris);
        };
        var fError = function (err) {
            deferGet.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readSchedulingPeriodsMaria(periodo, fSuccess, fError);
        return deferGet.promise;
    },

    // funzione che inseriche le richieste di modifica in FINAL
    insertModifyInFinal: function (array) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.insertMultiInFinal(array, fSuccess, fError);
        return defer.promise;
    },
    
    // funzione per inserimento ferie batch su final e draft
    sendBatchFerieFinalDraft: function (batchReq) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.saveBatchFerieFinalDraft(batchReq, fSuccess, fError);
        return defer.promise;
    },

    // funzione per inserimento/update note
    updateNote: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updateNoteAdmin(req, fSuccess, fError);
        return defer.promise;
    },
    
    getProjectListWithGenericConsultantFinal: function (params) {
        var defer = Q.defer();
        
        var fSuccess = function (res) {
            defer.resolve(res);
        };
        
        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getProjectListWithGenericConsultantFinal(params, fSuccess, fError);
        return defer.promise;
    }
};
