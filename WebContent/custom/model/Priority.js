jQuery.sap.declare("model.Priority");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.Priority = (function () {

    Priority = function (data) {
        this.arrayPriority = [];

        this.getModel = function () {
            var model = new sap.ui.model.json.JSONModel();
            model.setData(this.arrayPriority);
            return model;
        };

        this.getPriority = function () {

            var defer = Q.defer();
            //utils.Busy.show();

            var fSuccess = function (result) {
                utils.Busy.hide();
                this.arrayPriority = model.persistence.Serializer.projectPriority.fromSAPItems(result);
                defer.resolve(this.arrayPriority);
            };
            var fError = function (err) {
                utils.Busy.hide();
                defer.reject(err);
            };
            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            model.odata.chiamateOdata.getPriorities(fSuccess, fError);
//            $.ajax({
//                type: "GET",
//                url: "http://gspidev.icms.it:8000/GSPI/odata/odata.xsodata/projectPrioritySet",
//                success: fSuccess,
//                error: fError,
//                dataType: "json",
//                contentType: "application/json"
//            });
            return defer.promise;
        };
        return this;
    };

    return Priority;


})();
