jQuery.sap.declare("model.reqToDel");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.odata.chiamateOdata");


model.reqToDel = {
    sendRequest: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.delEntity(req, fSuccess, fError);
        return defer.promise;
    },

    sendRequestToFinal: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.delEntityFerie(req, fSuccess, fError);
        return defer.promise;
    },

    delMulti: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.delMultiEntity(req, fSuccess, fError);
        return defer.promise;
    }
};
