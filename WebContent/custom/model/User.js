jQuery.sap.require("view.abstract.AbstractController");

model.User = (function () {

    User = function (data) {
        this.console = function (testo, css) {
            view.abstract.AbstractController.prototype.console.apply(this, arguments);
        };
        
        this.username = "";
        this.surname = "";
        this.password = "";
        this.fullName = "";
        this.mail = "";
        this.type = "";

        this.setCredential = function (uname, pwd, name, surname) {
            this.username = uname;
            this.password = pwd;
            this.name = name;
            this.surname = surname;
        };

        this.setCurrentUser = function (userInfo) {
            this.username = userInfo.username;
            this.surname = userInfo.surname;
            this.password = userInfo.password;
            this.type = userInfo.type;

        };
        this.getCurrentUser = function () {
            return this;
        };
        this.getUsername = function () {
            return this.username;
        };
        this.setUserName = function (value) {
            this.username = value;
        };
        this.setPassword = function (value) {
            this.password = value;
        };

        this.getNewCredential = function () {
            this.username = "";
            this.password = "";
            return this.getModel();
        };

        this.getModel = function () {
            var model = new sap.ui.model.json.JSONModel();
            var user = this.getCurrentUser();
            model.setData(user);
            return model;
        };

        this.doLogin = function () {
            var defer = Q.defer();
            var fSuccess = function (result) {
//                _.remove(result.results, {COD_PROFILO_CONSULENTE: 'CP10000001'});
                this.arrayConsultants = model.persistence.Serializer.consultants.fromSAPItems(result);
                var user = _.find(this.arrayConsultants.items, {
                    nomeConsulente: this.name,
                    cognomeConsulente: this.surname
                });
                if (!user) {
                    defer.reject("user not found");
                    sap.m.MessageToast.show("user not found");
                } else {
                    this.setCurrentUser(user);
                    this.console("user logged", "success");
                    this.console(user);

                    model.persistence.Storage.session.save("user", user);
                    defer.resolve(user);
                }
            };
            
            var fError = function (err) {
                defer.reject(err);
            };
            
            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            
            model.odata.chiamateOdata.getConsultants(fSuccess, fError);
            return defer.promise;
        };

        this.sessionSave = function () {
            var user = {
                username: this.username,
                type: this.type
            };
            model.persistence.Storage.session.save("userType", user);
        };

        if (data)
            this.update(data);
        return this;
    };
    return User;

})();
