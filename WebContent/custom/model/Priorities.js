if (!model.persistence.Serializer)
    jQuery.sap.require("model.persistence.Serializer");
if (!model.odata.chiamateOdata)
    jQuery.sap.require("model.odata.chiamateOdata");

model.Priorities = {
    // legge tutte le priorità se req è undefined
    readPriorities: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.arrayPriority = model.persistence.Serializer.projectPriority.fromSAPItems(result);
            defer.resolve(this.arrayPriority);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getPriorities(req, fSuccess, fError);
        return defer.promise;
    },
    
    deletePriority: function (codicePrioritaCommessa) {
        this._deferDeletePriority = Q.defer();

        var fSuccess = function (result) {
            this._deferDeletePriority.resolve(result);
        };

        var fError = function (err) {
            this._deferDeletePriority.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deletePriority(codicePrioritaCommessa, fSuccess, fError);

        return this._deferDeletePriority.promise;
    },

    addPriority: function (descrizionePrioritaCommessa) {
        this._deferAddPriority = Q.defer();

        var fSuccess = function (result) {
            this._deferAddPriority.resolve(result);
        };

        var fError = function (err) {
            this._deferAddPriority.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.addPriority(descrizionePrioritaCommessa, fSuccess, fError);

        return this._deferAddPriority.promise;
    },

    updatePriority: function (codicePrioritaCommessa, descrizionePrioritaCommessa) {
        this._deferUpdatePriority = Q.defer();

        var fSuccess = function (result) {
            this._deferUpdatePriority.resolve(result);
        };

        var fError = function (err) {
            this._deferUpdatePriority.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.updatePriority(codicePrioritaCommessa, descrizionePrioritaCommessa, fSuccess, fError);

        return this._deferUpdatePriority.promise;
    }
};
