jQuery.sap.declare("model.requestSaveToDraft");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.requestSaveToDraft = {
    sendRequest: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.saveEntityToDraft(req, fSuccess, fError);
        return defer.promise;
    },
    
    delRequest: function (reqId, state) {
        var deferDelRequest = Q.defer();

        var fSuccess = function (result) {
            deferDelRequest.resolve(result);
        };

        var fError = function (err) {
            deferDelRequest.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteEntityToDraft(reqId, state, fSuccess, fError);
        return deferDelRequest.promise;
    },
    
    delDefRequest: function (reqId, state) {
        var deferDelRequest = Q.defer();

        var fSuccess = function (result) {
            deferDelRequest.resolve(result);
        };

        var fError = function (err) {
            deferDelRequest.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteDefEntityToDraft(reqId, fSuccess, fError);
        return deferDelRequest.promise;
    }
};