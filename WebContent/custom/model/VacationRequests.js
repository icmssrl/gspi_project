if (!model.persistence)
    jQuery.sap.require("model.persistence.Serializer");
if (!model.odata)
    jQuery.sap.require("model.odata.chiamateOdata");

model.VacationRequests = {
    readVacationRequests: function (req) {
        var defer = Q.defer();
        var requestList = [];
        var fSuccess = function (result) {
            var requests = [];
            result = JSON.parse(result);
            var table = [];
            if (result && result.results) {
                var res3 = result.results;
                for (var i = 0; i < res3.length; i++) {
                    var obj = {};
                    obj.nomeConsulente = res3[i].nomeConsulente;
                    obj.codiceConsulente = res3[i].cod_consulente;
                    var propNote = "nota" + res3[i].gg;
                    obj[propNote] = res3[i].note;
                    obj.clickable = res3[i].gg;
                    if (res3[i].gg) {
                        var prop = "gg" + res3[i].gg;
                        var propIcon = "ggIcon" + res3[i].gg;
                        var propConflict = "ggConflict" + res3[i].gg;
                    }
                    if (res3[i].commessa) {
                        obj[prop] = res3[i].commessa;
                    }
                    if (res3[i].dataRichiesta) {
                        var propDataRic = "ggDataRic" + res3[i].gg;
                        obj[propDataRic] = res3[i].dataRichiesta;
                    }
                    /* verifico se ci sono delle giornate con conflitti e in caso metto un'icona di avvertimento */
                    var richiesteContemporanee = _.where(res3, {
                        'gg': res3[i].gg,
                        'cod_consulente': res3[i].cod_consulente
                    });
                    var mezzeRichiesteFerieContemporanee = _.where(richiesteContemporanee, {
                        'ggIcon': "bed"
                    });
                    var nRichiesteContemporanee = richiesteContemporanee.length;

                    if (res3[i].ggIcon) {
                        if (res3[i].ggIcon === "complete-holidays") {
                            obj[propIcon] = "sap-icon://travel-itinerary";
                        } else {
                            obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                        };
                    };
                    if (res3[i].codicePJM) {
                        obj.codicePJM = res3[i].codicePJM;
                    };
                    if (res3[i].toCheck) {
                        var prop = "toCheck" + res3[i].gg;
                        obj[prop] = res3[i].toCheck;
                    };
                    if (res3[i].id_req) {
                        var prop = "req" + res3[i].gg;
                        obj[prop] = res3[i].id_req;
                    };
                    if (res3[i].stato) {
                        var prop = "stato" + res3[i].gg;
                        obj[prop] = res3[i].stato;
                    };
                    //**
                    //                    if (res3[i].stato !== "DELETED")
                    table.push(obj);
                };
                var res = [];
                res.push(table);
                defer.resolve(res);
            };
        };

        var fError = function (err) {
            requestList = [];
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getRequestListVacation(req, fSuccess, fError);

        return defer.promise;
    },

    saveRequestVacation: function (requests) {
        var defer = Q.defer();
        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.saveRequestVacation(requests, fSuccess, fError);

        return defer.promise;
    },

    deleteVacationRequest: function (codConsulente, idCommessa, idPeriodoSchedulingRic, giornoPeriodo) {
        var defer = Q.defer();
        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteVacationRequest(codConsulente, idCommessa, idPeriodoSchedulingRic, giornoPeriodo, fSuccess, fError);

        return defer.promise;
    },

    checkVacation: function (codConsulente, idPeriodo) {
        var defer = Q.defer();
        var fSuccess = function (result) {
            if (result.results) {
                result.results = model.persistence.Serializer.requestsFerie.fromSAPItems(result).items;
            }
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getRequestVacation(codConsulente, idPeriodo, fSuccess, fError);

        return defer.promise;
    },
};
