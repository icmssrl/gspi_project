jQuery.sap.declare("model.persistence.SapSerializer");
jQuery.sap.require("utils.Util");

model.persistence.SapSerializer = {

    cidEntity: {
        fromSap: function (sap) {
            var p = {};
            p.userID = sap.Pernr;
            p.Aedtm = utils.Util.dateToString(sap.Aedtm);
            p.Uname = sap.Uname;
            p.Bukrs = sap.Bukrs;
            p.Werks = sap.Werks;
            p.Persg = sap.Persg;
            p.Persk = sap.Persk;
            p.Vdsk1 = sap.Vdsk1;
            p.Gsber = sap.Gsber;
            p.Btrtl = sap.Btrtl;
            p.Juper = sap.Juper;
            p.Abkrs = sap.Abkrs;
            p.Ansvh = sap.Ansvh;
            p.Kostl = sap.Kostl;
            p.Orgeh = sap.Orgeh;
            p.Plans = sap.Plans;
            p.Stell = sap.Stell;
            p.Mstbr = sap.Mstbr;
            p.rimborsoChilometrico = sap.MileagePd;
            p.Sacha = sap.Sacha;
            p.Sachp = sap.Sachp;
            p.Sachz = sap.Sachz;
            p.Sbmod = sap.Sbmod;
            p.cognome = sap.Nachn;
            p.nome = sap.Vorna;
            p.Sprsl = sap.Sprsl;
            p.password = sap.ZzftrPwd;
            p.alias = sap.ZzftrAlias;
            p.auto = sap.Kzpmf;
            p.ultimaSincronizzazione = sap.ZzftrSync;
            p.pjm = sap.Responsible;
            return p;
        },

        toSap: function (p) {
            var sap = {};
            sap.Pernr = p.userID;
            sap.ZzftrAlias = p.alias;
            sap.ZzftrPwd = p.password;
            sap.Pernr = p.userID
            sap.Nachn = p.cognome;
            sap.Vorna = p.nome;
            sap.ZzftrPwd = p.password;
            sap.Kzpmf = p.auto;
            return sap;
        }
    },
    
    projectEntity: {
        fromSapItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSap(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },
        
        fromSap: function (sap) {
            var p = {};
            p.userID = sap.Pernr;
            p.descrizioneProgetto = sap.ProjectDefinition;
            p.descrizione = sap.Description;
            p.disabilitato = sap.Disabled;
            p.commessa = sap.Pspnr;
            return p;
        },

        toSap: function (p) {
            var sap = {};

            return sap;
        }
    },
    
    commessaEntity: {
        fromSapItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSap(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },
        
        fromSap: function (sap) {
            var p = {};
            p.userID = sap.Pernr;
            p.elementoWBSID = sap.Pspnr;
            p.elementoWBS = sap.Posid;
            p.descrizione = sap.Post1;
            p.commessa = sap.Psphi;
            p.identificazioneWBS = sap.Poski;   
            p.disabilitato = sap.Disabled;
            return p;
        },

        toSap: function (p) {
            var sap = {};

            return sap;
        }
    },
    
    menuTendinaEntity: {
        fromSapItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSap(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },
        
        fromSap: function (sap) {
            var p = {};
            p.Fieldname = sap.Fieldname;
            p.Value = sap.Value;
            p.descrizione = sap.Txtlg;
            return p;
        },
    },

    attivitaEntity: {
        fromSap: function (sap) {
            var p = {};
            p.counter = sap.Counter;
            p.cid = sap.Pernr;
            p.dataCommessa = utils.Util.dateToString(sap.Workdate);
            p.centroCostoMittente = sap.Skostl;
            p.tipoAttivita = sap.Lstar;
            p.attivita = sap.Rproj;
            p.tipoPresenzaAssenza = sap.Awart;
            p.numeroTrasferta = sap.Reinr;
            p.controllinArea = sap.Kokrs;
            p.unitaMisuraVisualizzazione = sap.Meinh;
            p.dataCreazione = utils.Util.dateToString(sap.Ersda);
            p.oraAcquisizione = utils.Util.getHourFromMs(sap.Erstm);
            p.nomeUtenteAcquisizioneRecord = sap.Ernam;
            p.dataUltimaModifica = utils.Util.dateToString(sap.Laeda);
            p.durata = sap.Catshours;
            p.luogo = sap.Zzworkloc;
            p.tipoOrario = sap.ZzworkType;
            p.nonFatturabile = sap.Zzfatt;
            p.descrizione = sap.Ltxa1;
            p.note = sap.Longtext;
            p.commessa = sap.Psphi;
            p.stato = sap.Status;
            p.orarioInizio = utils.Util.getHourFromMs(sap.Beguz);
            p.orarioFine = utils.Util.getHourFromMs(sap.Enduz);
            p.counterFiori = sap.Zidfiori;
            p.delete = sap.Zdelete;
            p.oraUltimaModifica = utils.Util.getHourFromMs(sap.Laetm);
            p.nomeConsulente = sap.Ename;
            return p;
        },

//        fromSapItems: function (results) {
//            var r = {};
//            var items = [];
//            if (results !== undefined && results.length > 0) {
//                _.each(results, function (sap) {
//                    var c = model.odata.serializer.attivitaEntity.fromSap(sap);
//                    items.push(c);
//                });
//            }
//            if (items.length > 0) {
//                r.items = items;
//            } else {
//                r = undefined;
//            }
//            return r;
//        },
        
        toSap: function (p) {
            var sap = {};
            sap.Counter = p.counter;
            sap.Pernr = p.cid;
            sap.Workdate = utils.Util.setDateToSap(p.dataCommessa);
            sap.Rproj = p.attivita;
            sap.Zidfiori = p.counterFiori;
            sap.Zdelete = p.delete ? p.delete : "";
            sap.Catshours = p.durata;
            sap.Zzworkloc = p.luogo;
            sap.ZzworkType = p.tipoOrario;
            sap.Zzfatt = p.nonFatturabile;
            sap.Ltxa1 = p.descrizione;
            sap.Longtext = p.note;
            sap.Beguz = utils.Util.getHoursToSap(p.orarioInizio);
            sap.Enduz = utils.Util.getHoursToSap(p.orarioFine);
            sap.Status = p.stato;
            sap.Laetm = utils.Util.getHoursToSap(p.oraUltimaModifica);
            sap.Laeda = utils.Util.setDateToSap(p.dataUltimaModifica);
            return sap;
        }
    },
    
    consultantList : 
    {
        fromSapItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSap(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },
        
        fromSap : function(sapData)
        {
            var c = {};
            
            c.name = sapData.Vorna ? sapData.Vorna : "";
            c.surname = sapData.Nachn ? sapData.Nachn : "";
            c.cid = sapData.Pernr ? sapData.Pernr : "";
            
            return c;
        },
        toSap : function(){}
    }

};