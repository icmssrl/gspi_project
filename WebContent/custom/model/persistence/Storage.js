jQuery.sap.declare("model.persistence.Storage");
jQuery.sap.require("jquery.sap.storage");

// jQuery.sap.require("model.Serializer");

model.persistence.Storage =
{
  local : {
    save : function(property, value)
    {
      localStorage.setItem(property, JSON.stringify(value));
    },
    get : function(property)
    {
      var objString = localStorage.getItem(property);
      return JSON.parse(objString);
    },
    remove : function(property)
    {
      localStorage.removeItem(property);
    },
  },
  session:
  {
    save : function(property, value)
    {
      sessionStorage.setItem(property, JSON.stringify(value));
    },
    get : function(property)
    {
      var objString = sessionStorage.getItem(property);
      return JSON.parse(objString);
    },
    remove : function(property)
    {
      sessionStorage.removeItem(property);
    },
  }
}
