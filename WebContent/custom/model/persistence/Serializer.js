jQuery.sap.declare("model.persistence.Serializer");

model.persistence.Serializer = {

    tiles: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            if (results.d) {
                results = results.d;
            }
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.id = sapData.ID_TILE ? sapData.ID_TILE : "";
            c.icon = sapData.ICONA ? sapData.ICONA : "";
            c.titleOR = sapData.TITOLO ? sapData.TITOLO : "";
            c.url = sapData.URL ? sapData.URL : "";
            c.group = sapData.GRUPPO ? sapData.GRUPPO : "";
            c.order = sapData.ORDINE ? sapData.ORDINE : "";
            c.isVisible = sapData.VISIBILE ? (sapData.VISIBILE === "true" ? true : false) : "";
            c.admin = sapData.CP00000003 ? (sapData.CP00000003 === "X" ? true : false) : false;
            c.unitManager = sapData.CP00000007 ? (sapData.CP00000007 === "X" ? true : false) : false;
            c.projectManager = sapData.CP00000002 ? (sapData.CP00000002 === "X" ? true : false) : false;
            c.consultant = sapData.CP00000001 ? (sapData.CP00000001 === "X" ? true : false) : false;
            c.ufficioAmministrativo = sapData.CP00000008 ? (sapData.CP00000008 === "X" ? true : false) : false;
            return c;
        }
    },

    periods: {
        fromSAPItems: function (sapData) {
            var results = sapData.d;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },

        fromSAP: function (sapData) {
            var p = {};
            var d = "";
            var d1 = "";
            p.annoPeriodo = sapData.ANNO_PERIODO_SCHEDULAZIONE ? sapData.ANNO_PERIODO_SCHEDULAZIONE : "";
            p.mesePeriodo = sapData.MESE_PERIODO ? sapData.MESE_PERIODO : "";
            p.stato = sapData.STATO ? sapData.STATO : "";
            p.idPeriod = sapData.ID_PERIODO ? sapData.ID_PERIODO : "";

            if (sapData.DATA_INIZIO_PERIODO) {
                d = sapData.DATA_INIZIO_PERIODO;
                d1 = new Date(parseFloat(d.substring(d.indexOf("(") + 1, d.length - 2)));
                p.dataInizio = d1.getDate() + "/" + (d1.getMonth() + 1) + "/" + d1.getFullYear();
                p.startDate = d1; // Javascript date format
            } else {
                p.dataInizio = "";
            }

            if (sapData.DATA_FINE_PERIODO) {
                d = sapData.DATA_FINE_PERIODO;
                d1 = new Date(parseFloat(d.substring(d.indexOf("(") + 1, d.length - 2)));
                p.dataFine = d1.getDate() + "/" + (d1.getMonth() + 1) + "/" + d1.getFullYear();
                p.endDate = d1; // javascript date format
            } else {
                p.dataFine = "";
            }

            if (sapData.DATA_FINE_RICHIESTE) {
                d = sapData.DATA_FINE_RICHIESTE;
                d1 = new Date(parseFloat(d.substring(d.indexOf("(") + 1, d.length - 2)));
                p.dataFineRichieste = d1.getDate() + "/" + (d1.getMonth() + 1) + "/" + d1.getFullYear();
                p.reqEndDate = d1; // javascript date format
            } else {
                p.dataFineRichieste = "";
            }
            p.note = sapData.NOTE ? sapData.NOTE : "";
            p.rilascioPjm = sapData.RILASCIO_PJM;
            p.rilascioAll = sapData.RILASCIO_ALL;
            p.numSettimane = sapData.NUM_SETTIMANE ? sapData.NUM_SETTIMANE : "";
            p.descrizioneMesi = sapData.DESCRIZIONE_MESI ? sapData.DESCRIZIONE_MESI : "";
            return p;
        },

        fromOData: function (sapData) {
            var p = {};
            var d = "";
            var d1 = "";
            p.annoPeriodo = sapData.ANNO_PERIODO_SCHEDULAZIONE ? sapData.ANNO_PERIODO_SCHEDULAZIONE : "";
            p.mesePeriodo = sapData.MESE_PERIODO ? sapData.MESE_PERIODO : "";
            p.stato = sapData.STATO ? sapData.STATO : "";
            p.idPeriod = sapData.ID_PERIODO ? sapData.ID_PERIODO : "";
            p.note = sapData.NOTE ? sapData.NOTE : "";
            p.descrizioneMesi = sapData.DESCRIZIONE_MESI ? sapData.DESCRIZIONE_MESI : "";

            if (sapData.DATA_INIZIO_PERIODO) {
                d = sapData.DATA_INIZIO_PERIODO;
                p.dataInizio = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();
                p.startDate = d; // Javascript date format
            } else {
                p.dataInizio = "";
            };

            if (sapData.DATA_FINE_PERIODO) {
                d = sapData.DATA_FINE_PERIODO;
                p.dataFine = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();
                p.endDate = d; // javascript date format
            } else {
                p.dataFine = "";
            };

            if (sapData.DATA_FINE_RICHIESTE) {
                d = sapData.DATA_FINE_RICHIESTE;
                p.dataFineRichieste = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();
                p.reqEndDate = d; // javascript date format
            } else {
                p.dataFineRichieste = "";
            };
            return p;
        },

        toOData: function (p) {
            var o = {};
            o.ANNO_PERIODO_SCHEDULAZIONE = p.annoPeriodo;
            o.MESE_PERIODO = p.mesePeriodo;
            o.STATO = p.stato;
            o.ID_PERIODO = p.idPeriod;
            o.DATA_INIZIO_PERIODO = p.startDate;
            o.DATA_FINE_PERIODO = p.endDate;
            o.DATA_FINE_RICHIESTE = p.reqEndDate;
            return o;
        }
    },

    userLogin: {
        fromSAPItems: function (sapData) {
            var ret = {
                items: []
            };
            if (sapData.hasOwnProperty("results")) {
                var l = sapData.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(sapData.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            } else {
                var oItem = this.fromSAP(sapData);
                if (oItem !== undefined) {
                    ret.items.push(oItem);
                };
            }
            return ret;
        },

        fromSAP: function (sapData) {
            var r = {};
            r.alias = sapData.ALIAS_LOGIN ? decodeURI(sapData.ALIAS_LOGIN) : "";
            r.nomeConsulente = sapData.NOME_CONSULENTE_LOGIN ? decodeURI(sapData.NOME_CONSULENTE_LOGIN) : "";
            r.cognomeConsulente = sapData.COGNOME_CONSULENTE_LOGIN ? decodeURI(sapData.COGNOME_CONSULENTE_LOGIN) : "";
            r.password = sapData.PASSWORD_LOGIN ? sapData.PASSWORD_LOGIN : "";
            r.stato = sapData.STATO_USER_LOGIN ? sapData.STATO_USER_LOGIN : "";
            return r;
        }
    },

    credentialsLogin: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var p = {};
            p.usernameConsulente = sapData.ALIAS_LOGIN ? sapData.ALIAS_LOGIN : "";
            p.passwordConsulente = sapData.PASSWORD_LOGIN ? sapData.PASSWORD_LOGIN : "";
            p.nomeConsulente = sapData.NOME_CONSULENTE_LOGIN ? sapData.NOME_CONSULENTE_LOGIN : "";
            p.cognomeConsulente = sapData.COGNOME_CONSULENTE_LOGIN ? sapData.COGNOME_CONSULENTE_LOGIN : "";
            p.statoConsulente = sapData.STATO_USER_LOGIN ? sapData.STATO_USER_LOGIN : "";
            return p;
        }
    },

    projectPriority: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var p = {};
            p.codPrioritaCommessa = sapData.COD_PRIORITA_COMMESSA ? sapData.COD_PRIORITA_COMMESSA : "";
            p.descrPriorita = sapData.DESCRIZIONE_PRIORITA_COMMESSA ? sapData.DESCRIZIONE_PRIORITA_COMMESSA : "";
            return p;
        }
    },

    requestType: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var p = {};
            p.codiceTipoRichiesta = sapData.COD_RICHIESTA ? sapData.COD_RICHIESTA : "";
            p.descrizioneTipoRichiesta = sapData.DESCRIZIONE_RICHIESTA ? sapData.DESCRIZIONE_RICHIESTA : "";
            return p;
        }
    },

    projects: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.codCommessa = sapData.COD_COMMESSA ? sapData.COD_COMMESSA : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.descrizioneCommessa = sapData.DESCRIZIONE_COMMESSA ? decodeURI(sapData.DESCRIZIONE_COMMESSA) : "";
            c.codPjmAssociato = sapData.COD_PJM_AC ? sapData.COD_PJM_AC : "";
            c.codiceStaffCommessa = sapData.COD_STAFF_COMMESSA ? sapData.COD_STAFF_COMMESSA : "";
            c.statoCommessa = sapData.STATO ? sapData.STATO : "";
            c.nomeConsulente = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
            c.cognomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            c.codPrioritaA = sapData.COD_PRIORITA_1 ? parseInt(sapData.COD_PRIORITA_1) : 0;
            c.codPrioritaB = sapData.COD_PRIORITA_2 ? parseInt(sapData.COD_PRIORITA_2) : 0;
            c.trasferteEstere = sapData.TRASFERTE_ESTERE;
            c.idPeriodoSched = sapData.ID_PERIODO_SCHEDULING_PROF;
            c.cod_bu = sapData.CODICE_BU;
            c.descrizione_bu = sapData.NOME_BU ? (sapData.NOME_BU + " - " + decodeURI(sapData.DESCRIZIONE_BU)) : "";
            c.descrizionePrioritaA = sapData.DESCRIZIONE_PRIORITA_COMMESSA_1 ? sapData.DESCRIZIONE_PRIORITA_COMMESSA_1 : "";
            c.descrizionePrioritaB = sapData.DESCRIZIONE_PRIORITA_COMMESSA_2 ? sapData.DESCRIZIONE_PRIORITA_COMMESSA_2 : "";
            return c;
        }
    },

    commessePerStaffDelPjm: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.codCommessa = sapData.COD_COMMESSA ? sapData.COD_COMMESSA : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.codPjmAssociato = sapData.COD_PJM_STAFF ? sapData.COD_PJM_STAFF : "";
            c.codConsulente = sapData.COD_CONSULENTE_STAFF ? sapData.COD_CONSULENTE_STAFF : "";
            c.stato = sapData.STATO ? sapData.STATO : "";
            c.teamLeader = sapData.TEAM_LEADER ? sapData.TEAM_LEADER : "";
            c.codSkill = sapData.COD_SKILL ? sapData.COD_SKILL : "";
            c.nomeSkill = sapData.NOME_SKILL ? sapData.NOME_SKILL : "";
            return c;
        }
    },

    commessePerStaffDelUm: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.codCommessa = sapData.COD_COMMESSA ? sapData.COD_COMMESSA : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.codPjmAssociato = sapData.COD_PJM_STAFF ? sapData.COD_PJM_STAFF : "";
            c.codConsulente = sapData.COD_CONSULENTE_STAFF ? sapData.COD_CONSULENTE_STAFF : "";
            c.stato = sapData.STATO ? sapData.STATO : "";
            c.codBu = sapData.CODICE_BU ? sapData.CODICE_BU : "";
            c.nomeSkill = sapData.NOME_SKILL ? sapData.NOME_SKILL : "";
            c.teamLeader = sapData.TEAM_LEADER ? sapData.TEAM_LEADER : "";
            c.codStaff = sapData.COD_STAFF ? sapData.COD_STAFF : "";
            return c;
        }
    },

    consultants: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            if (results.d) {
                results = results.d;
            }
            if (results.hasOwnProperty("results")) {
                results = results.results;
            }
            var ret = {
                items: []
            };
            if (results.length > 0) {
                var l = results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.codConsulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            c.codConsulenteBu = sapData.COD_CONSULENTE_BU ? sapData.COD_CONSULENTE_BU : "";
            c.codProfiloConsulente = sapData.COD_PROFILO_CONSULENTE ? sapData.COD_PROFILO_CONSULENTE : "";
            c.nomeConsulente = sapData.NOME_CONSULENTE ? decodeURI(decodeURI(sapData.NOME_CONSULENTE)) : "";
            c.cognomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(decodeURI(sapData.COGNOME_CONSULENTE)) : "";
            c.descBu = sapData.DESC_BU ? sapData.DESC_BU : "";
            c.note = sapData.NOTE ? decodeURI(decodeURI(sapData.NOTE)) : "";
            c.skill = sapData.SKILL ? sapData.SKILL : "";
            c.stato = sapData.STATO ? sapData.STATO : "";
            c.tipoAzienda = sapData.TIPO_AZIENDA ? sapData.TIPO_AZIENDA : "";
            c.descrizioneProfiloConsulente = sapData.DESCRIZIONE_PROFILO ? sapData.DESCRIZIONE_PROFILO : "";
            c.nomeCognome = c.cognomeConsulente + " " + c.nomeConsulente;
            c.ggIntercompany = sapData.GIORNATE_INTERCOMPANY ? sapData.GIORNATE_INTERCOMPANY : "";
            c.tariffaIntercompany = sapData.TARIFFA_INTERCOMPANY ? sapData.TARIFFA_INTERCOMPANY : "";
            c.intercompany = sapData.INTERCOMPANY ? sapData.INTERCOMPANY : "";
            c.partTime = sapData.PART_TIME ? sapData.PART_TIME : "";
            c.orePartTime = sapData.ORE_PART_TIME ? sapData.ORE_PART_TIME : "";
            c.cidSap = sapData.CID_CONSULENTE_SAP ? sapData.CID_CONSULENTE_SAP : "";
            c.codSkill = sapData.COD_SKILL ? sapData.COD_SKILL : "";
            c.nomeSkill = sapData.NOME_SKILL ? sapData.NOME_SKILL : "";
            c.skills = [];

            if (sapData.hasOwnProperty('skills')) {
                for (var i = 0; i < sapData.skills.length; i++) {
                    var skill = {
                        codSkill: sapData.skills[i].COD_SKILL,
                        nomeSkill: sapData.skills[i].NOME_SKILL,
                        descSkill: sapData.skills[i].DESC_SKILL
                    }
                    c.skills.push(skill);
                }
            }
            return c;
        }

    },

    simpleConsultant: {

        fromSAP: function (sapData) {
            var c = {};
            c.codConsulente = sapData.COD_CONSULENTE_STAFF ? sapData.COD_CONSULENTE_STAFF : "";
            c.codConsulenteBu = sapData.COD_CONSULENTE_BU ? sapData.COD_CONSULENTE_BU : "";
            c.codPJM = sapData.COD_PJM_AC ? sapData.COD_PJM_AC : "";
            c.nomeConsulente = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
            c.cognomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            c.descBu = sapData.DESC_BU ? sapData.DESC_BU : "";
            c.nomeCognome = c.cognomeConsulente + " " + c.nomeConsulente;
            return c;
        }

    },

    schedulingTable: {

        fromSAP: function (sapData) {
            var res = {};
            res.codAttività = sapData.COD_ATTIVITA_RICHIESTA ? sapData.COD_ATTIVITA_RICHIESTA : "";
            res.codicePJM = sapData.COD_PJM ? sapData.COD_PJM : "";
            res.codiceConsulente = sapData.COD_CONSULENTE_1 ? sapData.COD_CONSULENTE_1 : "";
            res.codiceCommessa = sapData.ID_COMMESSA ? sapData.ID_COMMESSA : "";
            res.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            res.nomeC = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
            res.cognomeC = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            res.nomeConsulente = res.nomeC + " " + res.cognomeC;
            res.dataInserimento = sapData.DATA_INSERIMENTO ? sapData.DATA_INSERIMENTO : "";
            res.dataReqPeriodo = sapData.DATA_REQ_PERIODO ? sapData.DATA_REQ_PERIODO : "";
            res.giornoReq = sapData.GIORNO_PERIODO ? parseInt(sapData.GIORNO_PERIODO) : "";

            return res;
        },

        toSAP: function (o) {

        }

    },

    profiloCommessaPeriodo: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            if (results.d) {
                results = results.d;
            };
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.codCommessaPeriodo = sapData.COD_COMMESSA_PERIODO ? sapData.COD_COMMESSA_PERIODO : "";
            c.idPeriodoSchedulingProf = sapData.ID_PERIODO_SCHEDULING_PROF ? sapData.ID_PERIODO_SCHEDULING_PROF : "";
            c.trasferteEstere = sapData.TRASFERTE_ESTERE ? sapData.TRASFERTE_ESTERE : "";
            c.codStaffCommessaPeriodo = sapData.COD_STAFF_COMMESSA_PERIODO ? sapData.COD_STAFF_COMMESSA_PERIODO : "";
            c.stato = sapData.STATO ? sapData.STATO : "";
            c.codPrioritaA = sapData.COD_PRIORITA_1 ? sapData.COD_PRIORITA_1 : "";
            c.codPrioritaB = sapData.COD_PRIORITA_2 ? sapData.COD_PRIORITA_2 : "";

            return c;
        }
    },

    getBuList: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            if (results.d) {
                results = results.d;
            };
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var bu = {};
            bu.codiceBu = sapData.COD_BU ? sapData.COD_BU : "";
            bu.nomeBu = sapData.NOME_BU ? sapData.NOME_BU : "";
            bu.descrizioneBu = sapData.DESCRIZIONE ? decodeURI(sapData.DESCRIZIONE) : "";
            bu.statoBu = sapData.STATO ? sapData.STATO : "";
            return bu;
        },

        toSAP: function (bu) { }
    },

    bu: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            if (results.d) {
                results = results.d;
            };
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.codBu = sapData.COD_BU ? sapData.COD_BU : "";
            c.nomeBu = sapData.NOME_BU ? sapData.NOME_BU : "";
            return c;
        }

    },

    getStaffView: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};

            c.nomeConsulente = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
            c.cognomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            c.codStaff = sapData.COD_STAFF ? sapData.COD_STAFF : "";
            c.codConsulenteStaff = sapData.COD_CONSULENTE_STAFF ? sapData.COD_CONSULENTE_STAFF : "";
            c.codCommessa = sapData.COD_COMMESSA ? sapData.COD_COMMESSA : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.descrizioneCommessa = sapData.DESCRIZIONE_COMMESSA ? decodeURI(sapData.DESCRIZIONE_COMMESSA) : "";
            return c;
        }
    },

    readStaffSingolaCommesseView: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.codConsulenteStaff = sapData.COD_CONSULENTE_STAFF ? sapData.COD_CONSULENTE_STAFF : "";
            c.codCommessa = sapData.COD_COMMESSA ? sapData.COD_COMMESSA : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.descrizioneCommessa = sapData.DESCRIZIONE_COMMESSA ? decodeURI(sapData.DESCRIZIONE_COMMESSA) : "";
            c.codStaff = sapData.COD_STAFF ? sapData.COD_STAFF : "";
            return c;
        }
    },

    readStaffCommesseView: {
        fromSAPItems: function (sapData) {
            var results = JSON.parse(sapData);
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

    },

    readStaffCompleteView: {
        fromSAPItems: function (sapData) {
            var results = sapData.d;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            if (sapData.COD_CONSULENTE_STAFF.indexOf("G") !== -1) {
                c.nomeConsulente = "Generico";
                c.cognomeConsulente = "Consulente";
            } else {
                c.nomeConsulente = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
                c.cognomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            }
            c.idStaff = sapData.ID_STAFF ? sapData.ID_STAFF : "";
            c.codStaff = sapData.COD_STAFF ? sapData.COD_STAFF : "";
            c.codConsulenteStaff = sapData.COD_CONSULENTE_STAFF ? sapData.COD_CONSULENTE_STAFF : "";
            c.codCommessa = sapData.COD_COMMESSA ? sapData.COD_COMMESSA : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.descrizioneCommessa = sapData.DESCRIZIONE_COMMESSA ? decodeURI(sapData.DESCRIZIONE_COMMESSA) : "";
            c.teamLeader = sapData.TEAM_LEADER ? (sapData.TEAM_LEADER === 'NO' ? false : true) : "";
            c.codSkill = sapData.COD_SKILL ? sapData.COD_SKILL : "";
            c.nomeSkill = sapData.NOME_SKILL ? sapData.NOME_SKILL : "";
            c.descSkill = sapData.DESC_SKILL ? sapData.DESC_SKILL : "";
            c.bu = sapData.COD_CONSULENTE_BU ? sapData.COD_CONSULENTE_BU : "";
            c.descBu = sapData.DESC_BU ? sapData.DESC_BU : "";
            c.stato = sapData.STATO ? sapData.STATO : "";

            return c;
        }
    },

    readNoStaffProject: {
        fromSAPItems: function (sapData) {
            var results = sapData.d;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.codCommessa = sapData.COD_COMMESSA ? sapData.COD_COMMESSA : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.descrizioneCommessa = sapData.DESCRIZIONE_COMMESSA ? decodeURI(sapData.DESCRIZIONE_COMMESSA) : "";
            c.codPjmAc = sapData.COD_PJM_AC ? sapData.COD_PJM_AC : "";
            c.nomePjm = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
            c.cognomePjm = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            c.codStaffCommessa = sapData.COD_STAFF_COMMESSA ? sapData.COD_STAFF_COMMESSA : "";
            c.stato = sapData.STATO ? sapData.STATO : "";
            c.codiceBu = sapData.CODICE_BU ? sapData.CODICE_BU : "";
            c.nomeBu = sapData.NOME_BU ? sapData.NOME_BU : "";
            return c;
        }
    },

    genericConsultants: {
        fromSAPItems: function (sapData) {
            var results = sapData.d;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.codConsulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            c.codConsulenteBu = sapData.COD_CONSULENTE_BU ? sapData.COD_CONSULENTE_BU : "";
            c.codProfiloConsulente = sapData.COD_PROFILO_CONSULENTE ? sapData.COD_PROFILO_CONSULENTE : "";
            c.descBu = sapData.DESC_BU ? sapData.DESC_BU : "";
            c.note = sapData.NOTE ? sapData.NOTE : "";
            c.stato = sapData.STATO ? sapData.STATO : "";
            c.tipoAzienda = sapData.TIPO_AZIENDA ? sapData.TIPO_AZIENDA : "";
            c.skill = sapData.SKILL ? sapData.SKILL : "";
            return c;
        }

    },

    requests: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.ggIcon = sapData.COD_ATTIVITA_RICHIESTA ? sapData.COD_ATTIVITA_RICHIESTA : "";
            c.cod_commessa = sapData.ID_COMMESSA ? sapData.ID_COMMESSA : "";
            c.cod_consulente = sapData.COD_CONSULENTE_STAFF ? sapData.COD_CONSULENTE_STAFF : "";
            c.codicePJM = sapData.COD_PJM ? sapData.COD_PJM : "";
            c.nomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) + " " + sapData.NOME_CONSULENTE : "";
            c.dataInserimento = sapData.DATA_INSERIMENTO ? sapData.DATA_INSERIMENTO : "";
            c.dataReqPeriodo = sapData.DATA_REQ_PERIODO ? sapData.DATA_REQ_PERIODO : "";
            c.gg = sapData.GIORNO_PERIODO ? sapData.GIORNO_PERIODO : "";
            c.idCommessa = sapData.ID_COMMESSA ? sapData.ID_COMMESSA : "";
            c.idPeriodoScheduling = sapData.ID_PERIODO_SCHEDULING_RIC ? sapData.ID_PERIODO_SCHEDULING_RIC : "";
            c.id_req = sapData.ID_REQ ? sapData.ID_REQ : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.note = sapData.NOTE ? sapData.NOTE : "";
            c.stato = sapData.STATO_REQ ? sapData.STATO_REQ : "";
            c.idStaff = sapData.COD_STAFF ? sapData.COD_STAFF : "";
            c.skill = sapData.NOME_SKILL ? sapData.NOME_SKILL + " " + sapData.DESC_SKILL : "";
            return c;
        }
    },

    requestsAll: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.ggIcon = sapData.COD_ATTIVITA_RICHIESTA ? sapData.COD_ATTIVITA_RICHIESTA : "";
            c.cod_commessa = sapData.COD_COMMESSA ? sapData.COD_COMMESSA : "";
            c.cod_consulente = sapData.COD_CONSULENTE_STAFF ? sapData.COD_CONSULENTE_STAFF : "";
            c.codicePJM = sapData.COD_PJM_STAFF ? sapData.COD_PJM_STAFF : "";
            //            c.codicePJM = sapData.COD_PJM ? sapData.COD_PJM : "";
            c.nomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) + " " + sapData.NOME_CONSULENTE : "";
            c.dataInserimento = sapData.DATA_INSERIMENTO ? sapData.DATA_INSERIMENTO : "";
            c.dataReqPeriodo = sapData.DATA_REQ_PERIODO ? sapData.DATA_REQ_PERIODO : "";
            c.gg = sapData.GIORNO_PERIODO ? sapData.GIORNO_PERIODO : "";
            c.idCommessa = sapData.ID_COMMESSA ? sapData.ID_COMMESSA : "";
            c.idPeriodoScheduling = sapData.ID_PERIODO_SCHEDULING_RIC ? sapData.ID_PERIODO_SCHEDULING_RIC : "";
            c.id_req = sapData.ID_REQ ? sapData.ID_REQ : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.note = sapData.NOTE ? sapData.NOTE : "";
            c.stato = sapData.STATO_REQ ? sapData.STATO_REQ : "";
            c.skill = sapData.NOME_SKILL ? sapData.NOME_SKILL + " " + sapData.DESC_SKILL : "";
            c.idStaff = sapData.COD_STAFF ? sapData.COD_STAFF : "";
            return c;
        }
    },

    requestsSingleConsultant: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.gg = sapData.GIORNO_PERIODO ? sapData.GIORNO_PERIODO : "";
            c.cod_consulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            c.dataReqPeriodo = sapData.DATA_REQ_PERIODO ? sapData.DATA_REQ_PERIODO : "";
            c.cod_commessa = sapData.ID_COMMESSA ? sapData.ID_COMMESSA : "";
            c.ggIcon = sapData.COD_ATTIVITA_RICHIESTA ? sapData.COD_ATTIVITA_RICHIESTA : "";
            c.idPeriodoScheduling = sapData.ID_PERIODO_SCHEDULING_RIC ? sapData.ID_PERIODO_SCHEDULING_RIC : "";
            c.codicePJM = sapData.COD_PJM ? sapData.COD_PJM : "";
            c.dataInserimento = sapData.DATA_INSERIMENTO ? sapData.DATA_INSERIMENTO : "";
            c.note = sapData.NOTE ? sapData.NOTE : "";
            c.stato = sapData.STATO_REQ ? sapData.STATO_REQ : "";
            c.id_req = sapData.ID_REQ ? sapData.ID_REQ : "";
            return c;
        }
    },

    requestsDraft: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.ggIcon = sapData.COD_ATTIVITA_RICHIESTA_DRAFT ? sapData.COD_ATTIVITA_RICHIESTA_DRAFT : "";
            c.cod_commessa = sapData.COD_COMMESSA ? sapData.COD_COMMESSA : "";
            c.cod_consulente = sapData.COD_CONSULENTE_DRAFT ? sapData.COD_CONSULENTE_DRAFT : "";
            c.cod_consulenteBu = sapData.COD_CONSULENTE_BU ? sapData.COD_CONSULENTE_BU : "";
            c.codicePJM = sapData.COD_PJM_DRAFT ? sapData.COD_PJM_DRAFT : "";
            c.nomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) + " " + sapData.NOME_CONSULENTE : "";
            c.dataInserimento = sapData.DATA_INSERIMENTO_DRAFT ? sapData.DATA_INSERIMENTO_DRAFT : "";
            c.dataReqPeriodo = sapData.DATA_REQ_PERIODO_DRAFT ? sapData.DATA_REQ_PERIODO_DRAFT : "";
            c.gg = sapData.GIORNO_PERIODO_DRAFT ? sapData.GIORNO_PERIODO_DRAFT : "";
            c.idCommessa = sapData.ID_COMMESSA_DRAFT ? sapData.ID_COMMESSA_DRAFT : "";
            c.idPeriodoScheduling = sapData.ID_PERIODO_SCHEDULING_DRAFT ? sapData.ID_PERIODO_SCHEDULING_DRAFT : "";
            c.id_req = sapData.ID_REQ_DRAFT ? sapData.ID_REQ_DRAFT : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.note = sapData.NOTE_DRAFT ? sapData.NOTE_DRAFT : "";
            c.stato = sapData.STATO_REQ_DRAFT ? sapData.STATO_REQ_DRAFT : "";
            c.skill = sapData.NOME_SKILL ? sapData.NOME_SKILL + " " + sapData.DESC_SKILL : "";
            c.nomeSkill = sapData.NOME_SKILL ? sapData.NOME_SKILL : "";
            c.idReqOld = sapData.ID_REQ_DRAFT_OLD ? sapData.ID_REQ_DRAFT_OLD : "";
            return c;
        }
    },

    requestsDraftAllConsultants: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.ggIcon = sapData.COD_ATTIVITA_RICHIESTA_DRAFT ? sapData.COD_ATTIVITA_RICHIESTA_DRAFT : "";
            c.cod_commessa = sapData.COD_COMMESSA ? sapData.COD_COMMESSA : "";
            c.cod_consulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            c.cod_consulenteBu = sapData.COD_CONSULENTE_BU ? sapData.COD_CONSULENTE_BU : "";
            c.descrizioneBu = sapData.DESC_BU ? sapData.DESC_BU : "";
            c.codicePJM = sapData.COD_PJM_DRAFT ? sapData.COD_PJM_DRAFT : "";
            c.nomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) + " " + sapData.NOME_CONSULENTE : "";
            c.dataInserimento = sapData.DATA_INSERIMENTO_DRAFT ? sapData.DATA_INSERIMENTO_DRAFT : "";
            c.dataReqPeriodo = sapData.DATA_REQ_PERIODO_DRAFT ? sapData.DATA_REQ_PERIODO_DRAFT : "";
            c.gg = sapData.GIORNO_PERIODO_DRAFT ? sapData.GIORNO_PERIODO_DRAFT : "";
            c.idCommessa = sapData.ID_COMMESSA_DRAFT ? sapData.ID_COMMESSA_DRAFT : "";
            c.idPeriodoScheduling = sapData.ID_PERIODO_SCHEDULING_DRAFT ? sapData.ID_PERIODO_SCHEDULING_DRAFT : "";
            c.id_req = sapData.ID_REQ_DRAFT ? sapData.ID_REQ_DRAFT : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.note = sapData.NOTE_DRAFT ? sapData.NOTE_DRAFT : "";
            c.stato = sapData.STATO_REQ_DRAFT ? sapData.STATO_REQ_DRAFT : "";
            c.codBU = sapData.CODICE_BU ? sapData.CODICE_BU : "";
            return c;
        }
    },

    requestsFinal: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.ggIcon = sapData.COD_ATTIVITA_RICHIESTA_FINAL_FINAL ? sapData.COD_ATTIVITA_RICHIESTA_FINAL_FINAL : "";
            c.cod_commessa = sapData.COD_COMMESSA ? sapData.COD_COMMESSA : "";
            c.cod_consulente = sapData.COD_CONSULENTE_FINAL ? sapData.COD_CONSULENTE_FINAL : "";
            c.codicePJM = sapData.COD_PJM_FINAL ? sapData.COD_PJM_FINAL : "";
            c.nomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) + " " + sapData.NOME_CONSULENTE : "";
            c.dataInserimento = sapData.DATA_INSERIMENTO_FINAL ? sapData.DATA_INSERIMENTO_FINAL : "";
            c.dataReqPeriodo = sapData.DATA_REQ_PERIODO_FINAL ? sapData.DATA_REQ_PERIODO_FINAL : "";
            c.gg = sapData.GIORNO_PERIODO_FINAL ? sapData.GIORNO_PERIODO_FINAL : "";
            c.idCommessa = sapData.ID_COMMESSA_FINAL ? sapData.ID_COMMESSA_FINAL : "";
            c.idPeriodoScheduling = sapData.ID_PERIODO_SCHEDULING_FINAL ? sapData.ID_PERIODO_SCHEDULING_FINAL : "";
            c.id_req = sapData.ID_REQ_FINAL ? sapData.ID_REQ_FINAL : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.note = sapData.NOTE_FINAL ? sapData.NOTE_FINAL : "";
            c.stato = sapData.STATO_REQ_FINAL ? sapData.STATO_REQ_FINAL : "";
            c.idCommessaSap = sapData.ID_COMMESSA_SAP ? sapData.ID_COMMESSA_SAP : "";
            c.codElemWbs = sapData.COD_ELEM_WBS ? sapData.COD_ELEM_WBS : "";
            return c;
        }
    },

    requestsModificheDraft: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.idReq = sapData.ID_REQ_MODIFICA ? sapData.ID_REQ_MODIFICA : "";
            c.idScambio = sapData.ID_SCAMBIO ? sapData.ID_SCAMBIO : "";
            c.gg = sapData.GIORNO_PERIODO_MODIFICA ? sapData.GIORNO_PERIODO_MODIFICA : "";
            c.noteModifica = sapData.NOTE_MODIFICA ? sapData.NOTE_MODIFICA : "";
            c.statoModifica = sapData.STATO_REQ_MODIFICA ? sapData.STATO_REQ_MODIFICA : "";
            c.dataInserimento = sapData.DATA_INSERIMENTO_MODIFICA ? (sapData.DATA_INSERIMENTO_MODIFICA).getFullYear() + "-" + ((sapData.DATA_INSERIMENTO_MODIFICA).getMonth() + 1) + "-" + (sapData.DATA_INSERIMENTO_MODIFICA).getDate() : "";
            c.codPjm = sapData.COD_PJM_MODIFICA ? sapData.COD_PJM_MODIFICA : "";
            c.idPeriodo = sapData.ID_PERIODO_SCHEDULING_RIC_MODIFICA ? sapData.ID_PERIODO_SCHEDULING_RIC_MODIFICA : "";
            c.codAttivita = sapData.COD_ATTIVITA_RICHIESTA_MODIFICA ? sapData.COD_ATTIVITA_RICHIESTA_MODIFICA : "";
            c.dataReq = sapData.DATA_REQ_PERIODO_MODIFICA ? (sapData.DATA_REQ_PERIODO_MODIFICA).getFullYear() + "-" + ((sapData.DATA_REQ_PERIODO_MODIFICA).getMonth() + 1) + "-" + (sapData.DATA_REQ_PERIODO_MODIFICA).getDate() : "";
            c.codConsulente = sapData.COD_CONSULENTE_MODIFICA ? sapData.COD_CONSULENTE_MODIFICA : "";
            c.codCommessa = sapData.COD_COMMESSA_MODIFICA ? sapData.COD_COMMESSA_MODIFICA : "";
            c.codCommessaOld = sapData.COD_COMMESSA_OLD_MODIFICA ? sapData.COD_COMMESSA_OLD_MODIFICA : "";
            return c;
        }
    },

    requestsFerie: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.id_req = sapData.ID_REQ ? sapData.ID_REQ : "";
            c.cod_consulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            c.dataReqPeriodo = sapData.DATA_REQ_PERIODO ? sapData.DATA_REQ_PERIODO : "";
            c.cod_commessa = sapData.ID_COMMESSA ? sapData.ID_COMMESSA : "";
            c.ggIcon = sapData.COD_ATTIVITA_RICHIESTA ? sapData.COD_ATTIVITA_RICHIESTA : "";
            c.idPeriodoScheduling = sapData.ID_PERIODO_SCHEDULING_RIC ? sapData.ID_PERIODO_SCHEDULING_RIC : "";
            c.codicePJM = sapData.COD_PJM ? sapData.COD_PJM : "";
            c.dataInserimento = sapData.DATA_INSERIMENTO ? sapData.DATA_INSERIMENTO : "";
            c.note = sapData.NOTE ? sapData.NOTE : "";
            c.stato = sapData.STATO_REQ ? sapData.STATO_REQ : "";
            c.gg = sapData.GIORNO_PERIODO ? sapData.GIORNO_PERIODO : "";
            return c;
        }
    },

    requestsFerieNew: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.idReqFerie = sapData.ID_REQ_FERIE ? sapData.ID_REQ_FERIE : "";
            c.idPeriodoRichiestaFerie = sapData.ID_PERIODO_RICHIESTA_FERIE ? sapData.ID_PERIODO_RICHIESTA_FERIE : "";
            c.gg = sapData.GIORNO_PERIODO_FERIE ? sapData.GIORNO_PERIODO_FERIE : "";
            c.codCommessaFerie = sapData.ID_COMMESSA_FERIE ? sapData.ID_COMMESSA_FERIE : "";
            c.codicePJM = sapData.COD_PJM_FERIE ? sapData.COD_PJM_FERIE : "";
            c.codConsulente = sapData.COD_CONSULENTE_FERIE ? sapData.COD_CONSULENTE_FERIE : "";
            c.ggIcon = sapData.COD_ATTIVITA_RICHIESTA ? sapData.COD_ATTIVITA_RICHIESTA : "";
            c.statoFerie = sapData.STATO_RICHIESTA_FERIE ? sapData.STATO_RICHIESTA_FERIE : "";
            c.note = sapData.NOTE_FERIE ? sapData.NOTE_FERIE : "";
            c.dataRichiestaFerie = sapData.DATA_RICHIESTA_FERIE ? sapData.DATA_RICHIESTA_FERIE : "";
            c.dataInserimentoFerie = sapData.DATA_INSERIMENTO_FERIE ? sapData.DATA_INSERIMENTO_FERIE : "";
            c.codConsulenteInserimentoFerie = sapData.COD_CONSULENTE_INSERIMENTO_FERIE ? sapData.COD_CONSULENTE_INSERIMENTO_FERIE : "";
            c.codUmApprovazioneFerie = sapData.COD_UM_APPROVAZIONE_FERIE ? sapData.COD_UM_APPROVAZIONE_FERIE : "";
            c.dataApprovazioneFerie = sapData.DATA_APPROVAZIONE_FERIE ? sapData.DATA_APPROVAZIONE_FERIE : "";
            c.codUmRifiutoFerie = sapData.COD_UM_RIFIUTO_FERIE ? sapData.COD_UM_RIFIUTO_FERIE : "";
            c.dataRifiutoFerie = sapData.DATA_RIFIUTO_FERIE ? sapData.DATA_RIFIUTO_FERIE : "";

            return c;
        }
    },

    getFromDraftByIdReq: {
        fromSAP: function (sapData) {
            var r = {};
            r.idReqDraft = sapData.ID_REQ_DRAFT ? sapData.ID_REQ_DRAFT : "";
            r.codConsulenteDraft = sapData.COD_CONSULENTE_DRAFT ? sapData.COD_CONSULENTE_DRAFT : "";
            r.dataReqPeriodoDraft = sapData.DATA_REQ_PERIODO_DRAFT ? sapData.DATA_REQ_PERIODO_DRAFT : "";
            r.codCommessaDraft = sapData.ID_COMMESSA_DRAFT ? sapData.ID_COMMESSA_DRAFT : "";
            r.codAttivitaRichiestaDraft = sapData.COD_ATTIVITA_RICHIESTA_DRAFT ? sapData.COD_ATTIVITA_RICHIESTA_DRAFT : "";
            r.idPeriodoSchedulingDraft = sapData.ID_PERIODO_SCHEDULING_DRAFT ? sapData.ID_PERIODO_SCHEDULING_DRAFT : "";
            r.codPjmDraft = sapData.COD_PJM_DRAFT ? sapData.COD_PJM_DRAFT : "";
            r.dataInserimentoDraft = sapData.DATA_INSERIMENTO_DRAFT ? sapData.DATA_INSERIMENTO_DRAFT : "";
            r.noteDraft = sapData.NOTE_DRAFT ? sapData.NOTE_DRAFT : "";
            r.giornoPeriodoDraft = sapData.GIORNO_PERIODO_DRAFT ? sapData.GIORNO_PERIODO_DRAFT : "";
            r.statoReqDraft = sapData.STATO_REQ_DRAFT ? sapData.STATO_REQ_DRAFT : "";
            return r;
        }
    },

    multipleReq: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var r = {};
            r.codAttivitaRichiesta = sapData.COD_ATTIVITA_RICHIESTA ? sapData.COD_ATTIVITA_RICHIESTA : "";
            r.codCommessa = sapData.COD_COMMESSA ? sapData.COD_COMMESSA : "";
            r.codConsulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            r.nomeConsulente = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
            r.cognomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            r.dataInserimento = sapData.DATA_INSERIMENTO ? sapData.DATA_INSERIMENTO : "";
            r.dataReqPeriodo = sapData.DATA_REQ_PERIODO ? sapData.DATA_REQ_PERIODO : "";
            r.giornoPeriodo = sapData.GIORNO_PERIODO ? sapData.GIORNO_PERIODO : "";
            r.descrizioneCommessa = sapData.DESCRIZIONE_COMMESSA ? decodeURI(sapData.DESCRIZIONE_COMMESSA) : "";
            r.idPeriodo = sapData.ID_PERIODO_SCHEDULING_RIC ? sapData.ID_PERIODO_SCHEDULING_RIC : "";
            r.nota = sapData.NOTE ? sapData.NOTE : "";
            r.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            r.codicePJM = sapData.COD_PJM ? sapData.COD_PJM : "";
            r.statoRic = sapData.STATO_REQ ? sapData.STATO_REQ : "";
            return r;
        }
    },


    skills: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var r = {};
            r.codSkill = sapData.COD_SKILL ? sapData.COD_SKILL : "";
            r.nomeSkill = sapData.NOME_SKILL ? sapData.NOME_SKILL : "";
            r.descSkill = sapData.DESC_SKILL ? sapData.DESC_SKILL : "";
            return r;
        }
    },

    consulenteSkill: {
        fromSAPItems: function (sapData) {
            var results = sapData.d;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var r = {};
            r.nomeConsulente = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
            r.cognomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            r.codProfiloConsulente = sapData.COD_PROFILO_CONSULENTE ? sapData.COD_PROFILO_CONSULENTE : "";
            r.note = sapData.NOTE ? sapData.NOTE : "";
            r.stato = sapData.STATO ? sapData.STATO : "";
            r.codiceConsulenteBu = sapData.COD_CONSULENTE_BU ? sapData.COD_CONSULENTE_BU : "";
            r.descBu = sapData.DESC_BU ? sapData.DESC_BU : "";
            r.tipoAzienda = sapData.TIPO_AZIENDA ? sapData.TIPO_AZIENDA : "";
            r.giornateIntercompany = sapData.GIORNATE_INTERCOMPANY ? sapData.GIORNATE_INTERCOMPANY : "";
            r.tariffaIntercompany = sapData.TARIFFA_INTERCOMPANY ? sapData.TARIFFA_INTERCOMPANY : "";
            r.intercompany = sapData.INTERCOMPANY ? sapData.INTERCOMPANY : "";
            r.codConsulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            r.codSkill = sapData.COD_SKILL ? sapData.COD_SKILL : "";
            r.nomeSkill = sapData.NOME_SKILL ? sapData.NOME_SKILL : "";
            r.descSkill = sapData.DESC_SKILL ? sapData.DESC_SKILL : "";
            return r;
        }
    },

    consulenteSkillNoD: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var r = {};
            r.nomeConsulente = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
            r.cognomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            r.codProfiloConsulente = sapData.COD_PROFILO_CONSULENTE ? sapData.COD_PROFILO_CONSULENTE : "";
            r.note = sapData.NOTE ? sapData.NOTE : "";
            r.stato = sapData.STATO ? sapData.STATO : "";
            r.codiceConsulenteBu = sapData.COD_CONSULENTE_BU ? sapData.COD_CONSULENTE_BU : "";
            r.descBu = sapData.DESC_BU ? sapData.DESC_BU : "";
            r.tipoAzienda = sapData.TIPO_AZIENDA ? sapData.TIPO_AZIENDA : "";
            r.giornateIntercompany = sapData.GIORNATE_INTERCOMPANY ? sapData.GIORNATE_INTERCOMPANY : "";
            r.tariffaIntercompany = sapData.TARIFFA_INTERCOMPANY ? sapData.TARIFFA_INTERCOMPANY : "";
            r.intercompany = sapData.INTERCOMPANY ? sapData.INTERCOMPANY : "";
            r.codConsulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            r.codSkill = sapData.COD_SKILL ? sapData.COD_SKILL : "";
            r.nomeSkill = sapData.NOME_SKILL ? sapData.NOME_SKILL : "";
            r.descSkill = sapData.DESC_SKILL ? sapData.DESC_SKILL : "";
            return r;
        }
    },

    consulenteSkillNoD: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var r = {};
            r.nomeConsulente = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
            r.cognomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            r.codProfiloConsulente = sapData.COD_PROFILO_CONSULENTE ? sapData.COD_PROFILO_CONSULENTE : "";
            r.note = sapData.NOTE ? sapData.NOTE : "";
            r.stato = sapData.STATO ? sapData.STATO : "";
            r.codiceConsulenteBu = sapData.COD_CONSULENTE_BU ? sapData.COD_CONSULENTE_BU : "";
            r.descBu = sapData.DESC_BU ? sapData.DESC_BU : "";
            r.tipoAzienda = sapData.TIPO_AZIENDA ? sapData.TIPO_AZIENDA : "";
            r.giornateIntercompany = sapData.GIORNATE_INTERCOMPANY ? sapData.GIORNATE_INTERCOMPANY : "";
            r.tariffaIntercompany = sapData.TARIFFA_INTERCOMPANY ? sapData.TARIFFA_INTERCOMPANY : "";
            r.intercompany = sapData.INTERCOMPANY ? sapData.INTERCOMPANY : "";
            r.codConsulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            r.codSkill = sapData.COD_SKILL ? sapData.COD_SKILL : "";
            r.nomeSkill = sapData.NOME_SKILL ? sapData.NOME_SKILL : "";
            r.descSkill = sapData.DESC_SKILL ? sapData.DESC_SKILL : "";
            return r;
        }
    },

    readOfficialScheduling: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var r = {};
            r.nomeConsulente = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
            r.cognomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            r.codConsulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            r.idReq = sapData.ID_REQ_FINAL ? sapData.ID_REQ_FINAL : "";
            r.codConsulente = sapData.COD_CONSULENTE_FINAL ? sapData.COD_CONSULENTE_FINAL : "";
            r.dataReqPeriodo = sapData.DATA_REQ_PERIODO_FINAL ? sapData.DATA_REQ_PERIODO_FINAL : "";
            r.idCommessa = sapData.ID_COMMESSA_FINAL ? sapData.ID_COMMESSA_FINAL : "";
            r.codAttivitaRichiesta = sapData.COD_ATTIVITA_RICHIESTA_FINAL_FINAL ? sapData.COD_ATTIVITA_RICHIESTA_FINAL_FINAL : "";
            r.idPeriodoScheduling = sapData.ID_PERIODO_SCHEDULING_FINAL ? sapData.ID_PERIODO_SCHEDULING_FINAL : "";
            r.codPjm = sapData.COD_PJM_FINAL ? sapData.COD_PJM_FINAL : "";
            r.dataInserimento = sapData.DATA_INSERIMENTO_FINAL ? sapData.DATA_INSERIMENTO_FINAL : "";
            r.idReqRichiesta = sapData.ID_REQ_RICHIESTA ? sapData.ID_REQ_RICHIESTA : "";
            r.idReqModifica = sapData.ID_REQ_MODIFICA ? sapData.ID_REQ_MODIFICA : "";
            r.giornoPeriodo = sapData.GIORNO_PERIODO_FINAL ? sapData.GIORNO_PERIODO_FINAL : "";
            r.note = sapData.NOTE_FINAL ? sapData.NOTE_FINAL : "";
            r.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            r.descrizioneCommessa = sapData.DESCRIZIONE_COMMESSA ? decodeURI(sapData.DESCRIZIONE_COMMESSA) : "";
            return r;
        }
    },

    getModifyRequest: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var r = {};
            r.codAttivita = sapData.COD_ATTIVITA_RICHIESTA_MODIFICA ? sapData.COD_ATTIVITA_RICHIESTA_MODIFICA : "";
            r.codCommessa = sapData.COD_COMMESSA_MODIFICA ? sapData.COD_COMMESSA_MODIFICA : "";
            r.codCommessaOld = sapData.COD_COMMESSA_OLD_MODIFICA ? sapData.COD_COMMESSA_OLD_MODIFICA : "";
            r.codConsulente = sapData.COD_CONSULENTE_MODIFICA ? sapData.COD_CONSULENTE_MODIFICA : "";
            r.codPjm = sapData.COD_PJM_MODIFICA ? sapData.COD_PJM_MODIFICA : "";
            r.dataInserimento = sapData.DATA_INSERIMENTO_MODIFICA ? (sapData.DATA_INSERIMENTO_MODIFICA).getFullYear() + "-" + ((sapData.DATA_INSERIMENTO_MODIFICA).getMonth() + 1) + "-" + (sapData.DATA_INSERIMENTO_MODIFICA).getDate() : "";
            r.dataRichiesta = sapData.DATA_REQ_PERIODO_MODIFICA ? (sapData.DATA_REQ_PERIODO_MODIFICA).getFullYear() + "-" + ((sapData.DATA_REQ_PERIODO_MODIFICA).getMonth() + 1) + "-" + (sapData.DATA_REQ_PERIODO_MODIFICA).getDate() : "";
            r.giorno = sapData.GIORNO_PERIODO_MODIFICA ? parseInt(sapData.GIORNO_PERIODO_MODIFICA) : "";
            r.idPeriodo = sapData.ID_PERIODO_SCHEDULING_RIC_MODIFICA ? parseInt(sapData.ID_PERIODO_SCHEDULING_RIC_MODIFICA) : "";
            r.idReqModifica = sapData.ID_REQ_MODIFICA ? parseInt(sapData.ID_REQ_MODIFICA) : "";
            r.idScambio = sapData.ID_SCAMBIO ? parseInt(sapData.ID_SCAMBIO) : "";
            r.note = sapData.NOTE_MODIFICA ? sapData.NOTE_MODIFICA : "";
            r.stato = sapData.STATO_REQ_MODIFICA ? sapData.STATO_REQ_MODIFICA : "";
            r.idReqOriginale = sapData.ID_REQ_ORIGINALE ? sapData.ID_REQ_ORIGINALE : 0;
            return r;
        }
    },

    getPeriodsModifyRequest: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = (this.fromSAP(results.results[i]));
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    };
                };
            };
            return ret;
        },

        fromSAP: function (sapData) {
            var r = {};
            r.idReqModifica = sapData.ID_REQ_MODIFICA ? sapData.ID_REQ_MODIFICA : "";
            r.idScambio = sapData.ID_SCAMBIO ? sapData.ID_SCAMBIO : "";
            r.giornoPeriodoModifica = sapData.GIORNO_PERIODO_MODIFICA ? sapData.GIORNO_PERIODO_MODIFICA : "";
            r.noteModifica = sapData.NOTE_MODIFICA ? sapData.NOTE_MODIFICA : "";
            r.statoReqModifica = sapData.STATO_REQ_MODIFICA ? sapData.STATO_REQ_MODIFICA : "";
            r.dataInserimentoModifica = sapData.DATA_INSERIMENTO_MODIFICA ? utils.Formatter.formatDateFromHana(sapData.DATA_INSERIMENTO_MODIFICA) : "";
            r.codPjmModifica = sapData.COD_PJM_MODIFICA ? sapData.COD_PJM_MODIFICA : "";
            r.idPeriodoSchedModifica = sapData.ID_PERIODO_SCHEDULING_RIC_MODIFICA ? sapData.ID_PERIODO_SCHEDULING_RIC_MODIFICA : "";
            r.codAttivitaRichiestaModifica = sapData.COD_ATTIVITA_RICHIESTA_MODIFICA ? sapData.COD_ATTIVITA_RICHIESTA_MODIFICA : "";
            r.codCommessaModifica = sapData.COD_COMMESSA_MODIFICA ? sapData.COD_COMMESSA_MODIFICA : "";
            r.codCommessaOldModifica = sapData.COD_COMMESSA_OLD_MODIFICA ? sapData.COD_COMMESSA_OLD_MODIFICA : "";
            r.dataRichiestaModifica = sapData.DATA_REQ_PERIODO_MODIFICA ? utils.Formatter.formatDateFromHana(sapData.DATA_REQ_PERIODO_MODIFICA) : "";
            r.codiceConsulenteModifica = sapData.COD_CONSULENTE_MODIFICA ? sapData.COD_CONSULENTE_MODIFICA : "";
            r.nomePjmModifica = sapData.NOME_CONSULENTE_PJM ? decodeURI(sapData.NOME_CONSULENTE_PJM) : "";
            r.cognomePjmModifica = sapData.COGNOME_CONSULENTE_PJM ? sapData.COGNOME_CONSULENTE_PJM : "";
            r.nomeConsulenteModifica = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
            r.cognomeConsulenteModifica = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            r.codProfiloConsulenteModifica = sapData.COD_PROFILO_CONSULENTE ? sapData.COD_PROFILO_CONSULENTE : "";
            r.noteConsulenteModifica = sapData.NOTE ? sapData.NOTE : "";
            r.statoConsulenteModifica = sapData.STATO ? sapData.STATO : "";
            r.skillConsulenteModifica = sapData.SKILL ? sapData.SKILL : "";
            r.codBuConsulenteModifica = sapData.COD_CONSULENTE_BU ? sapData.COD_CONSULENTE_BU : "";
            r.tipoAziendaConsulenteModifica = sapData.TIPO_AZIENDA ? sapData.TIPO_AZIENDA : "";
            r.intercompanyConsulenteModifica = sapData.INTERCOMPANY ? sapData.INTERCOMPANY : "";
            r.partTimeConsulenteModifica = sapData.PART_TIME ? sapData.PART_TIME : "";
            r.giornateIntercompanyConsulenteModifica = sapData.GIORNATE_INTERCOMPANY ? sapData.GIORNATE_INTERCOMPANY : "";
            r.tariffaIntercompanyConsulenteModifica = sapData.TARIFFA_INTERCOMPANY ? sapData.TARIFFA_INTERCOMPANY : "";
            r.orePartTimeConsulenteModifica = sapData.ORE_PART_TIME ? sapData.ORE_PART_TIME : "";
            r.nomeCommessaModifica = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            r.descrizioneCommessaModifica = sapData.DESCRIZIONE_COMMESSA ? decodeURI(sapData.DESCRIZIONE_COMMESSA) : "";
            r.nomeCommessaOldModifica = sapData.NOME_COMMESSA_OLD ? decodeURI(sapData.NOME_COMMESSA_OLD) : "";
            r.descrizioneCommessaOldModifica = sapData.DESCRIZIONE_COMMESSA_OLD ? decodeURI(sapData.DESCRIZIONE_COMMESSA_OLD) : "";
            r.statoCommessaModifica = sapData.STATO_COMMESSA ? sapData.STATO_COMMESSA : "";
            r.codPJMCommessaModifica = sapData.COD_PJM_AC ? sapData.COD_PJM_AC : "";
            r.codStaffCommessaModifica = sapData.COD_STAFF_COMMESSA ? sapData.COD_STAFF_COMMESSA : "";
            r.codBuCommessaModifica = sapData.CODICE_BU ? sapData.CODICE_BU : "";
            return r;
        }
    },

    travelReports: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            if (results.d) {
                results = results.d;
            }
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSAP(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },

        fromSAP: function (sapData) {
            var c = {};
            c.idRichiesta = sapData.ID_RICHIESTA ? sapData.ID_RICHIESTA : "";
            c.nomeCognome = sapData.NOME_COGNOME ? sapData.NOME_COGNOME : "";
            c.cid = sapData.CID ? sapData.CID : "";
            c.societa = sapData.SOCIETA ? sapData.SOCIETA : "";
            c.cliente = sapData.CLIENTE ? sapData.CLIENTE : "";
            c.commessa = sapData.COMMESSA ? sapData.COMMESSA : "";
            c.attivita = sapData.ATTIVITA ? sapData.ATTIVITA : "";
            c.elementoWbs = sapData.ELEMENTO_WBS ? sapData.ELEMENTO_WBS : "";
            c.rifatturabile = sapData.RIFATTURABILE ? sapData.RIFATTURABILE : "";
            c.tipoSpesaCod = sapData.TIPO_SPESA_COD ? sapData.TIPO_SPESA_COD : "";
            c.tipoSpesaDesc = sapData.TIPO_SPESA_DESC ? sapData.TIPO_SPESA_DESC : "";
            c.luogo = sapData.LUOGO ? sapData.LUOGO : "";
            c.dataCheckIn = sapData.DATA_CHECK_IN ? sapData.DATA_CHECK_IN : "";
            c.dataCheckOut = sapData.DATA_CHECK_OUT ? sapData.DATA_CHECK_OUT : "";
            c.noNotti = sapData.NO_NOTTI ? sapData.NO_NOTTI : "";
            c.dettagliTrasporto = sapData.DETTAGLI_TRASPORTO ? sapData.DETTAGLI_TRASPORTO : "";
            c.richiesteSpeciali = sapData.RICHIESTE_SPECIALI ? sapData.RICHIESTE_SPECIALI : "";
            c.confermato = sapData.CONFERMATO ? sapData.CONFERMATO : "";
            c.noteConferma = sapData.NOTE_CONFERMA ? sapData.NOTE_CONFERMA : "";
            c.dataInserimento = sapData.DATA_INSERIMENTO ? sapData.DATA_INSERIMENTO : "";
            c.costo = sapData.COSTO ? sapData.COSTO : "";
            c.attivitaCod = sapData.ATTIVITA_COD ? sapData.ATTIVITA_COD : "";
            return c;
        }

    },

    wbsSapHana: {
        fromSapItems: function (sapData) {
            var results = sapData;
            if (results.d) {
                results = results.d;
            }
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSap(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },

        fromSap: function (sapData) {
            var c = {};
            c.cidCosnuslenteSap = sapData.CID_CONSULENTE_SAP ? sapData.CID_CONSULENTE_SAP : "";
            c.codConsulenteHana = sapData.COD_CONSULENTE_HANA ? sapData.COD_CONSULENTE_HANA : "";
            c.codCommessaHana = sapData.COD_COMMESSA_HANA ? sapData.COD_COMMESSA_HANA : "";
            c.codCommessaSap = sapData.COD_COMMESSA_SAP ? sapData.COD_COMMESSA_SAP : "";
            c.codAttivitaSap = sapData.COD_ATTIVITA_SAP ? sapData.COD_ATTIVITA_SAP : "";
            c.descCommessaHana = sapData.DESC_COMMESSA_HANA ? sapData.DESC_COMMESSA_HANA : "";
            c.descCommessaSap = sapData.DESC_COMMESSA_SAP ? sapData.DESC_COMMESSA_SAP : "";
            c.descAttivitaSap = sapData.DESC_ATTIVITA_SAP ? sapData.DESC_ATTIVITA_SAP : "";
            c.codLuogoLavoro = sapData.COD_LUOGO_LAVORO_SAP ? sapData.COD_LUOGO_LAVORO_SAP : "";
            c.descLuogoLavoro = sapData.DESC_LUOGO_LAVORO_SAP ? sapData.DESC_LUOGO_LAVORO_SAP : "";
            c.descCommessa = sapData.DESC_COMMESSA ? sapData.DESC_COMMESSA : "";
            c.noteCommessa = sapData.NOTE_COMMESSA ? sapData.NOTE_COMMESSA : "";
            c.codTipoOrario = sapData.COD_TIPO_ORARIO_SAP ? sapData.COD_TIPO_ORARIO_SAP : "";
            c.descTipoOrario = sapData.DESC_TIPO_ORARIO_SAP ? sapData.DESC_TIPO_ORARIO_SAP : "";
            c.fatturabile = sapData.FATTURABILE ? sapData.FATTURABILE : "";
            c.stato = sapData.STATO ? sapData.STATO : "";
            return c;
        }
    },

    commesseHana: {
        fromSapItems: function (sapData) {
            var results = sapData;
            if (results.d) {
                results = results.d;
            }
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSap(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },

        fromSap: function (sapData) {
            var c = {};
            c.codStaff = sapData.COD_STAFF ? sapData.COD_STAFF : "";
            c.codConsulenteStaff = sapData.COD_CONSULENTE_STAFF ? sapData.COD_CONSULENTE_STAFF : "";
            c.codCommessaHana = sapData.COD_COMMESSA ? sapData.COD_COMMESSA : "";
            c.nomeConsulente = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
            c.cognomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            c.codPjmStaff = sapData.COD_PJM_STAFF ? sapData.COD_PJM_STAFF : "";
            c.idStaff = sapData.ID_STAFF ? sapData.ID_STAFF : "";
            c.teamLeader = sapData.TEAM_LEADER ? sapData.TEAM_LEADER : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.descCommessa = sapData.DESC_COMMESSA ? decodeURI(sapData.DESC_COMMESSA) : "";
            return c;
        }
    },

    holidayRequest: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSap(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },

        fromSap: function (sapData) {
            var c = {};
            c.codAttivita = sapData.COD_ATTIVITA_RICHIESTA ? sapData.COD_ATTIVITA_RICHIESTA : "";
            c.codConsulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            c.codBu = sapData.COD_CONSULENTE_BU ? sapData.COD_CONSULENTE_BU : "";
            c.nomeConsulente = sapData.NOME_CONSULENTE ? decodeURI(sapData.NOME_CONSULENTE) : "";
            c.cognomeConsulente = sapData.COGNOME_CONSULENTE ? decodeURI(sapData.COGNOME_CONSULENTE) : "";
            c.codPjm = sapData.COD_PJM ? sapData.COD_PJM : "";
            c.codCommessa = sapData.ID_COMMESSA ? sapData.ID_COMMESSA : "";
            c.nomeCommessa = sapData.NOME_COMMESSA ? decodeURI(sapData.NOME_COMMESSA) : "";
            c.dataInserimento = sapData.DATA_INSERIMENTO ? sapData.DATA_INSERIMENTO : "";
            c.dataRichiesta = sapData.DATA_REQ_PERIODO ? sapData.DATA_REQ_PERIODO : "";
            c.giornoPeriodo = sapData.GIORNO_PERIODO ? sapData.GIORNO_PERIODO : "";
            c.idPeriodo = sapData.ID_PERIODO_SCHEDULING_RIC ? sapData.ID_PERIODO_SCHEDULING_RIC : "";
            c.idReq = sapData.ID_REQ ? sapData.ID_REQ : "";
            c.note = sapData.NOTE ? sapData.NOTE : "";
            c.statoReq = sapData.STATO_REQ ? sapData.STATO_REQ : "";
            return c;
        }
    },


    listaConsulentiPeriodo: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSap(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },

        fromSap: function (sapData) {
            var c = {};
            c.codConsulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            c.idPeriodo = sapData.ID_PERIODO ? sapData.ID_PERIODO : "";
            c.stato = sapData.STATO ? sapData.STATO : "";
            return c;
        }
    },

    listaConsGenSkill: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            if (results.d) {
                results = results.d;
            }
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSap(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },

        fromSap: function (sapData) {
            var c = {};
            c.codConsulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            c.codProfiloConsulente = sapData.COD_PROFILO_CONSULENTE ? sapData.COD_PROFILO_CONSULENTE : "";
            c.codConsulenteBu = sapData.COD_CONSULENTE_BU ? sapData.COD_CONSULENTE_BU : "";
            c.descBu = sapData.DESC_BU ? sapData.DESC_BU : "";
            c.tipoAzienda = sapData.TIPO_AZIENDA ? sapData.TIPO_AZIENDA : "";
            c.note = sapData.NOTE ? sapData.NOTE : "";
            c.skill = sapData.SKILL ? sapData.SKILL : "";
            c.stato = sapData.STATO ? sapData.STATO : "";
            c.nomeSkill = sapData.NOME_SKILL ? sapData.NOME_SKILL : "";
            c.descSkill = sapData.DESC_SKILL ? sapData.DESC_SKILL : "";
            return c;
        }
    },

    getStatoConsPeriodoDraft: {
        fromSAPItems: function (sapData) {
            var results = sapData;
            if (results.d) {
                results = results.d;
            }
            var ret = {
                items: []
            };
            if (results.hasOwnProperty("results")) {
                var l = results.results.length;
                for (var i = 0; i < l; i++) {
                    var oItem = this.fromSap(results.results[i]);
                    if (oItem !== undefined) {
                        ret.items.push(oItem);
                    }
                }
            }
            return ret;
        },

        fromSap: function (sapData) {
            var c = {};
            c.codConsulente = sapData.COD_CONSULENTE ? sapData.COD_CONSULENTE : "";
            c.idPeriodo = sapData.ID_PERIODO ? sapData.ID_PERIODO : "";
            c.stato = sapData.STATO ? sapData.STATO : "";
            c.codBuPjm = sapData.COD_BU_PJM ? sapData.COD_BU_PJM : "";
            return c;
        }
    },


};