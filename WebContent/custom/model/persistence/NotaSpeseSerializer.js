jQuery.sap.declare("model.persistence.NotaSpeseSerializer");
jQuery.sap.require("utils.Util");

model.persistence.NotaSpeseSerializer = {

    notaSpeseEntity: {
        fromSap: function (sap) {
            var p = {};
            p.cid = sap.Pernr;
            p.numeroTrasferta = sap.Reinr;
            p.numeroPeriodoTrasferta = sap.Perio;
            p.attivita = sap.WbsElemt;
            p.numeroProgressivoAttCost = sap.Costseqno;
            p.dataNotaSpese = utils.Util.dateToString(sap.Datv1);
            p.localita = sap.Zort1;
            p.valuta = sap.RecCurr;
            p.categoriaSpesa = sap.ExpType;
            p.importo = sap.RecAmount;
            p.partenza = sap.LocFrom;
            p.arrivo = sap.LocTo;
            p.chilometri = sap.Kmsum;
            p.siglaTipoAuto = sap.Kzpmf;
            p.numProgressivoGiustificativo = sap.Seqno;
            p.numeroGiustificativo = sap.Receiptno;
            p.counterFiori = sap.Zidfiori;
            p.delete = sap.Zdelete;
            p.oraUltimaModifica = utils.Util.getHourFromMs(sap.Times);
            p.note = sap.Descr;
            p.dataUltimaModifica = utils.Util.dateToString(sap.Dates);
            p.trasfertaContabilizzata = sap.Antrg;
            return p;
        },

        fromSapItems: function (results) {
            var r = {};
            var items = [];
            if (results !== undefined && results.length > 0) {
                _.each(results, function (sap) {
                    var c = model.odata.NotaSpeseSerializer.notaSpeseEntity.fromSap(sap);
                    items.push(c);
                });
            }
            if (items.length > 0) {
                r.items = items;
            } else {
                r = undefined;
            }
            return r;
        },

        toSap: function (p) {
            var sap = {};
            sap.Seqno = p.numProgressivoGiustificativo;
            sap.Receiptno = p.numeroGiustificativo;
            sap.Pernr = p.cid;
            sap.Reinr = p.numeroTrasferta;
            sap.Perio = p.numeroPeriodoTrasferta;
            sap.WbsElemt = p.attivita;
            sap.Costseqno = p.numeroProgressivoAttCost;
            sap.Datv1 = utils.Util.setDateToSapInverted(p.dataNotaSpese);
            sap.RecCurr = p.valuta;
            sap.ExpType = p.categoriaSpesa;
            sap.RecAmount = p.importo;
            sap.LocFrom = p.partenza;
            sap.LocTo = p.arrivo;
            sap.Kmsum = p.chilometri;
            sap.Kzpmf = p.siglaTipoAuto;
            sap.Zidfiori = p.counterFiori;
            sap.Zdelete = p.delete;
            sap.Times = utils.Util.getHoursToSap(p.oraUltimaModifica);
            sap.Descr = p.note
            sap.Zort1 = p.localita;
            sap.Dates = utils.Util.setDateToSapInverted(p.dataUltimaModifica);
            return sap;
        }
    }
   
};
