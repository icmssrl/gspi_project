if (!model.persistence.Serializer)
    jQuery.sap.require("model.persistence.Serializer");
if (!model.odata)
    jQuery.sap.require("model.odata.chiamateOdata");

model.tabellaScheduling = {
    // funzione che mi restituisce un JSON formattato per la tabella
    readTable: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(JSON.parse(result));
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.newReadPJMRequest(req, fSuccess, fError);
        return defer.promise;
    },
    
    readPjmAllRequests: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(JSON.parse(result));
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readPjmAllRequests(req, fSuccess, fError);
        return defer.promise;
    },
    
    readPjmSingleConsultant: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(JSON.parse(result));
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readPjmSingleConsultant(req, fSuccess, fError);
        return defer.promise;
    },
    
    
    
    // funzione che mi estrae tutti i consulenti dello staff passato
    readStaffPerCommessaPJM: function (req) {
        var defer = Q.defer();
        this.req = req;

        var fSuccess = function (result) {
            this.staffCompleteList = model.persistence.Serializer.readStaffCompleteView.fromSAPItems(result);
            this.staffCompleteList.items = _.uniq(this.staffCompleteList.items, 'codConsulenteStaff');
            defer.resolve(this.staffCompleteList);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var filtri = "";
        if (req.idStaff) {
            filtri = "?$filter=COD_STAFF eq " + "'" + req.idStaff + "'";
        } else {
            filtri = "?$filter=COD_PJM_AC eq " + "'" + req.cod_consulente + "'";
        }
        model.odata.chiamateOdata.readStaffCompleteView(fSuccess, fError, filtri);
        return defer.promise;
    },
    
    // funzione che estrae tutte le schedulazioni di tutti i consulenti (schedulati) nel periodo passato, per codice staff e codPJM
    readTableTrial: function (req, staffCompleteList) {
        var defer = Q.defer();
        this.pjm = req.cod_consulente;
        this.richiesta = req;
        this.staffCompleteList = staffCompleteList.items;
        
        var fSuccess = function (result) {
            if (result.results) {
                result.results = model.persistence.Serializer.requests.fromSAPItems(result).items;
            }
            var table = [];
            if (result && result.results) {
                var res3 = result.results;
                for (var i = 0; i < res3.length; i++) {
                    if (this.richiesta.idPeriodo === res3[i].idPeriodoScheduling) {
                        if (this.richiesta.idCommessa !== res3[i].cod_commessa && res3[i].ggIcon === "offsite-work") {
                            continue;
                        }
                        var obj = {};
                        if (res3[i].cod_consulente.indexOf("CCG") >= 0) {
                             obj.nomeConsulente = res3[i].skill;
                        } else {
                            obj.nomeConsulente = res3[i].nomeConsulente;
                        }
                        obj.codiceConsulente = res3[i].cod_consulente;
                        if (res3[i].codicePJM === this.pjm || res3[i].cod_commessa === "AA00000008") {
                            if (res3[i].gg) {
                                var prop = "gg" + res3[i].gg;
                                var propIcon = "ggIcon" + res3[i].gg;
                                var propReq = "ggReq" + res3[i].gg;
                            }
                            if (res3[i].dataReqPeriodo) {
                                obj[propReq] = (res3[i].dataReqPeriodo).getFullYear() + "-" + ((res3[i].dataReqPeriodo).getMonth() + 1)+ "-" + (res3[i].dataReqPeriodo).getDate();
                            }
                            if (res3[i].nomeCommessa) {
                                obj[prop] = res3[i].nomeCommessa;
                            }

                            if (res3[i].ggIcon) {
                                obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                            }
                            if (res3[i].codicePJM) {
                                obj.codicePJM = res3[i].codicePJM;
                            }
                            if (res3[i].note) {
                                var prop = "nota" + res3[i].gg;
                                obj[prop] = res3[i].note;
                            }
                            if (res3[i].id_req) {
                                var prop = "req" + res3[i].gg;
                                obj[prop] = res3[i].id_req;
                            }
                            if (res3[i].idStaff) {
                                var prop = "idStaff";
                                obj[prop] = res3[i].idStaff;
                            }
                        }
                        table.push(obj);
                    }
                }
                
                // aggiungo all'array finale tutti i consulenti non schedulati
                for (var t = 0; t < this.staffCompleteList.length; t++) {
                    if (!_.find(table, {
                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff
                        })) {
                        table.push({
                            nomeConsulente: this.staffCompleteList[t].codConsulenteStaff.indexOf("CCG") >= 0 ? this.staffCompleteList[t].descSkill : this.staffCompleteList[t].cognomeConsulente + " " + this.staffCompleteList[t].nomeConsulente,
                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff
                        });
                    }
                }
                var res = [];
                res.push(table);
                defer.resolve(res);
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getTableSchedulingPeriodsTrial(req, fSuccess, fError);
        return defer.promise;
    },
    
    // funzione che estrae tutte le schedulazioni di tutti i consulenti (schedulati) nel periodo passato, per codice staff e codPJM
    readTableAllRequestsForUM: function (req, staffCompleteList) {
        var defer = Q.defer();
        this.pjm = req.cod_consulente;
        this.richiesta = req;
        this.staffCompleteList = staffCompleteList.items;
        
        var fSuccess = function (result) {
            if (result.results) {
                result.results = model.persistence.Serializer.requests.fromSAPItems(result).items;
            }
            var table = [];
            if (result && result.results) {
                var res3 = result.results;
                for (var i = 0; i < res3.length; i++) {
                    if (this.richiesta.idPeriodo === res3[i].idPeriodoScheduling) {
                        if (this.richiesta.idCommessa !== res3[i].cod_commessa && res3[i].ggIcon === "offsite-work") {
                            continue;
                        }
                        var obj = {};
                        if (res3[i].cod_consulente.indexOf("CCG") >= 0) {
                             obj.nomeConsulente = res3[i].skill;
                        } else {
                            obj.nomeConsulente = res3[i].nomeConsulente;
                        }
                        obj.codiceConsulente = res3[i].cod_consulente;
                        if (res3[i].codicePJM === this.pjm || res3[i].cod_commessa === "AA00000008") {
                            if (res3[i].gg) {
                                var prop = "gg" + res3[i].gg;
                                var propIcon = "ggIcon" + res3[i].gg;
                                var propReq = "ggReq" + res3[i].gg;
                            }
                            if (res3[i].dataReqPeriodo) {
                                obj[propReq] = (res3[i].dataReqPeriodo).getFullYear() + "-" + (res3[i].dataReqPeriodo).getMonth() + "-" + (res3[i].dataReqPeriodo).getDate();
                            }
                            if (res3[i].nomeCommessa) {
                                obj[prop] = res3[i].nomeCommessa;
                            }

                            if (res3[i].ggIcon) {
                                obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                            }
                            if (res3[i].codicePJM) {
                                obj.codicePJM = res3[i].codicePJM;
                            }
                            if (res3[i].note) {
                                var prop = "nota" + res3[i].gg;
                                obj[prop] = res3[i].note;
                            }
                            if (res3[i].id_req) {
                                var prop = "req" + res3[i].gg;
                                obj[prop] = res3[i].id_req;
                            }
                            if (res3[i].idStaff) {
                                var prop = "idStaff";
                                obj[prop] = res3[i].idStaff;
                            }
                        }
                        table.push(obj);
                    }
                }
                
                // aggiungo all'array finale tutti i consulenti non schedulati
                for (var t = 0; t < this.staffCompleteList.length; t++) {
                    if (!_.find(table, {
                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff
                        })) {
                        table.push({
                            nomeConsulente: this.staffCompleteList[t].codConsulenteStaff.indexOf("CCG") >= 0 ? this.staffCompleteList[t].descSkill : this.staffCompleteList[t].cognomeConsulente + " " + this.staffCompleteList[t].nomeConsulente,
                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff
                        });
                    }
                }
                var res = [];
                res.push(table);
                defer.resolve(res);
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getTableSchedulingAllForUm(req, fSuccess, fError);
        return defer.promise;
    },
    
    readTableAll: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (result.results) {
                result.results = model.persistence.Serializer.requestsSingleConsultant.fromSAPItems(result).items;
            }
            var table = [];
            if (result && result.results) {
                var res3 = result.results;
                for (var i = 0; i < res3.length; i++) {
                    var obj = {};
                    obj.codiceConsulente = res3[i].cod_consulente;
                    if (res3[i].gg) {
                        var prop = "gg" + res3[i].gg;
                        var propIcon = "ggIcon" + res3[i].gg;
                    }
                    if (res3[i].nomeCommessa) {
                        obj[prop] = res3[i].nomeCommessa;
                    }
                    if (res3[i].ggIcon) {
                        obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                    }
                    if (res3[i].codicePJM) {
                        obj.codicePJM = res3[i].codicePJM;
                    }
                    if (res3[i].note) {
                        var prop = "nota" + res3[i].gg;
                        obj[prop] = res3[i].note;
                    }
                    if (res3[i].id_req) {
                        var prop = "req" + res3[i].gg;
                        obj[prop] = res3[i].id_req;
                    }
                    table.push(obj);
                }
                var res = [];
                res.push(table);
                defer.resolve(res);
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getAllTableSchedulingPeriods(req, fSuccess, fError);
        return defer.promise;
    },
    
    readSchedulazioni: function (codPjm, codConsulente, idPeriodo) {
        var deferSchedulazioni = Q.defer();
        var fSuccess = function (result) {
            result.results = result.d.results;
            var arrayFinale = model.persistence.Serializer.multipleReq.fromSAPItems(result);
            deferSchedulazioni.resolve(arrayFinale.items);
        };
        var fError = function (err) {
            deferSchedulazioni.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.odata.chiamateOdata.readSchedulazioniPerControllo(codPjm, codConsulente, idPeriodo, fSuccess, fError);
        return deferSchedulazioni.promise;
    },
    
    readAllConsultantsByPJM: function (req, staffCompleteList) {
        var defer = Q.defer();
        this.pjm = req.cod_consulente;
        this.richiesta = req;
        this.staffCompleteList = staffCompleteList.items;
        
        var fSuccess = function (result) {
            result.results = model.persistence.Serializer.requestsAll.fromSAPItems(result).items;
            var table = [];
            var arrCosnulente = [];
            var arrCommesse = [];
            
            if (result && result.results) {
                var res = result.results;
                res3 = _.uniq(res,'id_req');
                for (var q=0; q<res.length;q++) {
                    if(!res[q].id_req) {
                        res3.push(res[q]);
                    }
                }
                for (var i = 0; i < res3.length; i++) {
                    if (!_.find(arrCommesse, {
                            cod: res3[i].cod_commessa
                        })) {
                        arrCommesse.push({
                            cod: res3[i].cod_commessa
                        });
                    }
                }
                arrCommesse.push({
                    cod: "AA00000008"
                });
                for (var i = 0; i < res3.length; i++) {
//                    if (!_.find(arrCosnulente, {
//                            cod: res3[i].cod_consulente
//                        })) {
//                        if (res3[i].cod_consulente.indexOf("CCG") >= 0) {
//                        }
//                        arrCosnulente.push({
//                            cod: res3[i].cod_consulente,
//                            nome: res3[i].nomeConsulente
//                        });
//                    }
                    if (this.richiesta.idPeriodo === res3[i].idPeriodoScheduling ) {
                        var obj = {};
                        if (res3[i].cod_consulente.indexOf("CCG") >= 0) {
                             obj.nomeConsulente = res3[i].skill;
                        } else {
                            obj.nomeConsulente = res3[i].nomeConsulente;
                        }
                        obj.codiceConsulente = res3[i].cod_consulente;

                        if(_.find(arrCommesse,{cod:res3[i].idCommessa})) {
                            if (res3[i].codicePJM === this.pjm ) {
                                if (res3[i].gg) {
                                    var prop = "gg" + res3[i].gg;
                                    var propIcon = "ggIcon" + res3[i].gg;
                                    var propReq = "ggReq" + res3[i].gg;
                                }
                                if (res3[i].nomeCommessa) {
                                    obj[prop] = res3[i].nomeCommessa;
                                }
                                if (res3[i].ggIcon) {
                                    obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                                }
                                if (res3[i].codicePJM) {
                                    obj.codicePJM = res3[i].codicePJM;
                                }
                                if (res3[i].note) {
                                    var prop = "nota" + res3[i].gg;
                                    obj[prop] = res3[i].note;
                                }
                                if (res3[i].id_req) {
                                    var prop = "req" + res3[i].gg;
                                    obj[prop] = res3[i].id_req;
                                }
                                if (res3[i].idStaff) {
                                    var prop = "idStaff";
                                    obj[prop] = res3[i].idStaff;
                                }
                            }
                            table.push(obj);
                        }
                    }
                }
                for (var t = 0; t < arrCosnulente.length; t++) {
                    if (!_.find(table, {
                            codiceConsulente: arrCosnulente[t].cod
                        })) {
                        table.push({
                            nomeConsulente: arrCosnulente[t].nome,
                            codiceConsulente: arrCosnulente[t].cod
                        });
                    }
                }
                
                // aggiungo all'array finale tutti i consulenti non schedulati
                for (var t = 0; t < this.staffCompleteList.length; t++) {
                    if (!_.find(table, {
                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff
                        })) {
                        table.push({
                            nomeConsulente: this.staffCompleteList[t].codConsulenteStaff.indexOf("CCG") >= 0 ? this.staffCompleteList[t].descSkill : this.staffCompleteList[t].cognomeConsulente + " " + this.staffCompleteList[t].nomeConsulente,
                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff
                        });
                    }
                }
                var res = [];
                res.push(table);
                defer.resolve(res);
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getTableSchedulingAllConsultantsByPJM(req, fSuccess, fError);
        
        return defer.promise;
    },
    
    
};