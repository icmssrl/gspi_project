jQuery.sap.declare("model.UpdateAnagStaffGenerico");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.UpdateAnagStaffGenerico = {
    
    updateAnagStaff: function (codConsGen, codCons, codCommessa) {
        var defer = Q.defer();

        var fSuccess = function () {
            defer.resolve();
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.changeCodConsAnagStaff(codConsGen, codCons, codCommessa, fSuccess, fError);
        return defer.promise;
    },
    
    updateRequest: function (codConsGen, codCons, codCommessa) {
        var defer = Q.defer();

        var fSuccess = function () {
            defer.resolve();
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.changeCodConsRequest(codConsGen, codCons, codCommessa, fSuccess, fError);
        return defer.promise;
    },
    
    updateDraft: function (codConsGen, codCons, codCommessa) {
        var defer = Q.defer();

        var fSuccess = function () {
            defer.resolve();
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.changeCodConsDraft(codConsGen, codCons, codCommessa, fSuccess, fError);
        return defer.promise;
    },
    
    updateFinal: function (codConsGen, codCons, codCommessa, idPeriodo) {
        var defer = Q.defer();

        var fSuccess = function () {
            defer.resolve();
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.changeCodConsFinal(codConsGen, codCons, codCommessa, idPeriodo, fSuccess, fError);
        return defer.promise;
    },
    
    restoreAnagStaff: function (codCons, codConsGen, codCommessa) {
        var defer = Q.defer();

        var fSuccess = function () {
            defer.resolve();
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.restoreCodConsAnagStaff(codCons, codConsGen, codCommessa, fSuccess, fError);
        return defer.promise;
    },
    
    restoreRequest: function (codCons, codConsGen, codCommessa) {
        var defer = Q.defer();

        var fSuccess = function () {
            defer.resolve();
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.restoreCodConsRequest(codCons, codConsGen, codCommessa, fSuccess, fError);
        return defer.promise;
    },
    
    restoreDraft: function (codCons, codConsGen, codCommessa) {
        var defer = Q.defer();

        var fSuccess = function () {
            defer.resolve();
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.restoreCodConsDraft(codCons, codConsGen, codCommessa, fSuccess, fError);
        return defer.promise;
    }
};
