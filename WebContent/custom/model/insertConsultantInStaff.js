if (!model.odata)
    jQuery.sap.require("model.odata.chiamateOdata");

model.insertConsultantInStaff = {
    // aggiunta di un consulente (reale o generico) nello staff del progetto
    insert: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.insertConsultantInStaff(req, fSuccess, fError);
        return defer.promise;
    }
};