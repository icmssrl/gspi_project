jQuery.sap.declare("model.tabellaSchedulingUM");
//jQuery.sap.require("utils.Busy");
//jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.odata.chiamateOdata");

model.tabellaSchedulingUM = {
    // funzione lettura richiesta per commessa DRAFT
    readUmProjectRequests: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (result && result !== "") {
                defer.resolve(JSON.parse(result));
            } else {
                defer.resolve();
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readUmProjectRequests(req, fSuccess, fError);
        return defer.promise;
    },
    
    // funzione lettura richiesta tutte commesse DRAFT
    readUmAllRequests: function (req) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            if (result && result !== "") {
                defer.resolve(JSON.parse(result));
            } else {
                defer.resolve();
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readUmAllRequests(req, fSuccess, fError);
        return defer.promise;
    },
    
    // funzione che inseriche le richieste di modifica in DRAFT
    insertModifyInFDraft: function (array) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.insertMultiInDraft(array, fSuccess, fError);
        return defer.promise;
    },
    
    
    
    // controllo se sono presenti richieste di modifica per il periodo e la BU
    checkListaCommesseConRichiesteModifica: function (periodo, bu, stato) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(JSON.parse(result));
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.checkListaCommesseConRichiesteModifica(periodo, bu, stato, fSuccess, fError);
        return defer.promise;
    },    
    
    deleteModifyInFDraft: function (arrayToDel) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteMultiInDraft(arrayToDel, fSuccess, fError);
        return defer.promise;
    },
    
    modModifyInFDraft: function (arrayToMod) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            defer.resolve(result);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.modifyMultiInDraft(arrayToMod, fSuccess, fError);
        return defer.promise;
    },
    
    readStaffPerCommessaUM: function (req) {
        var defer = Q.defer();
        this.req = req;

        var fSuccess = function (result) {
            this.staffCompleteList = model.persistence.Serializer.readStaffCompleteView.fromSAPItems(result);
            defer.resolve(this.staffCompleteList);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var filtri = "";
        if (req.idStaff) {
            filtri = "?$filter=COD_STAFF eq " + "'" + req.idStaff + "'";
        } else {
            filtri = "?$filter=COD_PJM_AC eq " + "'" + req.cod_consulente + "'";
        }
        model.odata.chiamateOdata.readStaffCompleteView(fSuccess, fError, filtri);
        return defer.promise;
    },
    
    confronta: function (a, b) {
        var c = this.richiesta.idCommessa;
        if (a.idCommessa === c) {
            if (b.idCommessa !== "AA00000008") {
                return -1;
            } else { 
                return 1;
            }
        } else if (a.idCommessa === "AA00000008") {
            return -1;
        } else {
            return 1;
        }
    },

    readTable: function (req, staffCompleteList) {
        var defer = Q.defer();
        this.richiesta = req;
        this.staffCompleteList = staffCompleteList.items;

        var fSuccess = function (result) {
            var requests = [];
            if (result.results) {
                result.results = model.persistence.Serializer.requestsDraft.fromSAPItems(result).items;
            }
            var table = [];
            var arrConsulente = [];
            if (result && result.results) {
                var res3 = _.uniq(result.results, function (item) {
                    return item.id_req;
                });
                res3 = res3.sort(this.confronta.bind(this));
                var resConfirmed = _.where(res3, {
                    stato: 'CONFIRMED',
                    idPeriodoScheduling: this.richiesta.idPeriodo
                });
                var resStandBy = _.where(res3, {
                    stato: 'STANDBY',
                    idPeriodoScheduling: this.richiesta.idPeriodo
                });
                var resSchedulazioni = resConfirmed.concat(resStandBy);
                for (var i = 0; i < res3.length; i++) {
//                    if (res3[i].stato === "CONFIRMED" || res3[i].stato === "STANDBY") {
                        var obj = {};
                        obj.nomeConsulente = res3[i].nomeConsulente;
                        obj.codiceConsulente = res3[i].cod_consulente;
                        obj.nomeSkill = res3[i].nomeSkill;
                        var propNote = "nota" + res3[i].gg;
                        obj[propNote] = res3[i].note;
                        if (res3[i].gg) {
                            var prop = "gg" + res3[i].gg;
                            var propIcon = "ggIcon" + res3[i].gg;
                            var propConflict = "ggConflict" + res3[i].gg;
                            var propReq = "ggReq" + res3[i].gg;
                            var propStato = "ggStato" + res3[i].gg;
                        }
                        if (res3[i].dataReqPeriodo) {
                            obj[propReq] = res3[i].dataReqPeriodo;
                        }
                        if (res3[i].nomeCommessa) {
                            obj[prop] = res3[i].nomeCommessa;
                        }
                        if (res3[i].stato) {
                            obj[propStato] = res3[i].stato;
                        }
                        /* verifico se ci sono delle giornate con conflitti e in caso metto un'icona di avvertimento */
                        var richiesteContemporanee = _.where(resSchedulazioni, {
                            'gg': res3[i].gg,
                            'cod_consulente': res3[i].cod_consulente
                        });
                        var mezzeRichiesteContemporanee = _.where(richiesteContemporanee, {
                            'ggIcon': "halfDay"
                        });
                        var mezzeRichiesteFerieContemporanee = _.where(richiesteContemporanee, {
                            'ggIcon': "bed"
                        });
                        var nRichiesteContemporanee = richiesteContemporanee.length;
                        var nMezzeRichiesteContemporanee = mezzeRichiesteContemporanee.length + mezzeRichiesteFerieContemporanee.length;

                        if (nMezzeRichiesteContemporanee > 2 || (nRichiesteContemporanee > 1 && nRichiesteContemporanee !== nMezzeRichiesteContemporanee)) {
                            obj[propConflict] = true;
                        } else {
                            obj[propConflict] = false;
                        }

                        if (res3[i].ggIcon) {
                            obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                        }
                        if (res3[i].codicePJM) {
                            obj.codicePJM = res3[i].codicePJM;
                        }
                        if (res3[i].cod_consulenteBu) {
                            obj.cod_consulenteBu = res3[i].cod_consulenteBu;
                        }
                        if (res3[i].toCheck) {
                            var prop = "toCheck" + res3[i].gg;
                            obj[prop] = res3[i].toCheck;
                        }
                        if (res3[i].id_req) {
                            var prop = "req" + res3[i].gg;
                            obj[prop] = res3[i].id_req;
                            var propOld = "idReqOld" + res3[i].gg;
                            obj[propOld] = (res3[i].idReqOld !== "") ? parseInt(res3[i].idReqOld) : 0;
                        }
                        //**
                        table.push(obj);

//                    }
                }
                // aggiungo all'array finale tutti i consulenti non schedulati
                for (var t = 0; t < this.staffCompleteList.length; t++) {
                    if (!_.find(table, {
                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff
                        })) {
                        table.push({
                            nomeConsulente: this.staffCompleteList[t].codConsulenteStaff.indexOf("CCG") >= 0 ? this.staffCompleteList[t].descSkill : this.staffCompleteList[t].cognomeConsulente + " " + this.staffCompleteList[t].nomeConsulente,
                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff,
                            richiesteCancellate: 0
                        });
                    }
                }
                var res = [];
                res.push(table);
                defer.resolve(res);
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getTableSchedulingPeriodsUM(req, fSuccess, fError);
        return defer.promise;
    },

    readSingleReq: function (idReq) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            var res = model.persistence.Serializer.getFromDraftByIdReq.fromSAP(result.results[0]);
            defer.resolve(res);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getFromDraftByIdReq(idReq, fSuccess, fError);
        return defer.promise;
    },

    deleteRequest: function (idReq, state, numero) {
        if (numero) {
            this.numeroRichiesteDaEliminare = numero;
        }
        this.numeroRichiesteDaEliminare--;

        var deferDeleteRequest = Q.defer();

        var fSuccess = function (ris) {
            // faccio ritornare alla funzione di successo solo se tutte le richieste di cambio stato sono andate a buon fine
            if (this.numeroRichiesteDaEliminare === 0) {
                deferDeleteRequest.resolve(ris);
            };
        };
        var fError = function (err) {
            deferDeleteRequest.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteRequest(idReq, state, fSuccess, fError);
        return deferDeleteRequest.promise;
    },

    deleteRequestsFromDraft: function (periodo, codBu) {
        var deferDelete = Q.defer();

        var fSuccess = function (ris) {
            deferDelete.resolve(ris);
        };
        var fError = function (err) {
            deferDelete.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.deleteSchedulingPeriodsUM(periodo, codBu, fSuccess, fError);
        return deferDelete.promise;
    },

    readRequests: function (periodo, codBu) {
        var deferRead = Q.defer();

        var fSuccess = function (ris) {
            deferRead.resolve(ris);
        };
        var fError = function (err) {
            deferRead.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readSchedulingPeriodsUM(periodo, codBu, fSuccess, fError);
        return deferRead.promise;
    },
    
    _checkJoinRichiesteDraftCommesse: function (periodo) {
        var deferCheck = Q.defer();

        var fSuccess = function (ris) {
            deferCheck.resolve(ris);
        };
        var fError = function (err) {
            deferCheck.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.checkJoinRichiesteDraftCommesse(periodo, fSuccess, fError);
        return deferCheck.promise;
    },

    writeRequests: function (requests) {
        /* chiamo in maniera ciclica il servizio per l'inserimento delle richieste e se almeno una non va a buon fine faccio tornare un errore */
        alert("FUNZIONE DISABILITATA, CONTATTARE L'AMMINISTRATORE DEL SISTEMA");
//        var response = [];
//        var deferGet = Q.defer();
//
//        var fSuccess = function (ris) {
//            /* ogni volta che un inserimento va a buon fine pusho la risposta dentro l'array. quindi torno al controller solo quando tutte le risposte
//            sono arrivate in maniera corretta */
//            if (ris.status.indexOf("OK") >= 0) {
//                deferGet.resolve(ris);
//            };
//        };
//        var fError = function (err) {
//            deferGet.reject(err);
//        };
//        fSuccess = _.bind(fSuccess, this);
//        fError = _.bind(fError, this);
//
//        var request = {};
//        request.results = requests;
//        model.odata.chiamateOdata.writeSchedulingPeriodsUM(request, fSuccess, fError);
//        return deferGet.promise;
    },

    updateNote: function (requests) {
        var deferNote = Q.defer();

        var fSuccess = function (ris) {
            deferNote.resolve(ris);
        };
        var fError = function (err) {
            deferNote.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var request = {};
        model.odata.chiamateOdata.updateNoteUM(requests, fSuccess, fError);
        return deferNote.promise;
    },

    readTableByBu: function (req) {
        var defer = Q.defer();
        this.richiesta = req;

        var fSuccess = function (result) {
            var requests = [];
            if (result.results) {
                result.results = model.persistence.Serializer.requestsDraftAllConsultants.fromSAPItems(result).items;
            }
            if (this.richiesta.selectedConsultantsCode) {
                for (var t = 0; t < this.richiesta.selectedConsultantsCode.length; t++) {
                    requests = requests.concat(_.where(result.results, {
                        cod_consulente: this.richiesta.selectedConsultantsCode[t].cod
                    }));
                }
            }

            var table = [];
            var arrConsulente = [];
            if (result && result.results) {
                var res3 = requests.length > 0 ? requests : result.results;
                var resConfirmed = _.where(res3, {
                    stato: 'CONFIRMED',
                    idPeriodoScheduling: this.richiesta.idPeriodo
                });
                var resStandBy = _.where(res3, {
                    stato: 'STANDBY',
                    idPeriodoScheduling: this.richiesta.idPeriodo
                });
                var resSchedulazioni = resConfirmed.concat(resStandBy);
                for (var i = 0; i < res3.length; i++) {

                    if (!_.find(arrConsulente, {
                            cod: res3[i].cod_consulente
                        })) {
                        if (res3[i].cod_consulente.indexOf("CCG") >= 0) {
                            res3[i].nomeConsulente = res3[i].skill;
                        }
                        arrConsulente.push({
                            cod: res3[i].cod_consulente,
                            nome: res3[i].nomeConsulente,
                            cod_consulenteBu: res3[i].cod_consulenteBu
                        });
                    }

//                    if (this.richiesta.idPeriodo === res3[i].idPeriodoScheduling && (res3[i].stato === "CONFIRMED" || res3[i].stato === "STANDBY")) {
                        var obj = {};
                        obj.nomeConsulente = res3[i].nomeConsulente;
                        obj.codiceConsulente = res3[i].cod_consulente;
                        var propNote = "nota" + res3[i].gg;
                        obj[propNote] = res3[i].note;
                        if (res3[i].gg) {
                            var prop = "gg" + res3[i].gg;
                            var propIcon = "ggIcon" + res3[i].gg;
                            var propConflict = "ggConflict" + res3[i].gg;
                            var propStato = "ggStato" + res3[i].gg;
                        }
                        if (res3[i].nomeCommessa) {
                            obj[prop] = res3[i].nomeCommessa;
                        }
                        if (res3[i].stato) {
                            obj[propStato] = res3[i].stato;
                        }
                        /* verifico se ci sono delle giornate con conflitti e in caso metto un'icona di avvertimento */
                        var richiesteContemporanee = _.where(resSchedulazioni, {
                            'gg': res3[i].gg,
                            'cod_consulente': res3[i].cod_consulente
                        });
                        var mezzeRichiesteContemporanee = _.where(richiesteContemporanee, {
                            'ggIcon': "halfDay"
                        });
                        var mezzeRichiesteFerieContemporanee = _.where(richiesteContemporanee, {
                            'ggIcon': "bed"
                        });
                        var nRichiesteContemporanee = richiesteContemporanee.length;
                        var nMezzeRichiesteContemporanee = mezzeRichiesteContemporanee.length + mezzeRichiesteFerieContemporanee.length;

                        if (nMezzeRichiesteContemporanee > 2 || (nRichiesteContemporanee > 1 && nRichiesteContemporanee !== nMezzeRichiesteContemporanee)) {
                            obj[propConflict] = true;
                        } else {
                            obj[propConflict] = false;
                        }

                        if (res3[i].ggIcon) {
                            obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                        }
                        if (res3[i].codicePJM) {
                            obj.codicePJM = res3[i].codicePJM;
                        }
                        if (res3[i].cod_consulenteBu) {
                            obj.cod_consulenteBu = res3[i].cod_consulenteBu;
                        }
                        if (res3[i].toCheck) {
                            var prop = "toCheck" + res3[i].gg;
                            obj[prop] = res3[i].toCheck;
                        }
                        if (res3[i].id_req) {
                            var prop = "req" + res3[i].gg;
                            obj[prop] = res3[i].id_req;
                        }
                        //**
                        table.push(obj);
//                    }
                }
                for (var t = 0; t < arrConsulente.length; t++) {
                    if (!_.find(table, {
                            codiceConsulente: arrConsulente[t].cod
                        })) {
                        table.push({
                            nomeConsulente: arrConsulente[t].nome,
                            codiceConsulente: arrConsulente[t].cod,
                            cod_consulenteBu: arrConsulente[t].cod_consulenteBu
                        });
                    }
                }
                var res = [];
                res.push(table);
                defer.resolve(res);
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getUmAllConsultantsByBu(req, fSuccess, fError);
        return defer.promise;
    },

    readTableByBuProject: function (req) {
        var defer = Q.defer();
        this.richiesta = req;

        var fSuccess = function (result) {
            var requests = [];
            if (result.results) {
                result.results = model.persistence.Serializer.requestsDraftAllConsultants.fromSAPItems(result).items;
            }
            if (this.richiesta.selectedConsultantsCode) {
                for (var t = 0; t < this.richiesta.selectedConsultantsCode.length; t++) {
                    requests = requests.concat(_.where(result.results, {
                        cod_consulente: this.richiesta.selectedConsultantsCode[t].cod
                    }));
                }
            }

            var table = [];
            var arrConsulente = [];
            if (result && result.results) {
                var res3 = requests.length > 0 ? requests : result.results;
                var resConfirmed = _.where(res3, {
                    stato: 'CONFIRMED',
                    idPeriodoScheduling: this.richiesta.idPeriodo
                });
                var resStandBy = _.where(res3, {
                    stato: 'STANDBY',
                    idPeriodoScheduling: this.richiesta.idPeriodo
                });
                var resSchedulazioni = resConfirmed.concat(resStandBy);
                for (var i = 0; i < res3.length; i++) {

                    if (!_.find(arrConsulente, {
                            cod: res3[i].cod_consulente
                        })) {
                        if (res3[i].cod_consulente.indexOf("CCG") >= 0) {
                            res3[i].nomeConsulente = res3[i].skill;
                        }
                        arrConsulente.push({
                            cod: res3[i].cod_consulente,
                            nome: res3[i].nomeConsulente,
                            cod_consulenteBu: res3[i].cod_consulenteBu
                        });
                    }

                    if (this.richiesta.idPeriodo === res3[i].idPeriodoScheduling && (res3[i].stato === "CONFIRMED" || res3[i].stato === "STANDBY")) {
                        var obj = {};
                        obj.nomeConsulente = res3[i].nomeConsulente;
                        obj.codiceConsulente = res3[i].cod_consulente;
                        var propNote = "nota" + res3[i].gg;
                        obj[propNote] = res3[i].note;
                        if (res3[i].gg) {
                            var prop = "gg" + res3[i].gg;
                            var propIcon = "ggIcon" + res3[i].gg;
                            var propConflict = "ggConflict" + res3[i].gg;
                            var propStato = "ggStato" + res3[i].gg;
                            var propReq = "ggReq" + res3[i].gg;
                        }
                        if (res3[i].nomeCommessa) {
                            obj[prop] = res3[i].nomeCommessa;
                        }
                        if (res3[i].stato) {
                            obj[propStato] = res3[i].stato;
                        }
                        /* verifico se ci sono delle giornate con conflitti e in caso metto un'icona di avvertimento */
                        var richiesteContemporanee = _.where(resSchedulazioni, {
                            'gg': res3[i].gg,
                            'cod_consulente': res3[i].cod_consulente
                        });
                        var mezzeRichiesteContemporanee = _.where(richiesteContemporanee, {
                            'ggIcon': "halfDay"
                        });
                        var mezzeRichiesteFerieContemporanee = _.where(richiesteContemporanee, {
                            'ggIcon': "bed"
                        });
                        var nRichiesteContemporanee = richiesteContemporanee.length;
                        var nMezzeRichiesteContemporanee = mezzeRichiesteContemporanee.length + mezzeRichiesteFerieContemporanee.length;

                        if (nMezzeRichiesteContemporanee > 2 || (nRichiesteContemporanee > 1 && nRichiesteContemporanee !== nMezzeRichiesteContemporanee)) {
                            obj[propConflict] = true;
                        } else {
                            obj[propConflict] = false;
                        }

                        if (res3[i].ggIcon) {
                            //                            if (res3[i].ggIcon === "halfDay") {
                            //                                obj[propIcon] = "sap-icon://time-entry-request";
                            //                            } else if (res3[i].ggIcon === "abroad") {
                            //                                obj[propIcon] = "sap-icon://globe";
                            //                            } else if (res3[i].ggIcon === "not-collaborate") {
                            //                                obj[propIcon] = "sap-icon://offsite-work";
                            //                            } else if (res3[i].ggIcon === "complete-holidays") {
                            //                                obj[propIcon] = "sap-icon://travel-itinerary";
                            //                            } else if (res3[i].ggIcon === "not-confirmed") {
                            //                                obj[propIcon] = "sap-icon://role";
                            //                            } else if (res3[i].ggIcon === "not-customer-and-contacts") {
                            //                                obj[propIcon] = "sap-icon://employee-rejections";
                            //                            } else {
                            //                                obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                            //                            }
                            obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                        }
                        if (res3[i].dataReqPeriodo) {
                            obj[propReq] = res3[i].dataReqPeriodo;
                        }
                        if (res3[i].codicePJM) {
                            obj.codicePJM = res3[i].codicePJM;
                        }
                        if (res3[i].cod_consulenteBu) {
                            obj.cod_consulenteBu = res3[i].cod_consulenteBu;
                        }
                        if (res3[i].toCheck) {
                            var prop = "toCheck" + res3[i].gg;
                            obj[prop] = res3[i].toCheck;
                        }
                        if (res3[i].id_req) {
                            var prop = "req" + res3[i].gg;
                            obj[prop] = res3[i].id_req;
                        }
                        //**
                        table.push(obj);
                    }
                }
                for (var t = 0; t < arrConsulente.length; t++) {
                    if (!_.find(table, {
                            codiceConsulente: arrConsulente[t].cod
                        })) {
                        table.push({
                            nomeConsulente: arrConsulente[t].nome,
                            codiceConsulente: arrConsulente[t].cod,
                            cod_consulenteBu: arrConsulente[t].cod_consulenteBu
                        });
                    }
                }
                var res = [];
                res.push(table);
                defer.resolve(res);
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getUmAllConsultantsByBuProject(req, fSuccess, fError);
        return defer.promise;
    },
    
    readSingoloConsulente: function (req) {
        var defer = Q.defer();
        this.richiesta = req;
//        this.staffCompleteList = staffCompleteList.items;

        var fSuccess = function (result) {
            var requests = [];
            if (result.results) {
                result.results = model.persistence.Serializer.requestsDraft.fromSAPItems(result).items;
            }
            var table = [];
            var arrConsulente = [];
            if (result && result.results) {
                var res3 = _.uniq(result.results, function (item) {
                    return item.id_req;
                });
                res3 = res3.sort(this.confronta.bind(this));
                var resConfirmed = _.where(res3, {
                    stato: 'CONFIRMED',
                    idPeriodoScheduling: this.richiesta.idPeriodo
                });
                var resStandBy = _.where(res3, {
                    stato: 'STANDBY',
                    idPeriodoScheduling: this.richiesta.idPeriodo
                });
                var resSchedulazioni = resConfirmed.concat(resStandBy);
                for (var i = 0; i < res3.length; i++) {
//                    if (res3[i].stato === "CONFIRMED" || res3[i].stato === "STANDBY") {
                        var obj = {};
                        obj.nomeConsulente = res3[i].nomeConsulente;
                        obj.codiceConsulente = res3[i].cod_consulente;
                        obj.nomeSkill = res3[i].nomeSkill;
                        var propNote = "nota" + res3[i].gg;
                        obj[propNote] = res3[i].note;
                        if (res3[i].gg) {
                            var prop = "gg" + res3[i].gg;
                            var propIcon = "ggIcon" + res3[i].gg;
                            var propConflict = "ggConflict" + res3[i].gg;
                            var propReq = "ggReq" + res3[i].gg;
                            var propStato = "ggStato" + res3[i].gg;
                        }
                        if (res3[i].dataReqPeriodo) {
                            obj[propReq] = res3[i].dataReqPeriodo;
                        }
                        if (res3[i].nomeCommessa) {
                            obj[prop] = res3[i].nomeCommessa;
                        }
                        if (res3[i].stato) {
                            obj[propStato] = res3[i].stato;
                        }
                        /* verifico se ci sono delle giornate con conflitti e in caso metto un'icona di avvertimento */
                        var richiesteContemporanee = _.where(resSchedulazioni, {
                            'gg': res3[i].gg,
                            'cod_consulente': res3[i].cod_consulente
                        });
                        var mezzeRichiesteContemporanee = _.where(richiesteContemporanee, {
                            'ggIcon': "halfDay"
                        });
                        var mezzeRichiesteFerieContemporanee = _.where(richiesteContemporanee, {
                            'ggIcon': "bed"
                        });
                        var nRichiesteContemporanee = richiesteContemporanee.length;
                        var nMezzeRichiesteContemporanee = mezzeRichiesteContemporanee.length + mezzeRichiesteFerieContemporanee.length;

                        if (nMezzeRichiesteContemporanee > 2 || (nRichiesteContemporanee > 1 && nRichiesteContemporanee !== nMezzeRichiesteContemporanee)) {
                            obj[propConflict] = true;
                        } else {
                            obj[propConflict] = false;
                        }

                        if (res3[i].ggIcon) {
                            obj[propIcon] = "sap-icon://" + res3[i].ggIcon;
                        }
                        if (res3[i].codicePJM) {
                            obj.codicePJM = res3[i].codicePJM;
                        }
                        if (res3[i].cod_consulenteBu) {
                            obj.cod_consulenteBu = res3[i].cod_consulenteBu;
                        }
                        if (res3[i].toCheck) {
                            var prop = "toCheck" + res3[i].gg;
                            obj[prop] = res3[i].toCheck;
                        }
                        if (res3[i].id_req) {
                            var prop = "req" + res3[i].gg;
                            obj[prop] = res3[i].id_req;
                            var propOld = "idReqOld" + res3[i].gg;
                            obj[propOld] = (res3[i].idReqOld !== "") ? parseInt(res3[i].idReqOld) : 0;
                        }
                        //**
                        table.push(obj);

//                    }
                }
                // aggiungo all'array finale tutti i consulenti non schedulati
//                for (var t = 0; t < this.staffCompleteList.length; t++) {
//                    if (!_.find(table, {
//                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff
//                        })) {
//                        table.push({
//                            nomeConsulente: this.staffCompleteList[t].codConsulenteStaff.indexOf("CCG") >= 0 ? this.staffCompleteList[t].descSkill : this.staffCompleteList[t].cognomeConsulente + " " + this.staffCompleteList[t].nomeConsulente,
//                            codiceConsulente: this.staffCompleteList[t].codConsulenteStaff,
//                            richiesteCancellate: 0
//                        });
//                    }
//                }
                var res = [];
                res.push(table);
                defer.resolve(res);
            }
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getDraftSingle(req, fSuccess, fError);
        return defer.promise;
    },
    
    // verifica se questa commessa per questo periodo è "in lavorazione da un altro UM" 
    checkIsToWork: function (req) {
        var defer = Q.defer();
        
        var fSuccess = function (res) {
            defer.resolve(res);
        };
        
        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.checkIsToWork(req, fSuccess, fError);
        return defer.promise;
    },
    
    setIsToWorkByUm: function (params) {
        var defer = Q.defer();
        
        var fSuccess = function (res) {
            defer.resolve(res);
        };
        
        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.setUmIsToWork(params, fSuccess, fError);
        return defer.promise;
    },
    
    getProjectListWithGenericConsultant: function (params) {
        var defer = Q.defer();
        
        var fSuccess = function (res) {
            defer.resolve(res);
        };
        
        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getProjectListWithGenericConsultant(params, fSuccess, fError);
        return defer.promise;
    }
    

};
