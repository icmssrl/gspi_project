if (!model.persistence.Serializer)
    jQuery.sap.require("model.persistence.Serializer");
if (!model.odata)
    jQuery.sap.require("model.odata.chiamateOdata");

model.StaffCommesseView = {
    // funzione che ritorna gli staff delle commesse
    readStaffCommessa: function () {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.staffCommessaList = model.persistence.Serializer.readStaffCommesseView.fromSAPItems(result);
            defer.resolve(this.staffCommessaList);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readStaffCommessaView(fSuccess, fError);
        return defer.promise;
    },
    
    // funzione che ritorna lo staff della singola commessa
    readStaffSingolaCommessa: function (nomeCommessa) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.staffCommessaList = model.persistence.Serializer.readStaffSingolaCommesseView.fromSAPItems(result);
            defer.resolve(this.staffCommessaList);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readStaffSingolaCommessaView(nomeCommessa, fSuccess, fError);
        return defer.promise;
    },
    
    readStaffComplete: function (filtri) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.staffCompleteList = model.persistence.Serializer.readStaffCompleteView.fromSAPItems(result);
            defer.resolve(this.staffCompleteList);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.readStaffCompleteView(fSuccess, fError, filtri);
        return defer.promise;
    },
    
    getCommesseForCurrentUser: function (idConsulente) {
        var defer = Q.defer();
        var items = [];
        var singleItem = {};
        var fSuccess = function (result) {
            if(result && result.results && result.results.length>0){
                for(var i= 0; i<result.results.length; i++){
                    singleItem = model.persistence.Serializer.readStaffCompleteView.fromSAP(result.results[i]);
                    items.push(singleItem);
                }
                
            }
            defer.resolve(items);
            console.log(items);
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getAnagStaffCommesseView(idConsulente, fSuccess, fError);
        return defer.promise;
    },
    
    getStaffByPJMCode: function (pjm) {
        var defer = Q.defer();
        var items = [];
        var singleItem = {};
        var fSuccess = function (result) {
            if(result && result.results && result.results.length>0){
                for(var i= 0; i<result.results.length; i++){
                    singleItem = model.persistence.Serializer.simpleConsultant.fromSAP(result.results[i]);
                    items.push(singleItem);
                }
                
            }
            defer.resolve(items);
            console.log(items);
            
        };

        var fError = function (err) {
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getStaffByPJMCode(pjm, fSuccess, fError);
        return defer.promise;
    },

    readNoStaffProject: function (filtri) {
        var defer = Q.defer();
        var that = this;
        that.filtri = filtri;
        var fSuccess = function (result) {
            this.commesseNoStaff = model.persistence.Serializer.readNoStaffProject.fromSAPItems(result);
            defer.resolve(this.commesseNoStaff);
        };

        var fError = function (err) {
            if (!this.oneTime) {
                this.oneTime = true;
                model.odata.chiamateOdata.chiamataForLoginToHana()
                    .then(_.bind(that.readNoStaffProject, that, that.filtri));
            }
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.getNoStaffProject(fSuccess, fError, that.filtri);
        return defer.promise;
    },

    addStaffToCommessa: function (codCommessa) {
        this._defer = Q.defer();

        var fSuccess = function (result) {
            if (JSON.parse(result).message === "updateOK") {
                this._defer.resolve(result);
            } else {
                this._defer.reject(result);
            }
        };

        var fError = function (err) {
            this._defer.reject(err);
        }

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.addStaffToCommessa(codCommessa, fSuccess, fError);
        return this._defer.promise;
    },
    
    deleteConsultantFromStaff: function( codStaff, codConsulenteStaff, codCommessa, codPJMStaff, fSuccess, fError ){
        this._defer = Q.defer();
        
        var fSuccess = function(result){
            this._defer.resolve(result);
        };
        
        var fError = function(err){
            this._defer.reject(err);
        };
        
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.deleteConsultantFromStaff(codStaff, codConsulenteStaff, codCommessa, codPJMStaff, fSuccess, fError);
        
        return this._defer.promise;
    },
    
    readAllConsultantsFilters: function(fSuccess, fError, filtri){
        var defer = Q.defer();
        
        var fSuccess = function(result){
            this.consulentiSkill = model.persistence.Serializer.consulenteSkill.fromSAPItems(result);
            defer.resolve(this.consulentiSkill);
            console.log(this.consulentiSkill);
        };
        
        var fError = function(err){
            defer.reject(err);
        };
        
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.readAllConsulenteSkill(fSuccess, fError, filtri);
        
        return defer.promise;
    },
    
    updateTeamLeaderStaff: function(teamLeader, codSkill, idStaff, fSuccess, fError){
    	var defer = Q.defer();
    	
    	var fSuccess = function(result){
    		defer.resolve(result);
    	};
    	
    	var fError = function(err){
    		defer.reject(err);
    	};
    	
    	fSuccess = _.bind(fSuccess, this);
    	fError = _.bind(fError, this);
    	
    	model.odata.chiamateOdata.updateTeamLeaderStaff(teamLeader, codSkill, idStaff, fSuccess, fError);
    	
    	return defer.promise;
    }
};
