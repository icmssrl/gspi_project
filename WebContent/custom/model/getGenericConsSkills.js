if (!model.persistence.Serializer)
    jQuery.sap.require("model.persistence.Serializer");
if (!model.odata)
    jQuery.sap.require("model.odata.chiamateOdata");

model.getGenericConsSkills = {

    getGenericConsSkills: function () {
        var defer = Q.defer();

        var fSuccess = function (result) {
            genericConsSkill = model.persistence.Serializer.listaConsGenSkill.fromSAPItems(result);
            defer.resolve(genericConsSkill);
        }

        var fError = function (err) {
            genericConsSkill = [];
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.odata.chiamateOdata.getGenConsultantSkill(fSuccess, fError);
        
        return defer.promise;
    }
};