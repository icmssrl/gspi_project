jQuery.sap.declare("utils.exportCsv");
jQuery.sap.require("sap.ui.core.util.Export");
jQuery.sap.require("sap.ui.core.util.ExportTypeCSV");

utils.exportCsv = {
    exportToExcel: function (tableId, oModel, nameFile) {
        var cols = tableId.getColumns();
        var items = tableId.getItems();
        var cellId = null;
        var cellObj = null;
        var cellVal = null;
        var headerColId = null;
        var headerColObj = null;
        var headerColVal = null;
        var column = null;
        var json = {};
        var colArray = [];
        var itemsArray = [];
        //push header column names to array
        for (var j = 0; j < cols.length; j++) {
            column = "";
            column = cols[j];
            headerColId = column.getAggregation("header").getId();
            //            headerColId = column.getId().split("--")[1]
            headerColObj = sap.ui.getCore().byId(headerColId);
            headerColVal = headerColObj.getText();
            if (headerColObj.getVisible()) {
                json = {
                    name: headerColVal
                };
                colArray.push(json);
            }
        }
        itemsArray.push(colArray);
        //push table cell values to array
        for (i = 0; i < items.length; i++) {
            colArray = [];
            cellId = "";
            cellObj = "";
            cellVal = "";
            headerColId = null;
            headerColObj = null;
            headerColVal = null;
            var item = items[i];
            for (var j = 0; j < cols.length; j++) {
                cellId = item.getAggregation("cells")[j].getId();
                cellObj = sap.ui.getCore().byId(cellId);
                if (cellObj.getVisible()) {
                    if (cellObj instanceof sap.m.Text || cellObj instanceof sap.m.Label || cellObj instanceof sap.m.Link)
                        cellVal = cellObj.getText();
                    if (cellObj instanceof sap.m.ObjectNumber) {
                        var k = cellObj.getUnit();
                        cellVal = cellObj.getNumber() + " " + k;
                    }
                    if (cellObj instanceof sap.m.ObjectIdentifier) {
                        var objectIdentifierVal = "";
                        if (cellObj.getTitle() != undefined && cellObj.getTitle() != "" && cellObj.getTitle() != null)
                            objectIdentifierVal = cellObj.getTitle();
                        if (cellObj.getText() != undefined && cellObj.getText() != "" && cellObj.getText() != null)
                            objectIdentifierVal = objectIdentifierVal + " " + cellObj.getText();
                        cellVal = objectIdentifierVal;
                    }
                    if (cellObj instanceof sap.ui.core.Icon) {
                        if (cellObj.getTooltip() != undefined && cellObj.getTooltip() != "" && cellObj.getTooltip() != null)
                            cellVal = cellObj.getTooltip();
                    };
                    if (j == 0) {
                        json = {
                            name: "\r" + cellVal
                        };
                    } else {
                        json = {
                            name: cellVal
                        };
                    };
                    colArray.push(json);
                };
            };
            itemsArray.push(colArray);
        };
        if (sessionStorage.getItem("cid") === "99000019" || sessionStorage.getItem("cid") === "11000133") {
            var separatorChar = ",";
        } else {
            var separatorChar = ";";
        };
        //export json array to csv file
        var oExport = new sap.ui.core.util.Export({
            // Type that will be used to generate the content. Own ExportType's can be created to support other formats
            exportType: new sap.ui.core.util.ExportTypeCSV({
                separatorChar: separatorChar
            }),
            // Pass in the model created above
            models: oModel,
            // binding information for the rows aggregation
            rows: {
                path: "/"
            },
            // column definitions with column name and binding info for the content
            columns: [itemsArray]
        });
        oExport.saveFile(nameFile).always(function () {
            this.destroy();
        });
    }
};
