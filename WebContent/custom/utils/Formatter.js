jQuery.sap.declare("utils.Formatter");
//jQuery.sap.require("utils.ParseDate");

utils.Formatter = {

    nascondiBottone: function (val) {
        if (val) {
            return true;
        } else {
            return false;
        }
    },

    stateChange: function (state) {
        if (state === "SI") {
            state = true;
        } else {
            state = false;
        }
        return state;
    },

    stateChangeInv: function (state) {
        if (state === true) {
            state = "SI";
        } else {
            state = "NO";
        }
        return state;
    },

    periodStatus: function (stato) {
        if (stato === "APERTO") {
            stato = "A";
        } else {
            stato = "C";
        }
        return stato;
    },
    
    periodStatusByData: function (data) {
        var dataOdierna = new Date();
        dataOdierna.setHours(0);
        if (data > dataOdierna) {
            data = "A";
        } else {
            data = "C";
        }
        return data;
    },
    
    rilascioPjmScheduling: function (statoPJM, statoALL, data) {
        if (statoALL === "X") {
            stato = "RIL";
        } else if (statoALL === "" && statoPJM === "X") {
            stato = "DRF";
        } else {
            var dataOdierna = new Date();
            dataOdierna.setHours(0);
            if (data > dataOdierna) {
                stato = "TO-DO";
            } else {
                stato = "CHIUSO";
            }
        }
        return stato;
    },
    
    
    periodsAdminScheduling: function (statoPJM, statoALL, data) {
        if (statoALL === "X") {
            stato = "RIL";
        } else if (statoALL === "" && statoPJM === "X") {
            stato = "DRF";
        } else {
            var dataOdierna = new Date();
            dataOdierna.setHours(0);
            if (data > dataOdierna) {
                stato = "TO-DO";
            } else {
                stato = "WIP";
            }
        }
        return stato;
    },
    
    rilascioPjm: function (stato) {
        if (stato === "X") {
            stato = "RIL";
        } else {
            stato = "WIP";
        }
        return stato;
    },
    
    rilascioPjmDraft: function (statoPJM, statoALL) {
        if (statoALL === "X") {
            stato = "RIL";
        } else if (statoALL === "" && statoPJM === "X") {
            stato = "DRF";
        } else {
            stato = "WIP";
        }
        return stato;
    },
    
    rilascioAllConsultants: function (stato) {
        if (stato === "X") {
            stato = "RIL";
        } else {
            stato = "WIP";
        }
        return stato;
    },

    formatIconState: function (state) {
        if (state === "APERTO") {
            return ""
        } else if (state === "CHIUSO") {
            return "sap-icon://locked";
        }
    },

    isSelected: function (obj) {
        if (obj !== "")
            return true;
        return false;
    },

    nascondiIcona: function (val) {
        if (val) {
            if (val.indexOf("itinerary") >= 0 || val.indexOf("bed") >= 0) {
                return val;
            } else {
                return "";
            }
        }
    },

    nascondiTesto: function (val) {
        if (val && val !== "") {
            if (val.indexOf("feri") >= 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    },

    formatDate: function (d) {
        if (!!d) {
            return utils.ParseDate().formatDate(d, "dd-MM-yyyy");
        } else {
            return "";
        }
    },

    formatDateFerie: function (d) {
        if (!!d) {
            return utils.ParseDate().formatDate(d, "yyyy-MM-dd");
        } else {
            return "";
        }
    },

    coloraScrittaFerie: function (val) {
        if (val === "PROPOSED") {
            val = "proposedClass";
        } else if (val === "DELETED") {
            val = "deletedClass";
        } else if (val === "CONFIRMED") {
            val = "confirmedClass";
        } else {
            val = "confirmedClass";
        }
        return val;
    },

    formatDateVacation: function (d) {
        if (!!d) {
            return utils.ParseDate().formatDate(d, "dd/MM/yyyy");
        } else {
            return "";
        }
    },

    formatDateFromHana: function (data) {
        if (typeof (data) === "string") {
            var f = data.split("(");
            var ff = f[1].split(")")[0];
            return parseInt(ff);
        } else {
            return utils.ParseDate().formatDate(data, "dd-MM-yyyy");
        }
    },

    dateToHana: function (data) {
        if (typeof (data) === "number") {
            return new Date(data);
        } else {
            return data.split("-")[2] + "-" + data.split("-")[1] + "-" + data.split("-")[0];
        };
    },

    visibleScambio: function (idScambio) {
        if (idScambio !== "") {
            return true;
        } else {
            return false;
        }
    },

    traduzione: function (stato) {
        switch (stato) {
            case "PROPOSED":
                return "Proposta";
                break;
            case "CONFIRMED":
                return "Confermata";
                break;
            case "DELETED":
                return "Respinta";
                break;
            case "ALL":
                return "Tutti";
                break;
        }
    },

    statoRichiesta: function (stato) {
        switch (stato) {
            case "PROPOSED":
                return "None";
                break;
            case "CONFIRMED":
                return "Success";
                break;
            case "DELETED":
                return "Warning";
                break;
            case "ALL":
                return "None";
                break;
            default:
                return "None";
        }
    },

    visibleEditButtonStaff: function (valA, valB, profilo) {
        if (profilo !== "CP00000002") {
            if (valA) {
                if (valA === valB) {
                    return true;
                } else {
                    return false;

                }
            }
        } else {
            return false;
        }
    },

    invertDate: function (dateInput) {
        var strToReturn;
        if (!!dateInput && typeof dateInput === "string") {
            var str = dateInput.substr(0, 4);
            var isnum = /^\d+$/.test(str);
            if (isnum === true) {
                switch (dateInput.length) {
                    case 10:
                        strToReturn = dateInput.substring(8, 10) + "/" + dateInput.substring(5, 7) + "/" + dateInput.substring(0, 4);
                        break;

                    case 9:
                        strToReturn = "0" + dateInput.substring(8) + "/" + dateInput.substring(5, 7) + "/" + dateInput.substring(0, 4);
                        break;

                    default:
                        return dateInput;
                        break;
                }
                return strToReturn;
            } else {
                return dateInput;
            }
        }

    },

    colorIcon: function (icon, iconA) {
        if (iconA) {
            return "red";
        }
        switch (icon) {
            case "sap-icon://complete":
                return "mediumpurple";
            case "sap-icon://time-entry-request":
                return "orange";
            case "sap-icon://customer-and-contacts":
                return "yellow";
            case "sap-icon://employee-rejections":
                return "chartreuse";
            case "sap-icon://collaborate":
                return "red";
//            case "sap-icon://offsite-work":
//                return "lightgrey";
            case "sap-icon://role":
                return "dodgerblue";
            case "sap-icon://globe":
                return "lightskyblue";
            case "sap-icon://travel-itinerary":
                return "powderblue";
            case "sap-icon://bed":
                return "lightcyan";
            case "sap-icon://warning2":
                return "red";
            default:
                return "white";
        }
    },
    
    colorIconUM: function (icon, iconA, iconAA) {
        if (iconAA && iconA !== "sap-icon://offsite-work") {
            return "red";
        }
        if (iconA) {
//            if (icon === "sap-icon://offsite-work" || icon === "sap-icon://bed" ) {
            if (icon === "sap-icon://offsite-work") {
                icon = iconA;
            } else if (icon === "sap-icon://time-entry-request" && iconA === "sap-icon://time-entry-request") {
                return "orange";
            } else if (icon !== "sap-icon://offsite-work" && iconA !== "sap-icon://offsite-work") {
                return "red";
            }
        }
        switch (icon) {
            case "sap-icon://complete":
                return "mediumpurple";
            case "sap-icon://time-entry-request":
                return "orange";
            case "sap-icon://customer-and-contacts":
                return "yellow";
            case "sap-icon://employee-rejections":
                return "chartreuse";
            case "sap-icon://collaborate":
                return "red";
            case "sap-icon://role":
                return "dodgerblue";
            case "sap-icon://globe":
                return "lightskyblue";
            case "sap-icon://travel-itinerary":
                return "powderblue";
            case "sap-icon://bed":
                return "lightcyan";
            case "sap-icon://warning2":
                return "red";
            default:
                return "white";
        }
    },
    
    colorIconPJM: function (icon, iconA) {
        if (iconA) {
            if (icon === "sap-icon://offsite-work") {
                icon = iconA;
            }
        }
        if (icon === "sap-icon://bed" && iconA) {
            icon = iconA;
        }
        switch (icon) {
            case "sap-icon://complete":
                return "mediumpurple";
            case "sap-icon://time-entry-request":
                return "orange";
            case "sap-icon://customer-and-contacts":
                return "yellow";
            case "sap-icon://employee-rejections":
                return "chartreuse";
            case "sap-icon://collaborate":
                return "red";
//            case "sap-icon://offsite-work":
//                return "lightgrey";
            case "sap-icon://role":
                return "dodgerblue";
            case "sap-icon://globe":
                return "lightskyblue";
            case "sap-icon://travel-itinerary":
                return "powderblue";
            case "sap-icon://bed":
                return "lightcyan";
            case "sap-icon://warning2":
                return "red";
            default:
                return "white";
        }
    },
    
    coloraScrittaModifica: function (value) {
        if(value && typeof(value) !== "number") {
            if(value.indexOf("M") !== -1) {
                return "red";
            } else {
                return '2';
            }
        } else {
            return '2';
        }
    },

};
