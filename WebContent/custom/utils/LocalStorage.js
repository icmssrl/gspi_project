jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("model.userInfo");
jQuery.sap.declare("utils.LocalStorage");


utils.LocalStorage = {
    _cid: undefined,
    _localStorage: undefined,

    _createPath: function (entitySet) {
        if (!this._localStorage) {
            this._localStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
        };
        this._cid = model.userInfo.getCID();
        var itemPath = entitySet + "_" + this._cid;
        return itemPath;
    },
    
    getItem: function (entitySet) {
        var itemPath = this._createPath(entitySet);
        return this._localStorage.get(itemPath);
    },
    
    setItem: function (entitySet, value) {
        var itemPath = this._createPath(entitySet);
        return this._localStorage.put(itemPath, value);
    },
    
    removeItem: function (entitySet) {
        var itemPath = this._createPath(entitySet);
        return this._localStorage.remove(itemPath);
    }
};
