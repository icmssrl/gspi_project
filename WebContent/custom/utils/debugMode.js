jQuery.sap.declare("utils.debugMode");

utils.debugMode = {
    console: function (testo, type) {
        var debugMode = icms.Component.getMetadata().getConfig().settings.debug;
        if (debugMode) {
            if (!this.fisrtTimeDebug) {
                this.fisrtTimeDebug = true;
                console.clear();
            }
            if (type) {
                var css = "font-size:12px; font-weight:bold; ";
                switch (type) {
                    case "success":
                        css=css + "color:#00FF00";
                        break;
                    case "error":
                        css=css + "color:#FF0000";
                        break;
                    case "confirm":
                        css=css + "color:#0000FF";
                        break;
                    case "cancel":
                        css=css + "color:#000000";
                        break;
                    default:
                        css=css + "color:#000000";
                        break;
                }
                console.log("%c" + testo, css);
            } else {
                console.log(testo);
            }
        } else {

        }
    }

}
