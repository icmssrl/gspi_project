jQuery.sap.declare("utils.Util");
jQuery.sap.require("sap.ui.core.format.DateFormat");

utils.Util = {

    stringToDate: function (sDateGGMMAAAA) {
        var objData = new Date();
        objData.setFullYear(sDateGGMMAAAA.substr(6));
        var month = parseInt(sDateGGMMAAAA.substr(3, 2)) - 1;
        objData.setMonth(month);
        if (objData.getMonth() !== month) {
            objData.setMonth(month);
        };
        objData.setDate(sDateGGMMAAAA.substr(0, 2));
        return objData;
    },

    compareDate: function (date1, date2) {
        date1 = new Date(date1);
        date2 = new Date(date2);
        if (date1.getFullYear() !== date2.getFullYear())
            return false;
        if (date1.getMonth() !== date2.getMonth())
            return false;
        if (date1.getDate() !== date2.getDate())
            return false;
        else {
            return true;
        };
    },

    groupByMulti: function (array, props, context) {
        if (!_.groupByMulti) {
            _.mixin({
                groupByMulti: function (obj, values, context) {
                    if (!values.length)
                        return obj;
                    var byFirst = _.groupBy(obj, values[0]),
                        rest = values.slice(1);
                    for (var prop in byFirst) {
                        byFirst[prop] = _.groupByMulti(byFirst[prop], rest, context);
                    };
                    return byFirst;
                }
            });
        };
        return _.groupByMulti(array, props, context);
    },

    formatDateToTable: function (date) {
        var d = new Date(date);
        var day = d.getDate();
        if (day <= 9)
            day = "0" + day;
        var month = d.getMonth() + 1;
        if (month <= 9)
            month = "0" + month;
        var year = d.getFullYear();
        return day + "/" + month + "/" + year;
    },

    formatDate: function (date) {
        var d = date;
        var gg = d.getDate();
        var mm = d.getMonth() + 1;
        var yyyy = d.getFullYear();
        if (gg < 10) {
            gg = "0" + gg;
        };
        if (mm < 10) {
            mm = "0" + mm;
        };
        return gg + "/" + mm + "/" + yyyy;
    },

    dateToString: function (date, dataX) {
        var selectedDate = new Date(date);

        //manipolazione per giorno e mese
        var day = selectedDate.getDate();
        if (day < 10) {
            day = "0" + day;
        };
        var month = selectedDate.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        };
        var year = selectedDate.getFullYear();

        if (dataX === "inDate") {
            return selectedDate = year + "" + month + "" + day;
        };
        return selectedDate = day + "/" + month + "/" + year;
        //fine manipolazione
    },

    getHourFromMs: function (millisecondi) {
        var newDate = new Date(millisecondi.ms);
        var timeZoneOffset = newDate.getTimezoneOffset() / 60;
        var ore = newDate.getHours() + timeZoneOffset;
        if (ore === -1) {
            ore = 23;
        };
        if (ore < 10) {
            ore = "0" + ore;
        };
        var minuti = newDate.getMinutes();
        if (minuti < 10) {
            minuti = "0" + minuti;
        };
        var orario = ore.toString() + ":" + minuti.toString() + ":00";
        return orario;
    },

    getLastDayFromMonth: function (year, month) {
        var data = new Date(year, month, 0);
        if (sap.ui.core.format) {
            var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
                pattern: "yyyy-MM-ddThh:mm:ss.mmm"
            });
            return oDateFormat.format(data);
        } else {
            var anno = data.getFullYear().toString();
            var mese = (data.getMonth() + 1).toString();
            if (mese.length < 2) {
                mese = "0" + mese;
            };
            var giorno = data.getDate().toString();
            if (giorno.length < 2) {
                giorno = "0" + giorno;
            };
            var oDateFormat = anno + "-" + mese + "-" + giorno + "T12:00:00.000";
            return oDateFormat;
        };
    },

    getDateToSap: function (year, month, day) {
        var data = new Date(year, month, day);
        var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
            pattern: "yyyy-MM-ddThh:mm:ss.mmm"
        });
        return oDateFormat.format(data);
    },

    getLastEditDateToSap: function () {
        var d = new Date();
        var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
            pattern: "yyyy-MM-ddThh:mm:ss.mmm"
        });
        return oDateFormat.format(d);
    },

    setDateToSap: function (date) {
        var day = parseInt(date.split("/")[0]);
        var month = parseInt(date.split("/")[1]) - 1;
        var year = parseInt(date.split("/")[2]);
        var d = new Date();
        d.setDate(1);
        d.setYear(year);
        d.setMonth(month);
        d.setDate(day);
        var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
            pattern: "yyyy-MM-ddThh:mm:ss.mmm"
        });
        return oDateFormat.format(d);
    },
    
    setDateToSapInverted: function (date) {
        var day = parseInt(date.split("/")[2]);
        var month = parseInt(date.split("/")[1]) - 1;
        var year = parseInt(date.split("/")[0]);
        var d = new Date();
        d.setDate(1);
        d.setYear(year);
        d.setMonth(month);
        d.setDate(day);
        var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
            pattern: "yyyy-MM-ddThh:mm:ss.mmm"
        });
        return oDateFormat.format(d);
    },

    getFirstDayFromMonth: function (year, month) {
        var data = new Date(year, month, 01);
        if (sap.ui.core.format) {
            var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
                pattern: "yyyy-MM-ddThh:mm:ss.mmm"
            });
            return oDateFormat.format(data);
        } else {
            var anno = data.getFullYear().toString();
            var mese = (data.getMonth() + 1).toString();
            if (mese.length < 2) {
                mese = "0" + mese;
            };
            var giorno = data.getDate().toString();
            if (giorno.length < 2) {
                giorno = "0" + giorno;
            };
            var oDateFormat = anno + "-" + mese + "-" + giorno + "T12:00:00.000";
            return oDateFormat;
        };
    },

    getHoursToSap: function (ore) {
        var orario = ore.split(":");
        var orarioToSap = "PT" + orario[0] + "H" + orario[1] + "M" + orario[2] + "S";
        return orarioToSap;
    },

    /* Funzione che mi restituisce il mese e l'ultimo giorno del mese in stringa  */
    _getMeseAttuale: function (meseAttuale, annoAttuale) {
        var result = {
            'mese': "",
            'giorno': ""
        };
        switch (meseAttuale) {
            case "Gennaio":
            case "January":
            case "gen":
            case "Jan":
                meseAttuale = "01";
                var giorniMax = "31";
                result = {
                    'mese': meseAttuale,
                    'giorno': giorniMax
                };
                break;
            case "Febbraio":
            case "February":
            case "feb":
            case "Feb":
                meseAttuale = "02";
                if (annoAttuale % 4 === 0) {
                    var giorniMax = "29";
                } else {
                    var giorniMax = "28";
                };
                result = {
                    'mese': meseAttuale,
                    'giorno': giorniMax
                };
                break;
            case "Marzo":
            case "March":
            case "mar":
            case "Mar":
                meseAttuale = "03";
                var giorniMax = "31";
                result = {
                    'mese': meseAttuale,
                    'giorno': giorniMax
                };
                break;
            case "Aprile":
            case "April":
            case "apr":
            case "Apr":
                meseAttuale = "04";
                var giorniMax = "30";
                result = {
                    'mese': meseAttuale,
                    'giorno': giorniMax
                };
                break;
            case "Maggio":
            case "May":
            case "mag":
            case "May":
                meseAttuale = "05";
                var giorniMax = "31";
                result = {
                    'mese': meseAttuale,
                    'giorno': giorniMax
                };
                break;
            case "Giugno":
            case "June":
            case "giu":
            case "Jun":
                meseAttuale = "06";
                var giorniMax = "30";
                result = {
                    'mese': meseAttuale,
                    'giorno': giorniMax
                };
                break;
            case "Luglio":
            case "July":
            case "lug":
            case "Jul":
                meseAttuale = "07";
                var giorniMax = "31";
                result = {
                    'mese': meseAttuale,
                    'giorno': giorniMax
                };
                break;
            case "Agosto":
            case "August":
            case "ago":
            case "Aug":
                meseAttuale = "08";
                var giorniMax = "31";
                result = {
                    'mese': meseAttuale,
                    'giorno': giorniMax
                };
                break;
            case "Settembre":
            case "September":
            case "set":
            case "Sep":
                meseAttuale = "09";
                var giorniMax = "30";
                result = {
                    'mese': meseAttuale,
                    'giorno': giorniMax
                };
                break;
            case "Ottobre":
            case "October":
            case "ott":
            case "Oct":
                meseAttuale = "10";
                var giorniMax = "31";
                result = {
                    'mese': meseAttuale,
                    'giorno': giorniMax
                };
                break;
            case "Novembre":
            case "November":
            case "Nov":
                meseAttuale = "11";
                var giorniMax = "30";
                result = {
                    'mese': meseAttuale,
                    'giorno': giorniMax
                };
                break;
            case "Dicembre":
            case "December":
            case "dic":
            case "Dec":
                meseAttuale = "12";
                var giorniMax = "31";
                result = {
                    'mese': meseAttuale,
                    'giorno': giorniMax
                };
                break;
        };
        return result;
    }
};
