jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.i18n");
jQuery.sap.require("utils.LocalStorage");

view.abstract.AbstractController.extend("view.SettingsWBSModify", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.WBSModifyModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.WBSModifyModel, "WBSModifyModel");

        this.selectModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.selectModel, "selectModel");

        this.commesseModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.commesseModel, "commesseModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "settingsWBSModify") {
            return;
        }

        this.oDialog = this.getView().byId("idWBSModify");
        this.state = evt.getParameters().arguments.state;
        if (this.state === "new") {
            this.WBSModifyModel.setProperty("/codCommessaHana", "");
            this.WBSModifyModel.setProperty("/codCommessaSap", "");
            this.WBSModifyModel.setProperty("/codAttivitaSap", "");
            this.WBSModifyModel.setProperty("/luogoLavoro", "");
            this.WBSModifyModel.setProperty("/tipoOrario", "");
            this.WBSModifyModel.setProperty("/descCommessa", "");
            this.WBSModifyModel.setProperty("/noteCommessa", "");
            this.WBSModifyModel.setProperty("/fatturabile", "NO");
//        } else {
            //            var WBSSelecet = JSON.parse(sessionStorage.getItem("selectedWBS"));
            //            this.WBSModifyModel.setData(WBSSelecet);
        }

        this.cid = sessionStorage.getItem("cid");
        this.codConsulente = JSON.parse(sessionStorage.getItem("GSPI_User_Logged")).codice_consulente;
        this._uploadCommesseHana();
    },

    checkFatturabile: function (value) {
        if (value && (value.toUpperCase()) === "SI") {
            return true;
        } else {
            return false;
        }
    },

    _uploadCommesseHana: function () {
        var fSuccess = function (result) {
            this.selectModel.setProperty("/commesseHANA", result[0].items);
            this._uploadWBS(this.cid);
        };

        var fError = function (err) {
            if (!this.oneTime) {
                this.oneTime = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(this._uploadCommesseHana(), this));
            }
            this.oDialog.setBusy(false);
            this.console(JSON.parse(err.response.body).error.message.value, "error");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if(!this.oDialog.getBusy()) {
            this.oDialog.setBusy(true);
        }
        model.WBS.readCommesseHana(this.codConsulente)
            .then(fSuccess, fError);
    },

    _uploadWBS: function (cid) {
        var fSuccess = function (ris) {
            console.log("odata ProjectSet OK");
            var projectSet = undefined;
            var projectSetOdata = ris[0].items;
            console.log(ris[0].items);
            var arrayCommesse = [];

            projectSet = projectSetOdata;

            for (var i = 0; i < projectSetOdata.length; i++) {
                var commessaTemp = {};
                commessaTemp.commessa = projectSetOdata[i].commessa;
                commessaTemp.colore = "";
                commessaTemp.disabilitato = projectSetOdata[i].disabilitato;
                arrayCommesse.push(commessaTemp);
            }
            var arrayCommesseUniq = _.uniq(arrayCommesse, 'commessa');
            arrayCommesseUniq = _.sortBy(arrayCommesseUniq, {
                'disabilitato': "X"
            });

            var commessaColorSet = arrayCommesseUniq;

            utils.LocalStorage.setItem("commessaColorSet", undefined);
            utils.LocalStorage.setItem("commessaColorSet", commessaColorSet);

            utils.LocalStorage.setItem("projectSet", undefined);
            if (!utils.LocalStorage.setItem("projectSet", projectSet)) {
                fError(this._getLocaleText("ErroreProgetti"));
            } else {
                this.selectModel.setProperty("/commesseSAP", projectSet);
                this._uploadAttivita(cid);
            }
        };
        var fError = function (err) {
            this.oDialog.setBusy(false);
            this.console(JSON.parse(err.response.body).error.message.value, "error");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if(!this.oDialog.getBusy()) {
            this.oDialog.setBusy(true);
        }

        model.WBS.readWBS(cid)
            .then(fSuccess, fError);
    },

    _uploadAttivita: function (cid) {
        var fSuccess = function (ris) {
            console.log("odata WBSSet OK");
            var commessaSetOdata = ris;
            var commessaSetLS = utils.LocalStorage.getItem("commessaSet");
            commessaSet = commessaSetOdata;
            utils.LocalStorage.setItem("commessaSet", undefined);
            utils.LocalStorage.setItem("commessaSet", commessaSet);

            //            this.oDialog.setBusy(false);
            this._uploadLuogoLavoro();
        };

        var fError = function (err) {
            this.oDialog.setBusy(false);
            this.console(JSON.parse(err.response.body).error.message.value, "error");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if(!this.oDialog.getBusy()) {
            this.oDialog.setBusy(true);
        }
        
        model.WBS.getCommessa(cid)
            .then(fSuccess, fError);
    },

    _uploadLuogoLavoro: function () {
        var fSuccess = function (ris) {
            console.log("odata LUOGHI LAVORO OK");
            var result = ris[0].items;
            _.remove(result, {
                Value: ''
            });
            this.selectModel.setProperty("/luoghi", result);

            this._uploadTipoOrario();
        };

        var fError = function (err) {
            this.oDialog.setBusy(false);
            this.console(JSON.parse(err.response.body).error.message.value, "error");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if(!this.oDialog.getBusy()) {
            this.oDialog.setBusy(true);
        }
        
        model.WBS.getMenuTendina("ZZWORKLOC")
            .then(fSuccess, fError);
    },

    _uploadTipoOrario: function () {
        var fSuccess = function (ris) {
            console.log("odata TIPO ORARIO OK");
            var result = ris[0].items;
            this.selectModel.setProperty("/tipiOrario", result);

            this.oDialog.setBusy(false);
            if (this.state !== "new") {
                var WBSSelecet = JSON.parse(sessionStorage.getItem("selectedWBS"));
                this.WBSModifyModel.setData(WBSSelecet);
                
                var comboBoxAttivitaSap = this.getView().byId("attivitaSAP");
                comboBoxAttivitaSap.setValue(WBSSelecet.descAttivitaSap);
                var comboBoxLuogoLavoro = this.getView().byId("luogoLavoro");
                comboBoxLuogoLavoro.setValue(WBSSelecet.descLuogoLavoro);
            }
        };

        var fError = function (err) {
            this.oDialog.setBusy(false);
            this.console(JSON.parse(err.response.body).error.message.value, "error");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if(!this.oDialog.getBusy()) {
            this.oDialog.setBusy(true);
        }
        
        model.WBS.getMenuTendina("ZZWORK_TYPE")
            .then(fSuccess, fError);
    },

    onSelectCommessaHana: function (evt) {
        var code = evt.getSource().getSelectedKey();
        var text = evt.getSource().getValue();
        console.log(code);
        console.log(text);
        this.WBSModifyModel.setProperty("/codCommessaHana", code);
        this.WBSModifyModel.setProperty("/nomeCommessaHana", text);
    },

    onSelectCommessaSap: function (evt) {
        var commessaScelta = evt.getSource().getSelectedKey();
        var commessaSceltaNome = evt.getSource().getValue();
        console.log(commessaScelta);
        console.log(commessaSceltaNome);

        this.WBSModifyModel.setProperty("/nomeCommessaSap", commessaSceltaNome);
        this.WBSModifyModel.setProperty("/codCommessaSap", commessaScelta);

        var commessaSet = utils.LocalStorage.getItem("commessaSet")[0].items;
        var attivitaList = [];
        for (var i = 0; i < commessaSet.length; i++) {
            var attivita = {
                'commessaKey': commessaSet[i].commessa,
                'attivitaKey': commessaSet[i].elementoWBSID,
                'name': commessaSet[i].elementoWBS.substring(11) + " - " + commessaSet[i].descrizione,
                'disabilitato': commessaSet[i].disabilitato
            };
            attivitaList.push(attivita);
        }
        var attivitaListUnificate = [];
        attivitaListUnificate = _.where(attivitaList, {
            'commessaKey': commessaScelta
        });
        _.remove(attivitaListUnificate, {
            'disabilitato': "X"
        });

        this.WBSModifyModel.setProperty("/nomeAttivitaSap", "");
        this.selectModel.setProperty("/attivitaSAP", attivitaListUnificate);
    },

    onSelectAttivitaSap: function (evt) {
        var code = evt.getSource().getSelectedKey();
        var text = evt.getSource().getValue();
        console.log(code);
        console.log(text);
        this.WBSModifyModel.setProperty("/codAttivitaSap", code);
        this.WBSModifyModel.setProperty("/nomeAttivitaSap", text);
    },

    onSelectLuogoLavoro: function (evt) {
        var code = evt.getSource().getSelectedKey();
        var text = evt.getSource().getValue();
        console.log(code);
        console.log(text);
        this.WBSModifyModel.setProperty("/luogoLavoro", code);
        this.WBSModifyModel.setProperty("/luogoLavoroDesc", text);
    },

    onSelectTipoOrario: function (evt) {
        var code = evt.getSource().getSelectedKey();
        var text = evt.getSource().getValue();
        console.log(code);
        console.log(text);
        this.WBSModifyModel.setProperty("/tipoOrario", code);
        this.WBSModifyModel.setProperty("/tipoOrarioDesc", text);
    },

    onChangeDescrizione: function (evt) {
        var text = evt.getSource().getValue();
        console.log(text);
        this.WBSModifyModel.setProperty("/descCommessa", text);
    },

    onChangeNote: function (evt) {
        var text = evt.getSource().getValue();
        console.log(text);
        this.WBSModifyModel.setProperty("/noteCommessa", text);
    },

    onChangeFatturabile: function (evt) {
        var value = evt.getSource().getSelected();
        var val = "";
        if (value === true) {
            val = "SI";
        } else {
            val = "NO";
        }
        console.log(val);
        this.WBSModifyModel.setProperty("/fatturabile", val);
    },

    cancelWBSPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_CANCEL_WBS"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_CANCEL_TITLE_WBS"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmCancelPress, this)
        );
    },

    _confirmCancelPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            this.router.navTo("settingsWBSList");
        }
    },

    saveWBSPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_SAVE_WBS"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_SAVE_TITLE_WBS"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmSavePress, this)
        );
    },

    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var wbs = {};
            wbs.cidCosnuslenteSap = this.cid;
            wbs.codConsulenteHana = this.codConsulente;
            wbs.codCommessaHana = this.WBSModifyModel.getProperty("/codCommessaHana");
            wbs.codCommessaSap = this.WBSModifyModel.getProperty("/codCommessaSap");
            wbs.codAttivitaSap = this.WBSModifyModel.getProperty("/codAttivitaSap");
            wbs.descCommessaHana = this.WBSModifyModel.getProperty("/nomeCommessaHana");
            wbs.descCommessaSap = this.WBSModifyModel.getProperty("/nomeCommessaSap");
            wbs.descAttivitaSap = this.WBSModifyModel.getProperty("/nomeAttivitaSap");
            wbs.codLuogoLavoro = this.WBSModifyModel.getProperty("/luogoLavoro");
            wbs.descLuogoLavoro = this.WBSModifyModel.getProperty("/luogoLavoroDesc");
            wbs.descCommessa = this.WBSModifyModel.getProperty("/descCommessa");
            wbs.noteCommessa = this.WBSModifyModel.getProperty("/noteCommessa");
            wbs.codTipoOrario = "";
            wbs.descTipoOrario = "Ordinario";
            wbs.fatturabile = this.WBSModifyModel.getProperty("/fatturabile");
            wbs.stato = "ATTIVO";
        }
        console.log(wbs);

        this._checkWbs(wbs);
    },

    _checkWbs: function (wbs) {
        var fSuccess = function (result) {
            var res = result[0].items;
            if (_.find(res, {
                    'codCommessaHana': wbs.codCommessaHana,
                    stato: 'ATTIVO'
                })) {
                this.oDialog.setBusy(false);
                sap.m.MessageToast.show(this._getLocaleText("IMPOSSIBILE_SOVRASCRIVERE"));
                return;
            } else {
                this._write(wbs);
            }
            this.oDialog.setBusy(false);
        };
        var fError = function (err) {
            this.oDialog.setBusy(false);
            this.console(JSON.parse(err.response.body).error.message.value, "error");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        var cid = sessionStorage.getItem("cid");
        var stato = "ATTIVO";
        model.WBS.readWbsSapHana(cid, stato)
            .then(fSuccess, fError);
    },

    _write: function (wbs) {
        var fSuccess = function (result) {
            sap.m.MessageToast.show(this._getLocaleText("SCRITTURA_WBS_OK"));
            jQuery.sap.delayedCall(1000, this, function () {
                this.router.navTo("settingsWBSList");
            });
            this.oDialog.setBusy(false);
        };

        var fError = function (err) {
            sap.m.MessageBox.alert(JSON.parse(err.responseText).error, {
                title: this._getLocaleText("error")
            });
            this.oDialog.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.WBS.writeNewWBS(wbs)
            .then(fSuccess, fError);
    }
});