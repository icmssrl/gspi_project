jQuery.sap.require("view.abstract.AbstractController");
if (!model.tabellaSchedulingAdmin)
    jQuery.sap.require("model.tabellaSchedulingAdmin");
if (!model.requestSave)
    jQuery.sap.require("model.requestSave");
if (!model.Consultants)
    jQuery.sap.require("model.Consultants");
if (!model.persistence.Storage)
    jQuery.sap.require("model.persistence.Storage");
if (!model.CommessePerStaffDelUm)
    jQuery.sap.require("model.CommessePerStaffDelUm");
if (!model.tabellaSchedulingUM)
    jQuery.sap.require("model.tabellaSchedulingUM");

if (!model.TabellaConsulentePeriodoFinal)
    jQuery.sap.require("model.TabellaConsulentePeriodoFinal");

if (!model.i18n)
    jQuery.sap.require("model.i18n");

view.abstract.AbstractController.extend("view.AdministratorScheduling", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.pmSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingModel, "pmSchedulingModel");
        this.pmSchedulingModel.setSizeLimit(1000);

        this.umSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.umSchedulingModel, "umSchedulingModel");
        this.umSchedulingModel.setSizeLimit(1000);
        this.umSchedulingModel.setProperty("/filtroLocked", "0");

        this.deletedRequestsModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.deletedRequestsModel, "deletedRequestsModel");
        this.deletedRequestsModel.setSizeLimit(1000);

        this.pmSchedulingConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingConsultantModel, "pmSchedulingConsultantModel");

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");

        this.noteModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteModel, "noteModel");

        this.modelloIcone = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelloIcone, "iconModel");

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        this.filterLockedModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.filterLockedModel, "filterLockedModel");

        this.listaCommesseRicModModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.listaCommesseRicModModel, "listaCommesseRicModModel");

        this.iconJsonModel = {
            'full': "sap-icon://complete",
            'half': "sap-icon://time-entry-request",
            'wCons': "sap-icon://customer-and-contacts",
            'wCust': "sap-icon://collaborate",
            'wNonCons': "sap-icon://employee-rejections",
            'wNonCust': "sap-icon://offsite-work",
            'notConfirmed': "sap-icon://role",
            'del': "sap-icon://question-mark",
            'halfText': "1/2",
            'globe': "sap-icon://globe",
            'travel': "sap-icon://travel-itinerary",
            'bed': "sap-icon://bed"
        };

        this.modelloIcone.setProperty("/icon", this.iconJsonModel.del);

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");

        this.pModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pModel, "pModel");

        this.buModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.buModel, "buModel");

        this.skillModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.skillModel, "skillModel");

        this.consultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.consultantModel, "consultantModel");

        this.adminFilterModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.adminFilterModel, "adminFilterModel");
        this.adminFilterModel.setSizeLimit(1000);

        this.uiModel.setProperty("/searchProperty", ["nomeCognome"]);

        this.noteConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteConsultantModel, "noteConsultantModel");
        this.noteConsultantModel.setSizeLimit(1000);

        this.assignGenericConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.assignGenericConsultantModel, "assignGenericConsultantModel");
        this.assignGenericConsultantModel.setSizeLimit(1000);

        this.listaCommesseConsulentiGenericiModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.listaCommesseConsulentiGenericiModel, "listaCommesseConsulentiGenericiModel");
        this.listaCommesseConsulentiGenericiModel.setSizeLimit(1000);
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");
        this.visibleModel.setProperty("/filterAfterCaricaTutto", false);

        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.userModel.setData(this.user);

        if (!this._checkRoute(evt, "administratorScheduling")) {
            return;
        }

        if (this._actionSheet)
            this._actionSheet.destroy(true);

        this.oDialog = this.getView().byId("BlockLayout");
        this.oDialogTable = this.getView().byId("idSchedulazioniTableAdmin");
        this.getView().byId("idSchedulazioniTableAdmin").setVisible(false);
        this.visibleModel.setProperty("/addConsultantButton", false);
        this.visibleModel.setProperty("/enabledButton", false);
        this.visibleModel.setProperty("/closeProjectButton", false);
        this.visibleModel.setProperty("/filterButton", false);
        this.visibleModel.setProperty("/undoButton", false);
        this.visibleModel.setProperty("/sceltaCaricamento", false);
        this.visibleModel.setProperty("/caricaTutto", true);
        this.visibleModel.setProperty("/comboBoxCommesse", false);
        this.visibleModel.setProperty("/giornateTotali", false);
        this.visibleModel.setProperty("/pjmRelease", false);
        this.visibleModel.setProperty("/allRelease", false);
        this.visibleModel.setProperty("/loadChangeRequests", false);
        this.visibleModel.setProperty("/checkChangeRequests", false);
        this.visibleModel.setProperty("/visibleAssignConsultant", false);
        this.visibleModel.setProperty("/checkProjectsGeneric", false);

        this.getView().byId("idButtonSelezionaCommessa").setPressed(false);

        this.getView().byId("idSelectCommessa").setSelectedKey("");
        this.getView().byId("idSelectCommessa").setValue("");

        this.uiModel.setProperty("/searchProperty", ["nomeCognome"]);
        this.uiModel.setProperty("/searchValue", "");
        if (sap.ui.getCore().getElementById("idListaConsulentiAdmin")) {
            sap.ui.getCore().getElementById("idListaConsulentiAdmin").removeSelections();
        }
        this.readPeriods();
        this.pmSchedulingModel.getData().giorniTotali = 0;
        this.visibleModel.setProperty("/chargeButton", false);
        this.getView().byId("refreshUM").setEnabled(false);

        this.clearPasteCommesse();
        this.moveHere = undefined;
        this.assignHere = undefined;
        this.svuotaLocalStorageUndo();
        this.fromSaveLocal = true;
        this.richiestaConFiltri = undefined;
        this.companyTypeList = [];
        //Inserted here filter lists Data in background//
        this._loadFilters();
    },

    readPeriods: function () {
        this._getPeriodsGlobal(undefined, "readStaffCommesseAdmin");
        //        var fSuccess = function (result) {
        //            var res = {};
        //            _.remove(result.items, {
        //                stato: 'CHIUSO'
        //            });
        //            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
        //            var res13 = {};
        //            res13.items = [];
        //            for (var i = 0; i < 13; i++) {
        //                if (res.items[i]) {
        //                    res13.items.push(res.items[i]);
        //                }
        //            }
        //            this.periodModel.setData(res13);
        //            this.getView().byId("idSelectPeriodo").setSelectedKey("");
        //            this.getView().byId("idSelectPeriodo").setValue("");
        //            this.readStaffCommesseAdmin();
        //        };
        //
        //        var fError = function () {
        //            if (!this.oneTimeReadPeriods) {
        //                // funzione che ripete la chiamata di login nel caso in cui sia scaduta l'autenticazione al sistema
        //                this.oneTimeReadPeriods = true;
        //                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
        //                    .then(_.bind(this.readPeriods(), this));
        //            } else {
        //                this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'getPeriods'");
        //            }
        //        };
        //
        //        fSuccess = _.bind(fSuccess, this);
        //        fError = _.bind(fError, this);
        //
        //        this.oDialog.setBusy(true);
        //        model.Periods().getPeriods()
        //            .then(fSuccess, fError);
    },

    readStaffCommesseAdmin: function () {
        var fSuccess = function (result) {
            var arrConsulente = [];
            arrConsulente = _.uniq(result.items, 'codConsulente');
            for (var t = 0; t < arrConsulente.length; t++) {
                arrConsulente[t].commesse = _.where(result.items, {
                    codConsulente: arrConsulente[t].codConsulente,
                    stato: 'APERTO'
                });
            }
            this.commessePerConsulente = arrConsulente;
            //            this.readProjects();
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'readCommessePerUmSet'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.CommessePerStaffDelUm.read()
            .then(fSuccess, fError);
    },

    readProjects: function (period) {
        this.oDialog.setBusy(true);
        var fSuccess = _.bind(function (result) {
            //            _.remove(result.items, {
            //                'statoCommessa': "CHIUSO"
            //            });
            var projectsBu = result.items;
            this.allCommesse = projectsBu;

            var comboCommessa = this.getView().byId("idSelectCommessa");
            if (comboCommessa && !this.commessaDaRisettare) {
                comboCommessa.setValue(comboCommessa.getDefaultSelectedItem());
                this.commessaDaRisettare = undefined;
            }
            var commesseOrdinate = _.sortByOrder(this.allCommesse, [function (o) {
                if (o.codPrioritaA !== 0)
                    return o.codPrioritaA;
            }, function (o) {
                if (o.codPrioritaB !== 0)
                    return o.codPrioritaB;
            }, function (o) {
                return o.nomeCommessa.toLowerCase();
            }]);
            this.pmSchedulingModel.setProperty("/commesse", []);
            this.pmSchedulingModel.setProperty("/commesse", commesseOrdinate);
            this.oDialog.setBusy(false);
        }, this);

        var fError = _.bind(function () {
            //            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'readCommessePerUmSet'");
            this._enterErrorFunction("warning", "Attenzione - errore Backend", "Errore in lettura progetti.\nContattare l'amministratore del sistema");
        }, this);

        //        fSuccess = _.bind(fSuccess, this);
        //        fError = _.bind(fError, this);

        //        model.Projects.readProjects(period)//projectsViewWithPriority
        //        .then(fSuccess, fError);
        var req = {
            idPeriod: period
        };
        model.Projects.readAdminProjectsByPeriod(req, fSuccess, fError);
    },

    onChangePeriod: function (pe) {
        this.filteredConsultants = undefined;
        this.oDialog.setBusy(true);

        this.visibleModel.setProperty("/closeProjectButton", false);
        this.visibleModel.setProperty("/filterButton", false);
        this.visibleModel.setProperty("/comboBoxCommesse", false);
        this.visibleModel.setProperty("/chargeButton", false);
        this.visibleModel.setProperty("/giornateTotali", false);
        this.visibleModel.setProperty("/pjmRelease", false);
        this.visibleModel.setProperty("/allRelease", false);
        this.visibleModel.setProperty("/loadChangeRequests", false);
        this.visibleModel.setProperty("/checkChangeRequests", false);
        this.visibleModel.setProperty("/checkProjectsGeneric", false);
        model.persistence.Storage.session.remove("commessa");

        this.getView().byId("idButtonSelezionaCommessa").setPressed(false);
        this.getView().byId("refreshUM").setEnabled(false);

        this.visibleModel.setProperty("/sceltaCaricamento", true);
        this.visibleModel.setProperty("/caricaTutto", true);

        this.svuotaSelect();
        this.svuotaTabella();

        var table = this.getView().byId("idSchedulazioniTableAdmin");

        this.pe = pe;
        var p = pe.getSource().getSelectedItem().getProperty("text").replace(/ /g, '');
        this.tempP = p;
        var period = pe.getSource().getProperty("selectedKey");
        this.periodo = parseInt(period);
        this.readProjects(this.periodo);
        model.persistence.Storage.session.save("period", period);
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(period)
        });
        model.persistence.Storage.session.save("periodSelected", periodSelect);
        if (periodSelect.rilascioPjm === "") {
            this.visibleModel.setProperty("/pjmReleasePressed", true);
        } else {
            this.visibleModel.setProperty("/pjmReleasePressed", false);
        }
        if (periodSelect.rilascioAll === "") {
            this.visibleModel.setProperty("/allReleasePressed", true);
        } else {
            this.visibleModel.setProperty("/allReleasePressed", false);
        }

        this.visibleModel.setProperty("/pjmRelease", true);
        this.visibleModel.setProperty("/allRelease", true);

        //--------------------------------------New Since 10-06-2106-----------------------------        
        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();

        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = pInizio;
        var dataF = pFine;

        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

        // ** rendo visibili solo le colonne del periodo selezionato
        var columns = table.getColumns();
        for (var i = 0; i < columns.length; i++) {
            columns[i].setVisible(true);
        }
        for (var i = this.numColonne; i < columns.length - 1; i++) {
            columns[i].setVisible(false);
        }

        // ** metto i nomi dei giorni sulle colonne
        for (var i = 2; i < columns.length; i++) {
            columns[i - 1].getLabel().mProperties.text = this.arrayGiorni[i - 2];
        }
        this.getView().byId("idSelectCommessa").setEnabled(true);

        this.periodStauts = pe.getSource().getSelectedItem().getProperty("additionalText");

        jQuery.sap.delayedCall(500, this, function () {
            this.oDialog.setBusy(false);
            if (this.getView().byId("idSelectPeriodo").getValue() === "") {
                this.getView().byId("idSelectPeriodo").setValue(this.tempP);
            }
        });
    },

    selectCommessaVisible: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            this.visibleModel.setProperty("/caricaTutto", false);
            this.visibleModel.setProperty("/comboBoxCommesse", true);
            this.visibleModel.setProperty("/giornateTotali", true);
        } else {
            this.visibleModel.setProperty("/caricaTutto", true);
            this.visibleModel.setProperty("/comboBoxCommesse", false);
            this.visibleModel.setProperty("/giornateTotali", false);
            this.visibleModel.setProperty("/closeProjectButton", false);
            this.visibleModel.setProperty("/filterButton", false);
            this.visibleModel.setProperty("/chargeButton", false);
            this.getView().byId("refreshUM").setEnabled(false);
            this.getView().byId("idSelectCommessa").setSelectedKey("");
            this.getView().byId("idSelectCommessa").setValue("");
        }
        this.svuotaTabella();
        this.pmSchedulingModel.setProperty("/giorniTotali", 0);
        model.persistence.Storage.session.remove("commessa");
        this.visibleModel.setProperty("/loadChangeRequests", false);
        this.visibleModel.setProperty("/checkChangeRequests", false);
        this.visibleModel.setProperty("/filterAfterCaricaTutto", false);
        this.visibleModel.setProperty("/visibleAssignConsultant", false);
    },

    chargeProjectsNotSelected: function () {
        this.richiestaConFiltri = undefined;
        if (model.persistence.Storage.session.get("commessa")) {
            model.persistence.Storage.session.remove("commessa");
        }
        this.getView().byId("idSelectCommessa").setSelectedKey("");
        this.getView().byId("idSelectCommessa").setValue("");
        var req = {};
        req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
        this.ric = req;
        this.commessaSelezionata = undefined;
        this.chosedCom = undefined;
        this.pmSchedulingModel.setProperty("/commessa", "");
        this.visibleModel.setProperty("/filterAfterCaricaTutto", false);
        this.visibleModel.setProperty("/giornateTotali", true);
        this.leggiTabella(req);
        this.visibleModel.setProperty("/loadChangeRequests", false);
    },

    onSelectionChangeCommessa: function (evt) {
        this.visibleModel.setProperty("/loadChangeRequests", false);
        this.visibleModel.setProperty("/chargeButton", true);
        this.chosedCom = evt.getSource().getValue().split(" - ")[0];
        this.chosedComDescrizione = evt.getSource().getValue().split(" - ")[1];
        this.commessaSelezionata = evt.getSource().getSelectedKey();
        this.pmSchedulingModel.setProperty("/commessa", this.chosedCom);
        this.getView().byId("refreshUM").setEnabled(false);
        this.visibleModel.setProperty("/filterAfterCaricaTutto", false);
        this.richiestaConFiltri = undefined;
    },

    chargeProjects: function (evt) {
        this.richiestaConFiltri = undefined;
        this.visibleModel.setProperty("/chargeButton", false);
    },

    onChangeCommessa: function (evt) {
        this.visibleModel.setProperty("/loadChangeRequests", false);
        this.oDialog.setBusy(true);
        if (!this.commessaSelezionata) {
            this._enterErrorFunction("alert", "Attenzione", "Commessa '" + this.chosedCom.toUpperCase() + "' non trovata");
            return;
        }
        var fSuccess = function (res) {
            this.currentStaff = res.items;
            if (this.currentStaff.length === 0) {
                this.oDialog.setBusy(false);
                this.oDialogTable.setBusy(false);
                sap.m.MessageToast.show("Nesuno staff presente per questa commessa");
                return;
            }
            this._onChangeCommessa(this.commessaSelezionata);
            this.clearPasteCommesse();
        };

        var fError = function () {
            sap.m.MessageToast.show("errore in lettura anagrafica staff");
            this.oDialogTable.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialogTable.setBusy(true);
        model.StaffCommesseView.readStaffSingolaCommessa(this.commessaSelezionata)
            .then(fSuccess, fError);
        this.visibleModel.setProperty("/chargeButton", false);
    },

    _onChangeCommessa: function (commessaP) {
        this.filteredConsultants = undefined;
        if (commessaP) {
            var commessaScelta = commessaP;

            var c = _.find(this.pmSchedulingModel.getData().commesse, {
                codCommessa: commessaScelta
            });
            model.persistence.Storage.session.save("commessa", c);

            var req = {};
            req.idStaff = this.currentStaff[0].codStaff;
            req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
            req.nomeCommessa = this.chosedCom;
            req.idCommessa = commessaScelta;
            this.ric = req;
            this.leggiTabella(req);
            this.visibleModel.setProperty("/visibleTable", true);
            var dataRichiesta = model.persistence.Storage.session.get("periodSelected").reqEndDate;
            this.visibleModel.setProperty("/addConsultantButton", true);
            this.visibleModel.setProperty("/enabledButton", true);
            this.visibleModel.setProperty("/closeProjectButton", true);
            this.visibleModel.setProperty("/filterButton", true);
        }
        this.oDialog.setBusy(false);
        this.oDialogTable.setBusy(false);
    },

    leggiTabella: function (req) {
        var defer = Q.defer();
        this.oDialogTable.setBusy(true);
        this.visibleModel.setProperty("/visibleAssignConsultant", false);
        var fSuccess = function (res) {
            defer.resolve();
            if (res && res.resultOrdinato) {
                var schedulazioni;
                if (this.ric.idStaff) {
                    schedulazioni = res.resultOrdinato;
                    //                    this.allSchedulingForFilter = _.cloneDeep(schedulazioni);
                    for (var t = 0; t < schedulazioni.length; t++) {
                        var commessaConsulente = _.find(_.find(this.commessePerConsulente, {
                            codConsulente: schedulazioni[t].codiceConsulente
                        }).commesse, {
                            codCommessa: this.commessaSelezionata
                        });
                        schedulazioni[t].teamLeader = commessaConsulente ? commessaConsulente.teamLeader : "";
                        schedulazioni[t].nomeSkill = commessaConsulente ? commessaConsulente.nomeSkill : "";
                        //                        this.allSchedulingForFilter[t].teamLeader = commessaConsulente ? commessaConsulente.teamLeader : "";
                        //                        this.allSchedulingForFilter[t].nomeSkill = commessaConsulente ? commessaConsulente.nomeSkill : "";
                    }
                    var listaConsulentiGenerici = _.filter(schedulazioni, function (item) {
                        if (item.codiceConsulente.indexOf("CCG") >= 0)
                            return item
                    });
                    if (_.size(listaConsulentiGenerici) > 0) {
                        this.visibleModel.setProperty("/visibleAssignConsultant", true);
                    } else {
                        this.visibleModel.setProperty("/visibleAssignConsultant", false);
                    }
                } else {
                    if (req.selectedConsultantsCode) {
                        schedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni");
                        _.remove(schedulazioni, {
                            'codiceConsulente': res.resultOrdinato[0].codiceConsulente
                        });
                        schedulazioni.push(res.resultOrdinato[0]);
                        schedulazioni = _.sortBy(schedulazioni, 'nomeConsulente');
                        //per filtro
                        //                        this.allSchedulingForFilter = _.cloneDeep(schedulazioni);
                        //                        _.remove(this.allSchedulingForFilter, {
                        //                            'codiceConsulente': res.resultOrdinato[0].codiceConsulente
                        //                        });
                        //                        this.allSchedulingForFilter.push(res.resultOrdinato[0]);
                    } else {
                        this._checkRichiesteModifica();
                        this._checkProjectsGeneric(res.resultOrdinato);
                        schedulazioni = res.resultOrdinato;
                        if (this.richiestaConFiltri && this.richiestaConFiltri.consulentiGenerici && this.richiestaConFiltri.consulentiGenerici === "X") {
                            schedulazioni = _.sortByOrder(schedulazioni, 'nomeSkill');
                        }
                    }
                    //                    this.allSchedulingForFilter = _.cloneDeep(schedulazioni);
                }
                var result = this.splitRequestIntoModel(schedulazioni);
                var uniEle = result.uniEle;
                var allRequests = result.allRequests;
                //                var schedOrdinato = _.sortBy(uniEle, 'nomeConsulente');
                var schedOrdinato = (uniEle);
                this.allRequests = _.sortBy(allRequests, 'nomeConsulente');
                if (!this.richiestaConFiltri) {
                    this.listAllConsultant = this.allRequests;
                    this.resetSelezioneFiltri = true;
                }
                this.umSchedulingModel.setProperty("/schedulazioni", this.allRequests);
                this.pmSchedulingModel.setProperty("/schedulazioni", schedOrdinato);
                if (schedOrdinato.length > 0 && schedOrdinato.length <= 6) {
                    this.pmSchedulingModel.setProperty("/nRighe", schedOrdinato.length);
                } else if (schedOrdinato.length > 6) {
                    this.pmSchedulingModel.setProperty("/nRighe", 6);
                } else {
                    this.pmSchedulingModel.setProperty("/nRighe", 1);
                }
                this.allScheduling = this.pmSchedulingModel.getProperty("/schedulazioni");
                //                this.allSchedulingForFilter = _.cloneDeep(this.pmSchedulingModel.getProperty("/schedulazioni"));
                this.getView().byId("refreshUM").setEnabled(true);
                this.visibleModel.setProperty("/filterAfterCaricaTutto", true);
                setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));
            } else {
                this.oDialogTable.setBusy(false);
                this._enterErrorFunction("error", "Errore", "Problemi nel caricamento dei dati");
            }
        };

        var fError = function (err) {
            this.oDialogTable.setBusy(false);
            this._enterErrorFunction("error", "Errore", "Errore lettura tabella: " + JSON.parse(err.responseText).error);
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if (!req.idStaff) {
            // legge tutti i consulenti di tutte le commesse
            model.tabellaSchedulingAdmin.readAdminAllRequests(req)
                .then(fSuccess, fError);
        } else {
            // legge tutti i consulenti di una singola commessa
            model.tabellaSchedulingAdmin.readAdminProjectRequests(req)
                .then(fSuccess, fError);
        }
        return defer.promise;
    },

    _leggiSingolo: function (req) {
        //this.allSchedulingForFilter = _.cloneDeep(this.pmSchedulingModel.getProperty("/schedulazioni"));
        var fSuccess = function (res) {
            //var schedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni");
            var schedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni");

            //            if (this.visibleModel.getProperty("/caricaTutto")) {
            var rimosso = _.find(schedulazioni, {
                'codiceConsulente': res.resultOrdinato[0].codiceConsulente
            });
            res.resultOrdinato[0].nomeSkill = rimosso.nomeSkill;
            res.resultOrdinato[0].isLocked = rimosso.isLocked;
            //            }

            var indiceArray = _.findIndex(schedulazioni, {
                codiceConsulente: res.resultOrdinato[0].codiceConsulente
            });
            if (res.resultOrdinato[0].vuoto) {
                var rimosso = _.find(schedulazioni, {
                    'codiceConsulente': res.resultOrdinato[0].codiceConsulente
                });
                //                res.resultOrdinato[0].nomeSkill = rimosso.nomeSkill;
                //                res.resultOrdinato[0].isLocked = rimosso.isLocked;
                res.resultOrdinato[0].cod_consulenteBu = rimosso.cod_consulenteBu;
                res.resultOrdinato[0].codicePJM = rimosso.codicePJM;
                res.resultOrdinato[0].giorniScheduling = 0;
            }

            schedulazioni.splice(indiceArray, 1, res.resultOrdinato[0]);

            var result = this.splitRequestIntoModel(schedulazioni);
            var uniEle = result.uniEle;
            var allRequests = result.allRequests;
            //            var schedOrdinato = _.sortBy(uniEle, 'nomeConsulente');
            var schedOrdinato = uniEle;
            //            this.allRequests = _.sortBy(allRequests, 'nomeConsulente');
            this.allRequests = allRequests;
            this.umSchedulingModel.setProperty("/schedulazioni", this.allRequests);
            this.pmSchedulingModel.setProperty("/schedulazioni", schedOrdinato);
            if (schedOrdinato.length > 0 && schedOrdinato.length <= 6) {
                this.pmSchedulingModel.setProperty("/nRighe", schedOrdinato.length);
            } else if (schedOrdinato.length > 6) {
                this.pmSchedulingModel.setProperty("/nRighe", 6);
            } else {
                this.pmSchedulingModel.setProperty("/nRighe", 1);
            }
            this.allScheduling = this.pmSchedulingModel.getProperty("/schedulazioni");

            //per filtro

            //            var indiceArray = _.findIndex(this.allSchedulingForFilter, {
            //                codiceConsulente: res.resultOrdinato[0].codiceConsulente
            //            });
            if (res.resultOrdinato[0].vuoto) {
                //                var rimosso = _.find(this.allSchedulingForFilter, {
                //                    'codiceConsulente': res.resultOrdinato[0].codiceConsulente
                //                });
                //                res.resultOrdinato[0].nomeSkill = rimosso.nomeSkill;
                //                res.resultOrdinato[0].isLocked = rimosso.isLocked;
                res.resultOrdinato[0].cod_consulenteBu = rimosso.cod_consulenteBu;
                res.resultOrdinato[0].codicePJM = rimosso.codicePJM;
                res.resultOrdinato[0].giorniScheduling = 0;
            }

            //            this.allSchedulingForFilter.splice(indiceArray, 1, res.resultOrdinato[0]);

            setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));
        };

        var fError = function (err) {
            this.oDialogTable.setBusy(false);
            sap.m.MessageToast.show("errore lettura tabella");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialogTable.setBusy(true);

        model.tabellaSchedulingAdmin.readAdminSingleConsultant(req)
            .then(fSuccess, fError);
    },

    splitRequestIntoModel: function (res) {
        // res saranno le righe di schedulazione per ogni consulente
        var allRequests = _.cloneDeep(res);
        var deletedRequests = [];
        var uniEle = _.forEach(res, function (n) {
            for (var i in n) {
                if (n[i] === "DELETED") {
                    var propRequestName = i.replace("Stato", "");
                    var propConflictName = i.replace("Stato", "Conflict");
                    var propIconName = i.replace("Stato", "Icon");
                    var propReqDate = i.replace("Stato", "Req");
                    var propReqOld = i.replace("Stato", "ReqOld");
                    propReqOld = propReqOld.replace("gg", "id");
                    var propReq = i.replace("ggStato", "req");
                    var propNota = i.replace("ggStato", "nota");
                    var idReqOriginal = i.replace("ggStato", "idReqMod");

                    // verifico se quello in questione è effettivamente una richiesta cancellata o se è già stata ri-assegnata
                    var commessaAssegnata = false;
                    for (var o in n) {
                        if (n[propReq] === n[o] && o !== propReq) {
                            commessaAssegnata = true;
                            break;
                        }
                    }
                    if (!commessaAssegnata) {
                        var objDeleted = {
                            'nomeConsulente': n.nomeConsulente,
                            'cod_consulenteBu': n.cod_consulenteBu,
                            'codiceConsulente': n.codiceConsulente,
                            'codicePJM': n.codicePJM
                        };
                        objDeleted.reqName = n[propRequestName];
                        objDeleted.reqType = n[propIconName];
                        objDeleted.reqDate = n[propReqDate];
                        objDeleted.reqOld = n[propReqOld];
                        objDeleted.idReq = n[propReq];
                        objDeleted.note = n[propNota];
                        objDeleted.idReqOriginal = n[idReqOriginal] === 1 ? "" : "1";
                        var reqTypeDescription = "";
                        switch (n[propIconName]) {
                            case "sap-icon://complete":
                                reqTypeDescription = this._getLocaleText("ASSIGN");
                                break;
                            case "sap-icon://time-entry-request":
                                reqTypeDescription = this._getLocaleText("ASSIGN_HALF_DAY");
                                break;
                            case "sap-icon://customer-and-contacts":
                                reqTypeDescription = this._getLocaleText("C_WHIT_CONSULTANT");
                                break;
                            case "sap-icon://collaborate":
                                reqTypeDescription = this._getLocaleText("C_WHIT_CUSTOMER");
                                break;
                            case "sap-icon://employee-rejections":
                                reqTypeDescription = this._getLocaleText("NON_C_WHIT_CONSULTANT");
                                break;
                            case "sap-icon://offsite-work":
                                reqTypeDescription = this._getLocaleText("NON_C_WHIT_CUSTOMER");
                                break;
                            case "sap-icon://role":
                                reqTypeDescription = this._getLocaleText("NON_CONFIRMED_CUSTOMER");
                                break;
                            case "sap-icon://globe":
                                reqTypeDescription = this._getLocaleText("ABROAD");
                                break;
                            case "sap-icon://travel-itinerary":
                                reqTypeDescription = this._getLocaleText("INTERAFERIE");
                                break;
                            case "sap-icon://bed":
                                reqTypeDescription = this._getLocaleText("MEZZAFERIE");
                                break;
                        }
                        objDeleted.reqTypeDescription = reqTypeDescription;
                        deletedRequests.push(objDeleted);
                        delete n[propRequestName];
                        delete n[propConflictName];
                        delete n[propIconName];
                        delete n[propReqDate];
                        delete n[propReqOld];
                        delete n[idReqOriginal];
                        delete n[propReq];
                        delete n[propNota];
                        delete n[i];
                    }
                }
            }
        }.bind(this));

        this.allDeletedRequests = deletedRequests;

        for (var g = 0; g < allRequests.length; g++) {
            var richiesteCancellate = _.filter(deletedRequests, {
                'codiceConsulente': allRequests[g].codiceConsulente
            }).length;
            uniEle[g].richiesteCancellate = richiesteCancellate;
            allRequests[g].richiesteCancellate = richiesteCancellate;
        }
        var result = {
            'allRequests': allRequests,
            'uniEle': uniEle
        };
        return result;
    },

    _onAfterRenderingTable: function () {
        if (!this.oDialogTable.getBusy()) {
            this.oDialogTable.setBusy(true);
        }
        this._renderTabelle();
        this._calcoloGiornateTotali();
        //la chiamata andava in errore se non c'è un codCommessa
        // aggiunto il controllo.
        if (model.persistence.Storage.session.get("commessa") && model.persistence.Storage.session.get("commessa").codCommessa) {
            this.checkChangeRequests();
        }

        setTimeout(_.bind(function () {
            this.oDialogTable.setBusy(false);
        }, this), 500);

    },

    _renderTabelle: function () {
        var table = this.getView().byId("idSchedulazioniTableAdmin");
        var rows = table.getRows();
        var arrayCounter = [];
        for (var i = 0; i < rows.length; i++) {
            var cellsPerRows = rows[i].getAggregation("cells");
            var g = 0;
            for (var j = 1; j < cellsPerRows.length - 1; j++) {
                var cella = cellsPerRows[j];
                if (_.find(this.weekendDays, _.bind(function (item) {
                        return (item == j);
                    }, this))) {
                    var column = $("#" + cella.getDomRef().id).parent().parent();
                    column.css('background-color', '#e5e5e5');
                }
            }
        }
    },

    _calcoloGiornateTotali: function () {
        var totalCount = 0;
        for (var i = 0; i < this.pmSchedulingModel.getProperty("/schedulazioni").length; i++) {
            totalCount = totalCount + this.pmSchedulingModel.getProperty("/schedulazioni")[i].giorniScheduling;
        }
        this.pmSchedulingModel.setProperty("/giorniTotali", totalCount);
    },

    afterLeggiTabella: function () {
        this.oDialog.setBusy(true);
        var fSuccess = function (res) {
            var schedu = this.pmSchedulingModel.getData().schedulazioni;
            for (var i = 0; i < schedu.length; i++) {
                var ee = _.find(res.items, {
                    codConsulente: schedu[i].codiceConsulente
                });
                if (ee) {
                    schedu[i].isLocked = ee.stato === "APERTO" ? false : true;
                }
            }
            var scheduUm = this.umSchedulingModel.getData().schedulazioni;
            for (var i = 0; i < scheduUm.length; i++) {
                var ee = _.find(res.items, {
                    codConsulente: scheduUm[i].codiceConsulente
                });
                if (ee) {
                    scheduUm[i].isLocked = ee.stato === "APERTO" ? false : true;
                }
            }
            this.oDialog.setBusy(false);
            this._onAfterRenderingTable();
        };

        var fError = function () {
            sap.m.MessageToast.show("Problemi nella lettura della tabella 'GSPI_STATO_CONSULENTE_PERIODO_FINAL'");
            this.oDialog.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.TabellaConsulentePeriodoFinal.read(this.periodo)
            .then(fSuccess, fError);
    },

    // funzione che 'blocca' il consulente: non permette più alcuna modifica e non viene più sovrascritto
    pressLocked: function (evt) {
        var oEvent = evt.getSource();
        var statoToWrite = oEvent.getSrc() === "sap-icon://unlocked" ? "CHIUSO" : "APERTO";
        var rowButton = parseInt(oEvent.getId().split("row")[1]);
        var row = oEvent.getBindingContext("pmSchedulingModel").getPath().split("/")[oEvent.getBindingContext("pmSchedulingModel").getPath().split("/").length - 1];

        // il cambio di stato del consulente non è possibile nel caso in cui ci siano ancora dei conflitti presenti nella sua schedulazione
        var errore = false;
        if (statoToWrite === "CHIUSO") {
            for (var i = 1; i <= 35; i++) {
                // costruisco ciclicamente l'id delle icone per vedere se c'è l'icona del conflitto; in quel caso non posso modificare lo stato
                var idIcon = "AdministratorSchedulingId--button" + i + "-col" + i + "-row" + rowButton;
                var icon = this.getView().byId(idIcon);
                if (icon) {
                    var iconSrc = icon.getSrc();
                    if (iconSrc.indexOf("warning") >= 0) {
                        sap.m.MessageBox.show(this._getLocaleText("ATTENZIONE_CONFLITTI"), {
                            icon: sap.m.MessageBox.Icon.ALLERT,
                            title: this._getLocaleText("ATTENZIONE_CONFLITTI_TITLE"),
                            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                            onClose: _.bind(function (action) {
                                if (action === "YES") {
                                    this.continuaFunzioneDiBlocco(row, statoToWrite);
                                } else {
                                    return;
                                }
                            }, this),
                        });

                        errore = true;
                        break;
                    }
                }
            }
        }
        if (errore)
            return;

        this.continuaFunzioneDiBlocco(row, statoToWrite);
    },

    continuaFunzioneDiBlocco: function (row, statoToWrite) {
        var codConsulente = this.pmSchedulingModel.getData().schedulazioni[row].codiceConsulente;
        var fSuccess = function (res) {
            var risposta = JSON.parse(res).data;
            var consulente = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                codiceConsulente: risposta.codConsulente
            });
            consulente.isLocked = risposta.stato === "APERTO" ? false : true;
            sap.m.MessageToast.show(risposta.stato === "APERTO" ? this._getLocaleText("CAMBIO_STATO_SBLOCCATO") : this._getLocaleText("CAMBIO_STATO_BLOCCATO"));
            var consulenteUm = _.find(this.umSchedulingModel.getData().schedulazioni, {
                codiceConsulente: risposta.codConsulente
            });
            consulenteUm.isLocked = risposta.stato === "APERTO" ? false : true;

            this.pmSchedulingModel.refresh();
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione", "Problemi nel cambio stato 'GSPI_STATO_CONSULENTE_PERIODO_DRAFT'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.TabellaConsulentePeriodoFinal.changeStatus(codConsulente, this.periodo, statoToWrite, this.user.umBU)
            .then(fSuccess, fError);
    },

    onConsultantPress: function (evt) {
        var oEvent = evt.getSource();
        var row = oEvent.getBindingContext("pmSchedulingModel").getPath().split("/")[oEvent.getBindingContext("pmSchedulingModel").getPath().split("/").length - 1];
        var consultantToOpen = this.pmSchedulingModel.getData().schedulazioni[row];
        var url = location.pathname + "#/tableReportConsultant/" + consultantToOpen.codiceConsulente;
        oEvent.setHref(url);
        model.persistence.Storage.session.save(consultantToOpen.codiceConsulente, consultantToOpen);
    },

    onRichiesteCancellatePress: function (evt) {
        var linkPressed = evt.getSource();
        var path = linkPressed.getBindingContext("pmSchedulingModel").getPath().split("/")[linkPressed.getBindingContext("pmSchedulingModel").getPath().split("/").length - 1];
        //        var path = parseInt(linkPressed.sId.split("row")[1]);
        var codiceConsulente = this.pmSchedulingModel.getProperty("/schedulazioni")[path].codiceConsulente;
        var nomeConsulente = this.pmSchedulingModel.getProperty("/schedulazioni")[path].nomeConsulente;
        var deletedRequests = _.filter(this.allDeletedRequests, {
            'codiceConsulente': codiceConsulente
        });
        var deletedRequestsOrderData = _.sortByOrder(deletedRequests, [function (item) {
            return new Date(item.reqDate)
        }, 'reqName']);
        this.deletedRequestsModel.setProperty("/deletedRequests", deletedRequestsOrderData);
        this.deletedRequestsModel.setProperty("/nomeConsulente", nomeConsulente);
        if (!this.dialogRichiesteCancellate) {
            this.dialogRichiesteCancellate = sap.ui.xmlfragment("view.dialog.richiesteCancellate", this);
            this.getView().addDependent(this.dialogRichiesteCancellate);
        }
        this.dialogRichiesteCancellate.open();
    },

    onAssignPress: function (evt) {
        var idButtonPressed = evt.getParameters().id;
        var path = parseInt(idButtonPressed.split("-")[2]);
        var objToAssign = this.deletedRequestsModel.getProperty("/deletedRequests")[path];

        this.objToAssign = {};
        this.objToAssign.codConsulenteFinal = objToAssign.codiceConsulente;
        this.objToAssign.dataReqFinal = objToAssign.reqDate;
        this.objToAssign.codAttivitaRichiestaFinal = objToAssign.reqType.split("//")[1];
        this.objToAssign.idPeriodoSchedulingFinal = this.periodo;
        this.objToAssign.codPjmFinal = objToAssign.codicePJM;
        this.objToAssign.dataInserimentoFinal = (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T"));
        this.objToAssign.noteFinal = objToAssign.note;
        this.objToAssign.giornoPeriodoFinal = 0;
        this.objToAssign.statoReqFinal = "CONFIRMED";
        this.objToAssign.id_req = objToAssign.idReq;

        var codCommessa = _.find(this.pmSchedulingModel.getProperty("/commesse"), {
            'nomeCommessa': objToAssign.reqName
        }).codCommessa;
        this.objToAssign.idCommessaFinal = codCommessa;
        this.onCloseDeletedRequestDialog();
    },

    onCloseDeletedRequestDialog: function () {
        this.dialogRichiesteCancellate.close();
    },

    onLaunchpadPress: function () {
        if (this.richiestaConFiltri) {
            this.onFilterAdministratorDialogRipristino(true);
        }
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.router.navTo("launchpad");
    },

    onAfterRendering: function () {
        this.pmSchedulingModel.refresh();
    },

    closeProjectPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_CLOSE_PROJECT"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_CLOSE_PROJECT_TITLE"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmSavePress, this)
        );
    },

    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            var fSuccess = function () {
                this.oDialog.setBusy(false);
                this.visibleModel.setProperty("/addConsultantButton", false);
                this.visibleModel.setProperty("/closeProjectButton", false);
                this.visibleModel.setProperty("/writeRequestsButton", false);
                this.svuotaTabella();
                this.svuotaSelect("noCommessa");
                this.readProjects("close");
            };

            var fError = function () {
                console.log("errore nel salvataggio");
                this.oDialog.setBusy(false);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            var comm = _.find(this.allCommesse, {
                codCommessa: this.getView().byId("idSelectCommessa").getSelectedKey()
            });

            model.Projects.updateProject(comm.codCommessa, comm.nomeCommessa, comm.descrizioneCommessa, comm.codPjmAssociato, comm.codiceStaffCommessa, "CHIUSO")
                .then(fSuccess, fError);
        };
    },

    onFilterAdministratorDialogRipristino: function (goToLauch) {
        this.filteredConsultants = undefined;
        this.richiestaConFiltri = undefined;
        this.pmSchedulingModel.setProperty("/schedulazioni", this.allScheduling);
        this._resetFilterSelectedItems();
        var lunghezzaSchedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni").length;
        sap.ui.getCore().getElementById("idListaConsulentiAdmin").removeSelections();
        sap.ui.getCore().getElementById("idListaBuAdmin").removeSelections();
        sap.ui.getCore().getElementById("idListaSkillAdmin").removeSelections();
        if (goToLauch !== true) {
            this.leggiTabella(this.ric);
            this.adminDialog.close();
        }
    },

    onFilterSelectAllConsultants: function (evt) {
        var src = evt.getSource();
        var txtButton = model.i18n._getLocaleText(src.getText());
        var lista = sap.ui.getCore().getElementById("idListaConsulenti");
        if (txtButton === "Seleziona tutto") {
            lista.selectAll();
            src.setText("Deseleziona tutto")
        } else {
            lista.removeSelections();
            src.setText("Seleziona tutto")
        }
    },

    onFilterDialogOK: function () {
        var buttons = sap.ui.getCore().getElementById("idSegmentedButton");
        var buttonId = buttons.getSelectedButton();
        if (buttonId === "idConsultantButton") {
            var lista = sap.ui.getCore().getElementById("idListaConsulenti");
            var selectedConsultants = lista.getSelectedItems();
            var selectedConsultantsCode = [];
            if (selectedConsultants.length > 0) {
                for (var i = 0; i < selectedConsultants.length; i++) {
                    var codConsulente = selectedConsultants[i].getDescription();
                    selectedConsultantsCode.push(codConsulente);
                };
                var schedulazioni = [];
                for (var j = 0; j < selectedConsultantsCode.length; j++) {
                    var schedulazione = _.find(this.allScheduling, {
                        'codiceConsulente': selectedConsultantsCode[j]
                    });
                    if (schedulazione) {
                        schedulazioni.push(schedulazione);
                    };
                };
                this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioni);
            };
        } else {
            this.entratoInBuDialog = true;
            var lista = sap.ui.getCore().getElementById("idListaBuAdmin");
            var selectedBusinessUnits = lista.getSelectedItems();
            var selectedBusinessUnitsCode = [];
            if (selectedBusinessUnits.length > 0) {
                for (var i = 0; i < selectedBusinessUnits.length; i++) {
                    var codBu = selectedBusinessUnits[i].getDescription();
                    selectedBusinessUnitsCode.push(codBu);
                };
                var consulenti = [];
                var allConsultants = this.pmSchedulingModel.getProperty("/consultants");
                for (var j = 0; j < selectedBusinessUnitsCode.length; j++) {
                    var consultantsForBu = _.where(allConsultants, {
                        'codConsulenteBu': selectedBusinessUnitsCode[j]
                    });
                    if (consultantsForBu.length > 0) {
                        for (var t = 0; t < consultantsForBu.length; t++) {
                            consulenti.push(consultantsForBu[t]);
                        };
                    };
                };
                var schedulazioni = [];
                for (var k = 0; k < consulenti.length; k++) {
                    var schedulazione = _.find(this.allScheduling, {
                        'codiceConsulente': consulenti[k].codConsulente
                    });
                    if (schedulazione) {
                        schedulazioni.push(schedulazione);
                    };
                };
                this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioni);
            };
        };
        var lunghezzaSchedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni").length;
        if (lunghezzaSchedulazioni === 0) {
            lunghezzaSchedulazioni = 1;
        };
        this.pmSchedulingModel.setProperty("/nRighe", lunghezzaSchedulazioni);
        this.pmSchedulingModel.refresh();
        setTimeout(_.bind(this.onAfter, this));
        jQuery.sap.delayedCall(500, this, function () {
            this.oDialogTable.setBusy(false);
        });
        this.visibleModel.setProperty("/commessaVisible", false);
        this.adminDialog.close();
    },

    getSelectButton: function (evt) {
        var selectedButton = evt.getParameters().id;
        if (selectedButton === "idConsultantButton") {
            sap.ui.getCore().getElementById("idListaConsulenti").removeSelections();
            sap.ui.getCore().getElementById("selectAllButtonAdmin").setVisible(true);
            var selectAllButton = sap.ui.getCore().byId("selectAllButtonAdmin");
            if (!!selectAllButton) {
                selectAllButton.setText(model.i18n._getLocaleText("SELECT_ALL"));
            }
            this.visibleModel.setProperty("/consultants", true);
            this.visibleModel.setProperty("/bu", false);
        } else {
            sap.ui.getCore().getElementById("idListaBuAdmin").removeSelections();
            sap.ui.getCore().getElementById("selectAllButtonAdmin").setVisible(false);
            this.visibleModel.setProperty("/consultants", false);
            this.visibleModel.setProperty("/bu", true);
        };
    },

    onFilterAdministratorSearch: function (evt) {
        var src = evt.getSource();
        this.searchValue = src.getValue();
        var searchProperty = this.uiModel.getProperty("/searchProperty");
        this.applyFilterAdministrator(this.searchValue, searchProperty);
    },

    applyFilterAdministrator: function (value, params) {
        var list = sap.ui.getCore().getElementById("idListaConsulenti");
        if (!list.getBinding("items").oList || list.getBinding("items").oList.length === 0)
            return;

        var temp = list.getBinding("items").oList[0]; // a template just to recover the data types
        var filtersArr = [];

        if (!_.isEmpty(params)) {
            if (!_.isArray(params)) {
                params = [params];
            };
            for (var i = 0; i < params.length; i++) {

                switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
                    case "undefined":
                        break;
                    case "string":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
                        break;
                    case "number":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
                        break;
                };
            };
            var filter = new sap.ui.model.Filter({
                filters: filtersArr,
                and: false
            });
            list.getBinding("items").filter(filter);
            return;
        };
        list.getBinding("items").filter();
    },

    svuotaTabella: function () {
        // svuoto la tabella
        this.getView().byId("idSchedulazioniTableAdmin").setVisible(true);
        this.pmSchedulingModel.setProperty("/schedulazioni", []);
        this.pmSchedulingModel.setProperty("/nRighe", 1);
        this.pmSchedulingModel.getData().schedulazioni = [];
    },

    svuotaSelect: function (value) {
        if (!value) {
            this.getView().byId("idSelectCommessa").setEnabled(false);
            this.getView().byId("idSelectCommessa").setSelectedKey("");
            this.getView().byId("idSelectCommessa").setValue("");
        };
    },

    refreshTable: function () {
        this.leggiTabella(this.ric);
    },

    actionButton: function (evt, richiesta) {
        var del = false;
        var b = this.oButton;
        var idButton = b.getId();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        var row = this.oButton.getBindingContext("pmSchedulingModel").getPath().split("/")[2];
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[row];
        //        var commessaComboBox = this.getView().byId("idSelectCommessa").getValue();
        var commessaComboBox = this.chosedCom;
        var prop = "gg" + nButton;
        var buttonPressed = evt.getSource();

        var tipoRichiesta = evt.getParameters().item.getText();
        if (!richiesta) {
            var richiesta = "";
            switch (tipoRichiesta) {
                case this._getLocaleText("ASSIGN"):
                    richiesta = "Assign";
                    break;
                case this._getLocaleText("ASSIGN_HALF_DAY"):
                    richiesta = "halfDay";
                    break;
                case this._getLocaleText("C_WHIT_CONSULTANT"):
                    richiesta = "wCons";
                    break;
                case this._getLocaleText("NON_C_WHIT_CONSULTANT"):
                    richiesta = "wNonCons";
                    break;
                case this._getLocaleText("C_WHIT_CUSTOMER"):
                    richiesta = "wCust";
                    break;
                case this._getLocaleText("NON_C_WHIT_CUSTOMER"):
                    richiesta = "wNonCust";
                    break;
                case this._getLocaleText("NON_CONFIRMED_CUSTOMER"):
                    richiesta = "notConfirmed";
                    break;
                case this._getLocaleText("ABROAD"):
                    richiesta = "globe";
                    break;
                case this._getLocaleText("INTERAFERIE"):
                    richiesta = "travel";
                    break;
                case this._getLocaleText("MEZZAFERIE"):
                    richiesta = "bed";
                    break;
                case this._getLocaleText("NOTE"):
                    richiesta = "note";
                    break;
                default:
                    richiesta = "del";
                    break;
            }
        }
        var tempPropIcon = "ggIcon" + nButton;
        var nomeCommessaUguale = "";
        var commessaUguale;
        if (this.commessa) {
            commessaUguale = _.find(this.commesseQuestoConsulente, {
                codCommessa: this.commessa
            });
        } else {
            commessaUguale = _.find(this.commesseQuestoConsulente, {
                nomeCommessa: dataModelConsultant[prop]
            });
        }
        if (commessaUguale) {
            nomeCommessaUguale = commessaUguale.nomeCommessa;
        }

        var tempProp = prop;
        var items = ""; //serie di "a" aggiunte alla proprietà
        if (richiesta !== "del" && richiesta !== "note") {
            while (dataModelConsultant[tempProp]) {
                if (dataModelConsultant[tempProp] === nomeCommessaUguale) {
                    prop = tempProp;
                    break;
                }
                items = "a";
                tempProp = tempProp + items;
            }
            if (dataModelConsultant[prop] && dataModelConsultant[prop] !== nomeCommessaUguale) {
                items = "";
                while (dataModelConsultant[prop]) {
                    items = "a";
                    prop = prop + items;
                    var propNotaGiorno = prop.replace("gg", "nota");
                    var propReq = prop.replace("gg", "req");
                    var idReqMod = prop.replace("gg", "idReqMod");
                }
            } else {
                propNotaGiorno = prop.replace("gg", "nota");
                propReq = prop.replace("gg", "req");
                idReqMod = prop.replace("gg", "idReqMod");
            }
        } else {
            propReq = prop.replace("gg", "req");
            idReqMod = prop.replace("gg", "idReqMod");
            var tempPropA = tempProp + "a";
            var requests = [];
            var req;
            var propIdReq;
            var propidReqMod;
            var propIconReq;
            var propDataReq;
            var propNote;
            if (dataModelConsultant[tempPropA]) {
                // scegliere commessa
                for (var t = 0; t < 3; t++) {
                    if (t === 0) {
                        req = dataModelConsultant[tempProp];
                        propIdReq = tempProp.replace("gg", "req");
                        propidReqMod = tempProp.replace("gg", "idReqMod");
                        propIconReq = tempProp.replace("gg", "ggIcon");
                        propDataReq = tempProp.replace("gg", "ggReq");
                        propNote = tempProp.replace("gg", "nota");
                    } else if (t === 1) {
                        req = dataModelConsultant[tempPropA];
                        propIdReq = tempPropA.replace("gg", "req");
                        propidReqMod = tempPropA.replace("gg", "idReqMod");
                        propIconReq = tempPropA.replace("gg", "ggIcon");
                        propDataReq = tempPropA.replace("gg", "ggReq");
                        propNote = tempPropA.replace("gg", "nota");
                    } else {
                        var tempPropAA = tempPropA + "a";
                        req = dataModelConsultant[tempPropAA];
                        propIdReq = tempPropAA.replace("gg", "req");
                        propidReqMod = tempPropAA.replace("gg", "idReqMod");
                        propIconReq = tempPropAA.replace("gg", "ggIcon");
                        propDataReq = tempPropAA.replace("gg", "ggReq");
                        propNote = tempPropAA.replace("gg", "nota");
                    }
                    if (req) {
                        var icon = dataModelConsultant[propIconReq];
                        var idReq = dataModelConsultant[propIdReq];
                        var idReqMod = dataModelConsultant[propidReqMod];
                        var commessa = _.find(this.commesseQuestoConsulente, {
                            'nomeCommessa': req
                        });
                        var idCommessa = commessa.codCommessa;
                        var pjmAss = commessa.codPjmAssociato;
                        var codConsulente = dataModelConsultant.codiceConsulente;
                        var dataReqFinal = dataModelConsultant[propDataReq];
                        var noteFinal = dataModelConsultant[propNote];
                        var periodoMese = model.persistence.Storage.session.get("period");

                        var requestToPush = {
                            'codConsulenteFinal': codConsulente,
                            'dataReqFinal': dataReqFinal,
                            'idCommessaFinal': idCommessa,
                            'codAttivitaRichiestaFinal': icon.split("//")[1],
                            'idPeriodoSchedulingFinal': periodoMese,
                            'codPjmFinal': pjmAss,
                            'dataInserimentoFinal': dataReqFinal,
                            'noteFinal': noteFinal,
                            'giornoPeriodoFinal': nButton,
                            'id_req': idReq,
                            'oldIdReqFinal': idReqMod,
                            'req': req,
                            'icon': icon
                        };
                        // non metto dentro il dialog la richiesta di non schedulabilità
                        if (icon !== "sap-icon://offsite-work")
                            requests.push(requestToPush);
                    }
                }
                if (!this.dialogCancellaRichiesta)
                    this.dialogCancellaRichiesta = sap.ui.xmlfragment("view.dialog.dialogCancellaRichiesta", this);
                this.getView().addDependent(this.dialogCancellaRichiesta);
                this.pmSchedulingModel.setProperty("/requestsToDelete", requests);
                this.dialogCancellaRichiesta.open();
                this.listaRichiesteDaCancellare = sap.ui.getCore().getElementById("idRichiesteDaCancellareList");
                this.listaRichiesteDaCancellare.removeSelections();
                return;
            }
        }

        this.oButton = "";
        // prendo il numero della tabella per passare correttamente la data
        var table = this.oDialogTable;
        var giornoMese = parseInt(table.getColumns()[nButton].getLabel().getProperty("text"));
        //** generazione data in modo corretto
        if (giornoMese < 10) {
            giornoMese = "0" + giornoMese;
        }
        var periodoMese = model.persistence.Storage.session.get("periodSelected").mesePeriodo;
        if (parseInt(giornoMese) < 10 && nButton > 20) {
            if (parseInt(periodoMese) !== 12) {
                periodoMese = parseInt(periodoMese) + 1;
            } else {
                periodoMese = "1";
                anno = parseInt(anno) + 1;
            }
        }
        if (periodoMese < 10) {
            periodoMese = "0" + periodoMese;
        }
        var anno = model.persistence.Storage.session.get("periodSelected").annoPeriodo;
        if (parseInt(periodoMese) === 12) {
            anno = parseInt(anno) + 1;
        }
        var dataRichiesta = anno + "-" + periodoMese + "-" + giornoMese;
        if (!(new Date(dataRichiesta)).getYear()) {
            var lastDayOfMonth = new Date(parseInt(dataRichiesta.split("-")[0]), parseInt(dataRichiesta.split("-")[1]), 0);
            lastDayOfMonth = lastDayOfMonth.getDate();
            var mesePlus = parseInt(periodoMese) + 1;
            if (mesePlus < 10) {
                mesePlus = "0" + mesePlus;
            }
            var giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
            dataRichiesta = anno + "-" + mesePlus + "-" + giornoPlus;
        }
        var note = "";
        if (this.paste) {
            note = model.persistence.Storage.local.get("copy").note ? model.persistence.Storage.local.get("copy").note : "";
        } else if (this.moveHere) {
            note = model.persistence.Storage.local.get("move").note ? model.persistence.Storage.local.get("move").note : "";
        } else if (this.assignHere) {
            note = this.objToAssign.note;
        } else {
            if (!note) {
                propNote = prop.replace("gg", "nota");
                note = dataModelConsultant[propNote];
            }
        }

        var dataToSend = {
            "codConsulenteFinal": "",
            "dataReqFinal": dataRichiesta,
            "idCommessaFinal": "",
            "codAttivitaRichiestaFinal": "",
            "idPeriodoSchedulingFinal": 0,
            "codPjmFinal": "",
            "dataInserimentoFinal": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "noteFinal": note,
            "giornoPeriodoFinal": 0,
            "statoReqFinal": "CONFIRMED",
            "id_req": dataModelConsultant[propReq] ? dataModelConsultant[propReq].toString() : undefined,
            "oldIdReqFinal": dataModelConsultant[idReqMod] ? dataModelConsultant[idReqMod] : dataModelConsultant[propReq] ? 0 : 1
        };

        dataToSend.codConsulenteFinal = dataModelConsultant.codiceConsulente;
        if (commessaUguale) {
            dataToSend.codPjmFinal = commessaUguale.codPjmAssociato;
        }
        if (this.paste) {
            var comProv = _.find(this.pmSchedulingModel.getProperty("/commesse"), {
                codCommessa: model.persistence.Storage.local.get("copy").commessa
            });
            dataToSend.codPjmFinal = comProv.codPjmAssociato;
        }
        dataToSend.idPeriodoSchedulingFinal = this.periodo;

        if (this.commessa) {
            dataToSend.idCommessaFinal = this.commessa;
            this.commessa = undefined;
        } else if (this.paste) {
            dataToSend.idCommessaFinal = model.persistence.Storage.local.get("copy").commessa;
        } else if (this.moveHere) {
            dataToSend.idCommessaFinal = model.persistence.Storage.local.get("move").commessa;
            dataToSend.oldIdReqFinal = (model.persistence.Storage.local.get("move").idReq).toString();
            model.persistence.Storage.local.remove("move");
            this.moveHere = undefined;
        } else if (this.assignHere) {
            dataToSend.idCommessaFinal = this.objToAssign.commessa;
            dataToSend.oldIdReqFinal = (this.objToAssign.idReq).toString();
            this.objToAssign = undefined;
            this.assignHere = undefined;
        } else {
            if (dataModelConsultant[prop] !== "feri") {
                dataToSend.idCommessaFinal = _.find(this.commesseQuestoConsulente, {
                    nomeCommessa: dataModelConsultant[prop]
                }).codCommessa;
            } else {
                dataToSend.idCommessaFinal = "AA00000008";
            }
        }
        dataToSend.giornoPeriodoFinal = nButton;

        var o = _.cloneDeep(this.pmSchedulingModel.getData().schedulazioni[row]);
        switch (richiesta) {
            case "Assign":
            case "sap-icon://complete":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaFinal = "complete";
                break;
            case "halfDay":
            case "sap-icon://time-entry-request":
                if (b.getSrc() === "sap-icon://time-entry-request" || b.getSrc() === "sap-icon://bed") {
                    if (o[prop] && o[prop] === this.chosedCom) {
                        o[prop] = this.chosedCom;
                    } else if (o[prop] && o[prop] !== this.chosedCom) {
                        prop = prop + "a";
                        o[prop] = this.chosedCom;
                    } else {
                        o[prop] = this.chosedCom;
                    }
                }
                dataToSend.codAttivitaRichiestaFinal = "time-entry-request";
                break;
            case "wCons":
            case "sap-icon://customer-and-contacts":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaFinal = "customer-and-contacts";
                break;
            case "wNonCons":
            case "sap-icon://employee-rejections":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaFinal = "employee-rejections";
                break;
            case "wCust":
            case "sap-icon://collaborate":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaFinal = "collaborate";
                break;
            case "wNonCust":
            case "sap-icon://offsite-work":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaFinal = "offsite-work";
                break;
            case "notConfirmed":
            case "sap-icon://role":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaFinal = "role";
                break;
            case "globe":
            case "sap-icon://globe":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaFinal = "globe";
                break;
            case "travel":
            case "sap-icon://travel-itinerary":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaFinal = "travel-itinerary";
                break;
            case "bed":
            case "sap-icon://bed":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaFinal = "bed";
                break;
            case "note":
                this.dataForUpdate = dataToSend;
                this.onAddNote();
                del = true;
                if (!model.persistence.Storage.local.get("undoPJM_1")) {
                    this.visibleModel.setProperty("/undoButton", false);
                }
                break;
            case "del":
                var icon = b.getSrc();
                var req = icon.substring(icon.lastIndexOf("/") + 1, icon.length);
                del = true;
                o[prop] = "";
                prop = prop + "a";
                if (o[prop]) {
                    o[prop] = "";
                }
                if (req === "time-entry-request") {
                    req = "time-entry-request";
                }
                dataToSend.codAttivitaRichiestaFinal = req;

                var idArray = "req" + nButton;
                var schedulazioneConsulente = this.pmSchedulingModel.getProperty("/schedulazioni")[row];
                var idReq = parseInt(schedulazioneConsulente[idArray]);
                dataToSend.id_req = idReq;
                this.dataToSave = dataToSend;
                this._delData(dataToSend);
                break;
            case "note":
                this.onAddNote();
                this.dataForUpdate = dataToSend;
                del = true;
                break;
        }
        if (!del) {
            this.saveRequest(dataToSend, prop);
        }
    },

    onDialogCancellaRichiestaClose: function () {
        this.dialogCancellaRichiesta.close();
    },

    onDialogCancellaRichiestaOK: function () {
        var selectedItems = this.listaRichiesteDaCancellare.getSelectedItems();
        if (selectedItems.length === 0) {
            sap.m.MessageToast.show(this._getLocaleText("selezionareUnaRichiesta"));
            return;
        } else {
            var path = parseInt(selectedItems[0].sId.split("List-")[1]);
            var request = this.pmSchedulingModel.getProperty("/requestsToDelete")[path];
            // Aggiunta funzione se è un nota 
            if (this.isNote) {
                this.dataForUpdate = request;
                // this.isNote = undefined;
                this.onAddNote();
            } else {
                this._delData(request);
            }
        }
        this.dialogCancellaRichiesta.close();
    },

    _delData: function (dataToSend) {
        if (dataToSend.req && dataToSend.icon) {
            delete dataToSend.req;
            delete dataToSend.icon;
        }
        this.datoEliminato = dataToSend;
        this.datoEliminatoToSend = _.cloneDeep(dataToSend);
        var fSuccess = function (res) {
            if (this.fromSaveLocal) {
                if (this.saveMove) {
                    var richiestaSalvata = model.persistence.Storage.local.get("move");
                    this.datoEliminato.giornoPeriodoFinal = richiestaSalvata.giornoPeriodoFinal;
                } else {
                    this.saveToLocalForUndo("del", this.datoEliminato);
                    this.visibleModel.setProperty("/undoButton", true);
                }
            }
            var elementoEliminato = this.datoEliminato;

            if (this.saveMove || this.undoMove) {
                this.saveRequest(this.datoEliminatoToSend);
            } else {
                var req = {};
                req.idPeriodo = this.periodo;
                req.selectedConsultantsCode = elementoEliminato.codConsulenteFinal;
                req.idCommessa = elementoEliminato.idCommessaFinal;
                //                req.nomeCommessa = elementoEliminato.nomeCommessa;
                if (!this.visibleModel.getProperty("/caricaTutto")) {
                    req.nomeCommessa = this.chosedCom ? this.chosedCom : _.find(this.commesseQuestoConsulente, {
                        codCommessa: this.dataToSave.idCommessaFinal
                    }).nomeCommessa;
                }
                req.idStaff = _.find(this.allCommesse, {
                    'codCommessa': this.datoEliminato.idCommessaFinal
                }).codiceStaffCommessa

                this._leggiSingolo(req);
            }
        };
        var fError = function (err) {
            sap.m.MessageToast.show("ERRORE");
            this.visibleModel.setProperty("/assignHereVisible", false);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        //Tengo traccia di tutte le richieste cancellate
        model.requestSaveToFinal.delRequest(dataToSend.id_req, "DELETED")
            .then(fSuccess, fError);

        // if (dataToSend.oldIdReqFinal === 1) {
        //     model.requestSaveToFinal.delDefRequest(dataToSend.id_req)
        //         .then(fSuccess, fError);
        // } else {
        //     model.requestSaveToFinal.delRequest(dataToSend.id_req, "DELETED")
        //         .then(fSuccess, fError);
        // }
    },

    saveRequest: function (req, prop) {
        var msgOk = "Richiesta inserita correttamente";
        var msgKo = "Errore in inserimento richiesta";
        if (this.undoMove) {
            if (req.oldGiorno) {
                req.giornoPeriodoFinal = req.oldGiorno;
                delete req.oldGiorno;
            }
        }

        this.dataToSave = req;
        this.propTemp = prop ? prop : (this.dataToSave ? "gg" + this.dataToSave.giornoPeriodoFinal : "");
        var fSuccess = function (ok) {
            sap.m.MessageToast.show(msgOk, {
                duration: 750,
            });
            this.idFinal = JSON.parse(ok).retData;
            if (this.dataToSave.idCommessaFinal === 'AA00000008') {
                var ferieToSave = {
                    codConsulente: this.dataToSave.codConsulenteFinal,
                    dataReqPeriodo: this.dataToSave.dataReqFinal,
                    idCommessa: this.dataToSave.idCommessaFinal,
                    codAttivitaRichiesta: this.dataToSave.codAttivitaRichiestaFinal,
                    idPeriodoSchedulingRic: parseInt(this.dataToSave.idPeriodoSchedulingFinal),
                    codPjm: "CC00000000",
                    dataInserimento: this.dataToSave.dataInserimentoFinal,
                    statoReq: this.dataToSave.statoReqFinal,
                    giorno: parseInt(this.dataToSave.giornoPeriodoFinal),
                    note: "INSERITA da ADMIN"
                };
                model.requestSave.sendHolidayRequest(ferieToSave)
                    .then(_.bind(function (result) {
                        // update id_req_ferie to final
                        var idFerie = JSON.parse(result).idReq;
                        var params = {
                            idFinal: parseInt(this.idFinal),
                            idFerie: parseInt(idFerie)
                        };
                        model.requestSaveToFinal.updateFerieinFinal(params);
                    }, this), _.bind(function (error) {
                        console.log(error);
                    }, this));
            }

            if (this.fromSaveLocal) {
                if (this.saveMove) {
                    this.saveMove = false;
                    var fileToSaveForUndo = model.persistence.Storage.local.get("move");
                    fileToSaveForUndo.newGiorno = this.dataToSave.giornoPeriodoFinal;
                    this.saveToLocalForUndo("move", fileToSaveForUndo);
                    model.persistence.Storage.local.remove("move");
                    this.visibleModel.setProperty("/undoButton", true);
                } else {
                    this.dataToSave.id_req = JSON.parse(ok).retData;
                    this.saveToLocalForUndo("insert", this.dataToSave);
                    this.visibleModel.setProperty("/undoButton", true);
                }
            }

            if (!this.undoMove) {
                var req = {};
                req.idPeriodo = this.periodo;
                req.selectedConsultantsCode = this.dataToSave.codConsulenteFinal;
                req.idCommessa = this.commessaSelezionata ? this.commessaSelezionata : this.dataToSave.idCommessaFinal;
                if (!this.visibleModel.getProperty("/caricaTutto")) {
                    req.nomeCommessa = this.chosedCom ? this.chosedCom : _.find(this.commesseQuestoConsulente, {
                        codCommessa: this.dataToSave.idCommessaFinal
                    }).nomeCommessa;
                }

                req.idStaff = this.ric.idStaff ? this.ric.idStaff : _.find(this.commesseQuestoConsulente, {
                    codCommessa: this.dataToSave.idCommessaFinal
                }).codStaff;

                //                this._leggiSingolo(req);
                if (!_.find(this.pmSchedulingModel.getProperty("/commesse"), {
                        codCommessa: this.dataToSave.idCommessaFinal
                    })) {
                    var staff = _.find(_.find(this.commessePerConsulente, {
                        codConsulente: this.dataToSave.codConsulenteFinal
                    }).commesse, {
                        codCommessa: this.dataToSave.idCommessaFinal
                    }).codStaff;
                    model.ProfiloCommessaPeriodo.newRecord(this.dataToSave.idCommessaFinal, this.periodo, staff, "CHIUSO", "0", "0")
                        .then(_.bind(function (res) {
                            this.commessaDaRisettare = this.commessaSelezionata;
                            this.readProjects(this.periodo);
                            this._leggiSingolo(req);
                        }, this), _.bind(function (err) {
                            this._leggiSingolo(req);
                        }, this));
                } else {
                    this._leggiSingolo(req);
                }
            }
            this.fromSaveLocal = true;
            this.undoMove = false;
            this.moveHere = undefined;
            if (model.persistence.Storage.local.get("move")) {
                model.persistence.Storage.local.remove("move");
                this.leggiTabella(this.ric);
            }

            if (!_.find(this.allCommesse, {
                    codCommessa: this.dataToSave.idCommessaFinal
                })) {
                this.readProjects(this.periodo)
            }
        };
        var fError = function (err) {
            sap.m.MessageToast.show(JSON.parse(err.responseText));
            this.leggiTabella(this.ric);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.requestSaveToFinal.sendRequest(req)
            .then(fSuccess, fError);
    },

    reInsertRequest: function (req) {
        this.dataToSave = req;

        var fSuccess = function () {
            this.saveRequest(this.dataToSave);
            this.objToAssign = undefined;
            this.assignHere = undefined;
        };

        var fError = function (err) {
            sap.m.MessageToast.show("ERRORE");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.requestSaveToFinal.delRequest(req.id_req, "CONFIRMED")
            .then(fSuccess, fError);
    },

    onRequestPress: function (evt) {
        var requestList = sap.ui.getCore().getElementById("idRequestsList");
        if (requestList.getMode() === "MultiSelect") {
            var mezzeGiornate = [];
            var giornateIntere = [];
            var requestChoice = evt.getParameters().listItem;
            var icon = requestChoice.mProperties.icon;
            var req = requestChoice.mProperties.title;
            var selected = requestChoice.mProperties.selected;
            if (selected) {
                var requestsSelected = requestList.getSelectedItems();
                for (var i = 0; i < requestsSelected.length; i++) {
                    var iconaCiclica = requestsSelected[i].getIcon();
                    if (iconaCiclica === "sap-icon://time-entry-request" || iconaCiclica === "sap-icon://bed") {
                        mezzeGiornate.push(requestsSelected[i]);
                    } else {
                        giornateIntere.push(requestsSelected[i]);
                    }
                }
                if (mezzeGiornate.length > 2 || giornateIntere.length > 1 || (mezzeGiornate.length > 0 && giornateIntere.length > 0)) {
                    sap.m.MessageBox.alert(this._getLocaleText("TROPPE_RICHIESTE"), {
                        title: this._getLocaleText("ATTENTION")
                    });
                    var idRequestChoiche = requestChoice.sId;
                    requestList.setSelectedItemById(idRequestChoiche, false);
                }
            }
        }
    },

    handleButtonPress: function (evt) {
        var del = false;
        var b = this.oButton;
        var idButton = b.getId();
        var richiestaPremuta = evt.getSource().mProperties;
        var selectedRequest = richiestaPremuta.text;
        var selectedIcon = richiestaPremuta.icon;
        var row = parseInt(idButton.split("row")[1]);
        var column = parseInt(idButton.split("col")[1]);
        var idRequests = [];
        var schedulazioneConsulente = this.pmSchedulingModel.getData().schedulazioni[row];
        for (var o in schedulazioneConsulente) {
            if (schedulazioneConsulente[o] !== selectedRequest && o.indexOf("gg" + column) >= 0 && o.indexOf("Conflict") >= 0 && o.indexOf("Icon") < 0) {
                var prop = o.replace("gg", "req");
                prop = o.replace("Conflict", "");
                var idRequest = schedulazioneConsulente[prop];
                if (selectedIcon !== "sap-icon://time-entry-request" && selectedIcon !== "sap-icon://bed") {
                    idRequests.push(idRequest);
                    //                    console.log(idRequests);
                } else {
                    var proprietaIcona = o.replace("gg", "ggIcon");
                    if (schedulazioneConsulente[proprietaIcona] !== "sap-icon://time-entry-request" && schedulazioneConsulente[proprietaIcona] !== "sap-icon://bed") {
                        idRequests.push(idRequest);
                    }
                }
            }
        }
    },

    onRequestDialogClose: function () {
        this._pmActionSheet.close();
        this._pmActionSheet.destroy(true);
    },

    removeStyleClassButton: function (b) {
        b.removeStyleClass("attention");
        b.removeStyleClass("travel");
        b.removeStyleClass("halfDay");
        b.removeStyleClass("wCons");
        b.removeStyleClass("fullDay");
        b.removeStyleClass("wNonCons");
        b.removeStyleClass("wCust");
        b.removeStyleClass("wNonCust");
        b.removeStyleClass("notConfirmed");
        b.removeStyleClass("bed");
        b.removeStyleClass("globe");
    },

    writeRequestsPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_WRITE_REQUEST"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_WRITE_REQUEST_TITLE"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmWriteRequest, this)
        );
    },

    _confirmWriteRequest: function (evt) {
        if (evt === sap.m.MessageBox.Action.YES) {
            var fScritturaRichiesteError = function (err) {
                this.oDialogTable.setBusy(false);
                sap.m.MessageToast.show(this._getLocaleText("erroreScritturaRichieste"));
            };
            var fLetturaRichiesteError = function (err) {
                this.oDialogTable.setBusy(false);
                sap.m.MessageToast.show(this._getLocaleText("erroreLetturaRichieste"));
            };
            var fCancellazioneRichiesteError = function (err) {
                this.oDialogTable.setBusy(false);
                sap.m.MessageToast.show(this._getLocaleText("erroreCancellazioneRichieste"));
            };
            var fScritturaRichiesteSuccess = function (ris) {
                if (ris.status.indexOf("OK") >= 0) {
                    this.oDialogTable.setBusy(false);
                    var table = this.getView().byId("idSchedulazioniTableUM");
                    var rows = table.getRows();
                    for (var i = 0; i < rows.length; i++) {
                        var cells = rows[i].getCells();
                        for (var j = 0; j < cells.length; j++) {
                            this.removeStyleClassButton(cells[j]);
                        }
                    }
                    if (this.ric) {
                        this.leggiTabella(this.ric);
                    } else {
                        this.oDialogTable.setBusy(false);
                    }
                }
            };
            var fLetturaRichiesteSuccess = function (ris) {
                var results = JSON.parse(ris);
                this.oDialogTable.setBusy(false);
            };
            var fCancellazioneRichiesteSuccess = function (ris) {
                if (ris.indexOf("OK") >= 0) {
                    var idPeriodo = model.persistence.Storage.session.get("period");
                    model.tabellaSchedulingUM.readRequests(idPeriodo)
                        .then(fLetturaRichiesteSuccess, fLetturaRichiesteError);
                }
            };
            fScritturaRichiesteError = _.bind(fScritturaRichiesteError, this);
            fLetturaRichiesteError = _.bind(fLetturaRichiesteError, this);
            fCancellazioneRichiesteError = _.bind(fCancellazioneRichiesteError, this);
            fScritturaRichiesteSuccess = _.bind(fScritturaRichiesteSuccess, this);
            fLetturaRichiesteSuccess = _.bind(fLetturaRichiesteSuccess, this);
            fCancellazioneRichiesteSuccess = _.bind(fCancellazioneRichiesteSuccess, this);

            this.oDialogTable.setBusy(true);
            var idPeriodo = model.persistence.Storage.session.get("period");
            model.tabellaSchedulingAdmin.deleteRequestsFromFinal(idPeriodo).then(fCancellazioneRichiesteSuccess, fCancellazioneRichiesteError);
        } else {
            return;
        }
    },

    // **********************noteDialog********************
    onAddNote: function (evt) {
        if (!this._noteDialog) {
            this._noteDialog = sap.ui.xmlfragment("view.dialog.noteDialog", this);
            this.getView().addDependent(this._noteDialog);
        }
        var commessaTitle = "";
        var selectedCommessa = _.find(this.allCommesse, {
            'codCommessa': this.dataForUpdate.idCommessaFinal
        });
        this.codCommessaForAddNote = _.cloneDeep(selectedCommessa);
        if (selectedCommessa) {
            commessaTitle = selectedCommessa.nomeCommessa + " - " + selectedCommessa.descrizioneCommessa;
        }
        if (this.isNote) {
            this.noteModel.setProperty("/note", this.dataForUpdate.noteFinal)
            this.isNote = undefined;
        }
        this.noteModel.setProperty("/commessaTitle", commessaTitle);
        this.noteModel.setProperty("/editable", true);
        this.noteModel.refresh();
        this._noteDialog.open();
    },

    onAfterCloseNoteDialog: function (evt) {
        this.noteModel.setProperty("/note", undefined);
        this.pmSchedulingModel.refresh();
    },

    onCancelNoteDialogPress: function (evt) {
        if (this._noteDialog) {
            this._noteDialog.close();
            this.pmSchedulingModel.refresh();
        };
    },

    onSaveNoteDialogPress: function (evt) {
        var fSuccess = function () {
            if (this._noteDialog) {
                this._noteDialog.close();
            };
            sap.m.MessageToast.show(this._getLocaleText("NOTE_ADDED"));
            var cloneRic = _.cloneDeep(this.ric);
            if (this.richiestaConFiltri) {
                cloneRic = _.merge(cloneRic, this.richiestaConFiltri);
            }
            cloneRic.idStaff = this.codCommessaForAddNote.codiceStaffCommessa ? this.codCommessaForAddNote.codiceStaffCommessa : "";
            cloneRic.selectedConsultantsCode = this.dataForUpdate.codConsulenteFinal ? this.dataForUpdate.codConsulenteFinal : "";
            this.dataForUpdate = "";
            this.codCommessaForAddNote = "";
            this.leggiTabella(cloneRic);
        };
        var fError = function (err) {
            sap.m.messageToast.show(this._getLocaleText("ERROR_NOTE_UPDATE"));
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        this.dataForUpdate.note = this.noteModel.getData().note;
        model.tabellaSchedulingAdmin.updateNote(this.dataForUpdate).then(fSuccess, fError);
    },

    onNotePress: function (evt) {
        var nota = evt.getSource().data("note");
        var jobName = evt.getSource().data("job");
        var job = _.find(this.getView().getModel("pmSchedulingModel").getProperty("/commesse"), {
            nomeCommessa: jobName
        });
        if (!job && jobName === "feri") {
            var commessaFeri = {};
            commessaFeri.codBu = "BU000";
            commessaFeri.codCommessa = "AA00000008";
            commessaFeri.codPjmAssociato = "";
            commessaFeri.nomeCommessa = "feri";
            commessaFeri.stato = "APERTO";
            job = commessaFeri;
        }

        var consultant = evt.getSource().getBindingContext("pmSchedulingModel").getObject();
        this.noteModel.setProperty("/note", consultant[nota]);
        var codReq = nota.substring(4);
        var propReq = "req" + codReq;

        this.dataForUpdate = {
            "codConsulenteFinal": consultant.codiceConsulente,
            "dataReqFinal": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "idCommessaFinal": job.codCommessa,
            "codAttivitaRichiestaFinal": "",
            "idPeriodoSchedulingFinal": this.periodo,
            "codPjmFinal": consultant.codicePJM,
            "dataInserimentoFinal": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "noteFinal": "",
            "giornoPeriodoFinal": codReq.indexOf("a") >= 0 ? parseInt(codReq.substr(0, codReq.length - 1)) : parseInt(codReq),
            "statoReqFinal": "CONFIRMED"
        };
        this.onAddNote();
        this.noteModel.setProperty("/editable", true);
    },

    openMenu: function (evt) {
        this.oButton = evt.getSource();
        this.idButton = this.oButton.getId();
        this.nButton = parseInt((this.idButton.split("-")[2]).substring(this.idButton.split("-")[2].indexOf("n") + 1));
        this.row = this.oButton.getBindingContext("pmSchedulingModel").getPath().split("/")[2];
        this.dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[this.row];
        if (!this.dataModelConsultant.isLocked) {
            var commesseQuestoConsulente = _.find(this.commessePerConsulente, {
                codConsulente: this.dataModelConsultant.codiceConsulente
            }).commesse;
            var commessaSelezionata = this.chosedCom;
            var cloneCommesseConsulente = _.clone(commesseQuestoConsulente);
            var commessaDaSpostare = _.remove(cloneCommesseConsulente, {
                'nomeCommessa': commessaSelezionata
            });
            this.commesseQuestoConsulente = [];
            if (commesseQuestoConsulente.length > 1)
                cloneCommesseConsulente = _.sortBy(cloneCommesseConsulente, function (n) {
                    return n.nomeCommessa.toUpperCase();
                });
            if (commessaDaSpostare.length !== 0) {
                cloneCommesseConsulente.unshift(commessaDaSpostare[0]);
            }
            this.commesseQuestoConsulente = _.clone(cloneCommesseConsulente);
            if (_.find(this.commesseQuestoConsulente, {
                    codCommessa: 'AA00000008'
                })) {
                _.remove(this.commesseQuestoConsulente, {
                    codCommessa: 'AA00000008'
                });
            }
            var commessaFeri = {};
            commessaFeri.codBu = "BU000";
            commessaFeri.codCommessa = "AA00000008";
            commessaFeri.codConsulente = this.commesseQuestoConsulente[0].codConsulente;
            commessaFeri.codPjmAssociato = "";
            commessaFeri.nomeCommessa = "feri";
            commessaFeri.stato = "APERTO";
            commessaFeri.codStaff = "CSC0000031";
            this.commesseQuestoConsulente.push(commessaFeri);

            this._openMenu();
            this.commesseOffsiteWork = [];
            var prop = "gg" + this.nButton;
            while (this.dataModelConsultant[prop]) {
                var propIcon = prop.replace("gg", "ggIcon");
                if (this.dataModelConsultant[propIcon] === "sap-icon://offsite-work") {
                    this.commesseOffsiteWork.push({
                        comm: this.dataModelConsultant[prop]
                    });
                }
                prop = prop + "a";
            }
        }
    },

    _openMenu: function () {
        if (!this._menu) {
            this._menu = sap.ui.xmlfragment("view.fragment.menuButtonsALL", this);
            this.getView().addDependent(this._menu);
        }
        var eDock = sap.ui.core.Popup.Dock;
        this._menu.setModel(this.visibleModel, "visibleModel");
        this._menu.open(undefined, this.oButton, eDock.BeginTop, eDock.BeginBottom, this.oButton);
        var icon = this.oButton.getSrc();

        this.visibleModel.setProperty("/assegnaProposta", false);
        this.visibleModel.setProperty("/removeAssegnaProposta", false);
        this.visibleModel.setProperty("/menuCommesseVisible", true);

        if (icon.indexOf("warning") >= 0) {
            setTimeout(_.bind(this.creaMenuConflitti, this));
            this.visibleModel.setProperty("/menuConflittiVisible", true);
        } else {
            setTimeout(_.bind(this.creaMenuScheduling, this));
            this.visibleModel.setProperty("/menuConflittiVisible", false);
        }
    },

    creaMenuScheduling: function (evt) {
        var prop = "gg" + this.nButton;
        var propBis = "gg" + this.nButton + "a";
        var schedulazioneGiornaliera = this.dataModelConsultant[prop];
        var schedulazioneGiornalieraBis = this.dataModelConsultant[propBis];
        var commessaSelezionata = this.chosedCom;

        //per le giornate non schedulabili nascondo il bottone cancella
        var propIcon = "ggIcon" + this.nButton;
        var noSchedulingIcon = this.dataModelConsultant[propIcon];
        // verifico se mostrare gli item CANCELLA, COPIA E NOTE
        if (schedulazioneGiornaliera) {
            this.visibleModel.setProperty("/deleteVisible", true);
            this.visibleModel.setProperty("/copyVisible", true);
            this.visibleModel.setProperty("/noteVisible", true);
        } else {
            this.visibleModel.setProperty("/deleteVisible", false);
            this.visibleModel.setProperty("/copyVisible", false);
            this.visibleModel.setProperty("/noteVisible", false);
        }
        if (noSchedulingIcon === "sap-icon://offsite-work") {
            this.visibleModel.setProperty("/deleteVisible", false);
        }
        // verifico se mostrare il menuItem INCOLLA
        this.showPasteItem();
        // verifico se mostrare il menuItem SPOSTA QUI
        this.showMoveHereItem();
        // verifico se mostrare il menuItem ASSEGNA QUI
        this.showAssignHereItem();
        // carico le commesse sul menuItem COMMESSE
        this.mostraCommesse();
    },

    creaMenuConflitti: function () {
        this.visibleModel.setProperty("/deleteVisible", false);
        this.visibleModel.setProperty("/copyVisible", false);
        this.visibleModel.setProperty("/noteVisible", false);
        // verifico se mostrare il menuItem INCOLLA
        this.showPasteItem();
        // verifico se mostrare il menuItem SPOSTA QUI
        this.showMoveHereItem();
        // carico le commesse sul menuItem CONFLITTO
        this.mostraConflitto();
        // verifico se mostrare il menuItem ASSEGNA QUI
        this.showAssignHereItem();
        // carico le commesse sul menuItem COMMESSE
        this.mostraCommesse();
    },

    showPasteItem: function () {
        var schedulazioneInLocal = model.persistence.Storage.local.get("copy");
        if (schedulazioneInLocal) {
            var commessaInLocal = schedulazioneInLocal.commessa;
            var commessa = _.find(this.commesseQuestoConsulente, {
                'codCommessa': commessaInLocal
            });
            var nomeCommessa = "";
            if (commessa)
                nomeCommessa = commessa.nomeCommessa;
            var presenzaCommessa = this.verificaPresenzaCommessa(nomeCommessa);

            if (commessa && !presenzaCommessa) {
                this.visibleModel.setProperty("/pasteVisible", true);
            } else {
                this.visibleModel.setProperty("/pasteVisible", false);
            }
        } else {
            this.visibleModel.setProperty("/pasteVisible", false);
        }
    },

    showMoveHereItem: function () {
        var schedulazioneSpostataInLocal = model.persistence.Storage.local.get("move");
        if (schedulazioneSpostataInLocal) {
            var commessaInLocal = schedulazioneSpostataInLocal.idCommessaFinal;
            var commessa = _.find(this.commesseQuestoConsulente, {
                'codCommessa': commessaInLocal
            });
            var nomeCommessa = "";
            if (commessa)
                nomeCommessa = commessa.nomeCommessa;
        }
        var presenzaCommessa = this.verificaPresenzaCommessa(nomeCommessa);
        if (schedulazioneSpostataInLocal && schedulazioneSpostataInLocal.codConsulenteFinal === this.dataModelConsultant.codiceConsulente && !presenzaCommessa) {
            this.visibleModel.setProperty("/moveHereVisible", true);
        } else {
            this.visibleModel.setProperty("/moveHereVisible", false);
        }
    },

    showAssignHereItem: function () {
        if (this.objToAssign) {
            var commessa = _.find(this.commesseQuestoConsulente, {
                'codCommessa': this.objToAssign.idCommessaFinal
            });
            var nomeCommessa = "";
            if (commessa)
                nomeCommessa = commessa.nomeCommessa;
        }
        var presenzaCommessa = this.verificaPresenzaCommessa(nomeCommessa);
        if (this.objToAssign && this.objToAssign.codConsulenteFinal === this.dataModelConsultant.codiceConsulente && !presenzaCommessa) {
            this.visibleModel.setProperty("/assignHereVisible", true);
        } else {
            this.visibleModel.setProperty("/assignHereVisible", false);
        }
    },

    verificaPresenzaCommessa: function (nomeCommessa) {
        var prop = "gg" + this.nButton;
        var presenzaCommessa = false;
        while (this.dataModelConsultant[prop]) {
            if (this.dataModelConsultant[prop] === nomeCommessa) {
                presenzaCommessa = true;
                break;
            }
            prop = prop + "a";
        }
        return presenzaCommessa;
    },

    mostraCommesse: function () {
        var menuCommesse = sap.ui.getCore().getElementById("idMenuCommesseALL");
        menuCommesse.removeAllItems();
        var tipiRichiesteFerie = sap.ui.xmlfragment("view.fragment.MenuFerie", this);
        for (var i = 0; i < this.commesseQuestoConsulente.length; i++) {
            var itemToAdd = new sap.ui.unified.MenuItem({
                text: this.commesseQuestoConsulente[i].nomeCommessa,
                select: _.bind(this.premiCommessa, this)
            });

            if (i === 1)
                itemToAdd.setStartsSection(true);

            // !! rimuovere commessa dove non schedulabile
            if (_.find(this.commesseOffsiteWork, {
                    comm: this.commesseQuestoConsulente[i].nomeCommessa
                })) {
                itemToAdd.setIcon("sap-icon://offsite-work");
                itemToAdd.setEnabled(false);
            }
            menuCommesse.addItem(itemToAdd);
        }
    },

    premiCommessa: function (evt) {
        var title = evt.getParameters().item.getProperty("text");
        var commessa = _.find(this.commesseQuestoConsulente, {
            'nomeCommessa': title
        });
        this.commessa = commessa.codCommessa;

        if (this.commessa !== "AA00000008") {
            if (!this._menuAttvita) {
                this._menuAttvita = sap.ui.xmlfragment("view.fragment.MenuRichieste", this);
                this.getView().addDependent(this._menuAttvita);
            }
            this._menuAttvita.getAggregation("items")[0].setText(title);
            var eDock = sap.ui.core.Popup.Dock;
            this._menuAttvita.open(undefined, this.oButton, eDock.BeginTop, eDock.BeginBottom, this.oButton);
        } else {
            if (!this._menuFerie) {
                this._menuFerie = sap.ui.xmlfragment("view.fragment.MenuFerie", this);
                this.getView().addDependent(this._menuFerie);
            }
            var eDock = sap.ui.core.Popup.Dock;
            this._menuFerie.open(undefined, this.oButton, eDock.BeginTop, eDock.BeginBottom, this.oButton);
        }

        setTimeout(_.bind(this._renderTabelle, this));
    },

    mostraConflitto: function () {
        var commesseConflict = [];
        var propA = "";
        var propConflict = "ggConflict" + this.nButton;
        while (this.dataModelConsultant[propConflict]) {
            var propIcon = "ggIcon" + this.nButton + propA;
            var propId = "req" + this.nButton + propA;
            var prop = "gg" + this.nButton + propA;
            var propStato = "ggStato" + this.nButton + propA;
            var propData = "ggReq" + this.nButton + propA;
            var propNote = "nota" + this.nButton + propA;
            var propReq = "req" + this.nButton + propA;
            var propReqOld = "idReqMod" + this.nButton + propA;

            var commessaSel = _.find(this.commesseQuestoConsulente, {
                nomeCommessa: this.dataModelConsultant[prop]
            });
            if (commessaSel) {
                var objToAdd = {
                    "codConsulenteFinal": this.dataModelConsultant.codiceConsulente,
                    "dataReqFinal": this.dataModelConsultant[propData],
                    "idCommessaFinal": commessaSel.codCommessa,
                    "codAttivitaRichiestaFinal": this.dataModelConsultant[propIcon].split("//")[1],
                    "idPeriodoSchedulingFinal": this.periodo,
                    "codPjmFinal": commessaSel.codPjmAssociato,
                    "dataInserimentoFinal": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
                    "noteFinal": this.dataModelConsultant[propNote],
                    "giornoPeriodoFinal": this.nButton,
                    "statoReqFinal": this.dataModelConsultant[propStato],
                    "id_req": this.dataModelConsultant[propReq],
                    "oldIdReqFinal": this.dataModelConsultant[propReqOld],
                    "nomeCommessa": this.dataModelConsultant[prop],
                    "icon": this.dataModelConsultant[propIcon],
                    "id": this.dataModelConsultant[propId],
                    "stato": this.dataModelConsultant[propStato]
                };
            } else {
                break;
            }
            commesseConflict.push(objToAdd);
            propA = propA + "a";
        }
        // se dentro all'array c'è la commessa selezionata nella combobox, allora la metto all'inizio del menu
        var commessaSelezionata = this.chosedCom;
        var cloneCommesseConflitto = _.clone(commesseConflict);
        var commessaDaSpostare = _.remove(cloneCommesseConflitto, {
            'nomeCommessa': commessaSelezionata
        });
        if (cloneCommesseConflitto.length > 1)
            cloneCommesseConflitto = _.sortBy(cloneCommesseConflitto, function (n) {
                return n.nomeCommessa.toUpperCase();
            });
        if (commessaDaSpostare.length !== 0)
            cloneCommesseConflitto.unshift(commessaDaSpostare[0]);

        this.commesseConflitto = _.clone(cloneCommesseConflitto);

        var menu = sap.ui.getCore().getElementById("idMenuConflittiALL");
        menu.removeAllItems();
        for (var i = 0; i < this.commesseConflitto.length; i++) {
            if (this.commesseConflitto[i].icon === "sap-icon://offsite-work") {
                continue;
            }
            var risolviConflitto = sap.ui.xmlfragment("view.fragment.MenuConflitto", this);
            var itemToAdd = new sap.ui.unified.MenuItem({
                text: this.commesseConflitto[i].nomeCommessa,
                icon: this.commesseConflitto[i].icon
            });

            var customData = new sap.ui.core.CustomData({
                key: "scrittaCommesseConflitti",
                value: this.commesseConflitto[i].stato,
                writeToDom: true
            });
            itemToAdd.addCustomData(customData)

            if (i === 1 && this.commesseConflitto[0].nomeCommessa === commessaSelezionata)
                itemToAdd.setStartsSection(true);
            var menuRisolviConflitto = new sap.ui.unified.Menu({
                items: risolviConflitto
            });
            itemToAdd.setSubmenu(menuRisolviConflitto);
            menu.addItem(itemToAdd);
        }
    },

    selectCommessa: function (evt) {

    },

    handleMenuItemPress: function (evt) {
        var selectedText = evt.getParameters().item.getText();
        if (selectedText === this._getLocaleText("COPY")) {
            var idButton = this.oButton.getId();
            var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
            var req = "req" + nButton;
            var ggIcon = "ggIcon" + nButton;
            var nota = "nota" + nButton;
            var com = "gg" + nButton;
            var row = this.oButton.getBindingContext("pmSchedulingModel").getPath().split("/")[2];
            var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[parseInt(row)];
            var codCommessa = _.find(this.pmSchedulingModel.getData().commesse, {
                nomeCommessa: dataModelConsultant[com]
            }).codCommessa
            var modelToSave = {
                codCons: dataModelConsultant.codiceConsulente,
                idReq: dataModelConsultant[req],
                richiesta: dataModelConsultant[ggIcon],
                note: dataModelConsultant[nota],
                commessa: codCommessa
            };
            model.persistence.Storage.local.save("copy", modelToSave);
        } else if (selectedText === this._getLocaleText("PASTE")) {
            if (model.persistence.Storage.local.get("copy")) {
                this.paste = true;
                var rich = model.persistence.Storage.local.get("copy");
                this.commessa = rich.commessa;
                this.actionButton(evt, rich.richiesta);
            }
        } else if (selectedText === this._getLocaleText("MOVE_HERE")) {
            if (model.persistence.Storage.local.get("move")) {
                this.moveHere = true;
                var rich = model.persistence.Storage.local.get("move");
                var col = this.oDialogTable.getColumns()[this.nButton];
                var giornata = col.getAggregation("label").getProperty("text");
                var nGiorno = parseInt(giornata.split(" ")[0]);
                if (nGiorno.toString().length === 1) {
                    nGiorno = "0" + nGiorno;
                }
                // Aggiungo dei controlli per recuperare il mese/anno da inizio periodo e fine periodo 
                var allPeriods = this.periodModel.getData() ? this.periodModel.getData().items : [];
                var periodo = _.find(allPeriods, function (n) {
                    return n.idPeriod === this.periodo;
                }.bind(this))
                // Mese e Anno di Inizio e Fine Periodo
                var meseInizioP = parseInt(periodo.dataInizio.split("/")[1]);
                var annoInizioP = parseInt(periodo.dataInizio.split("/")[2]);
                var meseFineP = parseInt(periodo.dataFine.split("/")[1]);
                var annoFineP = parseInt(periodo.dataFine.split("/")[2]);
                //Ricavo anno è mese corrente selezionati 
                var meseSelect = parseInt(rich.dataReqFinal.split("-")[1]);
                var annoSelect = parseInt(rich.dataReqFinal.split("-")[0]);
                //Confronto il mese è l'anno selezionato con quello del periodo
                if (this.nButton < 20 || (this.nButton >= 20 && nGiorno > 20)) {
                    if (meseSelect !== meseInizioP) {
                        meseSelect = meseInizioP;
                    }
                    if (annoSelect !== annoInizioP) {
                        annoSelect = annoInizioP;
                    }
                } else {
                    if (meseSelect !== meseFineP) {
                        meseSelect = meseFineP;
                    }
                    if (annoSelect !== annoFineP) {
                        annoSelect = annoFineP;
                    }
                }
                rich.dataReqFinal = annoSelect + "-" + meseSelect + "-" + nGiorno;
                this.commessa = rich.idCommessaFinal;
                delete rich.id;
                delete rich.nomeCommessa;
                delete rich.stato;
                delete rich.icon;
                rich.giornoPeriodoFinal = this.nButton
                this.saveMove = true;
                this._delData(rich);
            }
        } else if (selectedText === this._getLocaleText("ASSIGN_HERE")) {
            if (this.objToAssign) {
                this.assignHere = true;
                var col = this.oDialogTable.getColumns()[this.nButton];
                var giornata = col.getAggregation("label").getProperty("text");
                var nGiorno = parseInt(giornata.split(" ")[0]);
                if (nGiorno.toString().length === 1) {
                    nGiorno = "0" + nGiorno;
                }
                // var data = "";
                // Aggiungo dei controlli per recuperare il mese/anno da inizio periodo e fine periodo 
                var allPeriods = this.periodModel.getData() ? this.periodModel.getData().items : [];
                var periodo = _.find(allPeriods, function (n) {
                    return n.idPeriod === this.periodo;
                }.bind(this))
                // Mese e Anno di Inizio e Fine Periodo
                var meseInizioP = parseInt(periodo.dataInizio.split("/")[1]);
                var annoInizioP = parseInt(periodo.dataInizio.split("/")[2]);
                var meseFineP = parseInt(periodo.dataFine.split("/")[1]);
                var annoFineP = parseInt(periodo.dataFine.split("/")[2]);
                //Ricavo anno è mese corrente selezionati 
                var meseSelect = parseInt(this.objToAssign.dataReqFinal.split("-")[1]);
                var annoSelect = parseInt(this.objToAssign.dataReqFinal.split("-")[0]);
                //Confronto il mese è l'anno selezionato con quello del periodo
                if (this.nButton < 20 || (this.nButton >= 20 && nGiorno > 20)) {
                    if (meseSelect !== meseInizioP) {
                        meseSelect = meseInizioP;
                    }
                    if (annoSelect !== annoInizioP) {
                        annoSelect = annoInizioP;
                    }
                } else {
                    if (meseSelect !== meseFineP) {
                        meseSelect = meseFineP;
                    }
                    if (annoSelect !== annoFineP) {
                        annoSelect = annoFineP;
                    }
                }
                this.objToAssign.dataReqFinal = annoSelect + "-" + meseSelect + "-" + nGiorno;
                this.objToAssign.giornoPeriodoFinal = this.nButton;
                this.reInsertRequest(this.objToAssign);
                this.commessa = this.objToAssign.idCommessaFinal;
            }
        } else if (selectedText === this._getLocaleText("NOTE")) {
            // verifico se è una nota
            this.isNote = true;
            this.actionButton(evt);
        } else {
            // faccio un controllo se ho selezionato il menù per la risoluzione del conflitto
            var risoluzioneConflitto = false;
            if (!(selectedText === this._getLocaleText("CONFIRM") ||
                    (selectedText === this._getLocaleText("DELETE") && !evt.getParameters().item.getStartsSection()) ||
                    selectedText === this._getLocaleText("MOVE"))) {
                var selectedActiviy = _.find(this.commesseQuestoConsulente, {
                    'nomeCommessa': selectedText
                });
                if (selectedActiviy || !evt.getParameters().item.getIcon())
                    return;
                this.paste = false;
                this.actionButton(evt);
            }
        }
    },

    selectCommessaConflitto: function (evt) {
        var nomeCommessaSelezionata = evt.getParameters().item.getParent().getParent().getProperty("text");
        var commessaSelezionata = _.remove(this.commesseConflitto, {
            'nomeCommessa': nomeCommessaSelezionata
        })[0];
        var action = evt.getSource().getText();
        if (action === this._getLocaleText("CONFIRM")) {
            // se viene selezionato CONFERMA
            var fSuccess = function () {
                this.oDialogTable.setBusy(false);
                this.confermaCommessa(commessaSelezionata, this.commesseConflitto[0].codConsulenteFinal);
            };

            var fError = function () {
                this._pmActionSheet.close();
                this._pmActionSheet.destroy(true);
                this.oDialogTable.setBusy(false);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            for (var j = 0; j < this.commesseConflitto.length; j++) {
                var idReq = this.commesseConflitto[j].id;
                this.oDialogTable.setBusy(true);
                if (j === 0) {
                    if (this.commesseConflitto.length > 1) {
                        model.tabellaSchedulingAdmin.deleteRequest(idReq, "STANDBY", this.commesseConflitto.length);
                    } else {
                        model.tabellaSchedulingAdmin.deleteRequest(idReq, "STANDBY", this.commesseConflitto.length)
                            .then(fSuccess, fError);
                    }
                } else {
                    model.tabellaSchedulingAdmin.deleteRequest(idReq, "STANDBY")
                        .then(fSuccess, fError);
                }
            }
        } else if (action === this._getLocaleText("DELETE")) {
            // se viene selezionato CANCELLA
            commessaSelezionata.id_req = commessaSelezionata.id;
            this._delData(commessaSelezionata);
        } else if (action === this._getLocaleText("MOVE")) {
            // se viene selezionato SPOSTA
            var codConsulente = this.dataModelConsultant.codiceConsulente;
            var idReq = commessaSelezionata.id;
            var richiesta = commessaSelezionata.icon;
            for (var j in this.dataModelConsultant) {
                if (this.dataModelConsultant[j] === idReq) {
                    var propNote = j.replace("req", "note");
                    var note = this.dataModelConsultant[propNote];
                    break;
                }
            }
            var codCommessa = _.find(this.pmSchedulingModel.getData().commesse, {
                nomeCommessa: commessaSelezionata.nomeCommessa
            }).codCommessa

            model.persistence.Storage.local.save("move", commessaSelezionata);
        } else {
            // se viene selezionato qualcos'altro che per ora non c'è
            sap.m.MessageToast.show(this._getLocaleText("AZIONE_NON_RICONOSCIUTA"));
        }
    },

    confermaCommessa: function (req, codCons) {
        var fSuccess = function () {
            var req = {};
            req.idPeriodo = this.periodo;
            req.selectedConsultantsCode = codCons;
            req.idCommessa = this.commessaSelezionata;
            req.nomeCommessa = this.chosedCom;
            req.idStaff = this.ric.idStaff;
            this._leggiSingolo(req);
        };
        var fError = function (err) {
            sap.m.MessageToast.show("ERRORE")
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.requestSaveToFinal.delRequest(req.id, "CONFIRMED")
            .then(fSuccess, fError);
    },

    // funzione che ricarica la tabella
    reloadPress: function () {
        this.richiestaConFiltri = undefined;
        this.ric.conflitti = "";
        this.leggiTabella(this.ric);
        this.visibleModel.setProperty("/undoButton", false);
        this.clearPasteCommesse();
        this.svuotaLocalStorageUndo();
        this.fromSaveLocal = true;
    },

    /*************Filter Part**********************/
    _loadFilters: function () {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this._loadSkills()
                .then(defer.resolve, fError);
        }
        fSuccess = _.bind(fSuccess, this);

        var fError = function (err) {
            sap.m.MessageToast.show(this._getLocaleText("errorLoadingFilterLists"));
            defer.reject(err);
        }
        fError = _.bind(fError, this);

        this._loadConsultants()
            .then(_.bind(function (res) {
                this._loadBuList()
                    .then(_.bind(fSuccess, this), _.bind(fError, this))
            }, this), fError)
        return defer.promise;
    },

    _loadConsultants: function () {
        var defer = Q.defer();
        var fConsultantSuccess = function (result) {
            _.remove(result.items, {
                stato: 'CHIUSO'
            });
            var consultantsBu = result.items;
            this.consultantModel.setProperty("/results", consultantsBu);
            defer.resolve(consultantsBu);
        };
        var fConsultantError = function (err) {
            sap.m.MessageToast("error BU");
            defer.reject(err);
        };
        fConsultantSuccess = _.bind(fConsultantSuccess, this);
        fConsultantError = _.bind(fConsultantError, this);
        model.Consultants.readConsultants()
            .then(fConsultantSuccess, fConsultantError);

        return defer.promise;
    },

    _loadBuList: function () {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.buList = {
                results: result.items
            };
            this.buModel.setData(this.buList);
            defer.resolve(result);
        }
        fSuccess = _.bind(fSuccess, this);

        var fError = function (err) {
            this.buList = {
                "results": []
            };
            this.buModel.setData(this.buList);
            sap.m.MessageToast.show(this._getLocaleText("errorLoadingBuList"));
            defer.reject(err);
        }
        fError = _.bind(fError, this);

        model.BuList.getBuListByStatus("APERTO")
            .then(fSuccess, fError)

        return defer.promise;
    },

    _loadSkills: function () {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this.skills = {
                results: result.items
            };
            this.skillModel.setData(this.skills);
            defer.resolve(result);
        }
        fSuccess = _.bind(fSuccess, this);

        var fError = function (err) {
            this.skills = {
                "results": []
            };
            this.skillModel.setData(this.skills);
            sap.m.MessageToast.show(this._getLocaleText("errorLoadingSkillList"));
            defer.reject(err);
        }
        fError = _.bind(fError, this);

        model.Skill.readSkills()
            .then(fSuccess, fError)

        return defer.promise;
    },

    onFilterAdministratorConsultantSearch: function (evt) {
        this._onFilterAdministratorSearch(evt, "idListaConsulentiAdmin", ["nomeConsulente", "cognomeConsulente", "codConsulente"]);
    },

    onFilterAdministratorBuSearch: function (evt) {
        this._onFilterAdministratorSearch(evt, "idListaBuAdmin", ["descrizioneBu", "codiceBu"]);
    },

    onFilterAdministratorSkillSearch: function (evt) {
        this._onFilterAdministratorSearch(evt, "idListaSkillAdmin", ["nomeSkill", "descSkill", "codSkill"]);
    },

    _onFilterAdministratorSearch: function (evt, listId, params) {
        var value = evt.getSource().getValue();
        var list = sap.ui.getCore().getElementById(listId);
        if (!list.getBinding("items").oList || list.getBinding("items").oList.length === 0)
            return;

        var temp = list.getBinding("items").oList[0]; // a template just to recover the data types
        var filtersArr = [];

        if (!_.isEmpty(params)) {
            if (!_.isArray(params)) {
                params = [params];
            }
            for (var i = 0; i < params.length; i++) {
                switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
                    case "undefined":
                        break;
                    case "string":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
                        break;
                    case "number":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
                        break;
                }
            }
            var filter = new sap.ui.model.Filter({
                filters: filtersArr,
                and: false
            });
            list.getBinding("items").filter(filter);
            return;
        }
        list.getBinding("items").filter();
    },

    onFilterPress: function () {
        var page = this.getView().byId("schedulingAdministratorPage");
        if (!this.adminDialog) {
            this.adminDialog = sap.ui.xmlfragment("view.dialog.filterDialogAdmin_", this);
            page.addDependent(this.adminDialog);
        }
        if (this.resetSelezioneFiltri) {
            sap.ui.getCore().getElementById("idListaConsulentiAdmin").removeSelections();
            sap.ui.getCore().getElementById("idListaBuAdmin").removeSelections();
            sap.ui.getCore().getElementById("idListaSkillAdmin").removeSelections();
            sap.ui.getCore().getElementById("idListaTipoAzienda").removeSelections();
            this.resetSelezioneFiltri = false;
        }
        this._loadFilterLists();

        var binding = sap.ui.getCore().getElementById("idListaConsulentiAdmin").getBinding("items");
        if (binding) {
            binding.filter([]);
        }

        var buBinding = sap.ui.getCore().getElementById("idListaBuAdmin").getBinding("items");
        if (buBinding) {
            buBinding.filter([]);
        }

        var skillBinding = sap.ui.getCore().getElementById("idListaSkillAdmin").getBinding("items");
        if (skillBinding) {
            skillBinding.filter([]);
        }

        var tipoAziendaBinding = sap.ui.getCore().getElementById("idListaTipoAzienda").getBinding("items");
        if (tipoAziendaBinding) {
            tipoAziendaBinding.filter([]);
        }

        this.uiModel.setProperty("/searchValue", "");
        this.uiModel.refresh();
        this.adminDialog.open();
    },

    _loadFilterLists: function () {
        this.adminFilterModel.setProperty("/selectedTab", "consultants");
        this.adminFilterModel.setProperty("/allButtonVisible", true);
        this._setFilterProperty("/dayRange", "giorniScheduling", null, null);
        this._setFilterProperty("/consultants", "codiceConsulente", "consultantModel", "codConsulente");
        this._setFilterProperty("/buList", "cod_consulenteBu", "buModel", "codiceBu");
        this._setFilterProperty("/skills", "nomeSkill", "skillModel", "nomeSkill");
        // aggiungo proprietà filtro per tipo Azienda
        this.adminFilterModel.setProperty("/companyType", this.companyTypeList);

        if (this.companyTypeList.length === 0) {
            var consutantsArray = this.adminFilterModel.getProperty("/consultants");
            consutantsArray.forEach(function (k) {
                var obj = {
                    nomeAzienda: k.tipoAzienda,
                    key: k.tipoAzienda
                }
                this.companyTypeList.push(obj);
            }.bind(this));
            this.companyTypeList = _.uniq(this.companyTypeList, 'nomeAzienda');
            this.companyTypeList = _.sortByOrder(this.companyTypeList, 'nomeAzienda');
            this.adminFilterModel.setProperty("/companyType", this.companyTypeList);
        }

        var ids = this._getIdLabelInfo("consultants");
        var list = sap.ui.getCore().getElementById(ids.idLista);
        this._setNumeroSelezioni(list, ids.idLabel, ids.idInfoTollbar);

        var idsBu = this._getIdLabelInfo("buList");
        var listBu = sap.ui.getCore().getElementById(idsBu.idLista);
        this._setNumeroSelezioni(listBu, idsBu.idLabel, idsBu.idInfoTollbar);

        var idsSkill = this._getIdLabelInfo("skills");
        var listSkill = sap.ui.getCore().getElementById(idsSkill.idLista);
        this._setNumeroSelezioni(listSkill, idsSkill.idLabel, idsSkill.idInfoTollbar);

        var idsCompanyType = this._getIdLabelInfo("companyType");
        var listCompanyType = sap.ui.getCore().getElementById(idsCompanyType.idLista);
        this._setNumeroSelezioni(listCompanyType, idsCompanyType.idLabel, idsCompanyType.idInfoTollbar);

        // this.adminFilterModel.refresh();
    },

    _setFilterProperty: function (propertyName, key, model, modelKey) {
        var schedulations = this.listAllConsultant;
        var schedulationItems = _.pluck(schedulations, key);

        if (propertyName.indexOf("/dayRange") >= 0) {
            var maxDay = _.max(schedulationItems) ? _.max(schedulationItems) : 31;
            if (!this.adminFilterModel.getProperty("/dayRange")) {
                this._initializeDayCountRange();
            } else {
                this.adminFilterModel.setProperty("/dayRange/dayMax", maxDay);
                var filterValue = maxDay;
                this.adminFilterModel.setProperty("/dayRange/val", filterValue);
            }
        } else {
            var selectedItems = this._getFilterSelectedItems(propertyName);
            if (propertyName.indexOf("/skills") >= 0) {
                var skills = [];
                for (var i = 0; i < schedulationItems.length; i++) {
                    if (schedulationItems[i]) {
                        var singleSkills = schedulationItems[i].split(",");
                        singleSkills = _.map(singleSkills, function (item) {
                            return item.trim();
                        });
                        skills = skills.concat(singleSkills);
                    }
                }
                schedulationItems = skills;
            }
            if (modelKey !== "codiceBu") {
                var values = _.filter(this.getView().getModel(model).getProperty("/results"), _.bind(function (item) {
                    return _.contains(schedulationItems, item[modelKey]);
                }, this));

                values = _.map(values, _.bind(function (val) {
                    var selectedKeys = _.pluck(selectedItems, modelKey);
                    val.selected = _.contains(selectedKeys, val[modelKey]);
                    return val;

                }, this));
            } else {
                var values = this.getView().getModel(model).getProperty("/results");
            }
            this.adminFilterModel.setProperty(propertyName, values);
        }
    },

    _initializeDayCountRange: function () {
        //        var schedulations = this.allScheduling;
        var schedulations = this.umSchedulingModel.getProperty("/schedulazioni");
        var schedulationItems = _.pluck(schedulations, "giorniScheduling");

        var maxDay = _.max(schedulationItems) ? _.max(schedulationItems) : 31;
        var dayRange = {
            "dayMin": parseFloat(0),
            "dayMax": parseFloat(maxDay),
            "val": parseFloat(maxDay)
        }
        this.adminFilterModel.setProperty("/dayRange", dayRange);
    },

    _getFilterSelectedItems: function (prop) {
        var items = this.adminFilterModel.getProperty(prop);
        if (prop == "/dayRange")
            return parseFloat(items.val);

        var selectedItems = [];
        if (items) {
            selectedItems = _.filter(items, function (item) {
                return item.selected;
            })
        }
        return selectedItems;
    },

    _resetFilterSelectedItems: function () {
        var _resetPropertyItems = function (prop) {
            var items = this.adminFilterModel.getProperty(prop);

            if (prop.indexOf("/dayRange") >= 0) {
                this._initializeDayCountRange();
                return;
            }

            items = _.map(items, function (obj) {
                obj.selected = false;
                return obj;
            });
            this.adminFilterModel.setProperty(prop, items);
        }
        _resetPropertyItems = _.bind(_resetPropertyItems, this);

        for (var prop in this.adminFilterModel.getData()) {

            _resetPropertyItems("/" + prop);
        }
        this.adminFilterModel.refresh();
        this.companyTypeList = [];
        this.adminFilterModel.setProperty("/companyType", this.companyTypeList);
        this.adminFilterModel.setProperty("/selectedTab", "consultants");

    },

    //    _filterFunc: function (res) {
    //        var schedulations = this.allSchedulingForFilter;
    //        var selectedConsultants = this._getFilterSelectedItems("/consultants");
    //        var selectedConsultantsCodes = selectedConsultants && selectedConsultants.length > 0 ? _.pluck(selectedConsultants, 'codConsulente') : false;
    //
    //        var selectedBUnits = this._getFilterSelectedItems("/buList");
    //        var selectedBUnitsCodes = selectedBUnits && selectedBUnits.length > 0 ? _.pluck(selectedBUnits, 'codiceBu') : false;
    //
    //        var selectedSkills = this._getFilterSelectedItems("/skills");
    //        var selectedSkillsNames = selectedSkills && selectedSkills.length > 0 ? _.pluck(selectedSkills, 'nomeSkill') : false;
    //
    //        var filteredSchedulations = _.filter(schedulations, _.bind(function (item) {
    //            var result = true;
    //
    //            if (selectedConsultantsCodes) {
    //                result = _.contains(selectedConsultantsCodes, item.codiceConsulente);
    //            }
    //            if (!result)
    //                return false;
    //
    //            if (selectedSkillsNames) {
    //                var itemSkills = item.nomeSkill.split(",");
    //                var found = false;
    //
    //                for (var i = 0; i < itemSkills.length; i++) {
    //                    itemSkills[i] = itemSkills[i].trim();
    //                    found = _.contains(selectedSkillsNames, itemSkills[i]);
    //                    if (found)
    //                        break;
    //                }
    //                if (!found)
    //                    result = false;
    //
    //            }
    //            if (!result)
    //                return false;
    //
    //            if (selectedBUnitsCodes) {
    //                result = _.contains(selectedBUnitsCodes, item.cod_consulenteBu);
    //            }
    //            if (!result)
    //                return false;
    //
    //            var itemDay = item.giorniScheduling;
    //            result = (itemDay >= this.adminFilterModel.getProperty("/dayRange/dayMin") && itemDay <= this.adminFilterModel.getProperty("/dayRange/val"));
    //            return result;
    //
    //        }, this));
    //
    //        this.pmSchedulingModel.setProperty("/schedulazioni", filteredSchedulations);
    //        //         this.pmSchedulingModel.setProperty("/nRighe", filteredSchedulations.length);
    //        if (filteredSchedulations.length > 0 && filteredSchedulations.length <= 6) {
    //            this.pmSchedulingModel.setProperty("/nRighe", filteredSchedulations.length);
    //        } else if (filteredSchedulations.length > 6) {
    //            this.pmSchedulingModel.setProperty("/nRighe", 6);
    //        } else {
    //            this.pmSchedulingModel.setProperty("/nRighe", 1);
    //        }
    //        setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));
    //    },

    onFilterSelectAllItems: function (evt) {
        var src = evt.getSource();

        var tab = this.adminFilterModel.getProperty("/selectedTab");
        var text = src.getText();

        var op = !this.adminFilterModel.getProperty("/allSelected");

        this._setPropertyItems(tab, op);

        this.adminFilterModel.setProperty("/allSelected", this._checkAllSelectedFilterItems(tab));
        var ids = this._getIdLabelInfo(tab);
        var list = sap.ui.getCore().getElementById(ids.idLista);
        this._setNumeroSelezioni(list, ids.idLabel, ids.idInfoTollbar);

        this.adminFilterModel.refresh();
    },

    onFilterTabSelect: function (evt) {
        var src = evt.getSource();
        var tab = src.getSelectedKey();
        if (tab !== "days" && src.getExpanded()) {
            var ids = this._getIdLabelInfo(tab);
            var list = sap.ui.getCore().getElementById(ids.idLista);
            var items = list.getItems();
            var itemsSelected = list.getSelectedItems();
            if (items.length === itemsSelected.length) {
                this.adminFilterModel.setProperty("/allSelected", true);
            } else {
                this.adminFilterModel.setProperty("/allSelected", false);
            }
        }
    },

    _setPropertyItems: function (propertyName, selectedBool) {
        var property = "/" + propertyName;
        var items = this.adminFilterModel.getProperty(property);

        if (property.indexOf("/dayRange") >= 0) {
            this._initializeDayCountRange();
            return;
        }

        items = _.map(items, function (obj) {
            obj.selected = selectedBool;
            return obj;
        });
        this.adminFilterModel.setProperty(property, items);
    },

    _checkAllSelectedFilterItems: function (propertyName) {
        var property = "/" + propertyName;
        var items = this.adminFilterModel.getProperty(property);
        var falseItem = _.find(items, function (item) {
            return !item.selected;
        })
        var result = falseItem ? false : true;

        return result;
    },

    onSelectionChange: function (oEvt) {
        var oList = oEvt.getSource();
        var idLista = oList.getId();
        var idLabel, idInfoTollbar;
        var ids = this._getIdLabelInfo(idLista);
        this._setNumeroSelezioni(oList, ids.idLabel, ids.idInfoTollbar);
    },

    _getIdLabelInfo: function (idLista) {
        switch (idLista) {
            case "idListaConsulentiAdmin":
            case "consultants":
                idLabel = "idFilterLabelConsultant";
                idInfoTollbar = "idInfoToolbarConsultant";
                idLista = "idListaConsulentiAdmin";
                break;
            case "idListaBuAdmin":
            case "buList":
                idLabel = "idFilterLabelBu";
                idInfoTollbar = "idInfoToolbarBu";
                idLista = "idListaBuAdmin";
                break;
            case "idListaSkillAdmin":
            case "skills":
                idLabel = "idFilterLabelSkill";
                idInfoTollbar = "idInfoToolbarSkill";
                idLista = "idListaSkillAdmin";
                break;
            case "idListaTipoAzienda":
            case "companyType":
                idLabel = "idTipoAziendaLabel";
                idInfoTollbar = "idInfoToolbarTipoAzienda";
                idLista = "idListaTipoAzienda";
                break;
            default:
                return;
        }
        var obj = {};
        obj.idLabel = idLabel;
        obj.idInfoTollbar = idInfoTollbar;
        obj.idLista = idLista;
        return obj;
    },

    _setNumeroSelezioni: function (oList, idLabel, idInfoTollbar) {
        var oLabel = sap.ui.getCore().getElementById(idLabel);
        var oInfoToolbar = sap.ui.getCore().getElementById(idInfoTollbar);
        //        var aContexts = oList.getSelectedContexts(true);
        var aContexts = oList.getSelectedItems();
        var bSelected = (aContexts && aContexts.length > 0);
        var sText = (bSelected) ? aContexts.length + " selected" : null;
        oInfoToolbar.setVisible(bSelected);
        oLabel.setText(sText);
    },

    onFilterAdministratorDialogOK: function (evt) {
        //        this._filterFunc();
        utils.Busy.show("Ricaricamento Dati Real Time");
        //        var fSuccess = function () {
        //            utils.Busy.hide();
        //            this.adminDialog.close();
        //        }
        //        fSuccess = _.bind(fSuccess, this);

        var req = _.clone(this.ric);

        var selectedConsultant = this._getFilterSelectedItems("/consultants");
        var selectedBunits = this._getFilterSelectedItems("/buList");
        var selectedSkills = this._getFilterSelectedItems("/skills");
        var selectedSchedulingDays = this._getFilterSelectedItems("/dayRange");
        var selectedCompanyType = this._getFilterSelectedItems("/companyType");
        //        if (selectedConsultant && selectedConsultant.length === 1 && selectedBunits.length === 0 && selectedSkills.length === 0) {
        //            req.selectedConsultantsCode = selectedConsultant[0].codConsulente;
        //            req.idStaff = "CSC0000031";
        //        }
        //        if (selectedConsultant && selectedConsultant.length > 1) {
        if (selectedConsultant) {
            var arrayConsultants = [];
            for (var i = 0; i < selectedConsultant.length; i++) {
                arrayConsultants.push(selectedConsultant[i].codConsulente);
            }
            req.arrayConsultants = arrayConsultants;
        }
        //        if (!selectedConsultant || selectedConsultant.length !== 1) {
        if (selectedBunits.length !== 0) {
            var arrayBu = [];
            for (var i = 0; i < selectedBunits.length; i++) {
                arrayBu.push(selectedBunits[i].codiceBu);
            }
            req.arrayBu = arrayBu;
        }

        if (selectedSkills.length !== 0) {
            var arraySkill = [];
            for (var i = 0; i < selectedSkills.length; i++) {
                arraySkill.push(selectedSkills[i].codSkill);
            }
            req.arraySkill = arraySkill;
        }

        if (selectedSchedulingDays) {
            req.selectedSchedulingDays = selectedSchedulingDays;
        }

        if (selectedCompanyType.length !== 0) {
            var arrayCompany = [];
            for (var i = 0; i < selectedCompanyType.length; i++) {
                arrayCompany.push(selectedCompanyType[i].key);
            }
            req.selectedCompanyType = arrayCompany;
        }

        this.richiestaConFiltri = req;
        this.leggiTabella(req)
            .then(_.bind(function () {
                utils.Busy.hide();
                this.adminDialog.close();
            }, this), _.bind(function () {
                utils.Busy.hide();
            }, this));
    },

    onSliderChange: function (evt) {
        var value = parseFloat(evt.getSource().getValue());
        this.adminFilterModel.setProperty("/dayRange/val", value);
    },

    onSliderLiveChange: function (evt) {
        var value = parseFloat(evt.getSource().getValue());
        this.adminFilterModel.setProperty("/dayRange/val", value);
    },

    // funzioni per apertura visualizzazione di PJM e ALL_USER
    onPressPjmRelease: function (evt) {
        var e = evt.getSource();
        var type = e.getType();
        var rilascioPjm = "";
        var title = this._getLocaleText("CONFIRM_TITLE");
        var text = "";
        if (type === "Emphasized") {
            rilascioPjm = "X";
            text = this._getLocaleText("CONFIRM_X_PJM");
        } else {
            text = this._getLocaleText("CONFIRM_NOT_X_PJM");
        }
        sap.m.MessageBox.show(text, {
            icon: sap.m.MessageBox.Icon.INFORMATION,
            title: title,
            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            onClose: _.bind(function (oAction) {
                if (oAction === "YES") {
                    if (rilascioPjm === "X") {
                        this._checkConflitti(this.periodo, rilascioPjm);
                        //                            .then(this._chimataOdataModificaPeriodo(rilascioPjm, undefined), function (err) {console.log("errore"); return;});
                    } else {
                        this._chimataOdataModificaPeriodo(rilascioPjm, undefined);
                    }
                } else {
                    return;
                }
            }, this),
        });
    },

    onPressAllRelease: function (evt) {
        if (this.visibleModel.getProperty("/pjmReleasePressed")) {
            this._enterErrorFunction("information", "Attenzione", "Per procedere al rilascio degli ALL USER, bisogna prima rilasciare per i PJM");
            return;
        }

        var e = evt.getSource();
        var type = e.getType();
        var rilascioAll = "";
        var title = this._getLocaleText("CONFIRM_TITLE");
        var text = "";
        if (type === "Emphasized") {
            rilascioAll = "X";
            text = this._getLocaleText("CONFIRM_X_ALL");
        } else {
            text = this._getLocaleText("CONFIRM_NOT_X_ALL");
        }
        sap.m.MessageBox.show(text, {
            icon: sap.m.MessageBox.Icon.INFORMATION,
            title: title,
            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            onClose: _.bind(function (oAction) {
                if (oAction === "YES") {
                    this._chimataOdataModificaPeriodo(undefined, rilascioAll);
                } else {
                    return;
                }
            }, this),
        });
    },

    _checkConflitti: function (periodo, rilascioPjm) {
        var fSuccess = function (res) {
            var ris = JSON.parse(res);
            if (ris.res === 100) {
                this._chimataOdataModificaPeriodo(rilascioPjm, undefined)
            } else {
                this._enterErrorFunction("information", "Attenzione", "Per procedere al rilascio bisogna prima risolvere tutti i conflitti");
            }
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore nella lettura del servizio 'checkAdminConflicts'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.checkConflittiInFinal(periodo, fSuccess, fError);
    },

    _chimataOdataModificaPeriodo: function (rilascioPjm, rilascioAll) {
        var fSuccess = function () {
            this.oDialog.setBusy(false);
            this.readPeriods();
            this.svuotaTabella();
            this.svuotaSelect();
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");
            this.visibleModel.setProperty("/closeProjectButton", false);
            this.visibleModel.setProperty("/filterButton", false);
            this.visibleModel.setProperty("/comboBoxCommesse", false);
            this.visibleModel.setProperty("/chargeButton", false);
            this.visibleModel.setProperty("/giornateTotali", false);
            this.visibleModel.setProperty("/pjmRelease", false);
            this.visibleModel.setProperty("/allRelease", false);
            this.getView().byId("idButtonSelezionaCommessa").setPressed(false);
            this.getView().byId("refreshUM").setEnabled(false);
            this.getView().byId("idSchedulazioniTableAdmin").setVisible(false);
            this.visibleModel.setProperty("/sceltaCaricamento", false);
            this.visibleModel.setProperty("/caricaTutto", true);
            this.svuotaLocalStorageUndo();
            this.visibleModel.setProperty("/undoButton", false);
        };

        var fError = function () {
            this.oDialog.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);


        var entity = JSON.parse(sessionStorage.getItem("periodSelected"));
        entity.rilascioPjm = rilascioPjm !== undefined ? rilascioPjm : entity.rilascioPjm;
        entity.rilascioAll = rilascioAll !== undefined ? rilascioAll : entity.rilascioAll;
        entity.dataFine = this._dateConverter(entity.endDate);
        entity.dataFineRichieste = this._dateConverter(entity.reqEndDate);
        entity.dataInizio = this._dateConverter(entity.startDate);

        this.oDialog.setBusy(true);
        model.odata.chiamateOdata.updatePeriodToRelease(entity, fSuccess, fError);
    },

    _dateConverter: function (data) {
        var d = new Date(data);
        if (!_.isDate(d))
            return;

        var dateConverted = d.getFullYear() + '-' +
            ('0' + (d.getMonth() + 1)).slice(-2) + '-' +
            ('0' + d.getDate()).slice(-2);

        return dateConverted;
    },


    svuotaLocalStorageUndo: function () {
        var modelLocal = model.persistence.Storage.local;
        for (var i = 1; i < 6; i++) {
            var nameToSave = "undoAdmin_";
            nameToSave = nameToSave + i;
            if (modelLocal.get(nameToSave)) {
                modelLocal.remove(nameToSave);
            }
        }
    },

    // funzione che salva in localStorage le richieste
    /*
        request TYPE:
        * del -> salva la richiesta cancellata
        * insert -> salva la richiesta cancellata
        * move -> salva la richiesta da spostare
    */
    saveToLocalForUndo: function (requestType, dataToSave) {
        var modelLocal = model.persistence.Storage.local;
        var a = {};
        a[requestType] = dataToSave;
        for (var i = 1; i < 6; i++) {
            var nameToSave = "undoAdmin_";
            nameToSave = nameToSave + i;
            if (!modelLocal.get(nameToSave)) {
                modelLocal.save(nameToSave, a);
                return;
            } else if (i === 5) {
                modelLocal.save("undoAdmin_1", modelLocal.get("undoAdmin_2"));
                modelLocal.save("undoAdmin_2", modelLocal.get("undoAdmin_3"));
                modelLocal.save("undoAdmin_3", modelLocal.get("undoAdmin_4"));
                modelLocal.save("undoAdmin_4", modelLocal.get("undoAdmin_5"));
                modelLocal.save("undoAdmin_5", a);
            }
        }
    },

    // funzione lanciata alla pressione del tasto annulla
    undoPress: function () {
        var modelLocal = model.persistence.Storage.local;
        for (var i = 5; i > 0; i--) {
            var nameToSaveOR = "undoAdmin_";
            var nameToSave = nameToSaveOR + i;
            if (modelLocal.get(nameToSave)) {
                var dataToRestore = modelLocal.get(nameToSave);
                this.fromSaveLocal = false;
                if (modelLocal.get(nameToSave).del) {
                    //                    this.fromSaveLocal = false;
                    var oggetto = modelLocal.get(nameToSave).del;
                    if (oggetto.oldIdReqDraft === 1) {
                        oggetto.id_req = "";
                    }
                    oggetto.statoReqDraft = "CONFIRMED";
                    //                    oggetto.giorno = oggetto.giornoPeriodoDraft;
                    if (oggetto.id_req !== "") {
                        this.reInsertRequest(oggetto);
                    } else {
                        this.saveRequest(oggetto);
                    }
                } else if (modelLocal.get(nameToSave).move) {
                    this.undoMove = true;
                    var oggettoToMove = modelLocal.get(nameToSave).move;
                    oggettoToMove.oldGiorno = oggettoToMove.giornoPeriodoDraft;
                    oggettoToMove.giornoPeriodoDraft = oggettoToMove.newGiorno;
                    delete oggettoToMove.newGiorno;
                    model.persistence.Storage.local.save("move", oggettoToMove);
                    this._delData(oggettoToMove);
                } else {
                    //                    this.fromSaveLocal = false;
                    this._delData(modelLocal.get(nameToSave).insert);
                    var nameToSaveInf = nameToSaveOR + (i - 1);
                }
                modelLocal.remove(nameToSave);
                jQuery.sap.delayedCall(500, this, function () {
                    this.fromSaveLocal = true;
                });
                if (i === 1) {
                    this.visibleModel.setProperty("/undoButton", false);
                }
                return;
            }
        }
    },
    /* FINE funzioni per la gestione dell' 'annulla' */


    /** gestione Locked */
    onFilterStatoPress: function () {
        var page = this.getView().byId("schedulingAdministratorPage");
        if (!this.umFilterLockedDialog) {
            this.umFilterLockedDialog = sap.ui.xmlfragment("view.dialog.filterDialogLockedUM", this);
            page.addDependent(this.umFilterLockedDialog);
        }

        var stato = [{
            "value": "TUTTI"
        }, {
            "value": "BLOCCATI"
        }, {
            "value": "ATTIVI"
        }];
        this.filterLockedModel.setData(stato);
        this.umFilterLockedDialog.open();
        if (this.umSchedulingModel.getProperty("/filtroLocked") === "0") {
            var lis = sap.ui.getCore().getElementById("idListaLockedUM");
            _.find(lis.getItems(), function (n) {
                if (n.getTitle() === "TUTTI")
                    lis.setSelectedItem(n);
            });
        } else if (this.umSchedulingModel.getProperty("/filtroLocked") === "1") {
            var lis = sap.ui.getCore().getElementById("idListaLockedUM");
            _.find(lis.getItems(), function (n) {
                if (n.getTitle() === "BLOCCATI")
                    lis.setSelectedItem(n);
            });
        } else {
            var lis = sap.ui.getCore().getElementById("idListaLockedUM");
            _.find(lis.getItems(), function (n) {
                if (n.getTitle() === "ATTIVI")
                    lis.setSelectedItem(n);
            });
        }
    },

    onSelectionChangeLocked: function (evt) {
        var oList = evt.getSource();
        this.listLocked = oList.getSelectedContexts(true);
    },

    onCloseLockedFilter: function () {
        this.umFilterLockedDialog.close();
        this.umFilterLockedDialog.destroy(true);
        this.umFilterLockedDialog = undefined;
    },

    onOkLockedFilter: function () {
        var path = parseInt(this.listLocked[0].getPath().split("/")[1]);
        var value = this.listLocked[0].getModel().getData()[path].value;
        if (value === "TUTTI") {
            this.pmSchedulingModel.setProperty("/schedulazioni", this.allScheduling);
            this.umSchedulingModel.setProperty("/filtroLocked", "0");
            //            this.leggiTabella(this.ric);
            if (this.richiestaConFiltri) {
                this.leggiTabella(this.richiestaConFiltri);
            } else {
                this.leggiTabella(this.ric);
            }
        } else if (value === "BLOCCATI") {
            //            this.pmSchedulingModel.setProperty("/schedulazioni", _.where(this.allScheduling, {
            //                isLocked: true
            //            }));
            //            this.umSchedulingModel.setProperty("/schedulazioni", _.where(this.allScheduling, {
            //                isLocked: true
            //            }));
            var listaBloccati = [];
            if (this.richiestaConFiltri) {
                listaBloccati = _.where(this.pmSchedulingModel.getProperty("/schedulazioni"), {
                    isLocked: true
                });
            } else {
                listaBloccati = _.where(this.allScheduling, {
                    isLocked: true
                });
            }
            this.pmSchedulingModel.setProperty("/schedulazioni", listaBloccati);
            this.umSchedulingModel.setProperty("/schedulazioni", listaBloccati);
            this.umSchedulingModel.setProperty("/filtroLocked", "1");
        } else {
            //            this.pmSchedulingModel.setProperty("/schedulazioni", _.where(this.allScheduling, {
            //                isLocked: false
            //            }));
            //            this.umSchedulingModel.setProperty("/schedulazioni", _.where(this.allScheduling, {
            //                isLocked: false
            //            }));
            var listaAttivi = [];
            if (this.richiestaConFiltri) {
                listaAttivi = _.where(this.pmSchedulingModel.getProperty("/schedulazioni"), {
                    isLocked: false
                });
            } else {
                listaAttivi = _.where(this.allScheduling, {
                    isLocked: false
                });
            }
            this.pmSchedulingModel.setProperty("/schedulazioni", listaAttivi);
            this.umSchedulingModel.setProperty("/schedulazioni", listaAttivi);
            this.umSchedulingModel.setProperty("/filtroLocked", "2");
        }
        if (this.pmSchedulingModel.getProperty("/schedulazioni").length > 0 && this.pmSchedulingModel.getProperty("/schedulazioni").length <= 6) {
            this.pmSchedulingModel.setProperty("/nRighe", this.pmSchedulingModel.getProperty("/schedulazioni").length);
        } else if (this.pmSchedulingModel.getProperty("/schedulazioni").length > 6) {
            this.pmSchedulingModel.setProperty("/nRighe", 6);
        } else {
            this.pmSchedulingModel.setProperty("/nRighe", 1);
        }

        this.onCloseLockedFilter();
        setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));
    },

    onFilterAdministratorDialogClose: function () {
        this.adminDialog.close();
    },

    // funzione per caricare le richieste di modifica
    loadChangeRequests: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFERMA_UPLOAD_MODIFICHE") + " " + this.chosedCom, {
            icon: sap.m.MessageBox.Icon.ALLERT,
            title: this._getLocaleText("CONFERMA_UPLOAD_MODIFICHE_TITLE"),
            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            onClose: _.bind(function (action) {
                if (action === "YES") {
                    this.oDialogTable.setBusy(true);
                    var array = [];
                    var arrayToDel = [];
                    var arrayToMod = [];
                    for (var i = 0; i < this.richiesteModifica.length; i++) {
                        var modifica = false;
                        var obj = {};
                        obj.codConsulente = this.richiesteModifica[i].codConsulente;
                        obj.idCommessa = this.richiesteModifica[i].codCommessa;
                        obj.codAttivitaRichiesta = this.richiesteModifica[i].codAttivita;
                        obj.idPeriodoSchedulingRic = this.richiesteModifica[i].idPeriodo;
                        obj.codPjm = this.richiesteModifica[i].codPjm;
                        obj.note = this.richiesteModifica[i].note;
                        obj.giorno = this.richiesteModifica[i].giorno;
                        obj.statoReq = "CONFIRMED";
                        obj.idReqOld = 0;
                        obj.idReqOriginale = this.richiesteModifica[i].idReqOriginale;
                        //map date
                        obj.dataInserimento = this.richiesteModifica[i].dataInserimento;
                        var datesplit = obj.dataInserimento.split("-");
                        var giorno = parseInt(datesplit[2]);
                        var mese = parseInt(datesplit[1]);
                        var anno = parseInt(datesplit[0]);
                        if (giorno < 9) {
                            giorno = "0" + giorno;
                        }
                        if (mese < 9) {
                            mese = "0" + mese;
                        }
                        obj.dataInserimento = anno + "-" + mese + "-" + giorno;

                        obj.dataReqPeriodo = this.richiesteModifica[i].dataRichiesta;
                        var datesplit = obj.dataReqPeriodo.split("-");
                        var giorno = parseInt(datesplit[2]);
                        var mese = parseInt(datesplit[1]);
                        var anno = parseInt(datesplit[0]);
                        if (giorno < 9) {
                            giorno = "0" + giorno;
                        }
                        if (mese < 9) {
                            mese = "0" + mese;
                        }
                        obj.dataReqPeriodo = anno + "-" + mese + "-" + giorno;

                        var tempCons = _.find(this.pmSchedulingModel.getProperty("/schedulazioni"), {
                            codiceConsulente: this.richiesteModifica[i].codConsulente
                        });
                        if (tempCons) {
                            var prop = "gg" + this.richiesteModifica[i].giorno;
                            var tempCommessa = _.find(this.pmSchedulingModel.getProperty("/commesse"), {
                                codCommessa: this.richiesteModifica[i].codCommessa
                            });
                            while (tempCons[prop]) {
                                if (tempCons[prop] === tempCommessa.nomeCommessa) {
                                    var req = prop.replace("gg", "req");
                                    obj.idReq = tempCons[req];
                                    modifica = true;
                                    break;
                                }
                                items = "a";
                                prop = prop + items;
                            }

                            if (this.richiesteModifica[i].codAttivita === "DELETE") {
                                arrayToDel.push(obj);
                            } else if (this.richiesteModifica[i].idReqOriginale !== 0 || modifica) {
                                arrayToMod.push(obj);
                            } else {
                                array.push(obj);
                            }
                        }
                    }

                    var objUpdate = {};
                    objUpdate.idPeriodo = this.periodo;
                    objUpdate.codCommessa = this.commessaSelezionata;

                    if (arrayToDel.length > 0 && arrayToMod.length === 0) {
                        model.tabellaSchedulingUM.deleteModifyInFDraft(arrayToDel)
                            .then(_.bind(function () {
                                model.tabellaSchedulingUM.insertModifyInFDraft(array)
                                    .then(_.bind(function () {
                                        model.SchedulingModifyRequests.updateStatoMulti(objUpdate)
                                            .then(_.bind(function () {
                                                this.visibleModel.setProperty("/loadChangeRequests", false);
                                                this.reloadPress();
                                            }, this), _.bind(function () {
                                                this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'updateStatoRichiesteModifica'");
                                            }, this));
                                    }, this), _.bind(function () {
                                        this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'scritturaMultiDraft'");
                                    }, this))
                            }, this), _.bind(function () {
                                this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'cancellazioneMultiDraft'");
                            }, this));
                    } else if (arrayToDel.length === 0 && arrayToMod.length > 0) {
                        model.tabellaSchedulingUM.modModifyInFDraft(arrayToMod)
                            .then(_.bind(function () {
                                model.tabellaSchedulingUM.insertModifyInFDraft(array)
                                    .then(_.bind(function () {
                                        model.SchedulingModifyRequests.updateStatoMulti(objUpdate)
                                            .then(_.bind(function () {
                                                this.visibleModel.setProperty("/loadChangeRequests", false);
                                                this.reloadPress();
                                            }, this), _.bind(function () {
                                                this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'updateStatoRichiesteModifica'");
                                            }, this));
                                    }, this), _.bind(function () {
                                        this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'scritturaMultiDraft'");
                                    }, this))
                            }, this), _.bind(function () {
                                this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'modificaMultiDraft'");
                            }, this));
                    } else if (arrayToDel.length > 0 && arrayToMod.length > 0) {

                    } else {
                        model.tabellaSchedulingAdmin.insertModifyInFinal(array)
                            .then(_.bind(function () {
                                model.SchedulingModifyRequests.updateStatoMulti(objUpdate)
                                    .then(_.bind(function () {
                                        this.visibleModel.setProperty("/loadChangeRequests", false);
                                        this.reloadPress();
                                    }, this), _.bind(function () {
                                        this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'updateStatoRichiesteModifica'");
                                    }, this));
                            }, this), _.bind(function () {
                                this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'scritturaMultiDraft'");
                            }, this));
                    }
                } else {
                    return;
                }
            }, this),
        });
    },

    // funzione per verificare la presenza di richieste di modifica
    checkChangeRequests: function () {
        var fSuccess = function (result) {
            if (result && result.items && result.items.length === 1) {
                if (result.items[0].stato === "CHIUSO") {
                    var fSuccessBis = function (result) {
                        if (result && result.items && result.items.length > 0) {
                            this.richiesteModifica = result.items;
                            this.visibleModel.setProperty("/loadChangeRequests", true);
                        } else {
                            this.visibleModel.setProperty("/loadChangeRequests", false);
                        }
                    };

                    var fErrorBis = function (err) {
                        this.richiesteModifica = undefined;
                        this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'periodsModifyRequestSet'");
                    };

                    fSuccessBis = _.bind(fSuccessBis, this);
                    fErrorBis = _.bind(fErrorBis, this);
                    var stato = "PROPOSED";
                    model.SchedulingModifyRequests.checkValueToUpload(periodo, commessa, stato)
                        .then(fSuccessBis, fErrorBis);
                } else {
                    this.richiesteModifica = undefined;
                    this.visibleModel.setProperty("/loadChangeRequests", false);
                }
            } else {
                return;
            }
        };

        var fError = function (err) {
            this.richiesteModifica = undefined;
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'profiloCommessaPeriodoSet'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var periodo = this.periodo;
        var commessa = "";
        if (model.persistence.Storage.session.get("commessa") && model.persistence.Storage.session.get("commessa").codCommessa) {
            commessa = model.persistence.Storage.session.get("commessa").codCommessa;
            //        var stato = "PROPOSED";
        }

        model.ProfiloCommessaPeriodo.readRecordByPeriodCommessa(periodo, commessa)
            .then(fSuccess, fError);


        //        model.SchedulingModifyRequests.checkValueToUpload(periodo, commessa, stato)
        //            .then(fSuccess, fError);
    },

    _checkRichiesteModifica: function () {
        var fSuccess = function (result) {
            if (result && result.res && result.res.length > 0) {
                this.visibleModel.setProperty("/checkChangeRequests", true);
            } else {
                this.visibleModel.setProperty("/checkChangeRequests", false);
            }
        };

        var fError = function (err) {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura XSJS 'checkUmRequestsModify'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var periodo = this.periodo
        var stato = "PROPOSED";

        model.tabellaSchedulingUM.checkListaCommesseConRichiesteModifica(periodo, undefined, stato)
            .then(fSuccess, fError);
    },

    checkListRequestsProposed: function () {
        var fSuccess = function (result) {
            if (result && result.res && result.res.length > 0) {
                this.listaCommesseRicModModel.setProperty("/dati", result.res);
                if (!this.dialogListaCommesseConRichiestaModifica) {
                    this.dialogListaCommesseConRichiestaModifica = sap.ui.xmlfragment("view.dialog.listaCommesseDiRichiesteModifica", this);
                    this.getView().addDependent(this.dialogListaCommesseConRichiestaModifica);
                }
                this.dialogListaCommesseConRichiestaModifica.open();
            } else {
                this._enterErrorFunction("information", "Richieste modifiche", "Nessuna richiesta di modifica");
            }
        };

        var fError = function (err) {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura XSJS 'checkUmRequestsModify'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var periodo = this.periodo;
        var stato = "PROPOSED";

        model.tabellaSchedulingUM.checkListaCommesseConRichiesteModifica(periodo, undefined, stato)
            .then(fSuccess, fError);
    },

    onCloseDialogListaCommesseRicMod: function () {
        this.dialogListaCommesseConRichiestaModifica.close();
    },

    clearPasteCommesse: function () {
        if (model.persistence.Storage.local.get("copy")) {
            model.persistence.Storage.local.remove("copy");
        }
        if (model.persistence.Storage.local.get("copyBis")) {
            model.persistence.Storage.local.remove("copyBis");
        }
        this.paste = false;
    },

    onViewConflictsPress: function (evt) {
        var req = {};
        if (this.richiestaConFiltri) {
            req = this.richiestaConFiltri;
        } else {
            req = this.ric;
        }
        req.conflitti = "X";
        this.leggiTabella(req);
        //        this.ric.conflitti = "X";
        //        this.leggiTabella(this.ric);
    },

    onNoteConsultantPress: function (evt) {
        if (!this._noteConsultantDialog) {
            this._noteConsultantDialog = sap.ui.xmlfragment("view.dialog.um_admin_noteConsultant", this);
            this.getView().addDependent(this._noteConsultantDialog);
        }
        var icon = evt.getSource();
        this.consultantRow = icon.getParent().getBindingContext("pmSchedulingModel").getObject();
        var consultantNote = "";
        if (this.consultantRow.notaConsulente) {
            consultantNote = this.consultantRow.notaConsulente;
        }
        this.noteConsultantModel.setData(consultantNote);
        this._noteConsultantDialog.openBy(icon);
    },

    onCancelConsultantNotePress: function () {
        this._noteConsultantDialog.close();
    },

    onSaveConsultantNotePress: function () {
        var period = this.periodo;
        var consultantCode = this.consultantRow.codiceConsulente;
        var umCode = "";
        var administratorCode = model.persistence.Storage.session.get("user").codConsulente;
        var consultantNote = this.noteConsultantModel.getData();
        var fSuccess = function (ris) {
            this._noteConsultantDialog.close();
            var req = {
                idPeriodo: this.periodo,
                selectedConsultantsCode: this.consultantRow.codiceConsulente,
                idCommessa: 'AA00000008',
                idStaff: _.find(this.allCommesse, {
                    codCommessa: 'AA00000008'
                }).codiceStaffCommessa,
                conflitti: this.ric.conflitti
            };
            this._leggiSingolo(req);
        }.bind(this);
        var fError = function (ris) {
            this.oDialogTable.setBusy(false);
            var title = this._getLocaleText("attention");
            var messageText = this._getLocaleText("savingError");
            this._enterErrorFunction("warning", title, messageText);
        }.bind(this);

        this.oDialogTable.setBusy(true);
        model.requestSave.insertConsultantNote(period, consultantCode, umCode, administratorCode, consultantNote)
            .then(fSuccess, fError);
    },

    // associazione consulente su consulente generico
    onPressAssignConsultant: function () {
        // lista consulenti generici del progetto
        var listaConsulentiGenerici = _.filter(this.umSchedulingModel.getProperty("/schedulazioni"), function (item) {
            if (item.codiceConsulente.indexOf("CCG") >= 0)
                return item
        });
        this.assignGenericConsultantModel.setProperty("/listaGenerici", listaConsulentiGenerici);
        this.uiModel.setProperty("/closeVisible", true);
        this.uiModel.setProperty("/backVisible", false);
        this.uiModel.setProperty("/forwardVisible", true);
        this.uiModel.setProperty("/associateVisible", false);
        this.uiModel.setProperty("/titleDialogAssociate", this._getLocaleText("LISTA_SKILL"));
        var page = this.getView().byId("schedulingAdministratorPage");
        if (!this.dialogConsulentiGenerici) {
            this.dialogConsulentiGenerici = sap.ui.xmlfragment("view.dialog.GenericListConsultants", this);
            page.addDependent(this.dialogConsulentiGenerici);
        }

        this.dialogConsulentiGenerici.open();

        this.navContainer = sap.ui.getCore().getElementById("navContainerDialog");
        this.animation = "door"; // door, fade, slip, show, slide
        this.navContainer.to(sap.ui.getCore().getElementById("idPageListGeneric"));
    },

    onPressClose: function () {
        var list = sap.ui.getCore().getElementById("idListaGenerici");
        if (list.getSelectedItem()) {
            list.removeSelections();
        }
        this.dialogConsulentiGenerici.close();
    },

    onPressBack: function () {
        this.navContainer.back();
        this.uiModel.setProperty("/closeVisible", true);
        this.uiModel.setProperty("/backVisible", false);
        this.uiModel.setProperty("/forwardVisible", true);
        this.uiModel.setProperty("/associateVisible", false);
        this.uiModel.setProperty("/titleDialogAssociate", this._getLocaleText("LISTA_SKILL"));
    },

    onPressForward: function () {
        var list = sap.ui.getCore().getElementById("idListaGenerici");
        if (!list.getSelectedItem()) {
            sap.m.MessageToast.show(this._getLocaleText("SELECT_ONE_SKILL"));
            return;
        }
        var skill = list.getSelectedItem().getTitle();
        var codGenerico = list.getSelectedItem().getDescription();
        this.assignGenericConsultantModel.setProperty("/nomeSkill", skill);
        this.assignGenericConsultantModel.setProperty("/codiceConsulenteGenerico", codGenerico);
        this.uiModel.setProperty("/closeVisible", false);
        this.uiModel.setProperty("/backVisible", true);
        this.uiModel.setProperty("/forwardVisible", false);
        this.uiModel.setProperty("/associateVisible", true);
        this.uiModel.setProperty("/titleDialogAssociate", this._getLocaleText("SELECT_SINGLE_CONSULTANT"));
        var fSuccess = function (result) {
            var staff = _.cloneDeep(this.umSchedulingModel.getProperty("/schedulazioni"));
            _.remove(staff, function (item) {
                if (item.codiceConsulente.indexOf("CCG") >= 0)
                    return item
            });
            for (var i = 0; i < _.size(staff); i++) {
                _.remove(result, {
                    codConsulente: staff[i].codiceConsulente
                });
            }
            var restrictedConsultant = _.cloneDeep(result);
            restrictedConsultant = _.filter(restrictedConsultant, {
                nomeSkill: skill
            });
            this.assignGenericConsultantModel.setProperty("/restrictedConsultant", restrictedConsultant);
            this.assignGenericConsultantModel.setProperty("/allConsultant", _.uniq(result, 'codConsulente'));
            this.navContainer.to(sap.ui.getCore().getElementById("idPageListConsultants"));
            this.assignGenericConsultantModel.setProperty("/key", "restricted");
            var listConsulenti = sap.ui.getCore().getElementById("consultantPerGenerico");
            if (listConsulenti.getSelectedItem()) {
                listConsulenti.removeSelections();
            }
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'consulenteSkill'");
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Consultants.getConsultantFromSkill().then(fSuccess, fError);
    },

    onSelectSegmentedButton: function (evt) {
        var source = evt.getSource();
        var key = source.getSelectedKey();
        this.assignGenericConsultantModel.setProperty("/key", key);
    },

    onPressAssociate: function () {
        // update anagrafica
        var codConsGen = this.assignGenericConsultantModel.getProperty("/codiceConsulenteGenerico");
        var list;
        if (this.assignGenericConsultantModel.getProperty("/key") === "restricted") {
            list = sap.ui.getCore().getElementById("consultantPerGenerico");
        } else {
            list = sap.ui.getCore().getElementById("consultantPerGenericoAll");
        }
        if (!list.getSelectedItem()) {
            sap.m.MessageToast.show(this._getLocaleText("SELECT_ONE_CONSULTANT"));
            return;
        }
        var codCons = list.getSelectedItem().getDescription();
        var codCommessa = this.commessaSelezionata;

        model.UpdateAnagStaffGenerico.updateAnagStaff(codConsGen, codCons, codCommessa)
            .then(_.bind(function (result) {
                // update draft
                model.UpdateAnagStaffGenerico.updateFinal(codConsGen, codCons, codCommessa, this.periodo)
                    .then(_.bind(function (result) {
                        this.dialogConsulentiGenerici.close();
                        sap.m.MessageToast.show(this._getLocaleText("AGGIORNAMENTO_RIUSCITO"));
                        this.leggiTabella(this.ric);
                    }, this), _.bind(function (error) {
                        console.log(error);
                    }, this));
            }, this), _.bind(function (error) {
                console.log(error);
            }, this));
    },

    // controllo se ci sono consulenti generici e faccio vedere il bottone
    _checkProjectsGeneric: function (scheduling) {
        var listaConsulentiGenerici = _.filter(scheduling, function (item) {
            if (item.codiceConsulente.indexOf("CCG") >= 0)
                return item
        });
        if (listaConsulentiGenerici && _.size(listaConsulentiGenerici) > 0) {
            this.visibleModel.setProperty("/checkProjectsGeneric", true);
        } else {
            this.visibleModel.setProperty("/checkProjectsGeneric", false);
        }
    },

    onPressCheckProjectsGeneric: function () {
        var params = {
            idPeriodo: this.periodo
        }
        model.tabellaSchedulingAdmin.getProjectListWithGenericConsultantFinal(params)
            .then(_.bind(function (res) {
                this.listaCommesseConsulentiGenericiModel.setData(JSON.parse(res).results);

                if (!this.dialogListaCommesseConsulentiGenerici) {
                    this.dialogListaCommesseConsulentiGenerici = sap.ui.xmlfragment("view.dialog.listaCommesseConsulentiGenerici", this);
                    this.getView().addDependent(this.dialogListaCommesseConsulentiGenerici);
                }
                this.dialogListaCommesseConsulentiGenerici.open();
            }, this), _.bind(function (err) {
                // errore
                console.log(err)
            }, this));
    },

    onCloseDialogListaCommesseConsGen: function () {
        this.dialogListaCommesseConsulentiGenerici.close();
    },

    // funzione per cancellare la richiesta "non schedulabile"
    onPressOffisteWork: function (evt) {
        this.fromSaveLocal = false
        var model = evt.getSource()._getPropertiesToPropagate().oBindingContexts.pmSchedulingModel;
        var path = model.getPath();
        var jsonConsulente = model.getModel().getProperty(path);
        var btn = evt.getSource().getId();
        var col = _.find(btn.split("-"), function (o) {
            if (o.indexOf("col") !== -1) {
                return o
            }
        });
        col = col.substr(3, col.length);
        var req = "req" + col;
        var idReqMod = "idReqMod" + col;
        var idCommessa = "idCommessa" + col;

        var dataToDel = {
            oldIdReqFinal: jsonConsulente[idReqMod],
            id_req: jsonConsulente[req],
            codConsulenteFinal: jsonConsulente.codiceConsulente,
            idCommessaFinal: jsonConsulente[idCommessa]
        };
        this._confirmDeleteOffsiteWork(dataToDel);
    },

    onPressOffisteWork2: function (evt) {
        this.fromSaveLocal = false
        var model = evt.getSource()._getPropertiesToPropagate().oBindingContexts.pmSchedulingModel;
        var path = model.getPath();
        var jsonConsulente = model.getModel().getProperty(path);
        var btn = evt.getSource().getId();
        var col = _.find(btn.split("-"), function (o) {
            if (o.indexOf("col") !== -1) {
                return o
            }
        });
        col = col.substr(3, col.length);
        var req = "req" + col + "a";
        var idReqMod = "idReqMod" + col + "a";
        var idCommessa = "idCommessa" + col + "a";

        var dataToDel = {
            oldIdReqFinal: jsonConsulente[idReqMod],
            id_req: jsonConsulente[req],
            codConsulenteFinal: jsonConsulente.codiceConsulente,
            idCommessaFinal: jsonConsulente[idCommessa]
        };
        this._confirmDeleteOffsiteWork(dataToDel);
    },

    _confirmDeleteOffsiteWork: function (dataToDel) {
        sap.m.MessageBox.show(this._getLocaleText("confermaEliminazioneOffsiteWork"), {
            icon: sap.m.MessageBox.Icon.ALLERT,
            title: this._getLocaleText("confermaEliminazioneOffsiteWorkTitle"),
            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            onClose: _.bind(function (action) {
                if (action === "YES") {
                    this._delData(dataToDel);
                } else {
                    return;
                }
            }, this),
        });
    },

    onPressConsulentiGenerici: function () {
        utils.Busy.show("Ricaricamento Dati Real Time");
        var fSuccess = function () {
            utils.Busy.hide();
            this.adminDialog.close();
        }
        fSuccess = _.bind(fSuccess, this);

        var req = _.clone(this.ric);
        req.consulentiGenerici = "X";

        this.richiestaConFiltri = req;

        this.leggiTabella(req)
            .then(fSuccess);
    },

    /* DOWNOLAD EXCEL HTML*/
    // la funzione scarica quello che vedo a video // 
    onDownloadExcelPress: function () {
        var commessaScelta = this.chosedCom;
        var columns = [];
        var item = [];
        var rows = [];
        var column = {};
        var days = this.arrayGiorni;
        var daysLength = days.length;
        var schedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni");
        var schedulazioniLength = schedulazioni.length;
        for (var i = -1; i < daysLength; i++) {
            if (i === -1) {
                column = {
                    "name": this._getLocaleText("CONSULENTI")
                };
            } else {
                column = {
                    "name": days[i]
                };
            }
            columns.push(column);
        }

        for (var j = 0; j < schedulazioniLength; j++) {
            var row = [];
            for (var i = -1; i < daysLength; i++) {
                var item = "";
                if (i === -1) {
                    item = schedulazioni[j].nomeConsulente + ": " + schedulazioni[j].giorniScheduling;
                } else {
                    var index = i + 1;
                    var property = "gg" + index;
                    var propertyA = "gg" + index + "a";
                    var icon = "ggIcon" + index;
                    var iconA = "ggIcon" + index + "a";

                    var css = "";
                    switch (schedulazioni[j][icon]) {
                        case "sap-icon://complete":
                            css = "EXassign";
                            break;
                        case "sap-icon://time-entry-request":
                            css = "EXhalfDay";
                            break;
                        case "sap-icon://customer-and-contacts":
                            css = "EXwCons";
                            break;
                        case "sap-icon://employee-rejections":
                            css = "EXwNonCons";
                            break;
                        case "sap-icon://collaborate":
                            css = "EXwCust";
                            break;
                        case "sap-icon://offsite-work":
                            css = "EXwNonCust";
                            break;
                        case "sap-icon://role":
                            css = "EXnotConfirmed";
                            break;
                        case "sap-icon://globe":
                            css = "EXabroad";
                            break;
                    }
                    // ricerco le comesse di ogni consulente in tutte le commesse
                    var com = _.find(this.allCommesse, {
                        nomeCommessa: schedulazioni[j][property]
                    });
                    var comA = _.find(this.allCommesse, {
                        nomeCommessa: schedulazioni[j][propertyA]
                    });
                    if (com && !comA) {
                        item = schedulazioni[j][property] + "_" + css;
                    } else if (!com && comA) {
                        item = schedulazioni[j][propertyA] + "_" + css;
                    } else if (com && comA) {
                        item = schedulazioni[j][property] + "\n" + (schedulazioni[j][propertyA] + "_" + css);
                        // Aggiungo un controllo se in una giornata è presente un conflitto
                        if (css !== "EXhalfDay" && css !== "EXwNonCust") {
                            css = "EXwCust"
                            item = "*CONF*" + "_" + css;
                        }
                    }
                }
                row.push(item);
            }
            rows.push(row);
        }

        this.completeModel = new sap.ui.model.json.JSONModel();
        this.completeModel.setData({
            rows: rows,
            columns: columns
        });

        if (!this.excelDialog) {
            this.excelDialog = sap.ui.xmlfragment("view.dialog.excelDownload", this);
            this.getView().addDependent(this.excelDialog);
        }
        this.excelDialog.open();
    },

    afterExcelDialog: function () {
        this.excelDialog.setBusy(true);
        this.visibleModel.setProperty("/visibleExcelDialogButtons", false);
        var activityForm = sap.ui.getCore().getElementById("idExcelDialog");
        if (this.completeList) {
            activityForm.removeContent(this.completeList);
            this.completeList.destroy();
        }
        this.completeList = new sap.m.List({
            id: "listaSchedulazioni",
            showSeparators: "All"
        });

        var columns = this.completeModel.getData().columns;

        for (var i = 0; i < columns.length; i++) {
            var minWidth = "15px";
            if (i === 0) {
                minWidth = "50px";
            }
            var columnName = columns[i].name;
            var splitColumnName = columnName.split(" ");
            if (splitColumnName[1] === "Mar") {
                splitColumnName[1] = "Mart"
                columnName = splitColumnName[0] + " " + splitColumnName[1];
            }
            this.completeList.addColumn(new sap.m.Column({
                width: minWidth,
                hAlign: "Left",
                vAlign: "Middle",
                header: new sap.m.Label({
                    text: columnName,
                    design: sap.m.LabelDesign.Bold
                })
            }));
        }

        var rows = this.completeModel.getData().rows;
        for (var j = 0; j < rows.length; j++) {
            var item = new sap.m.ColumnListItem({
                width: "100%"
            });
            for (var k = 0; k < rows[j].length; k++) {
                if (rows[j][k] !== "") {
                    var tx1 = rows[j][k].split("_")[0];
                    var tx2 = rows[j][k].split("_")[1];
                    item.addCell(new sap.m.Text({
                        text: tx1
                    }));
                    item.getCells()[k].addStyleClass(tx2);
                } else {
                    item.addCell(new sap.m.Text({
                        text: rows[j][k]
                    }));
                }
            }
            this.completeList.addItem(item);
        }
        activityForm.addContent(this.completeList);

        if (this.legend) {
            activityForm.removeContent(this.legend);
            this.legend.destroy();
        }
        this.legend = new sap.m.List({
            id: "legendaSchedulazioni",
            showSeparators: "All"
        });

        for (var i = 0; i < 8; i++) {
            var minWidth = "12.50%";
            this.legend.addColumn(new sap.m.Column({
                width: minWidth,
                hAlign: "Center",
                vAlign: "Middle",
            }));
        }

        var item = new sap.m.ColumnListItem({
            width: "100%"
        });
        item.addCell(new sap.m.Text({
            text: "Assegnata"
        }));
        item.getCells()[0].addStyleClass("EXassign");
        item.addCell(new sap.m.Text({
            text: "Mezza giornata"
        }));
        item.getCells()[1].addStyleClass("EXhalfDay");
        item.addCell(new sap.m.Text({
            text: "Concomitanza"
        }));
        item.getCells()[2].addStyleClass("EXwCons");
        item.addCell(new sap.m.Text({
            text: "Non concomitanza"
        }));
        item.getCells()[3].addStyleClass("EXwNonCons");

        item.addCell(new sap.m.Text({
            text: "Fissata"
        }));
        item.getCells()[4].addStyleClass("EXwCust");
        item.addCell(new sap.m.Text({
            text: "Non schedulabile"
        }));
        item.getCells()[5].addStyleClass("EXwNonCust");
        item.addCell(new sap.m.Text({
            text: "Non confermata"
        }));
        item.getCells()[6].addStyleClass("EXnotConfirmed");
        item.addCell(new sap.m.Text({
            text: "Estero"
        }));
        item.getCells()[7].addStyleClass("EXabroad");
        this.legend.addItem(item);
        activityForm.addContent(this.legend);

        jQuery.sap.delayedCall(1500, this, function () {
            this.excelDialog.setBusy(false);
            this.createHTMLTable(".xls");
        });
    },

    onDownloadDialogPress: function (oEvent) {
        this.createHTMLTable(".xls");
    },

    createHTMLTable: function (extension) {
        var commessaScelta = this.chosedCom;
        var tab_text = "<table width='100%' border='2px'><tr bgcolor='#87AFC6'>";
        var textRange;
        var j = 0;
        var tab = document.getElementById('listaSchedulazioni-listUl'); // id of table

        for (j = 0; j < tab.rows.length; j++) {
            var tabHeader = tab.rows[j];
            var childrens;
            var children;

            childrens = tabHeader.childNodes;
            this.numeroColonne = childrens.length;
            if (tabHeader.id === "listaSchedulazioni-tblHeader") {
                for (var i = 0; i < childrens.length; i++) {
                    if (childrens[i].id === "listaSchedulazioni-tblHead0") {
                        childrens[i].style.width = "150px";
                        childrens[i].style.columnWidth = "150px";
                        childrens[i].attributes.style = "width: 150px: text-align: left";
                    } else {
                        childrens[i].style.width = "50px";
                        childrens[i].style.columnWidth = "50px";
                        childrens[i].attributes.style = "width: 50px: text-align: left";
                    }
                }
            } else {
                for (var i = 0; i < childrens.length - 1; i++) {
                    var xx = childrens[i].getElementsByTagName("span")[0];
                    if (xx.textContent !== "" && i !== 0) {
                        var classes = xx.getAttribute("class").split(" ");
                        var classeIteressata = classes[0];

                        var css = "";
                        switch (classeIteressata) {
                            case "EXassign":
                                css = "mediumpurple";
                                break;
                            case "EXhalfDay":
                                css = "orange";
                                break;
                            case "EXwCons":
                                css = "yellow";
                                break;
                            case "EXwNonCons":
                                css = "chartreuse";
                                break;
                            case "EXwCust":
                                css = "red";
                                break;
                            case "EXwNonCust":
                                css = "lightgrey";
                                break;
                            case "EXnotConfirmed":
                                css = "dodgerblue";
                                break;
                            case "EXabroad":
                                css = "lightskyblue";
                                break;
                        }
                        childrens[i].bgColor = css;
                    }
                }
            }
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
        }

        var tabLegenda = document.getElementById('legendaSchedulazioni-listUl');
        var tabHeader = tabLegenda.rows[1];
        var rigaInteressata = tabHeader.childNodes;
        for (var i = 0; i < rigaInteressata.length - 1; i++) {
            var xx = rigaInteressata[i].getElementsByTagName("span")[0];
            if (xx.textContent !== "") {
                var testo = xx.textContent;
                var css = "";
                switch (testo) {
                    case "Assegnata":
                        css = "mediumpurple";
                        break;
                    case "Mezza giornata":
                        css = "orange";
                        break;
                    case "Concomitanza":
                        css = "yellow";
                        break;
                    case "Non concomitanza":
                        css = "chartreuse";
                        break;
                    case "Fissata":
                        css = "red";
                        break;
                    case "Non schedulabile":
                        css = "lightgrey";
                        break;
                    case "Non confermata":
                        css = "dodgerblue";
                        break;
                    case "Estero":
                        css = "lightskyblue";
                        break;
                }
                rigaInteressata[i].bgColor = css;
            }
        }

        var st = tabLegenda.rows[1].innerHTML
        var str = "";
        var ff = "";
        for (var i = 0; i < st.split("</td>").length - 2; i++) {
            str = st.split("</td>")[i] + "</td>";
            ff = ff.concat(str)
        }

        var colspan = parseInt(this.numeroColonne / 8);
        tab_text = tab_text + "<tr><td colspan='" + this.numeroColonne + " border='0'></td></tr><tr><td><span>Legenda</span></td>" + ff.replace(/<td /g, "<td colspan='" + colspan + "' ") + "</tr>";

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
        {
            iframetxtArea1.document.open("txt/html", "replace");
            iframetxtArea1.document.write(tab_text);
            iframetxtArea1.document.close();
            iframetxtArea1.focus();
            sa = iframetxtArea1.document.execCommand("SaveAs", true, "Scheduling_" + (commessaScelta ? (commessaScelta + "_") : "") + this.getView().byId("idSelectPeriodo").getValue() + extension);
            return (sa);
            this.onCloseDialogPress();

        } else {
            var filename;
            switch (extension) {
                // case ".html":
                //     filename = "Scheduling_" + commessaScelta + extension;
                //     this.saveAsHTML(tab_text, filename);
                //     break;
                case ".xls":
                    filename = "Scheduling_" + (commessaScelta ? (commessaScelta + "_") : "") + this.getView().byId("idSelectPeriodo").getValue();
                    this.saveAsXLS(tab_text, filename);
                    break;
            }
        }
    },

    saveAsXLS: function (uri, filename) {
        var link = document.createElement('a');
        if (typeof link.download === 'string') {
            var file = new Blob([uri], {
                type: "application/vnd.ms-excel"
            });
            document.body.appendChild(link); // Firefox requires the link to be in the body
            link.download = filename;
            link.href = URL.createObjectURL(file);
            link.click();
            setTimeout(function () {
                document.body.removeChild(link); /*remove the link when done*/
            }, 2000);
        } else {
            location.replace(uri);
        }
        this.onCloseDialogPress();
    },

    onCloseDialogPress: function () {
        this.excelDialog.close();
    },

});