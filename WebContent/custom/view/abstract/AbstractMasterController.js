
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.declare("view.abstract.AbstractMasterController");

view.abstract.AbstractController.extend("view.abstract.AbstractMasterController", {
  


handleRouteMatched :function(evt){
view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
  
  this.uiModel.setProperty("/searchValue","");
  this.applyFilter();

},

  getFiltersOnList : function(evt)
  {
    var list = this.getView().byId("list");
    if(!list || !list.getBinding("items"))
      return null;

    return list.getBinding("items").aFilters;
  },
  



});
