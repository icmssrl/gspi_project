jQuery.sap.declare("view.abstract.AbstractController");
jQuery.sap.require("utils.Validator");
jQuery.sap.require("utils.ObjectUtils");
jQuery.sap.require("utils.debugMode");
jQuery.sap.require("utils.CalcoloPasqua");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.filters.Filter");

sap.ui.core.mvc.Controller.extend("view.abstract.AbstractController", {

    onInit: function () {
        this.router = sap.ui.core.UIComponent.getRouterFor(this);
        this.router.attachRoutePatternMatched(this.handleRouteMatched, this);

        var user = model.persistence.Storage.session.get("user");
        var userModel = new sap.ui.model.json.JSONModel(user);
        this.getView().setModel(userModel, "user");

        this.uiModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.uiModel, "ui");

        this.uiModelDialog = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.uiModelDialog, "uiD");
    },

    handleRouteMatched: function (evt) {
        this.nomeCommessaFerie = "feri";
        //**
        if (sap.ui.getCore().byId("actionSheet"))
            sap.ui.getCore().byId("actionSheet").destroy(true);
        //**
        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        var userLogged = model.persistence.Storage.session.get("user");
        if (userLogged) {
            this.user.tipoAzienda = userLogged.tipoAzienda;
        }
        if (!this.user) {
            if (sessionStorage.length !== 0) {
                this.onLogoutPress();
            }
            return;
        }
        if (!jQuery.device.is.desktop) {
            this.onLogoutPress();
            this._enterErrorFunction("information", "ATTENZIONE", "Questa applicazione può essere usata solo su computer");
            return;
        }
        // rimuove focus da bottone
        setTimeout(_.bind(this._removeFocus, this), 800);
    },

    _getLocaleText: function (key) {
        return this.getView().getModel("i18n").getProperty(key);
    },

    _checkRoute: function (evt, pattern) {
        if (pattern === "launchpad") {
            this.uiModel.setProperty("/backButtonVisible", false);
            this.uiModel.setProperty("/backToFtrButtonVisible", true);
        } else {
            this.uiModel.setProperty("/backButtonVisible", true);
            this.uiModel.setProperty("/backToFtrButtonVisible", false);
        }
        if (evt.getParameter("name") !== pattern) {
            return false;
        }
        this.uiModel.setProperty("/pageType", this._getLocaleText(pattern));
        return true;
    },

    onLogoutPress: function () {
        sessionStorage.clear();
        this.router.navTo("login");
    },

    onNavBackPress: function () {
        icms.MyRouter.myNavBack();
    },

    _removeFocus: function () {
        $(document).ready(function () {
            $(':focus').blur();
        });
    },

    // funzione unica per estrazione perdiodi
    _getPeriodsGlobal: function (filter, funcToLunch, reqFunc) {
        var fSuccess = function (result) {
            var res = {};
            _.remove(result.items, {
                stato: 'CHIUSO'
            });
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var data = new Date();

            var month = (data.getMonth() < 11) ? (data.getMonth() + 2) : 1;
            var year = (data.getMonth() < 11) ? data.getFullYear() : (data.getFullYear() + 1);

            _.find(res.items, {
                mesePeriodo: month,
                annoPeriodo: year
            }).current = "SI";

            var index = _.findIndex(res.items, {
                mesePeriodo: month,
                annoPeriodo: year
            });

            var res13 = {};
            res13.items = [];

            for (var i = ((index - 7) ? (index - 7) : 0); i < (index + 6); i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.periodModel.setData(res13);
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");

            this[funcToLunch](reqFunc);
        };
        var fError = function () {
            if (!this.oneTimeReadPeriods) {
                // funzione che ripete la chiamata di login nel caso in cui sia scaduta l'autenticazione al sistema
                this.oneTimeReadPeriods = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(this.readPeriods(), this));
            } else {
                this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'getPeriods'");
            }
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    // messaggio mostrato in caso di funzione andata in errore
    _enterErrorFunction: function (type, titolo, messaggio) {
        // type: warning, success, confirm, alert, error, information
        sap.m.MessageBox[type](messaggio, {
            title: titolo
        });
        if (this.oDialog && this.oDialog.getBusy() === true) {
            this.oDialog.setBusy(false);
        }
        if (this.oDialogTable && this.oDialogTable.getBusy() === true) {
            this.oDialogTable.setBusy(false);
        }
    },

    // funzione che calcola i giorni da inserire nell'header della tabella
    _calcoloGiorni: function (dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine) {
        var diffDays = (dataF.getTime() - dataI.getTime()) / (24 * 3600000) + 1;
        diffDays = Math.round(diffDays);
        var numColonne = diffDays + 1;
        this.numColonne = numColonne;

        this.arrayGiorni = [];
        var giornoSettimana = dataI.getDay();
        this.weekendDays = [];
        var numeroGiorniMese = "";
        var gg = 0;

        var arrayFestivita = this._calcolaArrayFestivita(annoInizio);
        var arrayFestivitaMese = [];
        if (mInizio !== mFine) {
            var w = 1;
            if (parseInt(mInizio) === 12 && parseInt(mFine) === 1) {
                mFine = 13;
            }
            for (var i = parseInt(mInizio); i <= parseInt(mFine); i++) {
                if (i === 13) {
                    arrayFestivitaMese = arrayFestivita["01"];
                } else {
                    var k = "";
                    if (i < 10) {
                        k = "0" + i;
                    } else {
                        k = i.toString();
                    }
                    arrayFestivitaMese = arrayFestivita[k];
                }

                if (i === parseInt(mInizio)) {
                    gg = parseInt(ggInizio);
                    numeroGiorniMese = this._giorniMese(mInizio, annoInizio);
                } else {
                    gg = 1;
                    numeroGiorniMese = ggFine;
                }

                var giorno = "";
                for (var j = gg; j <= numeroGiorniMese; j++, w++) {
                    if (giornoSettimana === 0) {
                        giorno = "Dom";
                    } else if (giornoSettimana === 1) {
                        giorno = "Lun";
                    } else if (giornoSettimana === 2) {
                        giorno = "Mar";
                    } else if (giornoSettimana === 3) {
                        giorno = "Mer";
                    } else if (giornoSettimana === 4) {
                        giorno = "Gio";
                    } else if (giornoSettimana === 5) {
                        giorno = "Ven";
                    } else if (giornoSettimana === 6) {
                        giorno = "Sab";
                    }
                    giornoSettimana = giornoSettimana + 1;
                    if (giornoSettimana > 6) {
                        giornoSettimana = 0;
                    }
                    if (giorno == "Dom" || giorno == "Sab") {
                        this.weekendDays.push(w);
                    }
                    if (j < 10) {
                        j = "0" + j;
                    }

                    var result = j + " " + giorno;
                    this.arrayGiorni.push(result);
                }
                if (arrayFestivitaMese) {
                    for (var q = 0; q < arrayFestivitaMese.length; q++) {
                        var firstDayPeriod = "";
                        if (i === parseInt(mInizio)) {
                            firstDayPeriod = 0;
                        } else {
                            // Se il mese e quello successivo calcolo il giorno d'inizio di questo mese
                            firstDayPeriod = this.arrayGiorni.length - parseInt(ggFine);
                        }
                        for (var t = firstDayPeriod; t < this.arrayGiorni.length; t++) {
                            if (this.arrayGiorni[t].substr(0, 2) === arrayFestivitaMese[q]) {
                                this.weekendDays.push(t + 1);
                            }
                        }
                    }
                }
            }
        } else {
            arrayFestivitaMese = arrayFestivita[mInizio];
            gg = parseInt(ggInizio);
            numeroGiorniMese = this._giorniMese(mInizio, annoInizio);

            var giorno = "";
            for (var j = gg, w = 1; j <= numeroGiorniMese; j++, w++) {
                if (giornoSettimana === 0) {
                    giorno = "Dom";
                } else if (giornoSettimana === 1) {
                    giorno = "Lun";
                } else if (giornoSettimana === 2) {
                    giorno = "Mar";
                } else if (giornoSettimana === 3) {
                    giorno = "Mer";
                } else if (giornoSettimana === 4) {
                    giorno = "Gio";
                } else if (giornoSettimana === 5) {
                    giorno = "Ven";
                } else if (giornoSettimana === 6) {
                    giorno = "Sab";
                }
                giornoSettimana = giornoSettimana + 1;
                if (giornoSettimana > 6) {
                    giornoSettimana = 0;
                }
                if (giorno == "Dom" || giorno == "Sab") {
                    this.weekendDays.push(w);
                }
                if (j < 10) {
                    j = "0" + j;
                }

                var result = j + " " + giorno;
                this.arrayGiorni.push(result);
            }
            if (arrayFestivitaMese) {
                for (var q = 0; q < arrayFestivitaMese.length; q++) {
                    for (var t = 0; t < this.arrayGiorni.length; t++) {
                        if (this.arrayGiorni[t].substr(0, 2) === arrayFestivitaMese[q]) {
                            this.weekendDays.push(t + 1);
                        }
                    }
                }
            }
        }
        var arrayFestivita = this._calcolaArrayFestivita(annoInizio);
    },

    // funzione che crea un array con i giorni festivi del periodo selezionato
    _calcolaArrayFestivita: function (annoInizio) {
        var pasqua = utils.CalcoloPasqua.calcolaPasqua(parseInt(annoInizio));
        var giornoPasqua = pasqua[0];
        var mesePasqua = pasqua[1];
        var arrayFestivita = {
            "01": ["01", "06"],
            "03": [],
            "04": ["25"],
            "05": ["01"],
            "06": ["02"],
            "08": ["15"],
            "11": ["01"],
            "12": ["08", "25", "26"]
        };
        var giornoPasquetta = parseInt(giornoPasqua) + 1;
        giornoPasquetta = giornoPasquetta < 10 ? "0" + giornoPasquetta : giornoPasquetta.toString();
        var mesePasquetta = mesePasqua;
        if (giornoPasqua === 31) {
            // se pasqua è il 31 marzo, la pasquetta sarà il 1o aprile
            giornoPasquetta = "01";
            mesePasquetta = "04";
        }
        arrayFestivita[mesePasquetta].push(giornoPasquetta);
        return arrayFestivita;
    },

    // funzione che ritorna il primo giorno del mese
    _giorniMese: function (mese, anno) {
        var d = new Date(anno, mese, 0);
        return d.getDate();
    },

    // funzione che converte le lettere accentate
    _checkAccenti: function (value) {
        if (value.indexOf("à") >= 0) {
            value = value.replace("à", "a");
        }
        if (value.indexOf("è") >= 0 || value.indexOf("é") >= 0) {
            value = value.replace("è", "e");
            value = value.replace("é", "e");
        }
        if (value.indexOf("ì") >= 0) {
            value = value.replace("ì", "i");
        }
        if (value.indexOf("ò") >= 0) {
            value = value.replace("ò", "o");
        }
        if (value.indexOf("ù") >= 0) {
            value = value.replace("ù", "ù");
        }
        return value;
    },

    //---------------------Functions to check input field--------------------------

    _checkingValues: [],

    setElementsToValidate: function () {
        var inputsId = _.chain($('input'))
            .map(function (item) {
                var innerIndex = item.id.indexOf("-inner");
                if (innerIndex)
                    return item.id.substring(0, innerIndex);
                return item.id;
            })
            .value();

        var controls = _.map(inputsId, function (item) {
            return sap.ui.getCore().byId(item);
        });

        this._checkingValues = _.compact(controls);
        for (var i = 0; i < this._checkingValues.length; i++) {
            this._checkingValues[i].attachChange(null, this.checkInputField, this);
        }
    },

    clearValueState: function () {
        if (this._checkingValues) {
            for (var i = 0; i < this._checkingValues.length; i++) {
                this._checkingValues[i].setValueState("None");
            }
        }
    },

    validateCheck: function () {
        var result = true;
        if (!this._checkingValues || this._checkingValues.length === 0)
            return true;

        for (var i = 0; i < this._checkingValues.length; i++) {
            if (!this.checkInputField(this._checkingValues[i])) {
                if (this._checkingValues[i].data("req") === "true") {
                    result = false;
                }
            }
        }
        return result;
    },

    checkInputField: function (evt) {
        var control = evt.getSource ? evt.getSource() : evt;
        var infoControl = control.data("req");
        var typeControl = control.data("input");
        var correct = true;
        if (infoControl === "true" || control.getValue() !== "") {
            switch (typeControl) {
                default:
                    correct = utils.Validator.required(control);
                    break;
            }
        };
        return correct;
    },

    getFailedControls: function () {
        var result = [];
        _.forEach(this._checkingValues, _.bind(function (item) {
            if (!this.checkInputField(item)) {
                result.push(item);
            }
        }, this));
        return result;
    },

    //--------------------------------------------------------------------------------------
    onSearch: function (evt) {
        var src = evt.getSource();
        this.searchValue = src.getValue();
        var searchProperty = this.uiModel.getProperty("/searchProperty");
        this.applyFilter(this.searchValue, searchProperty);
    },

    applyFilter: function (value, params) {
        var list = this.getView().byId("list");
        if (!list || !list.getBinding("items") || !list.getBinding("items").oList || list.getBinding("items").oList.length === 0)
            return;
        var temp = list.getBinding("items").oList[0]; // a template just to recover the data types
        var filtersArr = [];
        // var props = utils.ObjectUtils.getKeys(temp);

        if (!_.isEmpty(params)) {
            if (!_.isArray(params)) {
                params = [params];
            }
            for (var i = 0; i < params.length; i++) {


                switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
                    case "undefined":
                        break;
                    case "string":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
                        break;
                    case "number":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
                        break;
                }

            }
            var filter = new sap.ui.model.Filter({
                filters: filtersArr,
                and: false
            });
            list.getBinding("items").filter(filter);
            return;
        }
        list.getBinding("items").filter();
    },

    calcolaDifferenzaGiorni: function (date1, date2) {
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        var diffDays = Math.round(Math.abs((date1.getTime() - date2.getTime()) / (oneDay)));
        return diffDays;
    },

    onSearchDialog: function (evt) {
        var src = evt.getSource();
        this.searchValue = src.getValue();
        var searchPropertyDialog = this.uiModelDialog.getProperty("/searchPropertyDialog");
        this.applyFilterDialog(this.searchValue, searchPropertyDialog);
    },

    applyFilterDialog: function (value, params) {
        var list = sap.ui.getCore().getElementById("listS");
        if (!list.getBinding("items").oList || list.getBinding("items").oList.length === 0)
            return;
        var temp = list.getBinding("items").oList[0]; // a template just to recover the data types
        var filtersArr = [];
        // var props = utils.ObjectUtils.getKeys(temp);

        if (!_.isEmpty(params)) {
            if (!_.isArray(params)) {
                params = [params];
            }
            for (var i = 0; i < params.length; i++) {


                switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
                    case "undefined":
                        break;
                    case "string":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
                        break;
                    case "number":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
                        break;
                }

            }
            var filter = new sap.ui.model.Filter({
                filters: filtersArr,
                and: false
            });
            list.getBinding("items").filter(filter);
            return;
        }
        list.getBinding("items").filter();
    },

    doLogout: function () {
        //resetto dati
        sessionStorage.clear();
        console.clear();
        this.router.navTo("login");
    },

    //funzione back da rivedere
    onBackPress: function (evt) {
        var route = location.hash;
        if (route.indexOf("edit") > 0) {
            this.router.navTo("customerDetail", {
                id: this.customer.getId()
            });
        } else {
            if (model.persistence.Storage.session.get("society")) {
                var society = model.persistence.Storage.session.get("society");
            }
            this.router.navTo("launchpad", {
                society: society
            });
        }
    },

    console: function (testo, type) {
        utils.debugMode.console(testo, type);
    },

    _onAfterRenderingTable: function (tab) {
        var table = tab;
        if (!table.getBusy()) {
            table.setBusy(true);
        }
        var rows = table.getRows();
        var arrayCounter = [];
        var column;
        for (var i = 0; i < rows.length; i++) {
            var cellsPerRows = rows[i].getAggregation("cells");
            var g = 0;
            for (var j = 1; j < cellsPerRows.length - 1; j++) {
                var cella = cellsPerRows[j];
                if (_.find(this.weekendDays, _.bind(function (item) {
                        return (item == j);
                    }, this))) {
                    column = $("#" + cella.getDomRef().id).parent().parent();
                    column.css('background-color', '#e5e5e5');
                }
            }
        }
        setTimeout(_.bind(function () {
            table.setBusy(false);
        }, this), 500);
    },

});