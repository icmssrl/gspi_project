jQuery.sap.require("view.abstract.AbstractController");

view.abstract.AbstractController.extend("view.AnalisiCapacitaRisorse", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.tableAnalisiCapacitaModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.tableAnalisiCapacitaModel, "tableAnalisiCapacitaModel");

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");

        this.filterAnalisiCapacitaModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.filterAnalisiCapacitaModel, "filterAnalisiCapacitaModel");
        
        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");
        
        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");

        this.messagePopoverModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.messagePopoverModel, "messagePopoverModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.userModel.setData(this.user);

        if (name !== "analisiCapacitaRisorse") {
            return;
        }

         if (!this._checkRoute(evt, "analisiCapacitaRisorse")) {
            return;
        }
        
        this.nRigheTabella = 10;
        this.oDialog = this.getView().byId("idBlockLayoutAnalisiCapacitaRisorse");
        this.oDialogTable = this.getView().byId("idTableAnalisiCapacitaRisorse");
        this.oDialogTableTot = this.getView().byId("idTableAnalisiCapacitaRisorseTot");
        this.oDialogTable.setVisible(false);
        this.oDialogTableTot.setVisible(false);
//        this.getView().byId("idButtonDownloadExcel").setVisible(false);

        this.tableAnalisiCapacitaModel.setProperty("/giorniLavorativi", 0);
        this.visibleModel.setProperty("/chargeButton", false);

        this._setFilters();

        this.readPeriods();
    },

    // funzione per la navigazione alla lauchpad
    onLaunchpadPress: function () {
        if (this.oDialog.getBusy()) {
            this.oDialog.setBusy(false);
        }
        if (this.oDialogTable.getBusy()) {
            this.oDialogTable.setBusy(false);
        }
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.onNavBackPress();
    },

    // Funzione caricamento periodi
    readPeriods: function () {
        var fSuccess = function (result) {
            var res = {};
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for (var i = 0; i < 13; i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.periodModel.setData(res13);
            this.getView().byId("idSelectPeriodoAnalisiCapacita").setSelectedKey("");
            this.getView().byId("idSelectPeriodoAnalisiCapacita").setValue("");
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            if (!this.oneTimeReadPeriods) {
                // funzione che ripete la chiamata di login nel caso in cui sia scaduta l'autenticazione al sistema
                this.oneTimeReadPeriods = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(this.readPeriods(), this));
            } else {
                this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'getPeriods'");
            }
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    // funzione che, al cambio del periodo, svuota la tabella
    // successivamente popola la tabella con il nuovo periodo selezionato
    onChangePeriod: function (pe) {
        this.oDialog.setBusy(true);
        this.svuotaTabella();

        this.pe = pe;
        var p = pe.getSource().getSelectedItem().getProperty("text").replace(/ /g, '');
        this.period = parseInt(pe.getSource().getProperty("selectedKey"));
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': this.period
        });
        model.persistence.Storage.session.save("periodSelected", periodSelect);
        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();

        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = pInizio;
        var dataF = pFine;

        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

        this.giorniTotali = this.arrayGiorni.length;
        this.giorniFestivi = _.union(this.weekendDays).length;
        this.giorniLavorativi = this.giorniTotali - this.giorniFestivi;
        this.tableAnalisiCapacitaModel.setProperty("/giorniLavorativi", this.giorniLavorativi);

        this.oDialogTable.setVisible(true);
        this.oDialogTableTot.setVisible(true);
//        this.getView().byId("idButtonDownloadExcel").setVisible(true);
       
        this.oDialogTable.setBusy(true);
        setTimeout(_.bind(this._loadTable, this));
    },

    _loadTable: function () {
        var fSuccess = function (result) {
            var richieste = result;
            this.richiesteDaServer = richieste;
            this.tableAnalisiCapacitaModel.setProperty("/richieste", richieste.richiesteScheduling);
            this.tableAnalisiCapacitaModel.setProperty("/filtri", richieste.filtri);
            this.tableAnalisiCapacitaModel.setProperty("/richiesteTotaliAll", richieste.richiesteTotaliAll);
            this.tableAnalisiCapacitaModel.setProperty("/schedulazioniTotaliAll", richieste.schedulazioniTotaliAll);
            this.tableAnalisiCapacitaModel.setProperty("/ferieTotaliAll", richieste.ferieTotaliAll);
            this.tableAnalisiCapacitaModel.setProperty("/totaleTotaliAll", richieste.totaleTotaliAll);
            if (richieste.richiesteScheduling.length > this.nRigheTabella) {
                this.tableAnalisiCapacitaModel.setProperty("/nRighe", this.nRigheTabella);
            } else if (richieste.richiesteScheduling.length === 0) {
                this.tableAnalisiCapacitaModel.setProperty("/nRighe", 1);
            } else {
                this.tableAnalisiCapacitaModel.setProperty("/nRighe", richieste.richiesteScheduling.length);
            }
 
            this._setFilters(richieste.filtri);

            this.oDialog.setBusy(false);
            this.oDialogTable.setBusy(false);
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore lettura XSJS 'readAnalisiCapacitaRisorse'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var req = {};
        req.idPeriodo = this.period;
        model.getAnalisiCapacita.read(req)
            .then(fSuccess, fError);
    },

    _setFilters: function (values) {
        var filtri = [];
        if (values) {
            for (var prop in values) {
                var objFiltri = {};
                objFiltri.title = this._getLocaleText(prop);
                objFiltri.key = prop;
                objFiltri.items = values[prop];
                filtri.push(objFiltri);
            }
        }
        this.filterAnalisiCapacitaModel.setProperty("/filter", filtri);
    },

    _getFacetFilterLists: function () {
        var oFacetFilter = this.getView().byId("facetFilter");
        return oFacetFilter.getLists();
    },

    handleListClose: function () {
        var aFacetFilterLists = this._getFacetFilterLists().filter(function (oList) {
            return oList.getActive() && oList.getSelectedItems().length;
        });

        this._oFacetFilter = new sap.ui.model.Filter(aFacetFilterLists.map(function (oList) {
            return new sap.ui.model.Filter(oList.getSelectedItems().map(function (oItem) {
                var params = this._utilKey(oList.getKey());
                var operator = this._utilOperator(oList.getKey());
                return new sap.ui.model.Filter(params, operator, oItem.getText());
            }.bind(this)), false);
        }.bind(this)), true);

        this._filter();
    },

    _utilKey: function (key) {
        var param = "";
        switch (key) {
        case "ListaConsulenti":
            param = "nomeCompleto";
            break;
        case "ListaBu":
            param = "descBu";
            break;
        case "TipoAzienda":
            param = "tipoAzienda";
            break;
        case "ListaSkill":
            param = "stringSkills";
            break;
        }
        return param;
    },
    
    _utilOperator: function (key) {
        var param = "";
        switch (key) {
            case "ListaSkill":
                param = sap.ui.model.FilterOperator.Contains;
                break;
            default:
                param = sap.ui.model.FilterOperator.EQ;
                break;
        }
        return param;
    },

    handleFacetFilterReset: function (oEvent) {
        var aFacetFilterLists = this._getFacetFilterLists();

        for (var i = 0; i < aFacetFilterLists.length; i++) {
            for (var i = 0; i < aFacetFilterLists.length; i++) {
                aFacetFilterLists[i].setSelectedKeys();
            }
        }
        this._oFacetFilter = null;

        if (oEvent) {
            this._filter();
        }
    },

    _filter: function () {
        var oFilter = null;

        if (this._oTxtFilter && this._oFacetFilter) {
            oFilter = new sap.ui.model.Filter([this._oTxtFilter, this._oFacetFilter], true);
        } else if (this._oTxtFilter) {
            oFilter = this._oTxtFilter;
        } else if (this._oFacetFilter) {
            oFilter = this._oFacetFilter;
        }

        var tabella = this.oDialogTable.getBinding("rows");
        tabella.filter(oFilter, "Application");
        
        if (tabella.getLength() > this.nRigheTabella) {
            this.tableAnalisiCapacitaModel.setProperty("/nRighe", this.nRigheTabella);
        } else if (tabella.getLength() === 0) {
            this.tableAnalisiCapacitaModel.setProperty("/nRighe", 3);
        } else {
            this.tableAnalisiCapacitaModel.setProperty("/nRighe", tabella.getLength());
        }
        
        if (oFilter && oFilter !== null && oFilter !== undefined) {
            var richiesteTotaliAll = 0;
            var schedulazioniTotaliAll = 0;
            var ferieTotaliAll = 0;
            var totaleTotaliAll = 0;
            var array = tabella.aIndices;
            for (var i = 0; i < array.length; i++) {
                var path = array[i];
//                var elementoModello = tabella.getCurrentContexts()[i].getModel().getProperty("/richieste")[path];
                var elementoModello = tabella.oList[path];
                richiesteTotaliAll = richiesteTotaliAll + (elementoModello.nTotaliCommesse ? elementoModello.nTotaliCommesse : 0);
                schedulazioniTotaliAll = schedulazioniTotaliAll + (elementoModello.nTotaliCommesseFinal ? elementoModello.nTotaliCommesseFinal : 0);
                ferieTotaliAll = ferieTotaliAll + (elementoModello.nTotaliFerie ? elementoModello.nTotaliFerie : 0);
                totaleTotaliAll = totaleTotaliAll + (elementoModello.nTotale ? elementoModello.nTotale : 0);
            }
            this.tableAnalisiCapacitaModel.setProperty("/richiesteTotaliAll", richiesteTotaliAll);
            this.tableAnalisiCapacitaModel.setProperty("/schedulazioniTotaliAll", schedulazioniTotaliAll);
            this.tableAnalisiCapacitaModel.setProperty("/ferieTotaliAll", ferieTotaliAll);
            this.tableAnalisiCapacitaModel.setProperty("/totaleTotaliAll", totaleTotaliAll);
        } else {
            this.tableAnalisiCapacitaModel.setProperty("/richiesteTotaliAll", this.richiesteDaServer.richiesteTotaliAll);
            this.tableAnalisiCapacitaModel.setProperty("/schedulazioniTotaliAll", this.richiesteDaServer.schedulazioniTotaliAll);
            this.tableAnalisiCapacitaModel.setProperty("/ferieTotaliAll", this.richiesteDaServer.ferieTotaliAll);
            this.tableAnalisiCapacitaModel.setProperty("/totaleTotaliAll", this.richiesteDaServer.totaleTotaliAll);
        }
        
    },

    onConsultantPress: function (evt) {
        var oEvent = evt.getSource();
        var row = oEvent.getBindingContext("tableAnalisiCapacitaModel").getPath().split("/")[oEvent.getBindingContext("tableAnalisiCapacitaModel").getPath().split("/").length - 1];
        var consultantToOpen = this.tableAnalisiCapacitaModel.getProperty("/richieste")[row];
        if ((this.oMessagePopover1 && this.oMessagePopover1.isOpen()) && (this.tempCodConsultant && this.tempCodConsultant === consultantToOpen.codiceConsulente)) {
            this.oMessagePopover1.close();
            return;
        } else if ((this.oMessagePopover1 && this.oMessagePopover1.isOpen()) && (this.tempCodConsultant && this.tempCodConsultant !== consultantToOpen.codiceConsulente)) {
            this.oMessagePopover1.close();
        }
        this.tempCodConsultant = consultantToOpen.codiceConsulente;
        
        var descrpiptionInfo = this._getLocaleText("CODICE_CONSULENTE_ANALISI") + ": " + consultantToOpen.codiceConsulente + "\n \n" + this._getLocaleText("BU_ANALISI") + ": " + consultantToOpen.descBu + "\n \n" + this._getLocaleText("COMPANY_ANALISI") + ": " + consultantToOpen.tipoAzienda;
        var oggetti = [];

        oggetti.push({
            title: this._getLocaleText("INFO_UTENTE"),
            type: 'Information',
            subtitle: consultantToOpen.cognomeConsulente + " " + consultantToOpen.nomeConsulente,
            description: descrpiptionInfo
        });

        if (consultantToOpen.commesse && consultantToOpen.commesse.length > 0) {
            var descriptionCommesse = "";
            for (var i = 0; i < consultantToOpen.commesse.length; i++) {
                if (consultantToOpen.commesse[i].idCommessa !== "AA00000008") {
                    var desc = consultantToOpen.commesse[i].nomeCommessa + " - " + consultantToOpen.commesse[i].descCommessa + "\n N° giornate: " + consultantToOpen.commesse[i].nGiornate + "\n \n";
                    descriptionCommesse = descriptionCommesse.concat(desc);
                }
            }
            oggetti.push({
                title: this._getLocaleText("COMMESSE_RICHIESTE"),
                type: 'Warning',
                counter: consultantToOpen.commesse.length,
                description: descriptionCommesse
            });
        }

        if (consultantToOpen.commesseFinal && consultantToOpen.commesseFinal.length > 0) {
            var descriptionCommesseFinal = "";
            for (var i = 0; i < consultantToOpen.commesseFinal.length; i++) {
                if (consultantToOpen.commesseFinal[i].idCommessa !== "AA00000008") {
                    var descFinal = consultantToOpen.commesseFinal[i].nomeCommessa + " - " + consultantToOpen.commesseFinal[i].descCommessa + "\n N° giornate: " + consultantToOpen.commesseFinal[i].nGiornate + "\n \n";
                    descriptionCommesseFinal = descriptionCommesseFinal.concat(descFinal);
                }
            }
            oggetti.push({
                title: this._getLocaleText("COMMESSE_FINAL"),
                type: 'Success',
                counter: consultantToOpen.commesseFinal.length,
                description: descriptionCommesseFinal
            });
        }

        this.messagePopoverModel.setProperty("/oggetti", oggetti);

        var oMessageTemplate = new sap.m.MessagePopoverItem({
            type: '{type}',
            title: '{title}',
            description: '{description}',
            subtitle: '{subtitle}',
            counter: '{counter}'
        });

        this.oMessagePopover1 = new sap.m.MessagePopover({
            items: {
                path: '/oggetti',
                template: oMessageTemplate
            }
        });
        this.oMessagePopover1.setModel(this.messagePopoverModel);
        this.oMessagePopover1.openBy(evt.getSource());
    },

    svuotaTabella: function () {
        this.tableAnalisiCapacitaModel.setProperty("/richieste", []);
    },

    onPressDonwnloadExcel: function () {
        var columns = [];
        var rows = [];
        var table = this.oDialogTable;
        var richieste = this.tableAnalisiCapacitaModel.getProperty("/richieste");
        var richiesteLength = richieste.length;
        
        columns = [
            { "name": this._getLocaleText("CONSULENTI") },
            { "name": this._getLocaleText("BU") },
            { "name": this._getLocaleText("SKILLS") },
            { "name": this._getLocaleText("GIORNATE_RICHIESTE") },
            { "name": this._getLocaleText("GIORNATE_SCHEDULATE") },
            { "name": this._getLocaleText("GIORNATE_FERIE") },
            { "name": this._getLocaleText("GIORNATE_TOT") },
        ];

        var rowsTable = table.getRows();
        for (var i = 0; i < richiesteLength; i++) {
            var rowTable = richieste[i];
            var cells = rowTable.getCells();
            var items = [];
            for (var j = 0; j < cells.length; j++) {
                var cell = cells[j];
                var cellText = cell.getText();
                items.push(cellText);
            }
            rows.push(items);
        }

//        var columnsTable = table.getColumns();
//        for (var i = 0; i < columnsTable.length; i++) {
//            var columnTable = columnsTable[i];
//            var columnText = columnTable.mAggregations.label.getText();
//            var colToPush = {
//                'name': columnText
//            };
//        }

        this.completeExcelModel = new sap.ui.model.json.JSONModel();
        this.completeExcelModel.setData({
            rows: rows,
            columns: columns
        });

        if (!this.excelDialog) {
            this.excelDialog = sap.ui.xmlfragment("view.fragment.dialog.AnalisiCapacitaExcelDownload", this);
            this.getView().addDependent(this.excelDialog);
        }
        this.excelDialog.open();
    },
    
    _afterExcelDialog: function () {
        var activityForm = sap.ui.getCore().getElementById("idExcelDialog");
        if (this.completeExcelList) {
            activityForm.removeContent(this.completeExcelList);
            this.completeExcelList.destroy();
        }
        this.completeExcelList = new sap.m.List({
            id: "tableToExport",
            showSeparators: "All"
        });

        var columns = this.completeExcelModel.getProperty("/columns");

        for (var i = 0; i < columns.length; i++) {
            var minWidth = "50px";
            var textToWrite = this.controlloAccenti(columns[i].name);
            var column = new sap.m.Column({
                width: minWidth,
                hAlign: "Right",
                vAlign: "Middle",
                header: new sap.m.Label({
                    text: textToWrite
                })
            });
            this.completeExcelList.addColumn(column);
        }

        var rows = this.completeExcelModel.getProperty("/rows");
        for (var j = 0; j < rows.length; j++) {
            var item = new sap.m.ColumnListItem({
                width: "100%"
            });
            for (var k = 0; k < rows[j].length; k++) {
                if (rows[j][k] !== "") {
                    var tx1 = rows[j][k].split("_")[0];
                    var textToWrite = this.controlloAccenti(tx1);
                    item.addCell(new sap.m.Text({
                        text: textToWrite
                    }));
                } else {
                    item.addCell(new sap.m.Text({
                        text: rows[j][k]
                    }));
                }
            }
            this.completeExcelList.addItem(item);
        }
        activityForm.addContent(this.completeExcelList);
        setTimeout(_.bind(this.createHTMLTable, this));
    },

});