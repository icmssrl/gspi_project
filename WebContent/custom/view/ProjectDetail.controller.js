jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.Projects");
jQuery.sap.require("model.i18n");
if (!model.Periods)
    jQuery.sap.require("model.Periods");
if (!model.ProfiloCommessaPeriodo)
    jQuery.sap.require("model.ProfiloCommessaPeriodo");

view.abstract.AbstractController.extend("view.ProjectDetail", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        
        this.detailCommessa = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailCommessa, "detailCommessa");
        
        this.modelPeriodi = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelPeriodi, "modelPeriodi");
        
        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");
        this.visibleModel.setProperty("/listaPeriodi", true);
        this.visibleModel.setProperty("/tabellaPeriodi", false);
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        
        var name = evt.getParameter("name");
        if (name !== "projectDetail") {
            return;
        }
        this.visibleModel.setProperty("/buttonModify", true);
        var profilo = model.persistence.Storage.session.get("GSPI_User_Logged").codiceProfilo;
//        if (profilo !== "CP00000003") {
//            this.visibleModel.setProperty("/buttonModify", false);
//        }
        this.oDialog = this.getView().byId("idProjectDetail");

        var commessa = JSON.parse(sessionStorage.getItem("selectedProject"));

        this.detailCommessa.setProperty("/codCommessa", commessa.codCommessa);
        this.detailCommessa.setProperty("/nomeCommessa", commessa.nomeCommessa);
        this.detailCommessa.setProperty("/descrizioneCommessa", commessa.descrizioneCommessa);
        this.detailCommessa.setProperty("/codPjmAssociato", commessa.codPjmAssociato);
        this.detailCommessa.setProperty("/codiceStaffCommessa", commessa.codiceStaffCommessa);
        this.detailCommessa.setProperty("/statoCommessa", commessa.statoCommessa);
        this.detailCommessa.setProperty("/nomeConsulente", commessa.nomeConsulente);
        this.detailCommessa.setProperty("/cognomeConsulente", commessa.cognomeConsulente);
        this.detailCommessa.setProperty("/descrizione_bu", commessa.descrizione_bu);
    },

    modifyProjectPress: function () {
        this.router.navTo("projectModify", {
            state: "modify",
            detailId: this.detailCommessa.getProperty("/codCommessa")
        });
    },

    deleteProjectPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_DELETE_PROJECT"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_DELETE_TITLE_PROJECT"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmDeletePress, this)
        );
    },

    _confirmDeletePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var fSuccess = function () {
                this.oDialog.setBusy(false);
                this.router.navTo("projectList");
            };
            var fError = function () {
                this.oDialog.setBusy(false);
                console.log("errore nel salvataggio");
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            model.Projects.deleteProject(this.detailCommessa.getProperty("/codCommessa"))
                .then(fSuccess, fError);
        }
    },
    
    // funzione che apre un Dialog per la riapertura del progetto in un periodo
    onOpneProjectForPeriod: function() {
        this.selectPeriodDialog = sap.ui.xmlfragment("view.dialog.selezionaPeriodoDialog", this);
        var page = this.getView();
        page.addDependent(this.selectPeriodDialog);
        this.selectPeriodDialog.open();
    },
    
    // funzione che si lancia prima dell'apertura del dialog e carica i perdiodi
    onBeforOpenDialog: function () {
        var fSuccess = function (result) {
            var res = {};
            _.remove(result.items, {
                stato: 'CHIUSO'
            });
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for (var i = 0; i < 13; i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.modelPeriodi.setData(res13);
        };
        var fError = function () {
            if (!this.oneTimeReadPeriods) {
                // funzione che ripete la chiamata di login nel caso in cui sia scaduta l'autenticazione al sistema
                this.oneTimeReadPeriods = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(this.readPeriods(), this));
            } else {
                this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'getPeriods'");
            }
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },
    
    // selezionato un perdiodo mi salvo l'ID del periodo
    onSelectPeriod: function (evt) {
        this.periodoSelezionato = evt.getSource().getSelectedKey();
    },
    
    // visualizzo la tabella con tutti i periodi e lo stato della commessa per periodo
    onCheckDialogPress: function () {
        var fSuccess = function (result) {
            var periodi = this.modelPeriodi.getData();
            for (var i=0; i<periodi.items.length; i++) {
                var res = _.find(result.items, {idPeriodoSchedulingProf: periodi.items[i].idPeriod});
                if(res)
                    periodi.items[i].statoPeriodo = res.stato;
            }
            this.modelPeriodi.setData(periodi);
            this.visibleModel.setProperty("/listaPeriodi", false);
            this.visibleModel.setProperty("/tabellaPeriodi", true);
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'profiloCommessaPeriodoSet'");
        };
        
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.ProfiloCommessaPeriodo.readRecordByCodCommessa(this.detailCommessa.getProperty("/codCommessa"))
            .then(fSuccess, fError);
    },
    
    onCloseAllPeriodsDialogPress: function () {
        var fSuccess = function () {
            this.onCloseTableDialogPress();
            this.onCancelDialogPress();
            sap.m.MessageToast.show(this._getLocaleText("TUTTI_PERIODI_CHIUSI"));
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore lettura XSJS 'scritturaProfiloCommessaPeriodo.xsjs'");
        };
        
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.ProfiloCommessaPeriodo.changeStato(this.detailCommessa.getProperty("/codCommessa"), undefined, "CHIUSO")
            .then(fSuccess, fError);
    },
    
    // chiude la tabella e rivisualizza la lista
    onCloseTableDialogPress: function () {
        this.visibleModel.setProperty("/listaPeriodi", true);
        this.visibleModel.setProperty("/tabellaPeriodi", false);
    },
    
    // funzione che chiude e distrugge il dialog
    onCancelDialogPress: function () {
        this.selectPeriodDialog.close();
        this.selectPeriodDialog.destroy(true);
    },
    
    // apro il periodo richiesto
    onSelectDialogPress: function () {
        var fSuccess = function () {
            this.onCancelDialogPress();
            sap.m.MessageToast.show(this._getLocaleText("STATO_PERIODO_APERTO"));
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore lettura XSJS 'scritturaProfiloCommessaPeriodo.xsjs'");
        };
        
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.ProfiloCommessaPeriodo.changeStato(this.detailCommessa.getProperty("/codCommessa"), this.periodoSelezionato, "APERTO", this.detailCommessa.getProperty("/codiceStaffCommessa"))
            .then(fSuccess, fError);
    },

});
