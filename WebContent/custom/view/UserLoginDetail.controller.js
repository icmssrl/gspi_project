jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.UserLogin");
jQuery.sap.require("model.Skill");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");

view.abstract.AbstractController.extend("view.UserLoginDetail", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        
        this.detailUserLog = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailUserLog, "detailUserLog");
        
        this.adminType = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.adminType, "adminType");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "userLoginDetail") {
            return;
        };
        this.oDialog = this.getView().byId("idUserLoginDetail");

        var userLogin = JSON.parse(sessionStorage.getItem("selectedConsultantLogin"));
        
        this.consultantType = model.persistence.Storage.session.get("user").descrizioneProfiloConsulente;
        if(this.consultantType !== "Administrator") {
            this.adminType.setProperty("/admin", false);
        } else {
            this.adminType.setProperty("/admin", true);
        }

        this.detailUserLog.setProperty("/alias", userLogin.alias);
        this.detailUserLog.setProperty("/cognomeConsulente", userLogin.cognomeConsulente);
        this.detailUserLog.setProperty("/nomeConsulente", userLogin.nomeConsulente);
        this.detailUserLog.setProperty("/password", userLogin.password);
        this.detailUserLog.setProperty("/stato", userLogin.stato);
    },

    modifyConsultantPress: function () {
        this.router.navTo("userLoginModify", {
            state: "modify",
            alias: this.detailUserLog.getProperty("/alias")
        });
    },

    deleteConsultantPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_DELETE_USER"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_DELETE_TITLE_USER"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmDeletePress, this)
        );
    },

    _confirmDeletePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            var fSuccess = function () {
                this.oDialog.setBusy(false);
                this.router.navTo("userLoginList");
            }
            var fError = function () {
                this.oDialog.setBusy(false);
                console.log("errore nel salvataggio");
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            model.UserLogin.deleteAlias(this.detailUserLog.getProperty("/alias"))
                .then(fSuccess, fError);
        };
    },

});