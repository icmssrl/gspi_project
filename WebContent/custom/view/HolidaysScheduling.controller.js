jQuery.sap.require("view.abstract.AbstractController");
//jQuery.sap.require("model.tabellaScheduling");
jQuery.sap.require("model.requestSave");
jQuery.sap.require("model.reqToDel");
jQuery.sap.require("model.Consultants");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.Periods");
jQuery.sap.require("model.BuList");
jQuery.sap.require("model.tabellaSchedulingMaria");
jQuery.sap.require("model.tabellaFerie");

view.abstract.AbstractController.extend("view.HolidaysScheduling", {
    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.pmSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingModel, "pmSchedulingModel");
        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");
        this.noteModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteModel, "noteModel");
        this.addConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.addConsultantModel, "cModel");
        this.consultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.consultantModel, "consultantModel");
        this.consultantModel.setSizeLimit(1000);
        this.buModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.buModel, "buModel");
        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");
        this.inputModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.inputModel, "in");
        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");
        this.pModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pModel, "pModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");
        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.userModel.setData(this.user);
        this.visibleModel.setProperty("/enableBu", false);
        this.visibleModel.setProperty("/noteVisible", false);

        if (!this._checkRoute(evt, "holidaysScheduling")) {
            return;
        }

        this.filteredScheduling = undefined;
        this.filteredSchedulingClone = undefined;
        this.inputModel.setProperty("/codiceBu", "");
        this.uiModel.setProperty("/inputComplete", false);
        this.oDialog = this.getView().byId("BlockLayout");
        this.oDialogTable = this.getView().byId("idSchedulazioniFerieTable");
        this.getView().byId("idSchedulazioniFerieTable").setVisible(false);
        this.visibleModel.setProperty("/enabledButton", true);
        this.readPeriods();
        this.getBuList();
        this.chosedCom = "AA00000008";
        var modello = this.pmSchedulingModel.getData();
    },

    // funzione di lettura dei periodo e ordinati per anno, mese in ordine decrescente
    readPeriods: function () {
        this._getPeriodsGlobal(undefined, "oDialog.setBusy", false);
        /*
        var fSuccess = function (result) {
            var res = {};
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for (var i = 0; i < 13; i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.periodModel.setData(res13);
            console.log("Periods: ");
            console.log(res);
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            sap.m.MessageBox.alert("Errore in lettura periodo", {
                title: "Attenzione"
            });
            this.oDialog.setBusy(false);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.Periods().getPeriods().then(fSuccess, fError);
        */
    },

    // funzione per la lettura delle BU attive
    getBuList: function () {
        var fSuccess = function (result) {
            this.buModel.setData(result);
            var listBu = sap.ui.getCore().getElementById("idListBu");
            listBu.removeSelections();
            this.oDialog.setBusy(false);
        };
        var fError = function (err) {
            sap.m.MessageToast("error BU");
            this.oDialog.setBusy(false);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        var stato = "APERTO";
        //        model.BuList.getBuListByStatus(stato).then(fSuccess, fError);
        model.BusinessUnit.readBu(undefined, stato).then(fSuccess, fError);
    },

    // funzione che, al cambio del periodo, nasconde alcuni tasti, svuota la tabella e le select
    // successivamente popola la tabella con il nuovo periodo selezionato
    onChangePeriod: function (pe) {
        this.visibleModel.setProperty("/addConsultantButton", false);
        this.visibleModel.setProperty("/closeProjectButton", false);
        this.checkInput();
        this.svuotaSelectBu();
        this.svuotaTabella();
        var table = this.getView().byId("idSchedulazioniFerieTable");
        this.pe = pe;
        var p = pe.getSource().getSelectedItem().getProperty("text").replace(/ /g, '');
        var period = pe.getSource().getProperty("selectedKey");
        model.persistence.Storage.session.save("period", period);
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(period)
        });
        model.persistence.Storage.session.save("periodSelected", periodSelect);
        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();
        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();
        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }
        var dataI = pInizio;
        var dataF = pFine;
        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);
        //** rendo visibili solo le colonne del periodo selezionato
        var columns = table.getColumns();
        for (var i = 0; i < columns.length; i++) {
            columns[i].setVisible(true);
        }
        for (var i = this.numColonne; i < columns.length - 1; i++) {
            columns[i].setVisible(false);
        }
        //** metto i nomi dei giorni sulle colonne
        for (var i = 2; i < columns.length; i++) {
            columns[i - 1].getLabel().mProperties.text = this.arrayGiorni[i - 2];
        }
        this.filteredSchedulingClone = undefined;
    },

    // funzione che carica i consulenti con richieste ferie
    onSelectBu: function (bu) {
        this.visibleModel.setProperty("/enableBu", true);
        this.checkInput();
        var req = {};
        req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
        req.idCommessa = "AA00000008";
        req.cod_bu = this.inputModel.getProperty("/codiceBu");
        this.ric = req;
        this.loadConsultants(req);
        this.leggiTabella(this.ric);
        jQuery.sap.delayedCall(1500, this, function () {
            this.oDialog.setBusy(false);
        });
        this.listaSelezioniFiltro = undefined;
        this.filteredSchedulingClone = undefined;
        var lista = sap.ui.getCore().byId("consultantSelection");
        if (lista)
            lista.removeSelections();
    },

    // funzione per mostrare i bottoni delle note e del cancella (ultimi due bottoni dell'actionSheet) solo se ci sono già delle richieste
    //    showButton: function (evt) {
    //        var idButton = this.oButton.getId();
    //        var nButton = parseInt((idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1));
    //        if (nButton) var nota = "nota" + (nButton.toString());
    //        var gg = "gg" + (nButton.toString());
    //        var row = parseInt(idButton.substring(idButton.indexOf("row") + 3, idButton.length));
    //        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[row];
    //        var numeroBottoni = evt.getSource().getAggregation("buttons").length;
    //        if (!!dataModelConsultant[gg]) {
    //            if (!!dataModelConsultant[nota] && dataModelConsultant[nota].trim().length > 0) {
    //                evt.getSource().getAggregation("buttons")[numeroBottoni - 2].setVisible(false);
    //            } else {
    //                evt.getSource().getAggregation("buttons")[numeroBottoni - 2].setVisible(true);
    //            }
    //            evt.getSource().getAggregation("buttons")[numeroBottoni - 1].setVisible(true);
    //        } else {
    //            evt.getSource().getAggregation("buttons")[numeroBottoni - 2].setVisible(false);
    //            evt.getSource().getAggregation("buttons")[numeroBottoni - 1].setVisible(false);
    //        }
    //    }, 

    //funzione che nascond i bottoni delle note e del cancella (ultimi due bottoni dell'actionSheet) alla chiusura del dialog
    afterClose: function (evt) {
        if (evt.getSource().getAggregation("buttons")) {
            var numeroBottoni = evt.getSource().getAggregation("buttons").length;
        }
        if (evt.getSource().getAggregation("buttons")[numeroBottoni - 1]) {
            evt.getSource().getAggregation("buttons")[numeroBottoni - 1].setVisible(false);
        }
    },

    //funzione che mostra i bottoni delle note e del cancella (ultimi due bottoni dell'actionSheet) solo se ci sono già delle richieste
    showApproveActionSheetButtons: function (evt) {
        var idButton = this.oButton.getId();
        var nButton = parseInt((idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1));
        if (nButton) var nota = "nota" + (nButton.toString());
        var row = parseInt(idButton.substring(idButton.indexOf("row") + 3, idButton.length));
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[row];
        var numeroBottoni = evt.getSource().getAggregation("buttons").length;
        if (!!dataModelConsultant[nota] && dataModelConsultant[nota].trim().length > 0) {
            evt.getSource().getAggregation("buttons")[numeroBottoni - 1].setVisible(false);
        } else {
            evt.getSource().getAggregation("buttons")[numeroBottoni - 1].setVisible(true);
        }
    },

    actionButton: function (evt) {
        var del = false;
        var b = this.oButton;
        var idButton = b.getId();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        var row = idButton.substring(idButton.indexOf("row") + 3, idButton.length);
        var prop = "gg" + nButton;
        var propNotaGiorno = "nota" + nButton;
        var propReq = "req" + nButton;
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[row];
        this.noteModel.setProperty("/note", dataModelConsultant[propNotaGiorno]);
        this.oButton = "";
        var buttonPressed = evt.getSource();
        //prendo il numero della tabella per passare correttamente la data
        var table = this.getView().byId("idSchedulazioniFerieTable");
        var giornoMese = parseInt(table.getColumns()[nButton].getLabel().getProperty("text"));
        var dataToSend = {
            "codConsulente": "",
            "dataReqPeriodo": this.calcolareDataRichiesta(nButton),
            "idCommessa": "AA00000008",
            "codAttivitaRichiesta": "",
            "idPeriodoSchedulingRic": 0,
            "codPjm": "CC00000000",
            "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "note": dataModelConsultant[propNotaGiorno],
            "statoReq": "CONFIRMED",
            "giorno": 0,
            "id_req": dataModelConsultant[propReq]
        };
        dataToSend.codConsulente = dataModelConsultant.codiceConsulente;
        dataToSend.idPeriodoSchedulingRic = this.getView().byId("idSelectPeriodo").getSelectedKey();
        dataToSend.giorno = nButton;
        var jsonButtonModel = {
            halfDay: "1/2",
            fullDay: this.chosedCom,
            concConsultant: "yellow",
            concCustomer: "CC",
        };
        var richiesta = buttonPressed.getId();
        var o = _.cloneDeep(this.pmSchedulingModel.getData().schedulazioni[row]);
        switch (richiesta) {
            case "travel":
                b.setSrc("sap-icon://travel-itinerary");
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiesta = "travel-itinerary";
                b.addStyleClass("travel");
                break;
            case "bed":
                b.setSrc("sap-icon://bed");
                o[prop] = this.chosedCom;
                prop = prop + "a";
                if (o[prop]) {
                    o[prop] = "";
                }
                dataToSend.codAttivitaRichiesta = richiesta;
                break;
            case "delHolidays":
                var icon = b.getSrc();
                var req = icon.substring(icon.lastIndexOf("/") + 1, icon.length);
                del = true;
                b.setSrc("");
                o[prop] = "";
                prop = prop + "a";
                if (o[prop]) {
                    o[prop] = "";
                }
                dataToSend.codAttivitaRichiesta = req;
                break;
        }
        if (!del) {
            if (!this.visibleModel.getProperty("/noteAndCanceVisible")) {
                this.scrivereFerieSuGestioneRichieste(dataToSend, dataModelConsultant, propReq, nButton, dataToSend.codAttivitaRichiesta);
            } else {
                //sap.m.MessageBox.show("servizio di update");
                this.updateHoliday(dataToSend, dataModelConsultant, propReq, nButton, dataToSend.codAttivitaRichiesta);
            }
        } else {
            this.cancellareFerieSuGestioneRichieste(dataToSend);
        }
    },

    scrivereFerieSuGestioneRichieste: function (req, dataModelConsultant, propReq, nButton, codAttivitaRichiesta) {
        var fSuccess = function (res) {
            var idReq = JSON.parse(res).idReq;
            req.id_req = idReq;
            //            var schedCons = _.find(this.pmSchedulingModel.getData().schedulazioni, {
            //                codiceConsulente: dataModelConsultant.codiceConsulente
            //            });
            //            var reqGG = "req" + nButton;
            //            schedCons[reqGG] = idReq;
            this.scrivereFerieSuFinal(dataModelConsultant, propReq, nButton, codAttivitaRichiesta, undefined, req);
        };
        var fError = function (err) {
            var msgKo = "Errore in inserimento richiesta";
            sap.m.MessageToast.show(msgKo);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.requestSave.sendHolidayRequest(req).then(fSuccess, fError);
    },

    cancellareFerieSuGestioneRichieste: function (dataToSend) {
        var fSuccess = function () {
            this.cancellareFerieSuFinal(dataToSend);
        };
        var fError = function (err) {
            var msgKo = "Errore in cancellazione richiesta";
            sap.m.MessageToast.show(msgKo);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.reqToDel.sendRequest(dataToSend).then(fSuccess, fError);
    },

    cancellareFerieSuFinal: function (dataToSend) {
        var msgOk = "Richiesta cancellata correttamente";
        var codAttivitaRichiesta = dataToSend.codAttivitaRichiesta;
        if (codAttivitaRichiesta !== "bed") {
            codAttivitaRichiesta = "travel-itinerary";
        }
        dataToSend.codAttivitaRichiesta = codAttivitaRichiesta;
        dataToSend.idPeriodoSchedulingRic = parseInt(dataToSend.idPeriodoSchedulingRic);
        dataToSend.giorno = parseInt(dataToSend.giorno);
        this.tempButton = parseInt(dataToSend.giorno);
        this.tempCodCons = dataToSend.codConsulente;
        var fSuccess = function (result) {
            sap.m.MessageToast.show(msgOk);
            var propertyTodelete = {};
            var stato = "stato" + this.tempButton;
            propertyTodelete[stato] = stato;
            var gg = "gg" + this.tempButton;
            propertyTodelete[gg] = gg;
            var ggIcon = "ggIcon" + this.tempButton;
            propertyTodelete[ggIcon] = "ggIcon" + this.tempButton;
            var req = "req" + this.tempButton;
            propertyTodelete[req] = "req" + this.tempButton;
            var nota = "nota" + this.tempButton;
            propertyTodelete[nota] = "nota" + this.tempButton;
            var schedCons = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                codiceConsulente: this.tempCodCons
            });
            for (var prop in propertyTodelete) {
                delete schedCons[prop];
            }
            this._calcoloGiornate(schedCons);
            //            this.pmSchedulingModel.refresh();
        };
        var fError = function (err) {
            var msgKo = "Errore in cancellazione richiesta";
            sap.m.MessageToast.show(msgKo);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.reqToDel.sendRequestToFinal(dataToSend).then(fSuccess, fError);
    },

    scrivereFerieSuFinal: function (dataModelConsultant, propReq, nButton, codAttivitaRichiesta, stato, req) {
        var msgOk = "Richiesta modificata correttamente";
        var msgKo = "Errore in modifica richiesta";
        var dataToSend = {
            "codConsulenteFinal": dataModelConsultant.codiceConsulente,
            "dataReqFinal": this.calcolareDataRichiesta(nButton),
            "idCommessaFinal": "AA00000008",
            "codAttivitaRichiestaFinal": codAttivitaRichiesta,
            "idPeriodoSchedulingFinal": this.getView().byId("idSelectPeriodo").getSelectedKey(),
            "codPjmFinal": "CC00000000",
            "dataInserimentoFinal": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "noteFinal": req.note ? req.note : "",
            "statoReqFinal": stato ? stato : "CONFIRMED",
            "giornoPeriodoFinal": nButton,
            "idReqOr": req.id_req
        };
        this.tempButton = nButton;
        this.codAttivitaRichiesta = dataToSend.codAttivitaRichiesta;
        this.tempCodCons = dataModelConsultant.codiceConsulente;
        var fSuccess = function (result) {
            this.leggiTabella(this.ric);
        };
        var fError = function (err) {
            sap.m.MessageToast.show(msgKo);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.tabellaSchedulingMaria.sendRequest(dataToSend).then(fSuccess, fError);
    },

    actionApproveButton: function (evt) {
        var b = this.oButton;
        var codAttivitaRichiesta = "";
        if (b.mProperties.src.indexOf("travel-itinerary") >= 0) {
            codAttivitaRichiesta = "travel-itinerary";
        } else {
            codAttivitaRichiesta = "bed";
        }
        var idButton = b.getId();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        var row = idButton.substring(idButton.indexOf("row") + 3, idButton.length);
        var propReq = "req" + nButton;
        var propNote = "nota" + nButton;
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[row];
        var codConsulente = dataModelConsultant.codiceConsulente;
        this.oButton = "";
        var id_req = dataModelConsultant[propReq];
        var note = dataModelConsultant[propNote];
        var buttonPressed = evt.getSource();
        var buttonType = buttonPressed.getType();
        var comboBoxPeriod = this.getView().byId("idSelectPeriodo");
        var idPeriodo = comboBoxPeriod.getSelectedKey();
        var req = {};
        req.idCommessa = "AA00000008";
        req.cod_consulente = undefined;
        req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
        var stato = "";
        if (buttonType === "Accept") {
            stato = "CONFIRMED";
            // prima di tutto verifico se il consulente ha già qualcosa nella tabella final
            var fLetturaSuccess = function (result) {
                if (result && result.length > 0) {
                    this.pmSchedulingModel.setProperty("/requests", result);
                    if (!this._ferieDialog) {
                        this._ferieDialog = sap.ui.xmlfragment("view.dialog.ferieDialog", this);
                        this.getView().addDependent(this._ferieDialog);
                    }
                    var giornoRichiesta = this.calcolareDataRichiesta(nButton);
                    var day = giornoRichiesta.split("-")[2];
                    var month = giornoRichiesta.split("-")[1];
                    var year = giornoRichiesta.split("-")[0];
                    giornoRichiesta = day + "/" + month + "/" + year;
                    this.pmSchedulingModel.setProperty("/giorno", giornoRichiesta);
                    this._ferieDialog.open();
                    var entity = {
                        'idReq': id_req,
                        'stato': stato
                    };
                    this.datiPerCambioStato = {
                        'dataModelConsultant': dataModelConsultant,
                        'propReq': propReq,
                        'nButton': nButton,
                        'codAttivitaRichiesta': codAttivitaRichiesta,
                        'entity': entity
                    };
                    if (codAttivitaRichiesta === "travel-itinerary") {
                        this.inibireSelezioni = true;
                        this.singleSelect = undefined;
                    } else if (codAttivitaRichiesta === "bed") {
                        if (result.length > 1) {
                            this.inibireSelezioni = undefined;
                        } else {
                            this.inibireSelezioni = true;
                        }
                        this.singleSelect = true;
                    }
                } else if (result) {
                    var entity = {
                        'idReq': id_req,
                        'stato': stato,
                        'note': note ? note : ""
                    };
                    this.cambiareStatoRichiesta(entity, dataModelConsultant, propReq, nButton, codAttivitaRichiesta);
                }
            };
            var fLetturaError = function (err) {
                console.log(err);
            };
            fLetturaSuccess = _.bind(fLetturaSuccess, this);
            fLetturaError = _.bind(fLetturaError, this);
            model.requestSave.readDayInFinalTable(codConsulente, nButton, idPeriodo).then(fLetturaSuccess, fLetturaError);
        } else {
            stato = "DELETED";
            var entity = {
                'idReq': id_req,
                'stato': stato,
                'codConsulente': codConsulente
            };
            //precedente
            //this.cambiareStatoRichiesta(entity, undefined, undefined, nButton, undefined);
            this.cambiareStatoRichiesta(entity, dataModelConsultant, propReq, nButton, codAttivitaRichiesta);
        }
    },

    onRequestDialogClose: function () {
        this.datiPerCambioStato = undefined;
        this._ferieDialog.close();
    },

    onRequestDialogOK: function () {
        var requestList = sap.ui.getCore().getElementById("idSchedulazioniList");
        var requests = requestList.getItems();
        var selectedItems = [];
        for (var i = 0; i < requestList.getSelectedItems().length; i++) {
            var selectedRequest = requestList.getSelectedItems()[i];
            var index = parseInt(requestList.getSelectedItems()[0].sId.split("-")[2]);
            var request = this.pmSchedulingModel.getProperty("/requests")[index];
            selectedItems.push(request);
        }
        this.cancellareRichiestaDaFinal(selectedItems);
    },

    cancellareRichiestaDaFinal: function (selectedItems) {
        var fError = function (err) {
            var msgKo = "Errore in cancellazione richiesta";
            sap.m.MessageToast.show(msgKo);
        };
        var fSuccess = function (result) {
            var dataModelConsultant = this.datiPerCambioStato.dataModelConsultant;
            var propReq = this.datiPerCambioStato.propReq;
            var nButton = this.datiPerCambioStato.nButton;
            var codAttivitaRichiesta = this.datiPerCambioStato.codAttivitaRichiesta;
            var entity = this.datiPerCambioStato.entity;
            this.cambiareStatoRichiesta(entity, dataModelConsultant, propReq, nButton, codAttivitaRichiesta);
            this.onRequestDialogClose();
        };
        var fFirstSuccess = function (result) {
            model.tabellaSchedulingMaria.deleteRequestMaria(selectedItems[1].idReq, "DELETED", 1).then(fSuccess, fError);
        };
        fFirstSuccess = _.bind(fFirstSuccess, this);
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        if (selectedItems.length > 1) {
            model.tabellaSchedulingMaria.deleteRequestMaria(selectedItems[0].idReq, "DELETED", 1).then(fFirstSuccess, fError);
            //        } else {
            //            model.tabellaSchedulingMaria.deleteRequestMaria(selectedItems[0].idReq, "DELETED", 1).then(fSuccess, fError);
        }
    },

    cambiareStatoRichiesta: function (entity, dataModelConsultant, propReq, nButton, codAttivitaRichiesta) {
        var fSuccess = function (ok) {
            this.leggiTabella(this.ric);
            //se passo lo stato === DELETED NON devo scrivere su Final
            //            if (entity.stato !== "DELETED") {
            //                this.scrivereFerieSuFinal(dataModelConsultant, propReq, nButton, codAttivitaRichiesta, undefined, entity);
            //            } else {
            //                this.scrivereFerieSuFinal(dataModelConsultant, propReq, nButton, codAttivitaRichiesta, entity.stato, entity);
            //            }
        };
        var fError = function (err) {
            console.log("passed from error cambiareStatoRichiesta");
            var msgKo = "Errore in modifica richiesta";
            sap.m.MessageToast.show(msgKo);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.requestSave.changeStatus(entity).then(fSuccess, fError);
    },

    calcolareDataRichiesta: function (nButton) {
        var table = this.getView().byId("idSchedulazioniFerieTable");
        var giornoMese = parseInt(table.getColumns()[nButton].getLabel().getProperty("text"));
        if (giornoMese < 10) {
            giornoMese = "0" + giornoMese;
        }
        var anno = model.persistence.Storage.session.get("periodSelected").annoPeriodo;
        var periodoMese = model.persistence.Storage.session.get("periodSelected").mesePeriodo;
        if (parseInt(giornoMese) < 10 && nButton > 20) {
            if (parseInt(periodoMese) !== 12) {
                periodoMese = parseInt(periodoMese) + 1;
            } else {
                periodoMese = "1";
                anno = parseInt(anno) + 1;
            }
        }
        if (periodoMese < 10) {
            periodoMese = "0" + periodoMese;
        }

        var dataRichiesta = anno + "-" + periodoMese + "-" + giornoMese;
        if (!(new Date(dataRichiesta)).getYear()) {
            var lastDayOfMonth = new Date(parseInt(dataRichiesta.split("-")[0]), parseInt(dataRichiesta.split("-")[1]), 0);
            lastDayOfMonth = lastDayOfMonth.getDate();
            var mesePlus = parseInt(periodoMese) + 1;
            if (mesePlus < 10) {
                mesePlus = "0" + mesePlus;
            }
            var giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
            dataRichiesta = anno + "-" + mesePlus + "-" + giornoPlus;
        }
        return dataRichiesta;
    },

    onAfter: function (evt) {
        var table = this.getView().byId("idSchedulazioniFerieTable");
        var rows = table.getRows();
        var arrayCounter = [];
        for (var i = 0; i < rows.length; i++) {
            var cellsPerRows = rows[i].getAggregation("cells");
            for (var j = 1; j < cellsPerRows.length - 1; j++) {
                var cella = cellsPerRows[j];
                if (_.find(this.weekendDays, _.bind(function (item) {
                        return (item == j);
                    }, this))) {
                    var column = $("#" + cella.getDomRef().id).parent().parent();
                    column.css('background-color', '#e5e5e5');
                }
            }
        }
        var dati = this.pmSchedulingModel.getData();
        var schedulazioni = dati.schedulazioni;
        for (var i = 0; i < schedulazioni.length; i++) {
            if (!schedulazioni[i].giorniScheduling) {
                schedulazioni[i].giorniScheduling = 0;
            }
            this._calcoloGiornate(schedulazioni[i]);
        }
    },

    // funzione che calcola i giorni, e mezze giornate, delle ferie
    _calcoloGiornate: function (questoConsulente) {
        var giorniTemp = questoConsulente.giorniScheduling;
        var count = 0;
        for (var i = 1; i < 36; i++) {
            var propIn = "gg" + i;
            var propInIcon = "ggIcon" + i;
            if (questoConsulente[propIn]) {
                if (questoConsulente[propInIcon] === "sap-icon://bed") {
                    count = count + 0.5;
                } else {
                    count = count + 1;
                }
            }
        }
        questoConsulente.giorniScheduling = count;
        var totalCount = 0;
        for (var i = 0; i < this.pmSchedulingModel.getData().schedulazioni.length; i++) {
            totalCount = totalCount + this.pmSchedulingModel.getData().schedulazioni[i].giorniScheduling;
        }
        this.pmSchedulingModel.getData().giorniTotali = totalCount;
        this.pmSchedulingModel.refresh();
    },

    uniqueElement: function (result) {
        var arrayConsulentiStaff = result[0];
        var arrayGiornateSchedulate = result[0];
        //mi costruisco un oggetto con i codici di tutti i consulenti
        var objCodiciConsulenti = {};
        for (var i = 0; i < arrayConsulentiStaff.length; i++) {
            var c = "c" + i;
            objCodiciConsulenti[c] = arrayConsulentiStaff[i].cod_consulente;
        }
        var objCodiciConsulenti2 = {};
        for (var i = 0; i < arrayGiornateSchedulate.length; i++) {
            var c = "d" + i;
            objCodiciConsulenti2[c] = arrayGiornateSchedulate[i].codiceConsulente;
        }
        _.merge(objCodiciConsulenti, objCodiciConsulenti2);
        var arrayfinale = [];
        for (var prop in objCodiciConsulenti) {
            var arr = _.where(arrayGiornateSchedulate, {
                codiceConsulente: objCodiciConsulenti[prop]
            });
            if (arr.length === 0) {
                continue;
            }
            var obj = {};
            for (var i = 0; i < arr.length; i++) {
                for (var prop in arr[i]) {
                    if (!obj[prop]) {
                        obj[prop] = arr[i][prop];
                    } else {
                        if (prop.indexOf("gg") > -1 || prop.indexOf("nota") > -1 || prop.indexOf("req") > -1) {
                            var io = prop + "a";
                            obj[io] = arr[i][prop];
                        }
                    }
                }
            }
            for (prop in obj) {
                if (prop.indexOf("stato") > -1) {
                    if (obj[prop] === "PROPOSED") {
                        obj.acceptAllLinkVisible = true;
                        break;
                    } else {
                        obj.acceptAllLinkVisible = undefined;
                    }
                }
            }
            arrayfinale.push(obj);
        }
        arrayfinale = _.reduce(arrayfinale, function (results, item) {
            return _.any(results, function (result) {
                return _.isEqual(result, item);
            }) ? results : results.concat([item]);
        }, []);
        console.log(arrayfinale);
        return arrayfinale;
    },

    openMenu: function (oEvent) {
        this.oButton = oEvent.getSource();
        // quello che segue è per capire l'id del testo sotto al bottone e quindi la sua classe (=stato della schedulazione)
        var idButton = this.oButton.sId;
        var myButtId = idButton.match("button(.*)-col");
        this.giornoPeriodo = myButtId[1];
        var array = idButton.split("col");
        var inizioId = array[0];
        var fineId = array[1];
        inizioId = inizioId.substring(0, inizioId.length - 1);
        var idTesto = inizioId + "_text-col" + fineId;
        var testo = this.getView().byId(idTesto);
        if (testo) {
            var customData = testo.getCustomData()[0];
            if (customData) var classe = customData.getValue();
        }
        if (this.oButton.getSrc() === "sap-icon://sys-add") {
            if (!this._actionSheet) {
                this._actionSheet = sap.ui.xmlfragment("view.fragment.HolidaysActionSheet", this);
                this.getView().addDependent(this._actionSheet);
            }
            this._actionSheet.openBy(this.oButton);
        } else {
            if (!testo || classe === "confirmedClass") {
                // create action sheet only once
                if (!this._actionSheet) {
                    this._actionSheet = sap.ui.xmlfragment("view.fragment.HolidaysActionSheet", this);
                    this.getView().addDependent(this._actionSheet);
                }
                this._actionSheet.openBy(this.oButton);
            } else {
                if (!this._approveActionSheet) {
                    this._approveActionSheet = sap.ui.xmlfragment("view.fragment.ApproveHolidaysActionSheet", this);
                    this.getView().addDependent(this._approveActionSheet);
                }
                this._approveActionSheet.openBy(this.oButton);
            }
        }
        var propGiorno = "gg" + this.giornoPeriodo;
        var rowNumber = idButton.substring(idButton.indexOf("row") + 3, idButton.length);
        if (this.pmSchedulingModel.getData().schedulazioni[rowNumber][propGiorno]) {
            this.visibleModel.setProperty("/noteAndCanceVisible", true);
        } else {
            this.visibleModel.setProperty("/noteAndCanceVisible", false);
        }
    },

    leggiTabella: function (req) {
        var fSuccess = function (results) {
            _.remove(results[0], function (n) {
                for (var i in n) {
                    if (i.indexOf("stato") >= 0) {
                        if (n[i] === "DELETED") return true;
                    }
                }
            });
            //---------------------------Consultant Filter Data-------------------------------
            if (results && results.length > 0) {}
            //-----------------------------------------------------------
            var schedulazioni = this.uniqueElement(results);
            var schedOrdinato = _.sortBy(schedulazioni, 'nomeConsulente');
            if (this.filteredSchedulingClone) {
                _.forEach(this.filteredSchedulingClone, function (n) {
                    var consulenteTrovato = _.find(schedOrdinato, {
                        codiceConsulente: n.codiceConsulente
                    });
                    if (consulenteTrovato) {
                        _.merge(n, consulenteTrovato);
                    }
                    for (var prop in n) {
                        if (n[prop] && !consulenteTrovato[prop]) {
                            delete n[prop]
                        } 
                    }
                }.bind(this));
                schedOrdinato = _.sortBy(this.filteredSchedulingClone, 'nomeConsulente');
            }
            this.pmSchedulingModel.setProperty("/schedulazioni", schedOrdinato);
            this.allScheduling = schedOrdinato;
            if (schedOrdinato.length > 0) {
                this.pmSchedulingModel.setProperty("/nRighe", schedOrdinato.length);
            } else {
                this.pmSchedulingModel.setProperty("/nRighe", 1);
            }

            this.pmSchedulingModel.refresh();
            setTimeout(_.bind(this.onAfter, this));
            jQuery.sap.delayedCall(500, this, function () {
                this.oDialogTable.setBusy(false);
            });
            if (this.oDialog.getBusy() === true) {
                this.oDialog.setBusy(false);
            }
        };
        var fError = function (err) {
            if (this.oDialog.getBusy() === true) {
                this.oDialog.setBusy(false);
            }
            this.oDialogTable.setBusy(false);
            sap.m.MessageToast.show(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        this.oDialogTable.setBusy(true);
        model.tabellaFerie.readFerie(req).then(fSuccess, fError);
    },

    initializeConsultantsFilterModel: function (loadedItems) {
        var data = {
            "results": []
        };
        var items = loadedItems ? loadedItems : this.consultantModel.getData().results;
        var uniqueRes = _.uniq(items);
        this.inputModel.setProperty("/consultantFilterIn", "");
        for (var i = 0; uniqueRes && uniqueRes.length && uniqueRes.length > 0 && i < uniqueRes.length; i++) {
            var obj = _.cloneDeep(uniqueRes[i]);
            obj.selected = false;
            data.results.push(obj);
        }
        this.consultantModel.setData(data);
    },

    onFilterPress: function () {
        if (!this.consultantsFilterDialog) {
            this.consultantsFilterDialog = sap.ui.xmlfragment("view.dialog.filterConsultantForHolidays", this);
            this.getView().addDependent(this.consultantsFilterDialog);
        }
        this.consultantsFilterDialog.open();
    },

    afterOpenFilter: function (evt) {
        this.inputModel.setProperty("/consultantFilterIn", "");
        var list = sap.ui.getCore().byId("consultantSelection");
        //francesco non capisco il perché        
        //if (list && list.getBinding("items")) list.getBinding("items").filter([]);
        list.getBinding("items").filter([]);
    },

    afterCloseFilter: function () {
        //        var lista = sap.ui.getCore().byId("consultantSelection");
        //        var selectedConsultants = lista.getSelectedItems();
        //        if (!!selectedConsultants && selectedConsultants.length > 0) {
        //            lista.removeSelections();
        //        }
        //        if (!!this.filteredScheduling) {
        //            this.filteredScheduling = undefined;
        //        }
    },

    // funzione che costrisce un array di elementi selezionati
    onSelectionChangeFilterConsultants: function (evt) {
        var oList = evt.getSource();
        this.listaSelezioniFiltro = oList.getSelectedContexts(true);
    },

    handleConfirmSelectConsultantDialog: function (evt) {
        var lista = sap.ui.getCore().byId("consultantSelection");
        var selectedConsultants = lista.getSelectedItems();
        var selectedConsultantsCode = [];
        if (this.listaSelezioniFiltro && this.listaSelezioniFiltro.length > 0) {
            for (var i = 0; i < this.listaSelezioniFiltro.length; i++) {
                var path = parseInt(this.listaSelezioniFiltro[i].getPath().split("/")[2]);
                var cons = {};
                cons.codConsulente = this.listaSelezioniFiltro[i].getModel().getData().results[path].codConsulente;
                cons.nomeCognome = this.listaSelezioniFiltro[i].getModel().getData().results[path].nomeCognome;
                selectedConsultantsCode.push(cons);
            }
            var schedulazioni = [];
            for (var j = 0; j < selectedConsultantsCode.length; j++) {
                var schedulazione = _.find(this.allScheduling, {
                    'codiceConsulente': selectedConsultantsCode[j].codConsulente
                });

                if (schedulazione) {
                    schedulazioni.push(schedulazione);
                } else {
                    var obj = {};
                    obj.codiceConsulente = selectedConsultantsCode[j].codConsulente;
                    obj.nomeConsulente = selectedConsultantsCode[j].nomeCognome;
                    obj.giorniScheduling = 0;
                    schedulazioni.push(obj);
                }
            }
            var schedulazioniOrder = _.sortBy(schedulazioni, 'nomeConsulente');
            this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioniOrder);
            this.filteredScheduling = this.pmSchedulingModel.getProperty("/schedulazioni");
            // Aggiungo una variabile clone delle schedulazioni
            this.filteredSchedulingClone = _.cloneDeep(this.filteredScheduling);
        } else {
            this.handleResetSelectConsultantDialog();
        }
        var lunghezzaSchedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni").length;
        if (lunghezzaSchedulazioni === 0) {
            lunghezzaSchedulazioni = 1;
        }
        this.pmSchedulingModel.setProperty("/nRighe", lunghezzaSchedulazioni);
        this.pmSchedulingModel.refresh();
        setTimeout(_.bind(this.onAfter, this));
        jQuery.sap.delayedCall(500, this, function () {
            this.oDialogTable.setBusy(false);
        });
        this.consultantsFilterDialog.close();
        //this.consultantsFilterDialog.destroy(true);
    },

    handleCancelSelectConsultantDialog: function () {
        if (!!this.consultantsFilterDialog && this.consultantsFilterDialog.isOpen()) {
            this.consultantsFilterDialog.close();
            //this.consultantsFilterDialog.destroy(true);
        }
//        this.leggiTabella(this.ric);
    },

    handleResetSelectConsultantDialog: function () {
        this.pmSchedulingModel.setProperty("/schedulazioni", this.allScheduling);
        this.filteredSchedulingClone = undefined;
        this.listaSelezioniFiltro = undefined;
        this.pmSchedulingModel.refresh();
        var lista = sap.ui.getCore().byId("consultantSelection");
        this.pmSchedulingModel.setProperty("/nRighe", this.allScheduling.length);
        var binding = lista.getBinding("items");
        binding.filter([]);
        var selectedConsultants = lista.getSelectedItems();
        if (!!selectedConsultants && selectedConsultants.length > 0) {
            lista.removeSelections();
        }
        if (!!this.filteredScheduling) {
            this.filteredScheduling = undefined;
            this.filteredSchedulingClone = undefined;
        }
        this.handleCancelSelectConsultantDialog();
    },

    handleSearchConsultantDialog: function (evt) {
        var value = evt.getSource().getValue();
        var list = sap.ui.getCore().byId("consultantSelection");
        var filters = [];
        var nameFilter = new sap.ui.model.Filter("nomeCognome", sap.ui.model.FilterOperator.Contains, value);
        var codeFilter = new sap.ui.model.Filter("codConsulente", sap.ui.model.FilterOperator.Contains, value);
        filters.push(nameFilter);
        filters.push(codeFilter);
        filters = new sap.ui.model.Filter({
            filters: filters,
            and: false
        });
        var binding = list.getBinding("items");
        binding.filter(filters);
    },

    loadConsultants: function (params) {
        var defer = Q.defer();
        var fSuccess = function (result) {
            if (!!result.items) this.initializeConsultantsFilterModel(result.items);
            defer.resolve();
        };
        var fError = function (err) {
            sap.m.MessageToast("Consultant not added.");
            defer.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        var filterString = "?$filter=COD_CONSULENTE_BU eq '" + params.cod_bu + "'";
        model.Consultants.readConsultants(filterString).then(fSuccess, fError);
        return defer.promise;
    },

    onLaunchpadPress: function () {
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.onNavBackPress();
    },

    onAfterRendering: function () {
        this.pmSchedulingModel.refresh();
    },

    svuotaTabella: function () {
        // svuoto la tabella
        this.getView().byId("idSchedulazioniFerieTable").setVisible(true);
        this.pmSchedulingModel.setProperty("/schedulazioni", []);
        this.pmSchedulingModel.setProperty("/nRighe", 1);
        this.pmSchedulingModel.getData().schedulazioni = [];
    },

    svuotaSelectBu: function () {
        this.inputModel.setProperty("/codiceBu", "");
    },

    refreshTable: function () {
        this.leggiTabella(this.ric);
    },

    checkInput: function () {
        var buSelect = this.getView().byId("idSelectBu");
        var periodSelect = this.getView().byId("idSelectPeriodo");
        var isInputComplete = (buSelect.getSelectedKey() !== "" && periodSelect.getSelectedKey() !== "");
        this.uiModel.setProperty("/inputComplete", isInputComplete);
    },

    //----------------------------Notes-------------------------
    addNoteToHoliday: function (evt) {
        var idButton = this.oButton.getId();
        var nButton = parseInt((idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1));
        if (nButton) var nota = "nota" + (nButton.toString());
        var row = parseInt(idButton.substring(idButton.indexOf("row") + 3, idButton.length));
        var consultant = this.pmSchedulingModel.getData().schedulazioni[row];
        var stato = "stato" + (nButton.toString());
        var icon = "ggIcon" + (nButton.toString());
        this.noteModel.setProperty("/note", consultant[nota]);
        var jobName = evt.getSource().data("job");
        var codReq = nota.substring(4);
        var propReq = "req" + codReq;
        this.dataForUpdate = {
            "codConsulente": consultant.codiceConsulente,
            "dataReqPeriodo": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "idCommessa": "AA00000008",
            "codAttivitaRichiesta": consultant[icon].split("//")[1],
            "idPeriodoSchedulingRic": this.getView().byId("idSelectPeriodo").getSelectedKey(),
            //            "codPjm": this.user.codice_consulente,
            "codPjm": "CC00000000",
            "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "note": "",
            "statoReq": consultant[stato],
            "giorno": codReq.indexOf("a") >= 0 ? parseInt(codReq.substr(0, codReq.length - 1)) : parseInt(codReq),
            "id_req": consultant[propReq] ? consultant[propReq].toString() : ""
        };
        this.onAddNote();
    },

    onNotePress: function (evt) {
        var nota = evt.getSource().data("note");
        var consultant = evt.getSource().getBindingContext("pmSchedulingModel").getObject();
        this.noteModel.setProperty("/note", consultant[nota]);
        var jobName = evt.getSource().data("job");
        var codReq = nota.substring(4);
        var propReq = "req" + codReq;
        var icon = "ggIcon" + codReq;
        this.dataForUpdate = {
            "codConsulente": consultant.codiceConsulente,
            "dataReqPeriodo": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "idCommessa": "AA00000008",
            "codAttivitaRichiesta": consultant[icon].split("//")[1],
            "idPeriodoSchedulingRic": this.getView().byId("idSelectPeriodo").getSelectedKey(),
            //            "codPjm": this.user.codice_consulente,
            "codPjm": "CC00000000",
            "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "note": "",
            "statoReq": "CONFIRMED",
            "giorno": codReq.indexOf("a") >= 0 ? parseInt(codReq.substr(0, codReq.length - 1)) : parseInt(codReq),
            "id_req": consultant[propReq] ? consultant[propReq].toString() : ""
        };
        this.onAddNote();
    },
    //------------------------------------------------------------
    // **********************noteDialog********************
    onAddNote: function (evt) {
        if (!this._noteDialog) {
            this._noteDialog = sap.ui.xmlfragment("view.dialog.noteDialog", this);
            this.getView().addDependent(this._noteDialog);
        }
        this.noteModel.refresh();
        this._noteDialog.open();
    },

    onAfterCloseNoteDialog: function (evt) {
        this.noteModel.setProperty("/note", undefined);
        this.pmSchedulingModel.refresh();
    },

    onCancelNoteDialogPress: function (evt) {
        if (this._noteDialog) {
            this._noteDialog.close();
            this.pmSchedulingModel.refresh();
        }
    },

    onSaveNoteDialogPress: function (evt) {
        var fSuccess = function () {
            this.onCancelNoteDialogPress();
            sap.m.MessageToast.show(this._getLocaleText("NOTE_ADDED"));
            var schedulazioneConsulente = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                codiceConsulente: this.dataForUpdate.codConsulente
            });
            var prop = "nota" + this.dataForUpdate.giorno;
            var propReq = "req" + this.dataForUpdate.giorno;
            if (schedulazioneConsulente[propReq] !== parseInt(this.dataForUpdate.id_req)) {
                prop = "nota" + this.dataForUpdate.giorno + "a";
            }
            schedulazioneConsulente[prop] = this.dataForUpdate.note;
            // funzione salvataggio per undo
            this.pmSchedulingModel.refresh();
        };
        var fError = function (err) {
            sap.m.messageToast.show(this._getLocaleText("ERROR_NOTE_UPDATE"));
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        this.dataForUpdate.note = this.noteModel.getData().note ? this.noteModel.getData().note : "";
        if (this.dataForUpdate.note.trim() === "") {
            this.dataForUpdate.note = undefined;
            if (this._noteDialog) {
                this._noteDialog.close();
                this.pmSchedulingModel.refresh();
            }
            this.leggiTabella(this.ric);
        } else {
            //            model.requestSave.updateNote(this.dataForUpdate).then(fSuccess, fError);
            this.updateHoliday(this.dataForUpdate, undefined, undefined, undefined, undefined, true);
            this._noteDialog.close();
        }
    },

    // **********************noteDialog********note************
    onPressAcceptAll: function (evt) {
        var src = evt.getSource();
        var linkId = src.getId();
        this.chosenRow = parseInt(linkId.substring(linkId.indexOf("row") + 3, linkId.length));
        sap.m.MessageBox.confirm(this._getLocaleText("acceptAllConfirm"), {
            title: this._getLocaleText("acceptAllConfirmTitle"),
            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            onClose: _.bind(this.confirmAcceptAll, this)
        });
    },

    confirmAcceptAll: function (response) {
        if (response === "YES") {
            var consultant = this.pmSchedulingModel.getData().schedulazioni[this.chosenRow];
            var codConsulente = consultant.codiceConsulente;
            var codAttivitaRichiesta = "";
            var comboBoxPeriod = this.getView().byId("idSelectPeriodo");
            var idPeriodo = comboBoxPeriod.getSelectedKey();
            var stato = "CONFIRMED";
            var newArr = [];
            for (prop in consultant) {
                if (prop.indexOf("stato") > -1) {
                    if (consultant[prop] === "PROPOSED") {
                        newArr.push(prop);
                    }
                }
            }
            console.log(newArr);
            var batchArray = [];
            var entityArray = [];
            for (var i = 0; i < newArr.length; i++) {
                var obj = {};
                var idx = newArr[i].replace(/^\D+/g, '');
                var ggIcon = "ggIcon" + idx;
                var req = "req" + idx;
                var id_req = consultant[req];
                if (consultant[ggIcon].indexOf("travel-itinerary") >= 0) {
                    codAttivitaRichiesta = "travel-itinerary";
                } else {
                    codAttivitaRichiesta = "bed";
                }
                obj.consultant = consultant;
                obj.idx = idx;
                obj.codAttivitaRichiesta = codAttivitaRichiesta;
                batchArray.push(obj);
                var entity = {
                    'idReq': id_req,
                    'stato': stato
                };
                entityArray.push(entity);
            }
            this.changeStatusAndWriteToFinal(entityArray, batchArray);
        }
    },

    changeStatusAndWriteToFinal: function (entityArray, batchArray) {
        var fSuccess = function (ok) {
            this.writeBatchToFinal(batchArray);
            this.leggiTabella(this.ric);
            console.log("passed from cambiareStatoRichiesta");
        };
        var fError = function (err) {
            console.log("passed from error cambiareStatoRichiesta");
            var msgKo = "Errore in modifica richiesta";
            sap.m.MessageToast.show(msgKo);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.requestSave.changeStatusBatchRequest(entityArray).then(fSuccess, fError);
    },

    writeBatchToFinal: function (batchArrayIn) {
        var msgOk = "Richieste modificata correttamente";
        var msgKo = "Errore in modifica richieste";
        var batchArrayOut = [];
        for (var i = 0; i < batchArrayIn.length; i++) {
            var dataToSend = {
                "codConsulente": batchArrayIn[i].consultant.codiceConsulente,
                "dataReqPeriodo": this.calcolareDataRichiesta(batchArrayIn[i].idx),
                "idCommessa": "AA00000008",
                "codAttivitaRichiesta": batchArrayIn[i].codAttivitaRichiesta,
                "idPeriodoSchedulingRic": this.getView().byId("idSelectPeriodo").getSelectedKey(),
                "codPjm": "CC00000000",
                "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
                "note": "",
                "statoReq": "CONFIRMED",
                "giorno": batchArrayIn[i].idx
            };
            batchArrayOut.push(dataToSend);
        }
        var fSuccess = function (result) {
            sap.m.MessageToast.show(msgOk);
            //**
            var arrayStates = [];
            var myRow = this.pmSchedulingModel.getData().schedulazioni[0];
            for (var prop in myRow) {
                if (myRow[prop] == "PROPOSED") {
                    arrayStates.push(prop);
                }
            }
            for (var i = 0; i < arrayStates.length; i++) {
                var stato = arrayStates[i];
                myRow[stato] = "CONFIRMED";
            }

            this.pmSchedulingModel.refresh();
            //**
        };
        var fError = function (err) {
            sap.m.MessageToast.show(msgKo);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.tabellaSchedulingAdmin.sendBatchFerieFinalDraft(batchArrayOut).then(fSuccess, fError);
    },


    saveHolydayInDraft: function (req) {
        var msgOk = "Richiesta inserita correttamente";
        var msgKo = "Errore in inserimento richiesta";


        this.dataToSave = req;

        var fSuccess = function (ok) {
            sap.m.MessageToast.show(msgOk, {
                duration: 750,
            });


        };
        var fError = function (err) {
            sap.m.MessageToast.show(JSON.parse(err.responseText));
            this.leggiTabella(this.ric);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        /*
        {"codConsulenteDraft":"CC00000027","dataReqDraft":"2017-05-01","idCommessaDraft":"AA00000310","codAttivitaRichiestaDraft":"complete","idPeriodoSchedulingDraft":12,"codPjmDraft":"CC00000078","dataInserimentoDraft":"2017-03-28","noteDraft":"","giornoPeriodoDraft":"1","statoReqDraft":"CONFIRMED","oldIdReqDraft":1,"idReqOriginal":0}:
        */

        model.requestSaveToDraft.sendRequest(req)
            .then(fSuccess, fError);
    },

    updateHoliday: function (req, dataModelConsultant, propReq, nButton, codAttivitaRichiesta, note) {
        var fSuccess = function (res) {
            if (req.statoReq !== "PROPOSED") {
                this.updateHolidaySuFinal(dataModelConsultant, propReq, nButton, codAttivitaRichiesta, req, note);
            } else {
                this.leggiTabella(this.ric);
            }
        };
        var fError = function (err) {
            var msgKo = "Errore in inserimento richiesta";
            sap.m.MessageToast.show(msgKo);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.requestSave.sendRequest(req).then(fSuccess, fError);
    },

    updateHolidaySuFinal: function (dataModelConsultant, propReq, nButton, codAttivitaRichiesta, req, note) {
        var msgOk = "Richiesta modificata correttamente";
        var msgKo = "Errore in modifica richiesta";
        var dataToSend = {};
        if (!note) {
            dataToSend = {
                "codConsulente": dataModelConsultant.codiceConsulente,
                "codAttivita": codAttivitaRichiesta,
                "idPeriodoFinal": this.getView().byId("idSelectPeriodo").getSelectedKey(),
                "giornoPeriodoFinal": nButton
            };
        } else {
            dataToSend = {
                "codConsulente": req.codConsulente,
                "codAttivita": req.codAttivitaRichiesta,
                "idPeriodoFinal": req.idPeriodoSchedulingRic,
                "giornoPeriodoFinal": req.giorno,
                "note": req.note
            };
        }

        var fSuccess = function (result) {
            this.leggiTabella(this.ric);
        };
        var fError = function (err) {
            sap.m.MessageToast.show(msgKo);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.tabellaSchedulingMaria.updateHolidaySuFinal(dataToSend).then(fSuccess, fError);
    },


});