jQuery.sap.require("view.abstract.AbstractController");

view.abstract.AbstractController.extend("view.AnalisiDisponibilitaRisorse", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.tableAnalisiDisponibilitaModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.tableAnalisiDisponibilitaModel, "tableAnalisiDisponibilitaModel");

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");

        this.filterModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.filterModel, "filterModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.userModel.setData(this.user);

        //        if (name !== "analisiDisponibilitaRisorse") {
        //            return;
        //        }

        if (!this._checkRoute(evt, "analisiDisponibilitaRisorse")) {
            return;
        }

        this.nRigheTabella = 10; // variabile per settare il numero di righe da visualizzare
        this.oDialog = this.getView().byId("idBlockLayoutAnalisiDisponibilitaRisorse");
        this.oDialogTable = this.getView().byId("idTableAnalisiDisponibilitaRisorse");
        this.oDialogTable.setVisible(false);

        this.tableAnalisiDisponibilitaModel.setProperty("/giorniLavorativi", 0);

        this.readPeriods();
    },

    // funzione per la navigazione alla lauchpad
    onLaunchpadPress: function () {
        this.tableAnalisiDisponibilitaModel.setProperty("/richieste", undefined);
        if (this.oDialog.getBusy()) {
            this.oDialog.setBusy(false);
        }
        if (this.oDialogTable.getBusy()) {
            this.oDialogTable.setBusy(false);
        }
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.onNavBackPress();
    },

    // Funzione caricamento periodi
    readPeriods: function () {
        var fSuccess = function (result) {
            var res = {};
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for (var i = 0; i < 13; i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.periodModel.setData(res13);
            this.getView().byId("idSelectPeriodoAnalisiDisponibilita").setSelectedKey("");
            this.getView().byId("idSelectPeriodoAnalisiDisponibilita").setValue("");
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            if (!this.oneTimeReadPeriods) {
                // funzione che ripete la chiamata di login nel caso in cui sia scaduta l'autenticazione al sistema
                this.oneTimeReadPeriods = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(this.readPeriods(), this));
            } else {
                this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'getPeriods'");
            }
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    // funzione che, al cambio del periodo, svuota la tabella
    // successivamente popola la tabella con il nuovo periodo selezionato
    onChangePeriod: function (pe) {
        this.oDialog.setBusy(true);
        this.svuotaTabella();

        this.pe = pe;
        var p = pe.getSource().getSelectedItem().getProperty("text").replace(/ /g, '');
        this.period = parseInt(pe.getSource().getProperty("selectedKey"));
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': this.period
        });
        model.persistence.Storage.session.save("periodSelected", periodSelect);
        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();

        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = pInizio;
        var dataF = pFine;

        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

        this.giorniTotali = this.arrayGiorni.length;
        this.giorniFestivi = _.union(this.weekendDays).length;
        this.giorniLavorativi = this.giorniTotali - this.giorniFestivi;
        this.tableAnalisiDisponibilitaModel.setProperty("/giorniLavorativi", this.giorniLavorativi);
        this._initializeDayCountRange(this.giorniLavorativi);

        this.oDialogTable.setVisible(true);

        this.oDialogTable.setBusy(true);
        setTimeout(_.bind(this._loadTable, this));
    },

    _loadTable: function () {
        var fSuccess = function (result) {
            var richieste = result;
            this.richiesteDaServer = richieste;
            this.tableAnalisiDisponibilitaModel.setProperty("/richieste", richieste.richiesteScheduling);
            if (richieste.richiesteScheduling.length > this.nRigheTabella) {
                this.tableAnalisiDisponibilitaModel.setProperty("/nRighe", this.nRigheTabella);
            } else if (richieste.richiesteScheduling.length === 0) {
                this.tableAnalisiDisponibilitaModel.setProperty("/nRighe", 1);
            } else {
                this.tableAnalisiDisponibilitaModel.setProperty("/nRighe", richieste.richiesteScheduling.length);
            }

            this._closeButtonExpand();
            this.oDialog.setBusy(false);
            this.oDialogTable.setBusy(false);
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore lettura XSJS 'readAnalisiCapacitaRisorse'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var req = {};
        req.idPeriodo = this.period;
        model.getAnalisiDisponibilita.read(req)
            .then(fSuccess, fError);
    },

    onExpandFirstLevel: function (evt) {
        var button = this.getView().byId("idToggleButtonExpand");
        var oTreeTable = this.getView().byId("idTableAnalisiDisponibilitaRisorse");
        if (evt.getSource().getProperty("pressed")) {
            button.setText("Comprimi");
            button.setIcon("sap-icon://collapse-group");
            oTreeTable.expandToLevel(1);
        } else {
            this._closeButtonExpand();
        }
    },

    _closeButtonExpand: function () {
        var button = this.getView().byId("idToggleButtonExpand");
        var oTreeTable = this.getView().byId("idTableAnalisiDisponibilitaRisorse");
        button.setText("Espandi");
        button.setIcon("sap-icon://expand-group");
        button.setPressed(false);
        oTreeTable.collapseAll();
    },

    onPressFiltroNgiornate: function () {
        var page = this.getView().byId("idPageCapacitaDisponibilitaRisorse");
        //if (!this.filterDialog) {
        this.filterDialog = sap.ui.xmlfragment("view.dialog.filterDialogAnalisiDisponibilita", this);
        page.addDependent(this.filterDialog);
        //}
        this.filterDialog.open();
    },

    _initializeDayCountRange: function (maxDay) {
        var dayRange = {
            "dayMin": 0,
            "dayMax": maxDay,
            "valMin": 0,
            "valMax": 0,
            "range": [0, maxDay]
        };
        this.filterModel.setProperty("/dayRange", dayRange);
    },

    onSliderLiveChange: function (evt) {
        //var value = parseFloat(evt.getSource().getValue());
        //var value2 = parseFloat(evt.getSource().getValue2());
        var value = parseFloat(evt.getSource().getRange()[0]);
        var value2 = parseFloat(evt.getSource().getRange()[1]);
        this.filterModel.setProperty("/dayRange/valMin", value.toString());
        this.filterModel.setProperty("/dayRange/valMax", value2.toString());
    },

    onFilterUMDialogOK: function () {
        this._filterFunc();
        var valueMin = this.filterModel.getProperty("/dayRange/valMin");
        var valueMax = this.filterModel.getProperty("/dayRange/valMax");
        var dayMin = this.filterModel.getProperty("/dayRange/dayMin");
        var dayMax = this.filterModel.getProperty("/dayRange/dayMax");
        if (dayMin !== valueMin || valueMax !== dayMax) {
            this.getView().byId("idTextFiltri").setText("Filtro attivo: Da: " + valueMin + " A: " + valueMax);
        }
        this.onFilterUMDialogClose();
    },

    _filterFunc: function () {
        var filteredSchedulations = _.filter(this.richiesteDaServer.richiesteScheduling, _.bind(function (item) {
            var itemDay = item.nTotale ? item.nTotale : 0;
            var result = (itemDay >= parseFloat(this.filterModel.getProperty("/dayRange/valMin")) && itemDay <= parseFloat(this.filterModel.getProperty("/dayRange/valMax")));
            return result;
        }, this));

        this.tableAnalisiDisponibilitaModel.setProperty("/richieste", filteredSchedulations);
    },

    onFilterUMDialogClose: function () {
        this._closeButtonExpand();
        this.filterDialog.close();
        this.filterDialog.destroy(true);
    },

    onFilterUMDialogRipristino: function () {
        this.tableAnalisiDisponibilitaModel.setProperty("/richieste", this.richiesteDaServer.richiesteScheduling);
        this.getView().byId("idTextFiltri").setText("");
        setTimeout(_.bind(this.onFilterUMDialogClose, this));
    },

    svuotaTabella: function () {
        this.tableAnalisiDisponibilitaModel.setProperty("/richieste", []);
    },

    onDownloadExcelPress: function () {
        this.getView().setBusy(true);
        var richieste = this.tableAnalisiDisponibilitaModel.getProperty("/richieste");
        if (!richieste || !richieste.length) {
            sap.m.MessageBox.error(this._getLocaleText("noDataToDownload"));
            this.getView().setBusy(false);
            return;
        }
        setTimeout(_.bind(this._downloadExcel, this));
    },

    _downloadExcel: function () {
        var getWorkbook = function () {
            return XlsxPopulate.fromBlankAsync();
        };

        var generate = function (type) {
            return getWorkbook()
                .then(function (workbook) {
                    var tableItems = this.tableAnalisiDisponibilitaModel.getProperty("/richieste");

                    // intestazione Tabella
                    var columnsArray = ["A", "B", "C", "D", "E", "F", "G", "H", "I"];
                    var textHeader = ["CONSULENTI", "TIPO_AZIENDA", "BU_CONSULENTE", "COMMESSA", "DESCRIZIONE_COMESSA", "GIORNATE_PER_COMMESSA", "NOME_PJM_COMMESSA", "NOME_BU_COMMESSA", "MODULO_COMMESSA"];
                    var headerColumnsName = [];
                    for (var i = 0; i < textHeader.length; i++) {
                        var labelName = this._getLocaleText(textHeader[i]);
                        headerColumnsName.push(labelName);
                    }

                    // definisco la testata della tabella
                    for (i = 0; i < headerColumnsName.length; i++) {
                        workbook.sheet(0).column(columnsArray[i]).width(20);
                        var cell = columnsArray[i] + "1";
                        workbook.sheet(0).cell(cell).style("bold", true);
                        workbook.sheet(0).cell(cell).style("borderStyle", "thin");
                    }
                    workbook.sheet(0).cell("A1").value([headerColumnsName]);

                    // definisco i dati della tabella
                    var rows = [];
                    for (i = 0; i < tableItems.length; i++) {
                        rows = rows.concat(tableItems[i].commesseFinal);
                    }
                    for (i = 0; i < rows.length; i++) {
                        var dataValues = [
                            rows[i].nomeCompleto,
                            rows[i].tipoAzienda,
                            rows[i].buConsulente,
                            rows[i].nomeCommessa,
                            rows[i].descCommessa,
                            rows[i].nGiornate,
                            rows[i].nomePJM,
                            rows[i].nomeBU,
                            rows[i].skill
                        ];
                        var rowNumber = i + 2;
                        var startRange = "A" + rowNumber;
                        workbook.sheet(0).cell(startRange).value([dataValues]);
                    }
                    return workbook.outputAsync({
                        type: type
                    });
                }.bind(this));
        }.bind(this);

        var generateBlob = function (type) {
            return generate()
                .then(function (blob) {
                    this.title = this._getLocaleText("titleFileDownload");
                    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                        window.navigator.msSaveOrOpenBlob(blob, this.title + ".xlsx");
                    } else {
                        var url = window.URL.createObjectURL(blob);
                        var a = document.createElement("a");
                        document.body.appendChild(a);
                        a.href = url;
                        a.download = this.title + "_" + this.getView().byId("idSelectPeriodoAnalisiDisponibilita").getValue() + ".xlsx";
                        a.click();
                        window.URL.revokeObjectURL(url);
                        document.body.removeChild(a);
                    }
                    this.getView().setBusy(false);
                }.bind(this))
                .catch(function (err) {
                    alert(err.message || err);
                    throw err;
                });
        }.bind(this);
        generateBlob();
    }
});