jQuery.sap.require("view.abstract.AbstractMasterController");

view.abstract.AbstractMasterController.extend("view.ListaPeriodi", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        
        this.modelPeriodo = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelPeriodo, "modelPeriodo");
        
        this.uiModel.setProperty("/searchProperty", ["stato", "idPeriod", "mesePeriodo", "annoPeriodo"]);
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "periodList") {
            return;
        }
        
        this.oDialog = this.getView().byId("list");
        this.oDialog.removeSelections();
        this.periodsReader = new model.Periods();
        this.modelPeriodo.setData([]);
        this.readPeriods();
    },

    readPeriods: function () {
        this.oDialog.setBusy(true);
        var fSuccess = function (result) {
            this.modelPeriodo.setData(result.items);
            this.console(result.items);
            this.oDialog.setBusy(false);
        };

        var fError = function () {
            this._enterErrorFunction("error", "Errore", "Errore in lettura periodi");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.chiamataForLoginToHana()
            .then(this.periodsReader.getPeriods()
                .then(fSuccess, fError));
    },

    onHomePress: function () {
        this.router.navTo("launchpad");
    },
    
    onPeriodSelectBis: function (evt) {
        var src = evt.getSource();
        if (src.getSelectedItems()[0]) {
            var selectedItem = src.getSelectedItems()[0].getBindingContext("modelPeriodo").getObject();
            this.router.navTo("periodDetail", {
                detailId: selectedItem.idPeriod,
                year: selectedItem.annoPeriodo
            });
        }
    },

    addPeriodPress: function () {
        var nextPeriod = this.periodsReader.getNewInstance();
        console.log("----New Period----");
        console.log(nextPeriod);
        if (sessionStorage.getItem("selectedPeriod")) {
            sessionStorage.removeItem("selectedPeriod");
        }
        var session = jQuery.sap.storage(jQuery.sap.storage.Type.session);
        session.put("suggestedPeriod", nextPeriod);
        this.router.navTo("periodModify", {
            state: "new"
        });
    },

    onFilterPress: function () {
        this.filterModel = model.filters.Filter.getModel(this.modelPeriodo.getData(), "modelPeriodo");
        this.getView().setModel(this.filterModel, "filter");
        var page = this.getView().byId("ListaPeriodiId");
        this.filterDialog = sap.ui.xmlfragment("view.dialog.filterDialog", this);
        page.addDependent(this.filterDialog);
        this.filterDialog.open();

    },

    onFilterPropertyPress: function (evt) {
        var parentPage = sap.ui.getCore().byId("parent");
        var elementPage = sap.ui.getCore().byId("children");
        console.log(this.getView().getModel("filter").getData().toString());
        var navCon = sap.ui.getCore().byId("navCon");
        var selectedProp = evt.getSource().getBindingContext("filter").getObject();
        this.getView().getModel("filter").setProperty("/selected", selectedProp);
        this.elementListFragment = sap.ui.xmlfragment("view.fragment.filterList", this);
        elementPage.addContent(this.elementListFragment);
        navCon.to(elementPage, "slide");
        this.getView().getModel("filter").refresh();
    },

    onBackFilterPress: function () {
        this.navConBack();
        this.getView().getModel("filter").setProperty("/selected", "");
        this.elementListFragment.destroy();
    },
    
    navConBack: function () {
        var navCon = sap.ui.getCore().byId("navCon");
        navCon.to(sap.ui.getCore().byId("parent"), "slide");
        this.elementListFragment.destroy();
    },
    
    afterOpenFilter: function (evt) {
        var navCon = sap.ui.getCore().byId("navCon");
        if (navCon.getCurrentPage().getId() == "children")
            navCon.to(sap.ui.getCore().byId("parent"), "slide");
        this.getView().getModel("filter").setProperty("/selected", "");
    },

    onSearchFilter: function (oEvt) {
        var aFilters = [];
        var sQuery = oEvt.getSource().getValue();

        if (sQuery && sQuery.length > 0) {
            aFilters.push(this.createFilter(sQuery, "value"));
        }

        // update list binding
        var list = sap.ui.getCore().byId("filterList");
        var binding = list.getBinding("items");
        binding.filter(aFilters);
    },
    
    createFilter: function (query, property) {
        var filter = new sap.ui.model.Filter({
            path: property,
            test: function (val) {
                var prop = val.toString().toUpperCase();
                return (prop.indexOf(query.toString().toUpperCase()) >= 0)
            }
        });
        return filter;
    },
    
    onFilterDialogClose: function (evt) {
        if (this.elementListFragment) {
            this.elementListFragment.destroy();
        }
        if (this.filterDialog) {
            this.filterDialog.close();
            this.filterDialog.destroy();
        }
    },
    
    onFilterDialogOK: function (evt) {
        var filterItems = model.filters.Filter.getSelectedItems("modelPeriodo");
        if (this.elementListFragment)
            this.elementListFragment.destroy();
        this.filterDialog.close();
        this.getView().getModel("filter").setProperty("/selected", "");
        this.handleFilterConfirm(filterItems);
        this.filterDialog.destroy();
        delete(this.filterDialog);
    },
    
    handleFilterConfirm: function (selectedItems) {
        var filters = [];
        _.forEach(selectedItems, _.bind(function (item) {
                filters.push(this.createFilter(item.value, item.property));
            },
            this));
        var list = this.getView().byId("list");
        var binding = list.getBinding("items");
        binding.filter(filters);
    },
    
    onResetFilterPress: function () {
        model.filters.Filter.resetFilter("modelPeriodo");
        var list = this.getView().byId("list");
        var binding = list.getBinding("items");
        binding.filter();
        if (this.elementListFragment) {
            this.elementListFragment.destroy();
        }
        if (this.filterDialog) {
            this.filterDialog.close();
            this.filterDialog.destroy();
        }
    }
});