jQuery.sap.require("view.abstract.AbstractMasterController");

view.abstract.AbstractMasterController.extend("view.ProjectList", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.modelCommesse = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelCommesse, "modelCommesse");
        this.modelCommesse.setSizeLimit(1000);

        this.tempModelData = new sap.ui.model.json.JSONModel();

        this.uiModel.setProperty("/searchProperty", ["nomeCommessa", "codCommessa", "cognomeConsulente", "descrizioneCommessa"]);
    },

    handleRouteMatched: function (evt) {
        var name = evt.getParameter("name");
        this.name = name;
        if (name !== "projectList") {
            return;
        }
        var prefTitle = icms.Component.getMetadata().getConfig().settings.prefTitle;
        document.title = prefTitle + " _ PROGETTI";
        
        view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
        this.getView().byId("list").removeSelections();

        if (model.persistence.Storage.session.get("selectedProject")) {
            model.persistence.Storage.session.remove("selectedProject");
        }
        this.oDialog = this.getView().byId("list");

        this.readProjects(undefined);
    },

    readProjects: function (req) {
        var fSuccess = function (result) {
            this.modelCommesse.setProperty("/commesse", result.items);
            console.log("Projects");
            console.log(result);
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            this.oDialog.setBusy(false);
            console.log("errore in lettura progetti");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        
        model.odata.chiamateOdata.chiamataForLoginToHana()
            .then(model.Projects.newReadProjects(req)
                .then(fSuccess, fError));
    },

    onLaunchpadPress: function () {
        this.router.navTo("launchpad");
        if (model.persistence.Storage.session.get("selectedProject")) {
            model.persistence.Storage.session.remove("selectedProject");
        }
    },

    onFilterPress: function () {
        var stato = [{
            "value": "Aperto"
        }, {
            "value": "Chiuso"
        }];

        var page = this.getView().byId("listaCommesse");
        if (!this.projectDialog) {
            this.projectDialog = sap.ui.xmlfragment("view.dialog.filterDialogProjects", this);
        }

        this.tempModelData.setData(stato);

        this.getView().setModel(this.tempModelData, "filter");
        page.addDependent(this.projectDialog);

        this.projectDialog.open();
    },

    onFilterDialogClose: function () {
        this.projectDialog.close();
    },

    onFilterDialogOK: function () {
        this.projectDialog.close();

//        var filtro = new sap.ui.model.Filter('statoCommessa', sap.ui.model.FilterOperator.Contains, this.arrayFiltri[0]);
//        var table = this.getView().byId("list");
//        var binding = table.getBinding("items");
//        binding.filter(filtro);
        if (this.name === "projectDetail" || this.name === "projectModify") {
            this.router.navTo("projectList");
        }
        this.readProjects(this.arrayFiltri[0]);
    },

    onFilterDialogRipristino: function () {
        this.projectDialog.close();
        sap.ui.getCore().getElementById("listaFiltroStatoProgetti").removeSelections();
//        var table = this.getView().byId("list");
//        var binding = table.getBinding("items");
//        binding.filter();
        this.readProjects();
    },

    filterPress: function (evt) {
        var numeroFiltri = evt.getSource().getSelectedItems();
        var arrayFiltri = [];
        for (var i = 0; i < numeroFiltri.length; i++) {
            arrayFiltri.push((numeroFiltri[i].getProperty("title")).toUpperCase());
        }
        this.arrayFiltri = arrayFiltri;
        //        console.log(arrayFiltri);
    },

    addProjectPress: function () {
        this.router.navTo("projectModify", {
            state: "new"
        });
        if (sessionStorage.getItem("selectedProject")) {
            sessionStorage.removeItem("selectedProject");
        }
    },

    //    onProjectSelect: function (evt) {
    //        var src = evt.getSource();
    //        var selectedItem = src.getBindingContext("modelCommesse").getObject();
    //        sessionStorage.setItem("selectedProject", JSON.stringify(selectedItem));
    //        this.router.navTo("projectDetail", {
    //            detailId: selectedItem.codCommessa
    //        });
    //    },

    onProjectSelectBis: function (evt) {
        var src = evt.getSource();
        if (src.getSelectedItems()[0]) {
            var selectedItem = src.getSelectedItems()[0].getBindingContext("modelCommesse").getObject();
            sessionStorage.setItem("selectedProject", JSON.stringify(selectedItem));
            this.router.navTo("projectDetail", {
                detailId: selectedItem.codCommessa
            });
        }
    },
});