jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.Periods");
jQuery.sap.require("model.Period");

//jQuery.sap.require("model.AppModel");


view.abstract.AbstractController.extend("view.DetailPeriodi", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.detailPeriodo = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailPeriodo, "detailPeriodo");


    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "periodDetail") {
            return;
        }
        this.oDialog = this.getView().byId("BusyDialog");

        var id = parseInt(evt.getParameter("arguments").detailId);
        var year = parseInt(evt.getParameter("arguments").year);
        this.periodLoad(id, year)
            .then(_.bind(function (result) {
                    this.refreshView();
                }, this),
                _.bind(function (err) {
                    this.router.navTo("periodList");
                }, this))
        //var periodo = JSON.parse(sessionStorage.getItem("selectedPeriod"));
        // this.period= new model.Period(periodo);
        //
        // this.detailPeriodo.setProperty("/annoPeriodo", periodo.annoPeriodo);
        // this.detailPeriodo.setProperty("/dataFine", periodo.dataFine);
        // this.detailPeriodo.setProperty("/dataFineRichieste", periodo.dataFineRichieste);
        // this.detailPeriodo.setProperty("/dataInizio", periodo.dataInizio);
        // this.detailPeriodo.setProperty("/idPeriod", periodo.idPeriod);
        // this.detailPeriodo.setProperty("/mesePeriodo", periodo.idPeriod);
        // this.detailPeriodo.setProperty("/stato", periodo.stato);
        // //----------------Added----------------------------------------
        // this.detailPeriodo.setProperty("/path", periodo.path);


    },

    periodLoad: function (id, year) {
        var defer = Q.defer();
        this.oDialog.setBusy(true);
        var fSuccess = function (result) {
            this.oDialog.setBusy(false);
            this.period = new model.Period(result);
            defer.resolve(this.period);
        }
        fSuccess = _.bind(fSuccess, this);

        var fError = function (err) {
            sap.m.MessageToast(this._getLocaleText("LOADING_PERIOD_ERROR"));
            this.oDialog.setBusy(false);
            defer.reject(err);
        }
        fError = _.bind(fError, this);

        model.Periods().readPeriod(id, year)
            .then(fSuccess, fError);

        return defer.promise;
    },
    refreshView: function () {
        // this.detailPeriodo.setProperty("/annoPeriodo", this.period.annoPeriodo);
        // this.detailPeriodo.setProperty("/dataFine", this.period.dataFine);
        // this.detailPeriodo.setProperty("/dataFineRichieste", this.periodo.dataFineRichieste);
        // this.detailPeriodo.setProperty("/dataInizio", this.periodo.dataInizio);
        // this.detailPeriodo.setProperty("/idPeriod", this.periodo.idPeriod);
        // this.detailPeriodo.setProperty("/mesePeriodo", this.periodo.idPeriod);
        // this.detailPeriodo.setProperty("/stato", this.periodo.stato);
        // //----------------Added----------------------------------------
        // this.detailPeriodo.setProperty("/path", this.periodo.path);
        this.detailPeriodo.setData(this.period);
        this.detailPeriodo.refresh();
    },



    onHomePress: function () {
        this.router.navTo("launchpad");
    },

    modifyPeriodPress: function () {

        this.router.navTo("periodModify", {
            state: "modify",
            detailId: this.detailPeriodo.getProperty("/idPeriod"),
            year: this.period.annoPeriodo
        });

    },

    deletePeriodPress: function () {

        sap.m.MessageBox.show(this._getLocaleText("CONFIRMDELETEPERIOD"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRMDELETEPERIODTITLE"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmDeletePress, this)
        );
    },
    _confirmDeletePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            var fSuccess = function (result) {
                this.oDialog.setBusy(false);
                sap.m.MessageToast.show(this._getLocaleText("PERIOD_DELETE_SUCCESS"));
                this.router.navTo("periodList");
            }
            var fError = function (err) {
                this.oDialog.setBusy(false);
                console.log(err);
                sap.m.MessageToast.show(this._getLocaleText("PERIOD_DELETE_ERROR"));
                console.log("errore nel salvataggio");
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            // model.Periods.deletePeriod(this.detailPeriodo.getProperty("/idPeriod"))
            //     .then(fSuccess, fError);
            this.period.deleteODataPeriod()
                .then(fSuccess, fError);
        };
    },





});
