jQuery.sap.require("view.abstract.AbstractController");
if (!model.persistence) jQuery.sap.require("model.persistence.Storage");
if (!model.Periods) jQuery.sap.require("model.Periods");
if (!model.VacationRequests) jQuery.sap.require("model.VacationRequests");
view.abstract.AbstractController.extend("view.VacationRequestEdit", {
    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.detailRequestModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailRequestModel, "detailRequestModel");
        this.enableModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.enableModel, "enableModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");
        this.enableModel.setProperty("/intoOpenPeriod", true);

        if (!this._checkRoute(evt, "vacationRequestEdit")) {
            return;
        }

        this.oDialog = this.getView().byId("vacationRequestEditPage");
        this.state = evt.getParameters().arguments.state;
        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.codConsulente = this.user.codice_consulente;
        this.requestId = evt.getParameters().arguments.detailId;
        this.enableModel.setProperty("/saveButton", false);
        this.enableModel.setProperty("/cancelButton", false);
        if (this.state !== "modify") {
            this._clearInputValue();
        }
        this.detailRequestModel.setProperty("/note", "");

        this._setMail();
    },

    onCalendarPress: function (evt) {
        this.arrayRichieste = [];
        var firstDay = evt.getSource().getDateValue();
        var lastDay = evt.getSource().getSecondDateValue();
        //** controllo che il primo giorno di ferie non sia inferiore al primo gg del periodo aperto
        var periodSelectFromHoliday = model.persistence.Storage.session.get("selectedPeriodFromHoliday");
        var startDate = new Date(periodSelectFromHoliday.startDate);
        startDate.setHours("00");
        if (periodSelectFromHoliday && startDate > firstDay) {
            this.enableModel.setProperty("/intoOpenPeriod", false);
            sap.m.MessageBox.alert(this._getLocaleText("noIntoPeriod"));
            return;
        } else {
            this.enableModel.setProperty("/intoOpenPeriod", true);
            // le converto in stringa perché altrimenti non funziona il confronto
            var stringFirstDay = firstDay.toString();
            var stringLastDay = lastDay.toString();
            if (stringFirstDay === stringLastDay) {
                this.detailRequestModel.setProperty("/visibleHalfDay", true);
            } else {
                this.detailRequestModel.setProperty("/visibleHalfDay", false);
                this.detailRequestModel.setProperty("/halfDay", false);
                this.detailRequestModel.setProperty("/note", "");
            }
            var differenzaGiorni = this.calcolaDifferenzaGiorni(firstDay, lastDay);
            var today = new Date();
            today.setHours(0);
            today.setMinutes(1);
            this.enableModel.setProperty("/intoOpenPeriod", true);
            for (var i = 0; i <= differenzaGiorni; i++) {
                var date = this.calcolaGiorniSuccessivi(firstDay, i);
                date.setUTCHours(30);
                if (date >= today) {
                    this.calcolaRichiesta(date);
                } else {
                    var selectedDate = utils.Formatter.formatDateFerie(date);
                    this.enableModel.setProperty("/intoOpenPeriod", false);
                    sap.m.MessageBox.alert(selectedDate + ": " + this._getLocaleText("errorSavingDate"), {
                        title: this._getLocaleText("TITLE_CONFIRM_UPLOAD")
                    });
                    break;
                }
            }
        }
    },

    saveNote: function (evt) {
        this.detailRequestModel.setProperty("/note", evt.getSource().getValue());
    },

    calcolaRichiesta: function (date) {
        var codAttivitaRichiesta = "";
        if (this.detailRequestModel.getProperty("/halfDay") === false) {
            codAttivitaRichiesta = "travel-itinerary";
        } else {
            codAttivitaRichiesta = "bed";
        }
        //        var dataInserimento = utils.Formatter.formatDateFerie(date);
        var dataInserimento = utils.Formatter.formatDateFerie(new Date());
        var req = {
            'codAttivitaRichiesta': codAttivitaRichiesta,
            'codConsulente': this.codConsulente,
            'codPjm': "CC00000000",
            'dataInserimento': dataInserimento,
            'dataReqPeriodo': "",
            'giorno': "",
            'idCommessa': "AA00000008",
            'idPeriodoSchedulingRic': "",
            'id_req': undefined,
            'note': this.detailRequestModel.getProperty("/note") ? this.detailRequestModel.getProperty("/note") : "",
            'stato': "PROPOSED"
        };
        var fSuccess = function (result) {
            this.oDialog.setBusy(false);
            var periods = result.items;
            var startPeriodDate;
            var endPeriodDate;
            for (var i = 0; i < periods.length; i++) {
                // setto i seguenti orari per non avere il bug che l'ultimo giorno non viene salvato perché più avanti dell'ultimo giorno del periodo
                var startPeriodDate = new Date(periods[i].startDate.setHours(1));
                var endPeriodDate = new Date(periods[i].endDate.setHours(23));
                if (date >= startPeriodDate && date <= endPeriodDate) {
                    if (date.getDay() !== 0 && date.getDay() !== 6) {
                        /*req.dataReqPeriodo = utils.Formatter.formatDateFerie(date);
                        req.giorno = (this.calcolaDifferenzaGiorni(date, periods[i].startDate) + 1).toString();
                        req.idPeriodoSchedulingRic = periods[i].idPeriod.toString();
                        this.arrayRichieste.push(req);
                        break;*/
                    	var mese = date.getMonth() + 1;
                        if (mese < 10) {
                            mese = "0" + mese;
                        } else {
                            mese = mese.toString();
                        }
                        var festivitaMese = this._calcolaArrayFestivita(date.getFullYear())[mese];
                        var giorno = date.getDate().toString();
                        if (!_.include(festivitaMese, giorno)) {
                            req.dataReqPeriodo = utils.Formatter.formatDateFerie(date);
                            req.giorno = (this.calcolaDifferenzaGiorni(date, periods[i].startDate) + 1).toString();
                            req.idPeriodoSchedulingRic = periods[i].idPeriod.toString();
                            this.arrayRichieste.push(req);
                        }
                        break;
                    }
                }
            }
        };
        var fError = function () {
            this.oDialog.setBusy(false);
            sap.m.MessageBox.alert("Errore in lettura periodi", {
                title: "Attenzione"
            });
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        this.oDialog.setBusy(true);
        new model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    onCancelPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_CANCEL_VACATION"), sap.m.MessageBox.Icon.ALLERT, this._getLocaleText("CONFIRM_CANCEL_TITLE_VACATION"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO], _.bind(this._confirmCancelPress, this));
    },

    _confirmCancelPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            this.onNavBackPress();
        }
    },

    onSavePress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_SAVE_VACATION"), sap.m.MessageBox.Icon.ALLERT, this._getLocaleText("CONFIRM_SAVE_TITLE_VACATION"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO], _.bind(this._confirmSavePress, this));
    },

    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var codAttivitaRichiesta = "";
            if (this.detailRequestModel.getProperty("/halfDay") === false) {
                codAttivitaRichiesta = "travel-itinerary";
            } else {
                codAttivitaRichiesta = "bed";
            }
            for (var i = 0; i < this.arrayRichieste.length; i++) {
                this.arrayRichieste[i].codAttivitaRichiesta = codAttivitaRichiesta;
            }
            if (this.arrayRichieste.length === 1) {
                this.arrayRichieste[0].note = this.detailRequestModel.getProperty("/note");
            }
            if (this.detailRequestModel.getProperty("/note")) {
                for (var t = 0; t < this.arrayRichieste.length; t++) {
                    this.arrayRichieste[t].note = this.detailRequestModel.getProperty("/note");
                }
            }
            this._checkVacation();
        }
    },

    // funzione che controlla la presenza di altre ferie
    _checkVacation: function () {
        var fSuccess = function (res) {
            this.ferieGiaRichieste = res.results;
            var arrayClone = _.clone(this.arrayRichieste);
            for (var i = 0; i < this.ferieGiaRichieste.length; i++) {
                _.remove(arrayClone, {
                    giorno: (this.ferieGiaRichieste[i].gg).toString(),
                    idPeriodoSchedulingRic: (this.ferieGiaRichieste[i].idPeriodoScheduling).toString()
                });
            }
            if (arrayClone.length > 0) {
                this._saveRequest(arrayClone);
            } else {
                sap.m.MessageToast.show("Sono già presenti delle richieste ferie per questo periodo");
                jQuery.sap.delayedCall(1500, this, function () {
                    this.onNavBackPress();
                });
            }
        };
        var fError = function () {
            this.oDialog.setBusy(false);
            sap.m.MessageBox.alert("Lettura richieste ferie fallita", {
                title: "Attenzione"
            });
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        var codConsulente = this.user.codice_consulente;
        var idPeriodo = undefined;
        model.VacationRequests.checkVacation(codConsulente, idPeriodo).then(fSuccess, fError);
    },

    // funzione che inserisce le nuove richieste ferie
    _saveRequest: function (arrayToSave) {
        this.ferie = arrayToSave;
        var fSuccess = function () {
            this.oDialog.setBusy(false);
            // send mail
            this.mailObj.subject = "Richiesta ferie";
            var user = model.persistence.Storage.session.get("user");
            var fullName = user.nomeCognome;
            var msg = "";
            if (_.size(this.ferie) === 1) {
                var data = this.ferie[0].dataReqPeriodo.split("-");
                if (this.ferie[0].codAttivitaRichiesta === "bed") {
                    this.mailObj.subject = this._getLocaleText("richiestaPermesso");
                    this.mailObj.message = this._getLocaleText("richiestaPermesso") + " " + this._getLocaleText("perIlGiorno") + ": " + data[2] + "/" + data[1] + "/" + data[0] + "</br><b>" + this._getLocaleText("noteEmail") + ':</b> "' + arrayToSave[0].note + '"</br>' + this._getLocaleText("rimangoInAttesaDiConferma") + "</br></br>" + fullName;
                } else {
                    this.mailObj.message = this._getLocaleText("richiestaFerieUnGiorno") + ": " + data[2] + "/" + data[1] + "/" + data[0] + "</br><b>" + this._getLocaleText("noteEmail") + ':</b> "' + arrayToSave[0].note + '"</br>' + this._getLocaleText("rimangoInAttesaDiConferma") + "</br></br>" + fullName;
                }
            } else {
                var dataIniziale = this.detailRequestModel.getProperty("/inizioFerie");
                var dataFinale = this.detailRequestModel.getProperty("/fineFerie");
                this.mailObj.message = this._getLocaleText("richiestaFeriePiuGiorni") + " " + this._getLocaleText("da") + dataIniziale.getDate() + "/" + (dataIniziale.getMonth() + 1) + "/" + dataIniziale.getFullYear() + " " + this._getLocaleText("a") + " " + dataFinale.getDate() + "/" + (dataFinale.getMonth() + 1) + "/" + dataFinale.getMonth() + 1 + "</br><b>" + this._getLocaleText("noteEmail") + ':</b> "' + arrayToSave[0].note + '"</br>' + this._getLocaleText("rimangoInAttesaDiConferma") + "</br></br>" + fullName;
            }
//            model.SendMail.richiestaFerie(this.mailObj)
//                .then(function () {
                    this.onNavBackPress();
//                }.bind(this), function (err) {
//                    sap.m.MessageBox.alert("Invio email fallito. Si prega di avvisare hr@icms.it e i PJM di riferimento delle ferie richieste", {
//                        title: "Attenzione",
//                        onClose: function () {
//                            this.onNavBackPress();
//                        }.bind(this)
//                    });
//                }.bind(this));

            //            this.onNavBackPress();
        };
        var fError = function () {
            this.oDialog.setBusy(false);
            sap.m.MessageBox.alert("Salvataggio fallito", {
                title: "Attenzione"
            });
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        this.oDialog.setBusy(true);
        model.VacationRequests.saveRequestVacation(arrayToSave)
            .then(fSuccess, fError);
    },

    calcolaGiorniSuccessivi: function (date, days) {
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        var futureTimeToAdd = oneDay * days;
        var newDateMillis = date.getTime() + futureTimeToAdd;
        var newDate = new Date(newDateMillis);
        return newDate;
    },

    _clearInputValue: function () {
        var periodoFerie = model.persistence.Storage.session.get("selectedPeriodFromHoliday");
        this.detailRequestModel.setProperty("/halfDay", false);
        this.detailRequestModel.setProperty("/inizioFerie", new Date(periodoFerie.startDate));
        this.detailRequestModel.setProperty("/fineFerie", new Date(periodoFerie.startDate));
        this.detailRequestModel.setProperty("/visibleHalfDay", true);
        this.firstDay = new Date(periodoFerie.startDate);
        this.lastDay = new Date(periodoFerie.startDate);
        this.arrayRichieste = [];
        this.calcolaRichiesta(this.firstDay);
    },

    _setMail: function () {
        var user = model.persistence.Storage.session.get("user");
        model.PjmByConsultant.getRecords({
                codConsulente: user.codConsulente
            })
            .then(function (result) {
                var arrayMail = [];
                _.forEach(result, function (o) {
                    var name = (o.nomePjm.replace(/ /g, '')).toLowerCase();
                    var surname = (o.cognomePjm.replace(/ /g, '')).toLowerCase();
                    var mail = name + "." + surname + "@icms.it";
                    arrayMail.push(mail);
                });
                var user = model.persistence.Storage.session.get("user");
                //                var fullName = user.nomeCognome;
                var mittente = user.nomeConsulente.toLowerCase() + "." + user.cognomeConsulente.toLowerCase() + "@icms.it";
                //        var destinatario = "hr@icms.it";
                var destinatario = "fabio.giordano@icms.it";
                //                var destinatariInCopia = arrayMail;
                var destinatariInCopia = [];
                destinatariInCopia.push(mittente);
                //                var messaggio = fullName;   
                this.mailObj = {
                    from: mittente,
                    to: destinatario,
                    cc: destinatariInCopia,
                    subject: "",
                    message: ""
                }
            }.bind(this), function (err) {
                // errore
            }.bind(this));
    }
});
