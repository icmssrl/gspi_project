jQuery.sap.require("view.abstract.AbstractController");

view.abstract.AbstractController.extend("view.Launchpad", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.tileModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.tileModel, "tiles");

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");
    },

    handleRouteMatched: function (evt) {
        if (!this._checkRoute(evt, "launchpad")) {
            return;
        }
        var funx = function () {
            view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);

            var prefTitle = icms.Component.getMetadata().getConfig().settings.prefTitle;
            document.title = prefTitle;

            this.userModel.setData(this.user);
            this.refreshTiles(this.user);
            /*sap.m.MessageBox.show("Il sistema non è raggiungibile nella giornata odierna per degli aggiornamenti", {
            	icon: sap.m.MessageBox.Icon.INFORMATION,
            	title: "GSPI OFFLINE",
            	actions: [sap.m.MessageBox.Action.OK],
            	onClose: _.bind(function(oAction) { this.goToFTR() }, this)
            });*/

            sessionStorage.removeItem("GSPI_log_WAR");

            setInterval(this.loginRefresh(), 60000);

            //tile amministratrici
            var Success = function (result) {
                var notConfirmed = _.filter(result.items, {
                    "confermato": "0"
                });
                var num = notConfirmed.length;
                this.console("N° richieste viaggio: " + num);
                var tileApp = _.find(this.tileModel.getData(), {
                    'title': "Richieste Viaggio"
                });
                tileApp.number = num;
                this.tileModel.refresh(true);
            };

            var Error = function (err) {
                if (err === "undefined") {
                    this.console("errore di connessione", "error");
                } else {
                    this.console(JSON.parse(err.response.body).error.message.value, "error");
                }
            };

            Success = _.bind(Success, this);
            Error = _.bind(Error, this);

            if (this.user.codiceProfilo === "CP00000008") {
                model.TravelReq.getTravelReq()
                    .then(Success, Error);
            }
        };
        funx = _.bind(funx, this);

//        var userFromFTR = evt.getParameters().arguments.aliasLogged;
        var userFromFTR = sessionStorage.getItem("GSPI_Alias_Logged");
        if (userFromFTR) {
            sessionStorage.setItem("fromFTR", true);

            this.uiModel.setProperty("/logoutButtonVisible", false);
            this.uiModel.setProperty("/backToFtrButtonVisible", true)
            this.user = new model.User();
            var fError = function () {
                this.oDialog.setBusy(false);
                sap.m.MessageToast.show(
                    this._getLocaleText("USER_NOT_FOUND"), {
                        duration: 2000
                    }
                );
                this.router.navTo("");
            };

            var fSuccess = function (consulenteLogato) {
                if (consulenteLogato === "credenzialiErrate") {
                    sap.m.MessageBox.show("Utente non abilitato", {
                        icon: sap.m.MessageBox.Icon.INFORMATION,
                        title: "NON AUTORIZZATO",
                        actions: [sap.m.MessageBox.Action.OK],
                        onClose: _.bind(function(oAction) { this.goToFTR() }, this)
                    });
                    return;
                }
                var consulente = JSON.stringify(consulenteLogato);
                this.user.setCredential(consulenteLogato.usernameConsulente, undefined, consulenteLogato.nomeConsulente, consulenteLogato.cognomeConsulente);
                this.user.doLogin()
                    .then(_.bind(function (result) {
                        sessionStorage.setItem("isLoggedGSPI", true);
                        sessionStorage.setItem("GSPI_User_Logged", consulente);
                        _.bind(funx(), this);
                    }, this));
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            model.Login.doLoginFromFTR(userFromFTR, fSuccess, fError)
                .then(fSuccess, fError);
        } else {
            if (!sessionStorage.getItem("fromFTR")) {
                this.uiModel.setProperty("/logoutButtonVisible", true);
                this.uiModel.setProperty("/backToFtrButtonVisible", false);
            } else {
                this.uiModel.setProperty("/logoutButtonVisible", false);
                this.uiModel.setProperty("/backToFtrButtonVisible", true)
            }
            funx();
        }
    },

    onTilePress: function (oEvent) {
        var tilePressed = oEvent.getSource().getBindingContext("tiles").getObject();
        var url = tilePressed.url;
//        if (tilePressed.title === model.i18n._getLocaleText("WAR")) {
//            var url = this.verificaIndirizzamento();
//            sap.m.URLHelper.redirect(url, false);
//            sessionStorage.setItem("GSPI_log_WAR", true);
//        } else if (tilePressed.title === model.i18n._getLocaleText("schedulingApprove")) {
//            var url = this.verificaIndirizzamento();
//            sap.m.URLHelper.redirect(url, false);
//            sessionStorage.setItem("GSPI_log_SchedApp", true);
//        } else {
            this.router.navTo(url);
//        }
    },

    verificaIndirizzamento: function () {
        var pathName = location.pathname;
        var host = location.host;
        var url = "";
        if (pathName.indexOf("C:") >= 0) {
            url = "file:///C:/Users/fgiordano/Documents/ftr_2.0/WebContent/index.html";
            if (pathName.indexOf("ssecuro") >= 0) {
                url = url.replace("fgiordano", "ssecuro");
            }
        } else if (host.indexOf("gspidev.icms.it:8000") >= 0) {
            url = "http://gspidev.icms.it:8000/GSPI/app/GSPIFTR/FTR/";
        } else if (host.indexOf("wardev.icms.it") >= 0) {
            url = "http://wardev.icms.it/GSPI/app/GSPIFTR/FTR/";
        } else if (host.indexOf("war.icms") >= 0) {
            url = "http://war.icms.it/fiori/";
        }
        return url;
    },

    refreshTiles: function (user) {
        var tile = model.Tiles.getMenu(user.descrizioneProfilo, user.tipoAzienda);
        this.tileModel.setData(tile);
        this.tileModel.refresh();
        this.getView().rerender();
    },

    loginRefresh: function () {
        model.odata.chiamateOdata.chiamataForLoginToHanaSimple();
    },

    goToFTR: function () {
//        var pathName = location.pathname;
//        var host = location.host;
//        var url = "";
//        if (host.indexOf("wardev.icms.it") >= 0) {
//            url = "http://wardev.icms.it/fiori/fiori3.0/#/launchpad";
//        } else if (host.indexOf("war.icms") >= 0) {
//            url = "http://war.icms.it/fioriPortal/#/launchpad";
//        }
//        sap.m.URLHelper.redirect(url, false);
    	
    	var location = window.location.origin;
    	if (location.indexOf("dev") >= 0) {
    		url = location + "/fiori/fiori3.0/#/launchpad";
    	} else {
    		url = location + "/fioriPortal/#/launchpad";
    	}
    	sap.m.URLHelper.redirect(url, false);
    },

});
