jQuery.sap.require("view.abstract.AbstractController");

view.abstract.AbstractController.extend("view.BusinessUnitDetail", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        
        this.detailBusinessUnit = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailBusinessUnit, "detailBusinessUnit");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        
        var name = evt.getParameter("name");

        if (name !== "businessUnitDetail") {
            return;
        };
        this.oDialog = this.getView().byId("idBusinessUnitDetail");
        
        var codBu = evt.getParameters().arguments.detailId;

        this._getSingleRecordBu(codBu);
    },

    _getSingleRecordBu: function (codBu) {
        var fSuccess = function (result) {
            this.detailBusinessUnit.setProperty("/codiceBu", result.items[0].codiceBu);
            this.detailBusinessUnit.setProperty("/nomeBu", result.items[0].nomeBu);
            this.detailBusinessUnit.setProperty("/descrizioneBu", result.items[0].descrizioneBu);
            this.detailBusinessUnit.setProperty("/statoBu", result.items[0].statoBu);
        };
        
        var fError = function () {
            sap.m.MessageToast("errore lettura oData BusineUnit");
            console.log("errore in lettura business unit");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.BusinessUnit.readBu(codBu)
            .then(fSuccess, fError);
    },
    
    modifyBuPress: function () {
        this.router.navTo("businessUnitModify", {
            state: "modify",
            detailId: this.detailBusinessUnit.getProperty("/codiceBu")
        });
    },

    deleteBuPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("confirmDeleteBu"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("confirmDeleteTitleBu"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmDeletePress, this)
        );
    },

    _confirmDeletePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            var fSuccess = function () {
                this.oDialog.setBusy(false);
                this.router.navTo("businessUnitList");
            };
            var fError = function () {
                this.oDialog.setBusy(false);
                console.log("errore nel salvataggio");
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            model.BusinessUnit.deleteBu(this.detailBusinessUnit.getProperty("/codiceBu"))
                .then(fSuccess, fError);
        };
    },

});
