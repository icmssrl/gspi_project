jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("model.User");

view.abstract.AbstractController.extend("view.Login", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.credentialModel = new sap.ui.model.json.JSONModel();
    },

    handleRouteMatched: function (evt) {
        var name = evt.getParameter("name");
        if (name !== "login") {
            return;
        }
        var prefTitle = icms.Component.getMetadata().getConfig().settings.prefTitle;
        document.title = prefTitle;
        
        this.oDialog = this.getView().byId("idLoginForm");

        if (this._checkLogged()) {
            $(document).off("keypress");
//            sessionStorage.setItem("history", history.length);
            this.router.navTo("launchpad");
        } else {
            $(document).on("keypress", _.bind(function (e) {
        		if (e.keyCode == 13 && !_.isEmpty(sap.ui.getCore().byId("loginId--login_usr")._getInputValue()) &&
                        !_.isEmpty(sap.ui.getCore().byId("loginId--login_pwd")._getInputValue())) {
                        this.onLoginPress();
                    }
            }, this));
        }

        this.user = new model.User();
        this.getView().setModel(this.credentialModel, "usr");

        model.i18n.setModel(this.getView().getModel("i18n"));
    },

//    onLoginPress: function (evt) {
//        sessionStorage.clear();
//        this.console("login pressed", "confirm");
//        var username = sap.ui.getCore().byId("loginId--login_usr")._getInputValue();
//        var psw = sap.ui.getCore().byId("loginId--login_pwd")._getInputValue();
//        if (_.isEmpty(username) || _.isEmpty(psw)) {
//            sap.m.MessageToast.show(
//                this._getLocaleText("MESSAGE_USER_PASSWORD_EMPTY"), {
//                    duration: 4000
//                }
//            );
//            return;
//        }
//
//        this.oDialog.setBusy(true);
//        model.odata.chiamateOdata.chiamataForSap()
//            .then(function () {
//                model.odata.chiamateOdata.chiamataForLoginToHana()
//                    .then(function () {
//                        this._odataCallCidSet((username).toUpperCase(), btoa(psw));
//                    }.bind(this), function (err) {
//                        this.oDialog.setBusy(false);
//                        console.log(err);
//                    }.bind(this));
//            }.bind(this), function (err) {
//                this.oDialog.setBusy(false);
//                console.log(err);
//            }.bind(this));
//    },
//
//    _odataCallCidSet: function (user, password) {
//        var fError = function (err) {
//            this.oDialog.setBusy(false);
//            sap.m.MessageToast.show(
//                this._getLocaleText("usernotfound"), {
//                    duration: 2000
//            });
//        };
//
//        var fSuccessCidSet = function (ris) {
//            if (ris.dati.length === 0) {
//                this.getView().getModel("appModel").setProperty("/username", "");
//                this.getView().getModel("appModel").setProperty("/password", "");
//                this.oDialog.setBusy(false);
//                sap.m.MessageToast.show(
//                    this._getLocaleText("usernotfound"), {
//                        duration: 2000
//                });
//                return;
//            } else {
//                console.log("odata CID OK");
//                var cidSet = undefined;
//                var cidSetOdata = ris.dati;
//                sessionStorage.setItem("cid", cidSetOdata[0].userID);
//                //                sessionStorage.setItem("isLogged", true);
//                if (!utils.LocalStorage.setItem("cidSet", cidSetOdata)) {
//                    fError(this._getLocaleText("cidNonCaricato"));
//                } else {
//                    this._doLoginHana(cidSetOdata[0].userID);
//                }
//                var usersLS = localStorage.getItem("user");
//                if (!usersLS) {
//                    usersLS = [];
//                } else {
//                    usersLS = JSON.parse(atob(usersLS));
//                    for (var d = 0; d < usersLS.length; d++) {
//                        if (usersLS[d].u === sap.ui.getCore().byId("loginId--login_usr")._getInputValue() && usersLS[d].id === utils.LocalStorage.getItem("cidSet")[0].userID) {
//                            usersLS.splice(d, 1);
//                            break;
//                        }
//                    }
//                }
//                var localUser = {};
//                localUser.u = sap.ui.getCore().byId("loginId--login_usr")._getInputValue();
//                localUser.p = sap.ui.getCore().byId("loginId--login_pwd")._getInputValue();
//                localUser.id = utils.LocalStorage.getItem("cidSet")[0].userID;
//                usersLS.push(localUser);
//                this.saveCredentials(usersLS);
//                sessionStorage.setItem("loginSapFromGspi", btoa(JSON.stringify(localUser)));
//            }
//        };
//        fSuccessCidSet = _.bind(fSuccessCidSet, this);
//        fError = _.bind(fError, this);
//
//        model.cidOdata.getCid(user, password)
//            .then(fSuccessCidSet, fError);
//    },
//
//    _doLoginHana: function (cid) {
//        var fError = function () {
//            this.oDialog.setBusy(false);
//            sap.m.MessageToast.show(
//                this._getLocaleText("USER_NOT_FOUND"), {
//                    duration: 4000
//                }
//            );
//        };
//
//        var fSuccess = function (consulenteLogato) {
//            var consulente = JSON.stringify(consulenteLogato);
//            this.user.setCredential(consulenteLogato.usernameConsulente, consulenteLogato.passwordConsulente, consulenteLogato.nomeConsulente, consulenteLogato.cognomeConsulente);
//            this.user.doLogin()
//                .then(_.bind(function (result) {
//                    sessionStorage.setItem("isLoggedGSPI", true);
//                    sessionStorage.setItem("GSPI_User_Logged", consulente);
//                    sessionStorage.setItem("history", history.length);
//                    this.router.navTo("launchpad");
//                    this.oDialog.setBusy(false);
//                }, this));
//        };
//
//        fSuccess = _.bind(fSuccess, this);
//        fError = _.bind(fError, this);
//
//        model.Login.doLoginSAP(cid, fSuccess, fError)
//            .then(fSuccess, fError);
//    },
//
//    saveCredentials: function (user) {
//        localStorage.setItem("user", btoa(JSON.stringify(user)));
//    },

    _checkLogged: function () {
        return model.persistence.Storage.session.get("GSPI_User_Logged");
    },
    
    onLoginPress: function (evt) {
        sessionStorage.clear();
        this.console("login pressed", "confirm");

        model.odata.chiamateOdata.chiamataForLoginToHanaSimple();
        var username = sap.ui.getCore().byId("loginId--login_usr")._getInputValue();
        var psw = sap.ui.getCore().byId("loginId--login_pwd")._getInputValue();
        if (_.isEmpty(username) || _.isEmpty(psw)) {
            sap.m.MessageToast.show(
                this._getLocaleText("MESSAGE_USER_PASSWORD_EMPTY"), {
                    duration: 4000
                }
            );
            return;
        };
        this._doLogin((username).toUpperCase(), btoa(psw));
    },

//     LOGIN HANA 
    _doLogin: function (user, password) {
        this.oDialog.setBusy(true);
        var fError = function () {
            this.oDialog.setBusy(false);
            sap.m.MessageToast.show(
                this._getLocaleText("USER_NOT_FOUND"), {
                    duration: 4000
                }
            );
        };

        var fSuccess = function (consulenteLogato) {
            var consulente = JSON.stringify(consulenteLogato)
            if (consulenteLogato.statoConsulente === "BLOCCATO") {
                this.oDialog.setBusy(false);
                sap.m.MessageToast.show(
                    this._getLocaleText("USER_STUCK"), {
                        duration: 4000
                    }
                );
                return;
            } else if (consulenteLogato.statoConsulente === "INATTIVO") {
                this.oDialog.setBusy(false);
                sap.m.MessageToast.show(
                    this._getLocaleText("USER_INACTIVE"), {
                        duration: 4000
                    }
                );
                return;
            } else {
                this.user.setCredential(consulenteLogato.usernameConsulente, consulenteLogato.passwordConsulente, consulenteLogato.nomeConsulente,consulenteLogato.cognomeConsulente );
                this.user.doLogin()
                    .then(_.bind(function (result) {
                        sessionStorage.setItem("isLoggedGSPI", true);
                        sessionStorage.setItem("GSPI_User_Logged", consulente);
                        this.oDialog.setBusy(false);
                        $(document).off("keypress");
                        sessionStorage.setItem("history", history.length);
                        this.router.navTo("launchpad");
                    }, this));
            }
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Login.doLogin(user, password, fSuccess, fError)
            .then(fSuccess, fError);
    },
});