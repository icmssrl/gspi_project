jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.Consultants");
jQuery.sap.require("model.BusinessUnit");
jQuery.sap.require("model.BuList");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("sap.m.MessageToast");

view.abstract.AbstractMasterController.extend("view.ConsultantList", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.modelConsulenti = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelConsulenti, "modelConsulenti");

        this.tempModelData = new sap.ui.model.json.JSONModel();

        this.buModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.buModel, "buModel");

        this.modelFragmentConsultant = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelFragmentConsultant, "modelFragmentConsultant");

        this.uiModel.setProperty("/searchProperty", ["cognomeConsulente", "nomeConsulente", "descrizioneProfiloConsulente", "codConsulente"]);

        },

    handleRouteMatched: function (evt) {
        var name = evt.getParameter("name");
        this.name = name;

        if (name !== "consultantList") {
            return;
        }
        view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
        this.getView().byId("list").removeSelections();
        this.oDialog = this.getView().byId("list");

        if (model.persistence.Storage.session.get("selectedConsultant")) {
            model.persistence.Storage.session.remove("selectedConsultant");
        }

        var listConsultants = this.getView().byId("list");
        var binding = listConsultants.getBinding("items");
        binding.filter();

        this.readConsultants();
        
        this.uiModel.setProperty("/addConsultantEnable",true);
    },

    readConsultants: function () {
        var fSuccess = function (result) {
            _.remove(result.items, {codProfiloConsulente: 'CP10000001'});
            this.modelConsulenti.setProperty("/consulenti", result.items);
            this.console("consultants", "success");
            this.console(result);
            if (this.oneTime) {
                this.oneTime = undefined;
            }
            this.oDialog.setBusy(false);
        };
        var fError = function (err) {
            this.oDialog.setBusy(false);
            this.console(JSON.parse(err.response.body).error.message.value, "error");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.Consultants.readConsultants(undefined, this)
            .then(fSuccess, fError);
    },

    onLaunchpadPress: function () {
        if (model.persistence.Storage.session.get("selectedConsultant")) {
            model.persistence.Storage.session.remove("selectedConsultant");
        }
//        if (this.name === "consultantList") {
//            this.onNavBackPress();
//        } else {
//            this.router.navTo("launchpad");
//        }
        this.router.navTo("launchpad");
    },

    onFilterPress: function () {
        if (this.name === "consultantDetail" || this.name === "consultantModify") {
            this.router.navTo("consultantList");
        }
        if (sap.ui.getCore().getElementById("idSegmentedButton")) {
            sap.ui.getCore().getElementById("idSegmentedButton").setSelectedButton("statusButton");
        }
        this.modelFragmentConsultant.setProperty("/status", true);
        this.modelFragmentConsultant.setProperty("/bu", false);

        var stato = [{
            "value": "Aperto"
        }, {
            "value": "Chiuso"
        }];
        this.tempModelData.setData(stato);
        this.getView().setModel(this.tempModelData, "filter");

        var page = this.getView().byId("listaConsulenti");
        if (!this.consultantDialog) {
            this.consultantDialog = sap.ui.xmlfragment("view.dialog.filterDialogConsultants", this);
            page.addDependent(this.consultantDialog);
        }

        var fSuccess = function (result) {
            this.buModel.setData(result);
            this.consultantDialog.open();
        };

        var fError = function (err) {
            sap.m.MessageToast("error BU");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var statoBU = "APERTO";
        model.BuList.getBuListByStatus(statoBU)
            .then(fSuccess, fError);
    },

    getSelectButton: function (evt) {
        var buttonPressed = evt.getSource().getSelectedButton();
        if (buttonPressed === "statusButton") {
            this.modelFragmentConsultant.setProperty("/status", true);
            this.modelFragmentConsultant.setProperty("/bu", false);
        } else if (buttonPressed === "businessUnitButton") {
            this.modelFragmentConsultant.setProperty("/status", false);
            this.modelFragmentConsultant.setProperty("/bu", true);
        }
    },

    onFilterDialogClose: function () {
        this.consultantDialog.close();
        //this.consultantDialog.destroy(true);
    },

    onFilterDialogOK: function (evt) {
        this.consultantDialog.close();

        var filtersArr = [];
        var filtroBu = sap.ui.getCore().getElementById("idListBuConsultants").getSelectedItems();

        for (var i = 0; i < filtroBu.length; i++) {
            filtersArr.push(new sap.ui.model.Filter('descBu', sap.ui.model.FilterOperator.Contains, filtroBu[i].getProperty("title")));
        }
        var filtroOK = new sap.ui.model.Filter({
            filters: filtersArr,
            and: false
        });
        if (this.arrayFiltri) {
            var filtroStato = new sap.ui.model.Filter('stato', sap.ui.model.FilterOperator.Contains, this.arrayFiltri[0]);
            filtersArr.push(filtroStato)
            var filtroOK = new sap.ui.model.Filter({
                filters: filtersArr,
                and: true
            });
        }

        var list = this.getView().byId("list");
        var binding = list.getBinding("items");
        if (filtroOK && filtroOK.aFilters.length > 0) {
            binding.filter(filtroOK);
        } else {
            binding.filter();
        }
        this.console("filtri selezionati", "confirm");
        this.console(filtersArr);
        //this.consultantDialog.destroy(true);
    },

    onFilterDialogRipristino: function () {
        this.consultantDialog.close();
        sap.ui.getCore().getElementById("listaFiltroStato").removeSelections();
        this.arrayFiltri = undefined;
        sap.ui.getCore().getElementById("idListBuConsultants").removeSelections();
        var listConsultants = this.getView().byId("list");
        var binding = listConsultants.getBinding("items");
        binding.filter();
        //this.consultantDialog.destroy(true);
    },

    filterPress: function (evt) {
        var numeroFiltri = evt.getSource().getSelectedItems();
        var arrayFiltri = [];
        for (var i = 0; i < numeroFiltri.length; i++) {
            arrayFiltri.push((numeroFiltri[i].getProperty("title").toUpperCase()));
        }
        this.arrayFiltri = arrayFiltri;
    },

    addConsultantPress: function () {
        
        this.uiModel.setProperty("/addConsultantEnable",false);
        this.router.navTo("consultantModify", {
            state: "new"
        });
        if (sessionStorage.getItem("selectedConsultant")) {
            sessionStorage.removeItem("selectedConsultant");
        }
    },

    onConsultantSelectBis: function (evt) {
        var src = evt.getSource();
        if (src.getSelectedItems()[0]) {
            var selectedItem = src.getSelectedItems()[0].getBindingContext("modelConsulenti").getObject();
            sessionStorage.setItem("selectedConsultant", JSON.stringify(selectedItem));
            this.router.navTo("consultantDetail", {
                detailId: selectedItem.codConsulente
            });
        }
    },

    //    addConsultantPress: function () {
    //
    //
    //        var fSuccess = function (success) {
    //            this.consultantListModel.setData(success);
    //            //devo settare i dati dalla chiamata
    //            if (!this.consultantListDialog) {
    //                this.consultantListDialog = sap.ui.xmlfragment("view.dialog.addConsultantFromSap", this);
    //                this.getView().addDependent(this.consultantListDialog);
    //            }
    //            this.consultantListDialog.open();
    //        }
    //        var fError = function (err) {
    //            sap.m.MessageToast.show(err);
    //        }
    //
    //        fSuccess = _.bind(fSuccess, this);
    //        fError = _.bind(fError, this);
    //
    //        model.cidOdata.getConsultantListFromSap().then(fSuccess, fError);
    //    },


});