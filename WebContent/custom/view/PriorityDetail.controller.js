jQuery.sap.require("utils.Formatter");
//jQuery.sap.require("model.Periods");
//jQuery.sap.require("model.PeriodsGabio");
//jQuery.sap.require("model.Priority");
jQuery.sap.require("model.Priorities");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");

view.abstract.AbstractController.extend("view.PriorityDetail", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.detailPrioritaCommesse = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailPrioritaCommesse, "detailPrioritaCommesse");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "priorityDetail") {
            return;
        };
        this.oDialog = this.getView().byId("BusyDialog");

        var prioritaCommessa = JSON.parse(sessionStorage.getItem("selectedPriority"));

        this.detailPrioritaCommesse.setProperty("/codPrioritaCommessa", prioritaCommessa.codPrioritaCommessa);
        this.detailPrioritaCommesse.setProperty("/descrPriorita", prioritaCommessa.descrPriorita);
    },

    modifyPriorityPress: function () {
        this.router.navTo("priorityModify", {
            state: "modify",
            detailId: this.detailPrioritaCommesse.getProperty("/codPrioritaCommessa")
        });
    },

    deletePriorityPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("confirmDeletePriority"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("confirmDeleteTitlePriority"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmDeletePress, this)
        );
    },

    _confirmDeletePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            var fSuccess = function () {
                this.oDialog.setBusy(false);
                this.router.navTo("priorityList");
            };
            var fError = function () {
                this.oDialog.setBusy(false);
                console.log("errore nel salvataggio");
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            model.Priorities.deletePriority(this.detailPrioritaCommesse.getProperty("/codPrioritaCommessa"))
                .then(fSuccess, fError);
        };
    },

});
