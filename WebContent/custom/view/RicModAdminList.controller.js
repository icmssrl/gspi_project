jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.PeriodsModifyRequest");
jQuery.sap.require("sap.m.MessageToast");

view.abstract.AbstractMasterController.extend("view.RicModAdminList", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.modelRichiesteModifiche = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelRichiesteModifiche, "modelRichiesteModifiche");

        this.tempModelData = new sap.ui.model.json.JSONModel();

        //        this.uiModel.setProperty("/searchProperty", ["cognomeConsulente"]);
    },

    handleRouteMatched: function (evt) {
        var name = evt.getParameter("name");
        this.name = name;

        if (name !== "ricModAdminList") {
            return;
        }
        view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
        this.getView().byId("list").removeSelections();
        this.oDialog = this.getView().byId("list");

        if (model.persistence.Storage.session.get("selectedModifyRequest")) {
            model.persistence.Storage.session.remove("selectedModifyRequest");
        }
        if (model.persistence.Storage.session.get("selectedRequestApproveScambio")) {
            model.persistence.Storage.session.remove("selectedRequestApproveScambio");
        }

        var listModifyRequest = this.getView().byId("list");
        var binding = listModifyRequest.getBinding("items");
        binding.filter();

        this.readModifyRequests("PROPOSED");
    },

    readModifyRequests: function (stato) {
        var fSuccess = function (result) {
            this.modelRichiesteModifiche.setData(result.items);
            this.console("Richieste Modifiche", "success");
            this.console(result.items);
            this.oDialog.setBusy(false);
        };
        var fError = function (err) {
            this.oDialog.setBusy(false);
            if (err === "undefined") {
                this.console("errore di connessione", "error");
            } else {
                this.console(JSON.parse(err.response.body).error.message.value, "error");
            }
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        var statoRichiesta = stato;
        model.PeriodsModifyRequest.getRequestList(statoRichiesta, this)
            .then(fSuccess, fError);
    },

    onLaunchpadPress: function () {
        this.router.navTo("launchpad");
        if (model.persistence.Storage.session.get("selectedModifyRequest")) {
            model.persistence.Storage.session.remove("selectedModifyRequest");
        }
        if (model.persistence.Storage.session.get("selectedRequestApproveScambio")) {
            model.persistence.Storage.session.remove("selectedRequestApproveScambio");
        }
    },

    onRequestSelect: function (evt) {
        var src = evt.getSource();
        var selectedItem = src.getSelectedItems()[0].getBindingContext("modelRichiesteModifiche").getObject();
        sessionStorage.setItem("selectedRequestApprove", JSON.stringify(selectedItem));
        if (selectedItem.idScambio !== "") {
            var consulenteScambio = _.find(this.modelRichiesteModifiche.getData(), {
                'idReqModifica': selectedItem.idScambio
            });
            sessionStorage.setItem("selectedRequestApproveScambio", JSON.stringify(consulenteScambio));
        } else {
            if (model.persistence.Storage.session.get("selectedRequestApproveScambio")) {
                model.persistence.Storage.session.remove("selectedRequestApproveScambio");
            }
        }
        this.router.navTo("ricModAdminDetail", {
            detailId: selectedItem.idReqModifica
        });
    },

    onFilterPress: function () {
        if (this.name === "ricModAdminDetail" || this.name === "ricModAdminModify") {
            this.router.navTo("ricModAdminList");
        }

        var stato = [{
            "value": "PROPOSED"
        }, {
            "value": "CONFIRMED"
        }, {
            "value": "DELETED"
        }];
        this.tempModelData.setData(stato);

        this.getView().setModel(this.tempModelData, "filter");

        var page = this.getView().byId("RichiesteModifica");
        //        if (!this.filterApproveRequestDialog) {
        this.filterApproveRequestDialog = sap.ui.xmlfragment("view.dialog.filterApproveRequestDialog", this);
        //        }
        page.addDependent(this.filterApproveRequestDialog);
        this.filterApproveRequestDialog.open();
    },

    onFilterDialogClose: function () {
        this.filterApproveRequestDialog.close();
        this.filterApproveRequestDialog.destroy(true);
    },

    onFilterDialogOK: function () {

        var filtroStato = sap.ui.getCore().getElementById("listaFiltroStato").getSelectedItems();
        filtroStato = filtroStato[0].getProperty("title");
        switch (filtroStato) {
            case "Proposta":
                filtroStato = "PROPOSED";
                break;
            case "Confermata":
                filtroStato = "CONFIRMED";
                break;
            case "Respinta":
                filtroStato = "DELETED";
                break;
        }
        this.filterApproveRequestDialog.close();
        this.filterApproveRequestDialog.destroy(true);
        this.readModifyRequests(filtroStato);
    },
    //
    //    onFilterDialogRipristino: function () {
    //        this.consultantDialog.close();
    //        sap.ui.getCore().getElementById("listaFiltroStato").removeSelections();
    //        this.arrayFiltri = undefined;
    //        sap.ui.getCore().getElementById("idListBuConsultants").removeSelections();
    //        var listConsultants = this.getView().byId("list");
    //        var binding = listConsultants.getBinding("items");
    //        binding.filter();
    //    },
    //
    //    filterPress: function (evt) {
    //        var numeroFiltri = evt.getSource().getSelectedItems();
    //        var arrayFiltri = [];
    //        for (var i = 0; i < numeroFiltri.length; i++) {
    //            arrayFiltri.push((numeroFiltri[i].getProperty("title").toUpperCase()));
    //        }
    //        this.arrayFiltri = arrayFiltri;
    //        //        this.console(arrayFiltri);
    //    },
    //
    //    addConsultantPress: function () {
    //        this.router.navTo("userLoginModify", {
    //            state: "new"
    //        });
    //        if (sessionStorage.getItem("selectedConsultantLogin")) {
    //            sessionStorage.removeItem("selectedConsultantLogin");
    //        };
    //    },
    //    
    //    onConsultantSelectBis: function (evt) {
    //        var src = evt.getSource();
    //        var selectedItem = src.getSelectedItems()[0].getBindingContext("modelConsulentiLogin").getObject();
    //        sessionStorage.setItem("selectedConsultantLogin", JSON.stringify(selectedItem));
    //        this.router.navTo("userLoginDetail", {
    //            alias: selectedItem.alias
    //        });
    //    },

});