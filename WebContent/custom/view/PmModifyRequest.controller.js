jQuery.sap.require("view.abstract.AbstractController");
if (!sap.ui.unified.MenuItem)
    jQuery.sap.require("sap.ui.unified.MenuItem");

view.abstract.AbstractController.extend("view.PmModifyRequest", {
    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.pmSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingModel, "pmSchedulingModel");

        this.pmSchedulingConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingConsultantModel, "pmSchedulingConsultantModel");

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");

        this.noteModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteModel, "noteModel");

        this.modelloIcone = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelloIcone, "iconModel");

        this.addConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.addConsultantModel, "cModel");
        this.addConsultantModel.setSizeLimit(1000);

        this.genericConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.genericConsultantModel, "gModel");

        this.buModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.buModel, "buModel");

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        this.insertMultiModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.insertMultiModel, "insertMultiModel");

        this.multiModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.multiModel, "multiModel");
        this.multiModel.setProperty("/visibleSceltaCancella", false);

        this.iconJsonModel = {
            'full': "sap-icon://complete",
            'half': "sap-icon://time-entry-request",
            'wCons': "sap-icon://customer-and-contacts",
            'wNonCons': "sap-icon://employee-rejections",
            'wCust': "sap-icon://collaborate",
            'wNonCust': "sap-icon://offsite-work",
            'notConfirmed': "sap-icon://role",
            'abroad': "sap-icon://globe",
            'del': "sap-icon://question-mark",
            'halfText': "1/2",
            'travel': "sap-icon://travel-itinerary",
            'bed': "sap-icon://bed"
        };

        this.modelloIcone.setProperty("/icon", this.iconJsonModel.del);

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");

        this.pModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pModel, "pModel");

        var eventBus = sap.ui.getCore().getEventBus();
        eventBus.subscribe("colorCells", "fireColorTableCells", this.colorCells, this);
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);

        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.userModel.setData(this.user);

        if (!this._checkRoute(evt, "pmModifyRequest")) {
            return;
        }

        var prefTitle = icms.Component.getMetadata().getConfig().settings.prefTitle;
        document.title = prefTitle + " _ PJM SCHED";

        this.oDialog = this.getView().byId("requestModifySchedulingBox");
        this.oDialogTable = this.getView().byId("idSchedulazioniModifyRequestTable");
        this.oDialogTable.setVisible(false);
        this.selectCommessa = this.getView().byId("idSelectCommessa");
        this.selectCommessa.setEnabled(false);
        this.selectCommessa.setSelectedKey("");
        this.selectCommessa.setValue("");

        this.visibleModel.setProperty("/enabledButton", false);
        this.visibleModel.setProperty("/undoButton", false);
        this.visibleModel.setProperty("/chargeButton", false);
        this.visibleModel.setProperty("/filterButton", false);
        this.visibleModel.setProperty("/closeProjectButton", false);
        this.visibleModel.setProperty("/downloadExcel", false);
        this.visibleModel.setProperty("/addConsultantButton", false);

        this.getView().byId("idFiltroGiornateZeroPjmRichiesteModifica").setText(this._getLocaleText("NascondiConsulentiZeroGiornate"));
        this.getView().byId("idFiltroGiornateZeroPjmRichiesteModifica").setProperty("pressed", false);

        // pulisco eventuali selezioni sul filtro dei consulenti
        var lista = sap.ui.getCore().getElementById("idListaConsulentiByPJMModify");
        if (lista) {
            lista.removeSelections();
        }
        if (this.listaSelezioniFiltro) {
            this.listaSelezioniFiltro = undefined;
        }
        // fine pulizia filtri

        this.pmSchedulingModel.setProperty("/giorniTotali", 0);

        this.allConsultantsForProject = undefined;

        if (model.persistence.Storage.local.get("copy")) {
            model.persistence.Storage.local.remove("copy");
        }
        if (model.persistence.Storage.local.get("copyBis")) {
            model.persistence.Storage.local.remove("copyBis");
        }
        this.paste = false;
        this.fromSaveLocal = true;

        this.oDialog.setBusy(true);
        this.readPeriods();
    },

    // funzione per la navigazione alla lauchpad
    onLaunchpadPress: function () {
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.onNavBackPress();
    },

    // Funzione caricamento periodi
    readPeriods: function () {
        var fSuccess = function (result) {
            this.visibleModel.setProperty("/closeProjectButton", false);
            var res = {};
            _.remove(result.items, {
                stato: 'CHIUSO'
            });
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for (var i = 0; i < 13; i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.console("Periods: " + res13.items.length, "success");
            //            this.console(res13.items);
            this.periodModel.setData(res13);
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");
            var req = {};
            req.codPjm = this.user.codice_consulente;
            req.stato = "APERTO";
            this.readProjects(req);
        };
        var fError = function () {
            if (!this.oneTimeReadPeriods) {
                // funzione che ripete la chiamata di login nel caso in cui sia scaduta l'autenticazione al sistema
                this.oneTimeReadPeriods = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(this.readPeriods(), this));
            } else {
                this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'getPeriods'");
                this.oDialog.setBusy(false);
            }
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    // Funzione caricamento commesse
    readProjects: function (req) {
        console.time('readProjects'); // funzione per verificare il tempo che impiega la funzione ->START
        var fSuccess = function (result) {
            this.allCommesse = result.items;
            result.items = _.uniq(result.items, 'codCommessa');
            this.pmSchedulingModel.setProperty("/commesse", []);
            this.pmSchedulingModel.setProperty("/commesse", result.items);
            this.console("Projects: " + result.items.length, "success");
            this.console(result.items);
            var comboCommessa = this.selectCommessa;
            if (comboCommessa) {
                comboCommessa.setValue(comboCommessa.getDefaultSelectedItem());
            }
            this.oDialog.setBusy(false);
            console.timeEnd('readProjects'); // funzione per verificare il tempo che impiega la funzione ->END
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'projectsViewWithoutPriority'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Projects.readProjectsPerPJM(req)
            .then(fSuccess, fError);
    },

    // Funzione che carica le commsse, per estrarre le priorità della stessa per ogni periodo
    readProfiloCommessaPeriodo: function (period) {
        console.time('readProfiloCommessaPeriodo'); // funzione per verificare il tempo che impiega la funzione ->START
        var fSuccess = function (result) {
            this.profiloCommessaPeriodo = [];
            var commesse = this.pmSchedulingModel.getProperty("/commesse");
            for (var i = 0; i < commesse.length; i++) {
                this.profiloCommessaPeriodo.push(_.find(result.items, {
                    codCommessaPeriodo: commesse[i].codCommessa
                }));
            }
            this.console("ProfiloCommessaPeriodo: " + this.profiloCommessaPeriodo.length, "success");
            this.console(this.profiloCommessaPeriodo);
            console.timeEnd('readProfiloCommessaPeriodo'); // funzione per verificare il tempo che impiega la funzione ->END
            this.readCommessePerPjmSet();
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'profiloCommessaPeriodoSet'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.ProfiloCommessaPeriodo.readRecordByPeriod(period)
            .then(fSuccess, fError);
    },

    // Funzione caricamento di tutte le commesse di tutti i consulenti del PJM
    readCommessePerPjmSet: function () {
        console.time('readCommessePerPjmSet'); // funzione per verificare il tempo che impiega la funzione ->START
        var fSuccess = function (result) {
            var arrConsulente = [];
            for (var i = 0; i < result.items.length; i++) {
                if (!_.find(arrConsulente, {
                        cod: result.items[i].codConsulente
                    }))
                    arrConsulente.push({
                        cod: result.items[i].codConsulente
                    });
            }
            for (var t = 0; t < arrConsulente.length; t++) {
                arrConsulente[t].commesse = _.where(result.items, {
                    codConsulente: arrConsulente[t].cod,
                    stato: 'APERTO'
                });
            }
            this.commessePerConsulente = arrConsulente;
            this.oDialog.setBusy(false);
            console.timeEnd('readCommessePerPjmSet'); // funzione per verificare il tempo che impiega la funzione ->END
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'readCommessePerPjmSet'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var codPjm = this.user.codice_consulente;
        model.CommessePerStaffDelPjm.read(codPjm)
            .then(fSuccess, fError);
    },

    // funzione che, al cambio del periodo, nasconde alcuni tasti, svuota la tabella e le select
    // successivamente popola la tabella con il nuovo periodo selezionato
    onChangePeriod: function (pe) {
        console.time('onChangePeriod'); // funzione per verificare il tempo che impiega la funzione ->START
        this.filteredConsultants = undefined;
        this.oDialog.setBusy(true);
        this.visibleModel.setProperty("/undoButton", false);
        this.visibleModel.setProperty("/filterButton", false);
        this.visibleModel.setProperty("/closeProjectButton", false);
        this.visibleModel.setProperty("/downloadExcel", false);
        this.visibleModel.setProperty("/addConsultantButton", false);
        this.pmSchedulingModel.setProperty("/giorniTotali", "0");
        this.svuotaSelect();
        this.svuotaTabella();

        var table = this.oDialogTable;

        this.pe = pe;
        var p = pe.getSource().getSelectedItem().getProperty("text").replace(/ /g, '');
        var period = pe.getSource().getProperty("selectedKey");
        this.period = parseInt(period);
        model.persistence.Storage.session.save("period", period);
        this.periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(period)
        });
        model.persistence.Storage.session.save("periodSelected", this.periodSelect);
        this.periodStauts = pe.getSource().getSelectedItem().getProperty("additionalText");

        var fSuccess = function (result) {
            if (result.items.length > 0) {
                var commesseApertePjm = [];
                for (var i = 0; i < result.items.length; i++) {
                    if (_.find(this.allCommesse, {
                            codCommessa: result.items[i].codCommessaPeriodo
                        })) {
                        commesseApertePjm.push(_.find(this.allCommesse, {
                            codCommessa: result.items[i].codCommessaPeriodo
                        }));
                    }
                }
                if (commesseApertePjm.length === 0) {
                    this._enterErrorFunction("information", "ATTENZIONE", "Non sono presenti commesse aperte per questo periodo");
                    this.periodStauts = undefined;
                    this.oDialogTable.setVisible(false);
                    return;
                } else {
                    this.pmSchedulingModel.setProperty("/commesse", commesseApertePjm);

                    var pInizio = this.periodSelect.startDate;
                    var ggInizio = pInizio.getDate();
                    var mInizio = pInizio.getMonth() + 1;
                    var annoInizio = pInizio.getFullYear();

                    var pFine = this.periodSelect.endDate;
                    var ggFine = pFine.getDate();
                    var mFine = pFine.getMonth() + 1;
                    var annoFine = pFine.getFullYear();

                    if (ggInizio < 9) {
                        ggInizio = "0" + ggInizio;
                    }
                    if (mInizio < 9) {
                        mInizio = "0" + mInizio;
                    }
                    if (ggFine < 9) {
                        ggFine = "0" + ggFine;
                    }
                    if (mFine < 9) {
                        mFine = "0" + mFine;
                    }

                    var dataI = pInizio;
                    var dataF = pFine;

                    this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

                    // ** rendo visibili solo le colonne del periodo selezionato
                    var columns = table.getColumns();
                    for (var i = 0; i < columns.length; i++) {
                        columns[i].setVisible(true);
                    }
                    for (var i = this.numColonne; i < columns.length - 1; i++) {
                        columns[i].setVisible(false);
                    }
                    // **

                    // ** metto i nomi dei giorni sulle colonne
                    for (var i = 2; i < columns.length; i++) {
                        columns[i - 1].getLabel().mProperties.text = this.arrayGiorni[i - 2];
                    }
                    // **
                    this.pmSchedulingModel.setProperty("/days", this.arrayGiorni);
                    this.selectCommessa.setEnabled(true);

                    this.visibleModel.setProperty("/chargeButton", false);
                    this.readProfiloCommessaPeriodo(this.period);
                }
            } else {
                this._enterErrorFunction("information", "ATTENZIONE", "Non sono presenti commesse aperte per questo periodo");
                this.periodStauts = undefined;
                this.oDialogTable.setVisible(false);
            }
            console.timeEnd('onChangePeriod'); // funzione per verificare il tempo che impiega la funzione ->END
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'profiloCommessaPeriodoSet'");
            this.oDialogTable.setVisible(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var stato = "APERTO";
        model.ProfiloCommessaPeriodo.readRecordByPeriodStatus(parseInt(period), stato)
            .then(fSuccess, fError);
    },
    // **

    // **svuoto la tabella
    svuotaTabella: function () {
        console.time('svuotaTabella'); // funzione per verificare il tempo che impiega la funzione ->START
        this.visibleModel.setProperty("/downloadExcel", false);
        this.visibleModel.setProperty("/addConsultantButton", false);
        this.oDialogTable.setVisible(true);
        this.pmSchedulingModel.setProperty("/schedulazioni", []);
        this.pmSchedulingModel.setProperty("/nRighe", 1);
        this.pmSchedulingModel.getData().schedulazioni = [];
        console.timeEnd('svuotaTabella'); // funzione per verificare il tempo che impiega la funzione ->END
    },

    // **svuoto le select
    svuotaSelect: function (value) {
        console.time('svuotaSelect'); // funzione per verificare il tempo che impiega la funzione ->START
        if (!value) {
            this.selectCommessa.setEnabled(false);
            this.selectCommessa.setSelectedKey("");
            this.selectCommessa.setValue("");
        }
        console.timeEnd('svuotaSelect'); // funzione per verificare il tempo che impiega la funzione ->END
    },

    // selezionando una commessa faccio apparire il tasto di caricamento
    onSelectionChangeCommessa: function (evt) {
        this.svuotaTabella();
        this.visibleModel.setProperty("/chargeButton", true);
        this.visibleModel.setProperty("/closeProjectButton", true);
        this.visibleModel.setProperty("/downloadExcel", false);
        this.visibleModel.setProperty("/addConsultantButton", false);
        this.chosedCom = evt.getSource().getValue();
        this.pmSchedulingModel.setProperty("/commessa", this.chosedCom);
    },

    // al premere del pulsante faccio nascondere lo stesso
    chargeProjects: function (evt) {
        this.visibleModel.setProperty("/chargeButton", false);
    },

    // funzione che si avvia in automatico premendo un qualsiasi punto dopo aver selezionato la commessa
    // vado a leggermi lo staffo della commessa selezionata
    onChangeCommessa: function (evt) {
        this.oDialog.setBusy(true);
        var commessaScelta = _.find(this.pmSchedulingModel.getData().commesse, {
            nomeCommessa: this.chosedCom
        }).codCommessa;
        this.commessaScelta = commessaScelta;
        var fSuccess = function (res) {
            this.currentStaff = _.find(res.items, {
                codCommessa: commessaScelta
            });
            if (!this.currentStaff) {
                this.currentStaff = _.find(this.pmSchedulingModel.getData().commesse, {
                    codCommessa: commessaScelta
                });
            }
            console.log(this.currentStaff);
            this._onChangeCommessa(commessaScelta);
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore lettura XSJS 'letturaStaffCommessa.xsjs'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.StaffCommesseView.readStaffCommessa()
            .then(fSuccess, fError);
        this.visibleModel.setProperty("/chargeButton", false);
    },

    // in base al periodo e alla commessa selezionati setto tutti i parametri indispensabili
    // lancio al funzione di lettura schedulazioni per popolare la tabella
    _onChangeCommessa: function (commessaP) {
        this.allConsultantsForProject = undefined;
        this.filteredConsultants = undefined;
        var commessaScelta = commessaP;
        var c = _.find(this.pmSchedulingModel.getData().commesse, {
            codCommessa: commessaScelta
        });
        model.persistence.Storage.session.save("commessa", c);

        var req = {};
        req.idCommessa = commessaScelta;
        req.codPjm = c.codPjmAssociato;
        req.idStaff = this.currentStaff.codStaff ? this.currentStaff.codStaff : this.currentStaff.codiceStaffCommessa;
        if (req.idStaff) {
            req.cod_consulente = (this.user).codice_consulente;
            req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
            this.ric = req;
            req.nomeCommessa = this.chosedCom;
            if (!this.currentStaff.codStaff) {
                sap.m.MessageToast.show("Non è presente alcun consulente per questo staff");
            }
        } else {
            this._enterErrorFunction("information", "Informazione", "Creare lo staff per questa commessa!!!");
            //            this.visibleModel.setProperty("/undoButton", false);
            this.visibleModel.setProperty("/filterButton", false);
            return;
        }
        this.leggiTabella(req);
        this.visibleModel.setProperty("/visibleTable", true);
        this.visibleModel.setProperty("/filterButton", true);
        this.oDialog.setBusy(false);
    },

    // in base alla richiesta passata, popolo la tabella con i consulenti e le giornate schedulate
    leggiTabella: function (req) {
        this.oDialogTable.setBusy(true);
        var fSuccess = function (results) {
            var schedulazioni = this._uniqueElement(results);
            //            var ggTot = 0;
            //            for (var q = 0; q < schedulazioni.length; q++) {
            //                var ob = schedulazioni[q];
            //                ob.giorniScheduling = 0;
            //                for (var w = 1; w < 36; w++) {
            //                    var prp = "gg" + w;
            //                    var prpIcon = "ggIcon" + w;
            //                    if (ob[prp] && ob[prp] === this.chosedCom && ob[prpIcon].indexOf("delete") !== -1) {
            //                        ob.giorniScheduling++;
            //                    }
            //                }
            //                ggTot = ggTot + ob.giorniScheduling;
            //            }
            //            this.pmSchedulingModel.setProperty("/giorniTotali", ggTot);

            for (var t = 0; t < schedulazioni.length; t++) {
                var commessaConsulente = _.find(_.find(this.commessePerConsulente, {
                    cod: schedulazioni[t].codiceConsulente
                }).commesse, {
                    nomeCommessa: this.chosedCom
                });
                schedulazioni[t].teamLeader = commessaConsulente.teamLeader;
                schedulazioni[t].nomeSkill = commessaConsulente.nomeSkill;
            }

            var schedOrdinato = _.sortBy(schedulazioni, 'nomeConsulente');
            this.pmSchedulingModel.setProperty("/schedulazioni", schedOrdinato);
            this.allScheduling = this.pmSchedulingModel.getProperty("/schedulazioni");
            if (schedOrdinato.length > 0 && schedOrdinato.length <= 7) {
                this.pmSchedulingModel.setProperty("/nRighe", schedOrdinato.length);
            } else if (schedOrdinato.length > 7) {
                this.pmSchedulingModel.setProperty("/nRighe", 7);
            } else {
                this.pmSchedulingModel.setProperty("/nRighe", 1);
            }

            if (this.listaSelezioniFiltro) {
                this.onFilterConsultantsByPJMDialogOK();
            } else {
                this.getView().byId("idFiltroGiornateZeroPjmRichiesteModifica").setText(this._getLocaleText("NascondiConsulentiZeroGiornate"));
                this.getView().byId("idFiltroGiornateZeroPjmRichiesteModifica").setProperty("pressed", false);
                setTimeout(_.bind(this._onAfterRenderingTable, this));
            }

            //            setTimeout(_.bind(this._onAfterRenderingTable, this));
        };

        var fError = function (err) {
            console.log(err);
            this.oDialogTable.setBusy(false);
            this.visibleModel.setProperty("/closeProjectButton", false);
            this._enterErrorFunction("error", "Errore", "Errore lettura tabella: " + err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var fSuccessStaff = function (res) {
            model.tabellaSchedulingModify.readTable(req, res, this.chosedCom, this.allCommesse)
                .then(fSuccess, fError);
        };

        var fErrorStaff = function (err) {
            this.oDialogTable.setBusy(false);
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'anagStaffViewSet'");
        };

        fSuccessStaff = _.bind(fSuccessStaff, this);
        fErrorStaff = _.bind(fErrorStaff, this);

        model.tabellaSchedulingModify.readStaffPerCommessaPJM(req)
            .then(fSuccessStaff, fErrorStaff);
    },

    // funzione che mi construisce un array ordinato
    // legge gli skill dei consulenti estratti dalla lettura
    _uniqueElement: function (result) {
        var arrayfinale = result[0];
        var arrayGiornateSchedulate = result[0];

        var objCodiciConsulenti = {};
        for (var i = 0; i < arrayGiornateSchedulate.length; i++) {
            var c = "c" + i;
            objCodiciConsulenti[c] = arrayGiornateSchedulate[i].codiceConsulente;
        }
        var objCodiciConsulenti2 = {};
        for (var i = 0; i < arrayGiornateSchedulate.length; i++) {
            var c = "d" + i;
            objCodiciConsulenti2[c] = arrayGiornateSchedulate[i].codiceConsulente;
        }
        _.merge(objCodiciConsulenti, objCodiciConsulenti2);

        arrayfinale = [];
        for (var prop in objCodiciConsulenti) {
            var arr = _.where(arrayGiornateSchedulate, {
                codiceConsulente: objCodiciConsulenti[prop]
            });
            var obj = {};
            for (var i = 0; i < arr.length; i++) {
                obj.nomeConsulente = arr[i].nomeConsulente;
                obj.codiceConsulente = arr[i].codiceConsulente;
                obj.codicePJM = arr[i].codicePJM;
                var objProps = _.keys(arr[i]);
                var ggProp = _.find(objProps, function (item) {
                    return (item.indexOf("gg") > -1 && item.indexOf("Icon") < 0 && item.indexOf("Conflict") < 0 && item.indexOf("Req") < 0 && item.indexOf("Ins") < 0);
                });
                if (ggProp) {
                    var gg = ggProp.substring(2); //id del giorno nel periodo
                    // ggVal è il nome della proprietà "gg"+ la serie di "a" dipendente dalla quantità di attività in quello stesso giorno
                    var ggVal = ggProp;
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (obj[ggVal]) {
                        items = "a";
                        ggVal = ggVal + items;
                    }
                    obj[ggVal] = arr[i][ggProp];
                    obj["nota" + ggVal.substring(2)] = arr[i]["nota" + gg];
                    obj["req" + ggVal.substring(2)] = arr[i]["req" + gg];
                    obj["ggReq" + ggVal.substring(2)] = arr[i]["ggReq" + gg];
                    obj["ggIcon" + ggVal.substring(2)] = arr[i]["ggIcon" + gg];
                    obj["ggConflict" + ggVal.substring(2)] = arr[i]["ggConflict" + gg];
                    obj["ggIns" + ggVal.substring(2)] = arr[i]["ggIns" + gg];
                }
            }
            if (!_.isEmpty(obj))
                arrayfinale.push(obj);
        }
        arrayfinale = _.reduce(arrayfinale, function (results, item) {
            return _.any(results, function (result) {
                return _.isEqual(result, item);
            }) ? results : results.concat([item]);
        }, []);

        var arrCodConsulenti = [];
        for (var aa = 0; aa < arrayfinale.length; aa++) {
            for (var prop in arrayfinale[aa]) {
                if (prop.indexOf("codiceConsulente") > -1) {
                    arrCodConsulenti.push((arrayfinale[aa])[prop]);
                }
            }
        }
        return arrayfinale;
    },

    // funzione lanciata alla fine di tutto il caricamento
    // colora le celle in grigetto dei sabati e domeniche
    // somma e setta le giornate totali per consulenti e totale complessivo
    _onAfterRenderingTable: function (evt) {
        if (!this.oDialogTable.getBusy()) {
            this.oDialogTable.setBusy(true);
        }
        var table = this.oDialogTable;

        setTimeout(_.bind(function () {
            table.rerender()
            setTimeout(_.bind(function () {
                var rows = table.getRows();
                var arrayCounter = [];
                for (var i = 0; i < rows.length; i++) {
                    var cellsPerRows = rows[i].getAggregation("cells");
                    var g = 0;
                    for (var j = 1; j < cellsPerRows.length - 1; j++) {
                        var cella = cellsPerRows[j];
                        if (_.find(this.weekendDays, _.bind(function (item) {
                                return (item == j);
                            }, this))) {
                            var column = $("#" + cella.getDomRef().id).parent().parent();
                            column.css('background-color', '#e5e5e5');
                        }
                    }
                }

                var dati = this.pmSchedulingModel.getData();
                var schedulazioni = dati.schedulazioni;
                var commessaScelta = this.selectCommessa.getSelectedItem().mProperties.text;
                for (var i = 0; i < schedulazioni.length; i++) {
                    var giorniS = {
                        "giornoS": arrayCounter[i]
                    };
                    if (this.visibleModel.getProperty("/enabledButton") === false && arrayCounter[i]) {
                        schedulazioni[i].giorniScheduling = arrayCounter[i];
                    } else {
                        if (!schedulazioni[i].giorniScheduling) {
                            schedulazioni[i].giorniScheduling = 0;
                        }
                        this._calcoloGiornate(schedulazioni[i]);
                    }
                }

                this.visibleModel.setProperty("/downloadExcel", true);
                this.visibleModel.setProperty("/addConsultantButton", true);
                this.oDialogTable.setBusy(false);
                this.oDialog.setBusy(false);
            }, this));
        }, this));
    },

    /* INIZIO - funzioni per selezionare uno o più consulenti presenti nello staff */
    // apro dialog 'filterConsultantsByPJM' e carico la lista di consulenti presenti nello staff
    //    onFilterConsultantsByPJMPress: function () {
    onFilterPress: function () {
        var consultants = [];
        if (!this.allConsultantsForProject) {
            //            var schedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni");
            var schedulazioni = this.allScheduling;
            for (var i = 0; i < schedulazioni.length; i++) {
                var consultant = {
                    codConsulente: schedulazioni[i].codiceConsulente,
                    nomeCognome: schedulazioni[i].nomeConsulente
                };
                consultants.push(consultant);
            }
            this.allConsultantsForProject = consultants;
        }
        var page = this.getView().byId("requestModifySchedulingPage");
        if (!this.filterConsultantsByPJMDialog) {
            this.filterConsultantsByPJMDialog = sap.ui.xmlfragment("view.dialog.filterConsultantsByPJMModify", this);
            page.addDependent(this.filterConsultantsByPJMDialog);
        }

        this.pmSchedulingModel.setProperty("/consultants", this.allConsultantsForProject);
        var lista = sap.ui.getCore().getElementById("idListaConsulentiByPJMModify");
        var selectedConsultants = lista.getSelectedItems();
        var selectAllButton = sap.ui.getCore().byId("selectAllButtonModify");
        if (!!selectAllButton) {
            if (selectedConsultants.length === 0) {
                selectAllButton.setText(model.i18n._getLocaleText("SELECT_ALL"));
            } else {
                selectAllButton.setText(model.i18n._getLocaleText("DESELECT_ALL"));
            }
        }
        this.filterConsultantsByPJMDialog.open();
    },

    // filtro di ricerca dei consulenti nel dialog 'filterConsultantsByPJM'
    onFilterConsultantsByPJMSearch: function (evt) {
        var src = evt.getSource();
        this.searchValue = src.getValue();
        var searchProperty = ["codConsulente", "nomeCognome"];
        this.applyFilterConsultantsByPJM(this.searchValue, searchProperty);
    },

    // funzione concatenata al filtro di ricerca
    applyFilterConsultantsByPJM: function (value, params) {
        var list = sap.ui.getCore().getElementById("idListaConsulentiByPJMModify");
        if (!list.getBinding("items").oList || list.getBinding("items").oList.length === 0)
            return;

        var temp = list.getBinding("items").oList[0]; // a template just to recover the data types
        var filtersArr = [];

        if (!_.isEmpty(params)) {
            if (!_.isArray(params)) {
                params = [params];
            }
            for (var i = 0; i < params.length; i++) {
                switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
                    case "undefined":
                        break;
                    case "string":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
                        break;
                    case "number":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
                        break;
                }
            }
            var filter = new sap.ui.model.Filter({
                filters: filtersArr,
                and: false
            });
            list.getBinding("items").filter(filter);
            return;
        }
        list.getBinding("items").filter();
    },

    // funzione che costrisce un array di elementi selezionati
    onSelectionChangeFilterConsultants: function (evt) {
        var oList = evt.getSource();
        this.listaSelezioniFiltro = oList.getSelectedContexts(true);
    },

    // chiude il dialog, rimuove eventuali selezioni e filtri
    onFilterConsultantsByPJMDialogClose: function () {
        this.filterConsultantsByPJMDialog.close();
        var list = sap.ui.getCore().getElementById("idListaConsulentiByPJMModify");
        list.removeSelections();
        var oBinding = list.getBinding("items");
        oBinding.filter();
        sap.ui.getCore().getElementById("idOnFilterConsultantsByPJMModifySearch").setValue("");
        this.listaSelezioniFiltro = undefined;
    },

    // selezionato uno o piu consulenti, nasconde gli altri dalla tabella
    onFilterConsultantsByPJMDialogOK: function () {
        this.filteredConsultants = undefined;
        var selectedConsultantsCode = [];
        if (this.listaSelezioniFiltro && this.listaSelezioniFiltro.length > 0) {
            for (var i = 0; i < this.listaSelezioniFiltro.length; i++) {
                var path = parseInt(this.listaSelezioniFiltro[i].getPath().split("/")[2]);
                var codConsulente = this.listaSelezioniFiltro[i].getModel().getData().consultants[path].codConsulente;
                selectedConsultantsCode.push(codConsulente);
            }
            var schedulazioni = [];
            for (var j = 0; j < selectedConsultantsCode.length; j++) {
                var schedulazione = _.find(this.allScheduling, {
                    'codiceConsulente': selectedConsultantsCode[j]
                });
                if (schedulazione) {
                    schedulazioni.push(schedulazione);
                }
            }
            this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioni);
            this.getView().byId("idFiltroGiornateZeroPjmRichiesteModifica").setText(this._getLocaleText("NascondiConsulentiZeroGiornate"));
            this.getView().byId("idFiltroGiornateZeroPjmRichiesteModifica").setProperty("pressed", false);
            this.filteredConsultants = selectedConsultantsCode;
        } else {
            this.onFilterConsultantsByPJMDialogRipristino();
        }

        var lunghezzaSchedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni").length;
        if (lunghezzaSchedulazioni === 0) {
            lunghezzaSchedulazioni = 1;
        }
        this.pmSchedulingModel.setProperty("/nRighe", lunghezzaSchedulazioni);
        this.pmSchedulingModel.refresh();
        this.filterConsultantsByPJMDialog.close();
        setTimeout(_.bind(this._onAfterRenderingTable, this));
    },

    // funzione che ripristina la tabella con tutti i consulenti
    onFilterConsultantsByPJMDialogRipristino: function () {
        this.filteredConsultants = undefined;
        this.pmSchedulingModel.setProperty("/schedulazioni", this.allScheduling);
        var lunghezzaSchedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni").length;
        sap.ui.getCore().getElementById("idListaConsulentiByPJMModify").removeSelections();
        this.pmSchedulingModel.setProperty("/nRighe", lunghezzaSchedulazioni);
        this.pmSchedulingModel.refresh();
        this.onFilterConsultantsByPJMDialogClose();
        setTimeout(_.bind(this._onAfterRenderingTable, this));
    },

    // funzione che seleziona o deselezioni tutti i consulenti
    onFilterSelectAllItems: function (evt) {
        var src = evt.getSource();
        var txtButton = model.i18n._getLocaleText(src.getText());
        var lista = sap.ui.getCore().getElementById("idListaConsulentiByPJMModify");
        if (txtButton === "Seleziona tutto") {
            lista.selectAll();
            src.setText("Deseleziona tutto");
        } else {
            lista.removeSelections();
            src.setText("Seleziona tutto");
        }
        this.listaSelezioniFiltro = lista.getSelectedContexts(true);
    },
    /* FINE - funzioni per selezionare uno o più consulenti presenti nello staff */

    /* INIZIO - funzione di inserimento, modifica, cancellazione di richieste schedulazioni */
    // funzione lanciata alla pressione di una qualsiasi icona
    // trova il consulente e tutte le sue schedulazioni
    openMenu: function (evt) {
        this.halfDay = false;
        this.oButton = evt.getSource();
        this.idButton = this.oButton.getId();
        this.nButton = parseInt((this.idButton.split("-")[2]).substring(this.idButton.split("-")[2].indexOf("n") + 1));
        this.row = this.oButton.getBindingContext("pmSchedulingModel").getPath().split("/")[2];
        this.dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[this.row];

        var prop = "gg" + this.nButton;
        var rq = "req" + this.nButton;
        //        var propBis = "gg" + this.nButton + "a";
        //        var rqBis = "req" + this.nButton + "a";
        var schedulazioneGiornaliera = this.dataModelConsultant[prop];
        //        var schedulazioneGiornalieraBis = this.dataModelConsultant[propBis];

        //        if (evt.getSource().getSrc() === "sap-icon://delete" && schedulazioneGiornaliera === this.chosedCom) {
        //            return;
        //        }

        if (this.oButton.getSrc() !== "sap-icon://travel-itinerary") {
            if (this.oButton.getSrc() === "sap-icon://bed") {
                this.halfDay = true;
            }
            this._openMenu();
        }
    },

    // funzione che apre il menu 'menuButtonsPJM'
    _openMenu: function () {
        if (!this._menu) {
            this._menu = sap.ui.xmlfragment("view.fragment.menuButtonsModifyPJM", this);
            this.getView().addDependent(this._menu);
        }
        var eDock = sap.ui.core.Popup.Dock;
        this._menu.setModel(this.visibleModel, "visibleModel");
        this._menu.open(undefined, this.oButton, eDock.BeginTop, eDock.BeginBottom, this.oButton);
        setTimeout(_.bind(this.nascondiItems, this));
    },

    // funzione che nasconde tutte le righe del menu che non servono per il consulente scelto
    nascondiItems: function () {
        var prop = "gg" + this.nButton;
        var rq = "req" + this.nButton;
        var propIcon = "ggIcon" + this.nButton;
        var propNote = "nota" + this.nButton;

        //        var propBis = "gg" + this.nButton + "a";
        //        var rqBis = "req" + this.nButton + "a";
        var schedulazioneGiornaliera = this.dataModelConsultant[prop];
        var richiestaGiornaliera = this.dataModelConsultant[propIcon];
        var idReq = this.dataModelConsultant[rq];
        //        var schedulazioneGiornalieraBis = this.dataModelConsultant[propBis];

        this.visibleModel.setProperty("/deleteVisible", false);
        this.visibleModel.setProperty("/noteVisible", false);
        this.visibleModel.setProperty("/cancelVisible", false);
        this.visibleModel.setProperty("/request", true);
        this.visibleModel.setProperty("/halfRequest", true);

        if (schedulazioneGiornaliera && schedulazioneGiornaliera === this.chosedCom) {
            //            if (schedulazioneGiornaliera !== "feri" && typeof (this.dataModelConsultant[rq]) !== "number") {
            if (schedulazioneGiornaliera !== "feri") {
                this.visibleModel.setProperty("/deleteVisible", true);
                this.visibleModel.setProperty("/noteVisible", true);
                this.visibleModel.setProperty("/request", true);
                this.visibleModel.setProperty("/halfRequest", true);
            }
            if (richiestaGiornaliera === 'sap-icon://delete' || idReq.indexOf("M") !== -1) {
                this.visibleModel.setProperty("/request", false);
                this.visibleModel.setProperty("/halfRequest", false);
            }
            if (this.halfDay) {
                this.visibleModel.setProperty("/request", false);
            }
        } else if (schedulazioneGiornaliera) {
            this.visibleModel.setProperty("/request", false);
            this.visibleModel.setProperty("/halfRequest", false);
            if (this.dataModelConsultant[propNote]) {
                this.visibleModel.setProperty("/noteVisible", true);
            } else {
                this._menu.close();
            }
        }
        if (richiestaGiornaliera && richiestaGiornaliera === "sap-icon://delete") {
            if (schedulazioneGiornaliera !== "feri") {
                this.visibleModel.setProperty("/deleteVisible", false);
                this.visibleModel.setProperty("/noteVisible", false);
                this.visibleModel.setProperty("/cancelVisible", true);
            }
        }
    },

    // funzione lanciata alla pressione di una richiesta e trova la commessa selezionata
    _selectCommessa: function (evt) {
        this.nomeCommessaSelezionata = evt.getParameters().item.getParent().getParent().getProperty("text");
        var commessa = _.find(this.commesseQuestoConsulente, {
            'nomeCommessa': this.nomeCommessaSelezionata
        });
        this.commessa = commessa.codCommessa;
    },

    // funzione lanciata alla pressione di un bottone tra 'copia', 'incolla', 'taglia', 'cancella'
    handleMenuItemPress: function (evt) {
        var idButton = this.oButton.getId();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        var row = this.oButton.getBindingContext("pmSchedulingModel").getPath().split("/")[2];
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[parseInt(row)];
        var com = "gg" + nButton;
        var req = "req" + nButton;
        var ggIcon = "ggIcon" + nButton;
        var nota = "nota" + nButton;
        var comA = "gg" + nButton + "a";

        var ggIconA = "ggIcon" + nButton + "a";
        var notaA = "nota" + nButton + "a";
        if (evt.getParameters().item.getIcon() === "sap-icon://delete" && dataModelConsultant[comA]) {
            if (dataModelConsultant[com] === "feri" || dataModelConsultant[comA] === "feri") {
                this.actionButton(evt);
            } else {
                this.doppiaNote = undefined;
                this.doppiaCopia = undefined;
                this.doppiaCancellazione = [];
                this.doppiaCancellazione.push({
                    ric: dataModelConsultant[com],
                    icon: dataModelConsultant[ggIcon],
                    nota: dataModelConsultant[nota]
                }, {
                    ric: dataModelConsultant[comA],
                    icon: dataModelConsultant[ggIconA],
                    nota: dataModelConsultant[notaA]
                });
                this.commesseDialog = sap.ui.xmlfragment("view.fragment.SelectProjectActionSheet", this);
                this.getView().addDependent(this.commesseDialog);
                this.pmSchedulingModel.setProperty("/requests", this.doppiaCancellazione);
                this.commesseDialog.setModel(this.pmSchedulingModel, "pmSchedulingModel");
                this.commesseDialog.open();
            }
        } else if (evt.getParameters().item.getIcon() === "sap-icon://notes" && dataModelConsultant[comA]) {
            if (dataModelConsultant[com] === "feri" || dataModelConsultant[comA] === "feri") {
                this.actionButton(evt);
            } else {
                this.doppiaCancellazione = undefined;
                this.doppiaCopia = undefined;
                this.doppiaNote = [];
                this.doppiaNote.push({
                    ric: dataModelConsultant[com],
                    icon: dataModelConsultant[ggIcon],
                    nota: dataModelConsultant[nota]
                }, {
                    ric: dataModelConsultant[comA],
                    icon: dataModelConsultant[ggIconA],
                    nota: dataModelConsultant[notaA]
                });
                this.commesseDialog = sap.ui.xmlfragment("view.fragment.SelectProjectActionSheet", this);
                this.getView().addDependent(this.commesseDialog);
                this.pmSchedulingModel.setProperty("/requests", this.doppiaNote);
                this.commesseDialog.setModel(this.pmSchedulingModel, "pmSchedulingModel");
                this.commesseDialog.open();
            }
        } else if (evt.getParameters().item.getIcon()) {
            this.paste = false;
            this.actionButton(evt);
        } else {
            // entra in questo else se clicco per esempio sul nome della commessa e quindi mi aspetto che non succeda nulla
            return;
        }
        //        }
    },

    // funzione che chiude il dialog di scelta commessa nel caso di del o note
    onRequestDialogClose: function () {
        this.commesseDialog.close();
        this.commesseDialog.destroy(true);
        model.persistence.Storage.local.remove("copyBis");
    },

    // funzione che dopo ever scelto la commessa (nel caso di del o note) esegue il codice
    onRequestDialogOK: function (evt) {
        var requestList = sap.ui.getCore().getElementById("idRequestsList");
        var requestSelected = requestList.getSelectedItems();
        if (requestSelected.length > 0) {
            var req = requestSelected[0].getTitle();
            var icon = requestSelected[0].getIcon();
            this.tempRicToDel = {};
            this.tempRicToDel.req = req;
            this.tempRicToDel.icon = icon;

            if (this.doppiaCopia) {
                if (model.persistence.Storage.local.get("copy").nomeCommessa === this.tempRicToDel.req) {} else {
                    model.persistence.Storage.local.save("copy", model.persistence.Storage.local.get("copyBis"));
                }
                this.tempRicToDel = undefined;
                this.doppiaCopia = undefined;
            } else if (this.doppiaNote) {
                this.actionButton(undefined, "note");
            } else {
                this.actionButton();
            }
            this.onRequestDialogClose();
        }
    },

    // funzione che in base al tipo di richiesta, popola l'oggetto da scrivere, modificare, cancellare
    actionButton: function (evt, richiesta) {
        var del = false;
        var b = this.oButton;
        this.currentButton = b;
        var idButton = b.getId();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        var row = this.oButton.getBindingContext("pmSchedulingModel").getPath().split("/")[2];
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[parseInt(row)];
        var prop = "gg" + nButton;
        var propI = "ggIcon" + nButton;

        this.backupRowScheduling = dataModelConsultant;

        var tipoRichiesta = evt ? evt.getParameters().item.getText() : "";

        if (!richiesta) {
            var richiesta = "";
            switch (tipoRichiesta) {
                case this._getLocaleText("ASSIGN"):
                    richiesta = "Assign";
                    break;
                case this._getLocaleText("ASSIGN_HALF_DAY"):
                    richiesta = "halfDay";
                    break;
                case this._getLocaleText("C_WHIT_CONSULTANT"):
                    richiesta = "wCons";
                    break;
                case this._getLocaleText("NON_C_WHIT_CONSULTANT"):
                    richiesta = "wNonCons";
                    break;
                case this._getLocaleText("C_WHIT_CUSTOMER"):
                    richiesta = "wCust";
                    break;
                case this._getLocaleText("NON_C_WHIT_CUSTOMER"):
                    richiesta = "wNonCust";
                    break;
                case this._getLocaleText("NON_CONFIRMED_CUSTOMER"):
                    richiesta = "notConfirmed";
                    break;
                case this._getLocaleText("ABROAD"):
                    richiesta = "abroad";
                    break;
                case this._getLocaleText("NOTE"):
                    richiesta = "note";
                    break;
                default:
                    richiesta = "del";
                    break;
            }
        }
        var propNotaGiorno, propReq;
        var tempPropIcon = "ggIcon" + nButton;

        if ((dataModelConsultant[prop] && dataModelConsultant[prop] !== this.nomeCommessaSelezionata && (richiesta === "halfDay") && (dataModelConsultant[tempPropIcon] === "sap-icon://time-entry-request" || dataModelConsultant[tempPropIcon] === "sap-icon://bed")) || (this.tempRicToDel && dataModelConsultant[prop] !== this.tempRicToDel.req) || ((richiesta === 'note' || richiesta === "del") && dataModelConsultant[prop] === "feri")) {
            prop = prop + "a";
            propNotaGiorno = "nota" + nButton + "a";
            propReq = "req" + nButton + "a";
        } else {
            propNotaGiorno = "nota" + nButton;
            propReq = "req" + nButton;
        }
        this.noteModel.setProperty("/note", dataModelConsultant[propNotaGiorno]);
        this.oButton = "";

        // prendo il numero della tabella per passare correttamente la data
        var table = this.oDialogTable;
        var giornoMese = parseInt(table.getColumns()[nButton].getLabel().getProperty("text"));
        if (giornoMese < 10) {
            giornoMese = "0" + giornoMese;
        }
        var anno = model.persistence.Storage.session.get("periodSelected").annoPeriodo;
        var periodoMese = model.persistence.Storage.session.get("periodSelected").mesePeriodo;
        if (parseInt(giornoMese) < 10 && nButton > 20) {
            if (parseInt(periodoMese) !== 12) {
                periodoMese = parseInt(periodoMese) + 1;
            } else {
                periodoMese = "1";
                anno = parseInt(anno) + 1;
            }
        }
        if (periodoMese < 10) {
            periodoMese = "0" + periodoMese;
        }

        var dataRichiesta = anno + "-" + periodoMese + "-" + giornoMese;
        if (!(new Date(dataRichiesta)).getYear()) {
            var lastDayOfMonth = new Date(parseInt(dataRichiesta.split("-")[0]), parseInt(dataRichiesta.split("-")[1]), 0);
            lastDayOfMonth = lastDayOfMonth.getDate();
            var mesePlus = parseInt(periodoMese) + 1;
            if (mesePlus < 10) {
                mesePlus = "0" + mesePlus;
            }
            var giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
            dataRichiesta = anno + "-" + mesePlus + "-" + giornoPlus;
        }

        var dataToSend = {
            "idScambio": 0,
            "codiceConsulenteModifica": dataModelConsultant.codiceConsulente,
            "dataRichiestaModifica": dataRichiesta,
            "codCommessaModifica": this.commessaScelta,
            "codAttivitaRichiestaModifica": "",
            "idPeriodoSchedModifica": this.period,
            "codPjmModifica": this.user.codice_consulente,
            "dataInserimentoModifica": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "noteModifica": "",
            "statoReqModifica": "PROPOSED",
            "giornoPeriodoModifica": nButton,
            "codCommessaOldModifica": "0000000000",
            "idReqModificaInt": dataModelConsultant[propReq] ? ((dataModelConsultant[propReq].toString()).indexOf("M") !== -1 ? dataModelConsultant[propReq].substring(1) : 0) : 0,
            "idReqOriginale": dataModelConsultant[propReq] ? dataModelConsultant[propReq] : 0
        };

        var o = _.cloneDeep(this.pmSchedulingModel.getData().schedulazioni[row]);
        this.visibleModel.setProperty("/undoButton", true);
        switch (richiesta) {
            case "Assign":
            case "sap-icon://complete":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaModifica = "complete";
                break;
            case "halfDay":
            case "sap-icon://time-entry-request":
                if (b.getSrc() === "sap-icon://time-entry-request" || b.getSrc() === "sap-icon://bed") {
                    if (o[prop] && o[prop] === this.chosedCom) {
                        o[prop] = this.chosedCom;
                        dataToSend.codAttivitaRichiestaModifica = "complete";
                    } else if (o[prop] && o[prop] !== this.chosedCom) {
                        prop = prop + "a";
                        o[prop] = this.chosedCom;
                    }
                    dataToSend.codAttivitaRichiestaModifica = "time-entry-request";
                } else {
                    o[prop] = this.chosedCom;
                    dataToSend.codAttivitaRichiestaModifica = "time-entry-request";
                }
                break;
            case "wCons":
            case "sap-icon://customer-and-contacts":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaModifica = "customer-and-contacts";
                break;
            case "wNonCons":
            case "sap-icon://employee-rejections":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaModifica = "employee-rejections";
                break;
            case "wCust":
            case "sap-icon://collaborate":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaModifica = "collaborate";
                break;
            case "wNonCust":
            case "sap-icon://offsite-work":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaModifica = "offsite-work";
                break;
            case "notConfirmed":
            case "sap-icon://role":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaModifica = "role";
                break;
            case "abroad":
            case "sap-icon://globe":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaModifica = "globe";
                break;
            case "del":
                if (parseInt(dataToSend.idReqModificaInt) !== 0) {
                    var icon = b.getSrc();
                    var req = icon.substring(icon.lastIndexOf("/") + 1, icon.length);
                    del = true;
                    dataToSend.codAttivitaRichiestaModifica = req;
                    this.tempRicToDel = undefined;
                    this.dataToSave = dataToSend;
                    this._delData(parseInt(dataToSend.idReqModificaInt));
                } else {
                    o[prop] = this.chosedCom;
                    dataToSend.codAttivitaRichiestaModifica = "DELETE";
                }
                break;
            case "note":
                this.dataForUpdate = dataToSend;
                this.onAddNote();
                del = true;
                break;
        }

        if (!del) {
            if (richiesta === "del") {
                // entra qui dentro quando cancello una richiesta inserita nella pagina del pm e che non ha un idReqModificaInt
                this.saveRequest(dataToSend, prop);
            } else {
                if (o[tempPropIcon] === "sap-icon://offsite-work" || dataToSend.codAttivitaRichiesta === "offsite-work") {
                    dataToSend.id_req = "";
                }
                this.backupDataToSend = dataToSend;

                var fSuccess = function (results) {
                    if (!!results.results && results.results.length > 0) {
                        var bed = _.where(results.results, {
                            COD_ATTIVITA_RICHIESTA: 'bed'
                        });
                        if (bed.length !== results.results.length) {
                            this._openDialogConflicts(this.backupDataToSend.codiceConsulenteModifica, dataToSend);
                        } else {
                            this.saveRequest(dataToSend, prop);
                            return;
                        }
                    } else {
                        this.saveRequest(dataToSend, prop);
                        return;
                    }
                };
                var fError = function (err) {
                    this.console(err, "error");
                };
                fSuccess = _.bind(fSuccess, this);
                fError = _.bind(fError, this);

                this.checkForPreviousRequestForConsultant(idButton, row)
                    .then(fSuccess, fError);
            }
        }
    },

    // funzione che controlla se sono presenti in tabella altre richieste di altri PJM nel giorno selezionato
    checkForPreviousRequestForConsultant: function (idButton, rowId) {
        var defer = Q.defer();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        var row = rowId;
        var prop = "gg" + nButton;
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[row];
        var table = this.getView().byId("idSchedulazioniModifyRequestTable");
        var giornoMese = parseInt(table.getColumns()[nButton].getLabel().getProperty("text"));

        var dataToSend = {
            "idConsulente": "",
            "idPeriodo": 0,
            "idPJM": "",
            "idGiorno": 0
        };

        dataToSend.idConsulente = dataModelConsultant.codiceConsulente;
        this.consulenteSelezionato = dataToSend.idConsulente;
        dataToSend.idPJM = this.user.codice_consulente;
        dataToSend.idPeriodo = this.period;
        dataToSend.idGiorno = parseInt(nButton);

        var fSuccess = function (results) {
            results = JSON.parse(results);
            defer.resolve(results);
        };

        var fError = function (err) {
            this.console(err, "error");
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.checkForPreviousRequestForConsultant(dataToSend, fSuccess, fError);
        return defer.promise;
    },

    // funzione chiamata in caso di conflitto: fa apparire un dialog di scelta
    _openDialogConflicts: function (codConsulente, dataToSend) {
        this.visibleModel.setProperty("/undoButton", false);
        sap.m.MessageBox.show(
            this._getLocaleText("SHOW_SCHEDULE_CONFLICTS_WITH_OTHER_PJM"), {
                icon: sap.m.MessageBox.Icon.INFORMATION,
                title: this._getLocaleText("SCHEDULE_CONFLICTS_TITLE"),
                actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                onClose: _.bind(function (oAction) {
                    if (oAction === "YES") {
                        var req = {};
                        var commessaScelta = model.persistence.Storage.session.get("commessa");
                        req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
                        req.codPjm = this.user.codice_consulente;
                        req.codConsulente = codConsulente;
                        this._leggiTabellaSchedulingConsulenti(req);
                    } else {
                        this.saveRequest(dataToSend);
                    }
                }, this),
            }
        );
    },

    // funzione che legge tutte le richieste del singolo consulente
    _leggiTabellaSchedulingConsulenti: function (req) {
        var fSuccess = function (results) {
            var schedulazioni = results.resultOrdinato;

            // rimuovo tutte le giornate dove il consulente non può essere schedulato
            var gg, icon;
            for (x = 1; x < 36; x++) {
                gg = "gg" + x;
                icon = "ggIcon" + x;

                var items = ""; //serie di "a" aggiunte alla proprietà
                while (schedulazioni[0][icon]) {
                    if (schedulazioni[0][icon] === "sap-icon://offsite-work") {
                        delete schedulazioni[0][icon];
                        delete schedulazioni[0][gg];
                    }
                    items = "a";
                    icon = icon + items;
                    gg = gg + items;
                }
            }

            this.pmSchedulingConsultantModel.setProperty("/schedulazioni", []);
            this.pmSchedulingConsultantModel.setProperty("/schedulazioni", schedulazioni);
            this.pmSchedulingConsultantModel.setProperty("/nRighe", 1);
            if (!this.schedConsultantDialog) {
                this.schedConsultantDialog = sap.ui.xmlfragment("view.dialog.viewConsultantScheduling", this);
                this.getView().addDependent(this.schedConsultantDialog);
            }
            var propCons = _.find(this.pmSchedulingModel.getProperty("/schedulazioni"), {
                codiceConsulente: schedulazioni[0].codiceConsulente
            });
            if (propCons && propCons.nomeConsulente) {
                this.schedConsultantDialog.setTitle(propCons.nomeConsulente);
                schedulazioni[0].nomeConsulente = propCons.nomeConsulente;
            } else {
                this.schedConsultantDialog.setTitle("");
            }
            this.oDialogTable.setBusy(false);
            this.schedConsultantDialog.open();
        };

        var fError = function (err) {
            this.oDialogTable.setBusy(false);
            sap.m.MessageToast.show("errore lettura tabella");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialogTable.setBusy(true);
        model.tabellaScheduling.readPjmSingleConsultant(req)
            .then(fSuccess, fError);
    },

    // funzione che chiude il dialog 'viewConsultantScheduling'
    onConsultantSchedulingDialogClose: function () {
        if (!!this.schedConsultantDialog) {
            this.schedConsultantDialog.close();
        }
        this.multipleRequest = undefined;
        this.backupDataToSend = undefined;
    },

    // funzione che invia la richiesta da salvare
    onConsultantSchedulingDialogOK: function () {
        if (this.multipleRequest) {
            this.multipleRequest = undefined;
            this.saveMultipleRequest(this.inserimentiBackup);
        } else {
            this.saveRequest(this.backupDataToSend);
        }
        this.onConsultantSchedulingDialogClose();
    },

    beforeCloseConsultantScheduling: function () {
        this.pmSchedulingConsultantModel.setProperty("/schedulazioni", []);
        this.pmSchedulingConsultantModel.setProperty("/nRighe", 0);
        this.pmSchedulingConsultantModel.refresh();
    },

    // funzione che viene avviata all'apertura del dialog 'viewConsultantScheduling'
    afterOpenConsultantScheduling: function (evt) {
        var dialogPeriodInput = sap.ui.getCore().byId("idSelectPeriodoConsultantDialog");
        var viewPeriodValue = this.getView().byId("idSelectPeriodo").getValue();
        dialogPeriodInput.setValue(viewPeriodValue);

        /////**********************day names on the header columns*********************************////
        var table = sap.ui.getCore().byId("idSchedulazioniConsulenteTable");
        table.setBusy(true);
        var period = model.persistence.Storage.session.get("period");
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(period)
        });

        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();

        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = pInizio;
        var dataF = pFine;
        //---------------------------------------------------------------------------------------
        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

        // ** rendo visibili solo le colonne del periodo selezionato
        var columns = table.getColumns();
        for (var i = 0; i < columns.length; i++) {
            columns[i].setVisible(true);
        }
        for (var i = this.numColonne; i < columns.length - 1; i++) {
            columns[i].setVisible(false);
        }

        // ** metto i nomi dei giorni sulle colonne
        for (var i = 2; i < columns.length; i++) {
            if (columns[i - 1].getVisible()) {
                columns[i - 1].getLabel().setProperty("text", this.arrayGiorni[i - 2]);
            } else {
                continue;
            }
        }
        table.setBusy(false);

        jQuery.sap.delayedCall(500, this, function () {
            table.rerender();
            this._popolaTabellaSchedulazioneSingoloUtente();
        });
    },

    // funzione che popola correttamente la tabella 'viewConsultantScheduling'
    _popolaTabellaSchedulazioneSingoloUtente: function () {
        var table = sap.ui.getCore().byId("idSchedulazioniConsulenteTable");
        table.setBusy(true);
        var rows = table.getRows();
        var arrayCounter = [];
        var toCheck = "toCheck";
        for (var i = 0; i < rows.length; i++) {
            var cellsPerRows = rows[i].getAggregation("cells");
            var g = 0;
            for (var j = 1; j < cellsPerRows.length - 1; j++) {
                var cella = cellsPerRows[j];
                if (_.find(this.weekendDays, _.bind(function (item) {
                        return (item == j);
                    }, this))) {
                    var column = $("#" + cella.getDomRef().id).parent().parent();
                    column.css('background-color', '#e5e5e5');
                }
                var propGiornaliera = "gg" + j;

                var richiestaGiornaliera = this.pmSchedulingConsultantModel.getProperty("/schedulazioni")[0];
                if (richiestaGiornaliera) {
                    while (richiestaGiornaliera[propGiornaliera]) {
                        var iconProp = propGiornaliera.replace("gg", "ggIcon");
                        var commesseQuestoConsulente = _.find(this.commessePerConsulente, {
                            cod: this.dataModelConsultant.codiceConsulente
                        }).commesse;
                        var stato = "";
                        if (_.find(commesseQuestoConsulente, {
                                nomeCommessa: richiestaGiornaliera[propGiornaliera]
                            })) {
                            stato = "None";
                        } else {
                            stato = "Warning";
                        }
                        var txt = new sap.m.ObjectStatus({
                            text: richiestaGiornaliera[propGiornaliera],
                            icon: richiestaGiornaliera[iconProp],
                            state: stato
                        });
                        cella.addAggregation("items", txt);
                        propGiornaliera = propGiornaliera + "a";
                    }
                }
            }
        }
        table.setBusy(false);
    },

    // funzione che elimina una richiesta di schedulazione
    _delData: function (idReq) {
        var fSuccess = function (res) {
            this.leggiTabella(this.ric);
        };
        var fError = function (err) {
            sap.m.MessageToast.show("ERRORE cancellazione");
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.SchedulingModifyRequests.deleteSingleSchedModRequest(idReq)
            .then(fSuccess, fError);
    },

    // funzione che scrive in tabella una richiesta
    saveRequest: function (req) {
        var msgOk = "Richiesta effettuata correttamente";
        var msgKo = "Errore in inserimento richiesta";
        this.dataToSave = req;
        var fSuccess = function (ok) {
            sap.m.MessageToast.show(msgOk, {
                duration: 750,
            });
            this.leggiTabella(this.ric);
        };
        var fError = function (err) {
            sap.m.MessageToast.show(JSON.parse(err.responseText).error);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.SchedulingModifyRequests.sendRequest(req)
            .then(fSuccess, fError);
    },

    // funzione che calcola i giorni, e mezze giornate, della commessa in alto
    _calcoloGiornate: function (questoConsulente) {
        var giorniTemp = questoConsulente.giorniScheduling;
        var count = 0;
        for (var i = 1; i < 36; i++) {
            var propIn = "gg" + i;
            var propInIcon = "ggIcon" + i;
            var propInA = "gg" + i + "a";
            var propInIconA = "ggIcon" + i + "a";
            var commessaSelezionata = this.selectCommessa.getValue();
            if ((questoConsulente[propIn] === commessaSelezionata || questoConsulente[propInA] === commessaSelezionata) && questoConsulente[propInIcon] !== "sap-icon://delete") {
                if (questoConsulente[propInIcon] === "sap-icon://time-entry-request" || questoConsulente[propInIconA] === "sap-icon://time-entry-request") {
                    count = count + 0.5;
                } else if (questoConsulente[propInIcon] !== "sap-icon://offsite-work" && questoConsulente[propInIconA] !== "sap-icon://offsite-work") {
                    count = count + 1;
                }
            }
        }
        questoConsulente.giorniScheduling = count;
        var totalCount = 0;
        for (var i = 0; i < this.pmSchedulingModel.getData().schedulazioni.length; i++) {
            totalCount = totalCount + this.pmSchedulingModel.getData().schedulazioni[i].giorniScheduling;
        }
        this.pmSchedulingModel.setProperty("/giorniTotali", totalCount);
        this.pmSchedulingModel.refresh();
    },

    /* INIZIO - funzioni per l'aggiunta di un consulente generico allo staff */
    // funzione per aprire il Dialog per aggiungere il consulente generico
    openGenericConsultant: function (evt) {
        this._genericConsultantDialog = sap.ui.xmlfragment("view.dialog.addGenericConsultant", this);
        var page = this.getView().byId("requestModifySchedulingPage");
        this.getView().addDependent(this._genericConsultantDialog);

        this.loadGenericConsultants()
            .then(_.bind(function (result) {
                    this._genericConsultantDialog.open();
                    this.pmSchedulingModel.refresh();
                }, this),
                _.bind(function (err) {
                    this.pmSchedulingModel.refresh();
                }, this));
    },

    // carico tutti i consulenti generici
    loadGenericConsultants: function () {
        var defer = Q.defer();
        var fSuccess = function (result) {
            for (var i = 0; i < this.pmSchedulingModel.getProperty("/schedulazioni").length; i++) {
                _.remove(result.items, {
                    codConsulente: this.pmSchedulingModel.getProperty("/schedulazioni")[i].codiceConsulente
                });
            }
            this.genericConsultantModel.setData(_.unique(result.items, 'skill'));
            this.console("Lista consulenti generici: " + result.items.length, "success");
            defer.resolve();
        };

        var fError = function (err) {
            sap.m.MessageToast(this._getLocaleText("SKILL_NOT_READ"));
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.getGenericConsSkills.getGenericConsSkills()
            .then(fSuccess, fError);
        return defer.promise;
    },

    // filtro di ricerca delle BU nel dialog 'addGenericConsultant'
    onSearchGenericConsultant: function (oEvent) {
        var value = oEvent.getSource().getValue();
        var params = ["nomeSkill", "descSkill"];
        var filtersArr = [];
        for (var i = 0; i < params.length; i++) {
            filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
        }
        var oFilter = new sap.ui.model.Filter({
            filters: filtersArr,
            and: false
        });
        var ff = sap.ui.getCore().getElementById("listGenCons");
        var oBinding = ff.getBinding("items");
        oBinding.filter([oFilter]);
    },

    // funzione lanciata dal comando 'chiudi' del dialog (addGenericConsultant)
    // chiude il dialog, rimuove eventuali selezioni
    onCancelGenericConsultant: function () {
        if (this._genericConsultantDialog) {
            this.pmSchedulingModel.refresh();
            this._genericConsultantDialog.close();
            this._genericConsultantDialog.destroy(true);
        }
    },

    // funzione che aggiunge alla tabella dei consulenti generici quello selezionato
    onAddGenericConsultant: function (oEvent) {
        var consulentiSchedulati = _.clone(this.pmSchedulingModel.getData().schedulazioni);
        var aContexts = sap.ui.getCore().getElementById("listGenCons").getSelectedItems();

        if (aContexts.length) {
            var arrayReq = [];
            for (var i = 0; i < aContexts.length; i++) {
                var req = {};
                var obj = aContexts[i];
                var findSkill = _.find(this.genericConsultantModel.getData(), {
                    'descSkill': obj.getProperty("description"),
                    'nomeSkill': obj.getProperty("title")
                });

                if (findSkill) {
                    var skillCons = findSkill.skill;
                    req.codSkill = skillCons;
                    req.nomeSkill = obj.getProperty("title");
                    req.descSkill = obj.getProperty("description");
                    req.codCons = findSkill.codConsulente
                    this.pmSchedulingModel.getData().schedulazioni.push(req);
                    arrayReq.push(req);
                    this.insertGenericInTable(arrayReq);
                } else {
                    sap.m.MessageToast.show("errore funzione 'onAddGenericConsultant'");
                    return;
                }
            }
        }
    },

    // funzione che aggiunge allo staff il consulente generico appena creato
    insertGenericInTable: function (input) {
        var fSuccess = function (result) {
            sap.m.MessageToast.show(this._getLocaleText("ADDED_CONSULTANT"));
            this.onCancelGenericConsultant();
            this.readCommessePerPjmSet();

            //chiamata alla tabella stato_consulente_periodo_draft
            var successo = function (resultConsPer) {
                this.console(resultConsPer);
                var risultatiQuestoConsulente = _.find(resultConsPer.items, {
                    'codConsulente': this.input[0].codCons
                });
                //se risultatiQuestoConsulente è vuoto o undefined allora inserisco in tabella
            };

            var errore = function (err) {
                sap.m.MessageToast.show(err);
            };

            successo = _.bind(successo, this);
            errore = _.bind(errore, this);

            var periodo = model.persistence.Storage.session.get("period");

            model.getStatoConsPeriodoDraft.getStatoConsPeriodoDraft(periodo)
                .then(successo, errore);

            jQuery.sap.delayedCall(500, this, function () {
                this.leggiTabella(this.ric);
            });
        };

        var fError = function (err) {
            sap.m.MessageToast.show(err);
            this.onCancelGenericConsultant();
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var req = {};
        var arrayReq = [];
        var obj = input;
        var codCons = input[0].codCons;
        var skill = input[0].codSkill;

        var c = model.persistence.Storage.session.get("commessa");
        req.codStaff = c.codiceStaffCommessa;
        req.codConsulenteStaff = codCons;
        req.codCommessa = c.codCommessa;
        req.codPJMStaff = c.codPjmAssociato;
        req.codSkill = skill;
        req.teamLeader = "NO";
        arrayReq.push(req);

        this.input = input;

        model.insertConsultantInStaff.insert(arrayReq)
            .then(fSuccess, fError);
    },
    /* FINE - funzioni per l'aggiunta di un consulente genrico allo staff */

    // funzione che apre il dialog delle note 'noteDialog'
    onAddNote: function () {
        if (!this._noteDialog) {
            this._noteDialog = sap.ui.xmlfragment("view.dialog.noteDialog", this);
            this.getView().addDependent(this._noteDialog);
        }
        var commessaTitle = "";
        var selectedCommessa = _.find(this.allCommesse, {
            'codCommessa': this.dataForUpdate.codCommessaModifica
        });
        if (selectedCommessa) {
            commessaTitle = selectedCommessa.nomeCommessa + " - " + selectedCommessa.descrizioneCommessa;
        }
        this.noteModel.setProperty("/commessaTitle", commessaTitle);
        this._noteDialog.open();
    },

    // funzione che viene lanciata alla chiusura del dialog delle note
    onAfterCloseNoteDialog: function () {
        this.noteModel.setProperty("/note", undefined);
    },

    // funzione che chiude il dialog delle note
    onCancelNoteDialogPress: function (evt) {
        if (this._noteDialog) {
            this._noteDialog.close();
        }
        if (!model.persistence.Storage.local.get("undoPJM_1")) {
            this.visibleModel.setProperty("/undoButton", false);
        }
    },

    // funzione che salva le note
    onSaveNoteDialogPress: function () {
        var fSuccess = function (res) {
            this.onCancelNoteDialogPress();
            sap.m.MessageToast.show(this._getLocaleText("NOTE_ADDED"));

            this.dataForUpdate = "";
            this.notaConsultant = undefined;
            this.leggiTabella(this.ric);
        };

        var fError = function (err) {
            sap.m.messageToast.show(this._getLocaleText("ERROR_NOTE_UPDATE"));
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if (this.notaConsultant === this.noteModel.getProperty("/note")) {
            this.onCancelNoteDialogPress();
        } else {
            this.dataForUpdate.noteModifica = this.noteModel.getProperty("/note") ? this.noteModel.getProperty("/note") : "";
            model.SchedulingModifyRequests.updateNote(parseInt(this.dataForUpdate.idReqModificaInt), this.dataForUpdate.noteModifica)
                .then(fSuccess, fError);
        }
    },

    // funzione per la modifica delle note
    onNotePress: function (evt) {
        var nota = evt.getSource().data("note");
        var jobName = evt.getSource().data("job");
        var job = jobName === "feri" ? "AA00000000" : _.find(this.getView().getModel("pmSchedulingModel").getProperty("/commesse"), {
            nomeCommessa: jobName
        });
        var consultant = evt.getSource().getBindingContext("pmSchedulingModel").getObject();
        this.notaConsultant = consultant[nota];
        this.noteModel.setProperty("/note", consultant[nota]);
        var codReq = nota.substring(4);
        var propReq = "req" + codReq;

        this.dataForUpdate = {
            "idScambio": 0,
            "codiceConsulenteModifica": consultant.codiceConsulente,
            "dataRichiestaModifica": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "codCommessaModifica": job.codCommessa,
            "codAttivitaRichiestaModifica": "",
            "idPeriodoSchedModifica": this.period,
            "codPjmModifica": this.user.codice_consulente,
            "dataInserimentoModifica": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "noteModifica": "",
            "statoReqModifica": "PROPOSED",
            "giornoPeriodoModifica": codReq.indexOf("a") >= 0 ? parseInt(codReq.substr(0, codReq.length - 1)) : parseInt(codReq),
            "codCommessaOldModifica": "0000000000",
            "idReqModificaInt": consultant[propReq] ? consultant[propReq].substring(1) : ""
        };
        if (jobName === "feri") {
            this.noteModel.setProperty("/editable", false);
        } else {
            this.noteModel.setProperty("/editable", true);
        }
        this.onAddNote();
    },

    closeProjectPress: function () {
        var fSuccess = function () {
            this.svuotaTabella();
            this.svuotaSelect();
            this.oDialogTable.setVisible(false);
            this.readPeriods();
            sap.m.MessageToast.show(this._getLocaleText("TUTTI_PERIODI_CHIUSI"));
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore lettura XSJS 'scritturaProfiloCommessaPeriodo.xsjs'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.ProfiloCommessaPeriodo.changeStato(this.commessaScelta, this.period, "CHIUSO")
            .then(fSuccess, fError);
    },

    /* INIZIO funzioni inserimento multiplo */
    // funzione che apre il dialog 'insertMultiDay'
    onPressMultiDay: function (evt) {
        this.multiDayDialog = sap.ui.xmlfragment("view.dialog.insertMultiDay", this);
        var page = this.getView().byId("requestModifySchedulingPage");

        sap.ui.getCore().getElementById("delMulti").setProperty("visible", false);
        sap.ui.getCore().getElementById("idButtonCopyFrom").setProperty("visible", false);

        page.addDependent(this.multiDayDialog);

        var table = this.getView().byId("idSchedulazioniModifyRequestTable");
        var giornoPrimaColonna = table.getColumns()[1].getAggregation("label").getProperty("text");
        this.console(giornoPrimaColonna.split(" ")[1], "success");
        this.numPrimoGiornoMese = parseInt(giornoPrimaColonna.split(" ")[0]);
        // controllare se giornoPrimaColonna è LUN

        //////////////////////////////////////
        var id = evt.getSource().getId();
        var rowId = evt.getSource().getBindingContext("pmSchedulingModel").getPath().split("/")[2];
        this.console(rowId);

        this.schedCons = this.pmSchedulingModel.getData().schedulazioni[rowId];
        model.persistence.Storage.session.save("schedCons", this.schedCons);
        this.setDay();
        this.insertMultiModel.setProperty("/name", this.schedCons.nomeConsulente);

        this.attivitaScelta = undefined;
        this.selectLun = undefined;
        this.selectMar = undefined;
        this.selectMer = undefined;
        this.selectGio = undefined;
        this.selectVen = undefined;
        this.selectSab = undefined;
        this.selectDom = undefined;
        this.selectTutto = undefined;
        this.selectSoloCommessa = undefined;

        this.multiDayDialog.open();
    },

    // funzione che disabilita i giorni della settimana se trova giorni schedulati
    setDay: function () {
        var arrayTemp = [];
        for (var e = 1; e < 37; e++) {
            var gg = "gg" + e;
            if (this.schedCons[gg]) {
                arrayTemp.push(e);
            }
        }

        for (var q = 0; q < arrayTemp.length; q++) {
            for (var k = 1; k <= 7; k++) {
                for (var i = 0; i < 5; i++) {
                    if (arrayTemp[q] === k + (i * 7)) {
                        if (k === 1) {
                            sap.ui.getCore().getElementById("lun").setEnabled(false);
                            sap.ui.getCore().getElementById("lun").setProperty("selected", false);
                        } else if (k === 2) {
                            sap.ui.getCore().getElementById("mar").setEnabled(false);
                            sap.ui.getCore().getElementById("mar").setProperty("selected", false);
                        } else if (k === 3) {
                            sap.ui.getCore().getElementById("mer").setEnabled(false);
                            sap.ui.getCore().getElementById("mer").setProperty("selected", false);
                        } else if (k === 4) {
                            sap.ui.getCore().getElementById("gio").setEnabled(false);
                            sap.ui.getCore().getElementById("gio").setProperty("selected", false);
                        } else if (k === 5) {
                            sap.ui.getCore().getElementById("ven").setEnabled(false);
                            sap.ui.getCore().getElementById("ven").setProperty("selected", false);
                        } else if (k === 6) {
                            sap.ui.getCore().getElementById("sab").setEnabled(false);
                            sap.ui.getCore().getElementById("sab").setProperty("selected", false);
                        } else {
                            sap.ui.getCore().getElementById("dom").setEnabled(false);
                            sap.ui.getCore().getElementById("dom").setProperty("selected", false);
                        }
                    }
                }
            }
        }
        this.multiModel.setProperty("/visibleSceltaCancella", false);
    },

    // funzione che chiude il dialog e ripristina i valori di default
    onPressDialogMultiDayClose: function () {
        sap.ui.getCore().getElementById("lun").setSelected(false);
        sap.ui.getCore().getElementById("mar").setSelected(false);
        sap.ui.getCore().getElementById("mer").setSelected(false);
        sap.ui.getCore().getElementById("gio").setSelected(false);
        sap.ui.getCore().getElementById("ven").setSelected(false);
        sap.ui.getCore().getElementById("sab").setSelected(false);
        sap.ui.getCore().getElementById("dom").setSelected(false);

        this.multiDayDialog.close();
        this.multiDayDialog.destroy(true);
        model.persistence.Storage.session.remove("schedCons");
    },

    // funzione che conferma l'inserimento massivo 
    onPressDialogMultiDayOK: function () {
        if (!this.attivitaScelta) {
            sap.m.MessageToast.show(this._getLocaleText("SELECT_ATTIVITA"));
            return;
        }
        if (!this.selectLun && !this.selectMar && !this.selectMer && !this.selectGio && !this.selectVen && !this.selectSab && !this.selectDom) {
            sap.m.MessageToast.show(this._getLocaleText("SELECT_DAY"));
            return;
        }
        if (this.attivitaScelta === "del" && !this.selectTutto && !this.selectSoloCommessa) {
            sap.m.MessageToast.show(this._getLocaleText("SELECT_CHOICE"));
            return;
        }

        this.numPeriodoMese = parseInt(model.persistence.Storage.session.get("period"));
        var annoOR = model.persistence.Storage.session.get("periodSelected").annoPeriodo;
        var anno;
        var periodoMese = model.persistence.Storage.session.get("periodSelected").mesePeriodo;
        if (periodoMese < 10) {
            periodoMese = "0" + periodoMese;
        }
        var giorni = this.numColonne - 1;
        var inserimenti = [];

        var note = sap.ui.getCore().getElementById("idNoteMassivo").getValue();

        if (this.attivitaScelta !== "del") {
            var dataToSend = {
                "codConsulente": this.schedCons.codiceConsulente,
                "dataReqPeriodo": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
                "idCommessa": this.getView().byId("idSelectCommessa").getSelectedKey(),
                "codCommessaOldModifica": "0000000000",
                "codAttivitaRichiesta": this.attivitaScelta,
                "idPeriodoSchedulingRic": this.numPeriodoMese,
                "codPjm": this.user.codice_consulente,
                "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
                "note": note ? note : "",
                "statoReq": "PROPOSED",
                "giorno": 0,
                idReqModificaInt: 0,
                idReqOriginale: 0,
                idScambio: 0
            };

            var primoGG;
            var iteratore;
            var i = 0;
            var data;
            var lastDayOfMonth;
            var mesePlus;
            var giornoPlus;
            if (this.selectLun) {
                for (i = 0; i < giorni / 7; i++) {
                    primoGG = this.numPrimoGiornoMese + (i * 7);
                    if (primoGG < 10) {
                        primoGG = "0" + primoGG;
                    }
                    iteratore = _.clone(dataToSend);
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12) {
                            anno = parseInt(anno) + 1;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (1 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }
            if (this.selectMar) {
                for (i = 0; i < giorni / 7; i++) {
                    primoGG = this.numPrimoGiornoMese + 1 + (i * 7);
                    iteratore = _.clone(dataToSend);
                    if ((primoGG) < 10) {
                        primoGG = "0" + primoGG;
                    }
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12) {
                            anno = parseInt(anno) + 1;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (2 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }
            if (this.selectMer) {
                for (i = 0; i < giorni / 7; i++) {
                    primoGG = this.numPrimoGiornoMese + 2 + (i * 7);
                    iteratore = _.clone(dataToSend);
                    if (primoGG < 10) {
                        primoGG = "0" + primoGG;
                    }
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12) {
                            anno = parseInt(anno) + 1;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (3 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }
            if (this.selectGio) {
                for (i = 0; i < giorni / 7; i++) {
                    iteratore = _.clone(dataToSend);
                    primoGG = this.numPrimoGiornoMese + 3 + (i * 7);
                    if (primoGG < 10) {
                        primoGG = "0" + primoGG;
                    }
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12) {
                            anno = parseInt(anno) + 1;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (4 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }
            if (this.selectVen) {
                for (i = 0; i < giorni / 7; i++) {
                    iteratore = _.clone(dataToSend);
                    primoGG = this.numPrimoGiornoMese + 4 + (i * 7);
                    if (primoGG < 10) {
                        primoGG = "0" + primoGG;
                    }
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12) {
                            anno = parseInt(anno) + 1;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (5 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }
            if (this.selectSab) {
                for (i = 0; i < giorni / 7; i++) {
                    iteratore = _.clone(dataToSend);
                    primoGG = this.numPrimoGiornoMese + 5 + (i * 7);
                    if (primoGG < 10) {
                        primoGG = "0" + primoGG;
                    }
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12) {
                            anno = parseInt(anno) + 1;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (6 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }
            if (this.selectDom) {
                for (i = 0; i < giorni / 7; i++) {
                    iteratore = _.clone(dataToSend);
                    primoGG = this.numPrimoGiornoMese + 6 + (i * 7);
                    if (primoGG < 10) {
                        primoGG = "0" + primoGG;
                    }
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12 && !this.annoTrue) {
                            anno = parseInt(anno) + 1;
                            this.annoTrue = true;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (7 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }

            var fControlloError = function (err) {
                sap.m.MessageToast.show(err);
            };

            var fControlloSuccess = function (result) {
                var scriviRichieste = true;
                for (var i = 0; i < result.length; i++) {
                    var schedulazione = _.find(inserimenti, {
                        'giorno': result[i].giornoPeriodo
                    });
                    if (schedulazione) {
                        scriviRichieste = false;
                        break;
                    }
                }
                if (scriviRichieste) {
                    this.saveMultipleRequest(inserimenti);
                } else {
                    sap.m.MessageBox.show(
                        this._getLocaleText("SHOW_SCHEDULE_CONFLICTS_WITH_OTHER_PJM"), {
                            icon: sap.m.MessageBox.Icon.INFORMATION,
                            title: this._getLocaleText("SCHEDULE_CONFLICTS_TITLE"),
                            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                            onClose: _.bind(function (oAction) {
                                if (oAction === "YES") {
                                    this.multipleRequest = true;
                                    var req = {};
                                    var commessaScelta = model.persistence.Storage.session.get("commessa");
                                    req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
                                    req.codPjm = this.user.codice_consulente;
                                    req.codConsulente = inserimenti[0].codConsulente;
                                    this.dataModelConsultant = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                                        codiceConsulente: inserimenti[0].codConsulente
                                    });
                                    this._leggiTabellaSchedulingConsulenti(req);
                                    if (!this.schedConsultantDialog) {
                                        this.schedConsultantDialog = sap.ui.xmlfragment("view.dialog.viewConsultantScheduling", this);
                                        this.getView().addDependent(this.schedConsultantDialog);
                                    }
                                    this.schedConsultantDialog.open();
                                    this.inserimentiBackup = inserimenti;
                                } else {
                                    this.saveMultipleRequest(inserimenti);
                                }
                            }, this),
                        }
                    );
                }
            };

            fControlloSuccess = _.bind(fControlloSuccess, this);
            fControlloError = _.bind(fControlloError, this);

            // faccio un controllo per vedere se sto per inserire delle richieste dove
            // già altri consulenti ne hanno messe per quello stesso consulente
            if (inserimenti.length > 0) {
                this.consulenteSelezionato = inserimenti[0].codConsulente;
                model.tabellaScheduling.readSchedulazioni(inserimenti[0].codPjm, inserimenti[0].codConsulente, inserimenti[0].idPeriodoSchedulingRic)
                    .then(fControlloSuccess, fControlloError);
            }
        } else {
            var arrayToRemove = [];
            var r = 0;
            var gg;
            var req;
            if (this.selectLun) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (1 + (r * 7));
                    req = "req" + (1 + (r * 7));

                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== "feri") {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }
            if (this.selectMar) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (2 + (r * 7));
                    req = "req" + (2 + (r * 7));
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== "feri") {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }
            if (this.selectMer) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (3 + (r * 7));
                    req = "req" + (3 + (r * 7));
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== "feri") {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }
            if (this.selectGio) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (4 + (r * 7));
                    req = "req" + (4 + (r * 7));
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== "feri") {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }
            if (this.selectVen) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (5 + (r * 7));
                    req = "req" + (5 + (r * 7));
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== "feri") {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }
            if (this.selectSab) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (6 + (r * 7));
                    req = "req" + (6 + (r * 7));
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== "feri") {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }
            if (this.selectDom) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (7 + (r * 7));
                    req = "req" + (7 + (r * 7));
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== "feri") {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }

            var fSuccess = function (result) {
                sap.m.MessageToast.show(this._getLocaleText("CANCELLAZIONE_OK"));
                this.leggiTabella(this.ric);
            };

            var fError = function (err) {
                sap.m.MessageToast.show(err);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            var arrayModificheDaProporreInDelete = [];
            var arrayModificheDaCancellare = _.filter(arrayToRemove, function (item) {
                if (typeof (item) === "string" && item.indexOf("M") > -1) {
                    return item;
                } else {
                    arrayModificheDaProporreInDelete.push(item);
                }
            });

            if (arrayModificheDaCancellare.length > 0 && arrayModificheDaProporreInDelete === 0) {
                model.SchedulingModifyRequests.deleteMultiRequest(arrayModificheDaCancellare)
                    .then(fSuccess, fError);
                //            } else if (arrayModificheDaCancellare.length = 0 && arrayModificheDaProporreInDelete.length > 0) {
                //                model.SchedulingModifyRequests.deleteMultiRequest(arrayToRemove)
                //                    .then(fSuccess, fError);
                //            } else if (arrayModificheDaCancellare.length > 0 && arrayModificheDaProporreInDelete.length > 0) {
                //                model.SchedulingModifyRequests.deleteMultiRequest(arrayToRemove)
                //                    .then(fSuccess, fError);
            } else {
                sap.m.MessageToast.show(this._getLocaleText("NIENTE_DA_CANCELLARE"));
            }
        }

        this.multiDayDialog.close();
        this.multiDayDialog.destroy(true);
        model.persistence.Storage.session.remove("schedCons");
    },

    // funzione di inserimento multiplo in tabella
    saveMultipleRequest: function (inserimenti) {
        this.inserimenti = inserimenti;
        var fSuccess = function (result) {
            sap.m.MessageToast.show(this._getLocaleText("SCRITTURA_OK"));
            this.leggiTabella(this.ric);
        };

        var fError = function (err) {
            sap.m.MessageToast.show(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.SchedulingModifyRequests.sendMultiRequest(inserimenti)
            .then(fSuccess, fError);
    },

    onPressAssign: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "complete";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressHalf: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "time-entry-request";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressConsu: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "customer-and-contacts";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressNoConsu: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "employee-rejections";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressCust: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "collaborate";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressNoCust: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "offsite-work";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressNoConfCust: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "role";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressAbr: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "globe";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressDel: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            this.attivitaScelta = "del";
            sap.ui.getCore().getElementById("lun").setEnabled(true);
            sap.ui.getCore().getElementById("mar").setEnabled(true);
            sap.ui.getCore().getElementById("mer").setEnabled(true);
            sap.ui.getCore().getElementById("gio").setEnabled(true);
            sap.ui.getCore().getElementById("ven").setEnabled(true);
            sap.ui.getCore().getElementById("sab").setEnabled(true);
            sap.ui.getCore().getElementById("dom").setEnabled(true);
            this.multiModel.setProperty("/visibleSceltaCancella", true);
        } else {
            this.attivitaScelta = undefined;
            this.multiModel.setProperty("/visibleSceltaCancella", false);
            this.setDay();
        }
    },

    onSelectLun: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectLun = true;
        } else {
            this.selectLun = false;
        }
    },

    onSelectMar: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectMar = true;
        } else {
            this.selectMar = false;
        }
    },

    onSelectMer: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectMer = true;
        } else {
            this.selectMer = false;
        }
    },

    onSelectGio: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectGio = true;
        } else {
            this.selectGio = false;
        }
    },

    onSelectVen: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectVen = true;
        } else {
            this.selectVen = false;
        }
    },

    onSelectSab: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectSab = true;
        } else {
            this.selectSab = false;
        }
    },

    onSelectDom: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectDom = true;
        } else {
            this.selectDom = false;
        }
    },

    onSelectTutto: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectTutto = true;
            if (sap.ui.getCore().getElementById("soloCommessa").getProperty("selected")) {
                sap.ui.getCore().getElementById("soloCommessa").setProperty("selected", false);
            }
        } else {
            this.selectTutto = false;
        }
    },

    onSelectSoloCommessa: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectSoloCommessa = true;
            if (sap.ui.getCore().getElementById("tutto").getProperty("selected")) {
                sap.ui.getCore().getElementById("tutto").setProperty("selected", false);
            }
        } else {
            this.selectSoloCommessa = false;
        }
    },

    onPressDialogCopyFrom: function () {
        this.onPressDialogMultiDayClose();
        this.copyActivitiesDialog = sap.ui.xmlfragment("view.dialog.copyActivitiesDialog", this);
        var page = this.getView().byId("requestModifySchedulingPage");
        page.addDependent(this.copyActivitiesDialog);
        var cloneModel = _.clone(this.pmSchedulingModel.getData().schedulazioni);
        _.remove(cloneModel, this.schedCons);
        _.remove(cloneModel, {
            'giorniScheduling': 0
        })
        this.multiModel.setData(cloneModel);
        this.multiModel.setProperty("/visibleSceltaCancellaCopyActivities", true);
        this.multiModel.setProperty("/visibleSceltaCancellaCopyActivitiesInv", false);
        this.copyActivitiesDialog.open();
    },

    onDialogCopyActivitiesClose: function () {
        this.copyActivitiesDialog.open();
        this.copyActivitiesDialog.destroy(true);
    },

    onDialogCopyActivitiesOK: function (evt) {
        if (!this.selectTutto && !this.selectSoloCommessa) {
            this.multiModel.setProperty("/visibleSceltaCancellaCopyActivities", false);
            this.multiModel.setProperty("/visibleSceltaCancellaCopyActivitiesInv", true);
        } else {
            var aContexts = sap.ui.getCore().getElementById("listaConsulenti").getSelectedItems();
            var consSelezionato = aContexts[0].getProperty("description");
            var allInfoConsSelezionato = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                'codiceConsulente': consSelezionato
            });
            var numPeriodoMese = parseInt(model.persistence.Storage.session.get("period"));
            var dataToSend = {
                "codConsulente": this.schedCons.codiceConsulente,
                "idPeriodoSchedulingRic": numPeriodoMese,
                "codPjm": this.user.codice_consulente,
                "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
                "note": "",
                "statoReq": "CONFIRMED",
                "id_req": ""
            };
            var inserimenti = [];

            for (var i = 1; i < 36; i++) {
                var iteratore = _.clone(dataToSend);
                var gg = "gg" + i;
                var ggIcon = "ggIcon" + i;
                var ggReq = "ggReq" + i;
                var ggNota = "nota" + i;
                if (allInfoConsSelezionato[gg] && !this.schedCons[gg]) {
                    var commessa = _.find(this.pmSchedulingModel.getData().commesse, {
                        'nomeCommessa': allInfoConsSelezionato[gg]
                    });
                    if (commessa !== undefined) {
                        var codCommessa = commessa.codCommessa;
                        var icona = allInfoConsSelezionato[ggIcon].split("//")[1];
                        iteratore.dataReqPeriodo = allInfoConsSelezionato[ggReq];
                        iteratore.note = allInfoConsSelezionato[ggNota];
                        iteratore.idCommessa = codCommessa;
                        iteratore.codAttivitaRichiesta = icona;
                        iteratore.giorno = i;

                        if (!this.selectSoloCommessa) {
                            inserimenti.push(iteratore);
                        } else {
                            if (commessa.nomeCommessa === this.getView().byId("idSelectCommessa").getValue()) {
                                inserimenti.push(iteratore);
                            }
                        }

                    }
                }
            }

            var fControlloSuccess = function (result) {
                var scriviRichieste = true;
                for (var i = 0; i < result.length; i++) {
                    var schedulazione = _.find(inserimenti, {
                        'giorno': result[i].giornoPeriodo
                    });
                    if (schedulazione) {
                        scriviRichieste = false;
                        break;
                    }
                }
                if (scriviRichieste) {
                    this.saveMultipleRequest(inserimenti);
                } else {
                    sap.m.MessageBox.show(
                        this._getLocaleText("SHOW_SCHEDULE_CONFLICTS_WITH_OTHER_PJM"), {
                            icon: sap.m.MessageBox.Icon.INFORMATION,
                            title: this._getLocaleText("SCHEDULE_CONFLICTS_TITLE"),
                            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                            onClose: _.bind(function (oAction) {
                                if (oAction === "YES") {
                                    this.multipleRequest = true;
                                    var req = {};
                                    req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
                                    req.idPJM = this.user.codice_consulente;
                                    req.codConsulente = inserimenti[0].codConsulente;
                                    this.dataModelConsultant = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                                        codiceConsulente: inserimenti[0].codConsulente
                                    });
                                    this._leggiTabellaSchedulingConsulenti(req);
                                    if (!this.schedConsultantDialog) {
                                        this.schedConsultantDialog = sap.ui.xmlfragment("view.dialog.viewConsultantScheduling", this);
                                        this.getView().addDependent(this.schedConsultantDialog);
                                    }
                                    this.schedConsultantDialog.open();
                                    this.inserimentiBackup = inserimenti;
                                } else {
                                    this.saveMultipleRequest(inserimenti);
                                }
                            }, this),
                        }
                    );
                }
            };

            var fError = function (err) {
                sap.m.MessageToast.show(err);
            };

            fControlloSuccess = _.bind(fControlloSuccess, this);
            fError = _.bind(fError, this);

            // faccio un controllo per vedere se sto per inserire delle richieste dove
            // già altri consulenti ne hanno messe per quello stesso consulente
            if (inserimenti.length > 0) {
                this.consulenteSelezionato = inserimenti[0].codConsulente;
                model.tabellaScheduling.readSchedulazioni(inserimenti[0].codPjm, inserimenti[0].codConsulente, inserimenti[0].idPeriodoSchedulingRic)
                    .then(fControlloSuccess, fError);
            }

            this.copyActivitiesDialog.open();
            this.copyActivitiesDialog.destroy(true);
        }
    },
    /* FINE funzioni inserimento multiplo */

    /* DOWNOLAD EXCEL */
    onDownloadExcelPress: function (evt) {
        var oButton = evt.getSource();

        if (!this._actionSheetExcel) {
            this._actionSheetExcel = sap.ui.xmlfragment(
                "view.fragment.ActionSheetExcel",
                this
            );
            this.getView().addDependent(this._actionSheet);
        }
        this._actionSheetExcel.setModel(this.getView().getModel("i18n"), "i18n");
        this._actionSheetExcel.openBy(oButton);
    },

    _scegliCommessa: function () {
        if (!this._dialogCommessaExcel) {
            this._dialogCommessaExcel = sap.ui.xmlfragment(
                "view.dialog.selectCommessaExcelDialog",
                this
            );
            this.getView().addDependent(this._dialogCommessaExcel);
        }
        this._dialogCommessaExcel.setModel(this.getView().getModel("i18n"), "i18n");
        this._dialogCommessaExcel.open();
    },

    _onSelectionCommesse: function (evt) {
        var oList = evt.getSource();
        this.selezioneCommesse = oList.getSelectedContexts(true);
    },

    handleCancelSelectCommessaExcelDialog: function () {
        this._dialogCommessaExcel.close();
    },

    handleConfirmSelectCommessaExcelDialog: function () {
        if (this.selezioneCommesse && this.selezioneCommesse.length) {
            var arrayReq = [];
            for (var i = 0; i < this.selezioneCommesse.length; i++) {
                var path = parseInt(this.selezioneCommesse[i].getPath().split("/")[2]);
                arrayReq.push(this.selezioneCommesse[i].getModel().getData().commesse[path]);
            }

            var ric = {};
            ric.cod_consulente = (this.user).codice_consulente;
            ric.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));

            var fSuccessAll = function (results) {
                var schedulazioni = results.resultOrdinato;
                var arr = [];
                for (var t = 0; t < arrayReq.length; t++) {
                    for (var i = 0; i < this.commessePerConsulente.length; i++) {
                        if ((_.find(this.commessePerConsulente[i].commesse, {
                                nomeCommessa: arrayReq[t].nomeCommessa
                            }))) {
                            arr.push(_.find(this.commessePerConsulente[i].commesse, {
                                nomeCommessa: arrayReq[t].nomeCommessa
                            }));
                        }
                    }
                }
                var group = _.groupBy(arr, 'codConsulente');
                var sched = [];
                for (var prop in group) {
                    sched.push(_.find(schedulazioni, {
                        codiceConsulente: prop
                    }));
                }
                this.excelModel.setProperty("/schedulazioni", sched);
                this.oDialogTable.setBusy(false);
                this._excelCommesseSelezionate(arrayReq);
            };

            var fError = function (err) {
                this.console(err, "error");
                this._enterErrorFunction("error", "Errore", "Errore lettura tabella: " + err);
            };

            fSuccessAll = _.bind(fSuccessAll, this);
            fError = _.bind(fError, this);

            this.oDialogTable.setBusy(true);
            model.tabellaScheduling.readPjmAllRequests(ric)
                .then(fSuccessAll, fError);
        } else {
            sap.m.MessageToast.show(this._getLocaleText("NO_PROJECT_CHOSEN"));
        }
        this._dialogCommessaExcel.getAggregation("content")[0].removeSelections();
        this._dialogCommessaExcel.close();
    },

    _excelCommesseSelezionate: function (commesse) {
        var columns = [];
        var item = [];
        var rows = [];
        var column = {};
        var days = this.pmSchedulingModel.getProperty("/days");
        var daysLength = days.length;
        var schedulazioni = this.excelModel.getProperty("/schedulazioni");
        var schedulazioniLength = schedulazioni.length;
        for (var i = -1; i < daysLength; i++) {
            if (i === -1) {
                column = {
                    "name": this._getLocaleText("CONSULENTI")
                };
            } else {
                column = {
                    "name": days[i]
                };
            }
            columns.push(column);
        }

        for (var j = 0; j < schedulazioniLength; j++) {
            var row = [];
            for (var i = -1; i < daysLength; i++) {
                var item = "";
                if (i === -1) {
                    item = schedulazioni[j].nomeConsulente + ": " + schedulazioni[j].giorniScheduling;
                } else {
                    var index = i + 1;
                    var property = "gg" + index;
                    var propertyA = "gg" + index + "a";
                    var icon = "ggIcon" + index;
                    var iconA = "ggIcon" + index + "a";

                    var css = "";
                    switch (schedulazioni[j][icon]) {
                        case "sap-icon://complete":
                            css = "EXassign";
                            break;
                        case "sap-icon://time-entry-request":
                            css = "EXhalfDay";
                            break;
                        case "sap-icon://customer-and-contacts":
                            css = "EXwCons";
                            break;
                        case "sap-icon://employee-rejections":
                            css = "EXwNonCons";
                            break;
                        case "sap-icon://collaborate":
                            css = "EXwCust";
                            break;
                        case "sap-icon://offsite-work":
                            css = "EXwNonCust";
                            break;
                        case "sap-icon://role":
                            css = "EXnotConfirmed";
                            break;
                        case "sap-icon://globe":
                            css = "EXabroad";
                            break;
                    }
                    // la seguente proprietà mi serve per capire se nello stesso giorno un consulente ha due mezze giornate per la stessa commessa
                    var entratoInRapportino = false;
                    if (_.find(commesse, {
                            nomeCommessa: schedulazioni[j][property]
                        }) && schedulazioni[j][icon] !== "sap-icon://time-entry-request") {
                        item = schedulazioni[j][property] + "_" + css;
                    } else if (_.find(commesse, {
                            nomeCommessa: schedulazioni[j][property]
                        }) && schedulazioni[j][icon] === "sap-icon://time-entry-request") {
                        item = schedulazioni[j][property] + "_" + css;
                    }
                }
                row.push(item);
            }
            rows.push(row);
        }

        this.completeModel = new sap.ui.model.json.JSONModel();
        this.completeModel.setData({
            rows: rows,
            columns: columns
        });

        if (!this.excelDialog) {
            this.excelDialog = sap.ui.xmlfragment("view.dialog.excelDownload", this);
            this.getView().addDependent(this.excelDialog);
        }
        this.excelDialog.open();
    },

    _onDownloadExcelPress: function () {
        var commessaScelta = this.getView().byId("idSelectCommessa").getSelectedItem().mProperties.text;
        var columns = [];
        var item = [];
        var rows = [];
        var column = {};
        var days = this.pmSchedulingModel.getProperty("/days");
        var daysLength = days.length;
        var schedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni");
        var schedulazioniLength = schedulazioni.length;
        for (var i = -1; i < daysLength; i++) {
            if (i === -1) {
                column = {
                    "name": this._getLocaleText("CONSULENTI")
                };
            } else {
                column = {
                    "name": days[i]
                };
            }
            columns.push(column);
        }

        for (var j = 0; j < schedulazioniLength; j++) {
            var row = [];
            for (var i = -1; i < daysLength; i++) {
                var item = "";
                if (i === -1) {
                    item = schedulazioni[j].nomeConsulente + ": " + schedulazioni[j].giorniScheduling;
                } else {
                    var index = i + 1;
                    var property = "gg" + index;
                    var propertyA = "gg" + index + "a";
                    var icon = "ggIcon" + index;
                    var iconA = "ggIcon" + index + "a";

                    var css = "";
                    switch (schedulazioni[j][icon]) {
                        case "sap-icon://complete":
                            css = "EXassign";
                            break;
                        case "sap-icon://time-entry-request":
                            css = "EXhalfDay";
                            break;
                        case "sap-icon://customer-and-contacts":
                            css = "EXwCons";
                            break;
                        case "sap-icon://employee-rejections":
                            css = "EXwNonCons";
                            break;
                        case "sap-icon://collaborate":
                            css = "EXwCust";
                            break;
                        case "sap-icon://offsite-work":
                            css = "EXwNonCust";
                            break;
                        case "sap-icon://role":
                            css = "EXnotConfirmed";
                            break;
                        case "sap-icon://globe":
                            css = "EXabroad";
                            break;
                    }

                    // la seguente proprietà mi serve per capire se nello stesso giorno un consulente ha due mezze giornate per la stessa commessa
                    var entratoInRapportino = false;
                    if (schedulazioni[j][property] === commessaScelta && schedulazioni[j][icon] !== "sap-icon://time-entry-request") {
                        item = "1_" + css;
                    } else if (schedulazioni[j][property] === commessaScelta && schedulazioni[j][icon] === "sap-icon://time-entry-request") {
                        item = "0.5_" + css;
                    }
                }
                row.push(item);
            }
            rows.push(row);
        }

        this.completeModel = new sap.ui.model.json.JSONModel();
        this.completeModel.setData({
            rows: rows,
            columns: columns
        });

        if (!this.excelDialog) {
            this.excelDialog = sap.ui.xmlfragment("view.dialog.excelDownload", this);
            this.getView().addDependent(this.excelDialog);
        }
        this.excelDialog.open();
    },

    afterExcelDialog: function () {
        var activityForm = sap.ui.getCore().getElementById("idExcelDialog");
        if (this.completeList) {
            activityForm.removeContent(this.completeList);
            this.completeList.destroy();
        }
        this.completeList = new sap.m.List({
            id: "listaSchedulazioni",
            showSeparators: "All"
        });

        var columns = this.completeModel.getData().columns;

        for (var i = 0; i < columns.length; i++) {
            var minWidth = "15px";
            if (i === 0) {
                minWidth = "50px";
            }
            this.completeList.addColumn(new sap.m.Column({
                width: minWidth,
                hAlign: "Left",
                vAlign: "Middle",
                header: new sap.m.Label({
                    text: columns[i].name,
                    design: sap.m.LabelDesign.Bold
                })
            }));
        }

        var rows = this.completeModel.getData().rows;
        for (var j = 0; j < rows.length; j++) {
            var item = new sap.m.ColumnListItem({
                width: "100%"
            });
            for (var k = 0; k < rows[j].length; k++) {
                if (rows[j][k] !== "") {
                    var tx1 = rows[j][k].split("_")[0];
                    var tx2 = rows[j][k].split("_")[1];
                    item.addCell(new sap.m.Text({
                        text: tx1
                    }));
                    item.getCells()[k].addStyleClass(tx2);
                } else {
                    item.addCell(new sap.m.Text({
                        text: rows[j][k]
                    }));
                }
            }
            this.completeList.addItem(item);
        }
        activityForm.addContent(this.completeList);

        if (this.legend) {
            activityForm.removeContent(this.legend);
            this.legend.destroy();
        }
        this.legend = new sap.m.List({
            id: "legendaSchedulazioni",
            showSeparators: "All"
        });

        for (var i = 0; i < 8; i++) {
            var minWidth = "12.50%";
            this.legend.addColumn(new sap.m.Column({
                width: minWidth,
                hAlign: "Center",
                vAlign: "Middle",
            }));
        }

        var item = new sap.m.ColumnListItem({
            width: "100%"
        });
        item.addCell(new sap.m.Text({
            text: "Assegnata"
        }));
        item.getCells()[0].addStyleClass("EXassign");
        item.addCell(new sap.m.Text({
            text: "Mezza giornata"
        }));
        item.getCells()[1].addStyleClass("EXhalfDay");
        item.addCell(new sap.m.Text({
            text: "Concomitanza"
        }));
        item.getCells()[2].addStyleClass("EXwCons");
        item.addCell(new sap.m.Text({
            text: "Non concomitanza"
        }));
        item.getCells()[3].addStyleClass("EXwNonCons");

        item.addCell(new sap.m.Text({
            text: "Fissata"
        }));
        item.getCells()[4].addStyleClass("EXwCust");
        item.addCell(new sap.m.Text({
            text: "Non schedulabile"
        }));
        item.getCells()[5].addStyleClass("EXwNonCust");
        item.addCell(new sap.m.Text({
            text: "Non confermata"
        }));
        item.getCells()[6].addStyleClass("EXnotConfirmed");
        item.addCell(new sap.m.Text({
            text: "Estero"
        }));
        item.getCells()[7].addStyleClass("EXabroad");
        this.legend.addItem(item);
        activityForm.addContent(this.legend);
    },

    onDownloadDialogPress: function (oEvent) {
        var oButton = oEvent.getSource();
        // create action sheet only once
        if (!this._fileExtensionActionSheet) {
            this._fileExtensionActionSheet = sap.ui.xmlfragment(
                "view.fragment.ChooseFileExtension",
                this
            );
            this.getView().addDependent(this._fileExtensionActionSheet);
        }
        this._fileExtensionActionSheet.openBy(oButton);
    },

    handleFileExtensionChoice: function (evt) {
        var oButton = evt.getSource();
        var text = oButton.getText();
        switch (text) {
            case ".csv":
                this.onDownloadDialogCSVPress();
                break;
            case ".html":
                this.createHTMLTable(".html");
                break;
            case ".xls":
                this.createHTMLTable(".xls");
                break;
        }
    },

    onDownloadDialogCSVPress: function () {
        var commessaScelta = this.getView().byId("idSelectCommessa").getSelectedItem().mProperties.text;
        this.oDialog.setBusy(true);
        var tableId = this.completeList;
        var oModel = this.completeModel;
        utils.exportCsv.exportToExcel(tableId, oModel, "Scheduling " + commessaScelta);
        this.excelDialog.close();
        this.oDialog.setBusy(false);
    },

    createHTMLTable: function (extension) {
        var commessaScelta = this.getView().byId("idSelectCommessa").getSelectedItem().mProperties.text;
        var tab_text = "<table width='100%' border='2px'><tr bgcolor='#87AFC6'>";
        var textRange;
        var j = 0;
        var tab = document.getElementById('listaSchedulazioni-listUl'); // id of table

        for (j = 0; j < tab.rows.length; j++) {
            var tabHeader = tab.rows[j];
            var childrens;
            var children;

            childrens = tabHeader.childNodes;
            this.numeroColonne = childrens.length;
            if (tabHeader.id === "listaSchedulazioni-tblHeader") {
                for (var i = 0; i < childrens.length; i++) {
                    if (childrens[i].id === "listaSchedulazioni-tblHead0") {
                        childrens[i].style.width = "150px";
                        childrens[i].style.columnWidth = "150px";
                        childrens[i].attributes.style = "width: 150px: text-align: left";
                    } else {
                        childrens[i].style.width = "50px";
                        childrens[i].style.columnWidth = "50px";
                        childrens[i].attributes.style = "width: 50px: text-align: left";
                    }
                }
            } else {
                for (var i = 0; i < childrens.length - 1; i++) {
                    var xx = childrens[i].getElementsByTagName("span")[0];
                    if (xx.textContent !== "" && i !== 0) {
                        var classes = xx.getAttribute("class").split(" ");
                        var classeIteressata = classes[0];

                        var css = "";
                        switch (classeIteressata) {
                            case "EXassign":
                                css = "mediumpurple";
                                break;
                            case "EXhalfDay":
                                css = "orange";
                                break;
                            case "EXwCons":
                                css = "yellow";
                                break;
                            case "EXwNonCons":
                                css = "chartreuse";
                                break;
                            case "EXwCust":
                                css = "red";
                                break;
                            case "EXwNonCust":
                                css = "lightgrey";
                                break;
                            case "EXnotConfirmed":
                                css = "dodgerblue";
                                break;
                            case "EXabroad":
                                css = "lightskyblue";
                                break;
                        }
                        childrens[i].bgColor = css;
                    }
                }
            }
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
        }

        var tabLegenda = document.getElementById('legendaSchedulazioni-listUl');
        var tabHeader = tabLegenda.rows[1];
        var rigaInteressata = tabHeader.childNodes;
        for (var i = 0; i < rigaInteressata.length - 1; i++) {
            var xx = rigaInteressata[i].getElementsByTagName("span")[0];
            if (xx.textContent !== "") {
                var testo = xx.textContent;
                var css = "";
                switch (testo) {
                    case "Assegnata":
                        css = "mediumpurple";
                        break;
                    case "Mezza giornata":
                        css = "orange";
                        break;
                    case "Concomitanza":
                        css = "yellow";
                        break;
                    case "Non concomitanza":
                        css = "chartreuse";
                        break;
                    case "Fissata":
                        css = "red";
                        break;
                    case "Non schedulabile":
                        css = "lightgrey";
                        break;
                    case "Non confermata":
                        css = "dodgerblue";
                        break;
                    case "Estero":
                        css = "lightskyblue";
                        break;
                }
                rigaInteressata[i].bgColor = css;
            }
        }

        var st = tabLegenda.rows[1].innerHTML
        var str = "";
        var ff = "";
        for (var i = 0; i < st.split("</td>").length - 2; i++) {
            str = st.split("</td>")[i] + "</td>";
            ff = ff.concat(str)
        }

        var colspan = parseInt(this.numeroColonne / 8);
        tab_text = tab_text + "<tr><td colspan='" + this.numeroColonne + " border='0'></td></tr><tr><td><span>Legenda</span></td>" + ff.replace(/<td /g, "<td colspan='" + colspan + "' ") + "</tr>";

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
        {
            iframetxtArea1.document.open("txt/html", "replace");
            iframetxtArea1.document.write(tab_text);
            iframetxtArea1.document.close();
            iframetxtArea1.focus();
            sa = iframetxtArea1.document.execCommand("SaveAs", true, "Scheduling_" + commessaScelta + extension);
            return (sa);
            this.onCloseDialogPress();

        } else {
            var filename;
            switch (extension) {
                case ".html":
                    filename = "Scheduling_" + commessaScelta + extension;
                    this.saveAsHTML(tab_text, filename);
                    break;
                case ".xls":
                    filename = "Scheduling_" + commessaScelta;
                    this.saveAsXLS(tab_text, filename);
                    break;
            }
        }
    },

    saveAsXLS: function (uri, filename) {
        var link = document.createElement('a');
        if (typeof link.download === 'string') {
            var file = new Blob([uri], {
                type: "application/vnd.ms-excel"
            });
            document.body.appendChild(link); // Firefox requires the link to be in the body
            link.download = filename;
            link.href = URL.createObjectURL(file);
            link.click();
            setTimeout(function () {
                document.body.removeChild(link); /*remove the link when done*/
            }, 2000);
        } else {
            location.replace(uri);
        }
        this.onCloseDialogPress();
    },

    saveAsHTML: function (uri, filename) {
        var link = document.createElement('a');
        if (typeof link.download === 'string') {
            var file = new Blob([uri], {
                type: "application/vnd.ms-excel"
            });
            document.body.appendChild(link); // Firefox requires the link to be in the body
            link.download = filename;
            link.href = URL.createObjectURL(file);
            link.click();
            setTimeout(function () {
                document.body.removeChild(link); /*remove the link when done*/
            }, 2000);
        } else {
            location.replace(uri);
        }
        this.onCloseDialogPress();
    },

    onCloseDialogPress: function () {
        this.excelDialog.close();
    },



    /* INIZIO - funzione visualizza tutti i consulenti con giornate schedulate diverso da 0 */
    onFilterGioranteZero: function (evt) {
        if (!evt.getSource().getProperty("pressed")) {
            this.listaSelezioniFiltro = undefined;
            evt.getSource().setProperty("pressed", false);
            this.leggiTabella(this.ric);
            this.getView().byId("idFiltroGiornateZeroPjmRichiesteModifica").setText(this._getLocaleText("NascondiConsulentiZeroGiornate"));
        } else {
            var filterConsultantsList = sap.ui.getCore().getElementById("idListaConsulentiByPJMModify");
            if (filterConsultantsList) {
                filterConsultantsList.removeSelections();
            }
            this.getView().byId("idFiltroGiornateZeroPjmRichiesteModifica").setText(this._getLocaleText("RipristinaConsulentiZeroGiornate"));
            var schedulazioniZero = _.filter(this.allScheduling, function (item) {
                if (item.giorniScheduling !== 0) {
                    return item;
                }
            });
            if (_.size(schedulazioniZero) > 0 && _.size(schedulazioniZero) <= 6) {
                this.pmSchedulingModel.setProperty("/nRighe", _.size(schedulazioniZero));
            } else if (_.size(schedulazioniZero) > 6) {
                this.pmSchedulingModel.setProperty("/nRighe", 6);
            } else {
                this.pmSchedulingModel.setProperty("/nRighe", 1);
            }
            this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioniZero);
            setTimeout(_.bind(this._onAfterRenderingTable, this));
        }
    }
    /* FINE - funzione visualizza tutti i consulenti con giornate schedulate diverso da 0 */
});
