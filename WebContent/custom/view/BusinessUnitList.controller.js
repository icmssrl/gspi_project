jQuery.sap.require("view.abstract.AbstractMasterController");

view.abstract.AbstractMasterController.extend("view.BusinessUnitList", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.modelBu = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelBu, "modelBu");

        this.uiModel.setProperty("/searchProperty", ["descrizioneBu"]);
    },

    handleRouteMatched: function (evt) {
        var name = evt.getParameter("name");

        if (name !== "businessUnitList") {
            return;
        }
        view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
        this.getView().byId("list").removeSelections();

        if (sessionStorage.getItem("selectedBu")) {
            sessionStorage.removeItem("selectedBu");
        }

        this.oDialog = this.getView().byId("list");
        this.applyFilter();
        this.readBusinessUnits();
    },

    readBusinessUnits: function (stato) {
        var fSuccess = function (result) {
            this.modelBu.setProperty("/businessUnits", result.items);
            console.log("Business Unit:");
            console.log(result);
        };
        var fError = function () {
            console.log("errore in lettura business unit");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

//        var stato = "APERTO";
//        model.BuList.getBuListByStatus(stato)
        model.BusinessUnit.readBu(undefined, stato)
            .then(fSuccess, fError);
    },

    onHomePress: function () {
        this.router.navTo("launchpad");
    },

    addBUPress: function () {
        if (sessionStorage.getItem("selectedBu")) {
            sessionStorage.removeItem("selectedBu");
        };
        this.router.navTo("businessUnitModify", {
            state: "new"
        });
    },

    onBuSelectBis: function (evt) {
        var src = evt.getSource();
        var selectedItem = src.getSelectedItems()[0].getBindingContext("modelBu").getObject();
        sessionStorage.setItem("selectedBu", JSON.stringify(selectedItem));
        this.router.navTo("businessUnitDetail", {
            detailId: selectedItem.codiceBu
        });
    },

});