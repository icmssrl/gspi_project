jQuery.sap.require("view.abstract.AbstractController");

view.abstract.AbstractController.extend("view.PeriodModify", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.detailPeriodo = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailPeriodo, "detailPeriodo");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "periodModify") {
            return;
        }
        this.oDialog = this.getView().byId("BusyDialog");

        var arguments = evt.getParameter("arguments");
        this.state = arguments.state;
        var id = parseInt(arguments.detailId);
        var year = parseInt(arguments.year);

        this.resetDateStatuses(true);

        this.period = new model.Period(); //To not interfer with already existent data (for now)

        if (this.state !== "new") {
            this.periodLoad(id, year)
                .then(_.bind(function (result) {
                        this.refreshView();
                    }, this),
                    _.bind(function (err) {
                        this.router.navTo("periodList");
                    }, this));
        } else {
            this.period = new model.Period();
            var suggestedPeriod = jQuery.sap.storage.get("suggestedPeriod");
            this.period.update(suggestedPeriod);
            jQuery.sap.storage.remove("suggestedPeriod");

            this.refreshView();
        }
    },

    periodLoad: function (id, year) {
        var defer = Q.defer();
        this.oDialog.setBusy(true);
        var fSuccess = function (result) {
            this.oDialog.setBusy(false);
            this.period = new model.Period(result);
            defer.resolve(this.period);
        };
        fSuccess = _.bind(fSuccess, this);

        var fError = function (err) {
            sap.m.MessageToast(this._getLocaleText("LOADING_PERIOD_ERROR"));
            this.oDialog.setBusy(false);
            defer.reject(err);
        };
        fError = _.bind(fError, this);

        model.Periods().readPeriod(id, year)
            .then(fSuccess, fError);

        return defer.promise;
    },

    refreshView: function () {
        this.detailPeriodo.setData(this.period);
        this.detailPeriodo.refresh();
    },

    onHomePress: function () {
        this.router.navTo("launchpad");
    },

    savePeriodPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_SAVE_PERIOD"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_SAVE_TITLE_PERIOD"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmSavePress, this)
        );
    },

    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            var dateRes = this.setDateStatuses();
            if (!dateRes) {
                sap.m.MessageToast.show(this._getLocaleText("ERROR_WRONG_PARAMETERS"));
                return;
            }
            if (this.state === "new") {
                var fSuccess = function (result) {
                    sap.m.MessageToast.show(this._getLocaleText("PERIOD_CREATION_SUCCESS"));
                    console.log(result);
                    var resultPeriod = JSON.parse(result).results;

                    // funzione scrittura tutte commesse per questo periodo in PROFILO COMMESSA PERIODO
                    var fSuccessReadProject = function (result) {
                        var fSuccessWriteProjects = function () {
                            this.router.navTo("periodList", {
                                year: resultPeriod.annoPeriodoSchedulazione,
                                detailId: resultPeriod.idPeriodo
                            });
                        };
                        var fErrorWriteProjects = function () {
                            this.oDialog.setBusy(false);
                            this.console("errore in lettura progetti", "error");
                        };
                        
                        fSuccessWriteProjects = _.bind(fSuccessWriteProjects, this);
                        fErrorWriteProjects = _.bind(fErrorWriteProjects, this);
                        
                        var res = result.items;
                        var commesse = [];
                        for (var i = 0; i < res.length; i++) {
                            var obj = {};
                            obj.codCommessaPeriodo = res[i].codCommessa;
                            obj.idPeriodoSchedulingProf = resultPeriod.idPeriodo;
                            obj.codStaffCommessaPeriodo = res[i].codiceStaffCommessa;
                            obj.stato = "CHIUSO";
                            obj.codPrioritaA = "0";
                            obj.codPrioritaB = "0";
                            commesse.push(obj);
                        }
                        
                        model.ProfiloCommessaPeriodo.newRecordsFromNewPeriod(commesse)
                            .then(fSuccessWriteProjects, fErrorWriteProjects);
                        
                    };
                    var fErrorReadProject = function () {
                        this.oDialog.setBusy(false);
                        this.console("errore in lettura progetti", "error");
                    };

                    fSuccessReadProject = _.bind(fSuccessReadProject, this);
                    fErrorReadProject = _.bind(fErrorReadProject, this);

                    this.oDialog.setBusy(true);

                    model.Projects.newReadProjects("APERTO")
                        .then(fSuccessReadProject, fErrorReadProject);
                };

                var fError = function (err) {
                    sap.m.MessageToast.show(this._getLocaleText("PERIOD_CREATION_ERROR"));
                    this.setErrorValueState(err);
                    console.log(err);
                };

                fSuccess = _.bind(fSuccess, this);
                fError = _.bind(fError, this);

                this.period.createPeriod()
                    .then(fSuccess, fError);
            } else {
                var fSuccess = function (result) {
                    sap.m.MessageToast.show(this._getLocaleText("PERIOD_UPDATE_SUCCESS"));
                    console.log(result);
                    
                    this.router.navTo("periodList", {
                        year: this.period.annoPeriodo,
                        detailId: this.period.idPeriod
                    });
                };
                var fError = function (err) {
                    sap.m.MessageToast.show(this._getLocaleText("PERIOD_UPDATE_ERROR"));
                    this.setErrorValueState(err);
                    console.log(err);
                };
                fSuccess = _.bind(fSuccess, this);
                fError = _.bind(fError, this);
                this.period.updatePeriod()
                    .then(fSuccess, fError);
            }
        }
    },
    
    setErrorValueState: function (err) {
        var error = JSON.parse(err.responseText);
        var errorMsg = (JSON.parse(error.error)).error.message;
        var errorID = (JSON.parse(error.error)).error.code;

        switch (errorID) {
            case 1:
                this.getView().byId("startDatePicker").setValueState("Error");
                this.getView().byId("startDatePicker").setValueStateText(this._getLocaleText(errorMsg));
                break;
            case 2:
                this.getView().byId("endDatePicker").setValueState("Error");
                this.getView().byId("endDatePicker").setValueStateText(this._getLocaleText(errorMsg));
                break;
            case 3:
                this.getView().byId("startDatePicker").setValueState("Error");
                this.getView().byId("startDatePicker").setValueStateText(this._getLocaleText(errorMsg));
                this.getView().byId("endDatePicker").setValueState("Error");
                this.getView().byId("endDatePicker").setValueStateText(this._getLocaleText(errorMsg));
                break;
        }
    },
    
    cancelModifyPress: function () {
        if (this.state !== "new") {
            this.router.navTo("periodDetail", {
                year: this.period.annoPeriodo,
                detailId: this.period.idPeriod
            });
        } else {
            this.router.navTo("periodList");
        }

    },

    resetDateStatuses: function (all) {
        var dateStatus = []
        dateStatus.push(this.getView().byId("reqDatePicker"));
        dateStatus.push(this.getView().byId("startDatePicker"));
        dateStatus.push(this.getView().byId("endDatePicker"));

        for (var i = 0; i < dateStatus.length; i++) {
            if (all || dateStatus[i].getValueStateText() !== this._getLocaleText("ERROR_DATE_IN_PERIOD")) //To avoid a repetitive call
            {
                dateStatus[i].setValueState("None");
                dateStatus[i].setValueStateText("");
            }
        }

    },

    setDateStatuses: function () {
        var reqDate = this.getView().byId("reqDatePicker");
        var startDate = this.getView().byId("startDatePicker");
        var endDate = this.getView().byId("endDatePicker");


        var check = this.period.checkDateConstraints();
        if (!check.res) {
            var reqError = _.find(check.error, {
                errorCode: 1
            });
            if (reqError) {
                return true;
            }
//            if (reqError && reqDate.getValueStateText() !== this._getLocaleText("ERROR_DATE_IN_PERIOD")) {
//                reqDate.setValueState("Error");
//                reqDate.setValueStateText(this._getLocaleText(reqError.errorText));
//            }

            var invertedDate = _.find(check.error, {
                errorCode: 2
            });
            if (invertedDate) {
                if (startDate.getValueStateText() !== this._getLocaleText("ERROR_DATE_IN_PERIOD")) {
                    startDate.setValueState("Error");
                    startDate.setValueStateText(this._getLocaleText(invertedDate.errorText));

                }
                if (endDate.getValueStateText() !== this._getLocaleText("ERROR_DATE_IN_PERIOD")) {
                    endDate.setValueState("Error");
                    endDate.setValueStateText(this._getLocaleText(invertedDate.errorText));
                }
            }
            var periodTooLarge = _.find(check.error, {
                errorCode: 3
            });
            if (periodTooLarge) {
                if (endDate.getValueStateText() !== this._getLocaleText("ERROR_DATE_IN_PERIOD")) {
                    endDate.setValueState("Error");
                    endDate.setValueStateText(this._getLocaleText(periodTooLarge.errorText));
                }
                if (startDate.getValueStateText() !== this._getLocaleText("ERROR_DATE_IN_PERIOD")) {
                    startDate.setValueState("Error");
                    startDate.setValueStateText(this._getLocaleText(periodTooLarge.errorText));
                }
            }
            return false;
        } else {
            return true;
        }
    },
    DateChangeHandle: function (evt) {
        // var src = evt.getSource();
        //
        // this.period.checkDate();
        //
        // console.log("Periodo");
        // console.log(this.period);
        // // this.detailPeriodo = this.period.getModel();
        // this.getView().getModel("detailPeriodo").refresh();
        // console.log("modelData");
        // console.log(this.detailPeriodo.getData());
        //
        // src.setValueState("None");
        // src.setValueStateText("");
        //
        // return (this.setDateStatuses());

        var src = evt.getSource();

        this.period.checkDate();
        src.setValueState("None");
        src.setValueStateText("");

        console.log("Periodo");
        console.log(this.period);
        this.resetDateStatuses();

        this.setDateStatuses();
        // this.detailPeriodo = this.period.getModel();
        this.getView().getModel("detailPeriodo").refresh();


        return;

    },

    IntervalChangeHandle: function (evt) {
        this.DateChangeHandle(evt);

        var src = evt.getSource();
        var req;
        if (src.getId().indexOf("startDatePicker") > 0)
            req = "START";
        else {
            req = "END";
        }
        var fSuccess = function (result) {
            // sap.m.MessageToast.show(result);
            src.setValueState("None");
            src.setValueStateText("");
            this.setDateStatuses();
            // this.DateChangeHandle(evt);
        }
        fSuccess = _.bind(fSuccess, this);

        var fError = function (err) {
            var error = JSON.parse(err.responseText);
            sap.m.MessageToast.show(this._getLocaleText(error.error));
            src.setValueState("Error");
            src.setValueStateText(this._getLocaleText(error.error));
            this.setDateStatuses();
            // this.DateChangeHandle(evt);
        }
        fError = _.bind(fError, this);

        this.period.checkPeriodDate(req)
            .then(fSuccess, fError)

    },
    //Maybe to parametrize
    // EndDateChangeHandle:function(evt)
    // {
    //
    //   var src = evt.getSource();
    //   var fSuccess = function(result)
    //   {
    //     // sap.m.MessageToast.show(result);
    //     src.setValueState("None");
    //     src.setValueStateText("");
    //     // this.DateChangeHandle(evt);
    //   }
    //   fSuccess = _.bind(fSuccess, this);
    //
    //   var fError = function(err)
    //   {
    //     var error = JSON.parse(err.responseText);
    //     sap.m.MessageToast.show(this._getLocaleText(error.error));
    //     src.setValueState("Error");
    //     src.setValueStateText(this._getLocaleText(error.error));
    //     // this.DateChangeHandle(evt);
    //   }
    //   fError = _.bind(fError, this);
    //
    //   this.period.checkPeriodDate("END")
    //   .then(fSuccess, fError)
    // },




});
