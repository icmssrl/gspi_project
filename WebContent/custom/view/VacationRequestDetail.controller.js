jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.SingleSchedModRequest");
jQuery.sap.require("model.SchedulingModifyRequests");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");

view.abstract.AbstractController.extend("view.VacationRequestDetail", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.detailRequestModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailRequestModel, "detailRequestModel");

        this.detaiRichiesteModificheScambio = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detaiRichiesteModificheScambio, "detaiRichiesteModificheScambio");

        this.visibleDetail = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleDetail, "visibleDetail");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "vacationRequestDetail") {
            return;
        };

        var vacationRequest = model.persistence.Storage.session.get("selectedVacationRequest");
        if (vacationRequest) {
            this.detailRequestModel.setProperty("/periodo", vacationRequest.dataCorretta);
            this.detailRequestModel.setProperty("/halfDay", vacationRequest.mezzaGiornata);
            this.detailRequestModel.setProperty("/stato", vacationRequest.stato);
        }
    }
});
