jQuery.sap.require("view.abstract.AbstractController");

view.abstract.AbstractController.extend("view.AnagStaffDetail", {
    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.detailAnagStaff = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailAnagStaff, "detailAnagStaff");
        this.addConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.addConsultantModel, "cModel");
        this.buModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.buModel, "buModel");
        this.skillModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.skillModel, "skillModel");
        this.skillModelT = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.skillModelT, "skillModelT");
        this.modelFragmentStaff = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelFragmentStaff, "modelFragmentStaff");
        this.modelConsultant = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelConsultant, "modelConsultant");
        this.uiModelDialog.setProperty("/searchPropertyDialog", ["cognomeConsulente", "nomeConsulente"]);
        this.addStaffModule = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.addStaffModule, "addStaffModule");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");
        if (name !== "anagStaffDetail") {
            return;
        }
        var prefTitle = icms.Component.getMetadata().getConfig().settings.prefTitle;
        document.title = prefTitle + " _ STAFF";
        this.oDialog = this.getView().byId("listStaff");
        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        var anagStaff = JSON.parse(sessionStorage.getItem("selectedStaff"));
        var codStaff = anagStaff.codStaffCommessa;
        this.codStaff = anagStaff.codStaffCommessa;
        this.detailAnagStaff.setProperty("/nomeCommessa", anagStaff.nomeCommessa);
        this.detailAnagStaff.setProperty("/descrizioneCommessa", anagStaff.descrizioneCommessa);
        this.detailAnagStaff.setProperty("/codCommessa", anagStaff.codCommessa);
        this.detailAnagStaff.setProperty("/profiloUtenteLog", this.user.codiceProfilo);
        this.readStaffComplete(codStaff);
    },

    // funzione legge i consulenti di uno staff
    readStaffComplete: function (codStaff) {
        this.oDialog.setBusy(true);
        var fSuccess = function (result) {
            _.remove(result.items, {stato: 'CHIUSO'});
            this.detailAnagStaff.setProperty("/staffComplete", result.items);
            this.console("Complete Staff:", "success");
            this.console(result);
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'anagStaffViewSet'");
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        var filtri = "?$filter=COD_STAFF eq " + "'" + codStaff + "'";
        model.StaffCommesseView.readStaffComplete(filtri).then(fSuccess, fError);
    },

    // funzione che apre il dialog per aggiungere consulenti allo staff
    addConsultantPress: function () {
        this.addConsultantStaffDialog = sap.ui.xmlfragment("view.dialog.addConsultantStaffDialog", this);
        var page = this.getView().byId("idAnagStaffDetail");
        page.addDependent(this.addConsultantStaffDialog);
        this.addConsultantStaffDialog.setModel(this.uiModel, "ui");
        this.loadConsultants().then(_.bind(function (result) {
            this.addConsultantStaffDialog.open();
        }, this), _.bind(function (err) {
            this._enterErrorFunction("error", "ERRORE oData", "Errore nella lettura dell'oData di caricamento dei consulenti");
            return;
        }, this));
    },

    // funzione che carica tutti i consulenti e rimuove quelli già presenti nello staff
    loadConsultants: function (params) {
        this.oDialog.setBusy(true);
        var defer = Q.defer();
        var fSuccess = function (result) {
            for (var i = 0; i < this.detailAnagStaff.getData().staffComplete.length; i++) {
                _.remove(result.items, {
                    codConsulente: this.detailAnagStaff.getData().staffComplete[i].codConsulenteStaff
                });
            }
            console.log(result);

            this.addConsultantModel.setData(result);
            defer.resolve();
            this.oDialog.setBusy(false);
        };
        var fError = function (err) {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'consultantsSet'");
            defer.reject(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        var filterString = "";
        model.Consultants.readConsultantsWithSkills(filterString).then(fSuccess, fError);
        return defer.promise;
    },

    onFilterPress: function () {
        this.modelFragmentStaff.setProperty("/bu", true);
        this.modelFragmentStaff.setProperty("/skill", false);
        this.readBu();
        this.readSkill();
    },

    readBu: function () {
        this.oDialog.setBusy(true);
        var fSuccess = function (result) {
            this.buModel.setProperty("/buListItems", result.items);
            if (!this.filterDialogStaffConsultants) {
                this.filterDialogStaffConsultants = sap.ui.xmlfragment("view.dialog.filterDialogStaffConsultants", this);
            }
            this.getView().addDependent(this.filterDialogStaffConsultants);
            this.filterDialogStaffConsultants.open();
            if (sap.ui.getCore().getElementById("idSegmentedButton2")) {
                sap.ui.getCore().getElementById("idSegmentedButton2").setSelectedButton("businessUnitButton2");
            }
            //            console.log(result.items);
            this.oDialog.setBusy(false);
        };
        var fError = function (err) {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'listBuSet'");
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        var stato = "APERTO";
        model.BuList.getBuListByStatus(stato).then(fSuccess, fError);
    },

    readSkill: function () {
        this.oDialog.setBusy(true);
        var fSuccess = function (result) {
            this.skillModel.setProperty("/skillListItems", result.items);
            //            console.log(result.items);
            this.oDialog.setBusy(false);
        };
        var fError = function (err) {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'skillSet'");
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.Skill.readSkills().then(fSuccess, fError);
    },

    getSelectButton: function (evt) {
        var buttonPressed = evt.getSource().getSelectedButton();
        if (buttonPressed === "businessUnitButton2") {
            this.modelFragmentStaff.setProperty("/bu", true);
            this.modelFragmentStaff.setProperty("/skill", false);
        } else if (buttonPressed === "skillButton") {
            this.modelFragmentStaff.setProperty("/bu", false);
            this.modelFragmentStaff.setProperty("/skill", true);
        }
    },

    onFilterDialogClose: function () {
        this.filterDialogStaffConsultants.close();
        var listBu = sap.ui.getCore().getElementById("listaFiltroBu");
        var listSkill = sap.ui.getCore().getElementById("listaFiltroSkill");
        listBu.removeSelections();
        listSkill.removeSelections();
    },

    onFilterDialogOK: function (oEvent) {
        // inizio gestione filtri
        var buList = sap.ui.getCore().byId("listaFiltroBu").getSelectedItems();
        var selectedBu = [];
        var selectedSkill = [];
        var filtroBu = "";
        var filtroSkill = "";
        var filtriStaff = "";
        if (buList.length > 0) {
            for (var i = 0; i < buList.length; i++) {
                selectedBu.push("COD_CONSULENTE_BU eq '" + buList[i].getBindingContext("buModel").getObject().codiceBu + "' ");
            }
            for (var m = 0; m < selectedBu.length - 1; m++) {
                filtroBu = filtroBu + selectedBu[m] + "or ";
            }
            filtroBu = filtroBu + selectedBu[selectedBu.length - 1];
        }
        var skillList = sap.ui.getCore().byId("listaFiltroSkill").getSelectedItems();
        if (skillList.length > 0) {
            for (var j = 0; j < skillList.length; j++) {
                selectedSkill.push("COD_SKILL eq '" + skillList[j].getBindingContext("skillModel").getObject().codSkill + "' ");
            }
            for (var n = 0; n < selectedSkill.length - 1; n++) {
                filtroSkill = filtroSkill + selectedSkill[n] + "and ";
            }
            filtroSkill = filtroSkill + selectedSkill[selectedSkill.length - 1];
        }
        if (filtroBu !== "" && filtroSkill !== "") {
            this.filtriStaff = "?$filter=(" + filtroBu + ")" + " and " + "(" + filtroSkill + ")";
        } else if (filtroBu === "" && filtroSkill !== "") {
            this.filtriStaff = "?$filter=" + filtroSkill;
        } else if (filtroBu !== "" && filtroSkill === "") {
            this.filtriStaff = "?$filter=" + filtroBu;
        }
        //        console.log(this.filtriStaff);
        // fine gestione filtri
        if (this.filtriStaff === "") {
            this.filterDialogStaffConsultants.close();
        } else {
            this.loadFilteredConsultants(this.filtriStaff);
            this.filterDialogStaffConsultants.close();
        }
        var listBu = sap.ui.getCore().getElementById("listaFiltroBu");
        var listSkill = sap.ui.getCore().getElementById("listaFiltroSkill");
        listBu.removeSelections();
        listSkill.removeSelections();
    },

    loadFilteredConsultants: function (filtri) {
        this.oDialog.setBusy(true);
        var fSuccess = function (result) {
            this.temp = [];
            for (var i = 0; i < this.detailAnagStaff.getData().staffComplete.length; i++) {
                _.remove(result.items, {
                    codConsulente: this.detailAnagStaff.getData().staffComplete[i].codConsulenteStaff
                });
            }
            result.items = _.uniq(result.items, 'codConsulente');
            this.addConsultantModel.setData(result);
            //            console.log(result);
            this.oDialog.setBusy(false);
        };
        var fError = function (err) {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'consulenteSkill'");
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.StaffCommesseView.readAllConsultantsFilters(fSuccess, fError, filtri).then(fSuccess, fError);
    },

    onFilterDialogRipristino: function () {
        this.filterDialogStaffConsultants.close();
        this.addConsultantPress();
        var listBu = sap.ui.getCore().getElementById("listaFiltroBu");
        var listSkill = sap.ui.getCore().getElementById("listaFiltroSkill");
        listBu.removeSelections();
        listSkill.removeSelections();
    },

    onAddStaffDialogClose: function () {
        this.addConsultantStaffDialog.close();
        this.addConsultantStaffDialog.destroy(true);
    },

    onAddStaffDialogOK: function () {
        var staff = model.persistence.Storage.session.get("selectedStaff");
        var arrayReq = [];
        var skipEditSkills = true;
        for (var i = 0; i < this.aContexts.length; i++) {
            var path = parseInt(this.aContexts[i].getPath().split("/")[2]);
            var req = {};
            req.codStaff = staff.codStaffCommessa;
            req.codConsulenteStaff = this.aContexts[i].getModel().getData().items[path].codConsulente;
            req.codCommessa = staff.codCommessa;
            req.codPJMStaff = staff.codPjmAc;
            req.teamLeader = "NO";
            req.codSkill = "";

            var skills = this.aContexts[i].getModel().getData().items[path].skills;
            req._staffSkills = skills;
            if (skills.length == 1)
                req.codSkill = skills[0].codSkill;
            else
                skipEditSkills = false;
            req._name = this.aContexts[i].getModel().getData().items[path].nomeCognome;
            arrayReq.push(req);
        }
        if (arrayReq.length > 0) {
            var list = sap.ui.getCore().getElementById("listS");
            list.removeSelections();
            this.addConsultantStaffDialog.close();
            this.addConsultantStaffDialog.destroy(true);

            if (skipEditSkills)
                this.onAddStaffDialogModuleOK(null, arrayReq);
            else
                this.moduleSkillsDialog(arrayReq);
        } else {
            sap.m.MessageToast.show(this._getLocaleText("NO_CONSULTANT_CHOSEN"));
        }
    },

    moduleSkillsDialog: function (cons) {
        this.addConsultantStaffModuleDialog = sap.ui.xmlfragment("view.dialog.addConsultantStaffModuleDialog", this);
        var page = this.getView().byId("idAnagStaffDetail");
        this.addConsultantStaffModuleDialog.setModel(this.uiModel, "ui");
        page.addDependent(this.addConsultantStaffModuleDialog);

        this.addStaffModule.setData(cons);
        this.addConsultantStaffModuleDialog.setModel(this.addStaffModule, "addStaffModule");
        this.addConsultantStaffModuleDialog.open();
    },

    onAddStaffDialogModuleClose: function () {
        this.addConsultantStaffModuleDialog.close();
        this.addConsultantStaffModuleDialog.destroy(true);
    },

    onAddStaffDialogModuleDelete: function (evt) {
        var listItem = evt.getParameter("listItem");
        var path = listItem.getBindingContext("addStaffModule").getPath();
        var index = Number(path.substring(1));
        this.addStaffModule.getProperty('/').splice(index, 1);
        this.addStaffModule.refresh();
        if (this.addStaffModule.getData().length <= 0) {
            this.addConsultantStaffModuleDialog.close();
            this.addConsultantStaffModuleDialog.destroy(true);
        }
    },

    onAddStaffDialogModuleOK: function (evt, skippedData) {
        var that = this;
        var fSuccess = function (result) {
            that.codStaff = result.results[0].codStaff;
            if (that.addConsultantStaffModuleDialog && !that.addConsultantStaffModuleDialog.bIsDestroyed) {
                that.addConsultantStaffModuleDialog.close();
                that.addConsultantStaffModuleDialog.destroy(true);
            }
            var fSuccess = function (result) {
                that.detailAnagStaff.setProperty("/staffComplete", result.items);
                that.console("Complete Staff:", "success");
                that.console(result);
                sap.m.MessageToast.show(that._getLocaleText("ADDED_CONSULTANT"));
                that.oDialog.setBusy(false);
            };
            var fError = function () {
                that._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'anagStaffViewSet'");
            };
            fSuccess = _.bind(fSuccess, that);
            fError = _.bind(fError, that);
            var filtri = "?$filter=COD_STAFF eq " + "'" + that.codStaff + "' and STATO eq 'APERTO'";
            model.StaffCommesseView.readStaffComplete(filtri).then(fSuccess, fError);
        };
        var fError = function (err) {
            if (that.addConsultantStaffModuleDialog && !that.addConsultantStaffModuleDialog.bIsDestroyed)
                that.addConsultantStaffModuleDialog.close();
            var list = sap.ui.getCore().getElementById("listS");
            list.removeSelections();
            that._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore lettura servizio 'insertNewConsultantInStaff'");
        };

        var array = skippedData ? skippedData : this.addStaffModule.getData();
        var hasError = false;
        for (var i = 0; i < array.length; i++) {
            if (array[i]['codSkill'] === "") hasError = true;
        }

        if (array.length > 0 && !hasError) {
            this.oDialog.setBusy(true);
            for (var i = 0; i < array.length; i++) {
                delete array[i]['_name'];
                delete array[i]['_staffSkills'];
            }
            model.insertConsultantInStaff.insert(array).then(fSuccess, fError);
        } else if (hasError) {
            sap.m.MessageToast.show(this._getLocaleText("NO_SKILL_CHOSEN"));
        } else {
            sap.m.MessageToast.show(this._getLocaleText("NO_CONSULTANT_CHOSEN"));
        }
    },

    handleDelete: function (evt) {
        var listItem = evt.getParameter("listItem");
        var obj = listItem.getBindingContext("detailAnagStaff").getObject();
        this.codConsulenteStaff = obj.codConsulenteStaff;
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_DELETE_STAFF"), sap.m.MessageBox.Icon.ALLERT, this._getLocaleText("CONFIRM_DELETE_STAFF_TITLE"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO], _.bind(this._confirmDeletePress, this));
    },

    _confirmDeletePress: function (evt) {
        var item = sessionStorage.getItem("selectedStaff");
        var parsedItem = JSON.parse(item);
        this.codStaff = parsedItem.codStaffCommessa;
        var codConsulenteStaff = this.codConsulenteStaff;
        var codCommessa = parsedItem.codCommessa;
        var codPJMStaff = parsedItem.codPjmAc;
        //        console.log("codStaff= " + this.codStaff + ", codCommessa= " + codCommessa + ", codPJMStaff= " + codPJMStaff);
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            this.oDialog.setBusy(true);
            var fSuccess = function (result) {
                sap.m.MessageToast.show(this._getLocaleText("STAFF_DELETE_SUCCESS"));
                this.readStaffComplete(this.codStaff);
                this.oDialog.setBusy(false);
            };
            var fError = function (err) {
                this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore lettura servizio 'deleteAnagraficaStaffProgetti'");
            };
            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            model.StaffCommesseView.deleteConsultantFromStaff(this.codStaff, codConsulenteStaff, codCommessa, codPJMStaff, fSuccess, fError).then(fSuccess, fError);
        }
    },

    onSelezionaSkill: function (evt) {
        var teamLeader = "";
        var codSkill = "";
        this.itemId = evt.mParameters.id;
        this.goodItem = evt.mParameters;
        this.consulente = sap.ui.getCore().byId(this.itemId).getBindingContext("detailAnagStaff").getObject();
        this.codConsulente = sap.ui.getCore().byId(this.itemId).getBindingContext("detailAnagStaff").getObject().codConsulenteStaff;
        var idStaff = sap.ui.getCore().byId(this.itemId).getBindingContext("detailAnagStaff").getObject().idStaff;
        this.oDialog.setBusy(true);
        var fsuccess = function (result) {
            this.teamLeaderPresente = result.items[0].teamLeader;
            this.oDialog.setBusy(false);
            if (!this.teamLeaderPresente) {
                teamLeader = "SI";
                if (this.consulente.nomeSkill !== "") {
                    // verifico se è già presente un altro consulente TEAM LEADER in questo modulo su questa commessa
                    var staff = _.cloneDeep(this.detailAnagStaff.getProperty("/staffComplete"));
                    _.remove(staff, {
                        codConsulenteStaff: result.items[0].codConsulenteStaff
                    });
                    if (_.find(staff, {
                        codSkill: result.items[0].codSkill,
                        teamLeader: true
                    })) {
                        var consulenteTeamLeader = _.find(staff, {
                            codSkill: result.items[0].codSkill,
                            teamLeader: true
                        });
                        sap.m.MessageBox.show(this._getLocaleText("PER_IL_MODULO") + " " + this.consulente.nomeSkill + " " + this._getLocaleText("IL_TEAM_LEADER_E") + " " + consulenteTeamLeader.cognomeConsulente + " " + consulenteTeamLeader.nomeConsulente + ". " + this._getLocaleText("SI_DESIDERA_SOSTITUIRLO"), {
                            icon: sap.m.MessageBox.Icon.INFORMATION,
                            title: this._getLocaleText("CONFERMA"),
                            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                            onClose: _.bind(function (oAction) {
                                if (oAction === "YES") {
                                    codSkill = this.consulente.codSkill;
                                    this._updateTeamLeader(teamLeader, codSkill, idStaff).then(this._updateTeamLeader("NO", codSkill, consulenteTeamLeader.idStaff));
                                } else {
                                    _.find(this.detailAnagStaff.getData().staffComplete, {
                                        codConsulenteStaff: this.codConsulente
                                    }).teamLeader = false;
                                    this.detailAnagStaff.refresh();
                                    return;
                                }
                            }, this),
                        });
                    } else {
                        sap.m.MessageBox.show(this._getLocaleText("CONFERMA_LEADER_SI_SPUNTA") + " " + this.consulente.cognomeConsulente + " " + this.consulente.nomeConsulente + " " + this._getLocaleText("NEL_MODULO") + " " + this.consulente.nomeSkill, {
                            icon: sap.m.MessageBox.Icon.INFORMATION,
                            title: this._getLocaleText("CONFERMA"),
                            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                            onClose: _.bind(function (oAction) {
                                if (oAction === "YES") {
                                    codSkill = this.consulente.codSkill;
                                    this._updateTeamLeader(teamLeader, codSkill, idStaff);
                                } else {
                                    _.find(this.detailAnagStaff.getData().staffComplete, {
                                        codConsulenteStaff: this.codConsulente
                                    }).teamLeader = false;
                                    this.detailAnagStaff.refresh();
                                    return;
                                }
                            }, this),
                        });
                    }
                } else {
                    if (!this.chooseSkillForTeamLeader) {
                        this.chooseSkillForTeamLeader = sap.ui.xmlfragment("view.dialog.chooseSkillForTeamLeader", this);
                    }
                    var page = this.getView().byId("idAnagStaffDetail");
                    page.addDependent(this.chooseSkillForTeamLeader);
                    this.chooseSkillForTeamLeader.open();
                    this.oDialog.setBusy(true);
                    var fSuccess = function (result) {
                        this.skillModelT.setProperty("/skillListTeamLeader", result.items);
                        this.teamLeader = teamLeader;
                        this.idStaff = idStaff;
                        this.console(result.items);
                        this.oDialog.setBusy(false);
                    };
                    var fError = function (err) {
                        this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'consulenteSkill'");
                    };
                    fSuccess = _.bind(fSuccess, this);
                    fError = _.bind(fError, this);
                    model.Skill.getConsulenteSkillSerialized(this.codConsulente).then(fSuccess, fError);
                }
            } else if (this.teamLeaderPresente) {
                sap.m.MessageBox.show(this._getLocaleText("CONFERMA_LEADER_NO_SPUNTA"), {
                    icon: sap.m.MessageBox.Icon.INFORMATION,
                    title: this._getLocaleText("CONFERMA"),
                    actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                    onClose: _.bind(function (oAction) {
                        if (oAction === "YES") {
                            teamLeader = "NO";
                            codSkill = this.consulente.codSkill;
                            this._updateTeamLeader(teamLeader, codSkill, idStaff);
                        } else {
                            _.find(this.detailAnagStaff.getData().staffComplete, {
                                codConsulenteStaff: this.codConsulente
                            }).teamLeader = true;
                            this.detailAnagStaff.refresh();
                            return;
                        }
                    }, this),
                });
            }
        };
        var ferror = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'anagStaffViewSet'");
        };
        fsuccess = _.bind(fsuccess, this);
        ferror = _.bind(ferror, this);
        var filtri = "?$filter=ID_STAFF eq " + idStaff;
        model.StaffCommesseView.readStaffComplete(filtri).then(fsuccess, ferror);
    },

    handleCloseSkillDialog: function () {
        var item = JSON.parse(sessionStorage.getItem("selectedStaff"));
        this.codStaff = item.codStaffCommessa;
        this.readStaffComplete(this.codStaff);
        this.chooseSkillForTeamLeader.close();
        sap.ui.getCore().byId("idListSkillTeamLeader").removeSelections();
    },

    handleConfirmSkillDialog: function () {
        if (sap.ui.getCore().byId("idListSkillTeamLeader").getSelectedItem() === null) {
            sap.m.MessageToast.show(this._getLocaleText("NO_SKILL_CHOSEN"));
            return;
        }
        this.console(this.idStaff);
        var nonSelez = sap.ui.getCore().byId("idListSkillTeamLeader").getSelectedItem();
        var codSkill = "";
        if (nonSelez !== null) {
            codSkill = nonSelez.getBindingContext("skillModelT").getObject().codSkill;
        } else {
            codSkill = this.codSkill;
        }
        var staff = _.cloneDeep(this.detailAnagStaff.getProperty("/staffComplete"));
        _.remove(staff, {
            codConsulenteStaff: this.consulente.codConsulenteStaff
        });
        if (_.find(staff, {
            codSkill: codSkill,
            teamLeader: true
        }) && this.consulente.teamLeader) {
            var consulenteTeamLeader = _.find(staff, {
                codSkill: codSkill,
                teamLeader: true
            });
            sap.m.MessageBox.show(this._getLocaleText("PER_IL_MODULO") + " " + this.consulente.nomeSkill + " " + this._getLocaleText("IL_TEAM_LEADER_E") + " " + consulenteTeamLeader.cognomeConsulente + " " + consulenteTeamLeader.nomeConsulente + ". " + this._getLocaleText("SI_DESIDERA_SOSTITUIRLO"), {
                icon: sap.m.MessageBox.Icon.INFORMATION,
                title: this._getLocaleText("CONFERMA"),
                actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                onClose: _.bind(function (oAction) {
                    if (oAction === "YES") {
                        this._updateTeamLeader(this.teamLeader, codSkill, this.idStaff).then(this._updateTeamLeader("NO", codSkill, consulenteTeamLeader.idStaff));
                    } else {
                        _.find(this.detailAnagStaff.getData().staffComplete, {
                            codConsulenteStaff: this.codConsulente
                        }).teamLeader = false;
                        this.detailAnagStaff.refresh();
                        return;
                    }
                }, this),
            });
        } else {
            this._updateTeamLeader(this.teamLeader, codSkill, this.idStaff);
        }
    },

    _updateTeamLeader: function (teamLeader, codSkill, idStaff) {
        this.oDialog.setBusy(true);
        var defer = Q.defer();
        var fsuccess = function (result) {
            if (this.chooseSkillForTeamLeader) {
                this.chooseSkillForTeamLeader.close();
            }
            if (sap.ui.getCore().byId("idListSkillTeamLeader")) {
                sap.ui.getCore().byId("idListSkillTeamLeader").removeSelections();
            }
            var msg = this._getLocaleText("LEADER_UPDATED");
            if (this.teamLeader === 'NO') {
                msg = this._getLocaleText("SKILL_UPDATED");
            }
            sap.m.MessageToast.show(msg);
            var item = JSON.parse(sessionStorage.getItem("selectedStaff"));
            this.codStaff = item.codStaffCommessa;
            this.readStaffComplete(this.codStaff);
            this.teamLeader = undefined;
            this.idStaff = undefined;
            this.codSkill = undefined;
            this.oDialog.setBusy(false);
            defer.resolve();
        };
        var ferror = function () {
            sap.ui.getCore().byId("idListSkillTeamLeader").removeSelections();
            defer.reject();
            this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore lettura servizio 'updateTeamLeaderStaff'");
        };
        fsuccess = _.bind(fsuccess, this);
        ferror = _.bind(ferror, this);
        model.StaffCommesseView.updateTeamLeaderStaff(teamLeader, codSkill, idStaff).then(fsuccess, ferror);
        return defer.promise;
    },

    editSkill: function (evt) {
        var cognome = evt.getSource().getParent().getAggregation("cells")[0].getProperty("text").split(", ")[0];
        var nome = evt.getSource().getParent().getAggregation("cells")[0].getProperty("text").split(", ")[1];
        if (cognome === 'Consulente' && nome === 'Generico') {
            var skill = evt.getSource().getParent().getAggregation("cells")[2].getProperty("text");
            var consulenteGnerico = _.find(this.detailAnagStaff.getProperty("/staffComplete"), {
                'cognomeConsulente': cognome,
                'nomeConsulente': nome,
                'nomeSkill': skill
            });
            this.editGeneric(consulenteGnerico);
        } else {
            this.consulente = _.find(this.detailAnagStaff.getProperty("/staffComplete"), {
                'cognomeConsulente': cognome,
                'nomeConsulente': nome
            });
            this.teamLeader = this.consulente.teamLeader ? "SI" : "NO";
            this.codSkill = this.consulente.codSkill;
            this.idStaff = this.consulente.idStaff;
            var codConsulente = this.consulente.codConsulenteStaff;
            if (!this.chooseSkillForTeamLeader) {
                this.chooseSkillForTeamLeader = sap.ui.xmlfragment("view.dialog.chooseSkillForTeamLeader", this);
            }
            var page = this.getView().byId("idAnagStaffDetail");
            page.addDependent(this.chooseSkillForTeamLeader);
            this.chooseSkillForTeamLeader.open();
            this.oDialog.setBusy(true);
            var fSuccess = function (result) {
                this.skillModelT.setProperty("/skillListTeamLeader", result.items);
                this.oDialog.setBusy(false);
            };
            var fError = function (err) {
                this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'consulenteSkill'");
            };
            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            model.Skill.getConsulenteSkillSerialized(codConsulente).then(fSuccess, fError);
        }
    },

    editGeneric: function (consulenteGnerico) {
        var skill = consulenteGnerico.nomeSkill;
        var codSkill = consulenteGnerico.codSkill;
        this.modelConsultant.setProperty("/nomeSkill", skill);
        this.modelConsultant.setProperty("/codSkill", codSkill);
        this.modelConsultant.setProperty("/key", 'restricted');
        this.codConsGenDaSostiutire = consulenteGnerico.codConsulenteStaff;
        this.oDialog.setBusy(true);
        var fSuccess = function (result) {
            this.console("consulenti filtrati per skill", "confirm");
            //            console.log(result);
            this.setConsultant = sap.ui.xmlfragment("view.dialog.selectConsultantFromGeneric", this);
            var page = this.getView().byId("idAnagStaffDetail");
            page.addDependent(this.setConsultant);
            for (var i = 0; i < this.detailAnagStaff.getProperty("/staffComplete").length; i++) {
                _.remove(result, {
                    codConsulente: this.detailAnagStaff.getProperty("/staffComplete")[i].codConsulenteStaff
                });
            }
            var restrictedConsultant = _.cloneDeep(result);
            restrictedConsultant = _.filter(restrictedConsultant, { codSkill: this.modelConsultant.getProperty("/codSkill") });
            //            _.filter(restrictedConsultant, {});
            //            this.modelConsultant.setData(result);
            //            this.modelConsultant.setData(result.items);
            this.modelConsultant.setProperty("/restrictedConsultant", restrictedConsultant);
            this.modelConsultant.setProperty("/allConsultant", _.uniq(result, 'codConsulente'));
            this.setConsultant.open();
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'consulenteSkill'");
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        //        model.Consultants.getConsultantFromSkill(skill).then(fSuccess, fError);
        model.Consultants.getConsultantFromSkill().then(fSuccess, fError);
    },

    closeDialogSelectCosnusltantFromGeneric: function () {
        this.setConsultant.close();
        this.setConsultant.destroy(true);
    },

    okDialogSelectCosnusltantFromGeneric: function () {
        /*  manca la comparsa del setBusy per bloccare la pagina finché vengono fatte le varie chiamate
            da verificare se si vuole chiamare sia per il successo che per l'errore la stessa funzione
            --> nel network si vede se la chiamata è andata a buon fine, però facendo solo debug no) */
        //        var codCommessa = this.detailAnagStaff.getProperty("/codCommessa");
        this.codCommessaStaff = this.detailAnagStaff.getProperty("/codCommessa");
        var fRestoreAnagStaff = function () {
            sap.m.MessageToast.show(this._getLocaleText("ERROR_UPDATE_ANAGRAFICA_STAFF"));
            this.setConsultant.close();
            this.setConsultant.destroy(true);
            this.oDialog.setBusy(false);
        };
        var fErrorDraft = function () {
            // rifare update anagrafica staff con consulente generico
            // uso la stessa fSuccess perché tanto non cambierebbe farne due diverse
            sap.m.MessageToast.show(this._getLocaleText("ERROR_UPDATE_DRAFT"));
            //            model.UpdateAnagStaffGenerico.restoreAnagStaff(this.codConsProposto, this.codConsGenDaSostiutire, this.codCommessaStaff).then(fRestoreAnagStaff, fRestoreAnagStaff);
        };
        var fErrorFinal = function () {
            // rifare update draft con consulente generico
            // comunque vada questa chiamata, poi si va a cambiare comunque la tabella dell'anagraficaStaff entrando nella 
            sap.m.MessageToast.show(this._getLocaleText("ERROR_UPDATE_FINAL"));
            model.UpdateAnagStaffGenerico.restoreDraft(this.codConsProposto, this.codConsGenDaSostiutire, this.codCommessaStaff).then(fErrorDraft, fErrorDraft);
        };
        var fErrorAnagStaff = function () {
            sap.m.MessageToast.show(this._getLocaleText("ERROR_UPDATE_ANAGRAFICA_STAFF"));
            this.setConsultant.close();
            this.setConsultant.destroy(true);
            this.oDialog.setBusy(false);
        };
        var fSuccessFinal = function () {
            sap.m.MessageToast.show(this._getLocaleText("CAMBIO_UTENTE_OK"));
            this.setConsultant.close();
            this.setConsultant.destroy(true);
            this.readStaffComplete(this.codStaff);
            this.oDialog.setBusy(false);
        };
        var fSuccessDraft = function () {
            model.UpdateAnagStaffGenerico.updateFinal(this.codConsGenDaSostiutire, this.codConsProposto, this.codCommessaStaff)
                .then(fSuccessFinal, fErrorFinal);
        };
        var fSuccessAnagStaff = function () {
            model.UpdateAnagStaffGenerico.updateRequest(this.codConsGenDaSostiutire, this.codConsProposto, this.codCommessaStaff)
                .then(function () {
                    model.UpdateAnagStaffGenerico.updateDraft(this.codConsGenDaSostiutire, this.codConsProposto, this.codCommessaStaff)
                        .then(fSuccessDraft, fErrorDraft);
                }.bind(this), function () {
                    sap.m.MessageToast.show(this._getLocaleText("ERROR_UPDATE_REQUEST"));
                    model.UpdateAnagStaffGenerico.restoreAnagStaff(this.codConsProposto, this.codConsGenDaSostiutire, this.codCommessaStaff)
                        .then(fRestoreAnagStaff, fRestoreAnagStaff);
                }.bind(this));
        };
        fSuccessFinal = _.bind(fSuccessFinal, this);
        fErrorFinal = _.bind(fErrorFinal, this);
        fSuccessDraft = _.bind(fSuccessDraft, this);
        fErrorDraft = _.bind(fErrorDraft, this);
        fSuccessAnagStaff = _.bind(fSuccessAnagStaff, this);
        fErrorAnagStaff = _.bind(fErrorAnagStaff, this);
        fRestoreAnagStaff = _.bind(fRestoreAnagStaff, this);
        if (this.modelConsultant.getProperty("/key") === 'restricted') {
            var consProposto = sap.ui.getCore().getElementById("consultantPerGenerico").getSelectedItems()[0];
        } else {
            var consProposto = sap.ui.getCore().getElementById("consultantPerGenericoAll").getSelectedItems()[0];
        }
        if (consProposto) {
            this.oDialog.setBusy(true);
            this.codConsProposto = consProposto.getProperty("description");
            model.UpdateAnagStaffGenerico.updateAnagStaff(this.codConsGenDaSostiutire, this.codConsProposto, this.codCommessaStaff)
                .then(fSuccessAnagStaff, fErrorAnagStaff);
            //            model.UpdateAnagStaffGenerico.updateAnagStaff(this.codConsGenDaSostiutire, this.codConsProposto, this.detailAnagStaff.getProperty("/codCommessa")).then(fSuccessAnagStaff, fErrorAnagStaff);
        } else {
            sap.m.MessageToast.show(this._getLocaleText("SelectOneElement"));
        }
    },

    onSelectionChange: function (evt) {
        var oList = evt.getSource();
        this.aContexts = oList.getSelectedContexts(true);
    },

    onSelectSegmentedButton: function (evt) {
        var source = evt.getSource();
        var key = source.getSelectedKey();
        this.modelConsultant.setProperty("/key", key);
    },

    handleSearchConsultantDialog: function (evt) {
        var value = evt.getSource().getValue();
        if (this.modelConsultant.getProperty("/key") === 'restricted') {
            var list = sap.ui.getCore().byId("consultantPerGenerico");
        } else {
            var list = sap.ui.getCore().byId("consultantPerGenericoAll");
        }

        var filters = [];
        var nameFilter = new sap.ui.model.Filter("nomeCognome", sap.ui.model.FilterOperator.Contains, value);
        var codeFilter = new sap.ui.model.Filter("codConsulente", sap.ui.model.FilterOperator.Contains, value);
        filters.push(nameFilter);
        filters.push(codeFilter);
        filters = new sap.ui.model.Filter({
            filters: filters,
            and: false
        });
        var binding = list.getBinding("items");
        binding.filter(filters);
    },

});