jQuery.sap.require("view.abstract.AbstractController");

view.abstract.AbstractController.extend("view.AdministratorSchedulingReadOnly", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.pmSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingModel, "pmSchedulingModel");
        this.pmSchedulingModel.setSizeLimit(1000);

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");

        this.noteModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteModel, "noteModel");

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");

        this.adminFilterModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.adminFilterModel, "adminFilterModel");

        this.excelModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.excelModel, "excelModel");

        this.uiModel.setProperty("/searchProperty", ["nomeCognome"]);
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.userModel.setData(this.user);

        if (!this._checkRoute(evt, "administratorSchedulingReadOnly")) {
            return;
        }

        this.oDialog = this.getView().byId("BlockLayout");
        this.oDialogTable = this.getView().byId("idSchedulazioniTable");

        this.oDialogTable.setVisible(false);

        this.getView().byId("idSelectCommessa").setSelectedKey("");
        this.getView().byId("idSelectCommessa").setValue("");

        this.visibleModel.setProperty("/chargeButton", false);
        this.visibleModel.setProperty("/sceltaCaricamento", false);
        this.visibleModel.setProperty("/caricaTutto", false);
        this.visibleModel.setProperty("/comboBoxCommesse", false);
        this.visibleModel.setProperty("/giornateTotali", false);
        this.visibleModel.setProperty("/filterButton", false);
        this.visibleModel.setProperty("/downloadExcelButton", false);

        this.getView().byId("idButtonSelezionaCommessa").setPressed(false);

        this.readPeriods();
        this.chosedCodCom = undefined;
        this.listBu = [];
    },

    readPeriods: function () {
        var fSuccess = function (result) {
            var res = {};
            _.remove(result.items, {
                stato: 'CHIUSO'
            });
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for (var i = 0; i < 13; i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.periodModel.setData(res13);
            this.console("Periods: " + res13.items.length, "success");
            this.console(res13.items);
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");
            var req = {};
            req.codPjm = this.user.codice_consulente;
            req.stato = "APERTO";
            this.readProjects(req);
        };
        var fError = function () {
            if (!this.oneTimeReadPeriods) {
                // funzione che ripete la chiamata di login nel caso in cui sia scaduta l'autenticazione al sistema
                this.oneTimeReadPeriods = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(this.readPeriods(), this));
            } else {
                this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'getPeriods'");
            }
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    // Funzione caricamento commesse
    readProjects: function (req) {
        console.time('readProjects'); // funzione per verificare il tempo che impiega la funzione ->START
        var fSuccess = function (result) {
            this.allCommesse = result.items;
            result.items = _.uniq(result.items, 'codCommessa');
            this.pmSchedulingModel.setProperty("/commesse", []);
            var commesseOrdinate = _.sortByOrder(result.items, [function (o) {
                return o.nomeCommessa.toLowerCase();
            }]);
            this.pmSchedulingModel.setProperty("/commesse", commesseOrdinate);
            this.console("Projects: " + commesseOrdinate.length, "success");
            this.console(commesseOrdinate);
            var comboCommessa = this.getView().byId("idSelectCommessa");
            if (comboCommessa) {
                comboCommessa.setValue(comboCommessa.getDefaultSelectedItem());
            }
            console.timeEnd('readProjects'); // funzione per verificare il tempo che impiega la funzione ->END
            this.readCommessePerPjmSet();
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'projectsViewWithoutPriority'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Projects.readProjectsPerPJM(req)
            .then(fSuccess, fError);
    },

    // Funzione caricamento di tutte le commesse di tutti i consulenti del PJM
    readCommessePerPjmSet: function () {
        console.time('readCommessePerPjmSet'); // funzione per verificare il tempo che impiega la funzione ->START
        var fSuccess = function (result) {
            var arrConsulente = [];
            for (var i = 0; i < result.items.length; i++) {
                if (!_.find(arrConsulente, {
                        cod: result.items[i].codConsulente
                    }))
                    arrConsulente.push({
                        cod: result.items[i].codConsulente
                    });
            }
            for (var t = 0; t < arrConsulente.length; t++) {
                arrConsulente[t].commesse = _.where(result.items, {
                    codConsulente: arrConsulente[t].cod,
                    stato: 'APERTO'
                });
            }
            this.commessePerConsulente = arrConsulente;
            this.oDialog.setBusy(false);
            console.timeEnd('readCommessePerPjmSet'); // funzione per verificare il tempo che impiega la funzione ->END
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'readCommessePerPjmSet'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var codPjm = this.user.codice_consulente;
        model.CommessePerStaffDelPjm.read(codPjm)
            .then(fSuccess, fError);
    },

    onChangePeriod: function (pe) {
        this.oDialog.setBusy(true);

        this.visibleModel.setProperty("/comboBoxCommesse", false);
        this.visibleModel.setProperty("/chargeButton", false);
        this.visibleModel.setProperty("/giornateTotali", false);
        this.visibleModel.setProperty("/downloadExcelButton", false);
        this.getView().byId("idButtonSelezionaCommessa").setPressed(false);

        this.svuotaSelect();
        this.svuotaTabella();
        this.chosedCodCom = undefined;

        if (pe.getSource().getSelectedItem().getProperty("additionalText") === "WIP") {
            sap.m.MessageToast.show("Periodo non ancora rilasciato");
            this.oDialog.setBusy(false);
            return;
        }
        this.visibleModel.setProperty("/sceltaCaricamento", true);
        this.visibleModel.setProperty("/caricaTutto", true);

        var table = this.getView().byId("idSchedulazioniTable");
        this.pe = pe;
        this.pe = pe;
        var p = pe.getSource().getSelectedItem().getProperty("text").replace(/ /g, '');
        var period = pe.getSource().getProperty("selectedKey");
        this.period = parseInt(period);
        model.persistence.Storage.session.save("period", period);
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(period)
        });
        model.persistence.Storage.session.save("periodSelected", periodSelect);

        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();

        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = pInizio;
        var dataF = pFine;

        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

        // ** rendo visibili solo le colonne del periodo selezionato
        var columns = table.getColumns();
        for (var i = 0; i < columns.length; i++) {
            columns[i].setVisible(true);
        }
        for (var i = this.numColonne; i < columns.length; i++) {
            columns[i].setVisible(false);
        }
        // **

        // ** metto i nomi dei giorni sulle colonne
        for (var i = 2; i <= columns.length; i++) {
            columns[i - 1].getLabel().mProperties.text = this.arrayGiorni[i - 2];
        }
        // **
        this.pmSchedulingModel.setProperty("/days", this.arrayGiorni);

        this.getView().byId("idSelectCommessa").setEnabled(true);
        this.visibleModel.setProperty("/chargeButton", false);
        jQuery.sap.delayedCall(500, this, function () {
            this.oDialog.setBusy(false);
        });
    },

    selectCommessaVisible: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            this.visibleModel.setProperty("/filterButton", false);
            this.visibleModel.setProperty("/caricaTutto", false);
            this.visibleModel.setProperty("/comboBoxCommesse", true);
            this.visibleModel.setProperty("/giornateTotali", true);
        } else {
            this.visibleModel.setProperty("/caricaTutto", true);
            this.visibleModel.setProperty("/comboBoxCommesse", false);
            this.visibleModel.setProperty("/giornateTotali", false);
        }
        this.svuotaTabella();
        this.getView().byId("idSelectCommessa").setSelectedKey("");
        this.getView().byId("idSelectCommessa").setValue("");
        if (model.persistence.Storage.session.get("commessa")) {
            model.persistence.Storage.session.remove("commessa");
        }
        this.pmSchedulingModel.setProperty("/giorniTotali", 0);
        this.visibleModel.setProperty("/downloadExcelButton", false);
        this.listBu = [];
    },

    chargeProjectsNotSelected: function () {
        //        this.visibleModel.setProperty("/filterButton", true);
        this.listBu = [];
        this.pmSchedulingModel.setProperty("/commessa", "");
        this.chosedCodCom = undefined;
        var req = {};
        req.idPeriodo = this.period;
        req.codPjm = this.user.codice_consulente;
        this.ric = req;
        this.leggiTabella(req);
    },

    // selezionando una commessa faccio apparire il tasto di caricamento
    onSelectionChangeCommessa: function (evt) {
        this.svuotaTabella();
        this.visibleModel.setProperty("/chargeButton", true);
        this.chosedCodCom = evt.getSource().getSelectedKey();
        this.pmSchedulingModel.setProperty("/commessa", evt.getSource().getValue().split(" - ")[0]);
    },

    // al premere del pulsante faccio nascondere lo stesso
    chargeProjects: function (evt) {
        this.visibleModel.setProperty("/chargeButton", false);
    },

    onChangeCommessa: function (evt) {
        this.visibleModel.setProperty("/chargeButton", false);
        this._onChangeCommessa(this.chosedCodCom);
    },

    _onChangeCommessa: function (commessaP) {
        var commessaScelta = commessaP;
        var c = _.find(this.pmSchedulingModel.getData().commesse, {
            codCommessa: commessaScelta
        });
        var comm = model.persistence.Storage.session.save("commessa", c);

        var req = {};
        req.idCommessa = commessaScelta;
        req.idStaff = c.codiceStaffCommessa;
        req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
        req.nomeCommessa = c.nomeCommessa;
        this.ric = req;

        this.leggiTabella(req);
        this.visibleModel.setProperty("/visibleTable", true);
    },

    leggiTabella: function (req) {
        this.oDialogTable.setBusy(true);
        var fSuccess = function (res) {
            if (res && res.resultOrdinato) {
                var schedulazioni;
                if (_.size(this.listBu) === 0) {
                    this.listBu = res.arrayBu;
                }
                schedulazioni = res.resultOrdinato;
                if (!this.ric.codPjm) {
                    for (var t = 0; t < schedulazioni.length; t++) {
                        var commessaConsulente = _.find(_.find(this.commessePerConsulente, {
                            cod: schedulazioni[t].codiceConsulente
                        }).commesse, {
                            codCommessa: this.chosedCodCom
                        });
                        schedulazioni[t].teamLeader = commessaConsulente ? commessaConsulente.teamLeader : "";
                        schedulazioni[t].nomeSkill = commessaConsulente ? commessaConsulente.nomeSkill : "";
                    }
                } else {
                    for (var t = 0; t < schedulazioni.length; t++) {
                        var skills = schedulazioni[t].skill;
                        var skillInModel = "";
                        for (var c = 0; c < _.size(skills); c++) {
                            if (c === 0) {
                                skillInModel = skills[c];
                            } else {
                                skillInModel = skillInModel + ", " + skills[c];
                            }
                        }
                        schedulazioni[t].teamLeader = "";
                        schedulazioni[t].nomeSkill = skillInModel ? skillInModel : "";
                    }
                }
                var uniEle, schedOrdinato = schedulazioni;
                //                var schedOrdinato = uniEle;
                this.allRequests = schedulazioni;
                this.pmSchedulingModel.setProperty("/schedulazioni", schedOrdinato);
                if (schedOrdinato.length > 0 && schedOrdinato.length <= 6) {
                    this.pmSchedulingModel.setProperty("/nRighe", schedOrdinato.length);
                } else if (schedOrdinato.length > 6) {
                    this.pmSchedulingModel.setProperty("/nRighe", 6);
                } else {
                    this.pmSchedulingModel.setProperty("/nRighe", 1);
                }
                this.allScheduling = this.pmSchedulingModel.getProperty("/schedulazioni");
                this.visibleModel.setProperty("/filterButton", true);
                setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));
            } else {
                this.oDialogTable.setBusy(false);
                this._enterErrorFunction("error", "Errore", "Problemi nel caricamento dei dati");
            }
        };

        var fError = function (err) {
            this.oDialogTable.setBusy(false);
            this._enterErrorFunction("error", "Errore", "Errore lettura tabella: " + err);
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if (!this.ric.codPjm) {
            model.tabellaSchedulingPjmReadFinal.readFinalProjectRequests(req)
                .then(fSuccess, fError);
        } else {
            model.tabellaSchedulingPjmReadFinal.readFinalAllRequests(req)
                .then(fSuccess, fError);
        }
    },

    _onAfterRenderingTable: function (evt) {
        if (!this.oDialogTable.getBusy()) {
            this.oDialogTable.setBusy(true);
        }
        this._renderTabelle();

        this._calcoloGiornateTotali();

        this.oDialogTable.setBusy(false);
    },

    _renderTabelle: function () {
        this.arrayGiorniFestivi = [];
        var table = this.getView().byId("idSchedulazioniTable");
        var rows = table.getRows();
        var arrayCounter = [];
        for (var i = 0; i < rows.length; i++) {
            var cellsPerRows = rows[i].getAggregation("cells");
            var g = 0;
            for (var j = 1; j < cellsPerRows.length; j++) {
                var cella = cellsPerRows[j];
                if (_.find(this.weekendDays, _.bind(function (item) {
                        return (item == j);
                    }, this))) {
                    var column = $("#" + cella.getDomRef().id).parent().parent();
                    column.css('background-color', '#e5e5e5');
                    if (!this.arrayGiorniFestivi.includes(j)) {
                        this.arrayGiorniFestivi.push(j);
                    }
                }
            }
        }
    },

    _calcoloGiornateTotali: function () {
        var totalCount = 0;
        for (var i = 0; i < this.pmSchedulingModel.getProperty("/schedulazioni").length; i++) {
            totalCount = totalCount + this.pmSchedulingModel.getProperty("/schedulazioni")[i].giorniScheduling;
        }
        this.pmSchedulingModel.setProperty("/giorniTotali", totalCount);
        this.visibleModel.setProperty("/downloadExcelButton", true);
    },

    onLaunchpadPress: function () {
        if (this.administratorDialog) {
            this.administratorDialog.destroy();
            this.administratorDialog = undefined;
        }
        if (this.oDialog.getBusy()) {
            this.oDialog.setBusy(false);
        }
        if (this.oDialogTable.getBusy()) {
            this.oDialogTable.setBusy(false);
        }
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.router.navTo("launchpad");
    },

    svuotaTabella: function () {
        // svuoto la tabella
        this.oDialogTable.setVisible(true);
        this.pmSchedulingModel.setProperty("/schedulazioni", []);
        this.pmSchedulingModel.setProperty("/nRighe", 1);
    },

    svuotaSelect: function (value) {
        if (!value) {
            this.getView().byId("idSelectCommessa").setEnabled(false);
            this.getView().byId("idSelectCommessa").setSelectedKey("");
            this.getView().byId("idSelectCommessa").setValue("");
        }
    },

    onNotePress: function (evt) {
        var nota = evt.getSource().data("note");
        var jobName = evt.getSource().data("job");
        var job = _.find(this.getView().getModel("pmSchedulingModel").getProperty("/commesse"), {
            nomeCommessa: jobName
        });
        var consultant = evt.getSource().getBindingContext("pmSchedulingModel").getObject();
        this.noteModel.setProperty("/note", consultant[nota]);
        this.onAddNote();

    },

    onAddNote: function (evt) {
        if (!this._noteDialog) {
            this._noteDialog = sap.ui.xmlfragment(
                "view.dialog.noteDialog",
                this
            );
            this.getView().addDependent(this._noteDialog);
        }
        this.noteModel.setProperty("/editable", false);
        this.noteModel.refresh();
        this._noteDialog.open();
    },

    onAfterCloseNoteDialog: function (evt) {
        this.noteModel.setProperty("/note", undefined);
        this.pmSchedulingModel.refresh();
    },

    onCancelNoteDialogPress: function (evt) {
        if (this._noteDialog) {
            this._noteDialog.close();
            this.pmSchedulingModel.refresh();
        }
    },

    onFilterPress: function () {
        var listaConsulenti = [];
        var cloneSchedulazione = _.cloneDeep(this.allRequests);
        _.forEach(cloneSchedulazione, function (item) {
            var nome = item.nomeConsulente;
            if (item.codiceConsulente.indexOf("CCG") !== -1) {
                nome = nome + " " + item.nomeSkill;
            }
            listaConsulenti.push({
                nomeConsulente: nome,
                codConsulente: item.codiceConsulente,
                selected: false
            });
        });

        var listaBu = [];
        //        var listaBu = this.listBu;
        _.forEach(_.uniq(cloneSchedulazione, 'cod_consulenteBu'), _.bind(function (item) {
            if (item.cod_consulenteBu) {
                listaBu.push({
                    codBu: item.cod_consulenteBu,
                    descBu: _.find(this.listBu, {
                        codBu: item.cod_consulenteBu
                    }).descBu,
                    selected: false
                });
            }
        }, this));

        var listaSkill = [];
        if (this.ric.idStaff) {
            var skills = _.sortBy(_.uniq(_.compact(_.map(cloneSchedulazione, 'nomeSkill'))));
            _.forEach(skills, function (item) {
                listaSkill.push({
                    skill: item,
                    selected: false
                });
            });
        } else {
            var gorupSkill = _.map(cloneSchedulazione, 'skill');
            for (var t = 0; t < _.size(gorupSkill); t++) {
                var listaSkillGroup = gorupSkill[t];
                for (var k = 0; k < _.size(listaSkillGroup); k++) {
                    if (!_.find(listaSkill, {
                            skill: listaSkillGroup[k]
                        })) {
                        listaSkill.push({
                            skill: listaSkillGroup[k],
                            selected: false
                        });
                    }
                }
            }
        }

        var listaAziende = [];
        _.forEach(_.uniq(cloneSchedulazione, 'tipoAzienda'), _.bind(function (item) {
            if (item.tipoAzienda) {
                listaAziende.push({
                    nome: item.tipoAzienda,
                    selected: false
                });
            }
        }, this));

        this.adminFilterModel.setProperty("/listaConsulenti", listaConsulenti);
        this.adminFilterModel.setProperty("/listaBu", listaBu);
        this.adminFilterModel.setProperty("/listaSkill", listaSkill);
        this.adminFilterModel.setProperty("/listaAziende", listaAziende);

        var page = this.getView().byId("schedulingAdministratorPage");
        if (!this.administratorDialog) {
            this.administratorDialog = sap.ui.xmlfragment("view.dialog.filterDialogDraftFinal", this);
            page.addDependent(this.administratorDialog);
        }
        this.administratorDialog.setModel(this.adminFilterModel);
        this.administratorDialog.open();

        this.uiModel.setProperty("/searchValueCons", "");
        this.uiModel.setProperty("/searchValueBu", "");
        this.uiModel.setProperty("/searchValueSkill", "");
        var idListaConsulentiFiltro = sap.ui.getCore().byId("idListaConsulentiFiltroDF");
        var bindingIdListaConsulentiFiltro = idListaConsulentiFiltro.getBinding("items");
        bindingIdListaConsulentiFiltro.filter([], false);

        var idListaBuFiltro = sap.ui.getCore().byId("idListaBuFiltroDF");
        var bindingIdListaBuFiltro = idListaBuFiltro.getBinding("items");
        bindingIdListaBuFiltro.filter([], false);

        var idListaSkillFiltro = sap.ui.getCore().byId("idListaSkillFiltroDF");
        var bindingIdListaSkillFiltro = idListaSkillFiltro.getBinding("items");
        bindingIdListaSkillFiltro.filter([], false);
        //        var listaConsulenti = [];
        //        var cloneSchedulazione = _.cloneDeep(this.pmSchedulingModel.getProperty("/schedulazioni"));
        //        _.forEach(cloneSchedulazione, function (item) {
        //            listaConsulenti.push({
        //                nomeConsulente: item.nomeConsulente,
        //                codConsulente: item.codiceConsulente
        //            })
        //        });
        //
        //        var page = this.getView().byId("schedulingAdministratorPage");
        //        if (!this.administratorDialog) {
        //            this.administratorDialog = sap.ui.xmlfragment("view.dialog.filterDialogAdministrator", this);
        //            page.addDependent(this.administratorDialog);
        //        }
        //
        //        var fConsultantSuccess = function (result) {
        //            var res = _.sortByOrder(result.items, ['nomeCognome'], ['asc']);
        //            this.pmSchedulingModel.setProperty("/consultants", res);
        //            this.administratorDialog.open();
        //            this.visibleModel.setProperty("/consultants", true);
        //            var lista = sap.ui.getCore().getElementById("idListaConsulenti");
        //            var selectedConsultants = lista.getSelectedItems();
        //            var selectAllButton = sap.ui.getCore().byId("selectAllButtonAdmin");
        //            if (!!selectAllButton)
        //                if (selectedConsultants.length === 0) {
        //                    selectAllButton.setText(model.i18n._getLocaleText("SELECT_ALL"));
        //                } else {
        //                    selectAllButton.setText(model.i18n._getLocaleText("DESELECT_ALL"));
        //                }
        //        }.bind(this);
        //        var fConsultantError = function (err) {
        //            sap.m.MessageToast("error BU");
        //        }.bind(this);
        //        model.Consultants.readConsultants()
        //            .then(fConsultantSuccess, fConsultantError);
    },

    onFilterAdministratorSearch: function (evt) {
        if (evt) {
            var src = evt.getSource();
            this.searchValue = src.getValue();
        } else {
            this.searchValue = "";
        }
        var searchProperty = this.uiModel.getProperty("/searchProperty");
        this.applyFilterAdministrator(this.searchValue, searchProperty);
    },

    applyFilterAdministrator: function (value, params) {
        var list = sap.ui.getCore().getElementById("idListaConsulenti");
        if (!list.getBinding("items").oList || list.getBinding("items").oList.length === 0) {
            return;
        }
        var temp = list.getBinding("items").oList[0]; // a template just to recover the data types
        var filtersArr = [];

        if (!_.isEmpty(params)) {
            if (!_.isArray(params)) {
                params = [params];
            }
            for (var i = 0; i < params.length; i++) {
                switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
                    case "undefined":
                        break;
                    case "string":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
                        break;
                    case "number":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
                        break;
                }
            };
            var filter = new sap.ui.model.Filter({
                filters: filtersArr,
                and: false
            });
            list.getBinding("items").filter(filter);
            return;
        }
        list.getBinding("items").filter();
    },

    onFilterSelectAllConsultants: function (evt) {
        var src = evt.getSource();
        var txtButton = model.i18n._getLocaleText(src.getText());
        var lista = sap.ui.getCore().getElementById("idListaConsulenti");
        if (txtButton === "Seleziona tutto") {
            lista.selectAll();
            src.setText("Deseleziona tutto")
        } else {
            sap.ui.getCore().getElementById("idFilterSearchField").setValue("");
            this.onFilterAdministratorSearch();
            lista.removeSelections();
            src.setText("Seleziona tutto")
            this.filterConsultantsListCode = [];
        }
    },

    onFilterAdministratorDialogRipristino: function () {
        this.pmSchedulingModel.setProperty("/schedulazioni", this.allScheduling);
        if (_.size(this.allScheduling) > 0 && _.size(this.allScheduling) <= 7) {
            this.pmSchedulingModel.setProperty("/nRighe", _.size(this.allScheduling));
        } else if (_.size(this.allScheduling) > 7) {
            this.pmSchedulingModel.setProperty("/nRighe", 7);
        } else {
            this.pmSchedulingModel.setProperty("/nRighe", 1);
        }
        this.onFilterAdministratorDialogClose();
        setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));
        //        this.pmSchedulingModel.setProperty("/schedulazioni", this.allScheduling);
        //        var lunghezzaSchedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni").length;
        //        this.visibleModel.setProperty("/enabledButton", true);
        //        sap.ui.getCore().getElementById("idFilterSearchField").setValue("");
        //        this.onFilterAdministratorSearch();
        //        var lista = sap.ui.getCore().getElementById("idListaConsulenti");
        //        lista.removeSelections();
        //        this.filterConsultantsListCode = [];
        //        jQuery.sap.delayedCall(500, this, function () {
        //            this.onFilterAdministratorDialogClose();
        //        });
        //        this.leggiTabella(this.ric);
    },

    filterPress: function (evt) {
        var selectedConsultantCode = evt.getParameter("listItem").getBindingContext("pmSchedulingModel").getObject().codConsulente;
        var selected = evt.getParameter("selected");
        if (selected) {
            if (!this.filterConsultantsListCode) {
                this.filterConsultantsListCode = [];
            }
            this.filterConsultantsListCode.push(selectedConsultantCode);
        } else {
            for (var i = 0; i < this.filterConsultantsListCode.length; i++) {
                if (this.filterConsultantsListCode[i] === selectedConsultantCode) {
                    this.filterConsultantsListCode.splice(i, 1);
                    break;
                }
            }
        }
    },

    onFilterAdministratorDialogClose: function () {
        this.administratorDialog.close();
    },

    onFilterDialogOK: function () {
        var consulentiSelezionati = _.filter(this.adminFilterModel.getProperty("/listaConsulenti"), 'selected');
        var buSelezionate = _.filter(this.adminFilterModel.getProperty("/listaBu"), 'selected');
        var skillSelezionati = _.filter(this.adminFilterModel.getProperty("/listaSkill"), 'selected');
        var aziendeSelezionate = _.filter(this.adminFilterModel.getProperty("/listaAziende"), 'selected');

        var consulenti = [];
        if (_.size(consulentiSelezionati) > 0) {
            _.forEach(consulentiSelezionati, _.bind(function (o) {
                consulenti.push(_.find(_.cloneDeep(this.allRequests), {
                    codiceConsulente: o.codConsulente
                }))
            }, this));
        }

        if (_.size(buSelezionate) > 0) {
            if (_.size(consulenti) > 0) {
                var consulentiBu = [];
                _.forEach(buSelezionate, _.bind(function (o) {
                    consulenti = consulenti.concat(_.filter(consulenti, {
                        cod_bu_consulente: o.codBu
                    }));
                }, this));
                consulenti = consulentiBu;
            } else {
                _.forEach(buSelezionate, _.bind(function (o) {
                    consulenti = consulenti.concat(_.filter(_.cloneDeep(this.allRequests), {
                        cod_consulenteBu: o.codBu
                    }));
                }, this));
            }
        }
        if (_.size(skillSelezionati) > 0) {
            if (this.ric.idStaff) {
                if (_.size(consulenti) > 0) {
                    var consulentiSkill = [];
                    _.forEach(skillSelezionati, _.bind(function (o) {
                        consulentiSkill = consulentiSkill.concat(_.filter(consulenti, {
                            nomeSkill: o.skill
                        }));
                    }, this));
                    consulenti = consulentiSkill;
                } else {
                    _.forEach(skillSelezionati, _.bind(function (o) {
                        consulenti = consulenti.concat(_.filter(_.cloneDeep(this.allRequests), {
                            nomeSkill: o.skill
                        }));
                    }, this));
                }
            } else {
                if (_.size(consulenti) > 0) {
                    var consulentiSkillBis = [];
                    _.forEach(skillSelezionati, _.bind(function (o) {
                        _.filter(consulenti, function (item) {
                            _.find(item.skill, function (p) {
                                if (p === o.skill)
                                    consulentiSkillBis.push(item);
                            });
                        });
                    }, this));
                    consulenti = consulentiSkillBis;
                } else {
                    _.forEach(skillSelezionati, _.bind(function (o) {
                        _.filter(_.cloneDeep(this.allRequests), function (item) {
                            _.find(item.skill, function (p) {
                                if (p === o.skill)
                                    consulenti.push(item);
                            });
                        });
                    }, this));
                }
            }
        }
        if (_.size(aziendeSelezionate) > 0) {
            if (_.size(consulenti) > 0) {
                var consulentiAzienda = [];
                _.forEach(aziendeSelezionate, _.bind(function (o) {
                    consulentiAzienda = consulentiAzienda.concat(_.filter(consulenti, {
                        tipoAzienda: o.nome
                    }));
                }, this));
                consulenti = consulentiAzienda;
            } else {
                _.forEach(aziendeSelezionate, _.bind(function (o) {
                    consulenti = consulenti.concat(_.filter(_.cloneDeep(this.allRequests), {
                        tipoAzienda: o.nome
                    }));
                }, this));
            }
        }

        if (_.size(consulenti) === 0) {
            // nessun risultato per i parametri scelti
            this.visibleModel.setProperty("/downloadExcel", false);
        } else {
            this.visibleModel.setProperty("/downloadExcel", true);
        }
        this.pmSchedulingModel.setProperty("/schedulazioni", consulenti);
        if (_.size(consulenti) > 0 && _.size(consulenti) <= 7) {
            this.pmSchedulingModel.setProperty("/nRighe", _.size(consulenti));
        } else if (_.size(consulenti) > 7) {
            this.pmSchedulingModel.setProperty("/nRighe", 7);
        } else {
            this.pmSchedulingModel.setProperty("/nRighe", 1);
        }

        this.onFilterAdministratorDialogClose();
        setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));

        //        var selectedConsultantsCode = _.clone(this.filterConsultantsListCode);
        //        if (selectedConsultantsCode.length > 0) {
        //            var schedulazioni = [];
        //            for (var j = 0; j < selectedConsultantsCode.length; j++) {
        //                var schedulazione = _.find(this.allScheduling, {
        //                    'codiceConsulente': selectedConsultantsCode[j]
        //                });
        //                if (schedulazione) {
        //                    schedulazioni.push(schedulazione);
        //                }
        //            }
        //            this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioni);
        //        }
        //        var lunghezzaSchedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni").length;
        //        if (lunghezzaSchedulazioni === 0) {
        //            lunghezzaSchedulazioni = 1;
        //        }
        //        this.pmSchedulingModel.setProperty("/nRighe", lunghezzaSchedulazioni);
        //        this.pmSchedulingModel.refresh();
        //        jQuery.sap.delayedCall(500, this, function () {
        //            this.oDialogTable.setBusy(false);
        //            this._renderTabelle();
        //        });
        //        this.onFilterAdministratorDialogClose();
    },

    /* DOWNOLAD EXCEL */
    _scegliCommessa: function () {
        if (!this.chosedCodCom) {
            if (!this._dialogCommessaExcel) {
                this._dialogCommessaExcel = sap.ui.xmlfragment("view.dialog.selectCommessaExcelDialog", this);
                this.getView().addDependent(this._dialogCommessaExcel);
            }
            this._dialogCommessaExcel.setModel(this.getView().getModel("i18n"), "i18n");
            this._dialogCommessaExcel.open();
        } else {
            this._onDownloadExcelPress(this.chosedCodCom);
            this.selezioneCommesse = undefined;
        }
    },

    _onSelectionCommesse: function (evt) {
        var oList = evt.getSource();
        this.selezioneCommesse = oList.getSelectedContexts(true);
    },

    handleCancelSelectCommessaExcelDialog: function () {
        this._dialogCommessaExcel.close();
    },

    handleConfirmSelectCommessaExcelDialog: function () {
        if (this.selezioneCommesse && this.selezioneCommesse.length) {
            var arrayReq = [];
            for (var i = 0; i < this.selezioneCommesse.length; i++) {
                var path = parseInt(this.selezioneCommesse[i].getPath().split("/")[2]);
                arrayReq.push(this.selezioneCommesse[i].getModel().getProperty("/commesse")[path]);
            }

            var ric = {};
            ric.cod_consulente = (this.user).codice_consulente;
            ric.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));

            var arrConsulenti = [];
            var codiciCommesse = _.map(arrayReq, 'codCommessa');

            for (var i = 0; i < _.size(this.commessePerConsulente); i++) {
                if (this.commessePerConsulente[i].cod.indexOf("CCG") === -1) {
                    var commesse = this.commessePerConsulente[i].commesse;
                    _.forEach(codiciCommesse, _.bind(function (item) {
                        if (_.find(commesse, {
                                codCommessa: item
                            })) {
                            arrConsulenti.push(this.commessePerConsulente[i].cod)
                        }
                    }, this))
                }
            }
            var modelClone = _.cloneDeep(this.pmSchedulingModel.getProperty("/schedulazioni"));
            var modelDownloadExcel = [];
            for (var j = 0; j < _.size(arrConsulenti); j++) {
                var rowToPush = _.find(modelClone, {
                    codiceConsulente: arrConsulenti[j]
                });
                if (rowToPush) {
                    modelDownloadExcel.push(rowToPush);
                }
            }
            modelDownloadExcel = _.unique(modelDownloadExcel, 'codiceConsulente');
            this.excelModel.setProperty("/schedulazioni", _.sortByOrder(_.compact(modelDownloadExcel), 'nomeConsulente'));
            this._excelCommesseSelezionate(arrayReq)
        } else {
            sap.m.MessageToast.show(this._getLocaleText("NO_PROJECT_CHOSEN"));
        }
        this._dialogCommessaExcel.getAggregation("content")[0].removeSelections();
        this._dialogCommessaExcel.close();
    },

    _excelCommesseSelezionate: function (commesse) {
        var columns = [];
        var item = [];
        var rows = [];
        var rows0 = [];
        var column = {};
        var days = this.pmSchedulingModel.getProperty("/days");
        var daysLength = days.length;
        var schedulazioni = this.excelModel.getProperty("/schedulazioni");
        var schedulazioniLength = schedulazioni.length;
        for (var i = -2; i < daysLength; i++) {
            if (i === -2) {
                column = {
                    "name": this._getLocaleText("CONSULENTI")
                };
            } else if (i === -1) {
                column = {
                    "name": this._getLocaleText("SKILL")
                };
            } else {
                column = {
                    "name": days[i]
                };
            }
            columns.push(column);
        }
        for (var j = 0; j < schedulazioniLength; j++) {
            var row = [];
            for (var i = -2; i < daysLength; i++) {
                var item = "";
                if (i === -2) {
                    item = schedulazioni[j].nomeConsulente + ": " + schedulazioni[j].giorniScheduling;
                } else if (i === -1) {
                    var skills = schedulazioni[j].skill;
                    for (var x = 0; x < skills.length; x++) {
                        item += skills[x] + ", ";
                    }
                    item = item.substr(0, item.length - 2);
                } else {
                    var index = i + 1;
                    var property = "gg" + index;
                    var propertyA = "gg" + index + "a";
                    var icon = "ggIcon" + index;
                    var iconA = "ggIcon" + index + "a";
                    var css = "";
                    switch (schedulazioni[j][icon]) {
                        case "sap-icon://complete":
                            css = "EXassign";
                            break;
                        case "sap-icon://time-entry-request":
                            css = "EXhalfDay";
                            break;
                        case "sap-icon://customer-and-contacts":
                            css = "EXwCons";
                            break;
                        case "sap-icon://employee-rejections":
                            css = "EXwNonCons";
                            break;
                        case "sap-icon://collaborate":
                            css = "EXwCust";
                            break;
                        case "sap-icon://offsite-work":
                            css = "EXwNonCust";
                            break;
                        case "sap-icon://role":
                            css = "EXnotConfirmed";
                            break;
                        case "sap-icon://globe":
                            css = "EXabroad";
                            break;
                    }
                    // Controllo per doppie icone
                    var cssA = "";
                    switch (schedulazioni[j][iconA]) {
                        case "sap-icon://complete":
                            cssA = "EXassign";
                            break;
                        case "sap-icon://time-entry-request":
                            cssA = "EXhalfDay";
                            break;
                        case "sap-icon://customer-and-contacts":
                            cssA = "EXwCons";
                            break;
                        case "sap-icon://employee-rejections":
                            cssA = "EXwNonCons";
                            break;
                        case "sap-icon://collaborate":
                            cssA = "EXwCust";
                            break;
                        case "sap-icon://offsite-work":
                            cssA = "EXwNonCust";
                            break;
                        case "sap-icon://role":
                            cssA = "EXnotConfirmed";
                            break;
                        case "sap-icon://globe":
                            cssA = "EXabroad";
                            break;
                    }
                    var com = _.find(commesse, {
                        nomeCommessa: schedulazioni[j][property]
                    });
                    var comA = _.find(commesse, {
                        nomeCommessa: schedulazioni[j][propertyA]
                    });
                    if (com && !comA) {
                        item = schedulazioni[j][property] + "_" + css;
                        if (css === "EXwNonCust") {
                            item = "";
                        }
                    } else if (!com && comA) {
                        item = schedulazioni[j][propertyA] + "_" + cssA;
                        if (cssA === "EXwNonCust") {
                            item = "";
                        }
                    } else if (com && comA) {
                        item = schedulazioni[j][property] + "\n" + (schedulazioni[j][propertyA] + "_" + css);
                        if (cssA === "EXwNonCust") {
                            item = schedulazioni[j][property] + "_" + css;
                        } else if (css === "EXwNonCust") {
                            item = schedulazioni[j][propertyA] + "_" + cssA;
                        }
                    }
                }
                row.push(item);
            }
            var count = 0;
            for (var w = 2; w < _.size(row); w++) {
                if (row[w] !== "") {
                    count++;
                }
            }
            row[0] = row[0].split(": ")[0] + ": " + count;
            if (count !== 0) {
                rows.push(row);
            } else {
                rows0.push(row);
            }
        }
        rows = rows.concat(rows0);
        this.completeModel = new sap.ui.model.json.JSONModel();
        this.completeModel.setData({
            rows: rows,
            columns: columns
        });

        if (!this.excelDialog) {
            this.excelDialog = sap.ui.xmlfragment("view.dialog.excelDownload", this);
            this.getView().addDependent(this.excelDialog);
        }
        this.visibleModel.setProperty("/visibleExcelDialogButtons", false);
        this.excelDialog.open()
    },

    _onDownloadExcelPress: function (codCommessa) {
        var columns = [];
        var item = [];
        var rows = [];
        var column = {};
        var days = this.pmSchedulingModel.getProperty("/days");
        var daysLength = days.length;
        var schedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni");
        var schedulazioniLength = schedulazioni.length;
        for (var i = -2; i < daysLength; i++) {
            if (i === -2) {
                column = {
                    "name": this._getLocaleText("CONSULENTI")
                };
            } else if (i === -1) {
                column = {
                    "name": this._getLocaleText("SKILL")
                };
            } else {
                column = {
                    "name": days[i]
                };
            }
            columns.push(column);
        }

        var nomeCommessaSelezionata = _.find(this.allCommesse, {
            codCommessa: codCommessa
        }).nomeCommessa;

        for (var j = 0; j < schedulazioniLength; j++) {
            var row = [];
            for (var i = -2; i < daysLength; i++) {
                var item = "";
                if (i === -2) {
                    item = schedulazioni[j].nomeConsulente + ": " + schedulazioni[j].giorniScheduling;
                } else if (i === -1) {
                    item = schedulazioni[j].skill;
                } else {
                    var index = i + 1;
                    var property = "gg" + index;
                    var propertyA = "gg" + index + "a";
                    var icon = "ggIcon" + index;

                    var css = "";
                    switch (schedulazioni[j][icon]) {
                        case "sap-icon://complete":
                            css = "EXassign";
                            break;
                        case "sap-icon://time-entry-request":
                            css = "EXhalfDay";
                            break;
                        case "sap-icon://customer-and-contacts":
                            css = "EXwCons";
                            break;
                        case "sap-icon://employee-rejections":
                            css = "EXwNonCons";
                            break;
                        case "sap-icon://collaborate":
                            css = "EXwCust";
                            break;
                        case "sap-icon://offsite-work":
                            css = "EXwNonCust";
                            break;
                        case "sap-icon://role":
                            css = "EXnotConfirmed";
                            break;
                        case "sap-icon://globe":
                            css = "EXabroad";
                            break;
                    }
                    // la seguente proprietà mi serve per capire se nello stesso giorno un consulente ha due mezze giornate per la stessa commessa
                    if (schedulazioni[j][property] && schedulazioni[j][property] === nomeCommessaSelezionata) {
                        item = schedulazioni[j][property] + "_" + css;
                    }
                    // non mostro nell'excel le giornate non schedulabili
                    if (css === "EXwNonCust") {
                        item = "";
                    }
                }
                row.push(item);
            }
            rows.push(row);
        }
        this.completeModel = new sap.ui.model.json.JSONModel();
        this.completeModel.setData({
            rows: rows,
            columns: columns
        });

        if (!this.excelDialog) {
            this.excelDialog = sap.ui.xmlfragment("view.dialog.excelDownload", this);
            this.getView().addDependent(this.excelDialog);
        }
        this.visibleModel.setProperty("/visibleExcelDialogButtons", false);
        this.excelDialog.open();
    },

    afterExcelDialog: function () {
        var activityForm = sap.ui.getCore().getElementById("idExcelDialog");
        if (this.completeList) {
            activityForm.removeContent(this.completeList);
            this.completeList.destroy();
        }
        this.excelDialog.setBusy(true);
        this.completeList = new sap.m.List({
            id: "listaSchedulazioni",
            showSeparators: "All"
        });

        var columns = this.completeModel.getData().columns;

        for (var i = 0; i < columns.length; i++) {
            var minWidth = "15px";
            if (i === 0) {
                minWidth = "50px";
            }
            var columnName = columns[i].name;
            var splitColumnName = columnName.split(" ");
            if (splitColumnName[1] === "Mar") {
                splitColumnName[1] = "Mart"
                columnName = splitColumnName[0] + " " + splitColumnName[1];
            }
            this.completeList.addColumn(new sap.m.Column({
                width: minWidth,
                hAlign: "Left",
                vAlign: "Middle",
                header: new sap.m.Label({
                    text: columnName,
                    design: sap.m.LabelDesign.Bold
                })
            }));
        }

        var rows = this.completeModel.getData().rows;
        for (var j = 0; j < rows.length; j++) {
            var item = new sap.m.ColumnListItem({
                width: "100%"
            });
            for (var k = 0; k < rows[j].length; k++) {
                if (rows[j][k] !== "") {
                    var tx1 = rows[j][k].split("_")[0];
                    var tx2 = rows[j][k].split("_")[1];
                    item.addCell(new sap.m.Text({
                        text: tx1
                    }));
                    item.getCells()[k].addStyleClass(tx2);
                } else {
                    item.addCell(new sap.m.Text({
                        text: rows[j][k]
                    }));
                }
            }
            this.completeList.addItem(item);
        }
        activityForm.addContent(this.completeList);

        if (this.legend) {
            activityForm.removeContent(this.legend);
            this.legend.destroy();
        }
        this.legend = new sap.m.List({
            id: "legendaSchedulazioni",
            showSeparators: "All"
        });

        for (var i = 0; i < 8; i++) {
            var minWidth = "12.50%";
            this.legend.addColumn(new sap.m.Column({
                width: minWidth,
                hAlign: "Center",
                vAlign: "Middle",
            }));
        }

        var item = new sap.m.ColumnListItem({
            width: "100%"
        });
        item.addCell(new sap.m.Text({
            text: "Assegnata"
        }));
        item.getCells()[0].addStyleClass("EXassign");
        item.addCell(new sap.m.Text({
            text: "Mezza giornata"
        }));
        item.getCells()[1].addStyleClass("EXhalfDay");
        item.addCell(new sap.m.Text({
            text: "Concomitanza"
        }));
        item.getCells()[2].addStyleClass("EXwCons");
        item.addCell(new sap.m.Text({
            text: "Non concomitanza"
        }));
        item.getCells()[3].addStyleClass("EXwNonCons");

        item.addCell(new sap.m.Text({
            text: "Fissata"
        }));
        item.getCells()[4].addStyleClass("EXwCust");
        item.addCell(new sap.m.Text({
            text: "Non schedulabile"
        }));
        item.getCells()[5].addStyleClass("EXwNonCust");
        item.addCell(new sap.m.Text({
            text: "Non confermata"
        }));
        item.getCells()[6].addStyleClass("EXnotConfirmed");
        item.addCell(new sap.m.Text({
            text: "Estero"
        }));
        item.getCells()[7].addStyleClass("EXabroad");
        this.legend.addItem(item);
        activityForm.addContent(this.legend);

        jQuery.sap.delayedCall(5000, this, function () {
            this.createHTMLTable();
        });
    },

    createHTMLTable: function () {
        var nameFileExcel = "";
        var extension = ".xls";
        var period = _.find(this.periodModel.getData().items, {
            idPeriod: this.period
        });
        var mese = "";
        switch (period.mesePeriodo) {
            case (1):
                mese = "Gennaio";
                break;
            case (2):
                mese = "Febbraio";
                break;
            case (3):
                mese = "Marzo";
                break;
            case (4):
                mese = "Aprile";
                break;
            case (5):
                mese = "Maggio";
                break;
            case (6):
                mese = "Giugno";
                break;
            case (7):
                mese = "Luglio";
                break;
            case (8):
                mese = "Agosto";
                break;
            case (9):
                mese = "Settembre";
                break;
            case (10):
                mese = "Ottobre";
                break;
            case (11):
                mese = "Novembre";
                break;
            case (12):
                mese = "Dicembre";
                break;
        }

        nameFileExcel = mese + period.annoPeriodo;

        var commessaScelta;
        if (this.selezioneCommesse && _.size(this.selezioneCommesse) !== 0) {
            if (_.size(this.selezioneCommesse) === 1) {
                var path = parseInt(this.selezioneCommesse[0].getPath().split("/")[2]);
                commessaScelta = this.selezioneCommesse[0].getModel().getProperty("/commesse")[path].nomeCommessa;
                nameFileExcel = this.selezioneCommesse[0].getModel().getProperty("/commesse")[path].cognomeConsulente + "_" + commessaScelta + "_" + nameFileExcel;
            } else {
                nameFileExcel = this.user.cognomeConsulente + "_N_" + nameFileExcel;
            }
        } else {
            commessaScelta = this.getView().byId("idSelectCommessa").getSelectedItem().mProperties.text.split(" - ")[0];
            nameFileExcel = this.user.cognomeConsulente + "_" + commessaScelta + "_" + nameFileExcel;
        }

        var tab_text = "<table width='100%' border='2px'><tr bgcolor='#87AFC6'>";
        var textRange;
        var j = 0;

        var tab = document.getElementById('listaSchedulazioni-listUl');
        for (j = 0; j < tab.rows.length; j++) {
            var tabHeader = tab.rows[j];
            var childrens;
            var children;

            childrens = tabHeader.childNodes;
            this.numeroColonne = childrens.length;
            if (tabHeader.id === "listaSchedulazioni-tblHeader") {
                for (var i = 0; i < childrens.length; i++) {
                    if (childrens[i].id === "listaSchedulazioni-tblHead0") {
                        childrens[i].style.width = "150px";
                        childrens[i].style.columnWidth = "150px";
                        childrens[i].attributes.style = "width: 150px: text-align: left";
                    } else {
                        if (this.arrayGiorniFestivi.includes(i - 1)) {
                            childrens[i].bgColor = "#E5E5E5";
                        }
                        childrens[i].style.width = "50px";
                        childrens[i].style.columnWidth = "50px";
                        childrens[i].attributes.style = "width: 50px: text-align: left";
                    }
                }
            } else {
                for (var i = 0; i < childrens.length - 1; i++) {
                    var xx = childrens[i].getElementsByTagName("span")[0];
                    if (xx.textContent !== "" && i !== 0) {
                        var classes = xx.getAttribute("class").split(" ");
                        var classeIteressata = classes[0];
                        var css = "";
                        switch (classeIteressata) {
                            case "EXassign":
                                css = "mediumpurple";
                                break;
                            case "EXhalfDay":
                                css = "orange";
                                break;
                            case "EXwCons":
                                css = "yellow";
                                break;
                            case "EXwNonCons":
                                css = "chartreuse";
                                break;
                            case "EXwCust":
                                css = "red";
                                break;
                            case "EXwNonCust":
                                css = "";
                                break;
                            case "EXnotConfirmed":
                                css = "dodgerblue";
                                break;
                            case "EXabroad":
                                css = "lightskyblue";
                                break;
                        }
                        childrens[i].bgColor = css;
                    } else if (i > 0 && this.arrayGiorniFestivi.includes(i - 1)) {
                        childrens[i].bgColor = "#E5E5E5";
                    }
                }
            }
            var cellText = tab.rows[j].innerHTML;
            while (cellText.indexOf("à") > 0 || cellText.indexOf("è") > 0 || cellText.indexOf("é") > 0 || cellText.indexOf("ò") > 0 || cellText.indexOf("ù") > 0 || cellText.indexOf("ì") > 0) {
                if (cellText.indexOf("à") > 0) {
                    cellText = cellText.replace("à", "a'");
                }
                if (cellText.indexOf("è") > 0) {
                    cellText = cellText.replace("è", "e'");
                }
                if (cellText.indexOf("é") > 0) {
                    cellText = cellText.replace("é", "e'");
                }
                if (cellText.indexOf("ò") > 0) {
                    cellText = cellText.replace("ò", "o'");
                }
                if (cellText.indexOf("ù") > 0) {
                    cellText = cellText.replace("ù", "u'");
                }
                if (cellText.indexOf("ì") > 0) {
                    cellText = cellText.replace("ì", "i'");
                }
            }
            tab_text = tab_text + cellText + "</tr>";
        }

        var tabLegenda = document.getElementById('legendaSchedulazioni-listUl');
        var tabHeader = tabLegenda.rows[1];
        var rigaInteressata = tabHeader.childNodes;
        for (var i = 0; i < rigaInteressata.length - 1; i++) {
            var xx = rigaInteressata[i].getElementsByTagName("span")[0];
            if (xx.textContent !== "") {
                var testo = xx.textContent;
                var css = "";
                switch (testo) {
                    case "Assegnata":
                        css = "mediumpurple";
                        break;
                    case "Mezza giornata":
                        css = "orange";
                        break;
                    case "Concomitanza":
                        css = "yellow";
                        break;
                    case "Non concomitanza":
                        css = "chartreuse";
                        break;
                    case "Fissata":
                        css = "red";
                        break;
                    case "Non schedulabile":
                        css = "lightgrey";
                        break;
                    case "Non confermata":
                        css = "dodgerblue";
                        break;
                    case "Estero":
                        css = "lightskyblue";
                        break;
                }
                rigaInteressata[i].bgColor = css;
            }
        }

        var st = tabLegenda.rows[1].innerHTML
        var str = "";
        var ff = "";
        for (var i = 0; i < st.split("</td>").length - 2; i++) {
            str = st.split("</td>")[i] + "</td>";
            ff = ff.concat(str)
        }

        var colspan = parseInt(this.numeroColonne / 8);
        tab_text = tab_text + "<tr><td colspan='" + this.numeroColonne + " border='0'></td></tr><tr><td><span>Legenda</span></td>" + ff.replace(/<td /g, "<td colspan='" + colspan + "' ") + "</tr>";

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
        {
            iframetxtArea1.document.open("txt/html", "replace");
            iframetxtArea1.document.write(tab_text);
            iframetxtArea1.document.close();
            iframetxtArea1.focus();
            sa = iframetxtArea1.document.execCommand("SaveAs", true, "Scheduling_" + commessaScelta + extension);
            return (sa);
            this.onCloseDialogPress();

        } else {
            var filename;
            filename = nameFileExcel + extension;
            this.saveAsXLS(tab_text, filename);
        }
    },

    saveAsXLS: function (uri, filename) {
        var link = document.createElement('a');
        if (typeof link.download === 'string') {
            var file = new Blob([uri], {
                type: "application/vnd.ms-excel"
            });
            document.body.appendChild(link); // Firefox requires the link to be in the body
            link.download = filename;
            link.href = URL.createObjectURL(file);
            link.click();
            setTimeout(function () {
                document.body.removeChild(link); /*remove the link when done*/
            }, 2000);
        } else {
            location.replace(uri);
        }
        this.onCloseDialogPress();
    },

    saveAsHTML: function (uri, filename) {
        var link = document.createElement('a');
        if (typeof link.download === 'string') {
            var file = new Blob([uri], {
                type: "application/vnd.ms-excel"
            });
            document.body.appendChild(link); // Firefox requires the link to be in the body
            link.download = filename;
            link.href = URL.createObjectURL(file);
            link.click();
            setTimeout(function () {
                document.body.removeChild(link); /*remove the link when done*/
            }, 2000);
        } else {
            location.replace(uri);
        }
        this.onCloseDialogPress();
    },

    onCloseDialogPress: function () {
        this.excelDialog.close();
    },

});