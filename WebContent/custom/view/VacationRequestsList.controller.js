jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("model.SchedulingModifyRequests");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.Periods");
jQuery.sap.require("model.VacationRequests");
if (!model.requestSave)
    jQuery.sap.require("model.requestSave");

view.abstract.AbstractController.extend("view.VacationRequestsList", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.reqModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.reqModel, "reqModel");

        this.tempModelData = new sap.ui.model.json.JSONModel();

        this.pmSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingModel, "pmSchedulingModel");

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");

        this.noteModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteModel, "noteModel");

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);

        var name = evt.getParameter("name");
        this.name = name;

        if (!this._checkRoute(evt, "vacationRequestsList")) {
            return;
        }
        this.visibleModel.setProperty("/addHolidayButton", false);
        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.userModel.setData(this.user);

        var consultant = this.user.codice_consulente;
        this.req = {
            'codConsulente': consultant
        };
        this.oDialogTable = this.getView().byId("idSchedulazioniConsulenteFerieTable");
        this.oDialog = this.getView().byId("BlockLayoutConsulente");
        this.oDialogTable.setVisible(false);
        this.readPeriods();
    },

    readPeriods: function () {
        var fSuccess = function (result) {
            var res = {};
            _.remove(result.items, {
                stato: 'CHIUSO'
            });
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for (var i = 0; i < 13; i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.periodModel.setData(res13);
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");
            this.oDialog.setBusy(false);
            // verifico se sono tornato indietro dalla pagina di aggiunta ferie e se avevo selezionato precedentemente un periodo
            var idPeriod = model.persistence.Storage.session.get("selectedIdPeriod");
            if (idPeriod) {
                this.getView().byId("idSelectPeriodo").setSelectedKey(idPeriod);
                this.onChangePeriod();
            }
        };
        var fError = function () {
            sap.m.MessageBox.alert("Errore in lettura periodo", {
                title: "Attenzione"
            });
            this.oDialog.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    // funzione che, al cambio del periodo, popola la tabella con il nuovo periodo selezionato
    onChangePeriod: function (pe) {
        this.svuotaTabella();
        var table = this.getView().byId("idSchedulazioniConsulenteFerieTable");
        var period = "";

        if (pe && pe.getSource().getSelectedItem()) {
            this.pe = pe;
            var p = pe.getSource().getSelectedItem().getProperty("text").replace(/ /g, '');
            period = pe.getSource().getProperty("selectedKey");
        } else {
            period = model.persistence.Storage.session.get("selectedIdPeriod");
            model.persistence.Storage.session.remove("selectedIdPeriod");
        }

        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(period)
        });

        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();

        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = pInizio;
        var dataF = pFine;

        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

        //** rendo visibili solo le colonne del periodo selezionato
        var columns = table.getColumns();
        for (var i = 0; i < columns.length; i++) {
            columns[i].setVisible(true);
        }
        for (var i = this.numColonne; i < columns.length - 1; i++) {
            columns[i].setVisible(false);
        }
        columns[0].setVisible(false);

        //** metto i nomi dei giorni sulle colonne
        for (var i = 1; i < columns.length; i++) {
            columns[i - 1].getLabel().mProperties.text = this.arrayGiorni[i - 2];
        }

        var req = this.req;
        req.idPeriodo = parseInt(period);
        this.periodoDel = parseInt(period);
        req.idCommessa = "AA00000008";
        this.ric = req;

        this.leggiTabella(this.ric);
        //**
        var today = new Date();
        //**
        //mi salvo in sessione il periodo selezionato e non solo l'id 
        //per avere la possibilità di fare confronti alla nuova richiesta
        model.persistence.Storage.session.save("selectedPeriodFromHoliday", periodSelect);
        //**
        if (today < periodSelect.reqEndDate)
            this.visibleModel.setProperty("/addHolidayButton", true);
        else {
            this.visibleModel.setProperty("/addHolidayButton", false);
        }
        //**
        jQuery.sap.delayedCall(500, this, function () {
            this.oDialog.setBusy(false);
        });
    },

    _giorniMese: function (mese, anno) {
        var d = new Date(anno, mese, 0);
        return d.getDate();
    },

    onAfter: function (evt) {
        var table = this.getView().byId("idSchedulazioniConsulenteFerieTable");
        var rows = table.getRows();
        this.arrayGiorniFestivi = [];

        setTimeout(_.bind(function () {
            table.rerender()
            setTimeout(_.bind(function () {
                var arrayCounter = [];
                var column;
                for (var i = 0; i < rows.length; i++) {
                    var cellsPerRows = rows[i].getAggregation("cells");
                    var g = 0;
                    for (var j = 0; j < cellsPerRows.length - 1; j++) {
                        var cella = cellsPerRows[j];
                        if (_.find(this.weekendDays, _.bind(function (item) {
                                return (item == j + 1);
                            }, this))) {
                            column = $("#" + cella.getDomRef().id).parent().parent();
                            column.css('background-color', '#E5E5E5');
                            if (!this.arrayGiorniFestivi.includes(j)) {
                                this.arrayGiorniFestivi.push(j);
                            }
                        }
                    }
                }
                this.oDialogTable.setBusy(false);
            }, this));
        }, this));
    },

    uniqueElement: function (result) {
        var arrayfinale = result[0];
        var arrayGiornateSchedulate = result[0];

        var objCodiciConsulenti = {};
        for (var i = 0; i < arrayGiornateSchedulate.length; i++) {
            var c = "c" + i;
            if (arrayGiornateSchedulate[i].cod_consulente) {
                objCodiciConsulenti[c] = arrayGiornateSchedulate[i].cod_consulente;
            } else {
                objCodiciConsulenti[c] = arrayGiornateSchedulate[i].codiceConsulente;
            }
        }
        var objCodiciConsulenti2 = {};
        for (var i = 0; i < arrayGiornateSchedulate.length; i++) {
            var c = "d" + i;
            objCodiciConsulenti2[c] = arrayGiornateSchedulate[i].codiceConsulente;
        }

        _.merge(objCodiciConsulenti, objCodiciConsulenti2);

        var arrayfinale = [];
        for (var prop in objCodiciConsulenti) {
            var arr = _.where(arrayGiornateSchedulate, {
                codiceConsulente: objCodiciConsulenti[prop]
            });
            var obj = {};
            for (var i = 0; i < arr.length; i++) {
                for (var prop in arr[i]) {
                    if (!obj[prop]) {
                        obj[prop] = arr[i][prop]
                    } else {
                        if (prop.indexOf("gg") > -1 || prop.indexOf("nota") > -1 || prop.indexOf("req") > -1) {
                            var io = prop + "a";
                            while (obj[io]) {
                                io = io.concat("a");
                            }
                            obj[io] = arr[i][prop];
                        }
                    }
                }
            }
            if (!_.isEmpty(obj))
                arrayfinale.push(obj);
        }
        arrayfinale = _.reduce(arrayfinale, function (results, item) {
            return _.any(results, function (result) {
                return _.isEqual(result, item);
            }) ? results : results.concat([item]);
        }, []);

        return arrayfinale;
    },

    leggiTabella: function (req) {
        var fSuccess = function (results) {
            var schedulazioni = [];
            this.allScheduling = this.uniqueElement(results);
            if (results && results[0].length > 0) {
                var schedulazione = _.find(this.allScheduling, {
                    'codiceConsulente': results[0][0].codiceConsulente
                });
                if (schedulazione) {
                    schedulazioni.push(schedulazione);
                }
                this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioni);
            } else {
                this.pmSchedulingModel.setProperty("/schedulazioni", this.allScheduling);
                this.pmSchedulingModel.setProperty("/giorniTotali", "0");
            }

            this.pmSchedulingModel.setProperty("/nRighe", 1);
            var list = this.getView().byId("idSchedulazioniConsulenteFerieTable");
            var binding = list.getBinding();
            binding.filter([]);
            setTimeout(_.bind(this.onAfter, this));

            this.results = results;
            jQuery.sap.delayedCall(500, this, function () {
                this.oDialogTable.setBusy(false);
            });
            if (this.oDialog.getBusy() === true) {
                this.oDialog.setBusy(false);
            }
        };

        var fError = function (err) {
            if (this.oDialog.getBusy() === true) {
                this.oDialog.setBusy(false);
            }
            console.log(err);
            this.oDialogTable.setBusy(false);
            sap.m.MessageToast.show(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialogTable.setBusy(true);
        model.VacationRequests.readVacationRequests(req)
            .then(fSuccess, fError);
    },

    onLaunchpadPress: function () {
        this.onNavBackPress();
    },

    onAfterRendering: function () {
        this.pmSchedulingModel.refresh();
    },

    // svuoto la tabella
    svuotaTabella: function () {
        this.getView().byId("idSchedulazioniConsulenteFerieTable").setVisible(true);
        this.pmSchedulingModel.setProperty("/schedulazioni", []);
        this.pmSchedulingModel.setProperty("/nRighe", 1);
        this.pmSchedulingModel.getData().schedulazioni = [];
    },

    refreshTable: function () {
        this.leggiTabella(this.ric);
    },

    addNewModifySchedulingRequest: function () {
        var periodComboBox = this.getView().byId("idSelectPeriodo");
        var idPeriod = periodComboBox.getSelectedKey();
        if (idPeriod) {
            model.persistence.Storage.session.save("selectedIdPeriod", idPeriod);
            this.router.navTo("vacationRequestEdit", {
                state: "new"
            });
        } else {
            sap.m.MessageToast.show(this._getLocaleText("selectPeriodo"));
        }
    },

    handleMenuItemPress: function (evt) {
        if (evt.getParameter("item").getText() === this._getLocaleText("DELETE")) {
            sap.m.MessageBox.show(this._getLocaleText("CONFIRM_CANCEL_VACATION"),
                sap.m.MessageBox.Icon.ALLERT,
                this._getLocaleText("CONFIRM_CANCEL_TITLE_VACATION"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                _.bind(this._confirmSavePress, this)
            );
        } else {
            this.onAddNote();
        }
    },

    handleOpen: function (evt) {
        this.oButton = evt.getSource();
        var buttonId = this.oButton.sId;
        var myButtId = buttonId.match("button(.*)-col");
        this.giornoPeriodo = myButtId[1];

        if (!this._menu) {
            this._menu = sap.ui.xmlfragment("view.fragment.menuButtonsFerie", this);
            this.getView().addDependent(this._menu);
        }
        var eDock = sap.ui.core.Popup.Dock;
        var propNote = "nota" + this.giornoPeriodo;
        var nota = this.pmSchedulingModel.getData().schedulazioni[0][propNote];
        if (nota) {
            this.visibleModel.setProperty("/noteVisible", false);
        } else {
            this.visibleModel.setProperty("/noteVisible", true);
            this.noteModel.setProperty("/editable", true);
        }
        var propStato = "stato" + this.giornoPeriodo;

        if (_.find(this.pmSchedulingModel.getData().schedulazioni, function (o) {
                if (o[propStato] === "PROPOSED") {
                    return o;
                }
            })) {
            this.visibleModel.setProperty("/deleteVisible", true);
        } else {
            this.visibleModel.setProperty("/deleteVisible", false);
        }
        if (this.visibleModel.getProperty("/deleteVisible") || this.visibleModel.getProperty("/noteVisible"))
        	this._menu.open(undefined, this.oButton, eDock.BeginTop, eDock.BeginBottom, this.oButton);
        setTimeout(_.bind(this.onAfter, this));
    },

    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var codConsulente = this.user.codice_consulente;
            var idCommessa = "AA00000008";
            var idPeriodoSchedulingRic = this.periodoDel;
            var giornoPeriodo = parseInt(this.giornoPeriodo);

            var fSuccess = function (result) {
                this.leggiTabella(this.ric);
                sap.m.MessageToast.show(this._getLocaleText("VACATION_DELETED"));
            };
            var fError = function (err) {
                sap.m.MessageToast.show(this._getLocaleText("VACATION_DELETION_FAILED"));
                console.log(err);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            model.VacationRequests.deleteVacationRequest(codConsulente, idCommessa, idPeriodoSchedulingRic, giornoPeriodo)
                .then(fSuccess, fError);
        }
    },

    // funzione che apre il dialog delle note 'noteDialog'
    onAddNote: function () {
        if (!this._noteDialog) {
            this._noteDialog = sap.ui.xmlfragment("view.dialog.noteDialog", this);
            this.getView().addDependent(this._noteDialog);
        }
        this._noteDialog.open();
    },

    // funzione che viene lanciata alla chiusura del dialog delle note
    onAfterCloseNoteDialog: function () {
        setTimeout(_.bind(this.onAfter, this));
        this.noteModel.setProperty("/note", undefined);
    },

    // funzione che chiude il dialog delle note
    onCancelNoteDialogPress: function (evt) {
        if (this._noteDialog) {
            setTimeout(_.bind(this.onAfter, this));
            this._noteDialog.close();
        }
    },

    // funzione che salva le note
    onSaveNoteDialogPress: function () {
        var schedulazione = this.pmSchedulingModel.getData().schedulazioni[0];
        var prop = "gg" + this.giornoPeriodo;
        var propIcon = "ggIcon" + this.giornoPeriodo;
        var propNota = "nota" + this.giornoPeriodo;
        var propReq = "req" + this.giornoPeriodo;
        var propStato = "stato" + this.giornoPeriodo;
        var propDataRic = "ggDataRic" + this.giornoPeriodo;

        if (!this.dataForUpdate || this.dataForUpdate !== "") {
            this.dataForUpdate = {
                "codConsulente": schedulazione.codiceConsulente,
                "dataReqPeriodo": schedulazione[propDataRic],
                "idCommessa": "AA00000008",
                "codAttivitaRichiesta": schedulazione[propIcon],
                "idPeriodoSchedulingRic": this.periodoDel,
                "codPjm": "CC00000000",
                "dataInserimento": "",
                "note": schedulazione[propNota],
                "statoReq": schedulazione[propStato],
                "giorno": parseInt(this.giornoPeriodo),
                "id_req": schedulazione[propReq]
            };
        }

        var fSuccess = function (res) {
            this.onCancelNoteDialogPress();
            sap.m.MessageToast.show(this._getLocaleText("NOTE_ADDED"));
            var schedulazioneConsulente = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                codiceConsulente: this.dataForUpdate.codConsulente
            });
            var prop = "nota" + this.dataForUpdate.giorno;
            schedulazioneConsulente[prop] = this.dataForUpdate.note;
            this.pmSchedulingModel.refresh();
            this.dataForUpdate = "";
            this.notaConsultant = undefined;
            //aggiunta rilettura Francesco.
            this.leggiTabella(this.ric);
        };

        var fError = function (err) {
            sap.m.messageToast.show(this._getLocaleText("ERROR_NOTE_UPDATE"));
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if (this.notaConsultant === this.noteModel.getData().note) {
            this.onCancelNoteDialogPress();
        } else {
            this.dataForUpdate.note = this.noteModel.getData().note ? this.noteModel.getData().note : "";
            model.requestSave.updateNote(this.dataForUpdate)
                .then(fSuccess, fError);
        }
    },

    // funzione per la modifica delle note
    onNotePress: function (evt) {
        this.giornoPeriodo = evt.getSource().sId.match("col(.*)-row")[1];

        var nota = evt.getSource().data("note");
        var jobName = evt.getSource().data("job");
        var consultant = evt.getSource().getBindingContext("pmSchedulingModel").getObject();
        this.notaConsultant = consultant[nota];
        this.noteModel.setProperty("/note", consultant[nota]);
        var codReq = nota.substring(4);
        var propReq = "req" + codReq;
        var propStato = "stato" + codReq;
        var propDataRic = "ggDataRic" + codReq;
        var propIcon = "ggIcon" + codReq;

        this.dataForUpdate = {
            "codConsulente": consultant.codiceConsulente,
            "dataReqPeriodo": consultant[propDataRic],
            "idCommessa": "AA00000008",
            "codAttivitaRichiesta": consultant[propIcon] === "sap-icon://travel-itinerary" ? "travel-itinerary" : "bed",
            "idPeriodoSchedulingRic": this.getView().byId("idSelectPeriodo").getSelectedKey(),
            "codPjm": "CC00000000",
            "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "note": "",
            "statoReq": consultant[propStato],
            "giorno": codReq.indexOf("a") >= 0 ? parseInt(codReq.substr(0, codReq.length - 1)) : parseInt(codReq),
            "id_req": consultant[propReq] ? consultant[propReq].toString() : ""
        };
        if (consultant[propStato] === 'PROPOSED') {
        	this.noteModel.setProperty("/editable", true);
        } else {
        	this.noteModel.setProperty("/editable", false);
        }
        this.onAddNote();
    },

});
