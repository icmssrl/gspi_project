jQuery.sap.require("view.abstract.AbstractController");
if (!model.tabellaSchedulingAdmin)
    jQuery.sap.require("model.tabellaSchedulingAdmin");
if (!model.requestSave)
    jQuery.sap.require("model.requestSave");
if (!model.Consultants)
    jQuery.sap.require("model.Consultants");
if (!model.persistence.Storage)
    jQuery.sap.require("model.persistence.Storage");
if (!model.CommessePerStaffDelUm)
    jQuery.sap.require("model.CommessePerStaffDelUm");
if (!model.tabellaSchedulingUM)
    jQuery.sap.require("model.tabellaSchedulingUM");
if (!model.TabellaConsulentePeriodoFinal)
    jQuery.sap.require("model.TabellaConsulentePeriodoFinal");
if (!model.i18n)
    jQuery.sap.require("model.i18n");

view.abstract.AbstractController.extend("view.FinalSchedulingAll", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.pmSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingModel, "pmSchedulingModel");
        this.pmSchedulingModel.setSizeLimit(1000);

        this.umSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.umSchedulingModel, "umSchedulingModel");
        this.umSchedulingModel.setSizeLimit(1000);
        this.umSchedulingModel.setProperty("/filtroLocked", "0");

        this.deletedRequestsModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.deletedRequestsModel, "deletedRequestsModel");
        this.deletedRequestsModel.setSizeLimit(1000);

        this.pmSchedulingConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingConsultantModel, "pmSchedulingConsultantModel");

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");

        this.noteModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteModel, "noteModel");

        this.modelloIcone = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelloIcone, "iconModel");

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        this.filterLockedModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.filterLockedModel, "filterLockedModel");

        this.listaCommesseRicModModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.listaCommesseRicModModel, "listaCommesseRicModModel");

        this.iconJsonModel = {
            'full': "sap-icon://complete",
            'half': "sap-icon://time-entry-request",
            'wCons': "sap-icon://customer-and-contacts",
            'wCust': "sap-icon://collaborate",
            'wNonCons': "sap-icon://employee-rejections",
            'wNonCust': "sap-icon://offsite-work",
            'notConfirmed': "sap-icon://role",
            'del': "sap-icon://question-mark",
            'halfText': "1/2",
            'globe': "sap-icon://globe",
            'travel': "sap-icon://travel-itinerary",
            'bed': "sap-icon://bed"
        };

        this.modelloIcone.setProperty("/icon", this.iconJsonModel.del);

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");

        this.pModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pModel, "pModel");

        this.buModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.buModel, "buModel");

        this.skillModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.skillModel, "skillModel");

        this.consultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.consultantModel, "consultantModel");

        this.adminFilterModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.adminFilterModel, "adminFilterModel");

        this.uiModel.setProperty("/searchProperty", ["nomeCognome"]);

        this.noteConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteConsultantModel, "noteConsultantModel");
        this.noteConsultantModel.setSizeLimit(1000);

        this.assignGenericConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.assignGenericConsultantModel, "assignGenericConsultantModel");
        this.assignGenericConsultantModel.setSizeLimit(1000);

        this.listaCommesseConsulentiGenericiModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.listaCommesseConsulentiGenericiModel, "listaCommesseConsulentiGenericiModel");
        this.listaCommesseConsulentiGenericiModel.setSizeLimit(1000);
        
        this.requestTypeModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.requestTypeModel, "requestTypeModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");
        this.visibleModel.setProperty("/filterAfterCaricaTutto", false);

        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.userModel.setData(this.user);

        if (!this._checkRoute(evt, "finalSchedulingAll")) {
            return;
        }

        if (this._actionSheet)
            this._actionSheet.destroy(true);

        this.oDialog = this.getView().byId("BlockLayout");
        this.oDialogTable = this.getView().byId("idSchedulazioniTableAdmin");
        this.getView().byId("idSchedulazioniTableAdmin").setVisible(false);
        this.visibleModel.setProperty("/addConsultantButton", false);
        this.visibleModel.setProperty("/enabledButton", false);
        this.visibleModel.setProperty("/closeProjectButton", false);
        this.visibleModel.setProperty("/filterButton", false);
        this.visibleModel.setProperty("/sceltaCaricamento", false);
        this.visibleModel.setProperty("/caricaTutto", true);
        this.visibleModel.setProperty("/comboBoxCommesse", false);
        this.visibleModel.setProperty("/giornateTotali", false);
        this.visibleModel.setProperty("/pjmRelease", false);
        this.visibleModel.setProperty("/allRelease", false);
        this.visibleModel.setProperty("/checkChangeRequests", false);
        this.visibleModel.setProperty("/visibleAssignConsultant", false);
        this.visibleModel.setProperty("/downloadExcel", false);
        this.getView().byId("idButtonSelezionaCommessa").setPressed(false);
        this.getView().byId("idSelectCommessa").setSelectedKey("");
        this.getView().byId("idSelectCommessa").setValue("");
        this.uiModel.setProperty("/searchProperty", ["nomeCognome"]);
        this.uiModel.setProperty("/searchValue", "");
        if (sap.ui.getCore().getElementById("idListaConsulentiAdmin")) {
            sap.ui.getCore().getElementById("idListaConsulentiAdmin").removeSelections();
        }
        this.readPeriods();
        this.pmSchedulingModel.getData().giorniTotali = 0;
        this.visibleModel.setProperty("/chargeButton", false);

        //Inserted here filter lists Data in background//
//        this._loadFilters();

        //************************************************//
        
       	var group1= [{
	        	icon: 'complete',
	        	title: this._getLocaleText('ASSIGN')
	        },{
	        	icon: 'time-entry-request',
	        	title: this._getLocaleText('ASSIGN_HALF_DAY')
	        },{
	        	icon: 'customer-and-contacts',
	        	title: this._getLocaleText('C_WHIT_CONSULTANT')
	        },{
	        	icon: 'employee-rejections',
	        	title: this._getLocaleText('NON_C_WHIT_CONSULTANT')
	        },{
	        	icon: 'collaborate',
	        	title: this._getLocaleText('C_WHIT_CUSTOMER')
	        },{
	        	icon: 'role',
	        	title: this._getLocaleText('NON_CONFIRMED_CUSTOMER')
	        },{
	        	icon: 'globe',
	        	title: this._getLocaleText('ABROAD')
	        },{
        	icon: 'travel-itinerary',
        	title: this._getLocaleText('INTERAFERIE')
	        },{
	        	icon: 'bed',
	        	title: this._getLocaleText('MEZZAFERIE')
	        }];
        
        this.requestTypeModel.setProperty("/group1", group1);
    },

    readPeriods: function () {
        var fSuccess = function (result) {
            var res = {};
            _.remove(result.items, {
                stato: 'CHIUSO'
            });
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for (var i = 0; i < 13; i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.periodModel.setData(res13);
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");
            this.readStaffCommesseAdmin();
        };

        var fError = function () {
            if (!this.oneTimeReadPeriods) {
                // funzione che ripete la chiamata di login nel caso in cui sia scaduta l'autenticazione al sistema
                this.oneTimeReadPeriods = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(this.readPeriods(), this));
            } else {
                this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'getPeriods'");
            }
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    readStaffCommesseAdmin: function () {
        var fSuccess = function (result) {
            var arrConsulente = [];
            arrConsulente = _.uniq(result.items, 'codConsulente');
            for (var t = 0; t < arrConsulente.length; t++) {
                arrConsulente[t].commesse = _.where(result.items, {
                    codConsulente: arrConsulente[t].codConsulente,
                    stato: 'APERTO'
                });
            }
            this.commessePerConsulente = arrConsulente;
            this.readProjects();
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'readCommessePerUmSet'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        // chiamata che si adatta anke all'ADMIN
        model.CommessePerStaffDelUm.read()
            .then(fSuccess, fError);
    },

    readProjects: function () {
        var fSuccess = function (result) {
            _.remove(result.items, {
                'statoCommessa': "CHIUSO"
            });

            this.allCommesse = result.items;
            var comboCommessa = this.getView().byId("idSelectCommessa");
            if (comboCommessa) {
                comboCommessa.setValue(comboCommessa.getDefaultSelectedItem());
            }
            var commesseOrdinate = _.sortByOrder(this.allCommesse, 'nomeCommessa');
            this.pmSchedulingModel.setProperty("/commesse", []);
            this.pmSchedulingModel.setProperty("/commesse", commesseOrdinate);
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'readCommessePerUmSet'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Projects.readProjects()
            .then(fSuccess, fError);
    },

    onChangePeriod: function (pe) {
        this.filteredConsultants = undefined;
        this.oDialog.setBusy(true);
        this.visibleModel.setProperty("/closeProjectButton", false);
        this.visibleModel.setProperty("/filterButton", false);
        this.visibleModel.setProperty("/comboBoxCommesse", false);
        this.visibleModel.setProperty("/chargeButton", false);
        this.visibleModel.setProperty("/giornateTotali", false);
        this.visibleModel.setProperty("/pjmRelease", false);
        this.visibleModel.setProperty("/allRelease", false);
        this.visibleModel.setProperty("/checkChangeRequests", false);
        this.visibleModel.setProperty("/downloadExcel", false);
        model.persistence.Storage.session.remove("commessa");
        this.getView().byId("idButtonSelezionaCommessa").setPressed(false);
        this.svuotaSelect();
        this.svuotaTabella();

        if (pe.getSource().getSelectedItem().getProperty("additionalText") !== "RIL") {
            sap.m.MessageToast.show("Periodo non ancora rilasciato", {
                at: "center center"
            });
            this.oDialog.setBusy(false);
            this.visibleModel.setProperty("/sceltaCaricamento", false);
            this.visibleModel.setProperty("/caricaTutto", false);
            this.getView().byId("idSchedulazioniTableAdmin").setVisible(false);
            return;
        }

        this.visibleModel.setProperty("/sceltaCaricamento", true);
        this.visibleModel.setProperty("/caricaTutto", true);

        var table = this.getView().byId("idSchedulazioniTableAdmin");

        this.pe = pe;
        var p = pe.getSource().getSelectedItem().getProperty("text").replace(/ /g, '');

        this.tempP = p;
        var period = pe.getSource().getProperty("selectedKey");
        this.periodo = parseInt(period);
        model.persistence.Storage.session.save("period", period);
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(period)
        });
        model.persistence.Storage.session.save("periodSelected", periodSelect);

        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();

        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = pInizio;
        var dataF = pFine;

        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

        // ** rendo visibili solo le colonne del periodo selezionato
        var columns = table.getColumns();
        for (var i = 0; i < columns.length; i++) {
            columns[i].setVisible(true);
        }
        for (var i = this.numColonne; i < columns.length - 1; i++) {
            columns[i].setVisible(false);
        }
        // **

        // ** metto i nomi dei giorni sulle colonne
        for (var i = 2; i < columns.length; i++) {
            columns[i - 1].getLabel().mProperties.text = this.arrayGiorni[i - 2];
        }
        this.pmSchedulingModel.setProperty("/days", this.arrayGiorni);
        this.getView().byId("idSelectCommessa").setEnabled(true);

        this.periodStauts = pe.getSource().getSelectedItem().getProperty("additionalText");

        jQuery.sap.delayedCall(500, this, function () {
            this.oDialog.setBusy(false);
            if (this.getView().byId("idSelectPeriodo").getValue() === "") {
                this.getView().byId("idSelectPeriodo").setValue(this.tempP);
            }
        });
    },

    selectCommessaVisible: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            this.visibleModel.setProperty("/caricaTutto", false);
            this.visibleModel.setProperty("/comboBoxCommesse", true);
            this.visibleModel.setProperty("/giornateTotali", true);
        } else {
            this.visibleModel.setProperty("/caricaTutto", true);
            this.visibleModel.setProperty("/comboBoxCommesse", false);
            this.visibleModel.setProperty("/giornateTotali", false);
            this.visibleModel.setProperty("/closeProjectButton", false);
            this.visibleModel.setProperty("/filterButton", false);
            this.visibleModel.setProperty("/chargeButton", false);
            this.getView().byId("idSelectCommessa").setSelectedKey("");
            this.getView().byId("idSelectCommessa").setValue("");
        }
        this.svuotaTabella();
        this.pmSchedulingModel.setProperty("/giorniTotali", 0);
        model.persistence.Storage.session.remove("commessa");
        this.visibleModel.setProperty("/checkChangeRequests", false);
        this.visibleModel.setProperty("/downloadExcel", false);
    },

    chargeProjectsNotSelected: function () {
        if (model.persistence.Storage.session.get("commessa")) {
            model.persistence.Storage.session.remove("commessa");
        }
        this.getView().byId("idSelectCommessa").setSelectedKey("");
        this.getView().byId("idSelectCommessa").setValue("");
        var req = {};
        req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
        this.ric = req;
        this.commessaSelezionata = undefined;
        this.chosedCom = undefined;
        this.pmSchedulingModel.setProperty("/commessa", "");
        this.leggiTabella(req);
    },

    onSelectionChangeCommessa: function (evt) {
        this.visibleModel.setProperty("/chargeButton", true);
        this.chosedCom = evt.getSource().getValue().split(" - ")[0];
        this.chosedComDescrizione = evt.getSource().getValue().split(" - ")[1];
        this.commessaSelezionata = evt.getSource().getSelectedKey();
        this.pmSchedulingModel.setProperty("/commessa", this.chosedCom);
    },

    chargeProjects: function (evt) {
        this.visibleModel.setProperty("/chargeButton", false);
    },

    onChangeCommessa: function (evt) {
        this.oDialog.setBusy(true);
        if (!this.commessaSelezionata) {
            this._enterErrorFunction("alert", "Attenzione", "Commessa '" + this.chosedCom.toUpperCase() + "' non trovata");
            return;
        }
        var fSuccess = function (res) {
            this.currentStaff = res.items;
            if (this.currentStaff.length === 0) {
                this.oDialog.setBusy(false);
                this.oDialogTable.setBusy(false);
                sap.m.MessageToast.show("Nesuno staff presente per questa commessa");
                return;
            }
            this._onChangeCommessa(this.commessaSelezionata);
        };

        var fError = function () {
            sap.m.MessageToast.show("errore in lettura anagrafica staff");
            this.oDialogTable.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialogTable.setBusy(true);
        model.StaffCommesseView.readStaffSingolaCommessa(this.commessaSelezionata)
            .then(fSuccess, fError);
        this.visibleModel.setProperty("/chargeButton", false);
    },

    _onChangeCommessa: function (commessaP) {
        this.filteredConsultants = undefined;
        if (commessaP) {
            var commessaScelta = commessaP;

            var c = _.find(this.pmSchedulingModel.getData().commesse, {
                codCommessa: commessaScelta
            });
            model.persistence.Storage.session.save("commessa", c);

            var req = {};
            req.idStaff = this.currentStaff[0].codStaff;
            req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
            req.nomeCommessa = this.chosedCom;
            req.idCommessa = commessaScelta;
            this.ric = req;
            this.leggiTabella(req);
            this.visibleModel.setProperty("/visibleTable", true);
            var dataRichiesta = model.persistence.Storage.session.get("periodSelected").reqEndDate;
        }
        this.oDialog.setBusy(false);
        this.oDialogTable.setBusy(false);
    },

    leggiTabella: function (req) {
        this.oDialogTable.setBusy(true);
        this.visibleModel.setProperty("/visibleAssignConsultant", false);
        var fSuccess = function (res) {
            if (res && res.resultOrdinato) {
                var schedulazioni;
                var resto = 385;
                var rows = 7;
                if (this.ric.idStaff) {
                    resto = $(window).height() - 370;
                    schedulazioni = res.resultOrdinato;
                    this.allSchedulingForFilter = _.cloneDeep(schedulazioni);
                    var listaConsulentiGenerici = _.filter(schedulazioni, function (item) {
                        if (item.codiceConsulente.indexOf("CCG") >= 0)
                            return item
                    });
                } else {
                    resto = $(window).height() - 270;
                    schedulazioni = res.resultOrdinato;
                    for (var t = 0; t < schedulazioni.length; t++) {
                        var skills = schedulazioni[t].skill;
                        var skillInModel = "";
                        for (var c = 0; c < _.size(skills); c++) {
                            if (c === 0) {
                                skillInModel = skills[c];
                            } else {
                                skillInModel = skillInModel + ", " + skills[c];
                            }
                        }
                        schedulazioni[t].teamLeader = "";
                        schedulazioni[t].nomeSkill = skillInModel ? skillInModel : "";
                    }
                    this.allSchedulingForFilter = _.cloneDeep(schedulazioni);
                }
                var schedOrdinato = schedulazioni;
                this.allRequests = schedulazioni;
                this.umSchedulingModel.setProperty("/schedulazioni", this.allRequests);
                this.pmSchedulingModel.setProperty("/schedulazioni", schedOrdinato);
                rows = Math.round(resto / 55);
                if (schedOrdinato.length > 0 && schedOrdinato.length <= rows) {
                    this.pmSchedulingModel.setProperty("/nRighe", schedOrdinato.length);
                } else if (schedOrdinato.length > rows) {
                    this.pmSchedulingModel.setProperty("/nRighe", rows);
                } else {
                    this.pmSchedulingModel.setProperty("/nRighe", 1);
                }
                this.allScheduling = this.pmSchedulingModel.getProperty("/schedulazioni");
                setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));
                this.visibleModel.setProperty("/filterButton", true);
            } else {
                this.oDialogTable.setBusy(false);
                this._enterErrorFunction("error", "Errore", "Problemi nel caricamento dei dati");
            }
        };

        var fError = function (err) {
            this.oDialogTable.setBusy(false);
            this._enterErrorFunction("error", "Errore", "Errore lettura tabella: " + JSON.parse(err.responseText).error);
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if (!req.idStaff) {
            // legge tutti i consulenti di tutte le commesse
            model.tabellaSchedulingFinalIcms.readAllRequests(req)
                .then(fSuccess, fError);
        } else {
            // legge tutti i consulenti di una singola commessa
            model.tabellaSchedulingFinalIcms.readSingleProject(req)
                .then(fSuccess, fError);
        }
    },

    _onAfterRenderingTable: function () {
        if (!this.oDialogTable.getBusy()) {
            this.oDialogTable.setBusy(true);
        }
        this._renderTabelle();
        this._calcoloGiornateTotali();

        this.visibleModel.setProperty("/downloadExcel", true);

        setTimeout(_.bind(function () {
            this.oDialogTable.setBusy(false);
        }, this), 500);
    },

    _renderTabelle: function () {
        this.arrayGiorniFestivi = [];
        var table = this.getView().byId("idSchedulazioniTableAdmin");
        var rows = table.getRows();
        var arrayCounter = [];
        for (var i = 0; i < rows.length; i++) {
            var cellsPerRows = rows[i].getAggregation("cells");
            var g = 0;
            for (var j = 1; j < cellsPerRows.length - 1; j++) {
                var cella = cellsPerRows[j];
                if (_.find(this.weekendDays, _.bind(function (item) {
                        return (item == j);
                    }, this))) {
                    var column = $("#" + cella.getDomRef().id).parent().parent();
                    column.css('background-color', '#e5e5e5');
                    if (!this.arrayGiorniFestivi.includes(j)) {
                        this.arrayGiorniFestivi.push(j);
                    }
                }
            }
        }
    },

    _calcoloGiornateTotali: function () {
        var totalCount = 0;
        for (var i = 0; i < this.pmSchedulingModel.getProperty("/schedulazioni").length; i++) {
            totalCount = totalCount + this.pmSchedulingModel.getProperty("/schedulazioni")[i].giorniScheduling;
        }
        this.pmSchedulingModel.setProperty("/giorniTotali", totalCount);
    },

    onCloseDeletedRequestDialog: function () {
        this.dialogRichiesteCancellate.close();
    },

    onLaunchpadPress: function () {
        if (this.administratorDialog) {
            this.administratorDialog.destroy();
            this.administratorDialog = undefined;
        }
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.router.navTo("launchpad");
    },

    svuotaTabella: function () {
        // svuoto la tabella
        this.getView().byId("idSchedulazioniTableAdmin").setVisible(true);
        this.pmSchedulingModel.setProperty("/schedulazioni", []);
        this.pmSchedulingModel.setProperty("/nRighe", 1);
        this.pmSchedulingModel.getData().schedulazioni = [];
    },

    svuotaSelect: function (value) {
        if (!value) {
            this.getView().byId("idSelectCommessa").setEnabled(false);
            this.getView().byId("idSelectCommessa").setSelectedKey("");
            this.getView().byId("idSelectCommessa").setValue("");
        }
    },

    onFilterPress: function () {
        var listaConsulenti = [];
        var cloneSchedulazione = _.cloneDeep(this.allRequests);
        _.forEach(cloneSchedulazione, function (item) {
            var nome = item.nomeConsulente;
            if (item.codiceConsulente.indexOf("CCG") !== -1) {
                nome = nome + " " + item.nomeSkill;
            }
            listaConsulenti.push({
                nomeConsulente: nome,
                codConsulente: item.codiceConsulente,
                selected: false
            });
        });

        var listaBu = [];
        _.forEach(_.uniq(cloneSchedulazione, 'cod_bu_consulente'), _.bind(function (item) {
            if (item.cod_bu_consulente) {
                listaBu.push({
                    codBu: item.cod_bu_consulente,
                    descBu: item.desc_bu_consulente,
                    selected: false
                });
            }
        }, this));

        var listaSkill = [];
        if (this.ric.idStaff) {
            var skills = _.sortBy(_.uniq(_.compact(_.map(cloneSchedulazione, 'nomeSkill'))));
            _.forEach(skills, function (item) {
                listaSkill.push({
                    skill: item,
                    selected: false
                });
            });
        } else {
            var gorupSkill = _.map(cloneSchedulazione, 'skill');
            for (var t=0; t<_.size(gorupSkill); t++) {
                var listaSkillGroup = gorupSkill[t];
                for (var k=0; k<_.size(listaSkillGroup); k++) {
                    if (!_.find(listaSkill, {skill: listaSkillGroup[k]})) {
                        listaSkill.push({
                            skill: listaSkillGroup[k],
                            selected: false
                        });
                    }
                }
            }
        }
        
        var listaAziende = [];
        if (this.ric.idStaff) {
            this.uiModel.setProperty("/visibleFiltroAziende", false);
        } else {
            this.uiModel.setProperty("/visibleFiltroAziende", true);
            _.forEach(_.uniq(cloneSchedulazione, 'tipoAzienda'), _.bind(function (item) {
                if (item.tipoAzienda) {
                    listaAziende.push({
                        nome: item.tipoAzienda,
                        selected: false
                    });
                }
            }, this));
        }

        this.adminFilterModel.setProperty("/listaConsulenti", listaConsulenti);
        this.adminFilterModel.setProperty("/listaBu", listaBu);
        this.adminFilterModel.setProperty("/listaSkill", listaSkill);
        this.adminFilterModel.setProperty("/listaAziende", listaAziende);

        var page = this.getView().byId("finalSchedulingAllId");
        if (!this.administratorDialog) {
            this.administratorDialog = sap.ui.xmlfragment("view.dialog.filterDialogFinalAll", this);
            page.addDependent(this.administratorDialog);
        }
        this.administratorDialog.setModel(this.adminFilterModel);
        this.administratorDialog.open();

        this.uiModel.setProperty("/searchValueCons", "");
        this.uiModel.setProperty("/searchValueBu", "");
        this.uiModel.setProperty("/searchValueSkill", "");
        var idListaConsulentiFiltro = sap.ui.getCore().byId("idListaConsulentiFiltro");
        var bindingIdListaConsulentiFiltro = idListaConsulentiFiltro.getBinding("items");
        bindingIdListaConsulentiFiltro.filter([], false);

        var idListaBuFiltro = sap.ui.getCore().byId("idListaBuFiltro");
        var bindingIdListaBuFiltro = idListaBuFiltro.getBinding("items");
        bindingIdListaBuFiltro.filter([], false);

        var idListaSkillFiltro = sap.ui.getCore().byId("idListaSkillFiltro");
        var bindingIdListaSkillFiltro = idListaSkillFiltro.getBinding("items");
        bindingIdListaSkillFiltro.filter([], false);
    },

    onFilterAdministratorDialogRipristino: function () {
        this.pmSchedulingModel.setProperty("/schedulazioni", this.allScheduling);
        if (_.size(this.allScheduling) > 0 && _.size(this.allScheduling) <= 7) {
            this.pmSchedulingModel.setProperty("/nRighe", _.size(this.allScheduling));
        } else if (_.size(this.allScheduling) > 7) {
            this.pmSchedulingModel.setProperty("/nRighe", 7);
        } else {
            this.pmSchedulingModel.setProperty("/nRighe", 1);
        }
        this.onFilterAdministratorDialogClose();
        setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));
    },

//    filterPress: function (evt) {
//        var selectedConsultantCode = evt.getParameter("listItem").getBindingContext("pmSchedulingModel").getObject().codConsulente;
//        var selected = evt.getParameter("selected");
//        if (selected) {
//            if (!this.filterConsultantsListCode) {
//                this.filterConsultantsListCode = [];
//            }
//            this.filterConsultantsListCode.push(selectedConsultantCode);
//        } else {
//            for (var i = 0; i < this.filterConsultantsListCode.length; i++) {
//                if (this.filterConsultantsListCode[i] === selectedConsultantCode) {
//                    this.filterConsultantsListCode.splice(i, 1);
//                    break;
//                }
//            }
//        }
//    },

    onFilterAdministratorDialogClose: function () {
        this.administratorDialog.close();
        setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));
    },

    onFilterDialogOK: function () {
        var consulentiSelezionati = _.filter(this.adminFilterModel.getProperty("/listaConsulenti"), 'selected');
        var buSelezionate = _.filter(this.adminFilterModel.getProperty("/listaBu"), 'selected');
        var skillSelezionati = _.filter(this.adminFilterModel.getProperty("/listaSkill"), 'selected');
        var aziendeSelezionate = _.filter(this.adminFilterModel.getProperty("/listaAziende"), 'selected');

        var consulenti = [];
        if (_.size(consulentiSelezionati) > 0) {
            _.forEach(consulentiSelezionati, _.bind(function (o) {
                consulenti.push(_.find(_.cloneDeep(this.allRequests), {
                    codiceConsulente: o.codConsulente
                }))
            }, this));
        }

        if (_.size(buSelezionate) > 0) {
            if (_.size(consulenti) > 0) {
                var consulentiBu = [];
                _.forEach(buSelezionate, _.bind(function (o) {
                    consulenti = consulenti.concat(_.filter(consulenti, {
                        cod_bu_consulente: o.codBu
                    }));
                }, this));
                consulenti = consulentiBu;
            } else {
                _.forEach(buSelezionate, _.bind(function (o) {
                    consulenti = consulenti.concat(_.filter(_.cloneDeep(this.allRequests), {
                        cod_bu_consulente: o.codBu
                    }));
                }, this));
            }
        }

        if (_.size(skillSelezionati) > 0) {
            if (this.ric.idStaff) {
                if (_.size(consulenti) > 0) {
                    var consulentiSkill = [];
                    _.forEach(skillSelezionati, _.bind(function (o) {
                        consulentiSkill = consulentiSkill.concat(_.filter(consulenti, {
                            nomeSkill: o.skill
                        }));
                    }, this));
                    consulenti = consulentiSkill;
                } else {
                    _.forEach(skillSelezionati, _.bind(function (o) {
                        consulenti = consulenti.concat(_.filter(_.cloneDeep(this.allRequests), {
                            nomeSkill: o.skill
                        }));
                    }, this));
                }
            } else {
                if (_.size(consulenti) > 0) {
                    var consulentiSkillBis = [];
                    _.forEach(skillSelezionati, _.bind(function (o) {
                        _.filter(consulenti, function (item) {
                            _.find(item.skill, function (p) {
                                if (p === o.skill)
                                    consulentiSkillBis.push(item);
                            });
                        });
                    }, this));
                    consulenti = consulentiSkillBis;
                } else {
                    _.forEach(skillSelezionati, _.bind(function (o) {
                        _.filter(_.cloneDeep(this.allRequests), function (item) {
                            _.find(item.skill, function (p) {
                                if (p === o.skill)
                                    consulenti.push(item);
                            });
                        });
                    }, this));
                }
            }
        }
        if (_.size(aziendeSelezionate) > 0) {
            if (_.size(consulenti) > 0) {
                var consulentiAzienda = [];
                _.forEach(aziendeSelezionate, _.bind(function (o) {
                    consulentiAzienda = consulentiAzienda.concat(_.filter(consulenti, {
                        tipoAzienda: o.nome
                    }));
                }, this));
                consulenti = consulentiAzienda;
            } else {
                _.forEach(aziendeSelezionate, _.bind(function (o) {
                    consulenti = consulenti.concat(_.filter(_.cloneDeep(this.allRequests), {
                        tipoAzienda: o.nome
                    }));
                }, this));
            }
        }
        
        if (_.size(consulenti) === 0) {
            // nessun risultato per i parametri scelti
            this.visibleModel.setProperty("/downloadExcel", false);
        } else {
            this.visibleModel.setProperty("/downloadExcel", true);
        }
        this.pmSchedulingModel.setProperty("/schedulazioni", consulenti);
        if (_.size(consulenti) > 0 && _.size(consulenti) <= 7) {
            this.pmSchedulingModel.setProperty("/nRighe", _.size(consulenti));
        } else if (_.size(consulenti) > 7) {
            this.pmSchedulingModel.setProperty("/nRighe", 7);
        } else {
            this.pmSchedulingModel.setProperty("/nRighe", 1);
        }

        this.onFilterAdministratorDialogClose();
        setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));
    },

    onSearchConsulentiFiltro: function (evt) {
        var value = evt.getSource().getValue();
        var filters = [];

        if (value && value.length > 0) {
            var nomeConsulente = new sap.ui.model.Filter("nomeConsulente", sap.ui.model.FilterOperator.Contains, value);
            var codConsulente = new sap.ui.model.Filter("codConsulente", sap.ui.model.FilterOperator.Contains, value);
            filters = new sap.ui.model.Filter({
                filters: [nomeConsulente, codConsulente],
                and: false
            });
        }

        var list = sap.ui.getCore().byId("idListaConsulentiFiltro");
        var binding = list.getBinding("items");
        binding.filter(filters, false);
    },

    onSearchBuFiltro: function (evt) {
        var value = evt.getSource().getValue();
        var filters = [];

        if (value && value.length > 0) {
            var descBu = new sap.ui.model.Filter("descBu", sap.ui.model.FilterOperator.Contains, value);
            filters = new sap.ui.model.Filter({
                filters: [descBu],
                and: false
            });
        }

        var list = sap.ui.getCore().byId("idListaBuFiltro");
        var binding = list.getBinding("items");
        binding.filter(filters, false);
    },

    onSearchSkillFiltro: function (evt) {
        var value = evt.getSource().getValue();
        var filters = [];

        if (value && value.length > 0) {
            var skill = new sap.ui.model.Filter("skill", sap.ui.model.FilterOperator.Contains, value);
            filters = new sap.ui.model.Filter({
                filters: [skill],
                and: false
            });
        }

        var list = sap.ui.getCore().byId("idListaSkillFiltro");
        var binding = list.getBinding("items");
        binding.filter(filters, false);
    },

    _onDownloadExcelPress: function () {
        var columns = [];
        var item = [];
        var rows = [];
        var column = {};
        var days = this.pmSchedulingModel.getProperty("/days");
        var daysLength = days.length;
        var schedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni");
        var schedulazioniLength = schedulazioni.length;
        for (var i = -2; i < daysLength; i++) {
            if (i === -2) {
                column = {
                    "name": this._getLocaleText("CONSULENTI")
                };
            } else if (i === -1) {
                column = {
                    "name": this._getLocaleText("SKILL")
                };
            } else {
                column = {
                    "name": days[i]
                };
            }
            columns.push(column);
        }
        for (var j = 0; j < schedulazioniLength; j++) {
            var row = [];
            for (var i = -2; i < daysLength; i++) {
                var item = "";
                if (i === -2) {
                    var consultantName;
                    // se è un consulente generico, allora mostro lo skill
                    if (schedulazioni[j].codiceConsulente.indexOf("CCG") >= 0) {
                        consultantName = this._getLocaleText("GENERIC_CONSULTANT") + ":\n" + schedulazioni[j].nomeSkill;
                    } else {
                        consultantName = schedulazioni[j].nomeConsulente;
                    }
                    item = consultantName;
                } else if (i === -1) {
                    var skills = schedulazioni[j].skill;
                    if (skills) {
                        for (var x = 0; x < skills.length; x++) {
                            item += skills[x] + ", ";
                        }
                        item = item.substr(0, item.length - 2);
                    } else {
                        item = schedulazioni[j].nomeSkill ? schedulazioni[j].nomeSkill : "";
                    }
                } else {
                    var index = i + 1;
                    var property = "gg" + index;
                    var propertyA = "gg" + index + "a";
                    var icon = "ggIcon" + index;
                    var iconA = "ggIcon" + index + "a";

                    var css = "";
                    switch (schedulazioni[j][icon]) {
                        case "sap-icon://complete":
                            css = "EXassign";
                            break;
                        case "sap-icon://time-entry-request":
                            css = "EXhalfDay";
                            break;
                        case "sap-icon://customer-and-contacts":
                            css = "EXwCons";
                            break;
                        case "sap-icon://employee-rejections":
                            css = "EXwNonCons";
                            break;
                        case "sap-icon://collaborate":
                            css = "EXwCust";
                            break;
                        case "sap-icon://offsite-work":
                            css = "EXwNonCust";
                            break;
                        case "sap-icon://role":
                            css = "EXnotConfirmed";
                            break;
                        case "sap-icon://globe":
                            css = "EXabroad";
                            break;
                    }
                    var com = _.find(this.allCommesse, {
                        nomeCommessa: schedulazioni[j][property]
                    });
                    var comA = _.find(this.allCommesse, {
                        nomeCommessa: schedulazioni[j][propertyA]
                    });
                    if (com && !comA) {
                        item = schedulazioni[j][property] + "_" + css;
                    } else if (!com && comA) {
                        item = schedulazioni[j][propertyA] + "_" + css;
                    } else if (com && comA) {
                        item = schedulazioni[j][property] + "\n" + (schedulazioni[j][propertyA] + "_" + css);
                    }
                }
                row.push(item);
            }
            rows.push(row);
        }

        this.completeModel = new sap.ui.model.json.JSONModel();
        this.completeModel.setData({
            rows: rows,
            columns: columns
        });

        if (!this.excelDialog) {
            this.excelDialog = sap.ui.xmlfragment("view.dialog.excelDownloadDraftPJM", this);
            this.getView().addDependent(this.excelDialog);
        }
        this.excelDialog.open();
    },

    afterExcelDialog: function () {
        var activityForm = sap.ui.getCore().getElementById("idExcelDialogDraftPjm");
        if (this.completeList) {
            activityForm.removeContent(this.completeList);
            this.completeList.destroy();
        }
        this.excelDialog.setBusy(true);
        this.completeList = new sap.m.List({
            id: "listaSchedulazioniDraftPjm",
            showSeparators: "All"
        });
        var columns = this.completeModel.getData().columns;
        for (var i = 0; i < columns.length; i++) {
            var minWidth = "15px";
            if (i === 0) {
                minWidth = "50px";
            }
            var columnName = columns[i].name;
            var splitColumnName = columnName.split(" ");
            if (splitColumnName[1] === "Mar") {
                splitColumnName[1] = "Mart"
                columnName = splitColumnName[0] + " " + splitColumnName[1];
            }
            this.completeList.addColumn(new sap.m.Column({
                width: minWidth,
                hAlign: "Left",
                vAlign: "Middle",
                header: new sap.m.Label({
                    text: columnName,
                    design: sap.m.LabelDesign.Bold
                })
            }));
        }

        var rows = this.completeModel.getData().rows;
        for (var j = 0; j < rows.length; j++) {
            var item = new sap.m.ColumnListItem({
                width: "100%"
            });
            for (var k = 0; k < rows[j].length; k++) {
                if (rows[j][k]) {
                    var tx1 = rows[j][k].split("_")[0];
                    var tx2 = rows[j][k].split("_")[1];
                    item.addCell(new sap.m.Text({
                        text: tx1
                    }));
                    if (tx2) {
                        item.getCells()[k].addStyleClass(tx2);
                    }
                } else {
                    item.addCell(new sap.m.Text({
                        text: ""
                    }));
                }
            }
            this.completeList.addItem(item);
        }
        activityForm.addContent(this.completeList);

        if (this.legend) {
            activityForm.removeContent(this.legend);
            this.legend.destroy();
        }
        this.legend = new sap.m.List({
            id: "legendaSchedulazioniDraftPjm",
            showSeparators: "All"
        });

        for (var i = 0; i < 8; i++) {
            var minWidth = "12.50%";
            this.legend.addColumn(new sap.m.Column({
                width: minWidth,
                hAlign: "Center",
                vAlign: "Middle",
            }));
        }

        var item = new sap.m.ColumnListItem({
            width: "100%"
        });
        item.addCell(new sap.m.Text({
            text: "Assegnata"
        }));
        item.getCells()[0].addStyleClass("EXassign");
        item.addCell(new sap.m.Text({
            text: "Mezza giornata"
        }));
        item.getCells()[1].addStyleClass("EXhalfDay");
        item.addCell(new sap.m.Text({
            text: "Concomitanza"
        }));
        item.getCells()[2].addStyleClass("EXwCons");
        item.addCell(new sap.m.Text({
            text: "Non concomitanza"
        }));
        item.getCells()[3].addStyleClass("EXwNonCons");

        item.addCell(new sap.m.Text({
            text: "Fissata"
        }));
        item.getCells()[4].addStyleClass("EXwCust");
        item.addCell(new sap.m.Text({
            text: "Non schedulabile"
        }));
        item.getCells()[5].addStyleClass("EXwNonCust");
        item.addCell(new sap.m.Text({
            text: "Non confermata"
        }));
        item.getCells()[6].addStyleClass("EXnotConfirmed");
        item.addCell(new sap.m.Text({
            text: "Estero"
        }));
        item.getCells()[7].addStyleClass("EXabroad");
        this.legend.addItem(item);
        activityForm.addContent(this.legend);

        jQuery.sap.delayedCall(5000, this, function () {
            this.createHTMLTable();
        });
    },

    createHTMLTable: function () {
        var nameFileExcel = "ICMS_";
        var period = _.find(this.periodModel.getData().items, {
            idPeriod: this.periodo
        });
        var mese = "";
        switch (period.mesePeriodo) {
            case (1):
                mese = "Gennaio";
                break;
            case (2):
                mese = "Febbraio";
                break;
            case (3):
                mese = "Marzo";
                break;
            case (4):
                mese = "Aprile";
                break;
            case (5):
                mese = "Maggio";
                break;
            case (6):
                mese = "Giugno";
                break;
            case (7):
                mese = "Luglio";
                break;
            case (8):
                mese = "Agosto";
                break;
            case (9):
                mese = "Settembre";
                break;
            case (10):
                mese = "Ottobre";
                break;
            case (11):
                mese = "Novembre";
                break;
            case (12):
                mese = "Dicembre";
                break;
        }
        if (this.chosedCom) {
            nameFileExcel += this.chosedCom + "_";
        }
        nameFileExcel += mese + period.annoPeriodo;

        var tab_text = "<table width='100%' border='2px'><tr bgcolor='#87AFC6'>";
        var textRange;
        var j = 0;
        var tab = document.getElementById('listaSchedulazioniDraftPjm-listUl'); // id of table

        for (j = 0; j < tab.rows.length; j++) {
            var tabHeader = tab.rows[j];
            var childrens;
            var children;

            childrens = tabHeader.childNodes;
            this.numeroColonne = childrens.length;
            if (tabHeader.id === "listaSchedulazioniDraftPjm-tblHeader") {
                for (var i = 0; i < childrens.length; i++) {
                    if (childrens[i].id === "listaSchedulazioniDraftPjm-tblHead0") {
                        childrens[i].style.width = "150px";
                        childrens[i].style.columnWidth = "150px";
                        childrens[i].attributes.style = "width: 150px: text-align: left";
                    } else {
                        if (this.arrayGiorniFestivi.includes(i - 1)) {
                            childrens[i].bgColor = "#E5E5E5";
                        }
                        childrens[i].style.width = "50px";
                        childrens[i].style.columnWidth = "50px";
                        childrens[i].attributes.style = "width: 50px: text-align: left";
                    }
                }
            } else {
                for (var i = 0; i < childrens.length - 1; i++) {
                    var xx = childrens[i].getElementsByTagName("span")[0];
                    if (xx.textContent && i > 0) {
                        var classes = xx.getAttribute("class").split(" ");
                        var classeIteressata = classes[0];

                        var css = "";
                        switch (classeIteressata) {
                            case "EXassign":
                                css = "mediumpurple";
                                break;
                            case "EXhalfDay":
                                css = "orange";
                                break;
                            case "EXwCons":
                                css = "yellow";
                                break;
                            case "EXwNonCons":
                                css = "chartreuse";
                                break;
                            case "EXwCust":
                                css = "red";
                                break;
                            case "EXwNonCust":
                                css = "lightgrey";
                                break;
                            case "EXnotConfirmed":
                                css = "dodgerblue";
                                break;
                            case "EXabroad":
                                css = "lightskyblue";
                                break;
                        }
                        childrens[i].bgColor = css;
                    } else if (i > 0 && this.arrayGiorniFestivi.includes(i - 1)) {
                        childrens[i].bgColor = "#E5E5E5";
                    }
                }
            }
            var cellText = tab.rows[j].innerHTML;
            while (cellText.indexOf("à") > 0 || cellText.indexOf("è") > 0 || cellText.indexOf("é") > 0 || cellText.indexOf("ò") > 0 || cellText.indexOf("ù") > 0 || cellText.indexOf("ì") > 0) {
                if (cellText.indexOf("à") > 0) {
                    cellText = cellText.replace("à", "a'");
                }
                if (cellText.indexOf("è") > 0) {
                    cellText = cellText.replace("è", "e'");
                }
                if (cellText.indexOf("é") > 0) {
                    cellText = cellText.replace("é", "e'");
                }
                if (cellText.indexOf("ò") > 0) {
                    cellText = cellText.replace("ò", "o'");
                }
                if (cellText.indexOf("ù") > 0) {
                    cellText = cellText.replace("ù", "u'");
                }
                if (cellText.indexOf("ì") > 0) {
                    cellText = cellText.replace("ì", "i'");
                }
            }
            tab_text = tab_text + cellText + "</tr>";
        }

        var tabLegenda = document.getElementById('legendaSchedulazioniDraftPjm-listUl');
        var tabHeader = tabLegenda.rows[1];
        var rigaInteressata = tabHeader.childNodes;
        for (var i = 0; i < rigaInteressata.length - 1; i++) {
            var xx = rigaInteressata[i].getElementsByTagName("span")[0];
            if (xx.textContent !== "") {
                var testo = xx.textContent;
                var css = "";
                switch (testo) {
                    case "Assegnata":
                        css = "mediumpurple";
                        break;
                    case "Mezza giornata":
                        css = "orange";
                        break;
                    case "Concomitanza":
                        css = "yellow";
                        break;
                    case "Non concomitanza":
                        css = "chartreuse";
                        break;
                    case "Fissata":
                        css = "red";
                        break;
                    case "Non schedulabile":
                        css = "lightgrey";
                        break;
                    case "Non confermata":
                        css = "dodgerblue";
                        break;
                    case "Estero":
                        css = "lightskyblue";
                        break;
                }
                rigaInteressata[i].bgColor = css;
            }
        }

        var st = tabLegenda.rows[1].innerHTML
        var str = "";
        var ff = "";
        for (var i = 0; i < st.split("</td>").length - 2; i++) {
            str = st.split("</td>")[i] + "</td>";
            ff = ff.concat(str)
        }

        var colspan = parseInt(this.numeroColonne / 8);
        tab_text = tab_text + "<tr><td colspan='" + this.numeroColonne + " border='0'></td></tr><tr><td><span>Legenda</span></td>" + ff.replace(/<td /g, "<td colspan='" + colspan + "' ") + "</tr>";

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
        {
            iframetxtArea1.document.open("txt/html", "replace");
            iframetxtArea1.document.write(tab_text);
            iframetxtArea1.document.close();
            iframetxtArea1.focus();
            sa = iframetxtArea1.document.execCommand("SaveAs", true, nameFileExcel + extension);
            return (sa);
            this.onCloseDialogPress();

        } else {
            var filename;
            filename = nameFileExcel + ".xls";
            this.saveAsXLS(tab_text, filename);
        }
    },

    saveAsXLS: function (uri, filename) {
        var link = document.createElement('a');
        if (typeof link.download === 'string') {
            var file = new Blob([uri], {
                type: "application/vnd.ms-excel"
            });
            document.body.appendChild(link); // Firefox requires the link to be in the body
            link.download = filename;
            link.href = URL.createObjectURL(file);
            link.click();
            setTimeout(function () {
                document.body.removeChild(link); /*remove the link when done*/
            }, 2000);
        } else {
            location.replace(uri);
        }
        this.excelDialog.setBusy(false);
        this.excelDialog.close();
    }
});
