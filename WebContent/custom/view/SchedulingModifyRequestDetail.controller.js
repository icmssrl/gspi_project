jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.SingleSchedModRequest");
jQuery.sap.require("model.SchedulingModifyRequests");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");

view.abstract.AbstractController.extend("view.SchedulingModifyRequestDetail", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        
        this.detailRequest = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailRequest, "detailRequest");
        
        this.detaiRichiesteModificheScambio = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detaiRichiesteModificheScambio, "detaiRichiesteModificheScambio");
        
        this.visibleDetail = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleDetail, "visibleDetail");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "modifyRequestDetail") {
            return;
        };
        
        this.requestId = evt.getParameters().arguments.detailId;
        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        
        this.oDialog = this.getView().byId("schedulingdetailRequestPage");
        this.visibleDetail.setProperty("/scambio", false);
        
        this.requestSelected = model.persistence.Storage.session.get("selectedModifyRequest");
        this.detailRequest.setData(this.requestSelected);
        
        if(this.requestSelected.statoReqModifica !== "PROPOSED"){
            this.visibleDetail.setProperty("/stato", false);
        } else {
            this.visibleDetail.setProperty("/stato", true);
        }
        
        this.requestSelectedScambio = model.persistence.Storage.session.get("modifyRequestDetailScambio");
        if(this.requestSelectedScambio) {
            this.detaiRichiesteModificheScambio.setData(this.requestSelectedScambio);
            this.visibleDetail.setProperty("/scambio", true);
        }
    },

	modifyRequestPress: function () {
        this.router.navTo("modifyRequestEdit", {
            state: "modify",
            detailId: this.requestId
        });
    },

    deleteRequestPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_DELETE_REQUEST"),
            sap.m.MessageBox.Icon.ALERT,
            this._getLocaleText("CONFIRM_DELETE_REQUEST_TITLE"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmDeletePress, this)
        );
    },

    _confirmDeletePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var fSuccess = function () {
                if(this.requestSelectedScambio) {
                    var fSuccessScambio = function () {
                        this.oDialog.setBusy(false);
                        this.router.navTo("modifyRequestsList");
                    };
                    
                    fSuccessScambio = _.bind(fSuccessScambio, this);
                    
                    model.SchedulingModifyRequests.deleteSingleSchedModRequest(this.requestSelectedScambio.idReqModifica)
                        .then(fSuccessScambio, fError);
                } else {
                    this.oDialog.setBusy(false);
                    this.router.navTo("modifyRequestsList");
                }
            }
            var fError = function () {
                this.oDialog.setBusy(false);
                console.log("errore durante la richiesta di cancellazione!");
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            model.SchedulingModifyRequests.deleteSingleSchedModRequest(this.requestId)
                .then(fSuccess, fError);
        }
    }
    
});
