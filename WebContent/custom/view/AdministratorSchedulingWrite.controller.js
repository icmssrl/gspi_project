jQuery.sap.require("view.abstract.AbstractController");
if (!model.Periods) {
    jQuery.sap.require("model.Periods");
}
if (!model.tabellaSchedulingAdmin) {
    jQuery.sap.require("model.tabellaSchedulingAdmin");
}
if (!model.TabellaConsulentePeriodoFinal)
    jQuery.sap.require("model.TabellaConsulentePeriodoFinal");

view.abstract.AbstractController.extend("view.AdministratorSchedulingWrite", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        //        if (name !== "administratorSchedulingWrite")
        //            return;

        if (!this._checkRoute(evt, "administratorSchedulingWrite")) {
            return;
        }

        this.oDialog = this.getView().byId("BlockLayout");
        this.visibleModel.setProperty("/writeRequestsButton", false);
        this.readPeriods();
    },

    // funzione che legge i periodi, e visualizza gli ultimi 12 in ordine decrescente
    readPeriods: function () {
        var fSuccess = function (result) {
            var res = {};
            _.remove(result.items, {
                stato: 'CHIUSO'
            });
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for (var i = 0; i < 13; i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.periodModel.setData(res13);
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");
            this.oDialog.setBusy(false);
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'getPeriods'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    // funzione che al cambio del periodo verifica se la tabella 'GSPI_STATO_CONSULENTE_PERIODO_FINAL' è già stata popolata per il periodo selezioanto e visualizza il tasto di scrivi richieste
    onChangePeriod: function (pe) {
        this.oDialog.setBusy(true);

        this.visibleModel.setProperty("/writeRequestsButton", false);

        var fSuccess = function (res) {
            if (res === "OK") {
                this.consulentiPresentiNelPeriodo = true;
            } else {
                this.consulentiPresentiNelPeriodo = false;
            }
            this.visibleModel.setProperty("/writeRequestsButton", true);
            this.oDialog.setBusy(false);
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Problemi nella lettura della tabella 'GSPI_STATO_CONSULENTE_PERIODO_FINAL'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.period = pe.getSource().getProperty("selectedKey");

        model.TabellaConsulentePeriodoFinal.check(parseInt(this.period))
            .then(fSuccess, fError);
    },

    // al premere del tasto chiede conferma per la scrittura dei dati
    writeRequestsPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_WRITE_REQUEST_FINAL"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_WRITE_REQUEST_FINAL_TITLE"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmWriteRequest, this)
        );
    },

    // se la tabella 'GSPI_STATO_CONSULENTE_PERIODO_DRAFT' è già stata scritta esegue solo la scrittura delle richieste
    _confirmWriteRequest: function (evt) {
        if (evt === sap.m.MessageBox.Action.YES) {
            if (!this.consulentiPresentiNelPeriodo) {
                this.writeConsultants();
            } else {
                this.confirmWrite();
            }
        } else {
            return;
        }
    },

    // cancellazione, e scrittura delle richieste
    confirmWrite: function () {
        var fLetturaRichiesteError = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageToast.show(this._getLocaleText("erroreLetturaRichieste"));
        };
        var fCancellazioneRichiesteError = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageToast.show(this._getLocaleText("erroreCancellazioneRichieste"));
        };
        var fLetturaRichiesteSuccess = function (ris) {
            this.console(JSON.parse(ris).status, "Success");
            this.oDialog.setBusy(false);
            this.visibleModel.setProperty("/writeRequestsButton", false);
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");
            sap.m.MessageToast.show(this._getLocaleText("SUCCESSO_SCRITTURA_RICHIESTE"));
        };
        var fCancellazioneRichiesteSuccess = function (ris) {
            if (ris.indexOf("OK") >= 0) {
                model.tabellaSchedulingAdmin.readRequests(this.period)
                    .then(fLetturaRichiesteSuccess, fLetturaRichiesteError);
            }
        };

        fLetturaRichiesteError = _.bind(fLetturaRichiesteError, this);
        fCancellazioneRichiesteError = _.bind(fCancellazioneRichiesteError, this);
        fLetturaRichiesteSuccess = _.bind(fLetturaRichiesteSuccess, this);
        fCancellazioneRichiesteSuccess = _.bind(fCancellazioneRichiesteSuccess, this);

        this.oDialog.setBusy(true);

        model.tabellaSchedulingAdmin.deleteRequestsFromFinal(this.period)
            .then(fCancellazioneRichiesteSuccess, fCancellazioneRichiesteError);
    },

    // scrittura della tabella 'GSPI_STATO_CONSULENTE_PERIODO_FINAL'
    writeConsultants: function () {
        this.oDialog.setBusy(true);
        var fSuccess = function (res) {
            this.visibleModel.setProperty("/writeRequestsButton", false);
            this.oDialog.setBusy(false);
            this.confirmWrite();
        };

        var fError = function () {
            sap.m.MessageToast.show("Problemi nella scrittura della tabella 'GSPI_STATO_CONSULENTE_PERIODO_FINAL'");
            this.oDialog.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.TabellaConsulentePeriodoFinal.write(parseInt(this.period))
            .then(fSuccess, fError);
    },

    onLaunchpadPress: function () {
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.onNavBackPress();
    },

});