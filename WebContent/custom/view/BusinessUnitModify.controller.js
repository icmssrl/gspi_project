jQuery.sap.require("view.abstract.AbstractController");

view.abstract.AbstractController.extend("view.BusinessUnitModify", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.detailBu = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailBu, "detailBu");

        this.statusModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.statusModel, "statusModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);

        var name = evt.getParameter("name");
        if (name !== "businessUnitModify") {
            return;
        };

        this.oDialog = this.getView().byId("idBusinessUnitModify");
        this.state = evt.getParameters().arguments.state;

        if (this.state === "new") {
            this.detailBu.setProperty("/codiceBu", "");
            this.detailBu.setProperty("/nomeBu", "");
            this.detailBu.setProperty("/descrizioneBu", "");
        } else {
            var codBu = evt.getParameters().arguments.detailId;
            this._getSingleRecordBu(codBu);
        }

        this.statusModel.setData([{
            text: "APERTO",
            key: "APERTO"
        }, {
            text: "CHIUSO",
            key: "CHIUSO"
        }]);
        this.getView().byId("idStatoBu").setSelectedKey("APERTO");
    },

    _getSingleRecordBu: function (codBu) {
        var fSuccess = function (result) {
            this.detailBu.setProperty("/codiceBu", result.items[0].codiceBu);
            this.detailBu.setProperty("/nomeBu", result.items[0].nomeBu);
            this.detailBu.setProperty("/descrizioneBu", result.items[0].descrizioneBu);
            this.detailBu.setProperty("/statoBu", result.items[0].statoBu);
        };

        var fError = function () {
            sap.m.MessageToast("errore lettura oData BusineUnit");
            console.log("errore in lettura business unit");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.BusinessUnit.readBu(codBu)
            .then(fSuccess, fError);
    },

    saveBuPress: function () {
        if (this.detailBu.getProperty("/descrizioneBu") === "") {
            sap.m.MessageBox.show(this._getLocaleText("INSERT_DESCRIPTION"),
                sap.m.MessageBox.Icon.ALLERT);
            return;
        }
        sap.m.MessageBox.show(this._getLocaleText("confirmSaveBu"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("confirmSaveTitleBu"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmSavePress, this)
        );
    },

    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {

            var fSuccess = function () {
                this._clearInputValue();
                this.oDialog.setBusy(false);
                this.router.navTo("businessUnitList");
            };
            var fError = function () {
                console.log("errore nel salvataggio");
                this.oDialog.setBusy(false);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            var nomeBu = this.getView().byId("idNomeBu").getValue();
            var descrizioneBu = this.getView().byId("idDescrBu").getValue();
            var statoBu = this.getView().byId("idStatoBu").getSelectedKey();
            
            if (this.state === "new") {
                model.BusinessUnit.addBu(nomeBu, descrizioneBu, statoBu)
                    .then(fSuccess, fError);
            } else {
                var codiceBu = this.getView().byId("idCodBu").getValue();
                model.BusinessUnit.updateBu(nomeBu, descrizioneBu, codiceBu, statoBu)
                    .then(fSuccess, fError);
            };
        };
    },

    _clearInputValue: function () {
        this.getView().byId("idCodBu").setValue("");
        this.getView().byId("idDescrBu").setValue("");
        this.getView().byId("idStatoBu").setSelectedKey("APERTO");
    },

    cancelModifyPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("confirmCancelBu"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("confirmCancelTitleBu"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmCancelPress, this)
        );
    },

    _confirmCancelPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            this._clearInputValue();
            this.router.navTo("businessUnitList");
        };
    }
});
