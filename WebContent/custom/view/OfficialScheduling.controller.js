jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.LocalStorage");
jQuery.sap.require("model.tabellaSchedulingOfficial");
//jQuery.sap.require("model.Consultants");
jQuery.sap.require("model.persistence.Storage");
//jQuery.sap.require("model.insertConsultantInStaff");
//jQuery.sap.require("model.ProfiloCommessaPeriodo");
jQuery.sap.require("model.Periods");
//jQuery.sap.require("model.Priorities");
//jQuery.sap.require("model.Projects");
//jQuery.sap.require("model.BuList");
jQuery.sap.require("model.WBS");
jQuery.sap.require("model.StaffCommesseView");
jQuery.sap.require("model.OfficialScheduling");
jQuery.sap.require("model.persistence.SapSerializer");
jQuery.sap.require("model.odata.chiamateOdata");

view.abstract.AbstractController.extend("view.OfficialScheduling", {


    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.pmSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingModel, "pmSchedulingModel");

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");

        this.noteModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteModel, "noteModel");

        this.modelloIcone = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelloIcone, "iconModel");

        this.addConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.addConsultantModel, "cModel");

        this.genericConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.genericConsultantModel, "gModel");

        this.buModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.buModel, "buModel");

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        this.modelScheduling = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelScheduling, "modelScheduling");

        this.modelWBS = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelWBS, "modelWBS");

        this.iconJsonModel = {
            'full': "sap-icon://complete",
            'half': "",
            'wCons': "sap-icon://customer-and-contacts",
            'wNonCons': "sap-icon://employee-rejections",
            'wCust': "sap-icon://collaborate",
            'wNonCust': "sap-icon://offsite-work",
            'notConfirmed': "sap-icon://role",
            'abroad': "sap-icon://globe",
            'del': "sap-icon://question-mark",
            'halfText': "1/2",
            'travel': "sap-icon://travel-itinerary",
            'bed': "sap-icon://bed"
        };

        this.modelloIcone.setProperty("/icon", this.iconJsonModel.del);

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");

        this.pModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pModel, "pModel");

        this.cModel = new sap.ui.model.json.JSONModel();

    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.userModel.setData(this.user);

        if (name !== "officialScheduling") {
            return;
        }

        this.oDialog = this.getView().byId("BlockLayout");
        this.oDialogTable = this.getView().byId("idSchedulazioniTableOfficial");
        this.getView().byId("idSchedulazioniTableOfficial").setVisible(false);
        this.getView().byId("idSelectCommessa").setEnabled(false);

        this.getView().byId("idSelectCommessa").setSelectedKey("");
        this.getView().byId("idSelectCommessa").setValue("");
        this.readPeriods();
        this.getCommesseForCurrentUser();

        this.pmSchedulingModel.getData().giorniTotali = 0;
        this.pmSchedulingModel.setProperty("/fillWarButton", false);
        var modello = this.pmSchedulingModel.getData();

        model.persistence.Storage.session.remove("periodSelected");

        this.visibleModel.setProperty("/visibleButtonOwnScheduling", false);
        this.pmSchedulingModel.setProperty("/commessa", "");
    },

    readPeriods: function () {
        var fSuccess = function (result) {
            var res = {};
            _.remove(result.items, {stato:'CHIUSO'});
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for (var i = 0; i < 13; i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.periodModel.setData(res13);
            console.log("Periods: ");
            console.log(res);
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");
        };

        var fError = function () {
            sap.m.MessageBox.alert("Errore in lettura periodo", {
                title: "Attenzione"
            });
            this.oDialog.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    getCommesseForCurrentUser: function () {
        var codConsulente = this.user.codice_consulente;
        var arrayCommesse = [];
        var createCommessaTipo = function (newCommesa) {
            this.codCommessa = newCommesa.codCommessa;
            this.nomeCommessa = newCommesa.nomeCommessa;
            this.descrizioneCommessa = newCommesa.descrizioneCommessa;
        };
        var fSuccess = function (result) {
            _.remove(result, {
                codCommessa: 'AA00000098'
            });
            _.remove(result, {
                codCommessa: 'AA00000008'
            });

            for (var i = 0; i < result.length; i++) {
                var newCommessa = new createCommessaTipo(result[i]);
                arrayCommesse.push(newCommessa);
            }
            arrayCommesse = _.sortByOrder(arrayCommesse, 'nomeCommessa')

            this.cModel.setData({
                items: arrayCommesse
            });
            this.getView().setModel(this.cModel, "cModel");
        };
        var fError = function () {
            console.log("errore in lettura commesse");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.StaffCommesseView.getCommesseForCurrentUser(codConsulente)
            .then(fSuccess, fError);
    },

    onChangePeriod: function (pe) {
        this.oDialog.setBusy(true);
        this.visibleModel.setProperty("/visibleButtonOwnScheduling", false);
        this.pmSchedulingModel.setProperty("/commessa", "");
        this.svuotaSelect();
        this.svuotaTabella();

        if (pe.getSource().getSelectedItem().getProperty("additionalText") === "WIP") {
            sap.m.MessageToast.show("Periodo non ancora rilasciato");
            this.oDialog.setBusy(false);
            return;
        }

        var table = this.getView().byId("idSchedulazioniTableOfficial");
        if (!!this.getView().byId("idSelectCommessa")) {
            this.getView().byId("idSelectCommessa").setSelectedKey("");
            this.getView().byId("idSelectCommessa").setValue("");
        }
        this.pe = pe;
        var p = pe.getSource().getSelectedItem().getProperty("text").replace(/ /g, '');
        var period = pe.getSource().getProperty("selectedKey");
        model.persistence.Storage.session.save("period", period);
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(period)
        });
        model.persistence.Storage.session.save("periodSelected", periodSelect);
        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();
        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        // aaa
        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();

        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = pInizio;
        var dataF = pFine;

        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

        // ** rendo visibili solo le colonne del periodo selezionato
        var columns = table.getColumns();
        for (var i = 0; i < columns.length; i++) {
            columns[i].setVisible(true);
        }
        for (var i = this.numColonne; i < columns.length - 1; i++) {
            columns[i].setVisible(false);
        }

        // ** metto i nomi dei giorni sulle colonne
        for (var i = 2; i < columns.length; i++) {
            columns[i - 1].getLabel().mProperties.text = this.arrayGiorni[i - 2];
        }
        this.getView().rerender();
        this.getView().byId("idSelectCommessa").setEnabled(true);

        this.periodStauts = pe.getSource().getSelectedItem().getProperty("additionalText");

        jQuery.sap.delayedCall(500, this, function () {
            this.oDialog.setBusy(false);
            if (this.getView().byId("idSelectPeriodo").getValue() === "") {
                this.getView().byId("idSelectPeriodo").setValue(this.tempP);
            }
        });
        // aa
        this.ric = {
            'codConsulente': this.user.codice_consulente,
            'idPeriodo': period
        };
        this.leggiTabella(this.ric);
    },

    svuotaSelect: function (value) {
        if (!value) {
            this.getView().byId("idSelectCommessa").setEnabled(false);
            this.getView().byId("idSelectCommessa").setSelectedKey("");
            this.getView().byId("idSelectCommessa").setValue("");
        };
    },

    onLaunchpadPress: function () {
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.router.navTo("launchpad");
    },

    svuotaTabella: function () {
        // **svuoto la tabella
        this.getView().byId("idSchedulazioniTableOfficial").setVisible(true);
        this.pmSchedulingModel.setProperty("/schedulazioni", [])
        this.pmSchedulingModel.setProperty("/nRighe", 1);
    },

    onChangeCommessa: function (evt) {
        this.pmSchedulingModel.setProperty("/fillWarButton", false);

        var commessaScelta = evt.getSource().getSelectedItem().getProperty("key");
        this.chosedCom = evt.getSource().getSelectedItem().getProperty("text");
        
        this.pmSchedulingModel.setProperty("/commessa", this.chosedCom.split(" - ")[0]);

        var c = _.find(this.pmSchedulingModel.getData().commesse, {
            codCommessa: commessaScelta
        });
        var comm = model.persistence.Storage.session.save("commessa", c);

        var req = {};
        req.idCommessa = commessaScelta;
        //        req.cod_consulente = undefined;
        req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
        this.ric = req;

        this.leggiTabella(req);
        this.visibleModel.setProperty("/visibleTable", true);
    },

    leggiTabella: function (req) {
        var fSuccess = function (res) {
            if (res && res.resultOrdinato && res.resultOrdinato.length > 0) {
                var schedulazioni = res.resultOrdinato;
                this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioni);
                if (this.ric.codConsulente) {
                    this.pmSchedulingModel.setProperty("/giorniTotali", schedulazioni[0].giorniScheduling);
                } else {
                    if (schedulazioni.length > 0 && schedulazioni.length <= 7) {
                        this.pmSchedulingModel.setProperty("/nRighe", schedulazioni.length);
                    } else if (schedulazioni.length > 7) {
                        this.pmSchedulingModel.setProperty("/nRighe", 7);
                    } else {
                        this.pmSchedulingModel.setProperty("/nRighe", 1);
                    }
                    this.pmSchedulingModel.setProperty("/giorniTotali", 0);
                    this.visibleModel.setProperty("/visibleButtonOwnScheduling", true);
                }
                setTimeout(_.bind(function () {
                    this._renderTabelle();
                }, this), 250);

            } else {
                this.oDialogTable.setBusy(false);
                sap.m.MessageToast.show(this._getLocaleText("noScheduling"), {
                    at: "center center",
                });
            }
        };
        var fError = function (err) {
            if (this.oDialog.getBusy() === true) {
                this.oDialog.setBusy(false);
            }
            console.log(err);
            this.oDialogTable.setBusy(false);
            sap.m.MessageToast.show(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialogTable.setBusy(true);
        model.tabellaSchedulingOfficial.readTable(req)
            .then(fSuccess, fError);
    },

    _renderTabelle: function () {
        if (this.ric.idCommessa)
            this._calcoloGiornateTotali()
        var table = this.getView().byId("idSchedulazioniTableOfficial");
        var rows = table.getRows();
        var arrayCounter = [];
        for (var i = 0; i < rows.length; i++) {
            var cellsPerRows = rows[i].getAggregation("cells");
            var g = 0;
            for (var j = 1; j < cellsPerRows.length - 1; j++) {
                var cella = cellsPerRows[j];
                if (_.find(this.weekendDays, _.bind(function (item) {
                        return (item == j);
                    }, this))) {
                    var column = $("#" + cella.getDomRef().id).parent().parent();
                    column.css('background-color', '#e5e5e5');
                }
            }
        }
        this.oDialogTable.setBusy(false); 
    },

    _calcoloGiornateTotali: function () {
        var totalCount = 0;
        for (var i = 0; i < this.pmSchedulingModel.getProperty("/schedulazioni").length; i++) {
            totalCount = totalCount + this.pmSchedulingModel.getProperty("/schedulazioni")[i].giorniScheduling;
        }
        this.pmSchedulingModel.setProperty("/giorniTotali", totalCount);
    },


//    
    filterForProject: function () {
        var fSuccess = function (result) {
            var projects = result.items;
            var projectSelected = this.getView().byId("idSelectCommessa").getSelectedKey();
            var staffSelected = _.find(projects, {
                'codCommessa': projectSelected
            }).codStaffCommessa;
            this.readStaffComplete(staffSelected);
        };

        var fError = function () {
            console.log("errore in lettura commesse con staff");
            this.oDialogTable.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var loggedUser = JSON.parse(sessionStorage.getItem("GSPI_User_Logged")).codice_consulente;
        var filtro = "?$filter=COD_STAFF_COMMESSA ne ''";

        this.oDialogTable.setBusy(true);
        model.StaffCommesseView.readNoStaffProject(filtro)
            .then(fSuccess, fError);
    },

    readStaffComplete: function (codStaff) {
        var fSuccess = function (result) {
            var consultantsForStaff = result.items;
            this.allScheduling = this.pmSchedulingModel.getProperty("/schedulazioni");
            var schedulingForStaff = [];
            for (var i = 0; i < consultantsForStaff.length; i++) {
                var scheduling = _.find(this.allScheduling, {
                    'codiceConsulente': consultantsForStaff[i].codConsulenteStaff
                });
                if (scheduling) {
                    schedulingForStaff.push(scheduling);
                };
            };
            this.pmSchedulingModel.setProperty("/schedulazioni", schedulingForStaff);
            if (schedulingForStaff.length > 0) {
                this.pmSchedulingModel.setProperty("/nRighe", schedulingForStaff.length);
            } else {
                this.pmSchedulingModel.setProperty("/nRighe", 1);
            };
//            setTimeout(_.bind(this.onAfter, this));

            jQuery.sap.delayedCall(500, this, function () {
                this.oDialogTable.setBusy(false);
            });
        };

        var fError = function () {
            this.oDialogTable.setBusy(false);
            console.log("errore in lettura staff");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var filtri = "?$filter=COD_STAFF eq " + "'" + codStaff + "'";
        model.StaffCommesseView.readStaffComplete(filtri)
            .then(fSuccess, fError);
    },

    fillInWarPress: function () {
        var period = sessionStorage.getItem("periodSelected");
        var annoI = JSON.parse(period).dataInizio.split("/")[2];
        var meseI = JSON.parse(period).dataInizio.split("/")[1];
        if (meseI.length === 1) {
            meseI = "0" + meseI;
        }
        var giornoI = JSON.parse(period).dataInizio.split("/")[0];
        if (giornoI.length === 1) {
            giornoI = "0" + giornoI;
        }
        var dataI = annoI + "-" + meseI + "-" + giornoI + "T12:00:00.000";

        var annoF = JSON.parse(period).dataFine.split("/")[2];
        var meseF = JSON.parse(period).dataFine.split("/")[1];
        if (meseF.length === 1) {
            meseF = "0" + meseF;
        }
        var giornoF = JSON.parse(period).dataInizio.split("/")[0];
        if (giornoF.length === 1) {
            giornoF = "0" + giornoF;
        }
        var dataF = annoF + "-" + meseF + "-" + giornoF + "T12:00:00.000";

        var cid = sessionStorage.getItem("cid");

        fSuccess = function (result) {
            console.log("odata TimesheetSet OK");
            //            var attivitaSetOdata = result.dati;
            if (result.dati.length !== 0) {
                sap.m.MessageBox.alert(this._getLocaleText("notInsertToSAP"), {
                    title: "Attenzione"
                });
            } else {
                this._fillInWarPress();
            }
        }

        fError = function (err) {
            err = this._getLocaleText("ErroreCaricamentoAttivitaOdata");
            sap.m.MessageToast.show(err);
            return;
        }

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.WBS.checkActivities(dataI, dataF, cid)
            .then(fSuccess, fError);
    },


    _fillInWarPress: function () {
        var cid = sessionStorage.getItem("cid");
        this._readWBS(cid)
            .then(function (result) {
                var schedulazioni = this.pmSchedulingModel.getData().schedulazioni;
                var dataToSync = [];
                for (var i = 1; i < 36; i++) {
                    var prop = "ggComm" + i;
                    var propData = "ggReq" + i;
                    var propIcon = "ggIcon" + i;
                    var comm = schedulazioni[0][prop];
                    if (comm) {
                        console.log(comm);
                        var WBSfind = _.find(this.modelWBS.getData(), {
                            codCommessaHana: comm
                        });
                        if (!WBSfind) {
                            sap.m.MessageBox.alert(this._getLocaleText("needRelationship"), {
                                title: "Attenzione"
                            });
                            return;
                        }
                        console.log(WBSfind);

                        var descrizioneAttivita = "";
                        for (var c = 1; c < WBSfind.descAttivitaSap.split("- ").length; c++) {
                            if (c === 1) {
                                descrizioneAttivita = WBSfind.descAttivitaSap.split("- ")[c];
                            } else {
                                descrizioneAttivita = descrizioneAttivita + "- " + WBSfind.descAttivitaSap.split("- ")[c];
                            };
                        };

                        var dataSched = schedulazioni[0][propData].split("-");
                        var dataOra = new Date();

                        if (dataOra.getHours().toString().length < 2) {
                            var oraModifica = "0" + dataOra.getHours();
                        } else {
                            var oraModifica = dataOra.getHours();
                        };
                        if (dataOra.getMinutes().toString().length < 2) {
                            var minutiModifica = "0" + dataOra.getMinutes();
                        } else {
                            var minutiModifica = dataOra.getMinutes();
                        };
                        if (dataOra.getSeconds().toString().length < 2) {
                            var secondiModifica = "0" + dataOra.getSeconds();
                        } else {
                            var secondiModifica = dataOra.getSeconds();
                        };
                        var dataToSave = {};
                        dataToSave.commessa = WBSfind.codCommessaSap;
                        dataToSave.attivita = WBSfind.codAttivitaSap;
                        dataToSave.descrizioneAttivita = descrizioneAttivita;
                        dataToSave.descrizioneAttivita = WBSfind.descCommessaSap.split("- ")[1];
                        dataToSave.luogo = WBSfind.codLuogoLavoro;
                        dataToSave.dataCommessa = dataSched[2] + "/" + dataSched[1] + "/" + dataSched[0];
                        dataToSave.nonFatturabile = WBSfind.fatturabile === "SI" ? "X" : "";
                        dataToSave.tipoOrario = WBSfind.codTipoOrario;
                        dataToSave.descrizioneTipoOrario = WBSfind.descTipoOrario;
                        dataToSave.durata = schedulazioni[0][propIcon] === "sap-icon://complete" ? "8" : "4";
                        dataToSave.orarioInizio = "09:00:00";
                        dataToSave.orarioFine = "17:00:00";
                        dataToSave.counter = "";
                        dataToSave.counterFiori = i.toString();
                        dataToSave.colore = "#3366FF";
                        dataToSave.stato = "10";
                        dataToSave.descrizione = WBSfind.descCommessa;
                        dataToSave.note = WBSfind.noteCommessa;
                        dataToSave.dataUltimaModifica = dataOra.getDate() + "/" + parseInt(dataOra.getMonth() + 1) + "/" + +dataOra.getFullYear();
                        dataToSave.oraUltimaModifica = oraModifica + ":" + minutiModifica + ":" + secondiModifica;
                        dataToSave.cid = cid;

                        dataToSync.push(model.persistence.SapSerializer.attivitaEntity.toSap(dataToSave));
                    }

                }
                console.log(dataToSync)

                var cidSet = {};
                var cidElement = utils.LocalStorage.getItem("cidSet")[0];
                var datiUtenteLS = $.parseJSON(atob(localStorage.getItem("user")));
                for (var i = 0; i < datiUtenteLS.length; i++) {
                    if (datiUtenteLS[i].id === sessionStorage.getItem("cid")) {
                        this.password = datiUtenteLS[i].p;
                        break;
                    };
                };
                cidElement.password = this.password;
                var user = model.persistence.SapSerializer.cidEntity.toSap(cidElement);
                cidElement = {};
                cidElement = user;
                cidElement.TravelSet = [];
                cidElement.TimesheetSet = dataToSync;
                cidSet.cidElement = cidElement;

                this.writeToSap(cidSet)
                    .then(function (result) {
                        sap.m.MessageToast.show(this._getLocaleText("writeOK"));
                    }.bind(this), function (err) {
                        sap.m.MessageBox.alert(this._getLocaleText("unableToWrite"), {
                            title: "Attenzione"
                        });
                    }.bind(this));
            }.bind(this), function (err) {
                sap.m.MessageBox.alert(this._getLocaleText("errorReadWBS"), {
                    title: "Attenzione"
                });
            }.bind(this));
    },

    _readWBS: function (cid) {
        var defer = Q.defer();
        var fSuccess = function (result) {
            this.modelWBS.setData(result[0].items);
            this.console("WBS", "success");
            this.console(result[0].items);
            this.oDialogTable.setBusy(false);
            defer.resolve(result[0].items);
        };

        var fError = function (err) {
            this.oDialogTable.setBusy(false);
            this.console(JSON.parse(err.response.body).error.message.value, "error");
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialogTable.setBusy(true);
        var stato = "ATTIVO";
        model.WBS.readWbsSapHana(cid, stato)
            .then(fSuccess, fError);

        return defer.promise;
    },

    getOdataModel: function () {
        if (this._odataModel) {
            this._odataModel.destroy();
        }
        var url = model.odata.chiamateOdata._serviceUrlSap;
        this._odataModel = new sap.ui.model.odata.ODataModel(url, maxDataServiceVersion = '2.0');
        return this.getView().setModel(this._odataModel);
    },

    writeToSap: function (data) {
        this.getOdataModel();
        this.modelloOdata = this.getView().getModel().createEntry("CIDSet", data.cidElement);
        this._odataModel.submitChanges(function () {
            console.log("OK");
        }, function (err) {
            console.log(err);
        });
    },

    removeStyleClassButton: function (b) {
        b.removeStyleClass("attention");
        b.removeStyleClass("travel");
        b.removeStyleClass("halfDay");
        b.removeStyleClass("wCons");
        b.removeStyleClass("fullDay");
        b.removeStyleClass("wNonCons");
        b.removeStyleClass("wCust");
        b.removeStyleClass("wNonCust");
        b.removeStyleClass("notConfirmed");
        b.removeStyleClass("bed");
        b.removeStyleClass("abroad");
    },

    onViewOwnSchedulingPress: function (evt) {
        this.pmSchedulingModel.setProperty("/commessa", "");
        var selectedActivityInput = this.getView().byId("idSelectCommessa");
        selectedActivityInput.setSelectedKey("");

        var selectedPeriod = model.persistence.Storage.session.get("periodSelected");
        if (!selectedPeriod) {
            sap.m.MessageToast.show("Select a Period");
            return;
        }
        this.ric = {
            'codConsulente': this.user.codice_consulente,
            'idPeriodo': selectedPeriod.idPeriod
        };
        this.getView().getModel("pmSchedulingModel").setProperty("/nRighe", 1);
        this.leggiTabella(this.ric);
    }
});
