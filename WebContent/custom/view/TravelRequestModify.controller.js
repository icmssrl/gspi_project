jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.TravelReq");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.LocalStorage");
jQuery.sap.require("model.persistence.SapSerializer");
jQuery.sap.require("model.persistence.NotaSpeseSerializer");
jQuery.sap.require("model.WBS");

view.abstract.AbstractMasterController.extend("view.TravelRequestModify", {

  onInit: function () {
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

    this.travelReqDetailModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.travelReqDetailModel, "travelReqDetailModel");

    //this.uiModel.setProperty("/searchProperty", ["nomeCognome"]);
  },

  handleRouteMatched: function (evt) {

    var name = evt.getParameter("name");

    if (name !== "travelRequestModify") {
      return;
    }
    view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
      
    var travelReq = JSON.parse(sessionStorage.getItem("selectedTravelReq"));
    this.travelReqDetailModel.setProperty("/nomeCognome", travelReq.nomeCognome);
    this.travelReqDetailModel.setProperty("/societa", travelReq.societa);
    this.travelReqDetailModel.setProperty("/cliente", travelReq.cliente);
    this.travelReqDetailModel.setProperty("/commessa", travelReq.commessa);
    this.travelReqDetailModel.setProperty("/attivita", travelReq.attivita);
    this.travelReqDetailModel.setProperty("/elementoWbs", travelReq.elementoWbs);
      if(travelReq.rifatturabile === "1"){
          this.getView().byId("idCheckRifatturabile").setSelected(true);
      }else{
          this.getView().byId("idCheckRifatturabile").setSelected(false);
      }
    this.travelReqDetailModel.setProperty("/tipoSpesaCod", travelReq.tipoSpesaCod);  
    this.travelReqDetailModel.setProperty("/tipoSpesaDesc", travelReq.tipoSpesaDesc); 
      
    if(travelReq.tipoSpesaCod === "HTLF"){
        this.travelReqDetailModel.setProperty("/visibleHotel", true);
        this.travelReqDetailModel.setProperty("/visibleTrasporto", false);
    }else if(travelReq.tipoSpesaCod === "AIRF" || travelReq.tipoSpesaCod === "CARF" || travelReq.tipoSpesaCod === "TRNF"){
        this.travelReqDetailModel.setProperty("/visibleTrasporto", true);
        this.travelReqDetailModel.setProperty("/visibleHotel", false);
    }else{
        this.travelReqDetailModel.setProperty("/visibleTrasporto", false);
        this.travelReqDetailModel.setProperty("/visibleHotel", false);
    }
      

    this.travelReqDetailModel.setProperty("/dataCheckIn", utils.Formatter.invertDate(travelReq.dataCheckIn));
    this.travelReqDetailModel.setProperty("/dataCheckOut", utils.Formatter.invertDate(travelReq.dataCheckOut));
    this.travelReqDetailModel.setProperty("/noNotti", travelReq.noNotti);
    this.travelReqDetailModel.setProperty("/luogo", travelReq.luogo);
    this.travelReqDetailModel.setProperty("/dettagliTrasporto", travelReq.dettagliTrasporto);
    this.travelReqDetailModel.setProperty("/richiesteSpeciali", travelReq.richiesteSpeciali);
      if(travelReq.confermato === "0"){
          this.travelReqDetailModel.setProperty("/confermato", false);
      }else{
          this.travelReqDetailModel.setProperty("/confermato", true);
      }
    this.travelReqDetailModel.setProperty("/noteConferma", travelReq.noteConferma);
//      var dataInserimento = travelReq.dataInserimento.substring(8) + "/" + travelReq.dataInserimento.substring(5,7) + "/" + travelReq.dataInserimento.substring(0,4);
//    this.travelReqDetailModel.setProperty("/dataInserimento", dataInserimento);
    this.travelReqDetailModel.setProperty("/dataInserimento", utils.Formatter.invertDate(travelReq.dataInserimento));
    this.travelReqDetailModel.setProperty("/costo", travelReq.costo);
      
    
  },
    
    confModifyReqPress:function(){
        sap.m.MessageBox.show(this._getLocaleText("confirmTravelReq"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("confirmTravelReqTitle"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmSavePress, this)
        );
    },
    
    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            
             var fSuccess = function () {
                //this._clearInputValue();
                 
                var fSuccessWar = function (result) {
                    sap.ui.core.BusyIndicator.hide();
                    sap.m.MessageToast.show(this._getLocaleText("TRAVEL_REQ_CONFIRMED"));
                    this.router.navTo("travelRequestList");
                };
                 
                var fErrorWar = function (err) {
                    sap.ui.core.BusyIndicator.hide();
                    if (JSON.parse(err.response.body).error.message.value) {
                        sap.m.MessageBox.error(JSON.parse(err.response.body).error.message.value, {
                            title: this._getLocaleText("WARNING")
                        });
                    } else {
                        sap.m.MessageBox.error(this._getLocaleText("SincronizzazioneNONRiuscita"), {
                            title: this._getLocaleText("WARNING")
                        });
                    };
                };
                
                fSuccessWar = _.bind(fSuccessWar, this);
                fErrorWar = _.bind(fErrorWar, this);
                this._odataModelForWar.submitChanges(fSuccessWar, fErrorWar);
                 
                
            };
             var fError = function () {
                 sap.ui.core.BusyIndicator.hide();
                console.log("errore nel salvataggio");
                sap.m.MessageToast.show(this._getLocaleText("TRAVEL_REQ_FAILED"));
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            var travelReq = JSON.parse(sessionStorage.getItem("selectedTravelReq"));
            var nomeCognome = travelReq.nomeCognome;
            var cid = travelReq.cid;
            var societa = travelReq.societa;
            var cliente = travelReq.cliente;
            var commessa = travelReq.commessa;
            var attivita = travelReq.attivita;
            var elementoWbs = travelReq.elementoWbs;
            var rifatturabile = travelReq.rifatturabile;
            var tipoSpesaCod = travelReq.tipoSpesaCod;
            var tipoSpesaDesc = travelReq.tipoSpesaDesc;
            var luogo = travelReq.luogo;
            
            var dataCheckIn;
            if(travelReq.dataCheckIn !== ""){
//               dataCheckIn = travelReq.dataCheckIn.substring(6) + "/" + travelReq.dataCheckIn.substring(3,5) + "/" + //travelReq.dataCheckIn.substring(0,2);
                 dataCheckIn = travelReq.dataCheckIn;
            }else {
                dataCheckIn = "";
            }
 
            var dataCheckOut;
            if(travelReq.dataCheckOut !== ""){
//                dataCheckOut = travelReq.dataCheckOut.substring(6) + "/" + travelReq.dataCheckOut.substring(3,5) + "/" + //travelReq.dataCheckOut.substring(0,2);
                dataCheckOut = travelReq.dataCheckOut;
            }else{
                dataCheckOut = "";
            }
            
            var noNotti;
            if(travelReq.noNotti === ""){
                noNotti = 0;
            }else{
                noNotti = parseInt(travelReq.noNotti);
            }
            var dettagliTrasporto = travelReq.dettagliTrasporto;
            var richiesteSpeciali = travelReq.richiesteSpeciali;
            var confermato = "1";
            var dataInserimento = travelReq.dataInserimento;
            var idRichiesta = travelReq.idRichiesta;
            var noteConferma = this.getView().byId("idNoteConferma").getValue();
            var costo = this.getView().byId("idCosto").getValue();
            var attivitaCod = travelReq.attivitaCod;
            var cidSet = this.createCidSetObject();
            var fakeObj = _.cloneDeep(travelReq);
            fakeObj.costo = costo;
            fakeObj.noteConferma = noteConferma;
            var objToSend = this.createNewExpenseObject(fakeObj);
            var objToSendSerialized = model.persistence.NotaSpeseSerializer.notaSpeseEntity.toSap(objToSend);
            this.getOdataModelForWar();
            cidSet.cidElement.TravelSet.push(objToSendSerialized);
            this.modelloOdata = this.getView().getModel().createEntry("CIDSet", cidSet.cidElement);
            if(noteConferma === ""){
                sap.m.MessageToast.show(this._getLocaleText("inserireNoteConferma"))
            }else{
                sap.ui.core.BusyIndicator.show();
                model.TravelReq.confirmTravelReq(nomeCognome, cid, societa, cliente, commessa, attivita, elementoWbs, rifatturabile, tipoSpesaCod, tipoSpesaDesc, luogo, dataCheckIn, dataCheckOut, noNotti, dettagliTrasporto, richiesteSpeciali, confermato, noteConferma, dataInserimento, costo, attivitaCod, idRichiesta)
                .then(fSuccess, fError);
            }
            
           
        };
    },
    
    createNewExpenseObject: function(expense){
        var obj = {};
        var newDate = new Date();
        obj.arrivo = expense.arrivo ? expense.arrivo : "";//:"",
        obj.attivita = expense.attivitaCod ? expense.attivitaCod : "";//:"",
        obj.categoriaSpesa = expense.categoriaSpesa ? expense.categoriaSpesa : "";//:"",
        obj.chilometri = expense.chilometri ? expense.chilometri : "0";//:"",
        obj.cid = expense.cid ? expense.cid : "";//:"",
        obj.counterFiori = expense.counterFiori ? expense.counterFiori : "";//:"",
        obj.dataNotaSpese = expense.dataInserimento ? expense.dataInserimento : "";//:"",
        obj.dataUltimaModifica = expense.dataInserimento ? expense.dataInserimento : "";//:"",
        obj.descrizioneCategoriaSpesa = expense.descrizioneCategoriaSpesa ? expense.descrizioneCategoriaSpesa : "";//:"",
        obj.importo = expense.costo ? expense.costo : "";//:"",
        obj.localita = expense.luogo ? expense.arrivo : "";//:"",
        obj.note = expense.noteConferma ? expense.noteConferma : "";//:"",
        obj.numProgressivoGiustificativo = expense.numProgressivoGiustificativo ? expense.numProgressivoGiustificativo : "";//:"",
        obj.numeroGiustificativo = expense.numeroGiustificativo ? expense.numeroGiustificativo : "";//:"",
        obj.numeroPeriodoTrasferta = expense.numeroPeriodoTrasferta ? expense.numeroPeriodoTrasferta : "";//:"",
        obj.numeroProgressivoAttCost = expense.numeroProgressivoAttCost ? expense.numeroProgressivoAttCost : "";//:"",
        obj.numeroTrasferta = expense.numeroTrasferta ? expense.numeroTrasferta : "";//:"",
        obj.oraUltimaModifica = expense.oraUltimaModifica ? expense.oraUltimaModifica : newDate.getHours() +":"+ newDate.getMinutes() +":"+ newDate.getSeconds();//:"",
        obj.partenza = expense.partenza ? expense.partenza : "";//:"",
        obj.tipoAuto = expense.tipoAuto ? expense.tipoAuto : "";//:"",
        obj.valuta = expense.valuta ? expense.valuta : "EUR";//:"EUR"
        return obj;
    },
    
    getOdataModelForWar: function () {
        if (this._odataModelForWar) {
            this._odataModelForWar.destroy();
        };
        var url = model.odata.chiamateOdata._serviceUrlSap;
        this._odataModelForWar = new sap.ui.model.odata.ODataModel(url, maxDataServiceVersion = '2.0');
        return this.getView().setModel(this._odataModelForWar);
    },
    
    createCidSetObject: function(){      
        var cidSet = {};
        var cidElement = utils.LocalStorage.getItem("cidSet")[0];
        var datiUtenteLS = $.parseJSON(atob(localStorage.getItem("user")));
        for (var i = 0; i < datiUtenteLS.length; i++) {
            if (datiUtenteLS[i].id === sessionStorage.getItem("cid")) {
                this.password = datiUtenteLS[i].p;
                break;
            };
        };
        cidElement.password = this.password;
        var user = model.persistence.SapSerializer.cidEntity.toSap(cidElement);
        cidElement = {};
        cidElement = user;
        cidElement.TravelSet = [];
        cidElement.TimesheetSet = [];
        cidSet.cidElement = cidElement;
        return cidSet;
    },
    
    cancModifyReqPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("confirmCancelTravelReq"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("confirmCancelTravelReqTitle"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmCancelPress, this)
        );
    },

    _confirmCancelPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            this.router.navTo("travelRequestList");
        };
    }

});