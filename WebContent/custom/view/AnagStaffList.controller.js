jQuery.sap.require("view.abstract.AbstractMasterController");

view.abstract.AbstractMasterController.extend("view.AnagStaffList", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.modelStaff = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelStaff, "modelStaff");

        this.modelAddStaff = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelAddStaff, "modelAddStaff");

        this.modelStaffedProjects = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelStaffedProjects, "modelStaffedProjects");

        this.uiModel.setProperty("/searchProperty", ["nomeCommessa", "codStaffCommessa", "descrizioneCommessa", "cognomePjm"]);
        this.uiModelDialog.setProperty("/searchPropertyDialog", ["nomeCommessa", "codStaffCommessa"]);
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");
        this.name = name;
        if (name !== "anagStaffList") {
            return;
        }
        var prefTitle = icms.Component.getMetadata().getConfig().settings.prefTitle;
        document.title = prefTitle + " _ STAFF";

        this.oDialog = this.getView().byId("list");
        this.oDialog.removeSelections();
        this.getView().byId("idSearchAnagList").setValue("");

        var loggedUser = JSON.parse(sessionStorage.getItem("GSPI_User_Logged")).codice_consulente;
        var userRole = JSON.parse(sessionStorage.getItem("GSPI_User_Logged")).descrizioneProfilo;
        var filtro = "";
        if (userRole === "Administrator") {
            filtro = "?$filter=COD_STAFF_COMMESSA ne ''";
        } else if (userRole === "Unit Manager") {
//            var bu = JSON.parse(sessionStorage.getItem("user")).descBu;
//            filtro = "?$filter=COD_STAFF_COMMESSA ne '' and NOME_BU eq '" + bu + "'";
            var bu = JSON.parse(sessionStorage.getItem("user")).codConsulenteBu;
            filtro = "?$filter=COD_STAFF_COMMESSA ne '' and CODICE_BU eq '" + bu + "'";
            
        } else {
            filtro = "?$filter=COD_STAFF_COMMESSA ne '' and COD_PJM_AC eq '" + loggedUser + "'";
        }
        this.getWithStaffProject(filtro);
    },

    // funzione che legge tutti gli staff di commesse (già staffate) appartenenti ad una determinata BU, PJM o tutte
    getWithStaffProject: function (filtro) {
        var defer = Q.defer();
        var fSuccess = function (result) {
            _.remove(result.items, {stato: 'CHIUSO'});
            this.modelStaffedProjects.setProperty("/commesseConStaff", result.items);
            this.console("Commesse con staff:", "confirm");
            this.console(result);
            this.oDialog.setBusy(false);
            defer.resolve(result);
        };
        var fError = function (err) {
            this.console("errore in lettura commesse con staff", "error");
            this.oDialog.setBusy(false);
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.StaffCommesseView.readNoStaffProject(filtro)
            .then(fSuccess, fError);
        return defer.promise;
    },

    onHomePress: function () {
        if (this.name === "anagStaffList") {
            this.onNavBackPress();
        } else {
            this.router.navTo("launchpad");
        }
    },

    // visualizza tutte le commesse al quale non è stato abbianto uno staff
    addStaffPress: function () {
        this.router.navTo("anagStaffList");
        this.selectedCommessa = {};
//        var loggedUser = JSON.parse(sessionStorage.getItem("GSPI_User_Logged")).codice_consulente;
        var userRole = JSON.parse(sessionStorage.getItem("GSPI_User_Logged")).descrizioneProfilo;
        var bu = JSON.parse(sessionStorage.getItem("user")).codConsulenteBu;
        if (userRole === "Administrator") {
            this.filtro = "?$filter=COD_STAFF_COMMESSA eq ''";
        } else {
//            this.filtro = "?$filter=COD_STAFF_COMMESSA eq '' and COD_PJM_AC eq " + "'" + loggedUser + "'";
            this.filtro = "?$filter=COD_STAFF_COMMESSA eq '' and CODICE_BU eq " + "'" + bu + "'";
        }

        var page = this.getView().byId("AnagStaffListId");

        this.addStaffDialog = sap.ui.xmlfragment("view.dialog.addStaffDialog", this);
        this.addStaffDialog.setModel(this.uiModelDialog, "uiD");
        page.addDependent(this.addStaffDialog);

        this.getNoStaffProject(this.filtro);
    },

    onFilterDialogClose: function () {
        sap.ui.getCore().byId("listS").removeSelections();
        this.addStaffDialog.close();
        this.addStaffDialog.destroy(true);
    },

    onFilterDialogOK: function (evt) {
        var list = sap.ui.getCore().byId("listS");
        var item = list.getSelectedItem().getBindingContext("modelAddStaff").getObject().codCommessa;
        this.selectedCommessa = list.getSelectedItem().getBindingContext("modelAddStaff").getObject();
        var fSuccess = function (result) {
            var loggedUser = JSON.parse(sessionStorage.getItem("GSPI_User_Logged")).codice_consulente;
            var userRole = JSON.parse(sessionStorage.getItem("GSPI_User_Logged")).descrizioneProfilo;
            var filtro = "";
            if (userRole === "Administrator") {
                filtro = "?$filter=COD_STAFF_COMMESSA ne ''";
            } else {
                filtro = "?$filter=COD_STAFF_COMMESSA ne '' and COD_PJM_AC eq " + "'" + loggedUser + "'";

            }
            this.getWithStaffProject(filtro)
                .then(_.bind(function (result) {
                        this.selectedCommessa = _.find(result.items, {
                            codCommessa: this.selectedCommessa.codCommessa
                        });
                        sessionStorage.setItem("selectedStaff", JSON.stringify(this.selectedCommessa));
                        this.router.navTo("anagStaffDetail", {
                            detailId: this.selectedCommessa.codStaffCommessa
                        });
                    }, this),
                    _.bind(function () {
                        this.console("Error reading Staff", "error");
                    }, this));
            sap.ui.getCore().byId("listS").removeSelections();
            this.addStaffDialog.close();
            this.addStaffDialog.destroy(true);
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            this.console("errore", "error");
            this.oDialog.setBusy(false);
            this.addStaffDialog.close();
            this.addStaffDialog.destroy(true);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.StaffCommesseView.addStaffToCommessa(item)
            .then(fSuccess, fError);
    },

    getNoStaffProject: function (filtro) {
        var fSuccess = function (result) {
            this.modelAddStaff.setProperty("/commesseNoStaff", result.items);
            this.console("Commesse senza staff:");
            this.console(result);
            this.modelAddStaff.refresh();
            this.addStaffDialog.open();
            this.oDialog.setBusy(false);
        };
        
        var fError = function () {
            this.console("errore in lettura commesse senza staff", "error");
            this.oDialog.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.StaffCommesseView.readNoStaffProject(filtro)
            .then(fSuccess, fError);
    },

    
    onStaffSelectBis: function (evt) {
        var src = evt.getSource();
        if (src.getSelectedItems()[0]) {
            var selectedItem = src.getSelectedItems()[0].getBindingContext("modelStaffedProjects").getObject();
            this.selectedCommessa = selectedItem;
            sessionStorage.setItem("selectedStaff", JSON.stringify(selectedItem));
            this.router.navTo("anagStaffDetail", {
                detailId: selectedItem.codStaffCommessa
            });
        }
    },
});
