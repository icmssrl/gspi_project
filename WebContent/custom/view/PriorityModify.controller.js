jQuery.sap.require("utils.Formatter");

jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("model.Priorities");

view.abstract.AbstractController.extend("view.PriorityModify", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.detailPriorita = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailPriorita, "detailPriorita");

    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "priorityModify") {
            return;
        };

        this.oDialog = this.getView().byId("BusyDialog");
        this.state = evt.getParameters().arguments.state;

        if (this.state === "new") {
            this.detailPriorita.setProperty("/codPrioritaCommessa", "");
            this.detailPriorita.setProperty("/descrPriorita", "");
        } else {
            var priorita = JSON.parse(sessionStorage.getItem("selectedPriority"));
            this.detailPriorita.setProperty("/codPrioritaCommessa", priorita.codPrioritaCommessa);
            this.detailPriorita.setProperty("/descrPriorita", priorita.descrPriorita);
        };
    },

    setValori: function () {
        this.getView().byId("codPjmAssociato").setProperty("value", this.detailPriorita.getProperty("/cognomeNome"))
        this.getView().byId("statoPriorita").getProperty("value", this.detailPriorita.getProperty("/statoPriorita"))
    },


    modifyPress: function (evt) {
        var ff = evt.getSource();
        var indice = ff.getId().slice(ff.getId().length - 1, ff.getId().length);

        var table = this.getView().byId("idCommesseTable");
        var prioritaSel = this.modelCommesse.getData().commesse[indice];

        var fSuccess = function () {
            this.readProjects();
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            console.log("errore in lettura progetti");
            this.oDialog.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.PeriodsGabio.updateProject(prioritaSel.codPriorita, prioritaSel.nomePriorita, prioritaSel.descrizionePriorita, prioritaSel.codPjmAssociato, prioritaSel.codiceStaffPriorita, prioritaSel.statoPriorita).then(fSuccess, fError);

        console.log(prioritaSel)
    },

    cancelModifyPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("confirmCancelPriority"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("confirmCancelTitlePriority"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmCancelPress, this)
        );
    },

    saveProjectPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("confirmSavePriority"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("confirmSaveTitlePriority"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmSavePress, this)
        );
    },

    _confirmCancelPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            this._clearInputValue();
            this.router.navTo("priorityList");
        };
    },

    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {

            var fSuccess = function () {
                this._clearInputValue();
                this.oDialog.setBusy(false);
                this.router.navTo("priorityList");
            };
            var fError = function () {
                console.log("errore nel salvataggio");
                this.oDialog.setBusy(false);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            
            this.oDialog.setBusy(true);
            var descrizionePrioritaCommessa = this.getView().byId("idDescrizionePriorita").getValue();
            if (this.state === "new") {
                model.Priorities.addPriority(descrizionePrioritaCommessa)
                    .then(fSuccess, fError);
            } else {
              var codicePrioritaCommessa = this.getView().byId("idCodPriorita").getValue();
              model.Priorities.updatePriority(codicePrioritaCommessa, descrizionePrioritaCommessa)
                    .then(fSuccess, fError);
            };
        };
    },

    _clearInputValue: function () {
        this.getView().byId("idCodPriorita").setValue("");
        this.getView().byId("idDescrizionePriorita").setValue("");
    }
});
