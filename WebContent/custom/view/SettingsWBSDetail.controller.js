jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.UserLogin");
jQuery.sap.require("model.Skill");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");

view.abstract.AbstractController.extend("view.SettingsWBSDetail", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        
        this.detailWBS = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailWBS, "detailWBS");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "settingsWBSDetail") {
            return;
        }
        this.oDialog = this.getView().byId("idWBSDetail");

        var WBSSelecet = JSON.parse(sessionStorage.getItem("selectedWBS"));
        this.WBSSelecet = WBSSelecet;
        this.detailWBS.setData(WBSSelecet);
    },
    
    checkFatturabile: function (value) {
        if (value && (value.toUpperCase()) === "SI") {
            return true;
        } else {
            return false;
        }
    },

    modifyWBSPress: function () {
        this.router.navTo("settingsWBSModify", {
            state: "modify",
            codCommessaHana: "",
            codCommessaSap: "",
            codAttivitaSap: ""
        });
    },

    deleteWBSPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_DELETE_WBS"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_DELETE_TITLE_WBS"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmDeletePress, this)
        );
    },

    _confirmDeletePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var fSuccess = function () {
                this.oDialog.setBusy(false);
                this.router.navTo("settingsWBSList");
            };
            var fError = function () {
                this.oDialog.setBusy(false);
                console.log("errore nel salvataggio");
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            var wbs = {};
            wbs.cidCosnuslenteSap = this.WBSSelecet.cidCosnuslenteSap;
            wbs.codConsulenteHana = this.WBSSelecet.codConsulenteHana;
            wbs.codCommessaHana = this.WBSSelecet.codCommessaHana;
            wbs.codCommessaSap = this.WBSSelecet.codCommessaSap;
            wbs.codAttivitaSap = this.WBSSelecet.codAttivitaSap;
            wbs.stato = "CANCELLATO";
            model.WBS.changeStatus(wbs)
                .then(fSuccess, fError);
        }
    },
    
    activeWBSPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_RESTORE_WBS"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_RESTORE_TITLE_WBS"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmRestorePress, this)
        );
    },
    
    _confirmRestorePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var fSuccess = function () {
                this.oDialog.setBusy(false);
                this.router.navTo("settingsWBSList");
            };
            var fError = function () {
                this.oDialog.setBusy(false);
                console.log("errore nel salvataggio");
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            var wbs = {};
            wbs.cidCosnuslenteSap = this.WBSSelecet.cidCosnuslenteSap;
            wbs.codConsulenteHana = this.WBSSelecet.codConsulenteHana;
            wbs.codCommessaHana = this.WBSSelecet.codCommessaHana;
            wbs.codCommessaSap = this.WBSSelecet.codCommessaSap;
            wbs.codAttivitaSap = this.WBSSelecet.codAttivitaSap;
            wbs.stato = "ATTIVO";
            model.WBS.changeStatus(wbs)
                .then(fSuccess, fError);
        }
    },
    /* DA IMPLEMENTARE
    _checkWbs: function (wbs) {
        var fSuccess = function (result) {
            var res = result[0].items;
            if(_.find(res,{'codCommessaHana':wbs.codCommessaHana,stato:'ATTIVO'})) {
                this.oDialog.setBusy(false);
                sap.m.MessageToast.show(this._getLocaleText("IMPOSSIBILE_SOVRASCRIVERE"));
                return;
            } else {
                this._write(wbs);
            }
            this.oDialog.setBusy(false);
        };
        var fError = function (err) {
            this.oDialog.setBusy(false);
            this.console(JSON.parse(err.response.body).error.message.value, "error");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        var cid = sessionStorage.getItem("cid");
        var stato = "ATTIVO";
        model.WBS.readWbsSapHana(cid, stato)
            .then(fSuccess, fError);
    },
    */
});