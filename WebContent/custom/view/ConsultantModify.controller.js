jQuery.sap.require("view.abstract.AbstractController");

view.abstract.AbstractController.extend("view.ConsultantModify", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.detailConsulente = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailConsulente, "detailConsulente");

        this.modelBu = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelBu, "modelBu");

        this.statoConsulente = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.statoConsulente, "statoConsulente");

        this.companyTypeModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.companyTypeModel, "companyTypeModel");

        this.consultantListModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.consultantListModel, "cModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "consultantModify") {
            return;
        }

        this.oDialog = this.getView().byId("idConsultantModify");

        this.state = evt.getParameters().arguments.state;
        //------------------------------------------------
        this.consultant = undefined;
        if (this.state === "modify") {
            this.consultant = JSON.parse(sessionStorage.getItem("selectedConsultant"));
        }
        this.detailConsulente.setProperty("/skills", []);
        //-------------------------------------------------    
        var fErrorConsulenteSkills = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageBox.alert(err, {
                title: this._getLocaleText("errore in skill selected")
            });
        };

        var fErrorSkill = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageBox.alert(err, {
                title: this._getLocaleText("errore in skill list")
            });
        };

        var fErrorStatus = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageBox.alert(err, {
                title: this._getLocaleText("errore stauts list")
            });
        };

        var fErrorCompanyType = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageBox.alert(err, {
                title: this._getLocaleText("errore companyType list")
            });
        };

        var fErrorBu = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageBox.alert(err, {
                title: this._getLocaleText("errore in Bu List")
            });
        };

        var fError = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageBox.alert(err, {
                title: this._getLocaleText("error")
            });
        };

        var fSuccessConsulenteSkills = function (result) {
            var primarySkill = "";
            var otherSkills = [];
            var arraySkillSelected = [];
            if (this.state !== "new") {
                for (var i = 0; i < result.results.length; i++) {
                    arraySkillSelected.push(result.results[i].COD_SKILL);
                    if (result.results[i].SKILL_PRINCIPALE && result.results[i].SKILL_PRINCIPALE.length > 0) {
                        primarySkill = result.results[i].COD_SKILL;

                    } else {
                        otherSkills.push(result.results[i].COD_SKILL);
                    }
                }
            }
            var allSkills = this.detailConsulente.getProperty("/skillsWOPrimary");
            var skillsWOPrimary = _.filter(allSkills, function (o) {
                return o.codSkill !== primarySkill;
            })
            this.detailConsulente.setProperty("/allSkills", arraySkillSelected);
            this.detailConsulente.setProperty("/codSkillSelected", otherSkills);
            this.detailConsulente.setProperty("/primarySkill", primarySkill);
            this.detailConsulente.setProperty("/lastPrimarySkill", primarySkill);
            this.detailConsulente.setProperty("/skillsWOPrimary", skillsWOPrimary);
            this.oDialog.setBusy(false);
        };

        var fSuccessSkill = function (result) {
            var skills = _.slice(result.items);

            if (this.state === "new") {
                skills.unshift({ codSkill: "", nomeSkill: "" });
                //                this.loadConsultantFromSap();
                this.detailConsulente.setProperty("/codConsulente", "");
                this.detailConsulente.setProperty("/nomeConsulente", "");
                this.detailConsulente.setProperty("/cognomeConsulente", "");
                this.detailConsulente.setProperty("/nomeCognome", "");
                this.detailConsulente.setProperty("/descrizioneProfiloConsulente", "");
                this.detailConsulente.setProperty("/codProfiloConsulente", "");
                this.detailConsulente.setProperty("/codConsulenteBu", "");
                this.detailConsulente.setProperty("/descBu", "");
                this.detailConsulente.setProperty("/skill", "");
                this.detailConsulente.setProperty("/note", "");
                this.detailConsulente.setProperty("/intercompany", "");
                this.detailConsulente.setProperty("/tariffaIntercompany", "");
                this.detailConsulente.setProperty("/ggIntercompany", "");
                this.detailConsulente.setProperty("/partTime", "");
                this.detailConsulente.setProperty("/orePartTime", "");
                this.detailConsulente.setProperty("/codSkillSelected", []);
                this.detailConsulente.setProperty("/primarySkill", "");
                this.detailConsulente.setProperty("/lastPrimarySkill", "");
                this.detailConsulente.setProperty("/skills", skills);
                this.detailConsulente.setProperty("/skillsWOPrimary", result.items);
                this.detailConsulente.refresh();
                this.oDialog.setBusy(false);
            } else {
                var consulente = JSON.parse(sessionStorage.getItem("selectedConsultant"));
                this.detailConsulente.setProperty("/codConsulente", consulente.codConsulente);
                this.detailConsulente.setProperty("/nomeConsulente", consulente.nomeConsulente);
                this.detailConsulente.setProperty("/cognomeConsulente", consulente.cognomeConsulente);
                this.detailConsulente.setProperty("/nomeCognome", consulente.cognomeConsulente + " " + consulente.nomeConsulente);
                this.detailConsulente.setProperty("/descrizioneProfiloConsulente", consulente.descrizioneProfiloConsulente);
                this.detailConsulente.setProperty("/codProfiloConsulente", consulente.codProfiloConsulente);
                this.detailConsulente.setProperty("/codConsulenteBu", consulente.descBu);
                this.detailConsulente.setProperty("/descBu", consulente.descBu);
                this.detailConsulente.setProperty("/skill", consulente.skill);
                this.detailConsulente.setProperty("/tipoAzienda", consulente.tipoAzienda);
                this.detailConsulente.setProperty("/statoConsulente", consulente.stato);
                this.detailConsulente.setProperty("/note", consulente.note);
                this.detailConsulente.setProperty("/intercompany", consulente.intercompany);
                this.detailConsulente.setProperty("/tariffaIntercompany", consulente.tariffaIntercompany);
                this.detailConsulente.setProperty("/ggIntercompany", consulente.ggIntercompany);
                this.detailConsulente.setProperty("/partTime", consulente.partTime);
                this.detailConsulente.setProperty("/orePartTime", consulente.orePartTime);
                this.detailConsulente.setProperty("/skills", skills);
                this.detailConsulente.setProperty("/skillsWOPrimary", result.items);
                model.Skill.getConsulenteSkill(consulente.codConsulente)
                    .then(fSuccessConsulenteSkills, fErrorConsulenteSkills);
            }
        };

        var fSuccessStatus = function (result) {
            this.statoConsulente.setProperty("/status", result);
            var buComboBox = this.getView().byId("idStatoConsulente");
            if (this.consultant) {
                buComboBox.setValue(this.consultant.stato);
            } else {
                buComboBox.setValue("APERTO");
            }
            model.Skill.readSkills()
                .then(fSuccessSkill, fErrorSkill);
        };

        var fSuccessBu = function (result) {
            this.modelBu.setProperty("/items", result.items);
            var buComboBox = this.getView().byId("idCodConsulenteBu");
            if (this.consultant) {
                buComboBox.setValue(this.consultant.descBu);
            }
            model._CompanyType.getCompany()
                .then(fSuccessCompanyType, fErrorCompanyType);
        };

        var fSuccessCompanyType = function (res) {
            this.companyTypeModel.setProperty("/type", res);
            var buComboBox = this.getView().byId("idTipoAzienda");
            if (this.consultant) {
                buComboBox.setValue(this.consultant.tipoAzienda);
            } else {
                buComboBox.setValue("Interni");
            }
            model._ConsultantsStatus.getStauts()
                .then(fSuccessStatus, fErrorStatus);
        };

        var fSuccessProfiles = function (result) {
            if (model.persistence.Storage.session.get("user").codProfiloConsulente !== "CP10000001")
                _.remove(result.results, { COD_PROFILO: "CP10000001" });
            this.detailConsulente.setProperty("/profiloConsulente", result.results);
            var buComboBox = this.getView().byId("codProfiloConsulente");
            if (this.consultant) {
                buComboBox.setSelectedKey(this.consultant.codProfiloConsulente);
            }
            var stato = "APERTO";
            model.BuList.getBuListByStatus(stato)
                .then(fSuccessBu, fErrorBu);
        };

        fSuccessConsulenteSkills = _.bind(fSuccessConsulenteSkills, this);
        fErrorConsulenteSkills = _.bind(fErrorConsulenteSkills, this);

        fSuccessSkill = _.bind(fSuccessSkill, this);
        fErrorSkill = _.bind(fErrorSkill, this);

        fSuccessStatus = _.bind(fSuccessStatus, this);
        fErrorStatus = _.bind(fErrorStatus, this);

        fSuccessCompanyType = _.bind(fSuccessCompanyType, this);
        fErrorCompanyType = _.bind(fErrorCompanyType, this);

        fSuccessBu = _.bind(fSuccessBu, this);
        fErrorBu = _.bind(fErrorBu, this);

        fError = _.bind(fError, this);
        fSuccessProfiles = _.bind(fSuccessProfiles, this);

        this.oDialog.setBusy(true);
        model.Profiles.readProfiles()
            .then(fSuccessProfiles, fError);
    },

    cancelModifyPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_CANCEL_MODIFY_CONSULTANT"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_CANCEL_MODIFY_TITLE_CONSULTANT"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmCancelPress, this)
        );
    },

    saveProjectPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_SAVE_CONSULTANT"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_SAVE_TITLE_CONSULTANT"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmSavePress, this)
        );
    },

    _confirmCancelPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            this._clearInputValue();
            this.router.navTo("consultantList");
        }
    },

    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var fError = function (err) {
                console.log("errore nel salvataggio");
                this.oDialog.setBusy(false);
            };

            var fSuccessAddSkill = function (result) {
                if (JSON.parse(result).status !== "OK") {
                    sap.m.MessageBox.alert(result, {
                        title: this._getLocaleText("errore salvataggio skill")
                    });
                }
            };

            var fSuccessAddLastSkill = function (result) {
                this.oDialog.setBusy(false);
                if (JSON.parse(result).status !== "OK") {
                    sap.m.MessageBox.alert(result, {
                        title: this._getLocaleText("errore salvataggio skill")
                    });
                } else {
                    this._clearInputValue();
                    this.oDialog.setBusy(false);
                    this.router.navTo("consultantList");
                }
            };

            var fSucceddAddContultantToStaff = function (result) {
                var skillAggiunti = this.detailConsulente.getProperty("/codSkillSelected");
                var primarySkill = this.detailConsulente.getProperty('/primarySkill');
                for (var i = 0; i < skillAggiunti.length; i++) {
                    model.Skill.addSkillToConsultant(this.consulenteSalvato.codConsulente, skillAggiunti[i])
                        .then(fSuccessAddSkill, fError);
                }
                // L'ultima skill sarà la skill principale che sarà sempre valorizzata
                model.Skill.addSkillToConsultant(this.consulenteSalvato.codConsulente, primarySkill, true)
                    .then(fSuccessAddLastSkill, fError);

                this._clearInputValue();
                this.oDialog.setBusy(false);
                this.router.navTo("consultantList");
            };

            var fSuccessAddConsultant = function (result) {
                this.consulenteSalvato = JSON.parse(result).results;
                var initName = this.consulenteSalvato.nomeConsulente.slice(0, 1);
                var alias = initName.concat(this._checkAccenti(this.consulenteSalvato.cognomeConsulente));
                alias = alias.toUpperCase();
                var cognomeConsulente = this.consulenteSalvato.cognomeConsulente;
                var nomeConsulente = this.consulenteSalvato.nomeConsulente;
                var password = btoa(alias.toLowerCase());
                var stato = this.consulenteSalvato.statoConsulente;
                var primarySkill = this.detailConsulente.getProperty('/primarySkill');
                var codConsulente = this.consulenteSalvato.codConsulente;

                model.UserLogin.add(alias, cognomeConsulente, nomeConsulente, password, stato, codConsulente)
                    .then(function () {
                        var ferieRequest = {
                            'codCommessa': "AA00000008",
                            'codConsulenteStaff': this.consulenteSalvato.codConsulente,
                            'codPJMStaff': "CC00000021",
                            'codSkill': '',
                            'codStaff': "CSC0000031",
                            'teamLeader': "NO"
                        };
                        var icmsRequests = {
                            'codCommessa': "AA00000098",
                            'codConsulenteStaff': this.consulenteSalvato.codConsulente,
                            'codPJMStaff': "CC00000021",
                            'codSkill': primarySkill,
                            'codStaff': "CSC0000170",
                            'teamLeader': "NO"
                        };
                        var arrayReq = [ferieRequest, icmsRequests];
                        model.insertConsultantInStaff.insert(arrayReq)
                            .then(fSucceddAddContultantToStaff, fError);
                    }.bind(this), function () {
                        var ferieRequest = {
                            'codCommessa': "AA00000008",
                            'codConsulenteStaff': this.consulenteSalvato.codConsulente,
                            'codPJMStaff': "CC00000021",
                            'codSkill': '',
                            'codStaff': "CSC0000031",
                            'teamLeader': "NO"
                        };
                        var icmsRequests = {
                            'codCommessa': "AA00000098",
                            'codConsulenteStaff': this.consulenteSalvato.codConsulente,
                            'codPJMStaff': "CC00000021",
                            'codSkill': primarySkill,
                            'codStaff': "CSC0000170",
                            'teamLeader': "NO"
                        };
                        var arrayReq = [ferieRequest, icmsRequests];
                        model.insertConsultantInStaff.insert(arrayReq)
                            .then(fSucceddAddContultantToStaff, fError);
                    }.bind(this));
            };

            var fSuccessUpdateConsultant = function (res) {
            	this.consulenteModificato = JSON.parse(res).data;
            	
            	var initName = this.consulenteModificato.nomeConsulente.slice(0, 1);
                var alias = initName.concat(this._checkAccenti(this.consulenteModificato.cognomeConsulente));
                alias = alias.toUpperCase();
                var cognomeConsulente = this.consulenteModificato.cognomeConsulente;
                var nomeConsulente = this.consulenteModificato.nomeConsulente;
                var password = btoa(alias.toLowerCase());
                var stato = this.consulenteModificato.statoConsulente;
                var codConsulente = this.consulenteModificato.codConsulente;
                
            	model.UserLogin.updateByCodConsulente(alias, cognomeConsulente, nomeConsulente, password, stato, codConsulente)
            		.then(_.bind(function (succ) {
            			this._clearInputValue();
    	                this.oDialog.setBusy(false);
    	                this.router.navTo("consultantList");
            		}, this), _.bind(function (err) {
    	                this.oDialog.setBusy(false);
    	                var msg = "Anagrafica aggiornata con errore sul DB di gestione UTENTI. contattare il Team Fiori specificando l'utente modificato."
    	                sap.m.MessageBox.alert(msg, {
    	                    title: this._getLocaleText("errore salvataggio ANAGRAFICA UTENTI"),
    	                    onClose: _.bind(function(oAction) {
    	                    	this._clearInputValue();
    	                    	this.router.navTo("consultantList");
    	                    }, this)
    	                });
    	                
            		}, this));
            	
//            	if (this.changeStatus) {
//            		model.UserLogin.updateByCodConsulente()
//            	} else {
//	                this._clearInputValue();
//	                this.oDialog.setBusy(false);
//	                this.router.navTo("consultantList");
//            	}
            };

            fSucceddAddContultantToStaff = _.bind(fSucceddAddContultantToStaff, this);
            fSuccessUpdateConsultant = _.bind(fSuccessUpdateConsultant, this);
            fSuccessAddConsultant = _.bind(fSuccessAddConsultant, this);
            fSuccessAddLastSkill = _.bind(fSuccessAddLastSkill, this);
            fSuccessAddSkill = _.bind(fSuccessAddSkill, this);
            fError = _.bind(fError, this);

            var cogConsIn = this.getView().byId("idCognomeConsulente").getValue();
            cogConsIn = cogConsIn.trim();
            var cognomeConsulente;
            if (cogConsIn.split(" ").length === 1) {
                cognomeConsulente = cogConsIn.substr(0, 1).toUpperCase() + cogConsIn.substr(1, cogConsIn.length).toLowerCase();
            } else {
                cognomeConsulente = (cogConsIn.split(" ")[0]).substr(0, 1).toUpperCase() + (cogConsIn.split(" ")[0]).substr(1, (cogConsIn.split(" ")[0]).length).toLowerCase();
                for (var i = 1; i < cogConsIn.split(" ").length; i++) {
                    cognomeConsulente = cognomeConsulente.concat(" ");
                    cognomeConsulente = cognomeConsulente.concat((cogConsIn.split(" ")[i]).substr(0, 1).toUpperCase() + (cogConsIn.split(" ")[i]).substr(1, (cogConsIn.split(" ")[i]).length).toLowerCase());
                }
            }
            if (!cognomeConsulente || cognomeConsulente === undefined || cognomeConsulente === "") {
                this._enterErrorFunction("alert", "Errore inserimento campi", "Cognome non compilato correttamente");
                return;
            }

            var nomeConsIn = this.getView().byId("idNomeConsulente").getValue();
            nomeConsIn = nomeConsIn.trim();
            var nomeConsulente;
            if (nomeConsIn.split(" ").length === 1) {
                nomeConsulente = nomeConsIn.substr(0, 1).toUpperCase() + nomeConsIn.substr(1, nomeConsIn.length).toLowerCase();
            } else {
                nomeConsulente = (nomeConsIn.split(" ")[0]).substr(0, 1).toUpperCase() + (nomeConsIn.split(" ")[0]).substr(1, (nomeConsIn.split(" ")[0]).length).toLowerCase();
                for (var t = 1; t < nomeConsIn.split(" ").length; t++) {
                    nomeConsulente = nomeConsulente.concat(" ");
                    nomeConsulente = nomeConsulente.concat((nomeConsIn.split(" ")[t]).substr(0, 1).toUpperCase() + (nomeConsIn.split(" ")[t]).substr(1, (nomeConsIn.split(" ")[t]).length).toLowerCase());
                }
            }
            if (!nomeConsulente || nomeConsulente === undefined || nomeConsulente === "") {
                this._enterErrorFunction("alert", "Errore inserimento campi", "Nome non compilato correttamente");
                return;
            }

            var primSkill = this.getView().byId("idPrimarySkill").getSelectedKey();
            if (!primSkill || primSkill === undefined || primSkill === "") {
                this._enterErrorFunction("alert", "Errore inserimento campi", "Skill principale non inserita");
                return;
            }

            var profiloInput = _.find(this.detailConsulente.getData().profiloConsulente, {
                'DESCRIZIONE_PROFILO': this.getView().byId("codProfiloConsulente").getProperty("value")
            });
            var codProfiloConsulente = "";
            if (profiloInput) {
                codProfiloConsulente = profiloInput.COD_PROFILO;
            } else {
                codProfiloConsulente = "CP00000001";
            }
            var buInput = _.find(this.modelBu.getProperty("/items"), {
                nomeBu: this.getView().byId("idCodConsulenteBu").getValue().split(" - ")[0]
            });
            var codConsulenteBu = "";
            var descBu = "";
            if (buInput) {
                codConsulenteBu = buInput.codiceBu;
                descBu = this.getView().byId("idCodConsulenteBu").getValue();
            } else {
                codConsulenteBu = "BU001";
                descBu = "NE - nord - est";
            }
            var tipoAzienda = this.getView().byId("idTipoAzienda").getValue();
            if (!tipoAzienda || tipoAzienda === undefined || tipoAzienda === "") {
                stato = "Interni";
            }
            var stato = this.getView().byId("idStatoConsulente").getValue();
            if (!stato || stato === undefined || stato === "") {
                stato = "APERTO";
            }
            var note = this.getView().byId("idNote").getValue();
            var ggInterCompany = this.detailConsulente.getData().ggIntercompany;
            if (ggInterCompany === "") {
                ggInterCompany = 0;
            }
            var tariffaIntercompany = this.detailConsulente.getData().tariffaIntercompany;
            if (tariffaIntercompany === "") {
                tariffaIntercompany = 0;
            }
            var intercompany = this.detailConsulente.getData().intercompany;
            var partTime = this.detailConsulente.getData().partTime;
            var orePartTime = this.detailConsulente.getData().orePartTime;
            if (orePartTime === "") {
                orePartTime = 0;
            }
            var cid = this.detailConsulente.getData().cid;
            this.oDialog.setBusy(true);
            if (this.state === "new") {
                model.Consultants.addConsultant(cognomeConsulente, nomeConsulente, codProfiloConsulente, codConsulenteBu, descBu, tipoAzienda, stato, note, ggInterCompany, tariffaIntercompany, intercompany, partTime, orePartTime, cid)
                    .then(fSuccessAddConsultant, fError);
            } else {
                var codConsulente = this.getView().byId("idCodConsulente").getValue();
                model.Consultants.updateConsultant(codConsulente, cognomeConsulente, nomeConsulente, codProfiloConsulente, codConsulenteBu, descBu, tipoAzienda, stato, note, ggInterCompany, tariffaIntercompany, intercompany, partTime, orePartTime)
                    .then(fSuccessUpdateConsultant, fError);
            }
        }
    },

    _clearInputValue: function () {
        this.getView().byId("idCodConsulente").setValue("");
        this.getView().byId("idCognomeConsulente").setValue("");
        this.getView().byId("idNomeConsulente").setValue("");
        this.getView().byId("codProfiloConsulente").setValue("");
        this.getView().byId("idCodConsulenteBu").setValue("");
        this.getView().byId("idTipoAzienda").setValue("");
        this.getView().byId("idStatoConsulente").setValue("");
        this.getView().byId("idNote").setValue("");
        this.getView().byId("intercompany").setValue("");
        this.getView().byId("tariffaIntercompany").setValue("");
        this.getView().byId("ggIntercompany").setValue("");
        this.getView().byId("partTime").setValue("");
        this.getView().byId("orePartTime").setValue("");
    },

    onSkillChange: function (evt) {
        if (this.state !== "new") {
            var fSuccess = function (result) {
                this.oDialog.setBusy(false);
                if (JSON.parse(result).status !== "OK") {
                    sap.m.MessageBox.alert(result, {
                        title: this._getLocaleText("errore salvataggio skill")
                    });
                }
            };
            var fError = function (result) {
                this.oDialog.setBusy(false);
                sap.m.MessageBox.alert(result, {
                    title: this._getLocaleText("errore salvataggio skill")
                });
            };
            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            var skillToAdd = evt.getParameters().selected;
            var codSkillSelected = evt.getParameters().changedItem.mProperties.key;
            this.oDialog.setBusy(true);
            if (skillToAdd) {
                model.Skill.addSkillToConsultant(this.detailConsulente.getData().codConsulente, codSkillSelected)
                    .then(fSuccess, fError);
            } else {
                model.Skill.deleteSkillToConsultant(this.detailConsulente.getData().codConsulente, codSkillSelected)
                    .then(fSuccess, fError);
            }
        }
    },

    onPrimarySkillChange: function (evt) {
        var lastPrimarySkill = this.detailConsulente.getProperty("/lastPrimarySkill");
        var newPSkill = evt.getParameters().selectedItem.getProperty('key');
        if (lastPrimarySkill == newPSkill)
            return;
        var allSkills = this.detailConsulente.getProperty("/skills");
        var skillsWOPrimary = _.filter(allSkills, function (o) {
            return o.codSkill !== newPSkill;
        });
        var otherSkills = this.detailConsulente.getProperty("/codSkillSelected");
        var _length = otherSkills.length;
        otherSkills = _.remove(otherSkills, function (n) {
            return n != newPSkill;
        });
        arraySkillSelected = _.slice(otherSkills);
        arraySkillSelected.push(newPSkill);

        if (this.state !== "new") {
            var fSuccessDelete = function (result) {
                if (JSON.parse(result).status !== "OK") {
                    this.oDialog.setBusy(false);
                    sap.m.MessageBox.alert(result, {
                        title: this._getLocaleText("errore salvataggio skill")
                    });
                    this.detailConsulente.setProperty("/primarySkill", lastPrimarySkill);
                } else if (_length != otherSkills.length) {
                    model.Skill.deleteSkillToConsultant(this.detailConsulente.getData().codConsulente, newPSkill)
                        .then(fSuccessDeleteOther, fError);
                } else {
                    model.Skill.addSkillToConsultant(this.detailConsulente.getData().codConsulente, newPSkill, true)
                        .then(fSuccess, fError);
                }
            };
            var fSuccessDeleteOther = function (result) {
                if (JSON.parse(result).status !== "OK") {
                    this.oDialog.setBusy(false);
                    sap.m.MessageBox.alert(result, {
                        title: this._getLocaleText("errore salvataggio skill")
                    });
                    this.detailConsulente.setProperty("/primarySkill", lastPrimarySkill);
                } else {
                    model.Skill.addSkillToConsultant(this.detailConsulente.getData().codConsulente, newPSkill, true)
                        .then(fSuccess, fError);
                }
            };
            var fSuccess = function (result) {
                this.oDialog.setBusy(false);
                if (JSON.parse(result).status !== "OK") {
                    sap.m.MessageBox.alert(result, {
                        title: this._getLocaleText("errore salvataggio skill")
                    });
                } else {
                    this.detailConsulente.setProperty("/allSkills", arraySkillSelected);
                    this.detailConsulente.setProperty("/codSkillSelected", otherSkills);
                    this.detailConsulente.setProperty("/primarySkill", newPSkill);
                    this.detailConsulente.setProperty("/lastPrimarySkill", newPSkill);
                    this.detailConsulente.setProperty("/skillsWOPrimary", skillsWOPrimary);
                }
            };
            var fError = function (result) {
                this.oDialog.setBusy(false);
                sap.m.MessageBox.alert(result, {
                    title: this._getLocaleText("errore salvataggio skill")
                });
                this.detailConsulente.setProperty("/primarySkill", lastPrimarySkill);
            };
            fSuccessDelete = _.bind(fSuccessDelete, this);
            fSuccessDeleteOther = _.bind(fSuccessDeleteOther, this);
            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            this.oDialog.setBusy(true);

            model.Skill.deleteSkillToConsultant(this.detailConsulente.getData().codConsulente, lastPrimarySkill)
                .then(fSuccessDelete, fError);

        } else {
            this.detailConsulente.setProperty("/allSkills", arraySkillSelected);
            this.detailConsulente.setProperty("/codSkillSelected", otherSkills);
            this.detailConsulente.setProperty("/primarySkill", newPSkill);
            this.detailConsulente.setProperty("/lastPrimarySkill", newPSkill);
            this.detailConsulente.setProperty("/skillsWOPrimary", skillsWOPrimary);
        }
    },

    loadConsultantFromSap: function () {
        this.oDialog.setBusy(true);
        var fSuccess = function (success) {
            this.consultantListModel.setData(success);
            this.oDialog.setBusy(false);
            if (!this.consultantListDialog) {
                this.consultantListDialog = sap.ui.xmlfragment("view.dialog.addConsultantFromSap", this);
                this.getView().addDependent(this.consultantListDialog);
            }
            this.consultantListDialog.open();
        }
        var fError = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageToast.show(err);
        }

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.cidOdata.getConsultantListFromSap().then(fSuccess, fError);
    },

    onConsultantFromSapPress: function (evt) {
        var consultant = evt.getSource().getBindingContext("cModel").getObject();
        if (this.consultantListDialog && this.consultantListDialog.isOpen()) {
            this.consultantListDialog.close();
        }
        this.detailConsulente.setProperty("/nomeConsulente", consultant.name);
        this.detailConsulente.setProperty("/cognomeConsulente", consultant.surname);
        this.detailConsulente.setProperty("/cid", consultant.cid);
        this.detailConsulente.setProperty("/nomeCognome", consultant.surname + " " + consultant.name);
        //        sessionStorage.setItem("consultant", consultant);
    },

    onCloseDialog: function () {
        if (this.consultantListDialog && this.consultantListDialog.isOpen()) {
            this.consultantListDialog.close();
        }
        this.router.navTo("consultantList");
    },

    onSearchConsultant: function (oEvent) {
        var value = oEvent.getSource().getValue();
        var params = ["name", "surname", "cid"];
        var filtersArr = [];
        for (var i = 0; i < params.length; i++) {
            filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
        }
        var oFilter = new sap.ui.model.Filter({
            filters: filtersArr,
            and: false
        });
        var ff = sap.ui.getCore().getElementById("consultantListFromSap");
        var oBinding = ff.getBinding("items");
        oBinding.filter([oFilter]);
    },
    
    onSelectChange: function (evt) {
    	if (this.state === "modify") {
    		this.changeStatus = evt.getSource().getSelectedKey();
    	}
    }

});
