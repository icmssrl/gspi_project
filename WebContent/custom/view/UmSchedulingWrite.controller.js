jQuery.sap.require("view.abstract.AbstractController");
//if (!model.tabellaSchedulingUM) {
//    jQuery.sap.require("model.tabellaSchedulingUM");
//}
//if (!model.Periods) {
//    jQuery.sap.require("model.Periods");
//}
//if (!model.TabellaConsulentePeriodoDraft) {
//    jQuery.sap.require("model.TabellaConsulentePeriodoDraft");
//}

view.abstract.AbstractController.extend("view.UmSchedulingWrite", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        //        if (name !== "umSchedulingWrite") {
        //            return;
        //        }
        if (!this._checkRoute(evt, "umSchedulingWrite")) {
            return;
        }
        this.oDialog = this.getView().byId("BlockLayout");
        this.visibleModel.setProperty("/writeRequestsButton", false);
        this.readPeriods();
    },

    // funzione che legge i periodi, e visualizza gli ultimi 12 in ordine decrescente
    readPeriods: function () {
        var fSuccess = function (result) {
            var res = {};
            _.remove(result.items, {
                stato: 'CHIUSO'
            });
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for (var i = 0; i < 13; i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.periodModel.setData(res13);
            this.console("Periods: ", "success");
            this.console(res.items);
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");
            this.readBuList();
        };
        var fError = function () {
            this.console("errore in lettura periodo", "error");
            this._enterErrorFunction("error", "Errore", "errore in lettura periodo");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    // funzione per leggere le bu attive
    readBuList: function () {
        var fSuccess = function (result) {
            this.buList = result.items;
            this.console("Business Unit:", "success");
            this.console(result.items);
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            this.console("errore in lettura business unit", "error");
            this._enterErrorFunction("error", "Errore", "errore in lettura business unit");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var stato = "APERTO";
        model.BuList.getBuListByStatus(stato)
            .then(fSuccess, fError);
    },

    // funzione che al cambio del periodo verifica se la tabella 'GSPI_STATO_CONSULENTE_PERIODO_DRAFT' è già stata popolata per il periodo selezioanto e visualizza il tasto di scrivi richieste
    onChangePeriod: function (pe) {
        this.oDialog.setBusy(true);

        this.visibleModel.setProperty("/writeRequestsButton", false);

        var fSuccess = function (res) {
            if (res === "OK") {
                this.consulentiPresentiNelPeriodo = true;
            } else {
                this.consulentiPresentiNelPeriodo = false;
            }
            this.visibleModel.setProperty("/writeRequestsButton", true);
            this.oDialog.setBusy(false);
        };

        var fError = function () {
            sap.m.MessageToast.show("Problemi nella lettura della tabella 'GSPI_STATO_CONSULENTE_PERIODO_DRAFT'");
            this.oDialog.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.period = pe.getSource().getProperty("selectedKey");
        //        var codBu = model.persistence.Storage.session.get("user").codConsulenteBu;
        //        model.TabellaConsulentePeriodoDraft.check(parseInt(this.period), codBu)
        model.TabellaConsulentePeriodoDraft.check(parseInt(this.period))
            .then(fSuccess, fError);
    },

    // apro actionSheet per decidere se caricare solo la BU di riferimento, o tutte le richieste
    writeRequestsPress: function (evt) {
        var oButton = evt.getSource();

        if (!this._actionSheetCharge) {
            this._actionSheetCharge = sap.ui.xmlfragment(
                "view.fragment.ActionSheetWriteUM",
                this
            );
            this.getView().addDependent(this._actionSheetCharge);
        }
        this._actionSheetCharge.setModel(this.getView().getModel("i18n"), "i18n");
        this._actionSheetCharge.openBy(oButton);
    },

    // confermo scrittura di richieste di BU di riferimento
    writeRequestsBu: function () {
        this.bu = model.persistence.Storage.session.get("user").codConsulenteBu;
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_WRITE_REQUEST_BU"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_WRITE_REQUEST_TITLE"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmWriteRequest, this)
        );
    },

    // confermo scrittura di tutte le richieste
    writeRequestsAll: function () {
        this.bu = undefined;

        this.oDialog.setBusy(true);
        var fError = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageToast.show(this._getLocaleText("erroreCancellazioneRichieste"));
        };

        var fSuccess = function (ris) {
            for (var i = 0; i < this.buList.length; i++) {
                if (!_.find(ris.results, {
                        CODICE_BU: this.buList[i].codiceBu
                    })) {
                    this.giaScritto = false;
                } else {
                    this.giaScritto = true;
                    break;
                }
            }

            this.oDialog.setBusy(false);

            if (!this.giaScritto) {
                sap.m.MessageBox.show(this._getLocaleText("CONFIRM_WRITE_REQUEST_ALL"),
                    sap.m.MessageBox.Icon.ALLERT,
                    this._getLocaleText("CONFIRM_WRITE_REQUEST_TITLE"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                    _.bind(this._confirmWriteRequest, this)
                );
            } else {
                sap.m.MessageBox.show(this._getLocaleText("CONFIRM_WRITE_REQUEST_ALL_GIA_SCRITTO"),
                    sap.m.MessageBox.Icon.ALLERT,
                    this._getLocaleText("CONFIRM_WRITE_REQUEST_TITLE"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                    _.bind(this._confirmWriteRequest, this)
                );
            }
        };

        fError = _.bind(fError, this);
        fSuccess = _.bind(fSuccess, this);

        model.tabellaSchedulingUM._checkJoinRichiesteDraftCommesse(this.period)
            .then(fSuccess, fError);
    },

    // se la tabella 'GSPI_STATO_CONSULENTE_PERIODO_DRAFT' è già stata scritta esegue solo la scrittura delle richieste
    _confirmWriteRequest: function (evt) {
        if (evt === sap.m.MessageBox.Action.YES) {
            if (!this.consulentiPresentiNelPeriodo) {
                this.writeConsultants();
            } else {
                this.confirmWrite();
            }
        } else {
            return;
        }
    },

    // cancellazione, e scrittura delle richieste
    confirmWrite: function () {
        var fLetturaRichiesteError = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageToast.show(this._getLocaleText("erroreLetturaRichieste"));
        };
        var fCancellazioneRichiesteError = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageToast.show(this._getLocaleText("erroreCancellazioneRichieste"));
        };
        var fLetturaRichiesteSuccess = function (ris) {
            //            var results = JSON.parse(ris);
            this.oDialog.setBusy(false);
            this.visibleModel.setProperty("/writeRequestsButton", false);
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");
            sap.m.MessageToast.show(this._getLocaleText("SUCCESSO_SCRITTURA_RICHIESTE"));
        };
        var fCancellazioneRichiesteSuccess = function (ris) {
            if (ris.indexOf("OK") >= 0) {
                //                var codBu = model.persistence.Storage.session.get("user").codConsulenteBu;
                model.tabellaSchedulingUM.readRequests(this.period, this.bu)
                    .then(fLetturaRichiesteSuccess, fLetturaRichiesteError);
            }
        };
        fLetturaRichiesteError = _.bind(fLetturaRichiesteError, this);
        fCancellazioneRichiesteError = _.bind(fCancellazioneRichiesteError, this);
        fLetturaRichiesteSuccess = _.bind(fLetturaRichiesteSuccess, this);
        fCancellazioneRichiesteSuccess = _.bind(fCancellazioneRichiesteSuccess, this);

        this.oDialog.setBusy(true);
        //        var codBu = this.bu;
        model.tabellaSchedulingUM.deleteRequestsFromDraft(this.period, this.bu)
            .then(fCancellazioneRichiesteSuccess, fCancellazioneRichiesteError);
    },

    // scrittura della tabella 'GSPI_STATO_CONSULENTE_PERIODO_DRAFT'
    writeConsultants: function () {
        this.oDialog.setBusy(true);
        var fSuccess = function (res) {
            this.visibleModel.setProperty("/writeRequestsButton", false);
            this.oDialog.setBusy(false);
            this.confirmWrite();
        };

        var fError = function () {
            sap.m.MessageToast.show("Problemi nella scrittura della tabella 'GSPI_STATO_CONSULENTE_PERIODO_DRAFT'");
            this.oDialog.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        //        var codBuPjm = JSON.parse(sessionStorage.getItem("user")).codConsulenteBu;
        model.TabellaConsulentePeriodoDraft.write(parseInt(this.period), this.buList)
            .then(fSuccess, fError);
    },

    // navigazione alla launchpad
    onLaunchpadPress: function () {
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.onNavBackPress();
    },
});