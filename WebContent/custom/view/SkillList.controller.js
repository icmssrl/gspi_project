jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.Skill");
jQuery.sap.require("model.BusinessUnit");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("sap.m.MessageToast");

view.abstract.AbstractMasterController.extend("view.SkillList", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.modelSkill = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelSkill, "modelSkill");

        this.uiModel.setProperty("/searchProperty", ["nomeSkill"]);
    },

    handleRouteMatched: function (evt) {


        var name = evt.getParameter("name");
        this.name = name;

        if (name !== "skillList") {
            return;
        }
        view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
        this.getView().byId("list").removeSelections();
        this.oDialog = this.getView().byId("list");

        if (model.persistence.Storage.session.get("selectedSkill")) {
            model.persistence.Storage.session.remove("selectedSkill");
        };

        this.readSkills();
    },

    readSkills: function () {
        var fSuccess = function (result) {
            this.modelSkill.setProperty("/skill", result.items);
            this.console("skills", "success");
            this.console(result);
            this.oDialog.setBusy(false);
        };
        var fError = function (err) {
            this.oDialog.setBusy(false);
            this.console(JSON.parse(err.response.body).error.message.value, "error");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.Skill.readSkills()
            .then(fSuccess, fError);
    },

    onSkillSelect: function (evt) {
        var src = evt.getSource();
        if (src.getSelectedItems()[0]) {
            var selectedItem = src.getSelectedItems()[0].getBindingContext("modelSkill").getObject();
            sessionStorage.setItem("selectedSkill", JSON.stringify(selectedItem));
            this.router.navTo("skillDetail", {
                detailId: selectedItem.codSkill
            });
        }
    },

    addSkillPress: function () {
        this.router.navTo("skillModify", {
            state: "new"
        });
        if (sessionStorage.getItem("selectedSkill")) {
            sessionStorage.removeItem("selectedSkill");
        };
    },

    onLaunchpadPress: function () {
        this.router.navTo("launchpad");
        if (model.persistence.Storage.session.get("selectedSkill")) {
            model.persistence.Storage.session.remove("selectedSkill");
        }
    }
});