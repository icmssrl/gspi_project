jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.RequestTypes");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");

view.abstract.AbstractController.extend("view.RequestTypeDetail", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.detailTipiRichiesta = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailTipiRichiesta, "detailTipiRichiesta");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "requestTypeDetail") {
            return;
        };
        this.oDialog = this.getView().byId("BusyDialog");

        var tipoRichiesta = JSON.parse(sessionStorage.getItem("selectedRequestType"));

        this.detailTipiRichiesta.setProperty("/codiceTipoRichiesta", tipoRichiesta.codiceTipoRichiesta);
        this.detailTipiRichiesta.setProperty("/descrizioneTipoRichiesta", tipoRichiesta.descrizioneTipoRichiesta);
    },

    modifyRequestTypePress: function () {
        this.router.navTo("requestTypeModify", {
            state: "modify",
            detailId: this.detailTipiRichiesta.getProperty("/codiceTipoRichiesta")
        });
    },

    deleteRequestTypePress: function () {
        sap.m.MessageBox.show(this._getLocaleText("confirmDeleteRequestType"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("confirmDeleteTitleRequestType"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmDeletePress, this)
        );
    },

    _confirmDeletePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            var fSuccess = function () {
                this.oDialog.setBusy(false);
                this.router.navTo("requestTypeList");
            };
            var fError = function () {
                this.oDialog.setBusy(false);
                console.log("errore nel salvataggio");
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            model.RequestTypes.deleteRequestType(this.detailTipiRichiesta.getProperty("/codiceTipoRichiesta"))
                .then(fSuccess, fError);
        };
    },

});
