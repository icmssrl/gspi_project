jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("utils.Busy");

view.abstract.AbstractController.extend("view.UmScheduling", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.pmSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingModel, "pmSchedulingModel");
        this.pmSchedulingModel.setSizeLimit(1000);

        this.umSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.umSchedulingModel, "umSchedulingModel");
        this.umSchedulingModel.setSizeLimit(1000);
        this.umSchedulingModel.setProperty("/filtroLocked", "0");

        this.deletedRequestsModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.deletedRequestsModel, "deletedRequestsModel");
        this.deletedRequestsModel.setSizeLimit(1000);

        this.pmSchedulingConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingConsultantModel, "pmSchedulingConsultantModel");
        this.pmSchedulingConsultantModel.setSizeLimit(1000);

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");
        this.visibleModel.setSizeLimit(1000);

        this.noteModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteModel, "noteModel");
        this.noteModel.setSizeLimit(1000);

        this.modelloIcone = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelloIcone, "iconModel");
        this.modelloIcone.setSizeLimit(1000);

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");
        this.userModel.setSizeLimit(1000);

        this.filterLockedModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.filterLockedModel, "filterLockedModel");
        this.filterLockedModel.setSizeLimit(1000);

        this.listaCommesseRicModModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.listaCommesseRicModModel, "listaCommesseRicModModel");
        this.listaCommesseRicModModel.setSizeLimit(1000);

        this.listaCommesseConsulentiGenericiModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.listaCommesseConsulentiGenericiModel, "listaCommesseConsulentiGenericiModel");
        this.listaCommesseConsulentiGenericiModel.setSizeLimit(1000);

        this.noteConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteConsultantModel, "noteConsultantModel");
        this.noteConsultantModel.setSizeLimit(1000);

        this.iconJsonModel = {
            'full': "sap-icon://complete",
            'half': "sap-icon://time-entry-request",
            'wCons': "sap-icon://customer-and-contacts",
            'wCust': "sap-icon://collaborate",
            'wNonCons': "sap-icon://employee-rejections",
            'wNonCust': "sap-icon://offsite-work",
            'notConfirmed': "sap-icon://role",
            'del': "sap-icon://question-mark",
            'halfText': "1/2",
            'globe': "sap-icon://globe",
            'travel': "sap-icon://travel-itinerary",
            'bed': "sap-icon://bed"
        };

        this.modelloIcone.setProperty("/icon", this.iconJsonModel.del);

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");
        this.periodModel.setSizeLimit(1000);

        this.pModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pModel, "pModel");
        this.pModel.setSizeLimit(1000);

        this.buModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.buModel, "buModel");
        this.buModel.setSizeLimit(1000);

        this.skillModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.skillModel, "skillModel");
        this.skillModel.setSizeLimit(1000);

        this.consultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.consultantModel, "consultantModel");
        this.consultantModel.setSizeLimit(1000);

        this.umFilterModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.umFilterModel, "umFilterModel");
        this.umFilterModel.setSizeLimit(1000);

        this.ProjectNotOpenModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.ProjectNotOpenModel, "ProjectNotOpenModel");
        this.ProjectNotOpenModel.setSizeLimit(1000);

        this.assignGenericConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.assignGenericConsultantModel, "assignGenericConsultantModel");
        this.assignGenericConsultantModel.setSizeLimit(1000);
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.userModel.setData(this.user);

        if (!this._checkRoute(evt, "umScheduling")) {
            return;
        }
        if (this._actionSheet) {
            this._actionSheet.destroy(true);
        }

        this.umBU = model.persistence.Storage.session.get("user").codConsulenteBu;
        this.userModel.setProperty("/umBU", this.umBU);

        this.oDialog = this.getView().byId("BlockLayout");
        this.oDialogTable = this.getView().byId("idSchedulazioniTableUM");
        this.getView().byId("idSchedulazioniTableUM").setVisible(false);
        this.visibleModel.setProperty("/addConsultantButton", false);
        this.visibleModel.setProperty("/enabledButton", false);
        this.visibleModel.setProperty("/closeProjectButton", false);
        this.visibleModel.setProperty("/filterButton", false);
        this.visibleModel.setProperty("/undoButton", false);
        this.visibleModel.setProperty("/sceltaCaricamento", false);
        this.visibleModel.setProperty("/caricaTutto", true);
        this.visibleModel.setProperty("/comboBoxCommesse", false);
        this.visibleModel.setProperty("/giornateTotali", false);
        this.visibleModel.setProperty("/loadChangeRequests", false);
        this.visibleModel.setProperty("/checkChangeRequests", false);
        this.visibleModel.setProperty("/visibleAssignConsultant", false);
        this.visibleModel.setProperty("/checkProjectsGeneric", false);
        this.getView().byId("idButtonSelezionaCommessa").setPressed(false);

        this.getView().byId("idSelectCommessa").setSelectedKey("");
        this.getView().byId("idSelectCommessa").setValue("");

        this.uiModel.setProperty("/searchProperty", ["nomeCognome"]);
        this.uiModel.setProperty("/searchValue", "");
        if (sap.ui.getCore().getElementById("idListaConsulentiUM")) {
            sap.ui.getCore().getElementById("idListaConsulentiUM").removeSelections();
        }
        this.readPeriods();
        this.pmSchedulingModel.getData().giorniTotali = 0;
        this.visibleModel.setProperty("/chargeButton", false);
        this.buttonRefresh = this.getView().byId("refreshUM");
        this.buttonRefresh.setEnabled(false);

        this.clearPasteCommesse();
        this.moveHere = undefined;
        this.assignHere = undefined;
        this.svuotaLocalStorageUndo();
        this.fromSaveLocal = true;
        this.commessaDaProporre = undefined;
        this.richiestaConFiltri = undefined;

        //Inserted here filter lists Data in background//
        this._loadFilters();
        //************************************************//
    },

    readPeriods: function () {
        this._getPeriodsGlobal(undefined, "readCommessePerUmSet");
    },

    readCommessePerUmSet: function () {
        model.CommessePerStaffDelUm.read()
            .then(_.bind(function (result) {
                var arrConsulente = _.uniq(result.items, 'codConsulente');
                for (var t = 0; t < arrConsulente.length; t++) {
                    arrConsulente[t].commesse = _.where(result.items, {
                        codConsulente: arrConsulente[t].codConsulente,
                        stato: 'APERTO'
                    });
                }
                this.commessePerConsulente = arrConsulente;
                //                this.readProjects();
                this.readAllProjects();
            }, this), _.bind(function (error) {
                console.error(error);
                this._enterErrorFunction("warning", "Attenzione - errore Backend", "Errore in lettura periodo.\nContattare l'amministratore del sistema");
            }, this));
    },

    readAllProjects: function () {
        var fSuccess = _.bind(function (result) {
            this.allCommesseDonwload = _.cloneDeep(result.items);
            var buUm = model.persistence.Storage.session.get("user").codConsulenteBu;
            var projectsBu = _.where(result.items, {
                'cod_bu': buUm
            });

            this.allCommesse = projectsBu;
        }, this);

        var fError = _.bind(function (result) {
            this._enterErrorFunction("warning", "Attenzione - errore Backend", "Errore in lettura progetti.\nContattare l'amministratore del sistema");
        }, this);

        model.Projects.readAllProjects(fSuccess, fError);
    },

    //    readProjects: function (req) {
    //        this.reqPeriod = req ? req : undefined;
    //        model.Projects.readProjects(req)
    //            .then(_.bind(function (result) {
    //                _.remove(result.items, {
    //                    'statoCommessa': "CHIUSO"
    //                });
    //                this.allCommesseDonwload = _.cloneDeep(result.items);
    //
    //                var buUm = model.persistence.Storage.session.get("user").codConsulenteBu;
    //                var projectsBu = _.where(result.items, {
    //                    'cod_bu': buUm
    //                });
    //
    //                this.allCommesse = projectsBu;
    //                var comboCommessa = this.getView().byId("idSelectCommessa");
    //                if (comboCommessa && !this.commessaDaRisettare) {
    //                    comboCommessa.setValue(comboCommessa.getDefaultSelectedItem());
    //                    this.commessaDaRisettare = undefined;
    //                }
    //                if (!this.commessaIcms) {
    //                    this.commessaIcms = _.find(result.items, {
    //                        nomeCommessa: 'icms'
    //                    });
    //                }
    //                if (!_.find(this.allCommesse, {
    //                        nomeCommessa: 'icms'
    //                    })) {
    //                    this.allCommesse.push(this.commessaIcms);
    //                }
    //                var commesseOrdinate = _.sortByOrder(this.allCommesse, [function (o) {
    //                    if (o.codPrioritaA !== 0)
    //                        return o.codPrioritaA;
    //                }, function (o) {
    //                    if (o.codPrioritaB !== 0)
    //                        return o.codPrioritaB;
    //                }, function (o) {
    //                    return o.nomeCommessa.toLowerCase();
    //                }]);
    //
    //                this.pmSchedulingModel.setProperty("/commesse", []);
    //                this.pmSchedulingModel.setProperty("/commesse", commesseOrdinate);
    //
    //                this.oDialog.setBusy(false);
    //            }, this), _.bind(function (error) {
    //                console.error(error);
    //                this._enterErrorFunction("warning", "Attenzione - errore Backend", "Errore in lettura progetti.\nContattare l'amministratore del sistema");
    //            }, this));
    //    },

    onChangePeriod: function (pe) {
        this.oDialog.setBusy(true);
        this.filteredConsultants = undefined;

        this.visibleModel.setProperty("/closeProjectButton", false);
        this.visibleModel.setProperty("/filterButton", false);
        this.visibleModel.setProperty("/comboBoxCommesse", false);
        this.visibleModel.setProperty("/chargeButton", false);
        this.visibleModel.setProperty("/giornateTotali", false);
        this.visibleModel.setProperty("/loadChangeRequests", false);
        this.visibleModel.setProperty("/checkChangeRequests", false);
        this.visibleModel.setProperty("/checkProjectsGeneric", false);
        this.getView().byId("idButtonSelezionaCommessa").setPressed(false);
        this.buttonRefresh.setEnabled(false);

        this.visibleModel.setProperty("/sceltaCaricamento", true);
        this.visibleModel.setProperty("/caricaTutto", true);

        this.svuotaSelect();
        this.svuotaTabella();

        var table = this.getView().byId("idSchedulazioniTableUM");

        this.pe = pe;
        var p = pe.getSource().getSelectedItem().getProperty("text").replace(/ /g, '');

        var wipPeriod = true;
        if (pe.getSource().getSelectedItem().getProperty("additionalText") !== "WIP") {
            wipPeriod = false;
        }
        this.visibleModel.setProperty("/wipPeriod", wipPeriod);

        this.tempP = p;
        var period = pe.getSource().getProperty("selectedKey");
        this.periodo = parseInt(period);
        model.persistence.Storage.session.save("period", period);
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(period)
        });
        model.persistence.Storage.session.save("periodSelected", periodSelect);
        //        this.readProjects(periodSelect.idPeriod);
        this.readProjectsByPeriod(periodSelect.idPeriod);

        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();

        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = pInizio;
        var dataF = pFine;

        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

        // rendo visibili solo le colonne del periodo selezionato
        var columns = table.getColumns();
        for (var i = 0; i < columns.length; i++) {
            columns[i].setVisible(true);
        }
        for (var i = this.numColonne; i < columns.length - 1; i++) {
            columns[i].setVisible(false);
        }

        // metto i nomi dei giorni sulle colonne
        for (var i = 2; i < columns.length; i++) {
            columns[i - 1].getLabel().mProperties.text = this.arrayGiorni[i - 2];
        }
        this.getView().byId("idSelectCommessa").setEnabled(true);

        this.periodStauts = pe.getSource().getSelectedItem().getProperty("additionalText");

        jQuery.sap.delayedCall(500, this, function () {
            this.oDialog.setBusy(false);
            if (this.getView().byId("idSelectPeriodo").getValue() === "") {
                this.getView().byId("idSelectPeriodo").setValue(this.tempP);
            }
        });
    },

    readProjectsByPeriod: function (idPeriodo) {
        this.oDialog.setBusy(true);
        var fSuccess = _.bind(function (result) {
            var comboCommessa = this.getView().byId("idSelectCommessa");
            if (comboCommessa && !this.commessaDaRisettare) {
                comboCommessa.setValue(comboCommessa.getDefaultSelectedItem());
                this.commessaDaRisettare = undefined;
            }
            var commesseOrdinate = _.sortByOrder(result.items, [function (o) {
                if (o.codPrioritaA !== 0)
                    return o.codPrioritaA;
            }, function (o) {
                if (o.codPrioritaB !== 0)
                    return o.codPrioritaB;
            }, function (o) {
                return o.nomeCommessa.toLowerCase();
            }]);

            this.pmSchedulingModel.setProperty("/commesse", []);
            this.pmSchedulingModel.setProperty("/commesse", commesseOrdinate);
            this.oDialog.setBusy(false);
        }, this);

        var fError = _.bind(function (result) {
            this._enterErrorFunction("warning", "Attenzione - errore Backend", "Errore in lettura progetti.\nContattare l'amministratore del sistema");
        }, this);

        var req = {
            idPeriod: idPeriodo,
            idBu: model.persistence.Storage.session.get("user").codConsulenteBu
        };
        model.Projects.readUmProjectsByPeriod(req, fSuccess, fError)
    },

    // se si preme il tasto 'Sleziona commessa' si visualizza la lista di progetti della BU dell'UM
    selectCommessaVisible: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            this.visibleModel.setProperty("/caricaTutto", false);
            this.visibleModel.setProperty("/comboBoxCommesse", true);
            this.visibleModel.setProperty("/giornateTotali", true);
        } else {
            this.visibleModel.setProperty("/caricaTutto", true);
            this.visibleModel.setProperty("/comboBoxCommesse", false);
            this.visibleModel.setProperty("/giornateTotali", false);
        }
        this.svuotaTabella();
        this.pmSchedulingModel.setProperty("/giorniTotali", 0);
        this.visibleModel.setProperty("/loadChangeRequests", false);
        this.visibleModel.setProperty("/checkChangeRequests", false);
    },

    // funzione che carica la lista di consulenti di tutti i progetti
    chargeProjectsNotSelected: function () {
        this.richiestaConFiltri = undefined;
        this.getView().byId("idSelectCommessa").setSelectedKey("");
        this.getView().byId("idSelectCommessa").setValue("");
        if (model.persistence.Storage.session.get("commessa")) {
            model.persistence.Storage.session.remove("commessa");
        }
        var req = {};
        req.umBU = this.umBU;
        req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
        this.ric = req;
        this.leggiTabella(req);
        this.visibleModel.setProperty("/filterButton", true);
        this.visibleModel.setProperty("/giornateTotali", true);
        this.pmSchedulingModel.setProperty("/commessa", "");
        //        this.visibleModel.setProperty("/checkChangeRequests", true);
    },

    giorniMese: function (mese, anno) {
        var d = new Date(anno, mese, 0);
        return d.getDate();
    },

    onSelectionChangeCommessa: function (evt) {
        this.visibleModel.setProperty("/chargeButton", true);
        //        this.chosedCom = evt.getSource().getValue();
        this.chosedCom = evt.getSource().getValue().split(" - ")[0];
        this.codCommessaSelezionata = evt.getSource().getSelectedKey();
        this.chosedComDescription = evt.getSource().getValue().split(" - ")[1];
        this.pmSchedulingModel.setProperty("/commessa", this.chosedCom);
        this.pmSchedulingModel.setProperty("/descrizioneCommessa", this.chosedComDescription);
        this.buttonRefresh.setEnabled(false);
        this.richiestaConFiltri = undefined;
    },

    chargeProjects: function (evt) {
        this.richiestaConFiltri = undefined;
        this.visibleModel.setProperty("/chargeButton", false);
    },

    onChangeCommessa: function (evt) {
        this.oDialog.setBusy(true);

        if (!this.codCommessaSelezionata) {
            this._enterErrorFunction("alert", "Attenzione", "Commessa '" + this.chosedCom.toUpperCase() + "' non trovata");
            return;
        }
        var fSuccess = function (res) {
            this.currentStaff = _.find(res.items, {
                codCommessa: this.codCommessaSelezionata
            });
            if (!this.currentStaff) {
                this.currentStaff = _.find(this.pmSchedulingModel.getData().commesse, {
                    codCommessa: this.codCommessaSelezionata
                });
            }
            this.console(this.currentStaff);
            this._onChangeCommessa(this.codCommessaSelezionata);
            this.clearPasteCommesse();
        };

        var fError = function () {
            sap.m.MessageToast.show("errore in lettura anagrafica staff");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialogTable.setBusy(true);
        model.StaffCommesseView.readStaffCommessa()
            .then(fSuccess, fError);
        this.visibleModel.setProperty("/chargeButton", false);
    },

    _onChangeCommessa: function (commessaP) {
        this.filteredConsultants = undefined;
        if (commessaP) {
            // verifica se questa commessa per questo periodo è "in lavorazione da un altro UM"
            var params = {
                codCommessa: commessaP,
                idPeriodo: this.periodo
            };
            model.tabellaSchedulingUM.checkIsToWork(params)
                .then(_.bind(function (res) {
                    this._checkIsToWork();
                    if (!res.results[0].UM_MODIFY) {
                        var commessaScelta = commessaP;

                        var c = _.find(this.pmSchedulingModel.getData().commesse, {
                            codCommessa: commessaScelta
                        });
                        model.persistence.Storage.session.save("commessa", c);

                        var req = {};
                        req.idCommessa = commessaScelta;
                        req.idStaff = this.currentStaff.codStaff;
                        req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
                        req.nomeCommessa = this.chosedCom;
                        req.umBU = this.umBU;
                        this.ric = req;

                        this.leggiTabella(req);
                        this.visibleModel.setProperty("/visibleTable", true);

                        var dataRichiesta = model.persistence.Storage.session.get("periodSelected").reqEndDate;
                        this.visibleModel.setProperty("/addConsultantButton", true);
                        this.visibleModel.setProperty("/enabledButton", true);
                        this.visibleModel.setProperty("/closeProjectButton", true);
                        this.visibleModel.setProperty("/filterButton", true);
                    } else {
                        this.oDialog = this.getView().byId("BlockLayout");
                        this.oDialogTable = this.getView().byId("idSchedulazioniTableUM");
                        this.getView().byId("idSchedulazioniTableUM").setVisible(false);
                        this.visibleModel.setProperty("/addConsultantButton", false);
                        this.visibleModel.setProperty("/enabledButton", false);
                        this.visibleModel.setProperty("/closeProjectButton", false);
                        this.visibleModel.setProperty("/filterButton", false);
                        this.visibleModel.setProperty("/undoButton", false);
                        this.visibleModel.setProperty("/giornateTotali", false);
                        this.visibleModel.setProperty("/loadChangeRequests", false);
                        this.visibleModel.setProperty("/checkChangeRequests", false);

                        this.getView().byId("idSelectCommessa").setSelectedKey("");
                        this.getView().byId("idSelectCommessa").setValue("");

                        this.svuotaTabella();

                        this.clearPasteCommesse();
                        this.moveHere = undefined;
                        this.assignHere = undefined;
                        this.svuotaLocalStorageUndo();
                        this.fromSaveLocal = true;

                        this.oDialog.setBusy(false);
                        this._enterErrorFunction("alert", "Attenzione", "Commessa in lavorazione da " + res.results[0].COGNOME_CONSULENTE + " " + res.results[0].NOME_CONSULENTE);
                    }
                }, this), _.bind(function (err) {
                    sap.m.MessageToast.show("errore in lettura AT_UM_WORK_OTHER_PROJECT");
                    this.oDialog.setBusy(false);
                }, this));
        }
        this.oDialog.setBusy(false);
    },

    actionButton: function (evt, richiesta) {
        var del = false;
        var b = this.oButton;
        var idButton = b.getId();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        var row = this.oButton.getBindingContext("pmSchedulingModel").getPath().split("/")[2];
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[row];
        var commessaComboBox = this.chosedCom;
        var prop = "gg" + nButton;
        //        var buttonPressed = evt.getSource();

        var tipoRichiesta = evt ? evt.getParameters().item.getText() : "";
        if (!richiesta) {
            var richiesta = "";
            switch (tipoRichiesta) {
                case this._getLocaleText("ASSIGN"):
                    richiesta = "Assign";
                    break;
                case this._getLocaleText("ASSIGN_HALF_DAY"):
                    richiesta = "halfDay";
                    break;
                case this._getLocaleText("C_WHIT_CONSULTANT"):
                    richiesta = "wCons";
                    break;
                case this._getLocaleText("NON_C_WHIT_CONSULTANT"):
                    richiesta = "wNonCons";
                    break;
                case this._getLocaleText("C_WHIT_CUSTOMER"):
                    richiesta = "wCust";
                    break;
                case this._getLocaleText("NON_C_WHIT_CUSTOMER"):
                    richiesta = "wNonCust";
                    break;
                case this._getLocaleText("NON_CONFIRMED_CUSTOMER"):
                    richiesta = "notConfirmed";
                    break;
                case this._getLocaleText("ABROAD"):
                    richiesta = "globe";
                    break;
                case this._getLocaleText("INTERAFERIE"):
                    richiesta = "travel";
                    break;
                case this._getLocaleText("MEZZAFERIE"):
                    richiesta = "bed";
                    break;
                case this._getLocaleText("NOTE"):
                    richiesta = "note";
                    break;
                default:
                    richiesta = "del";
                    break;
            }
        }
        var tempPropIcon = "ggIcon" + nButton;
        var nomeCommessaUguale = "";
        var commessaUguale;
        if (this.commessa) {
            commessaUguale = _.find(this.commesseQuestoConsulente, {
                codCommessa: this.commessa
            });
        } else {
            commessaUguale = _.find(this.commesseQuestoConsulente, {
                nomeCommessa: dataModelConsultant[prop]
            });
        }
        if (commessaUguale) {
            nomeCommessaUguale = commessaUguale.nomeCommessa;
        }

        var tempProp = prop;
        var items = ""; //serie di "a" aggiunte alla proprietà
        if (richiesta !== "del" && richiesta !== "note") {
            while (dataModelConsultant[tempProp]) {
                if (dataModelConsultant[tempProp] === nomeCommessaUguale) {
                    prop = tempProp;
                    break;
                }
                items = "a";
                tempProp = tempProp + items;
            }
            if (dataModelConsultant[prop] && dataModelConsultant[prop] !== nomeCommessaUguale) {
                items = "";
                while (dataModelConsultant[prop]) {
                    items = "a";
                    prop = prop + items;
                    var propNotaGiorno = prop.replace("gg", "nota");
                    var propReq = prop.replace("gg", "req");
                    var idReqOld = prop.replace("gg", "idReqOld");
                    var idReqOriginal = prop.replace("gg", "idReqOriginal");
                }
            } else {
                propNotaGiorno = prop.replace("gg", "nota");
                propReq = prop.replace("gg", "req");
                idReqOld = prop.replace("gg", "idReqOld");
                idReqOriginal = prop.replace("gg", "idReqOriginal");
            }
        } else {
            propReq = prop.replace("gg", "req");
            idReqOld = prop.replace("gg", "idReqOld");
            idReqOriginal = prop.replace("gg", "idReqOriginal");
            var tempPropA = tempProp + "a";
            var ggIconA = prop.replace("gg", "ggIcon") + "a";
            var requests = [];
            var req;
            var propIdReq;
            var propIdReqOld;
            var propIconReq;
            var propDataReq;
            var propNote;
            if (dataModelConsultant[tempPropA] && dataModelConsultant[ggIconA]) {
                // if (dataModelConsultant[tempPropA] && dataModelConsultant[ggIconA] !== "sap-icon://offsite-work") {
                // scegliere commessa
                for (var t = 0; t < 3; t++) {
                    if (t === 0) {
                        req = dataModelConsultant[tempProp];
                        propIdReq = tempProp.replace("gg", "req");
                        propIdReqOld = tempProp.replace("gg", "idReqOld");
                        propIconReq = tempProp.replace("gg", "ggIcon");
                        propDataReq = tempProp.replace("gg", "ggReq");
                        propNote = tempProp.replace("gg", "nota");
                    } else if (t === 1) {
                        req = dataModelConsultant[tempPropA];
                        propIdReq = tempPropA.replace("gg", "req");
                        propIdReqOld = tempPropA.replace("gg", "idReqOld");
                        propIconReq = tempPropA.replace("gg", "ggIcon");
                        propDataReq = tempPropA.replace("gg", "ggReq");
                        propNote = tempPropA.replace("gg", "nota");
                    } else {
                        var tempPropAA = tempPropA + "a";
                        req = dataModelConsultant[tempPropAA];
                        propIdReq = tempPropAA.replace("gg", "req");
                        propIdReqOld = tempPropAA.replace("gg", "idReqOld");
                        propIconReq = tempPropAA.replace("gg", "ggIcon");
                        propDataReq = tempPropAA.replace("gg", "ggReq");
                        propNote = tempPropAA.replace("gg", "nota");
                    }
                    if (req) {
                        var icon = dataModelConsultant[propIconReq];
                        var idReq = dataModelConsultant[propIdReq];
                        var idReqOld = dataModelConsultant[propIdReqOld];
                        var commessa = _.find(this.commesseQuestoConsulente, {
                            'nomeCommessa': req
                        });
                        var idCommessa = commessa.codCommessa;
                        var pjmAss = commessa.codPjmAssociato;
                        var codConsulente = dataModelConsultant.codiceConsulente;
                        var dataReqDraft = dataModelConsultant[propDataReq];
                        var noteDraft = dataModelConsultant[propNote];
                        var periodoMese = model.persistence.Storage.session.get("period");

                        var requestToPush = {
                            'codConsulenteDraft': codConsulente,
                            'dataReqDraft': dataReqDraft,
                            'idCommessaDraft': idCommessa,
                            'codAttivitaRichiestaDraft': icon.split("//")[1],
                            'idPeriodoSchedulingDraft': periodoMese,
                            'codPjmDraft': pjmAss,
                            'dataInserimentoDraft': dataReqDraft,
                            'noteDraft': noteDraft,
                            'giornoPeriodoDraft': nButton,
                            'id_req': idReq,
                            'oldIdReqDraft': idReqOld,
                            'req': req,
                            'icon': icon
                        };
                        // non metto dentro il dialog la richiesta di non schedulabilità
                        // if (icon !== "sap-icon://offsite-work")
                        requests.push(requestToPush);
                    }
                }
                if (!this.dialogCancellaRichiesta)
                    this.dialogCancellaRichiesta = sap.ui.xmlfragment("view.dialog.dialogCancellaRichiesta", this);
                this.getView().addDependent(this.dialogCancellaRichiesta);
                this.pmSchedulingModel.setProperty("/requestsToDelete", requests);
                this.dialogCancellaRichiesta.open();
                this.listaRichiesteDaCancellare = sap.ui.getCore().getElementById("idRichiesteDaCancellareList");
                this.listaRichiesteDaCancellare.removeSelections();
                return;
            }
        }

        this.oButton = "";
        // prendo il numero della tabella per passare correttamente la data
        var table = this.getView().byId("idSchedulazioniTableUM");
        var giornoMese = parseInt(table.getColumns()[nButton].getLabel().getProperty("text"));
        //** generazione data in modo corretto
        if (giornoMese < 10) {
            giornoMese = "0" + giornoMese;
        }
        var periodoMese = model.persistence.Storage.session.get("periodSelected").mesePeriodo;
        if (parseInt(giornoMese) < 10 && nButton > 20) {
            if (parseInt(periodoMese) !== 12) {
                periodoMese = parseInt(periodoMese) + 1;
            } else {
                periodoMese = "1";
                var anno = parseInt(anno) + 1;
            }
        }
        if (periodoMese < 10) {
            periodoMese = "0" + periodoMese;
        }
        var anno = model.persistence.Storage.session.get("periodSelected").annoPeriodo;
        if (parseInt(periodoMese) === 12) {
            anno = parseInt(anno) + 1;
        }
        var dataRichiesta = anno + "-" + periodoMese + "-" + giornoMese;
        if (!(new Date(dataRichiesta)).getYear()) {
            var lastDayOfMonth = new Date(parseInt(dataRichiesta.split("-")[0]), parseInt(dataRichiesta.split("-")[1]), 0);
            lastDayOfMonth = lastDayOfMonth.getDate();
            var mesePlus = parseInt(periodoMese) + 1;
            if (mesePlus < 10) {
                mesePlus = "0" + mesePlus;
            }
            var giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
            dataRichiesta = anno + "-" + mesePlus + "-" + giornoPlus;
        }
        var note = "";
        if (this.paste) {
            note = model.persistence.Storage.local.get("copy").note ? model.persistence.Storage.local.get("copy").note : "";
        } else if (this.moveHere) {
            note = model.persistence.Storage.local.get("move").note ? model.persistence.Storage.local.get("move").note : "";
        } else if (this.assignHere) {
            note = this.objToAssign.note;
        } else {
            var propNoteNew = propReq.replace("req", "nota");
            if (dataModelConsultant[propNoteNew]) {
                note = dataModelConsultant[propNoteNew]
            }
        }
        //**************************
        var dataToSend = {
            "codConsulenteDraft": "",
            "dataReqDraft": dataRichiesta,
            "idCommessaDraft": "",
            "codAttivitaRichiestaDraft": "",
            "idPeriodoSchedulingDraft": 0,
            "codPjmDraft": "",
            "dataInserimentoDraft": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "noteDraft": note,
            "giornoPeriodoDraft": 0,
            "statoReqDraft": "CONFIRMED",
            "id_req": dataModelConsultant[propReq] ? dataModelConsultant[propReq].toString() : undefined,
            "oldIdReqDraft": dataModelConsultant[idReqOld] ? dataModelConsultant[idReqOld] : dataModelConsultant[propReq] ? 0 : 1,
            "idReqOriginal": dataModelConsultant[idReqOriginal] ? dataModelConsultant[idReqOriginal] : 0,
        };

        dataToSend.codConsulenteDraft = dataModelConsultant.codiceConsulente;
        if (commessaUguale) {
            dataToSend.codPjmDraft = commessaUguale.codPjmAssociato;
        }
        if (this.paste) {
            var comProv = _.find(this.pmSchedulingModel.getProperty("/commesse"), {
                codCommessa: model.persistence.Storage.local.get("copy").commessa
            });
            dataToSend.codPjmDraft = comProv.codPjmAssociato;
        }
        dataToSend.idPeriodoSchedulingDraft = this.periodo;

        if (this.commessa) {
            dataToSend.idCommessaDraft = this.commessa;
            this.commessa = undefined;
        } else if (this.paste) {
            dataToSend.idCommessaDraft = model.persistence.Storage.local.get("copy").commessa;
        } else if (this.moveHere) {
            dataToSend.idCommessaDraft = model.persistence.Storage.local.get("move").commessa;
            dataToSend.oldIdReqDraft = (model.persistence.Storage.local.get("move").idReq).toString();
            model.persistence.Storage.local.remove("move");
            this.moveHere = undefined;
        } else if (this.assignHere) {
            dataToSend.idCommessaDraft = this.objToAssign.commessa;
            dataToSend.oldIdReqDraft = (this.objToAssign.idReq).toString();
            this.objToAssign = undefined;
            this.assignHere = undefined;
        } else {
            if (dataModelConsultant[prop] !== "feri") {
                dataToSend.idCommessaDraft = _.find(this.commesseQuestoConsulente, {
                    nomeCommessa: dataModelConsultant[prop]
                }).codCommessa;
            } else {
                dataToSend.idCommessaDraft = "AA00000008";
            }
        }
        dataToSend.giornoPeriodoDraft = nButton;

        var o = _.cloneDeep(this.pmSchedulingModel.getData().schedulazioni[row]);
        switch (richiesta) {
            case "Assign":
            case "sap-icon://complete":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaDraft = "complete";
                break;
            case "halfDay":
            case "sap-icon://time-entry-request":
                if (b.getSrc() === "sap-icon://time-entry-request" || b.getSrc() === "sap-icon://bed") {
                    if (o[prop] && o[prop] === this.chosedCom) {
                        o[prop] = this.chosedCom;
                    } else if (o[prop] && o[prop] !== this.chosedCom) {
                        prop = prop + "a";
                        o[prop] = this.chosedCom;
                    } else {
                        o[prop] = this.chosedCom;
                    }
                }
                dataToSend.codAttivitaRichiestaDraft = "time-entry-request";
                break;
            case "wCons":
            case "sap-icon://customer-and-contacts":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaDraft = "customer-and-contacts";
                break;
            case "wNonCons":
            case "sap-icon://employee-rejections":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaDraft = "employee-rejections";
                break;
            case "wCust":
            case "sap-icon://collaborate":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaDraft = "collaborate";
                break;
            case "wNonCust":
            case "sap-icon://offsite-work":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaDraft = "offsite-work";
                break;
            case "notConfirmed":
            case "sap-icon://role":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaDraft = "role";
                break;
            case "globe":
            case "sap-icon://globe":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaDraft = "globe";
                break;
            case "travel":
            case "sap-icon://travel-itinerary":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaDraft = "travel-itinerary";
                break;
            case "bed":
            case "sap-icon://bed":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiestaDraft = "bed";
                break;
            case "del":
                var icon = b.getSrc();
                var req = icon.substring(icon.lastIndexOf("/") + 1, icon.length);
                del = true;
                o[prop] = "";
                prop = prop + "a";
                if (o[prop]) {
                    o[prop] = "";
                }
                if (req === "time-entry-request") {
                    req = "time-entry-request";
                }
                dataToSend.codAttivitaRichiestaDraft = req;

                var idArray = "req" + nButton;
                var schedulazioneConsulente = this.pmSchedulingModel.getProperty("/schedulazioni")[row];
                var idReq = parseInt(schedulazioneConsulente[idArray]);
                dataToSend.id_req = idReq;
                this.dataToSave = dataToSend;
                this._delData(dataToSend);
                break;
            case "note":
                this.dataForUpdate = dataToSend;
                this.onAddNote();
                del = true;
                break;
        }
        if (!del) {
            this.saveRequest(dataToSend, prop);
        }
    },

    onDialogCancellaRichiestaClose: function () {
        this.dialogCancellaRichiesta.close();
    },

    onDialogCancellaRichiestaOK: function () {
        var selectedItems = this.listaRichiesteDaCancellare.getSelectedItems();
        if (selectedItems.length === 0) {
            sap.m.MessageToast.show(this._getLocaleText("selezionareUnaRichiesta"));
            return;
        } else {
            var path = parseInt(selectedItems[0].sId.split("List-")[1]);
            var request = this.pmSchedulingModel.getProperty("/requestsToDelete")[path];
            if (this.isNote) {
                this.dataForUpdate = request;
                //this.isNote = undefined;
                this.onAddNote();
            } else {
                this._delData(request);
            }
        }
        this.dialogCancellaRichiesta.close();
    },

    _delData: function (dataToSend) {
        if (dataToSend.req && dataToSend.icon) {
            delete dataToSend.req;
            delete dataToSend.icon;
        }
        this.datoEliminato = dataToSend;
        this.datoEliminatoToSend = _.cloneDeep(dataToSend);
        var fSuccess = function (res) {
            if (this.fromSaveLocal) {
                if (this.saveMove) {
                    var richiestaSalvata = model.persistence.Storage.local.get("move");
                    if (this.datoEliminatoToSend.statoReqDraft === "STANDBY") {
                        this.datoEliminatoToSend.statoReqDraft = "CONFIRMED";
                    }
                    this.datoEliminato.giornoPeriodoDraft = richiestaSalvata.giornoPeriodoDraft;
                } else {
                    this.saveToLocalForUndo("del", this.datoEliminato);
                    this.visibleModel.setProperty("/undoButton", true);
                }
            }
            var elementoEliminato = this.datoEliminato;
            if (this.saveMove || this.undoMove) {
                this.saveRequest(this.datoEliminatoToSend);

            } else {
                var req = {};
                if (model.persistence.Storage.session.get("commessa")) {
                    req.single = true;
                    req.nomeCommessa = model.persistence.Storage.session.get("commessa").nomeCommessa;
                }
                req.codBu = model.persistence.Storage.session.get("user").codConsulenteBu;
                req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
                req.selectedConsultantsCode = elementoEliminato.codConsulenteDraft;
                //** gestione caso delle ferie
                var tempval = _.find(this.allCommesse, {
                    'codCommessa': this.datoEliminato.idCommessaDraft
                });
                if (tempval && tempval.codiceStaffCommessa) {
                    req.idStaff = tempval.codiceStaffCommessa;
                } else {
                    req.idStaff = "CSC0000031";
                    req.nomeCommessa = "ferie";
                }
                this.leggiTabella(req);
            }
        };
        var fError = function (err) {
            sap.m.MessageToast.show("ERRORE");
            this.visibleModel.setProperty("/assignHereVisible", false);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        //Tengo traccia di tutte le richieste cancellate
        model.requestSaveToDraft.delRequest(dataToSend.id_req, "DELETED")
            .then(fSuccess, fError);

        // if (dataToSend.oldIdReqDraft === 1) {
        //     model.requestSaveToDraft.delDefRequest(dataToSend.id_req)
        //         .then(fSuccess, fError);
        // } else {
        //     model.requestSaveToDraft.delRequest(dataToSend.id_req, "DELETED")
        //         .then(fSuccess, fError);
        // }
    },
    // *********************actionSheet*******************

    saveRequest: function (req, prop) {
        var msgOk = "Richiesta inserita correttamente";
        var msgKo = "Errore in inserimento richiesta";
        if (this.undoMove) {
            req.giornoPeriodoDraft = req.oldGiorno;
            delete req.oldGiorno;
        }

        this.dataToSave = req;
        this.propTemp = prop ? prop : (this.dataToSave ? "gg" + this.dataToSave.giornoPeriodoDraft : "");
        var fSuccess = function (ok) {
            sap.m.MessageToast.show(msgOk, {
                duration: 750,
            });
            this.commessaDaProporre = undefined;

            if (this.fromSaveLocal) {
                if (this.saveMove) {
                    this.saveMove = false;
                    var fileToSaveForUndo = model.persistence.Storage.local.get("move");
                    fileToSaveForUndo.newGiorno = this.dataToSave.giornoPeriodoDraft;
                    this.saveToLocalForUndo("move", fileToSaveForUndo);
                    model.persistence.Storage.local.remove("move");
                    this.visibleModel.setProperty("/undoButton", true);
                } else {
                    this.dataToSave.id_req = JSON.parse(ok).retData;
                    this.saveToLocalForUndo("insert", this.dataToSave);
                    this.visibleModel.setProperty("/undoButton", true);
                }
            }

            if (!this.undoMove) {
                var req = {};
                req.codBu = model.persistence.Storage.session.get("user").codConsulenteBu;
                req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
                req.selectedConsultantsCode = this.dataToSave.codConsulenteDraft;
                if (model.persistence.Storage.session.get("commessa")) {
                    req.single = true;
                }
                if (this.ric.idStaff) {
                    req.idStaff = this.ric.idStaff;
                    req.nomeCommessa = this.ric.nomeCommessa;
                } else {
                    var tempVal = _.find(this.allCommesse, {
                        'codCommessa': this.dataToSave.idCommessaDraft
                    });
                    if (tempVal && tempVal.codiceStaffCommessa) {
                        req.idStaff = tempVal.codiceStaffCommessa;
                    } else {
                        req.idStaff = "CSC0000031";
                        req.nomeCommessa = "ferie";
                    }
                }
                if (!_.find(this.pmSchedulingModel.getProperty("/commesse"), {
                        codCommessa: this.dataToSave.idCommessaDraft
                    })) {
                    var staff = _.find(_.find(this.commessePerConsulente, {
                        codConsulente: this.dataToSave.codConsulenteDraft
                    }).commesse, {
                        codCommessa: this.dataToSave.idCommessaDraft
                    }).codStaff;
                    model.ProfiloCommessaPeriodo.newRecord(this.dataToSave.idCommessaDraft, this.periodo, staff, "CHIUSO", "0", "0")
                        .then(_.bind(function (res) {
                            this.commessaDaRisettare = this.codCommessaSelezionata;
                            //                            this.readProjects(this.periodo);
                            this.readProjectsByPeriod(this.periodo);
                            this.leggiTabella(req);
                        }, this), _.bind(function (err) {
                            this.leggiTabella(req);
                            this.commessaDaRisettare = this.codCommessaSelezionata;
                            this.readProjectsByPeriod(this.periodo);
                        }, this));
                } else {
                    this.leggiTabella(req);
                }
            }
            this.fromSaveLocal = true;
            this.undoMove = false;
            this.moveHere = undefined;
            if (model.persistence.Storage.local.get("move")) {
                model.persistence.Storage.local.remove("move");
                this.leggiTabella(this.ric);
            }
        };
        var fError = function (err) {
            sap.m.MessageToast.show(JSON.parse(err.responseText));
            this.leggiTabella(this.ric);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.requestSaveToDraft.sendRequest(req)
            .then(fSuccess, fError);
    },

    reInsertRequest: function (req) {
        this.dataToSave = req;

        var fSuccess = function () {
            this.saveRequest(this.dataToSave);
            this.objToAssign = undefined;
            this.assignHere = undefined;
        };

        var fError = function (err) {
            sap.m.MessageToast.show("ERRORE");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.requestSaveToDraft.delRequest(req.id_req, "CONFIRMED")
            .then(fSuccess, fError);
    },

    onRequestPress: function (evt) {
        var requestList = sap.ui.getCore().getElementById("idRequestsList");
        if (requestList.getMode() === "MultiSelect") {
            var mezzeGiornate = [];
            var giornateIntere = [];
            var requestChoice = evt.getParameters().listItem;
            var icon = requestChoice.mProperties.icon;
            var req = requestChoice.mProperties.title;
            var selected = requestChoice.mProperties.selected;
            if (selected) {
                var requestsSelected = requestList.getSelectedItems();
                for (var i = 0; i < requestsSelected.length; i++) {
                    var iconaCiclica = requestsSelected[i].getIcon();
                    if (iconaCiclica === "sap-icon://time-entry-request" || iconaCiclica === "sap-icon://bed") {
                        mezzeGiornate.push(requestsSelected[i]);
                    } else {
                        giornateIntere.push(requestsSelected[i]);
                    }
                }
                if (mezzeGiornate.length > 2 || giornateIntere.length > 1 || (mezzeGiornate.length > 0 && giornateIntere.length > 0)) {
                    sap.m.MessageBox.alert(this._getLocaleText("TROPPE_RICHIESTE"), {
                        title: this._getLocaleText("ATTENTION")
                    });
                    var idRequestChoiche = requestChoice.sId;
                    requestList.setSelectedItemById(idRequestChoiche, false);
                }
            }
        }
    },

    handleButtonPress: function (evt) {
        var del = false;
        var b = this.oButton;
        var idButton = b.getId();
        var richiestaPremuta = evt.getSource().mProperties;
        var selectedRequest = richiestaPremuta.text;
        var selectedIcon = richiestaPremuta.icon;
        var row = parseInt(idButton.split("row")[1]);
        var column = parseInt(idButton.split("col")[1]);
        var idRequests = [];
        var schedulazioneConsulente = this.pmSchedulingModel.getData().schedulazioni[row];
        for (var o in schedulazioneConsulente) {
            if (schedulazioneConsulente[o] !== selectedRequest && o.indexOf("gg" + column) >= 0 && o.indexOf("Conflict") >= 0 && o.indexOf("Icon") < 0) {
                var prop = o.replace("gg", "req");
                prop = o.replace("Conflict", "");
                var idRequest = schedulazioneConsulente[prop];
                if (selectedIcon !== "sap-icon://time-entry-request" && selectedIcon !== "sap-icon://bed") {
                    idRequests.push(idRequest);
                    console.log(idRequests);
                } else {
                    var proprietaIcona = o.replace("gg", "ggIcon");
                    if (schedulazioneConsulente[proprietaIcona] !== "sap-icon://time-entry-request" && schedulazioneConsulente[proprietaIcona] !== "sap-icon://bed") {
                        idRequests.push(idRequest);
                        console.log(idRequests);
                    }
                }
            }
        }
    },
    // *********************pmActionSheet*******************

    onRequestDialogClose: function () {
        if (this._pmActionSheet) {
            this._pmActionSheet.close();
            this._pmActionSheet.destroy(true);
        }

        if (this.commesseDialog) {
            this.commesseDialog.close();
            this.commesseDialog.destroy(true);
        }
    },

    removeStyleClassButton: function (b) {
        b.removeStyleClass("attention");
        b.removeStyleClass("travel");
        b.removeStyleClass("halfDay");
        b.removeStyleClass("wCons");
        b.removeStyleClass("fullDay");
        b.removeStyleClass("wNonCons");
        b.removeStyleClass("wCust");
        b.removeStyleClass("wNonCust");
        b.removeStyleClass("notConfirmed");
        b.removeStyleClass("bed");
        b.removeStyleClass("globe");
    },

    writeRequestsPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_WRITE_REQUEST"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_WRITE_REQUEST_TITLE"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmWriteRequest, this)
        );
    },

    _confirmWriteRequest: function (evt) {
        if (evt === sap.m.MessageBox.Action.YES) {
            var fScritturaRichiesteError = function (err) {
                this.oDialogTable.setBusy(false);
                sap.m.MessageToast.show(this._getLocaleText("erroreScritturaRichieste"));
            };
            var fLetturaRichiesteError = function (err) {
                this.oDialogTable.setBusy(false);
                sap.m.MessageToast.show(this._getLocaleText("erroreLetturaRichieste"));
            };
            var fCancellazioneRichiesteError = function (err) {
                this.oDialogTable.setBusy(false);
                sap.m.MessageToast.show(this._getLocaleText("erroreCancellazioneRichieste"));
            };
            var fScritturaRichiesteSuccess = function (ris) {
                if (ris.status.indexOf("OK") >= 0) {
                    this.oDialogTable.setBusy(false);
                    var table = this.getView().byId("idSchedulazioniTableUM");
                    var rows = table.getRows();
                    for (var i = 0; i < rows.length; i++) {
                        var cells = rows[i].getCells();
                        for (var j = 0; j < cells.length; j++) {
                            this.removeStyleClassButton(cells[j]);
                        }
                    }
                    if (this.ric) {
                        this.leggiTabella(this.ric);
                    } else {
                        this.oDialogTable.setBusy(false);
                    }
                }
            };
            var fLetturaRichiesteSuccess = function (ris) {
                var results = JSON.parse(ris);
                this.oDialogTable.setBusy(false);
            };
            var fCancellazioneRichiesteSuccess = function (ris) {
                if (ris.indexOf("OK") >= 0) {
                    var idPeriodo = model.persistence.Storage.session.get("period");
                    model.tabellaSchedulingUM.readRequests(idPeriodo)
                        .then(fLetturaRichiesteSuccess, fLetturaRichiesteError);
                }
            };
            fScritturaRichiesteError = _.bind(fScritturaRichiesteError, this);
            fLetturaRichiesteError = _.bind(fLetturaRichiesteError, this);
            fCancellazioneRichiesteError = _.bind(fCancellazioneRichiesteError, this);
            fScritturaRichiesteSuccess = _.bind(fScritturaRichiesteSuccess, this);
            fLetturaRichiesteSuccess = _.bind(fLetturaRichiesteSuccess, this);
            fCancellazioneRichiesteSuccess = _.bind(fCancellazioneRichiesteSuccess, this);

            this.oDialogTable.setBusy(true);
            var idPeriodo = model.persistence.Storage.session.get("period");
            model.tabellaSchedulingUM.deleteRequestsFromDraft(idPeriodo).then(fCancellazioneRichiesteSuccess, fCancellazioneRichiesteError);
        } else {
            return;
        }
    },

    // **********************noteDialog********************
    onAddNote: function (evt) {
        if (!this._noteDialog) {
            this._noteDialog = sap.ui.xmlfragment("view.dialog.noteDialog", this);
            this.getView().addDependent(this._noteDialog);
        }
        var commessaTitle = "";
        var selectedCommessa = _.find(this.allCommesse, {
            'codCommessa': this.dataForUpdate.idCommessaDraft
        });
        this.codCommessaForAddNote = _.cloneDeep(selectedCommessa);
        if (selectedCommessa) {
            commessaTitle = selectedCommessa.nomeCommessa + " - " + selectedCommessa.descrizioneCommessa;
        }
        if (this.isNote) {
            this.noteModel.setProperty("/note", this.dataForUpdate.noteDraft)
            this.isNote = undefined;
        }
        this.noteModel.setProperty("/commessaTitle", commessaTitle);
        this.noteModel.setProperty("/editable", true);
        this.noteModel.refresh();
        this._noteDialog.open();
    },

    onAfterCloseNoteDialog: function (evt) {
        this.noteModel.setProperty("/note", undefined);
        this.pmSchedulingModel.refresh();
    },

    onCancelNoteDialogPress: function (evt) {
        if (this._noteDialog) {
            this._noteDialog.close();
            this.pmSchedulingModel.refresh();
        }
    },

    onSaveNoteDialogPress: function (evt) {
        var fSuccess = function () {
            if (this._noteDialog)
                this._noteDialog.close();
            sap.m.MessageToast.show(this._getLocaleText("NOTE_ADDED"));
            var cloneRic = _.cloneDeep(this.ric);
            if (this.richiestaConFiltri) {
                cloneRic = _.merge(cloneRic, this.richiestaConFiltri);
            }
            cloneRic.idStaff = this.codCommessaForAddNote.codiceStaffCommessa ? this.codCommessaForAddNote.codiceStaffCommessa : "";
            cloneRic.selectedConsultantsCode = this.dataForUpdate.codConsulenteDraft ? this.dataForUpdate.codConsulenteDraft : "";
            this.dataForUpdate = "";
            this.codCommessaForAddNote = "";
            this.leggiTabella(cloneRic);
        };
        var fError = function (err) {
            sap.m.MessageToast.show(this._getLocaleText("ERROR_NOTE_UPDATE"));
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        this.dataForUpdate.noteDraft = this.noteModel.getData().note;
        model.tabellaSchedulingUM.updateNote(this.dataForUpdate).then(fSuccess, fError);
    },

    uniqueElement: function (result) {
        var arrayfinale = result[0];
        var arrayGiornateSchedulate = result[0];

        var objCodiciConsulenti = {};
        for (var i = 0; i < arrayGiornateSchedulate.length; i++) {
            var c = "c" + i;
            objCodiciConsulenti[c] = arrayGiornateSchedulate[i].cod_consulente;
        }
        var objCodiciConsulenti2 = {};
        for (var i = 0; i < arrayGiornateSchedulate.length; i++) {
            var c = "d" + i;
            objCodiciConsulenti2[c] = arrayGiornateSchedulate[i].codiceConsulente;
        }

        _.merge(objCodiciConsulenti, objCodiciConsulenti2);

        var arrayfinale = [];
        for (var prop in objCodiciConsulenti) {
            var arr = _.where(arrayGiornateSchedulate, {
                codiceConsulente: objCodiciConsulenti[prop]
            });
            var obj = {};
            for (var i = 0; i < arr.length; i++) {
                obj.nomeConsulente = arr[i].nomeConsulente;
                obj.codiceConsulente = arr[i].codiceConsulente;
                obj.codicePJM = arr[i].codicePJM;
                obj.nomeSkill = arr[i].nomeSkill;
                obj.cod_consulenteBu = arr[i].cod_consulenteBu;
                var objProps = _.keys(arr[i]);
                var ggProp = _.find(objProps, function (item) {
                    return (item.indexOf("gg") > -1 && item.indexOf("Icon") < 0 && item.indexOf("Conflict") < 0 && item.indexOf("ggReq") < 0)
                });
                if (ggProp) {
                    var gg = ggProp.substring(2); //id del giorno nel periodo
                    // ggVal è il nome della proprietà "gg"+ la serie di "a" dipendente dalla quantità di attività in quello stesso giorno
                    var ggVal = ggProp;
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (obj[ggVal]) {
                        items = "a";
                        ggVal = ggVal + items;
                    }

                    obj[ggVal] = arr[i][ggProp];
                    obj["idReqOld" + ggVal.substring(2)] = arr[i]["idReqOld" + gg];
                    obj["nota" + ggVal.substring(2)] = arr[i]["nota" + gg];
                    obj["req" + ggVal.substring(2)] = arr[i]["req" + gg];
                    obj["ggReq" + ggVal.substring(2)] = arr[i]["ggReq" + gg];
                    obj["ggStato" + ggVal.substring(2)] = arr[i]["ggStato" + gg];
                    obj["ggIcon" + ggVal.substring(2)] = arr[i]["ggIcon" + gg];
                    obj["ggConflict" + ggVal.substring(2)] = arr[i]["ggConflict" + gg];
                    if (ggVal.indexOf("a") > 0) {
                        var propIcon = "ggIcon" + ggVal.substring(2);
                        var propIconOld = propIcon.substr(propIcon, propIcon.length - 1);
                        if (obj[propIconOld] === "sap-icon://offsite-work") {
                            var propOld = ggVal.substr(ggVal, ggVal.length - 1);
                            var propNota = "nota" + ggVal.substring(2);
                            var propNotaOld = propNota.substr(propNota, propNota.length - 1);
                            var propReq = "req" + ggVal.substring(2);
                            var propReqOld = propReq.substr(propReq, propReq.length - 1);
                            var propGgReq = "ggReq" + ggVal.substring(2);
                            var propGgReqOld = propGgReq.substr(propGgReq, propGgReq.length - 1);
                            var propStato = "ggStato" + ggVal.substring(2);
                            var propStatoOld = propStato.substr(propStato, propStato.length - 1);
                            var propConflict = "ggConflict" + ggVal.substring(2);
                            var propConflictOld = propConflict.substr(propConflict, propConflict.length - 1);

                            obj[ggVal] = obj[propOld];
                            obj["nota" + ggVal.substring(2)] = obj[propNotaOld];
                            obj["req" + ggVal.substring(2)] = obj[propReqOld];
                            obj["ggReq" + ggVal.substring(2)] = obj[propGgReqOld];
                            obj["ggStato" + ggVal.substring(2)] = obj[propStatoOld];
                            obj["ggIcon" + ggVal.substring(2)] = obj[propIconOld];
                            obj["ggConflict" + ggVal.substring(2)] = obj[propConflictOld];

                            obj[propOld] = arr[i]["gg" + gg];
                            obj[propNotaOld] = arr[i]["nota" + gg];
                            obj[propReqOld] = arr[i]["req" + gg];
                            obj[propGgReqOld] = arr[i]["ggReq" + gg];
                            obj[propStatoOld] = arr[i]["ggStato" + gg];
                            obj[propIconOld] = arr[i]["ggIcon" + gg];
                            obj[propConflictOld] = arr[i]["ggConflict" + gg];
                        }

                    }
                }
            }
            if (!_.isEmpty(obj))
                arrayfinale.push(obj);
        }
        arrayfinale = _.reduce(arrayfinale, function (results, item) {
            return _.any(results, function (result) {
                return _.isEqual(result, item);
            }) ? results : results.concat([item]);
        }, []);

        var arrCodConsulenti = [];
        for (var aa = 0; aa < arrayfinale.length; aa++) {
            for (var prop in arrayfinale[aa]) {
                if (prop.indexOf("codiceConsulente") > -1) {
                    arrCodConsulenti.push((arrayfinale[aa])[prop]);
                }

            }
        }
        var defer = Q.defer();
        var fSuccess = function (result) {
            console.log(result.items);
            if (result.items.length === 0) {
                defer.resolve(arrayfinale);
            } else {
                for (var ii = 0; ii < arrayfinale.length; ii++) {
                    for (var jj = 0; jj < result.items.length; jj++) {
                        if (arrayfinale[ii].codiceConsulente === (result.items)[jj].codConsulente) {
                            if (!!arrayfinale[ii].nomeSkill) {
                                if (arrayfinale[ii].nomeSkill.trim().length > 0) {
                                    arrayfinale[ii].nomeSkill += ", " + (result.items)[jj].nomeSkill;
                                } else {
                                    arrayfinale[ii].nomeSkill = (result.items)[jj].nomeSkill;
                                }
                            } else {
                                arrayfinale[ii].nomeSkill = (result.items)[jj].nomeSkill;
                            }

                        }
                    }
                }
                defer.resolve(arrayfinale);
            }
        };
        var fError = function (err) {
            defer.reject(err);
            sap.m.MessageToast("error skills");
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.Skill.getConsulenteSkillSerializedInBatch(arrCodConsulenti)
            .then(fSuccess, fError);
        return defer.promise;
    },

    leggiTabella: function (req) {
        var defer = Q.defer();
        this.oDialogTable.setBusy(true);
        // if (this.richiestaConFiltri) {
        //     req = _.merge(req, this.richiestaConFiltri);
        // }
        var fSuccess = function (res) {
            defer.resolve();
            if (res && res.resultOrdinato) {
                var schedulazioni;
                if (this.ric.idStaff) {
                    // faccio un if per verificare che se ho fatto un semplice inserimento,
                    // vado a cambiare solo la riga del consulente che mi sta tornando dalla chiamata
                    if (req.selectedConsultantsCode) {
                        schedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni");
                        var indiceArray = _.findIndex(schedulazioni, {
                            codiceConsulente: res.resultOrdinato[0].codiceConsulente
                        });
                        schedulazioni.splice(indiceArray, 1, res.resultOrdinato[0]);
                        //per filtro
                        this.allSchedulingForFilter.splice(indiceArray, 1, res.resultOrdinato[0]);
                    } else {
                        schedulazioni = res.resultOrdinato;
                        this.allSchedulingForFilter = _.cloneDeep(schedulazioni);
                    }
                    for (var t = 0; t < schedulazioni.length; t++) {
                        var commessaConsulente = _.find(_.find(this.commessePerConsulente, {
                            codConsulente: schedulazioni[t].codiceConsulente
                        }).commesse, {
                            codCommessa: this.codCommessaSelezionata
                        });

                        schedulazioni[t].teamLeader = commessaConsulente ? commessaConsulente.teamLeader : "NO";
                        schedulazioni[t].nomeSkill = commessaConsulente ? commessaConsulente.nomeSkill : "";
                        //per filtro
                        this.allSchedulingForFilter[t].teamLeader = commessaConsulente ? commessaConsulente.teamLeader : "NO";
                        this.allSchedulingForFilter[t].nomeSkill = commessaConsulente ? commessaConsulente.nomeSkill : "";
                    }
                    var listaConsulentiGenerici = _.filter(schedulazioni, function (item) {
                        if (item.codiceConsulente.indexOf("CCG") >= 0)
                            return item
                    });
                    if (_.size(listaConsulentiGenerici) > 0) {
                        this.visibleModel.setProperty("/visibleAssignConsultant", true);
                    } else {
                        this.visibleModel.setProperty("/visibleAssignConsultant", false);
                    }
                } else {
                    // faccio un if per verificare che se ho fatto un semplice inserimento,
                    // vado a cambiare solo la riga del consulente che mi sta tornando dalla chiamata
                    this.visibleModel.setProperty("/visibleAssignConsultant", false);
                    if (req.selectedConsultantsCode) {
                        schedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni");
                        //riaggiungo le skill che si erano perse.
                        if (res && res.resultOrdinato[0] && res.resultOrdinato[0].codiceConsulente) {
                            var rimosso = _.find(schedulazioni, {
                                'codiceConsulente': res.resultOrdinato[0].codiceConsulente
                            });
                            res.resultOrdinato[0].nomeSkill = rimosso.nomeSkill

                            var indiceArray = _.findIndex(schedulazioni, {
                                codiceConsulente: res.resultOrdinato[0].codiceConsulente
                            });
                            schedulazioni.splice(indiceArray, 1, res.resultOrdinato[0]);

                            //**parte per filtro
                            var rimossoFilter = _.find(this.allSchedulingForFilter, {
                                'codiceConsulente': res.resultOrdinato[0].codiceConsulente
                            });

                            this.allSchedulingForFilter.splice(indiceArray, 1, res.resultOrdinato[0]);
                        }
                    } else {
                        this._checkRichiesteModifica();
                        this._checkProjectsGeneric(res.resultOrdinato);
                        schedulazioni = res.resultOrdinato;
                        this.allSchedulingForFilter = _.cloneDeep(res.resultOrdinato);
                        if (this.richiestaConFiltri && this.richiestaConFiltri.consulentiGenerici && this.richiestaConFiltri.consulentiGenerici === "X") {
                            schedulazioni = _.sortByOrder(schedulazioni, 'nomeSkill');
                        }
                    }
                }
                var result = this.splitRequestIntoModel(schedulazioni);
                var uniEle = result.uniEle;
                this.allRequests = result.allRequests;
                var schedOrdinato = uniEle;
                this.allScheduling = uniEle;
                if (!this.richiestaConFiltri) {
                    this.listAllConsultant = this.allRequests;
                }

                this.umSchedulingModel.setProperty("/schedulazioni", []);
                this.pmSchedulingModel.setProperty("/schedulazioni", []);

                this.umSchedulingModel.setProperty("/schedulazioni", this.allRequests);
                this.pmSchedulingModel.setProperty("/schedulazioni", schedOrdinato);
                this.allSchedulingForFilter = schedOrdinato;
                this.umSchedulingModel.refresh();
                this.pmSchedulingModel.refresh();
                if (schedOrdinato.length > 0 && schedOrdinato.length <= 6) {
                    this.pmSchedulingModel.setProperty("/nRighe", schedOrdinato.length);
                } else if (schedOrdinato.length > 6) {
                    this.pmSchedulingModel.setProperty("/nRighe", 6);
                } else {
                    this.pmSchedulingModel.setProperty("/nRighe", 1);
                }
                //                this._calcoloGiornateTotali();
                this.buttonRefresh.setEnabled(true);

                setTimeout(_.bind(function () {
                    this._onAfterRenderingTable();
                }, this));
            } else {
                this._enterErrorFunction("error", "Errore", "Problemi nel caricamento dei dati");
            }

        };

        var fError = function (err) {

            this._enterErrorFunction("error", "Errore", "Errore lettura tabella: " + err);
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if (!req.idStaff && !req.selectedConsultantsCode) {
            // legge tutti i consulenti di tutte le commesse
            model.tabellaSchedulingUM.readUmAllRequests(req)
                .then(fSuccess, fError);
        } else {
            // legge tutti i consulenti di una singola commessa
            model.tabellaSchedulingUM.readUmProjectRequests(req)
                .then(fSuccess, fError);
        }
        return defer.promise;
    },

    splitRequestIntoModel: function (res) {
        // res saranno le righe di schedulazione per ogni consulente
        var allRequests = _.cloneDeep(res);
        var deletedRequests = [];
        var uniEle = _.forEach(res, function (n) {
            if (n.nomeConsulente === "Della Ragione Nunzio") {
                console.log(n);
            }
            for (var i in n) {
                if (n[i] === "DELETED") {
                    var propRequestName = i.replace("Stato", "");
                    var propConflictName = i.replace("Stato", "Conflict");
                    var propIconName = i.replace("Stato", "Icon");
                    var propReqDate = i.replace("Stato", "Req");
                    var propReqOld = i.replace("Stato", "ReqOld");
                    propReqOld = propReqOld.replace("gg", "id");
                    var propReq = i.replace("ggStato", "req");
                    var propNota = i.replace("ggStato", "nota");
                    var idReqOriginal = i.replace("ggStato", "idReqOriginal");

                    // verifico se quello in questione è effettivamente una richiesta cancellata o se è già stata ri-assegnata
                    var commessaAssegnata = false;
                    for (var o in n) {
                        if (n[propReq] === n[o] && o !== propReq) {
                            commessaAssegnata = true;
                            break;
                        }
                    }
                    if (!commessaAssegnata) {
                        var objDeleted = {
                            'nomeConsulente': n.nomeConsulente,
                            'cod_consulenteBu': n.cod_consulenteBu,
                            'codiceConsulente': n.codiceConsulente,
                            'codicePJM': n.codicePJM
                        };
                        objDeleted.reqName = n[propRequestName];
                        objDeleted.reqType = n[propIconName];
                        objDeleted.reqDate = n[propReqDate];
                        objDeleted.reqOld = n[propReqOld];
                        objDeleted.idReq = n[propReq];
                        objDeleted.note = n[propNota];
                        objDeleted.idReqOriginal = n[idReqOriginal];
                        var reqTypeDescription = "";
                        switch (n[propIconName]) {
                            case "sap-icon://complete":
                                reqTypeDescription = this._getLocaleText("ASSIGN");
                                break;
                            case "sap-icon://time-entry-request":
                                reqTypeDescription = this._getLocaleText("ASSIGN_HALF_DAY");
                                break;
                            case "sap-icon://customer-and-contacts":
                                reqTypeDescription = this._getLocaleText("C_WHIT_CONSULTANT");
                                break;
                            case "sap-icon://collaborate":
                                reqTypeDescription = this._getLocaleText("C_WHIT_CUSTOMER");
                                break;
                            case "sap-icon://employee-rejections":
                                reqTypeDescription = this._getLocaleText("NON_C_WHIT_CONSULTANT");
                                break;
                            case "sap-icon://offsite-work":
                                reqTypeDescription = this._getLocaleText("NON_C_WHIT_CUSTOMER");
                                break;
                            case "sap-icon://role":
                                reqTypeDescription = this._getLocaleText("NON_CONFIRMED_CUSTOMER");
                                break;
                            case "sap-icon://globe":
                                reqTypeDescription = this._getLocaleText("ABROAD");
                                break;
                            case "sap-icon://travel-itinerary":
                                reqTypeDescription = this._getLocaleText("INTERAFERIE");
                                break;
                            case "sap-icon://bed":
                                reqTypeDescription = this._getLocaleText("MEZZAFERIE");
                                break;
                        }
                        objDeleted.reqTypeDescription = reqTypeDescription;
                        deletedRequests.push(objDeleted);
                        delete n[propRequestName];
                        delete n[propConflictName];
                        delete n[propIconName];
                        delete n[propReqDate];
                        delete n[propReqOld];
                        delete n[idReqOriginal];
                        delete n[propReq];
                        delete n[propNota];
                        delete n[i];
                    }
                }
            }
        }.bind(this));

        this.allDeletedRequests = deletedRequests;

        for (var g = 0; g < allRequests.length; g++) {
            var richiesteCancellate = _.filter(deletedRequests, {
                'codiceConsulente': allRequests[g].codiceConsulente
            }).length;
            uniEle[g].richiesteCancellate = richiesteCancellate;
            allRequests[g].richiesteCancellate = richiesteCancellate;
        }
        var result = {
            'allRequests': allRequests,
            'uniEle': uniEle
        };
        return result;
    },

    _onAfterRenderingTable: function () {
        if (!this.oDialogTable.getBusy()) {
            this.oDialogTable.setBusy(true);
        }
        this._renderTabella();
        this._calcoloGiornateTotali();
        if (model.persistence.Storage.session.get("commessa")) {
            this.checkChangeRequests();
        }

        setTimeout(_.bind(function () {
            this.oDialogTable.setBusy(false);
            if (this.umSchedulingModel.getData().schedulazioni.length === 0) {
                sap.m.MessageBox.information(this._getLocaleText("NESSUN_CONFLITTO_TROVATO"), {
                    title: this._getLocaleText("INFO"),
                    onClose: _.bind(this.reloadPress, this)
                });
            }
        }, this), 500);
        this._checkIsToWork();
    },

    // funzione esportata per riutilizzarla in altre parti del codice
    _renderTabella: function () {
        var table = this.getView().byId("idSchedulazioniTableUM");
        var rows = table.getRows();
        var arrayCounter = [];
        for (var i = 0; i < rows.length; i++) {
            var cellsPerRows = rows[i].getAggregation("cells");
            var g = 0;
            for (var j = 1; j < cellsPerRows.length - 1; j++) {
                var cella = cellsPerRows[j];
                if (_.find(this.weekendDays, _.bind(function (item) {
                        return (item == j);
                    }, this))) {
                    var column = $("#" + cella.getDomRef().id).parent().parent();
                    column.css('background-color', '#e5e5e5');
                }
            }
        }
    },

    _calcoloGiornateTotali: function () {
        var totalCount = 0;
        for (var i = 0; i < this.pmSchedulingModel.getProperty("/schedulazioni").length; i++) {
            totalCount = totalCount + this.pmSchedulingModel.getProperty("/schedulazioni")[i].giorniScheduling;
        }
        this.pmSchedulingModel.setProperty("/giorniTotali", totalCount);
    },

    // funzione che 'blocca' il consulente: non permette più alcuna modifica e non viene più sovrascritto
    pressLocked: function (evt) {
        if (!this.visibleModel.getProperty("/wipPeriod")) {
            return;
        }
        var oEvent = evt.getSource();
        var statoToWrite = oEvent.getSrc() === "sap-icon://unlocked" ? "CHIUSO" : "APERTO";
        var rowButton = parseInt(oEvent.getId().split("row")[1]);
        var row = oEvent.getBindingContext("pmSchedulingModel").getPath().split("/")[oEvent.getBindingContext("pmSchedulingModel").getPath().split("/").length - 1];
        //        var schedulazioneConsulente = this.pmSchedulingModel.getData().schedulazioni[row];

        // il cambio di stato del consulente non è possibile nel caso in cui ci siano ancora dei conflitti presenti nella sua schedulazione
        var errore = false;
        if (statoToWrite === "CHIUSO") {
            for (var i = 1; i <= 35; i++) {
                // costruisco ciclicamente l'id delle icone per vedere se c'è l'icona del conflitto; in quel caso non posso modificare lo stato
                var idIcon = "UmSchedulingId--button" + i + "-col" + i + "-row" + rowButton;
                var icon = this.getView().byId(idIcon);
                if (icon) {
                    var iconSrc = icon.getSrc();
                    if (iconSrc.indexOf("warning") >= 0) {

                        sap.m.MessageBox.show(this._getLocaleText("ATTENZIONE_CONFLITTI"), {
                            icon: sap.m.MessageBox.Icon.ALLERT,
                            title: this._getLocaleText("ATTENZIONE_CONFLITTI_TITLE"),
                            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                            onClose: _.bind(function (action) {
                                if (action === "YES") {
                                    this.continuaFunzioneDiBlocco(row, statoToWrite);
                                } else {
                                    return;
                                }
                            }, this),
                        });

                        errore = true;
                        break;
                    }
                }
            }
        }
        if (errore)
            return;

        this.continuaFunzioneDiBlocco(row, statoToWrite);
    },

    continuaFunzioneDiBlocco: function (row, statoToWrite) {
        var codConsulente = this.pmSchedulingModel.getData().schedulazioni[row].codiceConsulente;
        var fSuccess = function (res) {
            var risposta = JSON.parse(res).data;
            var consulente = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                codiceConsulente: risposta.codConsulente
            });
            consulente.isLocked = risposta.stato === "APERTO" ? false : true;
            sap.m.MessageToast.show(risposta.stato === "APERTO" ? this._getLocaleText("CAMBIO_STATO_SBLOCCATO") : this._getLocaleText("CAMBIO_STATO_BLOCCATO"));
            var consulenteUm = _.find(this.umSchedulingModel.getData().schedulazioni, {
                codiceConsulente: risposta.codConsulente
            });
            consulenteUm.isLocked = risposta.stato === "APERTO" ? false : true;

            this.pmSchedulingModel.refresh();
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione", "Problemi nel cambio stato 'GSPI_STATO_CONSULENTE_PERIODO_DRAFT'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.TabellaConsulentePeriodoDraft.changeStuts(codConsulente, this.periodo, statoToWrite, this.user.umBU)
            .then(fSuccess, fError);
    },

    onConsultantPress: function (evt) {
        var oEvent = evt.getSource();
        var row = oEvent.getBindingContext("pmSchedulingModel").getPath().split("/")[oEvent.getBindingContext("pmSchedulingModel").getPath().split("/").length - 1];
        var consultantToOpen = this.pmSchedulingModel.getData().schedulazioni[row];
        var url = location.pathname + "#/tableReportConsultant/" + consultantToOpen.codiceConsulente;
        oEvent.setHref(url);
        model.persistence.Storage.session.save(consultantToOpen.codiceConsulente, consultantToOpen);
    },

    onRichiesteCancellatePress: function (evt) {
        var linkPressed = evt.getSource();
        var path = linkPressed.getBindingContext("pmSchedulingModel").getPath().split("/")[linkPressed.getBindingContext("pmSchedulingModel").getPath().split("/").length - 1];
        var codiceConsulente = this.pmSchedulingModel.getProperty("/schedulazioni")[path].codiceConsulente;
        var nomeConsulente = this.pmSchedulingModel.getProperty("/schedulazioni")[path].nomeConsulente;
        var deletedRequests = _.filter(this.allDeletedRequests, {
            'codiceConsulente': codiceConsulente
        });
        var deletedRequestsOrderData = _.sortByOrder(deletedRequests, [function (item) {
            return new Date(item.reqDate)
        }, 'reqName']);
        this.deletedRequestsModel.setProperty("/deletedRequests", deletedRequestsOrderData);
        this.deletedRequestsModel.setProperty("/nomeConsulente", nomeConsulente);
        if (!this.dialogRichiesteCancellate) {
            this.dialogRichiesteCancellate = sap.ui.xmlfragment("view.dialog.richiesteCancellate", this);
            this.getView().addDependent(this.dialogRichiesteCancellate);
        }
        this.dialogRichiesteCancellate.open();
    },

    onAssignPress: function (evt) {
        var idButtonPressed = evt.getParameters().id;
        var path = parseInt(idButtonPressed.split("-")[2]);
        var objToAssign = this.deletedRequestsModel.getProperty("/deletedRequests")[path];

        this.objToAssign = {};
        this.objToAssign.codConsulenteDraft = objToAssign.codiceConsulente;
        this.objToAssign.dataReqDraft = objToAssign.reqDate;
        this.objToAssign.codAttivitaRichiestaDraft = objToAssign.reqType.split("//")[1];
        this.objToAssign.idPeriodoSchedulingDraft = this.periodo;
        this.objToAssign.codPjmDraft = objToAssign.codicePJM;
        this.objToAssign.dataInserimentoDraft = (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T"));
        this.objToAssign.noteDraft = objToAssign.note;
        this.objToAssign.giornoPeriodoDraft = 0;
        this.objToAssign.statoReqDraft = "CONFIRMED";
        this.objToAssign.id_req = objToAssign.idReq;
        this.objToAssign.idReqOriginal = objToAssign.idReqOriginal;

        var codCommessa = _.find(this.pmSchedulingModel.getProperty("/commesse"), {
            'nomeCommessa': objToAssign.reqName
        }).codCommessa;
        this.objToAssign.idCommessaDraft = codCommessa;
        this.onCloseDeletedRequestDialog();
    },

    onCloseDeletedRequestDialog: function () {
        this.dialogRichiesteCancellate.close();
    },

    onLaunchpadPress: function () {
        if (this.richiestaConFiltri) {
            this.onFilterUMDialogRipristino(true);
        }
        if (this.oDialog.getBusy()) {
            this.oDialog.setBusy(false);
        }
        if (this.oDialogTable.getBusy()) {
            this.oDialogTable.setBusy(false);
        }
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.onNavBackPress();
    },

    onAfterRendering: function () {
        this.pmSchedulingModel.refresh();
    },

    svuotaTabella: function () {
        // svuoto la tabella
        this.getView().byId("idSchedulazioniTableUM").setVisible(true);
        this.pmSchedulingModel.setProperty("/schedulazioni", []);
        this.pmSchedulingModel.setProperty("/nRighe", 1);
        this.pmSchedulingModel.getData().schedulazioni = [];
    },

    svuotaSelect: function (value) {
        if (!value) {
            this.getView().byId("idSelectCommessa").setEnabled(false);
            this.getView().byId("idSelectCommessa").setSelectedKey("");
            this.getView().byId("idSelectCommessa").setValue("");
        }
    },

    refreshTable: function () {
        this.leggiTabella(this.ric);
    },

    onFilterUMDialogRipristino: function (goToLauch) {
        this.filteredConsultants = undefined;
        this.richiestaConFiltri = undefined;
        this.pmSchedulingModel.setProperty("/schedulazioni", this.allScheduling);
        this._resetFilterSelectedItems();
        var lunghezzaSchedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni").length;
        sap.ui.getCore().getElementById("idListaConsulentiUM").removeSelections();
        if (goToLauch !== true) {
            this.leggiTabella(this.ric);
            this.umDialog.close();
        }
    },

    onFilterUMDialogClose: function () {
        this.umDialog.close();
    },

    onFilterSelectAllItems: function (evt) {
        var src = evt.getSource();
        var tab = this.umFilterModel.getProperty("/selectedTab");
        var text = src.getText();
        var op = !this.umFilterModel.getProperty("/allSelected");
        this._setPropertyItems(tab, op);
        this.umFilterModel.setProperty("/allSelected", this._checkAllSelectedFilterItems(tab));
        this.umFilterModel.refresh();
    },

    onFilterUMSearch: function (evt) {
        var src = evt.getSource();
        this.searchValue = src.getValue();
        var searchProperty = this.uiModel.getProperty("/searchProperty");
        this.applyFilterUM(this.searchValue, searchProperty);
    },

    applyFilterUM: function (value, params) {
        var list = sap.ui.getCore().getElementById("idListaConsulentiUM");
        if (!list.getBinding("items").oList || list.getBinding("items").oList.length === 0)
            return;

        var temp = list.getBinding("items").oList[0]; // a template just to recover the data types
        var filtersArr = [];

        if (!_.isEmpty(params)) {
            if (!_.isArray(params)) {
                params = [params];
            }
            for (var i = 0; i < params.length; i++) {
                switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
                    case "undefined":
                        break;
                    case "string":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
                        break;
                    case "number":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
                        break;
                }
            }
            var filter = new sap.ui.model.Filter({
                filters: filtersArr,
                and: false
            });
            list.getBinding("items").filter(filter);
            return;
        }
        list.getBinding("items").filter();
    },

    onFilterStatoPress: function () {
        var page = this.getView().byId("schedulingPage");
        if (!this.umFilterLockedDialog) {
            this.umFilterLockedDialog = sap.ui.xmlfragment("view.dialog.filterDialogLockedUM", this);
            page.addDependent(this.umFilterLockedDialog);
        }

        var stato = [{
            "value": "TUTTI"
        }, {
            "value": "BLOCCATI"
        }, {
            "value": "ATTIVI"
        }];
        this.filterLockedModel.setData(stato);
        this.umFilterLockedDialog.open();
        var lis = sap.ui.getCore().getElementById("idListaLockedUM");
        if (this.umSchedulingModel.getProperty("/filtroLocked") === "0") {
            _.find(lis.getItems(), function (n) {
                if (n.getTitle() === "TUTTI")
                    lis.setSelectedItem(n);
            });
        } else if (this.umSchedulingModel.getProperty("/filtroLocked") === "1") {
            _.find(lis.getItems(), function (n) {
                if (n.getTitle() === "BLOCCATI")
                    lis.setSelectedItem(n);
            });
        } else {
            _.find(lis.getItems(), function (n) {
                if (n.getTitle() === "ATTIVI")
                    lis.setSelectedItem(n);
            });
        }
    },

    onSelectionChangeLocked: function (evt) {
        var oList = evt.getSource();
        this.listLocked = oList.getSelectedContexts(true);
    },

    onCloseLockedFilter: function () {
        this.umFilterLockedDialog.close();
        this.umFilterLockedDialog.destroy(true);
        this.umFilterLockedDialog = undefined;
    },

    onOkLockedFilter: function () {
        var path = parseInt(this.listLocked[0].getPath().split("/")[1]);
        var value = this.listLocked[0].getModel().getData()[path].value;
        if (value === "TUTTI") {
            this.pmSchedulingModel.setProperty("/schedulazioni", this.allScheduling);
            this.umSchedulingModel.setProperty("/filtroLocked", "0");
            if (this.richiestaConFiltri) {
                this.leggiTabella(this.richiestaConFiltri);
            } else {
                this.leggiTabella(this.ric);
            }
        } else if (value === "BLOCCATI") {
            var listaBloccati = [];
            if (this.richiestaConFiltri) {
                listaBloccati = _.where(this.pmSchedulingModel.getProperty("/schedulazioni"), {
                    isLocked: true
                });
            } else {
                listaBloccati = _.where(this.allScheduling, {
                    isLocked: true
                });
            }
            this.pmSchedulingModel.setProperty("/schedulazioni", listaBloccati);
            this.umSchedulingModel.setProperty("/schedulazioni", listaBloccati);
            this.umSchedulingModel.setProperty("/filtroLocked", "1");
        } else {
            var listaAttivi = [];
            if (this.richiestaConFiltri) {
                listaAttivi = _.where(this.pmSchedulingModel.getProperty("/schedulazioni"), {
                    isLocked: false
                });
            } else {
                listaAttivi = _.where(this.allScheduling, {
                    isLocked: false
                });
            }
            this.pmSchedulingModel.setProperty("/schedulazioni", listaAttivi);
            this.umSchedulingModel.setProperty("/schedulazioni", listaAttivi);
            this.umSchedulingModel.setProperty("/filtroLocked", "2");
        }
        if (this.pmSchedulingModel.getProperty("/schedulazioni").length > 0 && this.pmSchedulingModel.getProperty("/schedulazioni").length <= 6) {
            this.pmSchedulingModel.setProperty("/nRighe", this.pmSchedulingModel.getProperty("/schedulazioni").length);
        } else if (this.pmSchedulingModel.getProperty("/schedulazioni").length > 6) {
            this.pmSchedulingModel.setProperty("/nRighe", 6);
        } else {
            this.pmSchedulingModel.setProperty("/nRighe", 1);
        }

        this.onCloseLockedFilter();
        setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));
    },

    onNotePress: function (evt) {
        if (!this.visibleModel.getProperty("/wipPeriod")) {
            return;
        }
        var nota = evt.getSource().data("note");
        var jobName = evt.getSource().data("job");
        var job = _.find(this.getView().getModel("pmSchedulingModel").getProperty("/commesse"), {
            nomeCommessa: jobName
        });
        if (!job && jobName === "feri") {
            var commessaFeri = {};
            commessaFeri.codBu = "BU000";
            commessaFeri.codCommessa = "AA00000008";
            commessaFeri.codPjmAssociato = "";
            commessaFeri.nomeCommessa = "feri";
            commessaFeri.stato = "APERTO";
            job = commessaFeri;
        }

        var consultant = evt.getSource().getBindingContext("pmSchedulingModel").getObject();
        this.noteModel.setProperty("/note", consultant[nota]);
        var codReq = nota.substring(4);
        var propReq = "req" + codReq;

        this.dataForUpdate = {
            "codConsulenteDraft": consultant.codiceConsulente,
            "dataReqDraft": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "idCommessaDraft": job.codCommessa,
            "codAttivitaRichiestaDraft": "",
            "idPeriodoSchedulingDraft": this.periodo,
            "codPjmDraft": consultant.codicePJM,
            "dataInserimentoDraft": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "noteDraft": "",
            "giornoPeriodoDraft": codReq.indexOf("a") >= 0 ? parseInt(codReq.substr(0, codReq.length - 1)) : parseInt(codReq),
            "statoReqDraft": "CONFIRMED"
        };
        this.onAddNote();
        this.noteModel.setProperty("/editable", true);
    },

    onSelectionChange: function (evt) {
        var oList = evt.getSource();
        this.aContexts = oList.getSelectedContexts(true);
    },

    /******************/
    svuotaLocalStorageUndo: function () {
        var modelLocal = model.persistence.Storage.local;
        for (var i = 1; i < 6; i++) {
            var nameToSave = "undoUM_";
            nameToSave = nameToSave + i;
            if (modelLocal.get(nameToSave)) {
                modelLocal.remove(nameToSave);
            }
        }
    },

    // funzione che salva in localStorage le richieste
    /*
        request TYPE:
        * del -> salva la richiesta cancellata
        * insert -> salva la richiesta cancellata
        * move -> salva la richiesta da spostare
    */
    saveToLocalForUndo: function (requestType, dataToSave) {
        var modelLocal = model.persistence.Storage.local;
        var a = {};
        a[requestType] = dataToSave;
        for (var i = 1; i < 6; i++) {
            var nameToSave = "undoUM_";
            nameToSave = nameToSave + i;
            if (!modelLocal.get(nameToSave)) {
                modelLocal.save(nameToSave, a);
                return;
            } else if (i === 5) {
                modelLocal.save("undoUM_1", modelLocal.get("undoUM_2"));
                modelLocal.save("undoUM_2", modelLocal.get("undoUM_3"));
                modelLocal.save("undoUM_3", modelLocal.get("undoUM_4"));
                modelLocal.save("undoUM_4", modelLocal.get("undoUM_5"));
                modelLocal.save("undoUM_5", a);
            }
        }
    },

    // funzione lanciata alla pressione del tasto annulla
    undoPress: function () {
        var modelLocal = model.persistence.Storage.local;
        for (var i = 5; i > 0; i--) {
            var nameToSaveOR = "undoUM_";
            var nameToSave = nameToSaveOR + i;
            if (modelLocal.get(nameToSave)) {
                var dataToRestore = modelLocal.get(nameToSave);
                this.fromSaveLocal = false;
                if (modelLocal.get(nameToSave).del) {
                    //                    this.fromSaveLocal = false;
                    var oggetto = modelLocal.get(nameToSave).del;
                    if (oggetto.oldIdReqDraft === 1) {
                        oggetto.id_req = "";
                    }
                    oggetto.statoReqDraft = "CONFIRMED";
                    if (oggetto.id_req !== "") {
                        this.reInsertRequest(oggetto);
                    } else {
                        this.saveRequest(oggetto);
                    }
                } else if (modelLocal.get(nameToSave).move) {
                    this.undoMove = true;
                    var oggettoToMove = modelLocal.get(nameToSave).move;
                    oggettoToMove.oldGiorno = oggettoToMove.giornoPeriodoDraft;
                    oggettoToMove.giornoPeriodoDraft = oggettoToMove.newGiorno;
                    delete oggettoToMove.newGiorno;
                    model.persistence.Storage.local.save("move", oggettoToMove);
                    this._delData(oggettoToMove);
                } else {
                    this._delData(modelLocal.get(nameToSave).insert);
                    var nameToSaveInf = nameToSaveOR + (i - 1);
                }
                modelLocal.remove(nameToSave);
                jQuery.sap.delayedCall(500, this, function () {
                    this.fromSaveLocal = true;
                });
                if (i === 1) {
                    this.visibleModel.setProperty("/undoButton", false);
                }
                return;
            }
        }
    },
    /* FINE funzioni per la gestione dell' 'annulla' */

    /*****************/
    openMenu: function (evt) {
        if (!this.visibleModel.getProperty("/wipPeriod")) {
            return;
        }
        this.oButton = evt.getSource();
        this.idButton = this.oButton.getId();
        this.nButton = parseInt((this.idButton.split("-")[2]).substring(this.idButton.split("-")[2].indexOf("n") + 1));
        this.row = this.oButton.getBindingContext("pmSchedulingModel").getPath().split("/")[2];
        this.dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[this.row];
        if (!this.dataModelConsultant.isLocked) {
            var commesseQuestoConsulente = _.find(this.commessePerConsulente, {
                codConsulente: this.dataModelConsultant.codiceConsulente
            }).commesse;
            var commessaSelezionata = this.chosedCom;
            var cloneCommesseConsulente = _.clone(commesseQuestoConsulente);
            var commessaDaSpostare = _.remove(cloneCommesseConsulente, {
                'nomeCommessa': commessaSelezionata
            });
            this.commesseQuestoConsulente = [];
            if (commesseQuestoConsulente.length > 1)
                cloneCommesseConsulente = _.sortBy(cloneCommesseConsulente, function (n) {
                    return n.nomeCommessa.toUpperCase();
                });
            if (commessaDaSpostare.length !== 0) {
                cloneCommesseConsulente.unshift(commessaDaSpostare[0]);
            }
            this.commesseQuestoConsulente = _.clone(cloneCommesseConsulente);
            if (_.find(this.commesseQuestoConsulente, {
                    codCommessa: 'AA00000008'
                })) {
                _.remove(this.commesseQuestoConsulente, {
                    codCommessa: 'AA00000008'
                });
            }
            var commessaFeri = {};
            commessaFeri.codBu = "BU000";
            commessaFeri.codCommessa = "AA00000008";
            commessaFeri.codConsulente = this.commesseQuestoConsulente[0].codConsulente;
            commessaFeri.codPjmAssociato = "";
            commessaFeri.nomeCommessa = "feri";
            commessaFeri.stato = "APERTO";
            commessaFeri.codStaff = "CSC0000031";
            this.commesseQuestoConsulente.push(commessaFeri);

            this._openMenu();
            this.commesseOffsiteWork = [];
            var prop = "gg" + this.nButton;
            while (this.dataModelConsultant[prop]) {
                var propIcon = prop.replace("gg", "ggIcon");
                if (this.dataModelConsultant[propIcon] === "sap-icon://offsite-work") {
                    this.commesseOffsiteWork.push({
                        comm: this.dataModelConsultant[prop]
                    });
                }
                prop = prop + "a";
            }
        }
    },

    _openMenu: function () {
        if (!this._menu) {
            this._menu = sap.ui.xmlfragment("view.fragment.menuButtonsUM", this);
            this.getView().addDependent(this._menu);
        }
        var eDock = sap.ui.core.Popup.Dock;
        this._menu.setModel(this.visibleModel, "visibleModel");
        this._menu.open(undefined, this.oButton, eDock.BeginTop, eDock.BeginBottom, this.oButton);
        var icon = this.oButton.getSrc();

        this.visibleModel.setProperty("/assegnaProposta", false);
        this.visibleModel.setProperty("/removeAssegnaProposta", false);
        this.visibleModel.setProperty("/menuCommesseVisible", true);

        if (this.commessaDaProporre) {
            this.visibleModel.setProperty("/menuConflittiVisible", false);
            this.visibleModel.setProperty("/menuCommesseVisible", false);
            setTimeout(_.bind(this.creaMenuAssegnaProposta, this));
        } else {
            if (icon.indexOf("warning") >= 0) {
                setTimeout(_.bind(this.creaMenuConflitti, this));
                this.visibleModel.setProperty("/menuConflittiVisible", true);
            } else {
                setTimeout(_.bind(this.creaMenuScheduling, this));
                this.visibleModel.setProperty("/menuConflittiVisible", false);
            }
        }
    },

    creaMenuAssegnaProposta: function () {
        this.visibleModel.setProperty("/assegnaProposta", true);
        //        this.visibleModel.setProperty("/removeAssegnaProposta", false);
        var idButton = this.oButton.getId();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        if (parseInt(this.commessaDaProporre.giornoPeriodoDraft) === parseInt(nButton)) {
            this.visibleModel.setProperty("/assegnaProposta", false);
            this.visibleModel.setProperty("/removeAssegnaProposta", true);
        }
    },

    creaMenuScheduling: function (evt) {
        var prop = "gg" + this.nButton;
        var propBis = "gg" + this.nButton + "a";
        var schedulazioneGiornaliera = this.dataModelConsultant[prop];
        var schedulazioneGiornalieraBis = this.dataModelConsultant[propBis];
        var commessaSelezionata = this.chosedCom;

        //per le giornate non schedulabili nascondo il bottone cancella
        var propIcon = "ggIcon" + this.nButton;
        var noSchedulingIcon = this.dataModelConsultant[propIcon];
        // verifico se mostrare gli item CANCELLA, COPIA E NOTE
        if (schedulazioneGiornaliera && _.find(this.allCommesse, {
                nomeCommessa: schedulazioneGiornaliera
            })) {
            this.visibleModel.setProperty("/deleteVisible", true);
            this.visibleModel.setProperty("/copyVisible", true);
            this.visibleModel.setProperty("/noteVisible", true);
        } else {
            this.visibleModel.setProperty("/deleteVisible", false);
            this.visibleModel.setProperty("/copyVisible", false);
            this.visibleModel.setProperty("/noteVisible", false);
        }
        if (noSchedulingIcon === "sap-icon://offsite-work") {
            this.visibleModel.setProperty("/deleteVisible", false);
        }
        // verifico se mostrare il menuItem INCOLLA
        this.showPasteItem();
        // verifico se mostrare il menuItem SPOSTA QUI
        this.showMoveHereItem();
        // verifico se mostrare il menuItem ASSEGNA QUI
        this.showAssignHereItem();
        // carico le commesse sul menuItem COMMESSE
        this.mostraCommesse();
    },

    creaMenuConflitti: function () {
        this.visibleModel.setProperty("/deleteVisible", false);
        this.visibleModel.setProperty("/copyVisible", false);
        this.visibleModel.setProperty("/noteVisible", false);
        // verifico se mostrare il menuItem INCOLLA
        this.showPasteItem();
        // verifico se mostrare il menuItem SPOSTA QUI
        this.showMoveHereItem();
        // carico le commesse sul menuItem CONFLITTO
        this.mostraConflitto();
        // verifico se mostrare il menuItem ASSEGNA QUI
        this.showAssignHereItem();
        // carico le commesse sul menuItem COMMESSE
        this.mostraCommesse();
    },

    showPasteItem: function () {
        var schedulazioneInLocal = model.persistence.Storage.local.get("copy");
        if (schedulazioneInLocal) {
            var commessaInLocal = schedulazioneInLocal.commessa;
            var commessa = _.find(this.commesseQuestoConsulente, {
                'codCommessa': commessaInLocal
            });
            var nomeCommessa = "";
            if (commessa)
                nomeCommessa = commessa.nomeCommessa;
            var presenzaCommessa = this.verificaPresenzaCommessa(nomeCommessa);

            if (commessa && !presenzaCommessa) {
                this.visibleModel.setProperty("/pasteVisible", true);
            } else {
                this.visibleModel.setProperty("/pasteVisible", false);
            }
        } else {
            this.visibleModel.setProperty("/pasteVisible", false);
        }
    },

    showMoveHereItem: function () {
        var schedulazioneSpostataInLocal = model.persistence.Storage.local.get("move");
        if (schedulazioneSpostataInLocal) {
            var commessaInLocal = schedulazioneSpostataInLocal.idCommessaDraft;
            var commessa = _.find(this.commesseQuestoConsulente, {
                'codCommessa': commessaInLocal
            });
            var nomeCommessa = "";
            if (commessa)
                nomeCommessa = commessa.nomeCommessa;
        }
        var presenzaCommessa = this.verificaPresenzaCommessa(nomeCommessa);
        if (schedulazioneSpostataInLocal && schedulazioneSpostataInLocal.codConsulenteDraft === this.dataModelConsultant.codiceConsulente && !presenzaCommessa) {
            this.visibleModel.setProperty("/moveHereVisible", true);
        } else {
            this.visibleModel.setProperty("/moveHereVisible", false);
        }
    },

    showAssignHereItem: function () {
        if (this.objToAssign) {
            var commessa = _.find(this.commesseQuestoConsulente, {
                'codCommessa': this.objToAssign.idCommessaDraft
            });
            var nomeCommessa = "";
            if (commessa)
                nomeCommessa = commessa.nomeCommessa;
        }
        var presenzaCommessa = this.verificaPresenzaCommessa(nomeCommessa);
        if (this.objToAssign && this.objToAssign.codConsulenteDraft === this.dataModelConsultant.codiceConsulente && !presenzaCommessa) {
            this.visibleModel.setProperty("/assignHereVisible", true);
        } else {
            this.visibleModel.setProperty("/assignHereVisible", false);
        }
    },

    verificaPresenzaCommessa: function (nomeCommessa) {
        var prop = "gg" + this.nButton;
        var presenzaCommessa = false;
        while (this.dataModelConsultant[prop]) {
            if (this.dataModelConsultant[prop] === nomeCommessa) {
                presenzaCommessa = true;
                break;
            }
            prop = prop + "a";
        }
        return presenzaCommessa;
    },

    mostraCommesse: function () {
        var menuCommesse = sap.ui.getCore().getElementById("idMenuCommesseUM");
        menuCommesse.removeAllItems();

        var commesseDaMostrare = _.filter(this.commesseQuestoConsulente, {
            codBu: this.umBU
        });
        var commessaFerie = _.find(this.commesseQuestoConsulente, {
            codBu: 'BU000'
        });
        commesseDaMostrare.push(commessaFerie);
        for (var i = 0; i < commesseDaMostrare.length; i++) {
            var itemToAdd = new sap.ui.unified.MenuItem({
                text: commesseDaMostrare[i].nomeCommessa,
                select: _.bind(this.premiCommessa, this)
            });

            if (i === 1)
                itemToAdd.setStartsSection(true);

            // !! rimuovere commessa dove non schedulabile
            if (_.find(this.commesseOffsiteWork, {
                    comm: commesseDaMostrare[i].nomeCommessa
                })) {
                itemToAdd.setIcon("sap-icon://offsite-work");
                itemToAdd.setEnabled(false);
            }
            menuCommesse.addItem(itemToAdd);
        }
    },

    premiCommessa: function (evt) {
        var title = evt.getParameters().item.getProperty("text");
        var commessa = _.find(this.commesseQuestoConsulente, {
            'nomeCommessa': title
        });
        this.commessa = commessa.codCommessa;

        if (this.commessa !== "AA00000008") {
            if (!this._menuAttvita) {
                this._menuAttvita = sap.ui.xmlfragment("view.fragment.MenuRichieste", this);
                this.getView().addDependent(this._menuAttvita);
            }
            this._menuAttvita.getAggregation("items")[0].setText(title);
            var eDock = sap.ui.core.Popup.Dock;
            this._menuAttvita.open(undefined, this.oButton, eDock.BeginTop, eDock.BeginBottom, this.oButton);
        } else {
            if (!this._menuFerie) {
                this._menuFerie = sap.ui.xmlfragment("view.fragment.MenuFerie", this);
                this.getView().addDependent(this._menuFerie);
            }
            var eDock = sap.ui.core.Popup.Dock;
            this._menuFerie.open(undefined, this.oButton, eDock.BeginTop, eDock.BeginBottom, this.oButton);
        }
        setTimeout(_.bind(this._renderTabella, this));
    },

    mostraConflitto: function () {
        var commesseConflict = [];
        var propA = "";
        var propConflict = "ggConflict" + this.nButton;
        while (this.dataModelConsultant[propConflict]) {
            var propIcon = "ggIcon" + this.nButton + propA;
            var propId = "req" + this.nButton + propA;
            var prop = "gg" + this.nButton + propA;
            var propStato = "ggStato" + this.nButton + propA;
            var propData = "ggReq" + this.nButton + propA;
            var propNote = "nota" + this.nButton + propA;
            var propReq = "req" + this.nButton + propA;
            var propReqOld = "idReqOld" + this.nButton + propA;
            var idReqOriginal = "idReqOriginal" + this.nButton + propA;
            var overture = "propOverture" + this.nButton + propA;

            var commessaSel = _.find(this.commesseQuestoConsulente, {
                nomeCommessa: this.dataModelConsultant[prop]
            });
            if (commessaSel) {
                var objToAdd = {
                    "codConsulenteDraft": this.dataModelConsultant.codiceConsulente,
                    "dataReqDraft": this.dataModelConsultant[propData],
                    "idCommessaDraft": commessaSel.codCommessa,
                    "codAttivitaRichiestaDraft": this.dataModelConsultant[propIcon].split("//")[1],
                    "idPeriodoSchedulingDraft": this.periodo,
                    "codPjmDraft": commessaSel.codPjmAssociato,
                    "dataInserimentoDraft": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
                    "noteDraft": this.dataModelConsultant[propNote],
                    "giornoPeriodoDraft": this.nButton,
                    "statoReqDraft": this.dataModelConsultant[propStato],
                    "id_req": this.dataModelConsultant[propReq],
                    "oldIdReqDraft": this.dataModelConsultant[propReqOld],
                    "nomeCommessa": this.dataModelConsultant[prop],
                    "icon": this.dataModelConsultant[propIcon],
                    "id": this.dataModelConsultant[propId],
                    "stato": this.dataModelConsultant[propStato],
                    "idReqOriginal": this.dataModelConsultant[idReqOriginal],
                    "bu": commessaSel.codBu,
                    "overture": this.dataModelConsultant[overture]
                };
            } else {
                break;
            }
            commesseConflict.push(objToAdd);
            propA = propA + "a";
        }
        // se dentro all'array c'è la commessa selezionata nella combobox, allora la metto all'inizio del menu
        var commessaSelezionata = this.chosedCom;
        var cloneCommesseConflitto = _.clone(commesseConflict);
        var commessaDaSpostare = _.remove(cloneCommesseConflitto, {
            'nomeCommessa': commessaSelezionata
        });
        if (cloneCommesseConflitto.length > 1)
            cloneCommesseConflitto = _.sortBy(cloneCommesseConflitto, function (n) {
                return n.nomeCommessa.toUpperCase();
            });
        if (commessaDaSpostare.length !== 0)
            cloneCommesseConflitto.unshift(commessaDaSpostare[0]);

        this.commesseConflitto = _.clone(cloneCommesseConflitto);

        var menu = sap.ui.getCore().getElementById("idMenuConflittiUM");
        menu.removeAllItems();
        for (var i = 0; i < this.commesseConflitto.length; i++) {
            if (this.commesseConflitto[i].icon === "sap-icon://offsite-work") {
                continue;
            }
            var risolviConflitto = sap.ui.xmlfragment("view.fragment.MenuConflitto", this);
            var cancellaFerie = sap.ui.xmlfragment("view.fragment.MenuCancellaFerie", this);

            var itemToAdd = new sap.ui.unified.MenuItem({
                text: this.commesseConflitto[i].nomeCommessa,
                icon: this.commesseConflitto[i].icon
            });

            if (i === 1 && this.commesseConflitto[0].nomeCommessa === commessaSelezionata)
                itemToAdd.setStartsSection(true);
            if (this.commesseConflitto[i].bu === this.umBU) {
                if (this.commesseConflitto[i].overture !== 0) {
                    this.uiModel.setProperty("/visibleOverture", true);
                    this.commessaGiornoProposta = this.commesseConflitto[i];
                } else {
                    this.uiModel.setProperty("/visibleOverture", false);
                }

                var menuRisolviConflitto = new sap.ui.unified.Menu({
                    items: risolviConflitto
                });
                itemToAdd.setSubmenu(menuRisolviConflitto);
            } else if (this.commesseConflitto[i].bu === "BU000" && this.commesseConflitto[i].idCommessaDraft === "AA00000008") {
                var menuCancellaFerie = new sap.ui.unified.Menu({
                    items: cancellaFerie
                });
                itemToAdd.setSubmenu(menuCancellaFerie);
            }
            var commesseAperteAltreBu = _.filter(this.ProjectNotOpenModel.getData(), {
                open: true
            });

            var customData;
            if (this.commesseConflitto[i].bu !== this.umBU && _.find(commesseAperteAltreBu, {
                    codCommessa: this.commesseConflitto[i].idCommessaDraft
                })) {
                if (this.commesseConflitto[i].overture && parseInt(this.commesseConflitto[i].overture) !== 0) {
                    var col = this.oDialogTable.getColumns()[parseInt(this.commesseConflitto[i].overture)];
                    var giorno = col.getAggregation("label").getText();
                    this.umSchedulingModel.setProperty("/giornoProposto", giorno);
                    var infoProposta = sap.ui.xmlfragment("view.fragment.InfoPropostaUm", this);
                    var menuInfoProposta = new sap.ui.unified.Menu({
                        items: infoProposta
                    });
                    itemToAdd.setSubmenu(menuInfoProposta);
                    customData = new sap.ui.core.CustomData({
                        key: "scrittaCommesseConflitti",
                        value: 'OVERTURE',
                        writeToDom: true
                    });
                } else {
                    var proposta = sap.ui.xmlfragment("view.fragment.MenuPropostaUm", this);
                    var menuProposta = new sap.ui.unified.Menu({
                        items: proposta
                    });
                    itemToAdd.setSubmenu(menuProposta);

                    customData = new sap.ui.core.CustomData({
                        key: "scrittaCommesseConflitti",
                        value: this.commesseConflitto[i].stato,
                        writeToDom: true
                    });
                }
                itemToAdd.addCustomData(customData)
            } else {
                customData = new sap.ui.core.CustomData({
                    key: "scrittaCommesseConflitti",
                    value: this.commesseConflitto[i].stato,
                    writeToDom: true
                });
                itemToAdd.addCustomData(customData);
            }
            menu.addItem(itemToAdd);
        }
    },

    selectCommessaOverture: function () {
        var col = this.oDialogTable.getColumns()[parseInt(this.commessaGiornoProposta.overture)];
        var giorno = col.getAggregation("label").getText();

        sap.m.MessageBox.show(this._getLocaleText("giornoProposto") + ": " + giorno + ".\n" + this._getLocaleText("accettare"), {
            icon: sap.m.MessageBox.Icon.ALLERT,
            title: this._getLocaleText("ACCETTA_PROPOSTA"),
            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            onClose: _.bind(function (action) {
                if (action === "YES") {
                    var dataToSend = {
                        "codConsulenteDraft": this.commessaGiornoProposta.codConsulenteDraft,
                        "dataReqDraft": this.commessaGiornoProposta.dataReqDraft,
                        "idCommessaDraft": this.commessaGiornoProposta.idCommessaDraft,
                        "codAttivitaRichiestaDraft": this.commessaGiornoProposta.codAttivitaRichiestaDraft,
                        "idPeriodoSchedulingDraft": this.commessaGiornoProposta.idPeriodoSchedulingDraft,
                        "codPjmDraft": this.commessaGiornoProposta.codPjmDraft,
                        "dataInserimentoDraft": this.commessaGiornoProposta.dataInserimentoDraft,
                        "noteDraft": this.commessaGiornoProposta.noteDraft,
                        "giornoPeriodoDraft": this.commessaGiornoProposta.overture,
                        "statoReqDraft": this.commessaGiornoProposta.statoReqDraft,
                        "id_req": this.commessaGiornoProposta.id_req,
                        "oldIdReqDraft": this.commessaGiornoProposta.oldIdReqDraft ? this.commessaGiornoProposta.oldIdReqDraft : "",
                        "idReqOriginal": this.commessaGiornoProposta.idReqOriginal,
                        "giornoProposto": 0
                    };
                    this.saveRequest(dataToSend);
                } else {
                    return;
                }
            }, this)
        });
    },

    selectCommessa: function (evt) {
        //
    },

    handleMenuItemPress: function (evt) {
        var idButton = this.oButton.getId();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        var req = "req" + nButton;
        var ggIcon = "ggIcon" + nButton;
        var nota = "nota" + nButton;
        var com = "gg" + nButton;
        var row = this.oButton.getBindingContext("pmSchedulingModel").getPath().split("/")[2];
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[parseInt(row)];

        var selectedText = evt.getParameters().item.getText();
        switch (selectedText) {
            case this._getLocaleText("COPY"):

                var codCommessa = _.find(this.pmSchedulingModel.getData().commesse, {
                    nomeCommessa: dataModelConsultant[com]
                }).codCommessa
                var modelToSave = {
                    codCons: dataModelConsultant.codiceConsulente,
                    idReq: dataModelConsultant[req],
                    richiesta: dataModelConsultant[ggIcon],
                    note: dataModelConsultant[nota],
                    commessa: codCommessa
                };
                model.persistence.Storage.local.save("copy", modelToSave);
                break;
            case this._getLocaleText("PASTE"):
                if (model.persistence.Storage.local.get("copy")) {
                    this.paste = true;
                    var rich = model.persistence.Storage.local.get("copy");
                    this.commessa = rich.commessa;
                    this.actionButton(evt, rich.richiesta);
                }
                break;
            case this._getLocaleText("MOVE_HERE"):
                if (model.persistence.Storage.local.get("move")) {
                    this.moveHere = true;
                    var rich = model.persistence.Storage.local.get("move");
                    if (rich.statoReqDraft === "STANDBY") {
                        rich.statoReqDraft === "CONFIRMED"
                    }
                    var col = this.oDialogTable.getColumns()[this.nButton];
                    var giornata = col.getAggregation("label").getProperty("text");
                    var nGiorno = parseInt(giornata.split(" ")[0]);
                    if (nGiorno.toString().length === 1) {
                        nGiorno = "0" + nGiorno;
                    }
                    // Aggiungo dei controlli per recuperare il mese/anno da inizio periodo e fine periodo 
                    var allPeriods = this.periodModel.getData() ? this.periodModel.getData().items : [];
                    var periodo = _.find(allPeriods, function (n) {
                        return n.idPeriod === this.periodo;
                    }.bind(this))
                    // Mese e Anno di Inizio e Fine Periodo
                    var meseInizioP = parseInt(periodo.dataInizio.split("/")[1]);
                    var annoInizioP = parseInt(periodo.dataInizio.split("/")[2]);
                    var meseFineP = parseInt(periodo.dataFine.split("/")[1]);
                    var annoFineP = parseInt(periodo.dataFine.split("/")[2]);
                    //Ricavo anno è mese corrente selezionati 
                    var meseSelect = parseInt(rich.dataReqDraft.split("-")[1]);
                    var annoSelect = parseInt(rich.dataReqDraft.split("-")[0]);
                    //Confronto il mese è l'anno selezionato con quello del periodo
                    if (this.nButton < 20 || (this.nButton >= 20 && nGiorno > 20)) {
                        if (meseSelect !== meseInizioP) {
                            meseSelect = meseInizioP;
                        }
                        if (annoSelect !== annoInizioP) {
                            annoSelect = annoInizioP;
                        }
                    } else {
                        if (meseSelect !== meseFineP) {
                            meseSelect = meseFineP;
                        }
                        if (annoSelect !== annoFineP) {
                            annoSelect = annoFineP;
                        }
                    }
                    rich.dataReqDraft = annoSelect + "-" + meseSelect + "-" + nGiorno;
                    this.commessa = rich.idCommessaDraft;
                    delete rich.id;
                    delete rich.nomeCommessa;
                    delete rich.stato;
                    delete rich.icon;
                    rich.giornoPeriodoDraft = this.nButton
                    this.saveMove = true;
                    this._delData(rich);
                }
                break;
            case this._getLocaleText("ASSIGN_HERE"):
                if (this.objToAssign) {
                    this.assignHere = true;
                    var col = this.oDialogTable.getColumns()[this.nButton];
                    var giornata = col.getAggregation("label").getProperty("text");
                    var nGiorno = parseInt(giornata.split(" ")[0]);
                    if (nGiorno.toString().length === 1) {
                        nGiorno = "0" + nGiorno;
                    }
                    var allPeriods = this.periodModel.getData() ? this.periodModel.getData().items : [];
                    var periodo = _.find(allPeriods, function (n) {
                        return n.idPeriod === this.periodo;
                    }.bind(this))
                    // Mese e Anno di Inizio e Fine Periodo
                    var meseInizioP = parseInt(periodo.dataInizio.split("/")[1]);
                    var annoInizioP = parseInt(periodo.dataInizio.split("/")[2]);
                    var meseFineP = parseInt(periodo.dataFine.split("/")[1]);
                    var annoFineP = parseInt(periodo.dataFine.split("/")[2]);
                    //Ricavo anno è mese corrente selezionati 
                    var meseSelect = parseInt(this.objToAssign.dataReqDraft.split("-")[1]);
                    var annoSelect = parseInt(this.objToAssign.dataReqDraft.split("-")[0]);
                    //Confronto il mese è l'anno selezionato con quello del periodo
                    if (this.nButton < 20 || (this.nButton >= 20 && nGiorno > 20)) {
                        if (meseSelect !== meseInizioP) {
                            meseSelect = meseInizioP;
                        }
                        if (annoSelect !== annoInizioP) {
                            annoSelect = annoInizioP;
                        }
                    } else {
                        if (meseSelect !== meseFineP) {
                            meseSelect = meseFineP;
                        }
                        if (annoSelect !== annoFineP) {
                            annoSelect = annoFineP;
                        }
                    }
                    this.objToAssign.dataReqDraft = annoSelect + "-" + meseSelect + "-" + nGiorno;
                    this.objToAssign.giornoPeriodoDraft = this.nButton;
                    this.reInsertRequest(this.objToAssign);
                    this.commessa = this.objToAssign.idCommessaDraft;
                }
                break;
            case this._getLocaleText("PROPOSE"):

                break;
            case this._getLocaleText("ASSIGN_OVERTURE"):
                this.assegnaProposta(this.commessaDaProporre);
                break;
            case this._getLocaleText("REMOVE_OVERTURE"):
                this.commessaDaProporre = undefined;
                break;
            case this._getLocaleText("NOTE"):
                //                var comA = "gg" + nButton + "a";
                //                var ggIconA = "ggIcon" + nButton + "a";
                //                var notaA = "nota" + nButton + "a";
                //                if (dataModelConsultant[comA] && dataModelConsultant[ggIconA] !== "sap-icon://offsite-work") {
                //                    if (dataModelConsultant[com] === this.nomeCommessaFerie || dataModelConsultant[comA] === this.nomeCommessaFerie) {
                //                        this.actionButton(evt);
                //                    } else {
                //                        this.doppiaCancellazione = undefined;
                //                        this.doppiaCopia = undefined;
                //                        this.doppiaNote = [];
                //                        this.doppiaNote.push({
                //                            ric: dataModelConsultant[com],
                //                            icon: dataModelConsultant[ggIcon],
                //                            nota: dataModelConsultant[nota]
                //                        }, {
                //                            ric: dataModelConsultant[comA],
                //                            icon: dataModelConsultant[ggIconA],
                //                            nota: dataModelConsultant[notaA]
                //                        });
                //                        this.commesseDialog = sap.ui.xmlfragment("view.fragment.SelectProjectActionSheet", this);
                //                        this.getView().addDependent(this.commesseDialog);
                //                        this.pmSchedulingModel.setProperty("/requests", this.doppiaNote);
                //                        this.commesseDialog.setModel(this.pmSchedulingModel, "pmSchedulingModel");
                //                        this.commesseDialog.open();
                //                    }
                //                } else {
                this.isNote = true;
                this.actionButton(evt);
                //                }
                //
                //
                break;
            default:
                var risoluzioneConflitto = false;
                if (!(selectedText === this._getLocaleText("CONFIRM") ||
                        (selectedText === this._getLocaleText("DELETE") && !evt.getParameters().item.getStartsSection()) ||
                        selectedText === this._getLocaleText("MOVE"))) {
                    var selectedActiviy = _.find(this.commesseQuestoConsulente, {
                        'nomeCommessa': selectedText
                    });
                    if (selectedActiviy || !evt.getParameters().item.getIcon())
                        return;
                    this.paste = false;
                    this.actionButton(evt);
                }
        }
    },

    selectCommessaConflitto: function (evt) {
        var nomeCommessaSelezionata = evt.getParameters().item.getParent().getParent().getProperty("text");
        var commessaSelezionata = _.remove(this.commesseConflitto, {
            'nomeCommessa': nomeCommessaSelezionata
        })[0];
        var action = evt.getSource().getText();

        var params = {
            codCommessa: commessaSelezionata.idCommessaDraft,
            idPeriodo: this.periodo
        };
        model.tabellaSchedulingUM.checkIsToWork(params)
            .then(_.bind(function (res) {
                if (!res.results[0].UM_MODIFY) {
                    if (action === this._getLocaleText("CONFIRM")) {
                        // se viene selezionato CONFERMA
                        var fSuccess = function () {
                            this.oDialogTable.setBusy(false);
                            this.confermaCommessa(commessaSelezionata);
                        };

                        var fError = function () {
                            this._pmActionSheet.close();
                            this._pmActionSheet.destroy(true);
                            this.oDialogTable.setBusy(false);
                        };

                        fSuccess = _.bind(fSuccess, this);
                        fError = _.bind(fError, this);
                        for (var j = 0; j < this.commesseConflitto.length; j++) {
                            var idReq = this.commesseConflitto[j].id;
                            this.oDialogTable.setBusy(true);
                            if (j === 0) {
                                if (this.commesseConflitto.length > 1) {
                                    model.tabellaSchedulingUM.deleteRequest(idReq, "STANDBY", this.commesseConflitto.length);
                                } else {
                                    var a = _.find(this.umSchedulingModel.getProperty("/schedulazioni"), {
                                        codiceConsulente: this.commesseConflitto[0].codConsulenteDraft
                                    });
                                    for (var prop in a) {
                                        if (a[prop] === this.commesseConflitto[0].id) {
                                            var cambioStato = prop.replace("req", "ggStato");
                                            a[cambioStato] = "STANDBY";
                                        }
                                    }

                                    var b = _.find(this.pmSchedulingModel.getProperty("/schedulazioni"), {
                                        codiceConsulente: this.commesseConflitto[0].codConsulenteDraft
                                    });
                                    for (var prop in b) {
                                        if (b[prop] === this.commesseConflitto[0].id) {
                                            var cambioStato = prop.replace("req", "ggStato");
                                            b[cambioStato] = "STANDBY";
                                        }
                                    }

                                    model.tabellaSchedulingUM.deleteRequest(idReq, "STANDBY", this.commesseConflitto.length)
                                        .then(fSuccess, fError);
                                }
                            } else {
                                model.tabellaSchedulingUM.deleteRequest(idReq, "STANDBY")
                                    .then(fSuccess, fError);
                            }
                        }
                    } else if (action === this._getLocaleText("DELETE")) {
                        // se viene selezionato CANCELLA
                        commessaSelezionata.id_req = commessaSelezionata.id;
                        this._delData(commessaSelezionata);
                    } else
                    if (action === this._getLocaleText("MOVE")) {
                        // se viene selezionato SPOSTA
                        var codConsulente = this.dataModelConsultant.codiceConsulente;
                        var idReq = commessaSelezionata.id;
                        var richiesta = commessaSelezionata.icon;
                        for (var j in this.dataModelConsultant) {
                            if (this.dataModelConsultant[j] === idReq) {
                                var propNote = j.replace("req", "note");
                                var note = this.dataModelConsultant[propNote];
                                break;
                            }
                        }
                        //                        var codCommessa = _.find(this.pmSchedulingModel.getData().commesse, {
                        //                            nomeCommessa: commessaSelezionata.nomeCommessa
                        //                        }).codCommessa

                        var codCommessa = commessaSelezionata.idCommessaDraft;

                        model.persistence.Storage.local.save("move", commessaSelezionata);
                    } else {
                        // se viene selezionato qualcos'altro che per ora non c'è
                        sap.m.MessageToast.show(this._getLocaleText("AZIONE_NON_RICONOSCIUTA"));
                    }
                } else {
                    this._enterErrorFunction("alert", "Attenzione", "Commessa in lavorazione da " + res.results[0].COGNOME_CONSULENTE + " " + res.results[0].NOME_CONSULENTE);
                }
            }, this), _.bind(function (err) {
                console.log(err)
            }, this));
    },

    confermaCommessa: function (req) {
        var fSuccess = function () {
            // leggi singolo consulente
            var obj = {};
            obj.codBu = this.user.umBU;
            obj.idPeriodo = this.periodo;
            obj.idStaff = _.find(this.allCommesse, {
                codCommessa: this.tempReq.idCommessaDraft
            }).codiceStaffCommessa;
            obj.selectedConsultantsCode = this.tempReq.codConsulenteDraft;
            this.leggiTabella(obj);
        };

        var fError = function (err) {
            sap.m.MessageToast.show("ERRORE")
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        this.tempReq = req;

        model.requestSaveToDraft.delRequest(req.id, "CONFIRMED")
            .then(fSuccess, fError);
    },

    // funzione che ricarica la tabella
    reloadPress: function () {
        this.ric.conflitti = "";
        this.richiestaConFiltri = undefined;
        //        if(this.ric.idStaff && this.ric.idStaff==="CSC0000031")
        //        {
        //            delete this.ric.idStaff;
        //        }
        delete this.ric.selectedConsultantsCode;

        this.leggiTabella(this.ric);
        this._resetFilterSelectedItems();
        this.visibleModel.setProperty("/undoButton", false);
        this.clearPasteCommesse();
        this.svuotaLocalStorageUndo();
        this.fromSaveLocal = true;
    },

    assegnaProposta: function (proposta) {
        var dataToSend = {
            "codConsulenteDraft": proposta.codConsulenteDraft,
            "dataReqDraft": proposta.dataReqDraft,
            "idCommessaDraft": proposta.idCommessaDraft,
            "codAttivitaRichiestaDraft": proposta.codAttivitaRichiestaDraft,
            "idPeriodoSchedulingDraft": proposta.idPeriodoSchedulingDraft,
            "codPjmDraft": proposta.codPjmDraft,
            "dataInserimentoDraft": proposta.dataInserimentoDraft,
            "noteDraft": proposta.noteDraft,
            "giornoPeriodoDraft": proposta.giornoPeriodoDraft,
            "statoReqDraft": proposta.statoReqDraft,
            "id_req": proposta.id_req,
            "oldIdReqDraft": proposta.oldIdReqDraft ? proposta.oldIdReqDraft : "",
            "idReqOriginal": proposta.idReqOriginal,
            "giornoProposto": this.nButton
        };

        this.saveRequest(dataToSend);
    },

    // funzione per verificare la presenza di richieste di modifica
    checkChangeRequests: function () {
        var fSuccess = function (result) {
            if (result && result.items && result.items.length === 1) {
                if (result.items[0].stato === "CHIUSO") {
                    var fSuccessBis = function (result) {
                        if (result && result.items && result.items.length > 0) {
                            this.richiesteModifica = result.items;
                            this.visibleModel.setProperty("/loadChangeRequests", true);
                        } else {
                            this.visibleModel.setProperty("/loadChangeRequests", false);
                        }
                    };

                    var fErrorBis = function (err) {
                        this.richiesteModifica = undefined;
                        this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'periodsModifyRequestSet'");
                    };

                    fSuccessBis = _.bind(fSuccessBis, this);
                    fErrorBis = _.bind(fErrorBis, this);
                    var stato = "PROPOSED";
                    model.SchedulingModifyRequests.checkValueToUpload(periodo, commessa, stato)
                        .then(fSuccessBis, fErrorBis);
                } else {
                    this.richiesteModifica = undefined;
                    this.visibleModel.setProperty("/loadChangeRequests", false);
                }
            } else {
                return;
            }
        };

        var fError = function (err) {
            this.richiesteModifica = undefined;
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'profiloCommessaPeriodoSet'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var periodo = this.periodo;
        var commessa = model.persistence.Storage.session.get("commessa").codCommessa;
        //        var stato = "PROPOSED";


        model.ProfiloCommessaPeriodo.readRecordByPeriodCommessa(periodo, commessa)
            .then(fSuccess, fError);


        //        model.SchedulingModifyRequests.checkValueToUpload(periodo, commessa, stato)
        //            .then(fSuccess, fError);
    },

    // funzione per caricare le richieste di modifica
    loadChangeRequests: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFERMA_UPLOAD_MODIFICHE") + " " + this.chosedCom, {
            icon: sap.m.MessageBox.Icon.ALLERT,
            title: this._getLocaleText("CONFERMA_UPLOAD_MODIFICHE_TITLE"),
            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            onClose: _.bind(function (action) {
                if (action === "YES") {
                    this.oDialogTable.setBusy(true);
                    var array = [];
                    var arrayToDel = [];
                    var arrayToMod = [];
                    for (var i = 0; i < this.richiesteModifica.length; i++) {
                        var modifica = false;
                        var obj = {};
                        obj.codConsulente = this.richiesteModifica[i].codConsulente;
                        obj.dataReqPeriodo = this.richiesteModifica[i].dataRichiesta;
                        obj.idCommessa = this.richiesteModifica[i].codCommessa;
                        obj.codAttivitaRichiesta = this.richiesteModifica[i].codAttivita;
                        obj.idPeriodoSchedulingRic = this.richiesteModifica[i].idPeriodo;
                        obj.codPjm = this.richiesteModifica[i].codPjm;
                        obj.dataInserimento = this.richiesteModifica[i].dataInserimento;
                        obj.note = this.richiesteModifica[i].note;
                        obj.giorno = this.richiesteModifica[i].giorno;
                        obj.statoReq = "CONFIRMED";
                        obj.idReqOld = 0;
                        obj.idReqOriginale = this.richiesteModifica[i].idReqOriginale;
                        obj.idReqModifica = this.richiesteModifica[i].idReqModifica;

                        var tempCons = _.find(this.pmSchedulingModel.getProperty("/schedulazioni"), {
                            codiceConsulente: this.richiesteModifica[i].codConsulente
                        });
                        if (tempCons) {
                            var prop = "gg" + this.richiesteModifica[i].giorno;
                            var tempCommessa = _.find(this.pmSchedulingModel.getProperty("/commesse"), {
                                codCommessa: this.richiesteModifica[i].codCommessa
                            });
                            while (tempCons[prop]) {
                                if (tempCons[prop] === tempCommessa.nomeCommessa) {
                                    var req = prop.replace("gg", "req");
                                    obj.idReq = tempCons[req];
                                    modifica = true;
                                    break;
                                }
                                items = "a";
                                prop = prop + items;
                            }

                            if (this.richiesteModifica[i].codAttivita === "DELETE") {
                                arrayToDel.push(obj);
                            } else if (this.richiesteModifica[i].idReqOriginale !== 0 || modifica) {
                                arrayToMod.push(obj);
                            } else {
                                array.push(obj);
                            }
                        }
                    }

                    var objUpdate = {};
                    objUpdate.idPeriodo = this.periodo;
                    objUpdate.codCommessa = this.codCommessaSelezionata;

                    if (arrayToDel.length > 0 && arrayToMod.length === 0) {
                        model.tabellaSchedulingUM.deleteModifyInFDraft(arrayToDel)
                            .then(_.bind(function () {
                                model.tabellaSchedulingUM.insertModifyInFDraft(array)
                                    .then(_.bind(function () {
                                        model.SchedulingModifyRequests.updateStatoMulti(objUpdate)
                                            .then(_.bind(function () {
                                                this.visibleModel.setProperty("/loadChangeRequests", false);
                                                this.reloadPress();
                                            }, this), _.bind(function () {
                                                this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'updateStatoRichiesteModifica'");
                                            }, this));
                                    }, this), _.bind(function () {
                                        this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'scritturaMultiDraft'");
                                    }, this))
                            }, this), _.bind(function () {
                                this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'cancellazioneMultiDraft'");
                            }, this));
                    } else if (arrayToDel.length === 0 && arrayToMod.length > 0) {
                        model.tabellaSchedulingUM.modModifyInFDraft(arrayToMod)
                            .then(_.bind(function () {
                                model.tabellaSchedulingUM.insertModifyInFDraft(array)
                                    .then(_.bind(function () {
                                        model.SchedulingModifyRequests.updateStatoMulti(objUpdate)
                                            .then(_.bind(function () {
                                                this.visibleModel.setProperty("/loadChangeRequests", false);
                                                this.reloadPress();
                                            }, this), _.bind(function () {
                                                this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'updateStatoRichiesteModifica'");
                                            }, this));
                                    }, this), _.bind(function () {
                                        this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'scritturaMultiDraft'");
                                    }, this))
                            }, this), _.bind(function () {
                                this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'modificaMultiDraft'");
                            }, this));
                    } else if (arrayToDel.length > 0 && arrayToMod.length > 0) {
                        model.tabellaSchedulingUM.deleteModifyInFDraft(arrayToDel)
                            .then(_.bind(function () {
                                model.tabellaSchedulingUM.modModifyInFDraft(arrayToMod)
                                    .then(_.bind(function () {
                                        model.tabellaSchedulingUM.insertModifyInFDraft(array)
                                            .then(_.bind(function () {
                                                model.SchedulingModifyRequests.updateStatoMulti(objUpdate)
                                                    .then(_.bind(function () {
                                                        this.visibleModel.setProperty("/loadChangeRequests", false);
                                                        this.reloadPress();
                                                    }, this), _.bind(function () {
                                                        this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'updateStatoRichiesteModifica'");
                                                    }, this));
                                            }, this), _.bind(function () {
                                                this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'scritturaMultiDraft'");
                                            }, this))
                                    }, this), _.bind(function () {
                                        this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'modificaMultiDraft'");
                                    }, this));
                            }, this), _.bind(function () {
                                this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'cancellazioneMultiDraft'");
                            }, this));
                    } else {
                        model.tabellaSchedulingUM.insertModifyInFDraft(array)
                            .then(_.bind(function () {
                                model.SchedulingModifyRequests.updateStatoMulti(objUpdate)
                                    .then(_.bind(function () {
                                        this.visibleModel.setProperty("/loadChangeRequests", false);
                                        this.reloadPress();
                                    }, this), _.bind(function () {
                                        this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'updateStatoRichiesteModifica'");
                                    }, this));
                            }, this), _.bind(function () {
                                this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore servizio 'scritturaMultiDraft'");
                            }, this));
                    }
                } else {
                    return;
                }
            }, this),
        });
    },

    /*************Filter Part**********************/

    /*****Functions To populate List Filter with all fields *******/
    _loadFilters: function () {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this._loadSkills()
                .then(defer.resolve, fError);
        }
        fSuccess = _.bind(fSuccess, this);

        var fError = function (err) {
            sap.m.MessageToast.show(this._getLocaleText("errorLoadingFilterLists"));
            defer.reject(err);
        }
        fError = _.bind(fError, this);

        this._loadConsultants()
            .then(_.bind(function (res) {
                this._loadBuList()
                    .then(_.bind(fSuccess, this), _.bind(fError, this))

            }, this), fError)
        return defer.promise;
    },

    _loadConsultants: function () {
        var defer = Q.defer();
        var fConsultantSuccess = function (result) {

            //            console.log("-------Consultants Load-----------");
            //            console.log(result);
            //            var buUm = model.persistence.Storage.session.get("user").codConsulenteBu;
            var consultantsBu = result.items;
            this.consultantModel.setProperty("/results", consultantsBu);
            defer.resolve(consultantsBu);
        };
        var fConsultantError = function (err) {
            sap.m.MessageToast("error BU");
            defer.reject(err);
        };
        fConsultantSuccess = _.bind(fConsultantSuccess, this);
        fConsultantError = _.bind(fConsultantError, this);

        model.Consultants.readConsultants()
            .then(fConsultantSuccess, fConsultantError);

        return defer.promise;
    },

    _loadBuList: function () {
        var defer = Q.defer();

        var fSuccess = function (result) {
            //            console.log("-------Bu Load-----------");
            //            console.log(result);
            this.buList = {
                results: result.items
            };
            this.buModel.setData(this.buList);
            defer.resolve(result);
        }
        fSuccess = _.bind(fSuccess, this);

        var fError = function (err) {
            this.buList = {
                "results": []
            };
            this.buModel.setData(this.buList);
            sap.m.MessageToast.show(this._getLocaleText("errorLoadingBuList"));
            defer.reject(err);
        }
        fError = _.bind(fError, this);

        model.BuList.getBuListByStatus("APERTO")
            .then(fSuccess, fError)

        return defer.promise;
    },

    _loadSkills: function () {
        var defer = Q.defer();

        var fSuccess = function (result) {
            //            console.log("-------Skill Load-----------");
            //            console.log(result);
            this.skills = {
                results: result.items
            };
            this.skillModel.setData(this.skills);
            defer.resolve(result);
        }
        fSuccess = _.bind(fSuccess, this);

        var fError = function (err) {
            this.skills = {
                "results": []
            };
            this.skillModel.setData(this.skills);
            sap.m.MessageToast.show(this._getLocaleText("errorLoadingSkillList"));
            defer.reject(err);

        }
        fError = _.bind(fError, this);

        model.Skill.readSkills()
            .then(fSuccess, fError)

        return defer.promise;
    },
    /********************************************************/
    /********SearchField Management****************************/
    onFilterUMConsultantSearch: function (evt) {
        this._onFilterUMSearch(evt, "idListaConsulentiUM", ["nomeConsulente", "cognomeConsulente", "codConsulente"]);
    },

    onFilterUMBuSearch: function (evt) {
        this._onFilterUMSearch(evt, "idListaBuUM", ["descrizioneBu", "codiceBu"]);
    },

    onFilterUMSkillSearch: function (evt) {
        this._onFilterUMSearch(evt, "idListaSkillUM", ["nomeSkill", "descSkill", "codSkill"]);
    },

    _onFilterUMSearch: function (evt, listId, params) {
        var value = evt.getSource().getValue();
        var list = sap.ui.getCore().getElementById(listId);
        if (!list.getBinding("items").oList || list.getBinding("items").oList.length === 0)
            return;

        var temp = list.getBinding("items").oList[0]; // a template just to recover the data types
        var filtersArr = [];

        if (!_.isEmpty(params)) {
            if (!_.isArray(params)) {
                params = [params];
            }
            for (var i = 0; i < params.length; i++) {
                switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
                    case "undefined":
                        break;
                    case "string":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
                        break;
                    case "number":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
                        break;
                }
            }
            var filter = new sap.ui.model.Filter({
                filters: filtersArr,
                and: false
            });
            list.getBinding("items").filter(filter);
            return;
        }
        list.getBinding("items").filter();
    },
    /****************************************************************************/

    /**************Applying Filter****************************************/
    onFilterPress: function () {
        var page = this.getView().byId("schedulingPage");
        if (!this.umDialog) {
            this.umDialog = sap.ui.xmlfragment("view.dialog.filterDialogUM_", this);
            page.addDependent(this.umDialog);
        }
        this._loadFilterLists();

        var binding = sap.ui.getCore().getElementById("idListaConsulentiUM").getBinding("items");
        if (binding) {
            binding.filter([]);
        }

        var buBinding = sap.ui.getCore().getElementById("idListaBuUM").getBinding("items");
        if (buBinding) {
            buBinding.filter([]);
        }

        var skillBinding = sap.ui.getCore().getElementById("idListaSkillUM").getBinding("items");
        if (skillBinding) {
            skillBinding.filter([]);
        }

        this.uiModel.setProperty("/searchValue", "");
        this.uiModel.refresh();
        this.umDialog.open();
    },

    _loadFilterLists: function () {
        this.umFilterModel.setProperty("/selectedTab", "consultants");
        this.umFilterModel.setProperty("/allButtonVisible", true);
        this._setFilterProperty("/dayRange", "giorniScheduling", null, null);
        this._setFilterProperty("/consultants", "codiceConsulente", "consultantModel", "codConsulente");
        this._setFilterProperty("/buList", "cod_consulenteBu", "buModel", "codiceBu");
        this._setFilterProperty("/skills", "nomeSkill", "skillModel", "nomeSkill");
        this.umFilterModel.refresh();
    },

    _setFilterProperty: function (propertyName, key, model, modelKey) {
        var schedulations = this.listAllConsultant;
        //        var schedulations = this.allSchedulingForFilter;
        //        var schedulations = this.umSchedulingModel.getProperty("/schedulazioni");
        var schedulationItems = _.pluck(schedulations, key);

        if (propertyName.indexOf("/dayRange") >= 0) {
            var maxDay = _.max(schedulationItems) ? _.max(schedulationItems) : 31;
            if (!this.umFilterModel.getProperty("/dayRange")) {
                this._initializeDayCountRange();
            } else {
                this.umFilterModel.setProperty("/dayRange/dayMax", maxDay);
                var filterValue = parseFloat(this.umFilterModel.getProperty("/dayRange").val);
                if (!filterValue)
                    filterValue = maxDay;
                this.umFilterModel.setProperty("/dayRange/val", filterValue);
            }
        } else {
            var selectedItems = this._getFilterSelectedItems(propertyName);
            if (propertyName.indexOf("/skills") >= 0) {
                var skills = [];
                for (var i = 0; i < schedulationItems.length; i++) {
                    var singleSkills = schedulationItems[i].split(",");
                    singleSkills = _.map(singleSkills, function (item) {
                        return item.trim();
                    });
                    skills = skills.concat(singleSkills);
                }
                schedulationItems = skills;
            }
            var values;
            if (modelKey !== "codiceBu") {
                values = _.filter(this.getView().getModel(model).getProperty("/results"), _.bind(function (item) {
                    return _.contains(schedulationItems, item[modelKey]);
                }, this));
                values = _.map(values, _.bind(function (val) {
                    var selectedKeys = _.pluck(selectedItems, modelKey);
                    val.selected = _.contains(selectedKeys, val[modelKey]);
                    return val;
                }, this));
            } else {
                values = this.getView().getModel(model).getProperty("/results");
            }
            this.umFilterModel.setProperty(propertyName, values);
        }
    },

    _initializeDayCountRange: function () {
        var schedulations = this.allScheduling;

        var schedulationItems = _.pluck(schedulations, "giorniScheduling");

        var maxDay = _.max(schedulationItems) ? parseFloat(_.max(schedulationItems)) : 31;
        var dayRange = {
            "dayMin": 0,
            "dayMax": maxDay,
            "val": maxDay
        }
        this.umFilterModel.setProperty("/dayRange", dayRange);
    },

    _getFilterSelectedItems: function (prop) {
        var items = this.umFilterModel.getProperty(prop);
        if (prop == "/dayRange")
            return parseFloat(items.val);

        var selectedItems = [];
        if (items) {
            selectedItems = _.filter(items, function (item) {
                return item.selected;
            })
        }
        return selectedItems;
    },

    _setPropertyItems: function (propertyName, selectedBool) {
        var property = "/" + propertyName;
        var items = this.umFilterModel.getProperty(property);

        if (property.indexOf("/dayRange") >= 0) {
            this._initializeDayCountRange();
            return;
        }

        items = _.map(items, function (obj) {
            obj.selected = selectedBool;
            return obj;
        });
        this.umFilterModel.setProperty(property, items);
    },

    _checkAllSelectedFilterItems: function (propertyName) {
        var property = "/" + propertyName;
        var items = this.umFilterModel.getProperty(property);
        var falseItem = _.find(items, function (item) {
            return !item.selected;
        })
        var result = falseItem ? false : true;

        return result;
    },

    _resetFilterSelectedItems: function () {
        delete this.ric.selectedConsultantsCode;
        for (var prop in this.umFilterModel.getData()) {
            if (prop !== "allButtonVisible" && prop !== "allSelected")
                this._setPropertyItems(prop, false);
        }
        this.umFilterModel.refresh();
    },

    //    _filterFunc: function (res) {
    //        var schedulations = this.allSchedulingForFilter;
    //
    //        var selectedConsultants = this._getFilterSelectedItems("/consultants");
    //        var selectedConsultantsCodes = selectedConsultants && selectedConsultants.length > 0 ? _.pluck(selectedConsultants, 'codConsulente') : false;
    //
    //        var selectedBUnits = this._getFilterSelectedItems("/buList");
    //        var selectedBUnitsCodes = selectedBUnits && selectedBUnits.length > 0 ? _.pluck(selectedBUnits, 'codiceBu') : false;
    //
    //        var selectedSkills = this._getFilterSelectedItems("/skills");
    //        var selectedSkillsNames = selectedSkills && selectedSkills.length > 0 ? _.pluck(selectedSkills, 'nomeSkill') : false;
    //
    //        var filteredSchedulations = _.filter(schedulations, _.bind(function (item) {
    //            var result = true;
    //
    //            if (selectedConsultantsCodes) {
    //                result = _.contains(selectedConsultantsCodes, item.codiceConsulente);
    //            }
    //            if (!result)
    //                return false;
    //
    //            if (selectedSkillsNames) {
    //                var itemSkills = item.nomeSkill.split(",");
    //                var found = false;
    //
    //                for (var i = 0; i < itemSkills.length; i++) {
    //                    itemSkills[i] = itemSkills[i].trim();
    //                    found = _.contains(selectedSkillsNames, itemSkills[i]);
    //                    if (found)
    //                        break;
    //                }
    //                if (!found)
    //                    result = false;
    //
    //            }
    //            if (!result)
    //                return false;
    //
    //            if (selectedBUnitsCodes) {
    //                result = _.contains(selectedBUnitsCodes, item.cod_consulenteBu);
    //            }
    //            if (!result)
    //                return false;
    //
    //            var itemDay = item.giorniScheduling;
    //            result = (itemDay >= this.umFilterModel.getProperty("/dayRange/dayMin") && itemDay <= this.umFilterModel.getProperty("/dayRange/val"));
    //            return result;
    //
    //        }, this));
    //
    //        this.pmSchedulingModel.setProperty("/schedulazioni", filteredSchedulations);
    //        if (filteredSchedulations.length > 0 && filteredSchedulations.length <= 6) {
    //            this.pmSchedulingModel.setProperty("/nRighe", filteredSchedulations.length);
    //        } else if (filteredSchedulations.length > 6) {
    //            this.pmSchedulingModel.setProperty("/nRighe", 6);
    //        } else {
    //            this.pmSchedulingModel.setProperty("/nRighe", 1);
    //        }
    //        setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));
    //    },

    onFilterUMDialogOK: function (evt) {
        utils.Busy.show("Ricaricamento Dati Real Time");
        var fSuccess = function () {
            utils.Busy.hide();
            //            this._filterFunc();
            this.umDialog.close();
        }
        fSuccess = _.bind(fSuccess, this);

        var req = _.clone(this.ric);

        var selectedConsultant = this._getFilterSelectedItems("/consultants");
        var selectedBunits = this._getFilterSelectedItems("/buList");
        var selectedSkills = this._getFilterSelectedItems("/skills");
        var selectedSchedulingDays = this._getFilterSelectedItems("/dayRange");
        //        if (selectedConsultant && selectedConsultant.length === 1 && selectedBunits.length == 0 && selectedSkills.length == 0) {
        //            req.selectedConsultantsCode = selectedConsultant[0].codConsulente;
        //            req.idStaff = "CSC0000031";
        //        }
        if (selectedConsultant && selectedConsultant.length > 0) {
            var arrayConsultants = [];
            for (var i = 0; i < selectedConsultant.length; i++) {
                arrayConsultants.push(selectedConsultant[i].codConsulente);
            }
            req.arrayConsultants = arrayConsultants;
        }
        if (!selectedConsultant || selectedConsultant.length !== 1) {
            if (selectedBunits.length !== 0) {
                var arrayBu = [];
                for (var i = 0; i < selectedBunits.length; i++) {
                    arrayBu.push(selectedBunits[i].codiceBu);
                }
                req.arrayBu = arrayBu;
            }
            if (selectedSkills.length !== 0) {
                var arraySkill = [];
                for (var i = 0; i < selectedSkills.length; i++) {
                    //                    arraySkill.push(selectedSkills[i].nomeSkill);
                    arraySkill.push(selectedSkills[i].codSkill);
                }
                req.arraySkill = arraySkill;
            }
            if (selectedSchedulingDays)
                req.selectedSchedulingDays = selectedSchedulingDays;
        }
        this.richiestaConFiltri = req;

        this.leggiTabella(req).then(fSuccess);
    },

    onFilterTabSelect: function (evt) {
        var tab = evt.getParameter("selectedKey");

        if (tab == "days") {
            sap.ui.getCore().byId("sliderId").setValue(this.umFilterModel.getProperty("/dayRange/val"));
            this.umFilterModel.setProperty("/allButtonVisible", false);
        } else {
            this.umFilterModel.setProperty("/allSelected", this._checkAllSelectedFilterItems(tab));
            this.umFilterModel.setProperty("/allButtonVisible", true);
        }
        this.umFilterModel.refresh();
    },

    onSliderChange: function (evt) {
        var value = parseFloat(evt.getSource().getValue());
        this.umFilterModel.setProperty("/dayRange/val", parseFloat(value));
    },

    onSliderLiveChange: function (evt) {
        var value = parseFloat(evt.getSource().getValue());
        this.umFilterModel.setProperty("/dayRange/val", parseFloat(value));
    },
    /*************************************************/

    onViewConflictsPress: function () {
        var req = {};
        if (this.richiestaConFiltri) {
            req = this.richiestaConFiltri;
        } else {
            req = this.ric;
        }
        req.conflitti = "X";
        this.leggiTabella(req);
    },

    _checkRichiesteModifica: function () {
        var fSuccess = function (result) {
            if (result && result.res && result.res.length > 0) {
                this.visibleModel.setProperty("/checkChangeRequests", true);
            } else {
                this.visibleModel.setProperty("/checkChangeRequests", false);
            }
        };

        var fError = function (err) {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura XSJS 'checkUmRequestsModify'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var periodo = this.periodo;
        var bu = this.umBU;
        var stato = "PROPOSED";

        model.tabellaSchedulingUM.checkListaCommesseConRichiesteModifica(periodo, bu, stato)
            .then(fSuccess, fError);
    },

    // controllo se ci sono consulenti generici e faccio vedere il bottone
    _checkProjectsGeneric: function (scheduling) {
        var listaConsulentiGenerici = _.filter(scheduling, function (item) {
            if (item.codiceConsulente.indexOf("CCG") >= 0)
                return item
        });
        if (listaConsulentiGenerici && _.size(listaConsulentiGenerici) > 0) {
            this.visibleModel.setProperty("/checkProjectsGeneric", true);
        } else {
            this.visibleModel.setProperty("/checkProjectsGeneric", false);
        }
    },

    checkListRequestsProposed: function () {
        var fSuccess = function (result) {
            if (result && result.res && result.res.length > 0) {
                this.listaCommesseRicModModel.setProperty("/dati", result.res);
                if (!this.dialogListaCommesseConRichiestaModifica) {
                    this.dialogListaCommesseConRichiestaModifica = sap.ui.xmlfragment("view.dialog.listaCommesseDiRichiesteModifica", this);
                    this.getView().addDependent(this.dialogListaCommesseConRichiestaModifica);
                }
                this.dialogListaCommesseConRichiestaModifica.open();
            } else {
                this._enterErrorFunction("information", "Richieste modifiche", "Nessuna richiesta di modifica");
            }

        };

        var fError = function (err) {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura XSJS 'checkUmRequestsModify'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var periodo = this.periodo;
        var bu = this.umBU;
        var stato = "PROPOSED";

        model.tabellaSchedulingUM.checkListaCommesseConRichiesteModifica(periodo, bu, stato)
            .then(fSuccess, fError);
    },

    onCloseDialogListaCommesseRicMod: function () {
        this.dialogListaCommesseConRichiestaModifica.close();
    },

    onNoteConsultantPress: function (evt) {
        if (!this.visibleModel.getProperty("/wipPeriod")) {
            return;
        }
        if (!this._noteConsultantDialog) {
            this._noteConsultantDialog = sap.ui.xmlfragment("view.dialog.um_admin_noteConsultant", this);
            this.getView().addDependent(this._noteConsultantDialog);
        }
        var icon = evt.getSource();
        this.consultantRow = icon.getParent().getBindingContext("pmSchedulingModel").getObject();
        var consultantNote = "";
        if (this.consultantRow.notaConsulente) {
            consultantNote = this.consultantRow.notaConsulente;
        }
        this.noteConsultantModel.setData(consultantNote);
        this._noteConsultantDialog.openBy(icon);
    },

    onCancelConsultantNotePress: function () {
        this._noteConsultantDialog.close();
    },

    onSaveConsultantNotePress: function () {
        var period = this.periodo;
        var consultantCode = this.consultantRow.codiceConsulente;
        var umCode = model.persistence.Storage.session.get("user").codConsulente;
        var administratorCode = "";
        var consultantNote = this.noteConsultantModel.getData();
        var fSuccess = function (ris) {
            this._noteConsultantDialog.close();
            this.leggiTabella(this.ric);
        }.bind(this);
        var fError = function (ris) {
            this.oDialogTable.setBusy(false);
            var title = this._getLocaleText("attention");
            var messageText = this._getLocaleText("savingError");
            this._enterErrorFunction("warning", title, messageText);
        }.bind(this);

        this.oDialogTable.setBusy(true);
        model.requestSave.insertConsultantNote(period, consultantCode, umCode, administratorCode, consultantNote)
            .then(fSuccess, fError);
    },

    clearPasteCommesse: function () {
        if (model.persistence.Storage.local.get("copy")) {
            model.persistence.Storage.local.remove("copy");
        }
        if (model.persistence.Storage.local.get("copyBis")) {
            model.persistence.Storage.local.remove("copyBis");
        }
        this.paste = false;
    },

    _checkIsToWork: function () {
        var allCommesse = _.cloneDeep(this.allCommesseDonwload);
        _.remove(allCommesse, {
            cod_bu: this.umBU
        });
        var params = {
            idPeriodo: this.periodo
        };
        model.tabellaSchedulingUM.checkIsToWork(params)
            .then(_.bind(function (res) {
                var commesseNonSchedulate = [];
                _.forEach(allCommesse, function (item) {
                    var commessaFind = _.find(res.results, {
                        COD_COMMESSA_PERIODO: item.codCommessa
                    });
                    if (commessaFind) {
                        if (commessaFind.UM_MODIFY) {
                            item.open = true;
                        } else {
                            item.open = false;
                        }
                    } else {
                        commesseNonSchedulate.push(item);
                    }
                })
                if (_.size(commesseNonSchedulate) > 0) {
                    _.forEach(commesseNonSchedulate, function (item) {
                        _.remove(allCommesse, item)
                    })
                }
                this.ProjectNotOpenModel.setData(allCommesse);
            }, this), _.bind(function (err) {
                sap.m.MessageToast.show("errore in lettura AT_UM_WORK_OTHER_PROJECT");
                this.oDialog.setBusy(false);
            }, this));
    },

    onPressOpenOtherProject: function (variabile) {
        var allCommesse = _.cloneDeep(this.allCommesseDonwload);
        _.remove(allCommesse, {
            cod_bu: this.umBU
        });

        if (!this.umSelectProjectDialog) {
            this.umSelectProjectDialog = sap.ui.xmlfragment("view.dialog.UM_selectProjectToOpenDialog", this);
            this.getView().addDependent(this.umSelectProjectDialog);
        }

        var params = {
            idPeriodo: this.periodo
        };
        model.tabellaSchedulingUM.checkIsToWork(params)
            .then(_.bind(function (res) {
                var commesseNonSchedulate = [];
                _.forEach(allCommesse, function (item) {
                    var commessaFind = _.find(res.results, {
                        COD_COMMESSA_PERIODO: item.codCommessa
                    });
                    if (commessaFind) {
                        if (commessaFind.UM_MODIFY) {
                            item.open = true;
                        } else {
                            item.open = false;
                        }
                    } else {
                        commesseNonSchedulate.push(item);
                    }
                })
                if (_.size(commesseNonSchedulate) > 0) {
                    _.forEach(commesseNonSchedulate, function (item) {
                        _.remove(allCommesse, item)
                    })
                }

                this.uiModel.setProperty("/searchBarDialogSelectProject", "");
                this.ProjectNotOpenModel.setData(allCommesse);
                this.umSelectProjectDialog.setModel(this.ProjectNotOpenModel);
                this.umSelectProjectDialog.getAggregation("content")[0].removeSelections();

                if (variabile !== "read")
                    this.umSelectProjectDialog.open();
            }, this), _.bind(function (err) {
                sap.m.MessageToast.show("errore in lettura AT_UM_WORK_OTHER_PROJECT");
                this.oDialog.setBusy(false);
            }, this));
    },

    afterOpenDialogSelectProject: function () {
        var list = this.umSelectProjectDialog.getAggregation("content")[0];
        var filters = [];
        var binding = list.getBinding("items");
        binding.filter(filters);
    },

    onSearchDialogProject: function (evt) {
        var value = evt.getSource().getValue();
        var list = this.umSelectProjectDialog.getAggregation("content")[0];
        var filters = [];
        var nameFilter = new sap.ui.model.Filter("descrizioneCommessa", sap.ui.model.FilterOperator.Contains, value);
        var codeFilter = new sap.ui.model.Filter("nomeCommessa", sap.ui.model.FilterOperator.Contains, value);
        filters.push(nameFilter);
        filters.push(codeFilter);
        filters = new sap.ui.model.Filter({
            filters: filters,
            and: false
        });
        var binding = list.getBinding("items");
        binding.filter(filters);
    },

    onPressCloseUmSelectOtherProject: function () {
        this.umSelectProjectDialog.close();
    },

    onPressOkUmSelectOtherProject: function () {
        var list = this.umSelectProjectDialog.getAggregation("content")[0];
        var path = list.getSelectedContextPaths()[0].substring(1, list.getSelectedContextPaths()[0].length);
        var element = list.getModel().getData()[path];

        if (element.open) {
            sap.m.MessageToast.show(this._getLocaleText("projectAlreadyOpened"));
            return;
        }

        var params = {
            codCommessa: element.codCommessa,
            idPeriodo: this.periodo,
            codUm: this.userModel.getData().codice_consulente
        };

        model.tabellaSchedulingUM.setIsToWorkByUm(params)
            .then(_.bind(function (res) {
                this.umSelectProjectDialog.close();
                this._checkIsToWork();
                sap.m.MessageToast.show(this._getLocaleText("projectOpened"));
            }, this), _.bind(function (err) {
                sap.m.MessageToast.show("errore in scrittura AT_UM_WORK_OTHER_PROJECT");
                this.oDialog.setBusy(false);
            }, this));

    },

    closeProject: function (evt) {
        var parameters = evt.getParameters().listItem;
        var path = parameters.getBindingContextPath().substring(1, parameters.getBindingContextPath().length);
        var commessa = parameters.getBindingContext().getModel().getData()[parseInt(path)];

        if (!commessa.open) {
            sap.m.MessageToast.show(this._getLocaleText("projectAlreadyClosed"));
            return;
        }

        var params = {
            codCommessa: commessa.codCommessa,
            idPeriodo: this.periodo,
            codUm: ''
        }

        model.tabellaSchedulingUM.setIsToWorkByUm(params)
            .then(_.bind(function (res) {
                this.umSelectProjectDialog.close();
                this._checkIsToWork();
                sap.m.MessageToast.show(this._getLocaleText("projectClosed"));
            }, this), _.bind(function (err) {
                sap.m.MessageToast.show("errore in scrittura AT_UM_WORK_OTHER_PROJECT");
                this.oDialog.setBusy(false);
            }, this));
    },

    selectCommessProposta: function (evt) {
        var nomeCommessaSelezionata = evt.getParameters().item.getParent().getParent().getProperty("text");
        this.commessaDaProporre = _.remove(this.commesseConflitto, {
            'nomeCommessa': nomeCommessaSelezionata
        })[0];
        sap.m.MessageToast.show(this._getLocaleText("selezionareUnGiornoDaProporre"), {
            at: "center center"
        });
        return;
    },

    // associazione consulente su consulente generico
    onPressAssignConsultant: function () {
        // lista consulenti generici del progetto
        var listaConsulentiGenerici = _.filter(this.umSchedulingModel.getProperty("/schedulazioni"), function (item) {
            if (item.codiceConsulente.indexOf("CCG") >= 0)
                return item
        });
        this.assignGenericConsultantModel.setProperty("/listaGenerici", listaConsulentiGenerici);
        this.uiModel.setProperty("/closeVisible", true);
        this.uiModel.setProperty("/backVisible", false);
        this.uiModel.setProperty("/forwardVisible", true);
        this.uiModel.setProperty("/associateVisible", false);
        this.uiModel.setProperty("/titleDialogAssociate", this._getLocaleText("LISTA_SKILL"));
        var page = this.getView().byId("schedulingPage");
        if (!this.dialogConsulentiGenerici) {
            this.dialogConsulentiGenerici = sap.ui.xmlfragment("view.dialog.GenericListConsultants", this);
            page.addDependent(this.dialogConsulentiGenerici);
        }

        this.dialogConsulentiGenerici.open();

        this.navContainer = sap.ui.getCore().getElementById("navContainerDialog");
        this.animation = "door"; // door, fade, slip, show, slide
        this.navContainer.to(sap.ui.getCore().getElementById("idPageListGeneric"));
    },

    onPressClose: function () {
        var list = sap.ui.getCore().getElementById("idListaGenerici");
        if (list.getSelectedItem()) {
            list.removeSelections();
        }
        this.dialogConsulentiGenerici.close();
    },

    onPressBack: function () {
        this.navContainer.back();
        this.uiModel.setProperty("/closeVisible", true);
        this.uiModel.setProperty("/backVisible", false);
        this.uiModel.setProperty("/forwardVisible", true);
        this.uiModel.setProperty("/associateVisible", false);
        this.uiModel.setProperty("/titleDialogAssociate", this._getLocaleText("LISTA_SKILL"));
    },

    onPressForward: function () {
        var list = sap.ui.getCore().getElementById("idListaGenerici");
        if (!list.getSelectedItem()) {
            sap.m.MessageToast.show(this._getLocaleText("SELECT_ONE_SKILL"));
            return;
        }
        var skill = list.getSelectedItem().getTitle();
        var codGenerico = list.getSelectedItem().getDescription();
        this.assignGenericConsultantModel.setProperty("/nomeSkill", skill);
        this.assignGenericConsultantModel.setProperty("/codiceConsulenteGenerico", codGenerico);
        this.uiModel.setProperty("/closeVisible", false);
        this.uiModel.setProperty("/backVisible", true);
        this.uiModel.setProperty("/forwardVisible", false);
        this.uiModel.setProperty("/associateVisible", true);
        this.uiModel.setProperty("/titleDialogAssociate", this._getLocaleText("SELECT_SINGLE_CONSULTANT"));
        var fSuccess = function (result) {
            var staff = _.cloneDeep(this.umSchedulingModel.getProperty("/schedulazioni"));
            _.remove(staff, function (item) {
                if (item.codiceConsulente.indexOf("CCG") >= 0)
                    return item
            });
            for (var i = 0; i < _.size(staff); i++) {
                _.remove(result, {
                    codConsulente: staff[i].codiceConsulente
                });
            }
            var restrictedConsultant = _.cloneDeep(result);
            restrictedConsultant = _.filter(restrictedConsultant, {
                nomeSkill: skill
            });
            this.assignGenericConsultantModel.setProperty("/restrictedConsultant", restrictedConsultant);
            this.assignGenericConsultantModel.setProperty("/allConsultant", _.uniq(result, 'codConsulente'));
            this.navContainer.to(sap.ui.getCore().getElementById("idPageListConsultants"));
            this.assignGenericConsultantModel.setProperty("/key", "restricted");
            var listConsulenti = sap.ui.getCore().getElementById("consultantPerGenerico");
            if (listConsulenti.getSelectedItem()) {
                listConsulenti.removeSelections();
            }
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'consulenteSkill'");
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Consultants.getConsultantFromSkill().then(fSuccess, fError);
    },

    onSelectSegmentedButton: function (evt) {
        var source = evt.getSource();
        var key = source.getSelectedKey();
        this.assignGenericConsultantModel.setProperty("/key", key);
    },

    onPressAssociate: function () {
        // update anagrafica
        var codConsGen = this.assignGenericConsultantModel.getProperty("/codiceConsulenteGenerico");
        var list;
        if (this.assignGenericConsultantModel.getProperty("/key") === "restricted") {
            list = sap.ui.getCore().getElementById("consultantPerGenerico");
        } else {
            list = sap.ui.getCore().getElementById("consultantPerGenericoAll");
        }
        if (!list.getSelectedItem()) {
            sap.m.MessageToast.show(this._getLocaleText("SELECT_ONE_CONSULTANT"));
            return;
        }
        var codCons = list.getSelectedItem().getDescription();
        var codCommessa = this.codCommessaSelezionata;

        model.UpdateAnagStaffGenerico.updateAnagStaff(codConsGen, codCons, codCommessa)
            .then(_.bind(function (result) {
                // update draft
                model.UpdateAnagStaffGenerico.updateDraft(codConsGen, codCons, codCommessa)
                    .then(_.bind(function (result) {
                        this.dialogConsulentiGenerici.close();
                        sap.m.MessageToast.show(this._getLocaleText("AGGIORNAMENTO_RIUSCITO"));
                        this.leggiTabella(this.ric);
                    }, this), _.bind(function (error) {
                        console.log(error);
                    }, this));
            }, this), _.bind(function (error) {
                console.log(error);
            }, this));
    },

    onPressCheckProjectsGeneric: function () {
        var params = {
            idPeriodo: this.periodo
        }
        model.tabellaSchedulingUM.getProjectListWithGenericConsultant(params)
            .then(_.bind(function (res) {
                this.listaCommesseConsulentiGenericiModel.setData(JSON.parse(res).results);

                if (!this.dialogListaCommesseConsulentiGenerici) {
                    this.dialogListaCommesseConsulentiGenerici = sap.ui.xmlfragment("view.dialog.listaCommesseConsulentiGenerici", this);
                    this.getView().addDependent(this.dialogListaCommesseConsulentiGenerici);
                }
                this.dialogListaCommesseConsulentiGenerici.open();
            }, this), _.bind(function (err) {
                // errore
                console.log(err)
            }, this));
    },

    onCloseDialogListaCommesseConsGen: function () {
        this.dialogListaCommesseConsulentiGenerici.close();
    },

    // funzione per cancellare la richiesta "non schedulabile"
    // MANCA ID COMMESSA 
    onPressOffisteWork: function (evt) {
        this.fromSaveLocal = false
        var model = evt.getSource()._getPropertiesToPropagate().oBindingContexts.pmSchedulingModel;
        var path = model.getPath();
        var jsonConsulente = model.getModel().getProperty(path);
        var btn = evt.getSource().getId();
        var col = _.find(btn.split("-"), function (o) {
            if (o.indexOf("col") !== -1) {
                return o
            }
        });
        col = col.substr(3, col.length);
        var req = "req" + col;
        var idReqOld = "idReqOld" + col;
        var idReqOriginal = "idReqOriginal" + col;
        var prop = "gg" + col;
        var propReq = prop.replace("gg", "req");
        var idCommessa = "idCommessa" + col;

        var dataToDel = {
            codConsulenteDraft: jsonConsulente.codiceConsulente,
            oldIdReqDraft: jsonConsulente[idReqOld] ? jsonConsulente[idReqOld] : jsonConsulente[propReq] ? 0 : 1, //(campo che decide quale richiesta effettuare)
            idReqOriginal: jsonConsulente[idReqOriginal] ? jsonConsulente[idReqOriginal] : 0,
            id_req: jsonConsulente[req],
            codPjmDraft: jsonConsulente.codicePJM,
            idPeriodoSchedulingDraft: this.periodo,
            idCommessaDraft: jsonConsulente[idCommessa],
            giornoPeriodoDraft: col
        }
        this._confirmDeleteOffsiteWork(dataToDel);
    },

    onPressOffisteWork2: function (evt) {
        this.fromSaveLocal = false
        var model = evt.getSource()._getPropertiesToPropagate().oBindingContexts.pmSchedulingModel;
        var path = model.getPath();
        var jsonConsulente = model.getModel().getProperty(path);
        var btn = evt.getSource().getId();
        var col = _.find(btn.split("-"), function (o) {
            if (o.indexOf("col") !== -1) {
                return o
            }
        });
        col = col.substr(3, col.length);
        var req = "req" + col + "a";
        var idCommessa = "idCommessa" + col + "a";
        var idReqOld = "idReqOld" + col + "a";
        var idReqOriginal = "idReqOriginal" + col + "a";
        var prop = "gg" + col + "a";
        var propReq = prop.replace("gg", "req");

        var dataToDel = {
            codConsulenteDraft: jsonConsulente.codiceConsulente,
            oldIdReqDraft: jsonConsulente[idReqOld] ? jsonConsulente[idReqOld] : jsonConsulente[propReq] ? 0 : 1, //(campo che decide quale richiesta effettuare)
            idReqOriginal: jsonConsulente[idReqOriginal] ? jsonConsulente[idReqOriginal] : 0,
            id_req: jsonConsulente[req],
            codPjmDraft: jsonConsulente.codicePJM,
            idPeriodoSchedulingDraft: this.periodo,
            idCommessaDraft: jsonConsulente[idCommessa],
            giornoPeriodoDraft: col
        }
        this._confirmDeleteOffsiteWork(dataToDel);
    },

    _confirmDeleteOffsiteWork: function (dataToDel) {
        if (!this.visibleModel.getProperty("/wipPeriod")) {
            return;
        }
        sap.m.MessageBox.show(this._getLocaleText("confermaEliminazioneOffsiteWork"), {
            icon: sap.m.MessageBox.Icon.ALLERT,
            title: this._getLocaleText("confermaEliminazioneOffsiteWorkTitle"),
            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            onClose: _.bind(function (action) {
                if (action === "YES") {
                    this._delData(dataToDel);
                } else {
                    return;
                }
            }, this),
        });
    },

    onPressConsulentiGenerici: function () {
        utils.Busy.show("Ricaricamento Dati Real Time");
        var fSuccess = function () {
            utils.Busy.hide();
            this.umDialog.close();
        }
        fSuccess = _.bind(fSuccess, this);

        var req = _.clone(this.ric);
        req.consulentiGenerici = "X";

        this.richiestaConFiltri = req;

        this.leggiTabella(req)
            .then(fSuccess);
    },

});