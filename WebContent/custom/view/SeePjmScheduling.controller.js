jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("model.tabellaSchedulingOnlyReadPJM");

view.abstract.AbstractController.extend("view.SeePjmScheduling", {
    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.pmSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingModel, "pmSchedulingModel");

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");

        this.noteModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteModel, "noteModel");

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");

        this.pModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pModel, "pModel");

        var eventBus = sap.ui.getCore().getEventBus();
        eventBus.subscribe("colorCells", "fireColorTableCells", this.colorCells, this);
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.userModel.setData(this.user);

        if (!this._checkRoute(evt, "seePjmScheduling")) {
            return;
        }
        
        var prefTitle = icms.Component.getMetadata().getConfig().settings.prefTitle;
        document.title = prefTitle + " _ UM ALL REQS";
        
        this.oDialog = this.getView().byId("BlockLayoutSeeAll");
        this.oDialogTable = this.getView().byId("idSchedulazioniTableAll");
        this.getView().byId("idSchedulazioniTableAll").setVisible(false);
        this.getView().byId("idSelectCommessaSeeAll").setEnabled(false);
        this.getView().byId("idSelectCommessaSeeAll").setSelectedKey("");
        this.getView().byId("idSelectCommessaSeeAll").setValue("");

        this.pmSchedulingModel.getData().giorniTotali = 0;

        this.visibleModel.setProperty("/chargeButton", false);
        
        this.umBU = model.persistence.Storage.session.get("user").codConsulenteBu;
        this.readPeriods();
    },

    // funzione per la navigazione alla lauchpad
    onLaunchpadPress: function () {
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.onNavBackPress();
    },

    // Funzione caricamento periodi
    readPeriods: function () {
        this._getPeriodsGlobal(undefined, "oDialog.setBusy", false);
    },

    // Funzione caricamento commesse
    readProjects: function (req) {
        var fSuccess = function (result) {
            _.remove(result.items, {
                'statoCommessa': "CHIUSO"
            });

            this.allCommesse = result.items;
            var comboCommessa = this.getView().byId("idSelectCommessaSeeAll");
            if (comboCommessa) {
                comboCommessa.setValue(comboCommessa.getDefaultSelectedItem());
            }

            var commesseOrdinate = _.sortByOrder(this.allCommesse, [function (o) {
                    if (o.codPrioritaA !== 0)
                        return o.codPrioritaA;
            }, function (o) {
                    if (o.codPrioritaB !== 0)
                        return o.codPrioritaB;
            }, function (o) {
                    return o.nomeCommessa.toLowerCase();
            }]);
            this.pmSchedulingModel.setSizeLimit(_.size(commesseOrdinate));
            this.pmSchedulingModel.setProperty("/commesse", []);
            this.pmSchedulingModel.setProperty("/commesse", commesseOrdinate);
            //this.oDialog.setBusy(false);
            utils.Busy.hide();
        };
        var fError = function () {
            sap.m.MessageToast.show("errore in lettura progetti");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.Projects.readProjectSeePjmScheduling(req)
            .then(fSuccess, fError);
    },

    // funzione che, al cambio del periodo, nasconde alcuni tasti, svuota la tabella e le select
    // successivamente popola la tabella con il nuovo periodo selezionato
    onChangePeriod: function (pe) {
        //this.oDialog.setBusy(true);
        utils.Busy.show("Caricamento Dati");
        this.visibleModel.setProperty("/closeProjectButton", false);
        this.pmSchedulingModel.setProperty("/giorniTotali", "0");
        this.svuotaSelect();
        this.svuotaTabella();

        var table = this.oDialogTable;

        this.pe = pe;
        var p = pe.getSource().getSelectedItem().getProperty("text").replace(/ /g, '');
        var period = pe.getSource().getProperty("selectedKey");
        model.persistence.Storage.session.save("period", period);
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(period)
        });
        model.persistence.Storage.session.save("periodSelected", periodSelect);
        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();

        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = pInizio;
        var dataF = pFine;

        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

        // ** rendo visibili solo le colonne del periodo selezionato
        var columns = table.getColumns();
        for (var i = 0; i < columns.length; i++) {
            columns[i].setVisible(true);
        }
        for (var i = this.numColonne; i < columns.length - 1; i++) {
            columns[i].setVisible(false);
        }
        // **

        // ** metto i nomi dei giorni sulle colonne
        for (var i = 2; i < columns.length; i++) {
            columns[i - 1].getLabel().mProperties.text = this.arrayGiorni[i - 2];
        }
        // **
        this.pmSchedulingModel.setProperty("/days", this.arrayGiorni);

        this.getView().byId("idSelectCommessaSeeAll").setEnabled(true);

        this.periodStauts = pe.getSource().getSelectedItem().getProperty("additionalText");

        jQuery.sap.delayedCall(500, this, function () {
            this.oDialog.setBusy(false);
        });
        this.visibleModel.setProperty("/chargeButton", false);
        
        this.readProjects(parseInt(period));
    },

    // svuoto la tabella
    svuotaTabella: function () {
        this.oDialogTable.setVisible(true);
        this.pmSchedulingModel.setProperty("/schedulazioni", []);
        this.pmSchedulingModel.setProperty("/nRighe", 1);
        this.pmSchedulingModel.getData().schedulazioni = [];
    },

    // svuoto le select
    svuotaSelect: function (value) {
        if (!value) {
            this.getView().byId("idSelectCommessaSeeAll").setEnabled(false);
            this.getView().byId("idSelectCommessaSeeAll").setSelectedKey("");
            this.getView().byId("idSelectCommessaSeeAll").setValue("");
        }
    },

    // selezionando una commessa faccio apparire il tasto di caricamento
    onSelectionChangeCommessa: function (evt) {
        this.svuotaTabella();
        this.visibleModel.setProperty("/chargeButton", true);
        this.chosedComName = evt.getSource().getValue().split(" - ")[0];
        this.chosedCom = evt.getSource().getSelectedKey();
    },

    // al premere del pulsante faccio nascondere lo stesso
    chargeProjects: function (evt) {
        this.visibleModel.setProperty("/chargeButton", false);
    },

    // funzione che si avvia in automatico premendo un qualsiasi punto dopo aver selezionato la commessa
    // vado a leggermi lo staffo della commessa selezionata
    onChangeCommessa: function (evt) {
        this.oDialog.setBusy(true);
        utils.Busy.show("Caricamento Dati");

        var commessaScelta = this.chosedCom;
        var fSuccess = function (res) {
            this.currentStaff = _.find(res.items, {
                codCommessa: commessaScelta
            });
            if (!this.currentStaff) {
                this.currentStaff = _.find(this.pmSchedulingModel.getData().commesse, {
                    codCommessa: commessaScelta
                });
            }
            this._onChangeCommessa(commessaScelta);
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore lettura XSJS 'letturaStaffCommessa.xsjs'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.StaffCommesseView.readStaffCommessa()
            .then(fSuccess, fError);
        this.visibleModel.setProperty("/chargeButton", false);
    },

    // in base al periodo e alla commessa selezionati setto tutti i parametri indispensabili
    // lancio al funzione di lettura schedulazioni per popolare la tabella
    _onChangeCommessa: function (commessaP) {
        this.allConsultantsForProject = undefined;
        this.filteredConsultants = undefined;
        var commessaScelta = commessaP;
        var c = _.find(this.pmSchedulingModel.getData().commesse, {
            codCommessa: commessaScelta
        });
        model.persistence.Storage.session.save("commessa", c);

        var req = {};
        req.idCommessa = commessaScelta;
        req.idStaff = this.currentStaff.codStaff ? this.currentStaff.codStaff : this.currentStaff.codiceStaffCommessa;
        if (req.idStaff) {
            req.cod_consulente = c.codPjmAssociato;
            req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
            this.ric = req;
            if (!this.currentStaff.codStaff) {
                sap.m.MessageToast.show("Non è presente alcun consulente per questo staff");
                return;
            }
        } else {
            this._enterErrorFunction("information", "Informazione", "Creare lo staff per questa commessa!!!");
            return;
        }

        this.leggiTabella(req);
        this.visibleModel.setProperty("/visibleTable", true);
        this.oDialog.setBusy(false);
    },

    // in base alla richiesta passata, popolo la tabella con i consulenti e le giornate schedulate
    leggiTabella: function (req) {
        this.oDialogTable.setBusy(true);
        var fSuccess = function (results) {
            var uniqueSuccess = function (res) {
                var schedulazioni = res;
                var schedOrdinato = _.sortBy(schedulazioni, 'nomeConsulente');
                this.pmSchedulingModel.setProperty("/schedulazioni", schedOrdinato);
                this.allScheduling = this.pmSchedulingModel.getProperty("/schedulazioni");
                if (schedOrdinato.length > 0 && schedOrdinato.length <= 7) {
                    this.pmSchedulingModel.setProperty("/nRighe", schedOrdinato.length);
                } else if (schedOrdinato.length > 7) {
                    this.pmSchedulingModel.setProperty("/nRighe", 7);
                } else {
                    this.pmSchedulingModel.setProperty("/nRighe", 1);
                }
                setTimeout(_.bind(this._onAfterRenderingTable, this));
            };

            var uniqueError = function (err) {
                this.oDialogTable.setBusy(false);
                this._enterErrorFunction("error", "Errore", err);
            };

            uniqueSuccess = _.bind(uniqueSuccess, this);
            uniqueError = _.bind(uniqueError, this);

            this._uniqueElement(results)
                .then(uniqueSuccess, uniqueError);
        };

        var fError = function (err) {
            console.log(err);
            this.oDialogTable.setBusy(false);
            this.visibleModel.setProperty("/closeProjectButton", false);
            this._enterErrorFunction("error", "Errore", "Errore lettura tabella: " + err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var fSuccessStaff = function (res) {
            model.tabellaSchedulingOnlyReadPJM.readTable(req, res, this.chosedComName, this.allCommesse)
                .then(fSuccess, fError);
        };

        var fErrorStaff = function (err) {
            this.oDialogTable.setBusy(false);
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'anagStaffViewSet'");
        };

        fSuccessStaff = _.bind(fSuccessStaff, this);
        fErrorStaff = _.bind(fErrorStaff, this);

        model.tabellaSchedulingOnlyReadPJM.readStaffPerCommessaPJM(req)
            .then(fSuccessStaff, fErrorStaff);
    },

    // funzione che mi construisce un array ordinato
    // legge gli skill dei consulenti estratti dalla lettura
    _uniqueElement: function (result) {
        var arrayfinale = result[0];
        var arrayGiornateSchedulate = result[0];

        var objCodiciConsulenti = {};
        for (var i = 0; i < arrayGiornateSchedulate.length; i++) {
            var c = "c" + i;
            objCodiciConsulenti[c] = arrayGiornateSchedulate[i].codiceConsulente;
        }
        var objCodiciConsulenti2 = {};
        for (var i = 0; i < arrayGiornateSchedulate.length; i++) {
            var c = "d" + i;
            objCodiciConsulenti2[c] = arrayGiornateSchedulate[i].codiceConsulente;
        }
        _.merge(objCodiciConsulenti, objCodiciConsulenti2);

        arrayfinale = [];
        for (var prop in objCodiciConsulenti) {
            var arr = _.where(arrayGiornateSchedulate, {
                codiceConsulente: objCodiciConsulenti[prop]
            });
            var obj = {};
            for (var i = 0; i < arr.length; i++) {
                obj.nomeConsulente = arr[i].nomeConsulente;
                obj.codiceConsulente = arr[i].codiceConsulente;
                obj.codicePJM = arr[i].codicePJM;
                var objProps = _.keys(arr[i]);
                var ggProp = _.find(objProps, function (item) {
                    return (item.indexOf("gg") > -1 && item.indexOf("Icon") < 0 && item.indexOf("Conflict") < 0 && item.indexOf("Req") < 0 && item.indexOf("Ins") < 0);
                });
                if (ggProp) {
                    var gg = ggProp.substring(2); //id del giorno nel periodo
                    // ggVal è il nome della proprietà "gg"+ la serie di "a" dipendente dalla quantità di attività in quello stesso giorno
                    var ggVal = ggProp;
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (obj[ggVal]) {
                        items = "a";
                        ggVal = ggVal + items;
                    }
                    obj[ggVal] = arr[i][ggProp];
                    obj["nota" + ggVal.substring(2)] = arr[i]["nota" + gg];
                    obj["req" + ggVal.substring(2)] = arr[i]["req" + gg];
                    obj["ggReq" + ggVal.substring(2)] = arr[i]["ggReq" + gg];
                    obj["ggIcon" + ggVal.substring(2)] = arr[i]["ggIcon" + gg];
                    obj["ggConflict" + ggVal.substring(2)] = arr[i]["ggConflict" + gg];
                }
            }
            if (!_.isEmpty(obj))
                arrayfinale.push(obj);
        }
        arrayfinale = _.reduce(arrayfinale, function (results, item) {
            return _.any(results, function (result) {
                return _.isEqual(result, item);
            }) ? results : results.concat([item]);
        }, []);

        var arrCodConsulenti = [];
        for (var aa = 0; aa < arrayfinale.length; aa++) {
            for (var prop in arrayfinale[aa]) {
                if (prop.indexOf("codiceConsulente") > -1) {
                    arrCodConsulenti.push((arrayfinale[aa])[prop]);
                }
            }
        }
        var defer = Q.defer();
        var fSuccess = function (result) {
            console.log(result.items);
            if (result.items.length === 0) {
                defer.resolve(arrayfinale);
            } else {
                for (var ii = 0; ii < arrayfinale.length; ii++) {
                    for (var jj = 0; jj < result.items.length; jj++) {
                        if (arrayfinale[ii].codiceConsulente === (result.items)[jj].codConsulente) {
                            if (!!arrayfinale[ii].nomeSkill) {
                                if (arrayfinale[ii].nomeSkill.trim().length > 0) {
                                    arrayfinale[ii].nomeSkill += ", " + (result.items)[jj].nomeSkill;
                                } else {
                                    arrayfinale[ii].nomeSkill = (result.items)[jj].nomeSkill;
                                }
                            } else {
                                arrayfinale[ii].nomeSkill = (result.items)[jj].nomeSkill;
                            }

                        }
                    }
                }
                defer.resolve(arrayfinale);
            }
        };
        var fError = function (err) {
            defer.reject("error skills:" + err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.Skill.getConsulenteSkillSerializedInBatch(arrCodConsulenti)
            .then(fSuccess, fError);
        return defer.promise;
    },

    // funzione lanciata alla fine di tutto il caricamento
    // colora le celle in grigetto dei sabati e domeniche
    // somma e setta le giornate totali per consulenti e totale complessivo
    _onAfterRenderingTable: function (evt) {
        if (!this.oDialogTable.getBusy()) {
            this.oDialogTable.setBusy(true);
        }
        var table = this.oDialogTable;
        var rows = table.getRows();
        var arrayCounter = [];
        for (var i = 0; i < rows.length; i++) {
            var cellsPerRows = rows[i].getAggregation("cells");
            var g = 0;
            for (var j = 1; j < cellsPerRows.length - 1; j++) {
                var cella = cellsPerRows[j];
                if (_.find(this.weekendDays, _.bind(function (item) {
                        return (item == j);
                    }, this))) {
                    var column = $("#" + cella.getDomRef().id).parent().parent();
                    column.css('background-color', '#e5e5e5');
                }
                var buttonInCell = cella.getAggregation("items")[0];
                var text1 = cella.getAggregation("items")[1];
                var text2 = cella.getAggregation("items")[2];
            }
        }

        var dati = this.pmSchedulingModel.getData();
        var schedulazioni = dati.schedulazioni;
        var commessaScelta = this.getView().byId("idSelectCommessaSeeAll").getSelectedItem().mProperties.text;
        for (var i = 0; i < schedulazioni.length; i++) {
            var giorniS = {
                "giornoS": arrayCounter[i]
            };
            if (this.visibleModel.getProperty("/enabledButton") === false) {
                schedulazioni[i].giorniScheduling = arrayCounter[i];
            } else {
                if (!schedulazioni[i].giorniScheduling) {
                    schedulazioni[i].giorniScheduling = 0;
                }
                this._calcoloGiornate(schedulazioni[i]);
            }
        }
        var schedulazioniRichieste = _.sortBy(_.filter(schedulazioni, function (sched) {return sched.giorniScheduling > 0}), 'nomeConsulente');
        var schedulazioniSenzaRichieste = _.sortBy(_.filter(schedulazioni, function (sched) {return sched.giorniScheduling == 0}), 'nomeConsulente');
        var schedOrdinato = schedulazioniRichieste.concat(schedulazioniSenzaRichieste);
        this.pmSchedulingModel.setProperty("/schedulazioni", schedOrdinato);
        this.oDialogTable.setBusy(false);
        utils.Busy.hide();
    },

    // funzione che calcola i giorni, e mezze giornate, della commessa in alto
    _calcoloGiornate: function (questoConsulente) {
        var giorniTemp = questoConsulente.giorniScheduling;
        var count = 0;
        for (var i = 1; i < 36; i++) {
            var propIn = "gg" + i;
            var propInIcon = "ggIcon" + i;
            var propInA = "gg" + i + "a";
            var propInIconA = "ggIcon" + i + "a";
            var commessaSelezionata = this.getView().byId("idSelectCommessaSeeAll").getValue().split(" - ")[0];
            if (questoConsulente[propIn] && questoConsulente[propInIcon] === 'sap-icon://delete') {
                continue;
            }
            if (questoConsulente[propIn] === commessaSelezionata || questoConsulente[propInA] === commessaSelezionata) {
                if (questoConsulente[propInIcon] === "sap-icon://time-entry-request" || questoConsulente[propInIconA] === "sap-icon://time-entry-request") {
                    count = count + 0.5;
                } else if (questoConsulente[propInIcon] !== "sap-icon://offsite-work" && questoConsulente[propInIconA] !== "sap-icon://offsite-work") {
                    count = count + 1;
                }
            }
        }
        questoConsulente.giorniScheduling = count;
        var totalCount = 0;
        for (var i = 0; i < this.pmSchedulingModel.getData().schedulazioni.length; i++) {
            totalCount = totalCount + this.pmSchedulingModel.getData().schedulazioni[i].giorniScheduling;
        }
        this.pmSchedulingModel.getData().giorniTotali = totalCount;
        this.pmSchedulingModel.refresh();
    },

   // funzione per la modifica delle note
    onNotePress: function (evt) {
        var nota = evt.getSource().data("note");
        var jobName = evt.getSource().data("job");
        var job = jobName === "feri" ? "AA00000000" : _.find(this.getView().getModel("pmSchedulingModel").getProperty("/commesse"), {
            nomeCommessa: jobName
        });
        var consultant = evt.getSource().getBindingContext("pmSchedulingModel").getObject();
        this.notaConsultant = consultant[nota];
        this.noteModel.setProperty("/note", consultant[nota]);
        var codReq = nota.substring(4);
        var propReq = "req" + codReq;

        this.dataForUpdate = {
            "codConsulente": consultant.codiceConsulente,
            "dataReqPeriodo": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "idCommessa": job.codCommessa,
            "codAttivitaRichiesta": "",
            "idPeriodoSchedulingRic": this.getView().byId("idSelectPeriodo").getSelectedKey(),
            "codPjm": this.user.codice_consulente,
            "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "note": "",
            "statoReq": "CONFIRMED",
            "giorno": codReq.indexOf("a") >= 0 ? parseInt(codReq.substr(0, codReq.length - 1)) : parseInt(codReq),
            "id_req": consultant[propReq] ? consultant[propReq].toString() : ""
        };
        this.noteModel.setProperty("/editable", false);
        this.onAddNote();
    },

    // funzione che apre il dialog delle note 'noteDialog'
    onAddNote: function () {
        if (!this._noteDialog) {
            this._noteDialog = sap.ui.xmlfragment(
                "view.dialog.noteDialog",
                this
            );
            this.getView().addDependent(this._noteDialog);
        }
        this._noteDialog.open();
    },

    // funzione che viene lanciata alla chiusura del dialog delle note
    onAfterCloseNoteDialog: function () {
        this.noteModel.setProperty("/note", undefined);
    },

    // funzione che chiude il dialog delle note
    onCancelNoteDialogPress: function (evt) {
        if (this._noteDialog) {
            this._noteDialog.close();
        }
    },
});