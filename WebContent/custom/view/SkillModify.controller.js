jQuery.sap.require("utils.Formatter");

jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("model.Skill");

view.abstract.AbstractController.extend("view.SkillModify", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.detailSkillModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailSkillModel, "detailSkillModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "skillModify") {
            return;
        }

        this.oDialog = this.getView().byId("idSkillModify");

        this.state = evt.getParameters().arguments.state;

        if (this.state === "new") {
            this.detailSkillModel.setProperty("/codSkill", "");
            this.detailSkillModel.setProperty("/nomeSkill", "");
            this.detailSkillModel.setProperty("/descrizioneSkill", "");
        } else {
            var skill = JSON.parse(sessionStorage.getItem("selectedSkill"));
            this.detailSkillModel.setProperty("/codSkill", skill.codSkill);
            this.detailSkillModel.setProperty("/nomeSkill", skill.nomeSkill);
            this.detailSkillModel.setProperty("/descrizioneSkill", skill.descSkill);
        }
    },

    cancelModifyPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_CANCEL_MODIFY_SKILL"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_CANCEL_MODIFY_TITLE_SKILL"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmCancelPress, this)
        );
    },

    saveSkillPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_SAVE_SKILL"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_SAVE_TITLE_SKILL"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmSavePress, this)
        );
    },

    _confirmCancelPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            this._clearInputValue();
            this.router.navTo("skillList");
        };
    },

    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var fSuccess = function () {
                this._clearInputValue();
                this.oDialog.setBusy(false);
                this.router.navTo("skillList");
            };
            var fError = function (err) {
                sap.m.MessageBox.alert(err, {
                    title: this._getLocaleText("error")
                });
                this.oDialog.setBusy(false);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            var nomeSkill = this.detailSkillModel.getProperty("/nomeSkill");
            var descrizioneSkill = this.detailSkillModel.getProperty("/descrizioneSkill");
            this.oDialog.setBusy(true);
            if (this.state === "new") {
                model.Skill.addSkill(nomeSkill, descrizioneSkill)
                    .then(fSuccess, fError);
            } else {
                var codSkill = this.detailSkillModel.getProperty("/codSkill");
                model.Skill.updateSkill(codSkill, nomeSkill, descrizioneSkill)
                    .then(fSuccess, fError);
            };
        };
    },

    _clearInputValue: function () {
        this.detailSkillModel.setProperty("/codSkill", "");
        this.detailSkillModel.setProperty("/nomeSkill", "");
        this.detailSkillModel.setProperty("/descrizioneSkill", "");
    }

});
