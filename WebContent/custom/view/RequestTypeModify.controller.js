jQuery.sap.require("utils.Formatter");

jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("model.RequestTypes");

view.abstract.AbstractController.extend("view.RequestTypeModify", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.detailTipoRichiesta = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailTipoRichiesta, "detailTipoRichiesta");

    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "requestTypeModify") {
            return;
        };

        this.oDialog = this.getView().byId("BusyDialog");
        this.state = evt.getParameters().arguments.state;

        if (this.state === "new") {
            this.detailTipoRichiesta.setProperty("/codiceTipoRichiesta", "");
            this.detailTipoRichiesta.setProperty("/descrizioneTipoRichiesta", "");
        } else {
            var tipoRichiesta = JSON.parse(sessionStorage.getItem("selectedRequestType"));
            this.detailTipoRichiesta.setProperty("/codiceTipoRichiesta", tipoRichiesta.codiceTipoRichiesta);
            this.detailTipoRichiesta.setProperty("/descrizioneTipoRichiesta", tipoRichiesta.descrizioneTipoRichiesta);
        };
    },

    setValori: function () {
        this.getView().byId("codPjmAssociato").setProperty("value", this.detailTipoRichiesta.getProperty("/cognomeNome"))
        this.getView().byId("statoTipoRichiesta").getProperty("value", this.detailTipoRichiesta.getProperty("/statoTipoRichiesta"))
    },


    modifyPress: function (evt) {
        var ff = evt.getSource();
        var indice = ff.getId().slice(ff.getId().length - 1, ff.getId().length);

        var table = this.getView().byId("idCommesseTable");
        var tipoRichiestaSel = this.modelCommesse.getData().commesse[indice];

        var fSuccess = function () {
            this.readProjects();
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            console.log("errore in lettura progetti");
            this.oDialog.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.PeriodsGabio.updateProject(tipoRichiestaSel.codTipoRichiesta, tipoRichiestaSel.nomeTipoRichiesta, tipoRichiestaSel.descrizioneTipoRichiesta, tipoRichiestaSel.codPjmAssociato, tipoRichiestaSel.codiceStaffTipoRichiesta, tipoRichiestaSel.statoTipoRichiesta).then(fSuccess, fError);

        console.log(tipoRichiestaSel)
    },

    cancelModifyPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("confirmCancelRequestType"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("confirmCancelTitleRequestType"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmCancelPress, this)
        );
    },

    saveProjectPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("confirmSaveRequestType"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("confirmSaveTitleRequestType"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmSavePress, this)
        );
    },

    _confirmCancelPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            this._clearInputValue();
            this.router.navTo("confirmCancelRequestType");
        };
    },

    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {

            var fSuccess = function () {
                this._clearInputValue();
                this.oDialog.setBusy(false);
                this.router.navTo("requestTypeList");
            };
            var fError = function () {
                console.log("errore nel salvataggio");
                this.oDialog.setBusy(false);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            
            this.oDialog.setBusy(true);
            var descrizioneTipoRichiesta = this.getView().byId("idDescrizioneTipoRichiesta").getValue();
            if (this.state === "new") {
                model.RequestTypes.addRequestType(descrizioneTipoRichiesta)
                    .then(fSuccess, fError);
            } else {
              var codiceTipoRichiesta = this.getView().byId("idCodiceTipoRichiesta").getValue();
              model.RequestTypes.updateRequestType(codiceTipoRichiesta, descrizioneTipoRichiesta)
                    .then(fSuccess, fError);
            };
        };
    },

    _clearInputValue: function () {
        this.getView().byId("idCodiceTipoRichiesta").setValue("");
        this.getView().byId("idDescrizioneTipoRichiesta").setValue("");
    }
});
