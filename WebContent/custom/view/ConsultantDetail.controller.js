jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.Consultants");
jQuery.sap.require("model.UserLogin");
jQuery.sap.require("model.Skill");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");

view.abstract.AbstractController.extend("view.ConsultantDetail", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.detailConsulente = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailConsulente, "detailConsulente");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "consultantDetail") {
            return;
        };
        this.oDialog = this.getView().byId("idConsultantDetail");

        var fError = function (err) {
            this.oDialog.setBusy(false);
            this.console(JSON.parse(err.response.body).error.message.value, "error");
        };

        var fSuccessConsulenteSkills = function (result) {
            var arraySkillSelected = [];
            var primarySkill = {};
            var otherSkills = [];
            for (var i = 0; i < result.results.length; i++) {
                arraySkillSelected.push(result.results[i].COD_SKILL);
                if (result.results[i].SKILL_PRINCIPALE && result.results[i].SKILL_PRINCIPALE.length > 0) {
                    primarySkill = {
                        codSkill: result.results[i].COD_SKILL,
                        skillName: result.results[i].NOME_SKILL
                    }
                } else {
                    otherSkills.push(result.results[i].COD_SKILL);
                }
            };
            this.detailConsulente.setProperty("/allSkills", arraySkillSelected);
            this.detailConsulente.setProperty("/codSkillSelected", otherSkills);
            this.detailConsulente.setProperty("/primarySkill", primarySkill);
            
            model.Consultants.checkIfConsultantIsRequired(this.detailConsulente.getProperty("/codConsulente"), _.bind(function (succ) {
            		var richieste = JSON.parse(succ).find;
            		if (richieste) {
            			this.detailConsulente.setProperty("/buttonDelete", false);
            		} else {
            			this.detailConsulente.setProperty("/buttonDelete", true);
            		}
            	}, this), _.bind(function (err) {}, this));
            
            this.oDialog.setBusy(false);
        };

        var fSuccessSkills = function (result) {
            var consulente = JSON.parse(sessionStorage.getItem("selectedConsultant"));
            this.detailConsulente.setProperty("/codConsulente", consulente.codConsulente);
            this.detailConsulente.setProperty("/cognomeConsulente", consulente.cognomeConsulente);
            this.detailConsulente.setProperty("/nomeConsulente", consulente.nomeConsulente);
            this.detailConsulente.setProperty("/codProfiloConsulente", consulente.codProfiloConsulente);
            this.detailConsulente.setProperty("/tipoAzienda", consulente.tipoAzienda);
            this.detailConsulente.setProperty("/note", consulente.note);
            this.detailConsulente.setProperty("/descrizioneProfiloConsulente", consulente.descrizioneProfiloConsulente);
            this.detailConsulente.setProperty("/codConsulenteBu", consulente.codConsulenteBu);
            this.detailConsulente.setProperty("/descBu", consulente.descBu);
            this.detailConsulente.setProperty("/statoConsulente", consulente.stato);
            this.detailConsulente.setProperty("/intercompany", consulente.intercompany);
            this.detailConsulente.setProperty("/tariffaIntercompany", consulente.tariffaIntercompany);
            this.detailConsulente.setProperty("/ggIntercompany", consulente.ggIntercompany);
            this.detailConsulente.setProperty("/partTime", consulente.partTime);
            this.detailConsulente.setProperty("/orePartTime", consulente.orePartTime);
            var arraySkills = [];
            for (var i = 0; i < result.items.length; i++) {
                arraySkills.push(result.items[i]);
            };
            this.detailConsulente.setProperty("/skills", arraySkills);
            model.Skill.getConsulenteSkill(consulente.codConsulente)
                .then(fSuccessConsulenteSkills, fError);
        };

        fSuccessConsulenteSkills = _.bind(fSuccessConsulenteSkills, this);
        fSuccessSkills = _.bind(fSuccessSkills, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.Skill.readSkills()
            .then(fSuccessSkills, fError);
    },

    modifyConsultantPress: function () {
        this.router.navTo("consultantModify", {
            state: "modify",
            detailId: this.detailConsulente.getProperty("/codConsulente")
        });
    },

    deleteConsultantPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_DELETE_CONSULTANT"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_DELETE_TITLE_CONSULTANT"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmDeletePress, this)
        );
    },

    _confirmDeletePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            var fSuccess = function () {
            	model.UserLogin.deleteByCodConsulente(this.detailConsulente.getProperty("/codConsulente"), _.bind(function (succ) {
            		this.oDialog.setBusy(false);
                    this.router.navTo("consultantList");
            	}, this), _.bind(function (err) {
            		this.oDialog.setBusy(false);
                    this.router.navTo("consultantList");
            	}, this));
            }
            var fError = function () {
                this.oDialog.setBusy(false);
                console.log("errore nel salvataggio");
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            model.Consultants.deleteConsultant(this.detailConsulente.getProperty("/codConsulente"))
                .then(fSuccess, fError);
        };
    },

    onSkillChange: function (evt) {
        var fSuccess = function (result) {
            this.oDialog.setBusy(false);
            if (JSON.parse(result).status !== "OK") {
                sap.m.MessageBox.alert(result, {
                    title: this._getLocaleText("errore salvataggio skill")
                });
            };
        };
        var fError = function (result) {
            this.oDialog.setBusy(false);
            sap.m.MessageBox.alert(result, {
                title: this._getLocaleText("errore salvataggio skill")
            });
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        var skillToAdd = evt.getParameters().selected;
        var codSkillSelected = evt.getParameters().changedItem.mProperties.key;
        this.oDialog.setBusy(true);
        if (skillToAdd) {
            model.Skill.addSkillToConsultant(this.detailConsulente.getData().codConsulente, codSkillSelected)
                .then(fSuccess, fError);
        } else {
            model.Skill.deleteSkillToConsultant(this.detailConsulente.getData().codConsulente, codSkillSelected)
                .then(fSuccess, fError);
        };
    }
});
