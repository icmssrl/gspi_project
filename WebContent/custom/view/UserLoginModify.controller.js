jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("model._ConsultantsStatus");
jQuery.sap.require("model.UserLogin");
jQuery.sap.require("model.Consultants");

view.abstract.AbstractController.extend("view.UserLoginModify", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.detailUserLog = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailUserLog, "detailUserLog");

        this.statoConsulente = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.statoConsulente, "statoConsulente");

        this.editModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.editModel, "userDetail");

        this.addConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.addConsultantModel, "cModel");

        this.adminType = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.adminType, "adminType");

        this.uiModel.setProperty("/searchProperty", ["cognomeConsulente"]);
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "userLoginModify") {
            return;
        }

        this.oDialog = this.getView().byId("idUserLoginModify");

        this.state = evt.getParameters().arguments.state;

        this._userLogin = model.persistence.Storage.session.get("user");
        this.consultantType = this._userLogin.descrizioneProfiloConsulente;
        if (this.consultantType !== "SuperUser") {
            this.adminType.setProperty("/admin", false);
            this.adminType.setProperty("/showPsw", true);
            this.adminType.setProperty("/resetPsw", false);
        } else {
            this.adminType.setProperty("/admin", true);
            this.adminType.setProperty("/showPsw", false);
            this.adminType.setProperty("/resetPsw", true);
        }

        if (this.state === "new") {
            this.detailUserLog.setProperty("/alias", "");
            this.detailUserLog.setProperty("/nomeConsulente", "");
            this.detailUserLog.setProperty("/cognomeConsulente", "");
            this.detailUserLog.setProperty("/password", "");
            this.detailUserLog.setProperty("/stato", "");
            this.adminType.setProperty("/addFromConsultant", true);
        } else {
            var userLog = JSON.parse(sessionStorage.getItem("selectedConsultantLogin"));
            this.detailUserLog.setProperty("/alias", userLog.alias);
            this.detailUserLog.setProperty("/nomeConsulente", userLog.nomeConsulente);
            this.detailUserLog.setProperty("/cognomeConsulente", userLog.cognomeConsulente);
            this.detailUserLog.setProperty("/password", atob(userLog.password));
            this.detailUserLog.setProperty("/stato", userLog.stato);
            this.adminType.setProperty("/addFromConsultant", false);
            if (userLog.cognomeConsulente === this._userLogin.cognomeConsulente && userLog.nomeConsulente === this._userLogin.nomeConsulente) {
                this.adminType.setProperty("/showPsw", true);
            }
        }
        this.editModel.setProperty("/showPass", "Password");

        var fErrorStatus = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageBox.alert(err, {
                title: this._getLocaleText("errore stauts list")
            });
        };

        var fSuccessStatus = function (result) {
            this.statoConsulente.setProperty("/status", result);
            var buComboBox = this.getView().byId("idStatoConsulente");
            buComboBox.setValue(this.detailUserLog.getData().stato);
        };

        fSuccessStatus = _.bind(fSuccessStatus, this);
        fErrorStatus = _.bind(fErrorStatus, this);

        model._ConsultantsStatus.getStauts()
            .then(fSuccessStatus, fErrorStatus);


    },

    cancelModifyPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_CANCEL_MODIFY_USER"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_CANCEL_MODIFY_TITLE_USER"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmCancelPress, this)
        );
    },

    saveProjectPress: function (psw) {
        var title = "";
        var text = "";
        if (psw){
            title = this._getLocaleText("SAVE_NEW_PASSWORD_TITLE");
            text = this._getLocaleText("SAVE_NEW_PASSWORD");
        } else {
            title = this._getLocaleText("CONFIRM_SAVE_TITLE_USER");
            text = this._getLocaleText("CONFIRM_SAVE_USER");
        }
        sap.m.MessageBox.show(text,
            sap.m.MessageBox.Icon.ALLERT,
            title, [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmSavePress, this)
        );
    },

    _confirmCancelPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            this._clearInputValue();
            this.router.navTo("userLoginList");
        }
    },

    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var fError = function (err) {
                console.log("errore nel salvataggio");
                this.oDialog.setBusy(false);
            };

            var fSuccessAddUserLogin = function (result) {
                this._clearInputValue();
                this.oDialog.setBusy(false);
                this.router.navTo("userLoginList");
            };

            fSuccessAddUserLogin = _.bind(fSuccessAddUserLogin, this);
            fError = _.bind(fError, this);

            var alias = this.getView().byId("idAlias").getValue();
            var cognomeConsulente = this.getView().byId("idCognomeConsulente").getValue();
            var nomeConsulente = this.getView().byId("idNomeConsulente").getValue();
            var password = btoa(this.getView().byId("idPassword").getValue());
            var stato = this.getView().byId("idStatoConsulente").getValue();

            this.oDialog.setBusy(true);
            if (this.state === "new") {
                model.UserLogin.add(alias, cognomeConsulente, nomeConsulente, password, stato)
                    .then(fSuccessAddUserLogin, fError);
            } else {
                var userLog = JSON.parse(sessionStorage.getItem("selectedConsultantLogin"));
                var alias_OR = userLog.alias;
                model.UserLogin.update(alias, cognomeConsulente, nomeConsulente, password, stato, alias_OR)
                    .then(fSuccessAddUserLogin, fError);
            }
        }
    },

    _clearInputValue: function () {
        this.getView().byId("idAlias").setValue("");
        this.getView().byId("idCognomeConsulente").setValue("");
        this.getView().byId("idNomeConsulente").setValue("");
        this.getView().byId("idPassword").setValue("");
        this.getView().byId("idStatoConsulente").setValue("");
    },

    showPass: function () {
        var typePSW = this.getView().byId("idPassword").getType();
        if (typePSW === "Text") {
            this.editModel.setProperty("/showPass", "Password");
        } else {
            this.editModel.setProperty("/showPass", "Text");
        }
    },

    liveName: function () {
        var name = this.getView().byId("idNomeConsulente").getValue();
        this.detailUserLog.setProperty("/nomeConsulente", name);
        var surname = this.getView().byId("idCognomeConsulente").getValue();
        var initName = name.slice(0, 1);
        if (surname.indexOf(" ") !== -1) {
            var int = surname.split(" ").length;
            for (var i = 1; i < int; i++) {
                surname = surname.replace(" ", "");
            }
        }
        var username = initName.concat(surname);
        this.detailUserLog.setProperty("/alias", username.toUpperCase());
    },

    liveSurname: function () {
        var name = this.getView().byId("idNomeConsulente").getValue();
        var surname = this.getView().byId("idCognomeConsulente").getValue();
        this.detailUserLog.setProperty("/cognomeConsulente", surname);
        if (surname.indexOf(" ") !== -1) {
            var int = surname.split(" ").length;
            for (var i = 1; i < int; i++) {
                surname = surname.replace(" ", "");
            }
        }
        var initName = name.slice(0, 1);
        var username = initName.concat(surname);
        this.detailUserLog.setProperty("/alias", username.toUpperCase());
    },

    addFromConsultat: function () {
        var page = this.getView().byId("idPageUserLoginModify");
        this.consultantDialog = sap.ui.xmlfragment("view.dialog.addConsultantForLoginDialog", this);

        page.addDependent(this.consultantDialog);

        var fSuccess = function (result) {
            this.consultantDialog.open();
        };
        var fError = function (err) {
            sap.m.MessageToast("error BU");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.loadConsultants()
            .then(fSuccess, fError);
    },

    loadConsultants: function (params) {
        var defer = Q.defer();
        var fSuccess = function (result) {
            var consulentiPresenti = JSON.parse(sessionStorage.getItem("listaConsulentiLogin"));
            if (consulentiPresenti && consulentiPresenti.length > 0) {
                for (var i = 0; i < consulentiPresenti.length; i++) {
                    _.remove(result.items, {
                        cognomeConsulente: consulentiPresenti[i].cognomeConsulente,
                        nomeConsulente: consulentiPresenti[i].nomeConsulente
                    });
                }
            }
            this.addConsultantModel.setData(result);
            defer.resolve();
        };

        var fError = function (err) {
            sap.m.MessageToast("User not added.");
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Consultants.readConsultants()
            .then(fSuccess, fError);

        return defer.promise;
    },

    filterPress: function (evt) {
        var item = evt.getSource();
        var codCon = item.getProperty("description");
        var user = _.find(this.addConsultantModel.getData().items, {
            'codConsulente': codCon
        });

        this.detailUserLog.setProperty("/cognomeConsulente", user.cognomeConsulente);
        this.detailUserLog.setProperty("/nomeConsulente", user.nomeConsulente);

        var initName = user.nomeConsulente.slice(0, 1);
        var alias = initName.concat(this._checkAccenti(user.cognomeConsulente));
        this.detailUserLog.setProperty("/alias", alias.toUpperCase());
        this.consultantDialog.close();
        this.consultantDialog.destroy(true);
    },

    onFilterDialogClose: function () {
        this.consultantDialog.close();
        this.consultantDialog.destroy(true);
    },

    onSearchConsultant: function (oEvent) {
        var value = oEvent.getSource().getValue();
        var params = ["nomeConsulente", "cognomeConsulente", "codConsulente"];
        var filtersArr = [];
        for (var i = 0; i < params.length; i++) {
            filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
        }
        var oFilter = new sap.ui.model.Filter({
            filters: filtersArr,
            and: false
        });
        var ff = sap.ui.getCore().getElementById("listConsultantLogin");
        var oBinding = ff.getBinding("items");
        oBinding.filter([oFilter]);
    },

    resetPsw: function () {
        this.getView().byId("idPassword").setValue((this.getView().byId("idAlias").getValue()).toLowerCase());
        if (this.state !== "new") {
            this.saveProjectPress("pswRest");
        }
    },

});