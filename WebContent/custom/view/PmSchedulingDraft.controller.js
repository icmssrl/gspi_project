jQuery.sap.require("view.abstract.AbstractController");

view.abstract.AbstractController.extend("view.PmSchedulingDraft", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.pmSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingModel, "pmSchedulingModel");

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");

        this.noteModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteModel, "noteModel");

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");

        this.pModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pModel, "pModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.userModel.setData(this.user);

        if (!this._checkRoute(evt, "pmSchedulingDraft")) {
            return;
        }

        this.oDialog = this.getView().byId("BlockLayout");
        this.oDialogTable = this.getView().byId("idPjmSchedulazioniTableDraft");
        this.getView().byId("idPjmSchedulazioniTableDraft").setVisible(false);
        this.getView().byId("idSelectCommessa").setEnabled(false);
        this.visibleModel.setProperty("/chargeButton", false);
        this.visibleModel.setProperty("/filterButton", false);
        this.getView().byId("idSelectCommessa").setSelectedKey("");
        this.getView().byId("idSelectCommessa").setValue("");
        this.readPeriods();

        this.pmSchedulingModel.getData().giorniTotali = 0;
        this.oneTimeReadPeriods = false;
    },

    // Funzione caricamento periodi
    readPeriods: function () {
        var fSuccess = function (result) {
            var res = {};
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for (var i = 0; i < 13; i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.periodModel.setData(res13);
            this.console("Periods: " + res.items.length, "success");
            this.console(res.items);
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");
            var req = {};
            req.codPjm = this.user.codice_consulente;
            req.stato = "APERTO";
            this.readProjects(req);
        };
        var fError = function () {
            if (!this.oneTimeReadPeriods) {
                // funzione che ripete la chiamata di login nel caso in cui sia scaduta l'autenticazione al sistema
                this.oneTimeReadPeriods = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(this.readPeriods(), this));
            } else {
                this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'getPeriods'");
            }
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    // Funzione caricamento commesse
    readProjects: function (req) {
        var fSuccess = function (result) {
            this.allCommesse = result.items;
            result.items = _.uniq(result.items, 'codCommessa');
            this.pmSchedulingModel.setProperty("/commesse", []);
            var commesseOrdinate = _.sortByOrder(result.items, [function (o) {
                return o.nomeCommessa.toLowerCase();
            }]);
            this.pmSchedulingModel.setProperty("/commesse", commesseOrdinate);
            this.console("Projects: " + commesseOrdinate.length, "success");
            this.console(commesseOrdinate);
            var comboCommessa = this.getView().byId("idSelectCommessa");
            if (comboCommessa) {
                comboCommessa.setValue(comboCommessa.getDefaultSelectedItem());
            }
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'projectsViewWithoutPriority'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Projects.readProjectsPerPJM(req)
            .then(fSuccess, fError);
    },

    // funzione che, al cambio del periodo, nasconde alcuni tasti, svuota la tabella e le select
    // successivamente popola la tabella con il nuovo periodo selezionato
    onChangePeriod: function (pe) {
        this.oDialog.setBusy(true);
        this.visibleModel.setProperty("/filterButton", false);
        this.pmSchedulingModel.setProperty("/giorniTotali", "0");
        this.visibleModel.setProperty("/filterButton", false);
        this.svuotaSelect();
        this.svuotaTabella();

        var table = this.oDialogTable;

        this.pe = pe;
        var p = pe.getSource().getSelectedItem().getProperty("text").replace(/ /g, '');
        var period = pe.getSource().getProperty("selectedKey");
        model.persistence.Storage.session.save("period", period);
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(period)
        });
        model.persistence.Storage.session.save("periodSelected", periodSelect);

        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();

        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = pInizio;
        var dataF = pFine;

        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

        // ** rendo visibili solo le colonne del periodo selezionato
        var columns = table.getColumns();
        for (var i = 0; i < columns.length; i++) {
            columns[i].setVisible(true);
        }
        for (var i = this.numColonne; i < columns.length - 1; i++) {
            columns[i].setVisible(false);
        }
        // **

        // ** metto i nomi dei giorni sulle colonne
        for (var i = 2; i < columns.length; i++) {
            columns[i - 1].getLabel().mProperties.text = this.arrayGiorni[i - 2];
        }
        // **
        this.pmSchedulingModel.setProperty("/days", this.arrayGiorni);

        this.getView().byId("idSelectCommessa").setEnabled(true);

        this.periodStauts = pe.getSource().getSelectedItem().getProperty("additionalText");

        jQuery.sap.delayedCall(500, this, function () {
            this.oDialog.setBusy(false);
        });
    },

    // **svuoto la tabella
    svuotaTabella: function () {
        // **svuoto la tabella
        this.getView().byId("idPjmSchedulazioniTableDraft").setVisible(true);
        this.pmSchedulingModel.setProperty("/schedulazioni", [])
        this.pmSchedulingModel.setProperty("/nRighe", 1);
        this.pmSchedulingModel.getData().schedulazioni = [];
    },

    // **svuoto le select
    svuotaSelect: function (value) {
        if (!value) {
            this.getView().byId("idSelectCommessa").setEnabled(false);
            this.getView().byId("idSelectCommessa").setSelectedKey("");
            this.getView().byId("idSelectCommessa").setValue("");
        }
    },

    // selezionando una commessa faccio apparire il tasto di caricamento
    onSelectionChangeCommessa: function (evt) {
        this.svuotaTabella();
        this.visibleModel.setProperty("/chargeButton", true);
        this.chosedCom = evt.getSource().getValue();
        this.pmSchedulingModel.setProperty("/commessa", this.chosedCom);
        this.pmSchedulingModel.setProperty("/giorniTotali", 0);
        this.visibleModel.setProperty("/filterButton", false);
    },

    // al premere del pulsante faccio nascondere lo stesso
    chargeProjects: function (evt) {
        this.visibleModel.setProperty("/chargeButton", false);
    },

    // funzione che si avvia in automatico premendo un qualsiasi punto dopo aver selezionato la commessa
    // vado a leggermi lo staffo della commessa selezionata
    onChangeCommessa: function (evt) {
        this.oDialog.setBusy(true);
        var comFind = _.find(this.pmSchedulingModel.getData().commesse, {
            nomeCommessa: this.chosedCom
        });
        if (comFind) {
            var commessaScelta = comFind.codCommessa;
            this.codCommessaSelezionata = comFind.codCommessa;
        } else {
            this._enterErrorFunction("alert", "Attenzione", "Commessa '" + this.chosedCom.toUpperCase() + "' non trovata");
            return;
        }
        var fSuccess = function (res) {
            this.currentStaff = _.find(res.items, {
                codCommessa: commessaScelta
            });
            if (!this.currentStaff) {
                this.currentStaff = _.find(this.pmSchedulingModel.getData().commesse, {
                    codCommessa: commessaScelta
                });
            }
            this.console("Staff commessa:", "success");
            this.console(this.currentStaff);
            this._onChangeCommessa(commessaScelta);
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore lettura XSJS 'letturaStaffCommessa.xsjs'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.StaffCommesseView.readStaffCommessa()
            .then(fSuccess, fError);
        this.visibleModel.setProperty("/chargeButton", false);
    },

    // in base al periodo e alla commessa selezionati setto tutti i parametri indispensabili
    // lancio al funzione di lettura schedulazioni per popolare la tabella
    _onChangeCommessa: function (commessaP) {
        var commessaScelta = commessaP;
        var c = _.find(this.pmSchedulingModel.getData().commesse, {
            codCommessa: commessaScelta
        });
        var comm = model.persistence.Storage.session.save("commessa", c);

        var req = {};
        req.idCommessa = commessaScelta;
        req.codStaff = this.currentStaff.codStaff ? this.currentStaff.codStaff : this.currentStaff.codiceStaffCommessa;
        if (req.codStaff) {
            req.cod_consulente = (this.user).codice_consulente;
            req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
            req.nomeCommessa = this.chosedCom;
            this.ric = req;
            if (!this.currentStaff.codStaff) {
                sap.m.MessageToast.show("Non è presente alcun consulente per questo staff");
            }
        } else {
            this._enterErrorFunction("information", "Informazione", "Creare lo staff per questa commessa!!!");
            this.visibleModel.setProperty("/filterButton", false);
            return;
        }
        this.oDialog.setBusy(false);
        this.leggiTabella(req);
    },

    leggiTabella: function (req) {
        this.oDialogTable.setBusy(true);

        var fSuccess = function (res) {
            if (res && res.resultOrdinato) {
                var schedulazioni = res.resultOrdinato;
                this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioni);
                this.allScheduling = this.pmSchedulingModel.getProperty("/schedulazioni");
                if (schedulazioni.length > 0 && schedulazioni.length <= 6) {
                    this.pmSchedulingModel.setProperty("/nRighe", schedulazioni.length);
                } else if (schedulazioni.length > 6) {
                    this.pmSchedulingModel.setProperty("/nRighe", 6);
                } else {
                    this.pmSchedulingModel.setProperty("/nRighe", 1);
                }
                setTimeout(_.bind(this._onAfterRenderingTable, this));
            }
        };

        var fError = function (err) {
            this._enterErrorFunction("error", "Errore", "Errore lettura tabella: " + err);
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.SchedulingModifyRequests.readPjmRequestModify(req)
            .then(fSuccess, fError);
    },

    // funzione lanciata alla fine di tutto il caricamento
    // colora le celle in grigetto dei sabati e domeniche
    // somma e setta le giornate totali per consulenti e totale complessivo
    _onAfterRenderingTable: function (evt) {
        if (!this.oDialogTable.getBusy()) {
            this.oDialogTable.setBusy(true);
        }
        this.visibleModel.setProperty("/filterButton", true);
        var table = this.oDialogTable;

        this._calcoloGiornateTotali();

        setTimeout(_.bind(function () {
            table.rerender()
            setTimeout(_.bind(function () {
                var rows = table.getRows();
                var arrayCounter = [];
                for (var i = 0; i < rows.length; i++) {
                    var cellsPerRows = rows[i].getAggregation("cells");
                    var g = 0;
                    for (var j = 1; j < cellsPerRows.length - 1; j++) {
                        var cella = cellsPerRows[j];
                        if (_.find(this.weekendDays, _.bind(function (item) {
                                return (item == j);
                            }, this))) {
                            var column = $("#" + cella.getDomRef().id).parent().parent();
                            column.css('background-color', '#e5e5e5');
                        }
                    }
                }
                this.oDialogTable.setBusy(false);
            }, this));
        }, this));
    },

    _calcoloGiornateTotali: function () {
        var totalCount = 0;
        for (var i = 0; i < this.pmSchedulingModel.getProperty("/schedulazioni").length; i++) {
            totalCount = totalCount + this.pmSchedulingModel.getProperty("/schedulazioni")[i].giorniScheduling;
        }
        this.pmSchedulingModel.setProperty("/giorniTotali", totalCount);
    },
    
    // funzione per la navigazione alla lauchpad
    onLaunchpadPress: function () {
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.onNavBackPress();
    },

    onFilterConsultantsByPJMPress: function () {
        var pjmCode = this.user.codice_consulente;
        var page = this.getView().byId("pmSchedulingDraftPage");
        this.filterConsultantsByPJMDialog = sap.ui.xmlfragment("view.dialog.filterConsultantsByPJM", this);
        page.addDependent(this.filterConsultantsByPJMDialog);

//        var fConsultantSuccess = function (result) {
//            var uniqueRes = _.uniq(result, 'codConsulente');
        var users = this.allScheduling;
        for (var i=0; i<users.length; i++) {
            users[i].nomeCognome = users[i].nomeConsulente;
            users[i].codConsulente = users[i].codiceConsulente;
        }
            this.pmSchedulingModel.setProperty("/consultants", users);
            var lista = sap.ui.getCore().getElementById("idListaConsulentiByPJM");
            var selectedConsultants = lista.getSelectedItems();
            var selectAllButton = sap.ui.getCore().byId("selectAllButton");
            if (!!selectAllButton)
                if (selectedConsultants.length === 0) {
                    selectAllButton.setText(model.i18n._getLocaleText("SELECT_ALL"));
                } else {
                    selectAllButton.setText(model.i18n._getLocaleText("DESELECT_ALL"));
                }
            this.filterConsultantsByPJMDialog.open();
//        };
//
//        var fConsultantError = function (err) {
//            sap.m.MessageToast("error loading consultants!");
//        };
//
//        fConsultantSuccess = _.bind(fConsultantSuccess, this);
//        fConsultantError = _.bind(fConsultantError, this);
//
//        model.StaffCommesseView.getStaffByPJMCode(pjmCode)
//            .then(fConsultantSuccess, fConsultantError);
    },
    //
    onFilterSelectAllItems: function (evt) {
        var src = evt.getSource();
        var txtButton = model.i18n._getLocaleText(src.getText());
        var lista = sap.ui.getCore().getElementById("idListaConsulentiByPJM");
        var selectedConsultants = lista.getSelectedItems();
        if (txtButton === "Seleziona tutto") {
            lista.selectAll();
            src.setText("Deseleziona tutto");
        } else {
            lista.removeSelections();
            src.setText("Seleziona tutto");
        }
    },
    //
    onFilterConsultantsByPJMDialogRipristino: function () {
        this.pmSchedulingModel.setProperty("/schedulazioni", this.allScheduling);
        var lunghezzaSchedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni").length;
        sap.ui.getCore().getElementById("idListaConsulentiByPJM").removeSelections();
        this.leggiTabella(this.ric);
        this.filterConsultantsByPJMDialog.close();
        this.filterConsultantsByPJMDialog.destroy(true);
    },
    //
    onFilterConsultantsByPJMDialogClose: function () {
        this.filterConsultantsByPJMDialog.close();
        this.filterConsultantsByPJMDialog.destroy(true);
    },
    //
    onFilterConsultantsByPJMDialogOK: function () {
        var lista = sap.ui.getCore().getElementById("idListaConsulentiByPJM");
        var selectedConsultants = lista.getSelectedItems();
        var selectedConsultantsCode = [];
        if (selectedConsultants.length > 0) {
            for (var i = 0; i < selectedConsultants.length; i++) {
                var codConsulente = selectedConsultants[i].getDescription();
                selectedConsultantsCode.push(codConsulente);
            };
            var schedulazioni = [];
            for (var j = 0; j < selectedConsultantsCode.length; j++) {
                var schedulazione = _.find(this.allScheduling, {
                    'codiceConsulente': selectedConsultantsCode[j]
                });
                if (schedulazione) {
                    schedulazioni.push(schedulazione);
                }
            }
            this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioni);
        } else {
            this.onFilterConsultantsByPJMDialogRipristino();
        }

        var lunghezzaSchedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni").length;
        if (lunghezzaSchedulazioni === 0) {
            lunghezzaSchedulazioni = 1;
        }
        this.pmSchedulingModel.setProperty("/nRighe", lunghezzaSchedulazioni);
        this.pmSchedulingModel.refresh();
//        setTimeout(_.bind(this.onAfter, this));
        setTimeout(_.bind(this._onAfterRenderingTable, this));
        jQuery.sap.delayedCall(500, this, function () {
            this.oDialogTable.setBusy(false);
        });
        //        this.visibleModel.setProperty("/commessaVisible", false);
        this.filterConsultantsByPJMDialog.close();
        this.filterConsultantsByPJMDialog.destroy(true);
    },
    onNotePress: function (evt) {
        var nota = evt.getSource().data("note");
        var jobName = evt.getSource().data("job");
        var job = _.find(this.getView().getModel("pmSchedulingModel").getProperty("/commesse"), {
            nomeCommessa: jobName
        });
        var consultant = evt.getSource().getBindingContext("pmSchedulingModel").getObject();
        this.noteModel.setProperty("/note", consultant[nota]);
        //    	var codReq = nota.substring(4);
        //    	var propReq = "req"+codReq;
        //
        //    	 this.dataForUpdate = {
        //        "codConsulente": consultant.codiceConsulente,
        //               "dataReqPeriodo": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
        //              "idCommessa": job.codCommessa,
        //               "codAttivitaRichiesta":"",
        //               "idPeriodoSchedulingRic": this.getView().byId("idSelectPeriodo").getSelectedKey(),
        //               "codPjm": this.user.codice_consulente,
        //               "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
        //               "note": "",
        //               "statoReq": "CONFIRMED",
        //               "giorno": codReq.indexOf("a")>= 0 ? parseInt(codReq.substr(0, codReq.length-1)) : parseInt(codReq),
        //               "id_req": consultant[propReq] ? consultant[propReq].toString() : ""
        //        };
        this.onAddNote();



    },
    onAddNote: function (evt) {
        //      if (!sap.ui.getCore().byId("closeDescriptionButton")) {
        if (!this._noteDialog) {
            this._noteDialog = sap.ui.xmlfragment(
                "view.dialog.noteDialog",
                this
            );
            this.getView().addDependent(this._noteDialog);
        };
        this.noteModel.setProperty("/editable", false);
        this.noteModel.refresh();
        this._noteDialog.open();
        //    if(sap.ui.getCore().byId("fileDescriptionText"))
        //    {
        //      sap.ui.getCore().byId("fileDescriptionText").setEditable(false);
        //    }
    },
    onAfterCloseNoteDialog: function (evt) {
        this.noteModel.setProperty("/note", undefined);
        this.pmSchedulingModel.refresh();
    },

    onCancelNoteDialogPress: function (evt) {
        if (this._noteDialog) {
            this._noteDialog.close();
            this.pmSchedulingModel.refresh();
        };
        this.leggiTabella(this.ric);
    },


    //------------------------------------------------------------

});