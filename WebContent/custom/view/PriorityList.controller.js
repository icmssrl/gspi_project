jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.Priorities");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("sap.m.MessageToast");

view.abstract.AbstractMasterController.extend("view.PriorityList", {

  onInit: function () {
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    this.modelPrioritaCommesse = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.modelPrioritaCommesse, "modelPrioritaCommesse");

    this.uiModel.setProperty("/searchProperty", ["descrPriorita"]);
  },

  handleRouteMatched: function (evt) {

    var name = evt.getParameter("name");

    if (name !== "priorityList") {
      return;
    }
    view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
    this.getView().byId("list").removeSelections();
    this.oDialog = this.getView().byId("list");

    this.readPriority();
  },

  readPriority: function () {
    var fSuccess = function (result) {
      this.modelPrioritaCommesse.setProperty("/prioritaCommesse", result.items);
      console.log(result);
      this.oDialog.setBusy(false);
    };
    var fError = function (err) {
      this.oDialog.setBusy(false);
      console.log(JSON.parse(err.response.body).error.message.value);
    };

    fSuccess = _.bind(fSuccess, this);
    fError = _.bind(fError, this);

    this.oDialog.setBusy(true);
    model.Priorities.readPriorities()
      .then(fSuccess, fError);
  },

  onLaunchpadPress: function () {
    this.router.navTo("launchpad");
    if (sessionStorage.getItem("selectedPriority")) {
      sessionStorage.removeItem("selectedPriority");
    };
  },

  onFilterPress: function () {
    var prova = [{
      "value": "APERTO"
        }, {
      "value": "CHIUSO"
        }];

    var page = this.getView().byId("listaPriorita");
    if (!this.dataDialog) {
      this.dataDialog = sap.ui.xmlfragment("view.dialog.filterDialogProjects", this);
    };

    this.tempModelData = new sap.ui.model.json.JSONModel();
    this.tempModelData.setData(prova);
    this.getView().setModel(this.tempModelData, "filter");
    page.addDependent(this.dataDialog);

    this.dataDialog.open();
  },

  onFilterDialogClose: function () {
    this.dataDialog.close();
  },

  onFilterDialogOK: function () {

    this.dataDialog.close();

    var filtro = new sap.ui.model.Filter('statoCommessa', sap.ui.model.FilterOperator.Contains, this.arrayFiltri[0])
    var table = this.getView().byId("list");
    var binding = table.getBinding("items");
    binding.filter(filtro);
  },

  onFilterDialogRipristino: function () {
    this.dataDialog.setBusy(false);

    this.dataDialog.destroy();
    delete(this.dataDialog);

    var table = this.getView().byId("list");
    var binding = table.getBinding("items");
    binding.filter();
  },

  filterPress: function (evt) {
    console.log(evt);
    var numeroFiltri = evt.getSource().getSelectedItems();
    var arrayFiltri = [];
    for (var i = 0; i < numeroFiltri.length; i++) {
      arrayFiltri.push(numeroFiltri[i].getProperty("title"));
    }
    this.arrayFiltri = arrayFiltri;
    console.log(arrayFiltri);
  },

  addPriorityPress: function () {
    this.router.navTo("priorityModify", {
      state: "new"
    });
    if (sessionStorage.getItem("selectedPriority")) {
      sessionStorage.removeItem("selectedPriority");
    };
  },

  //    onPrioritySelect: function (evt) {
  //        var src = evt.getSource();
  //        var selectedItem = src.getBindingContext("modelPrioritaCommesse").getObject();
  //        sessionStorage.setItem("selectedPriority", JSON.stringify(selectedItem));
  //        this.router.navTo("priorityDetail", {
  //            detailId: selectedItem.codPrioritaCommessa
  //        });
  //    },

  onPrioritySelectBis: function (evt) {
    var src = evt.getSource();
    var selectedItem = src.getSelectedItems()[0].getBindingContext("modelPrioritaCommesse").getObject();
    sessionStorage.setItem("selectedPriority", JSON.stringify(selectedItem));
    this.router.navTo("priorityDetail", {
      detailId: selectedItem.codPrioritaCommessa
    });
  },

});