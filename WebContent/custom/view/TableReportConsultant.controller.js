//jQuery.sap.declare("view.TableReportConsultant");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("model.persistence.Storage");

view.abstract.AbstractController.extend("view.TableReportConsultant", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.detailConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailConsultantModel, "detailConsultantModel");
        
        this.nameConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.nameConsultantModel, "nameConsultantModel");

        this.enableModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.enableModel, "enableModel");
        
        this.iconJsonModel = {
            "sap-icon://complete" : "Assegnata",
            "sap-icon://time-entry-request" : "Mezza giornata",
            "sap-icon://customer-and-contacts" : "Concomitanza consulente",
            "sap-icon://collaborate" : "Fissata",            
            "sap-icon://employee-rejections" : "Non concomitanza consulente",
            "sap-icon://offsite-work" : "Giornata non schedulabile",
            "sap-icon://role" : "Non confermato cliente",
            "sap-icon://globe": "Estero",
            "sap-icon://travel-itinerary": "Giornata di ferie",
            "sap-icon://bed": "Mezza giornata di ferie"
        };
        
        
        
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "tableReportConsultant") {
            return;
        }

        
        var codConsulente = evt.getParameters().arguments.codConsulente;


        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.codConsulente = this.user.codice_consulente;

        this.schedulazione = model.persistence.Storage.session.get(codConsulente);
        //        this.detailConsultantModel.setData(this.schedulazione);
        var tabella = [];
        var dataInizio = model.persistence.Storage.session.get("periodSelected").startDate;
        var dataFine = model.persistence.Storage.session.get("periodSelected").endDate;
        var dataFormattata = new Date(dataInizio);
        var dataFormattataFine = new Date(dataFine);
        var oneDay = 24*60*60*1000;
        var numGiorni = Math.round(Math.abs((dataFormattataFine.getTime() - dataFormattata.getTime())/(oneDay)));
        
        this.nameConsultantModel.setProperty("/nomeCons", this.schedulazione.nomeConsulente );
        this.nameConsultantModel.setProperty("/giorniScheduling", this.schedulazione.giorniScheduling );

        document.title = this.schedulazione.nomeConsulente;
        
        for (var i = 1; i < numGiorni +2; i++) {
            if (i === 1) {
                var data = new Date(dataFormattata.setDate(dataFormattata.getDate()));
            } else {
                var data = new Date(dataFormattata.setDate(dataFormattata.getDate() + 1));
            }
            var dataToInsert = data.getDate() + "/" + (data.getMonth() + 1) + "/" + data.getFullYear();
            var prop = "gg" + i;
            //            var ggConflict = "gg" + i + "Conflict";
            var propIcon = "ggIcon" + i;
            var propConflict = "ggConflict" + i;
            var propReq = "ggReq" + i;
            var nota = "nota" + i;
            var idReqOld = "idReqOld" +i;
            var stato = "ggStato" +i;
//            var reqType = "reqType" + i;
            //            var notaConflict = "nota" + i + "Conflict";

            if (this.schedulazione[propConflict]) {
                var propA = "";
                prop = "gg" + i + propA;
                nota = "nota" + i + propA;
                propIcon = "ggIcon" + i + propA;
                idReqOld = "idReqOld" +i + propA;
                stato = "ggStato" +i + propA;
                //reqType = "reqType"  +i + propA;
                while (this.schedulazione[prop]) {
                    var obj = {};
                    obj.commessa = this.schedulazione[prop] ? this.schedulazione[prop] : "";
                    obj.data = dataToInsert;
                    obj.nota = this.schedulazione[nota] ? this.schedulazione[nota] : "";
                    obj.idReqOld = this.schedulazione[idReqOld] ? (this.schedulazione[idReqOld]).toString() : "";
                    obj.reqType = this.schedulazione[propIcon] ? this.schedulazione[propIcon] : "";
                    obj.reqTypeTxt  = this.iconJsonModel[obj.reqType];
                    obj.stato = this.schedulazione[stato] ? (this.schedulazione[stato]).toString() : "";
                    if (this.schedulazione[propIcon]) {
                        obj.ore = (this.schedulazione[propIcon] === "sap-icon://time-entry-request" || this.schedulazione[propIcon] === "sap-icon://bed") ? "4" : "8";
                    } else {
                        obj.ore = "";
                    }
                    tabella.push(obj);
                    propA = propA + "a";
                    prop = "gg" + i + propA;
                    nota = "nota" + i + propA;
                    propIcon = "ggIcon" + i + propA;
                    idReqOld = "idReqOld" +i + propA;
                    stato = "ggStato" +i + propA;
                    //reqType = "reqType"  +i + propA;
                }
            } else {
                var obj = {};
                obj.commessa = this.schedulazione[prop] ? this.schedulazione[prop] : "";
                obj.data = dataToInsert;
                obj.nota = this.schedulazione[nota] ? this.schedulazione[nota] : "";
                obj.idReqOld = this.schedulazione[idReqOld] ? (this.schedulazione[idReqOld]).toString() : "";
                obj.reqType = this.schedulazione[propIcon] ? this.schedulazione[propIcon] : "";
                obj.reqTypeTxt  = this.iconJsonModel[obj.reqType];
                obj.stato = this.schedulazione[stato] ? (this.schedulazione[stato]).toString() : "";
                if (this.schedulazione[propIcon]) {
                   // obj.ore = this.schedulazione[propIcon] === "sap-icon://time-entry-request" ? "4" : "8";
                    obj.ore = (this.schedulazione[propIcon] === "sap-icon://time-entry-request" || this.schedulazione[propIcon] === "sap-icon://bed") ? "4" : "8";
                } else {
                    obj.ore = "";
                }
                tabella.push(obj);
            }


        }
        this.detailConsultantModel.setData(tabella);
    },


});