jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("model.SingleSchedModRequest");
jQuery.sap.require("model.SchedulingModifyRequests");
jQuery.sap.require("model.Periods");
jQuery.sap.require("model.Projects");
jQuery.sap.require("model.StaffCommesseView");
jQuery.sap.require("model.Consultants");
jQuery.sap.require("model.tabellaSchedulingOfficial");
jQuery.sap.require("model._ConsultantsStatus");
jQuery.sap.require("model.persistence.Storage");

view.abstract.AbstractController.extend("view.SchedulingModifyRequestEdit", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");

        this.addConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.addConsultantModel, "cModel");

        this.pmSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingModel, "pmSchedulingModel");
        this.pmSchedulingModel.setSizeLimit(1000);

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");

        this.enableModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.enableModel, "enableModel");

        this.requestModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.requestModel, "requestModel");

        this.iconJsonModel = {
            'full': "sap-icon://complete",
            'half': "",
            'wCons': "sap-icon://customer-and-contacts",
            'wNonCons': "sap-icon://employee-rejections",
            'wCust': "sap-icon://collaborate",
            'wNonCust': "sap-icon://offsite-work",
            'notConfirmed': "sap-icon://role",
            'abroad': "sap-icon://globe",
            'del': "sap-icon://question-mark",
            'halfText': "1/2",
            'travel': "sap-icon://travel-itinerary",
            'bed': "sap-icon://bed"
        };

        this.modelloIcone = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelloIcone, "iconModel");
        this.modelloIcone.setProperty("/icon", this.iconJsonModel.del);

        this.uiModelDialog.setProperty("/searchPropertyDialog", ["nomeCompletoConsulente"]);
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "modifyRequestEdit") {
            return;
        }

        this.oDialog = this.getView().byId("schedulingModifyRequestEditPage");
        this.oDialogTable = this.getView().byId("idSchedulazioniTableRequestModify");

        this.state = evt.getParameters().arguments.state;
        this.requestId = evt.getParameters().arguments.detailId;
        this.periodSelect = this.getView().byId("idPeriodSelect");
        this.projectSelect = this.getView().byId("idProjectSelect");
        this.pm = this.user.codice_consulente;
        this.allConsultants = true;

        this.getView().byId("idPeriodSelect").setEnabled(false);
        this.getView().byId("idProjectSelect").setEnabled(false);
        this.getView().byId("idNote").setEnabled(false);
        this.getView().byId("idProposeExchange").setEnabled(false);
        this.enableModel.setProperty("/saveButton", false);
        this.enableModel.setProperty("/cancelButton", false);

        if (this.state === "modify") {
            // per ora non è prevista la modifica. 

            //            this.enableModel.setProperty("/periodSelect", true);
            //            
            //            var requestSelected = model.persistence.Storage.session.get("selectedModifyRequest");
            ////            this.detailRequest.setData(this.requestSelected);
            //            
            //            this.getView().byId("chooseConsultantInput").setValue(requestSelected.nomeConsulenteModifica + " " + requestSelected.cognomeConsulenteModifica);
            //            this.getView().byId("idCodConsulente").setValue(requestSelected.codProfiloConsulenteModifica);
            ////            this.getView().byId("idPeriodSelect").setValue(requestSelected.);
            //            this.getView().byId("idPeriodSelect").setSelectedKey(requestSelected.idPeriodoSchedModifica);
            //            this.getView().byId("idProjectSelect").setValue(requestSelected.nomeCommessaModifica);
            //            this.getView().byId("idProjectSelect").setSelectedKey(requestSelected.codCommessaModifica);
            ////            this.getView().byId("idConsultantProposedInput").setValue(requestSelected.);
            ////            this.getView().byId("idCodConsulenteProposto").setValue(requestSelected.);
            //            
            //            this.getView().byId("idPeriodSelect").setEnabled(true);
            //            this.getView().byId("idProjectSelect").setEnabled(true);
            //            this.getView().byId("idNote").setEnabled(true);
            //            this.getView().byId("idProposeExchange").setEnabled(true);
            //            this.enableModel.setProperty("/saveButton", true);
            //            this.enableModel.setProperty("/cancelButton", true);

        } else {
            this.newRequest = new model.SingleSchedModRequest();
            this.requestModel.setProperty("/periods", []);
            this.requestModel.setProperty("/days", []);
            this.requestModel.setProperty("/projects", []);
            this.requestModel.setProperty("/nomeConsulente", "");
            this.requestModel.setProperty("/codConsulente", "");
            this.requestModel.setProperty("/nomeConsulenteProposto", "");
            this.requestModel.setProperty("/codConsulenteProposto", "");
            this.getView().byId("idSchedulazioniTableRequestModify").setVisible(false);
            this.requestModel.setProperty("/checkBox", false);
            this.visibleModel.setProperty("/consultantProposed", false);

            this._clearInputValue();
        }

        //        var fErrorStatus = function (err) {
        //            this.oDialog.setBusy(false);
        //            sap.m.MessageBox.alert(err, {
        //                title: this._getLocaleText("errore status list")
        //            });
        //        };


        //        var fError = function (err) {
        //            this.oDialog.setBusy(false);
        //            sap.m.MessageBox.alert(err, {
        //                title: this._getLocaleText("error")
        //            });
        //        };

        //        var fSuccessStatus = function (result) {
        //            this.oDialog.setBusy(false);
        //            this.statoConsulente.setProperty("/status", result);
        //        };
        //
        //        fSuccessStatus = _.bind(fSuccessStatus, this);
        //        fErrorStatus = _.bind(fErrorStatus, this);
        //
        //        this.oDialog.setBusy(true);
        //        model._ConsultantsStatus.getStauts()
        //            .then(fSuccessStatus, fErrorStatus)
    },

    giorniMese: function (mese, anno) {
        var d = new Date(anno, mese, 0);
        return d.getDate();
    },

    readPeriods: function () {
        var fSuccess = function (result) {
            this.getView().byId("idPeriodSelect").setEnabled(true);
//            this.periodModel.setData(result);
            var res = {};
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for(var i=0; i<13; i++ ) {
                if(res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.periodModel.setData(res13);
            console.log("Periods: ");
            console.log(res);
        
            this.requestModel.setProperty("/periods", res13.items);
//            this.requestModel.setProperty("/periods", result.items);
            this.periodSelect.setSelectedItem(res13.items[res13.items.length]);
//            this.periodSelect.setSelectedItem(result.items[result.items.length]);
            this.oDialog.setBusy(false);
            jQuery.sap.delayedCall(100, this, function () {
                this.periodSelect.setValue("");
            });
        };
        var fError = function () {
            sap.m.MessageBox.alert("Errore in lettura periodo", {
                title: "Attenzione"
            });
            this.oDialog.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    onSelectPeriod: function (evt) {
        this.svuotaTabella();
        this.getView().byId("idSchedulazioniTableRequestModify").setVisible(false);
        this.getView().byId("idProjectSelect").setEnabled(false);
        this.getView().byId("idNote").setEnabled(false);
        this.getView().byId("idProposeExchange").setEnabled(false);
        this.getView().byId("idProjectSelect").setValue("");
        this.getView().byId("idProjectSelect").setSelectedKey("");
        this.enableModel.setProperty("/saveButton", false);
        this.enableModel.setProperty("/cancelButton", false);

        var fSuccess = function (result) {
            this.getView().byId("idProjectSelect").setEnabled(true);
            this.allCommesse = result.items;
            this.requestModel.setProperty("/projects", result.items);
            jQuery.sap.delayedCall(100, this, function () {
                this.getView().byId("idProjectSelect").setValue("");
            });
        };

        var fError = function () {
            sap.m.MessageBox.alert("Errore in lettura periodo", {
                title: "Attenzione"
            });
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.codConsultant = this.requestModel.getProperty("/codConsulente");
        this.period = parseInt(evt.getSource().getProperty("selectedKey"));
        this.periodValue = evt.getParameters().selectedItem.getProperty("text");
        var filtro = "?$filter=COD_CONSULENTE_STAFF eq '" + this.codConsultant + "' and COD_PJM_AC eq '" + this.pm + "'";
        model.StaffCommesseView.readStaffComplete(filtro)
            .then(fSuccess, fError);
    },

    onCheckBoxPress: function (evt) {
        if (evt.getParameters().selected === true) {
            this.requestModel.setProperty("/nomeConsulenteProposto", "");
            this.requestModel.setProperty("/codConsulenteProposto", "");
            this.visibleModel.setProperty("/consultantProposed", true);
        } else {
            this.visibleModel.setProperty("/consultantProposed", false);
        }
    },

    onSelectProject: function (evt) {
        this.svuotaTabella();
        this.getView().byId("idNote").setEnabled(true);
        this.getView().byId("idProposeExchange").setEnabled(true);
        this.enableModel.setProperty("/saveButton", true);
        this.enableModel.setProperty("/cancelButton", true);

        var table = this.getView().byId("idSchedulazioniTableRequestModify");
        this.pe = evt;
        var p = this.periodValue.replace(/ /g, '');
        model.persistence.Storage.session.save("period", this.period);
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(this.period)
        });
        model.persistence.Storage.session.save("periodSelected", periodSelect);
        var arrayP = p.split("-");

        var pInizio = arrayP[0];
        var ggInizio = pInizio.split("/")[0];
        var mInizio = pInizio.split("/")[1];
        var annoInizio = pInizio.split("/")[2];

        var pFine = arrayP[1];
        var ggFine = pFine.split("/")[0];
        var mFine = pFine.split("/")[1];
        var annoFine = pFine.split("/")[2];

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = new Date(annoInizio + "-" + mInizio + "-" + ggInizio);
        var dataF = new Date(annoFine + "-" + mFine + "-" + ggFine);
        var diffDays = (dataF.getTime() - dataI.getTime()) / (24 * 3600000) + 1;
        var numColonne = diffDays + 1;

        var array = [];
        var giornoSettimana = dataI.getDay();
        if (mInizio !== mFine) {
            for (var i = mInizio; i <= mFine; i++) {
                if (i === mInizio) {
                    var gg = parseInt(ggInizio);
                    var numeroGiorniMese = this.giorniMese(mInizio, annoInizio);
                } else {
                    var gg = 1;
                    var numeroGiorniMese = ggFine;
                }

                var giorno = "";
                for (var j = gg; j <= numeroGiorniMese; j++) {
                    if (giornoSettimana === 0) {
                        giorno = "Dom";
                    } else if (giornoSettimana === 1) {
                        giorno = "Lun";
                    } else if (giornoSettimana === 2) {
                        giorno = "Mar";
                    } else if (giornoSettimana === 3) {
                        giorno = "Mer";
                    } else if (giornoSettimana === 4) {
                        giorno = "Gio";
                    } else if (giornoSettimana === 5) {
                        giorno = "Ven";
                    } else if (giornoSettimana === 6) {
                        giorno = "Sab";
                    }
                    giornoSettimana = giornoSettimana + 1;
                    if (giornoSettimana > 6) {
                        giornoSettimana = 0;
                    }
                    if (j < 10) {
                        j = "0" + j;
                    }
                    var result = j + " " + giorno;
                    array.push(result);
                }
            }
            // return array;
        } else {
            var gg = parseInt(ggInizio);
            var numeroGiorniMese = this.giorniMese(mInizio, annoInizio);

            for (var j = gg; j <= numeroGiorniMese; j++) {
                if (giornoSettimana === 0) {
                    giorno = "Dom";
                } else if (giornoSettimana === 1) {
                    giorno = "Lun";
                } else if (giornoSettimana === 2) {
                    giorno = "Mar";
                } else if (giornoSettimana === 3) {
                    giorno = "Mer";
                } else if (giornoSettimana === 4) {
                    giorno = "Gio";
                } else if (giornoSettimana === 5) {
                    giorno = "Ven";
                } else if (giornoSettimana === 6) {
                    giorno = "Sab";
                }
                giornoSettimana = giornoSettimana + 1;
                if (giornoSettimana > 6) {
                    giornoSettimana = 0;
                }
                if (j < 10) {
                    j = "0" + j;
                }
                var result = j + " " + giorno;
                array.push(result);
            }
        }
        // ** rendo visibili solo le colonne del periodo selezionato
        var columns = table.getColumns();
        for (var i = 0; i < columns.length; i++) {
            columns[i].setVisible(true);
        }
        for (var i = numColonne; i < columns.length - 1; i++) {
            columns[i].setVisible(false);
        }
        // **

        // ** metto i nomi dei giorni sulle colonne
        for (var i = 2; i < columns.length; i++) {
            columns[i - 1].getLabel().mProperties.text = array[i - 2];
        }

        this.project = evt.getParameters().selectedItem.mProperties.key;
        this.ric = {
            'cod_consulente': this.codConsultant,
            'idPeriodo': this.period
        };
        this.leggiTabella(this.ric);
    },

    onAfter: function (evt) {
        var table = this.getView().byId("idSchedulazioniTableRequestModify");
        var rows = table.getRows();
        var arrayCounter = [];
        var toCheck = "toCheck";
        for (var i = 0; i < rows.length; i++) {
            var cellsPerRows = rows[i].getAggregation("cells");
            var g = 0;
            for (var j = 1; j < cellsPerRows.length - 1; j++) {
                var cella = cellsPerRows[j];
                var buttonInCell = cella.getAggregation("items")[0];
                var text1 = cella.getAggregation("items")[1];
                var text2 = cella.getAggregation("items")[2];
                this.removeStyleClassButton(buttonInCell);
                var prop = toCheck + j;
                if (table.getBinding("rows").getModel("schedulazioni").getData().schedulazioni.length > 0 &&
                    table.getBinding("rows").getModel("schedulazioni").getData().schedulazioni[i][prop] == "X") {};

                if (buttonInCell.getIcon() === "sap-icon://complete") {
                    buttonInCell.addStyleClass("fullDay");
                    g++;
                } else if (buttonInCell.getIcon() === "./custom/img/half3.png") {
                    buttonInCell.addStyleClass("halfDay");
                    if (text2.getText() !== "") {
                        g = g + 1;
                    } else {
                        g = g + 0.5;
                    }
                } else if (buttonInCell.getIcon() === "sap-icon://globe") {
                    buttonInCell.addStyleClass("abroad");
                    g++;
                } else if (buttonInCell.getIcon() === "sap-icon://employee-rejections") {
                    buttonInCell.addStyleClass("wNonCons");
                    g++;
                } else if (buttonInCell.getIcon() === "sap-icon://travel-itinerary") {
                    buttonInCell.addStyleClass("travel");
                    g++;
                } else if (buttonInCell.getIcon() === "sap-icon://role") {
                    buttonInCell.addStyleClass("notConfirmed");
                    g++;
                } else if (buttonInCell.getIcon() === "sap-icon://bed") {
                    buttonInCell.addStyleClass("bed");
                    g++;
                } else if (buttonInCell.getIcon() === this.iconJsonModel.wCons) {
                    buttonInCell.addStyleClass("wCons");
                    g++;
                } else if (buttonInCell.getIcon() === this.iconJsonModel.wCust) {
                    buttonInCell.addStyleClass("wCust");
                    g++;
                } else if (buttonInCell.getIcon() !== "") {
                    var a = buttonInCell.getIcon();
                    console.log(buttonInCell.getIcon());
                }
            }
            arrayCounter.push(g);
        }

        var dati = this.pmSchedulingModel.getData();
        var schedulazioni = dati.schedulazioni;
        if (this.getView().byId("idProjectSelect").getSelectedItem()) {
            var commessaScelta = this.getView().byId("idProjectSelect").getSelectedItem().mProperties.text;
        } else {
            var commessaScelta = undefined;
        }
        var gg = 0;
        for (var i = 0; i < schedulazioni.length; i++) {
            var giorniS = {
                "giornoS": arrayCounter[i]
            };
            if (this.visibleModel.getProperty("/enabledButton") === false) {
                schedulazioni[i].giorniScheduling = arrayCounter[i];
            } else {
                // tutto il codice che segue serve per mettere le giornate corrette per ogni consulente e nel campo giornateTotali
                var gg = 0;
                for (var j in schedulazioni[i]) {
                    if (!commessaScelta || schedulazioni[i][j] === commessaScelta) {
                        var index = parseInt(j.substring(2));
                        var elementoArray = "ggIcon" + index;
                        var icon = schedulazioni[i][elementoArray];
                        if (icon) {
                            if (icon === "./custom/img/half3.png" || icon === "sap-icon://bed") {
                                gg = gg + 0.5;
                            } else {
                                gg++;
                            };
                            schedulazioni[i].giorniScheduling = gg;
                        }
                    }
                }
                if (!schedulazioni[i].giorniScheduling) {
                    schedulazioni[i].giorniScheduling = 0;
                }
            }
        }
        var oreTotaliPerCommessa = 0;
        for (var t = 0; t < schedulazioni.length; t++) {
            if (schedulazioni[t].giorniScheduling) {
                oreTotaliPerCommessa = oreTotaliPerCommessa + schedulazioni[t].giorniScheduling;
            }
        }
        this.pmSchedulingModel.getData().giorniTotali = oreTotaliPerCommessa;
        this.pmSchedulingModel.refresh();
    },

    leggiTabella: function (req) {
        var fSuccess = function (results) {
            console.log(results);
            var schedulazioni = this.uniqueElement(results);
            for (var c = 0; c < schedulazioni.length; c++) {
                if (!schedulazioni[c].nomeConsulente || (!schedulazioni[c].nomeConsulente && schedulazioni[c].cod_consulente.indexOf("CCG") >= 0)) {
                    // elimino i consulenti che non hanno un nome e non sono consulenti generici (da rivedere servizio)
                    schedulazioni.splice(c, 1);
                }
            }
            this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioni);
            this.pmSchedulingModel.setProperty("/giorniTotali", "0");
            setTimeout(_.bind(this.onAfter, this));
            jQuery.sap.delayedCall(500, this, function () {
                this.oDialogTable.setBusy(false);
            });
        };
        var fError = function (err) {
            if (this.oDialog.getBusy() === true) {
                this.oDialog.setBusy(false);
            }
            console.log(err);
            this.oDialogTable.setBusy(false);
            sap.m.MessageToast.show(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialogTable.setBusy(true);
        model.tabellaSchedulingOfficial.readTable(req)
            .then(fSuccess, fError);
    },

    uniqueElement: function (result) {
//        var arrayfinale = result[0];
        var arrayGiornateSchedulate = result[0];

        var objCodiciConsulenti = {};
        for (var i = 0; i < arrayGiornateSchedulate.length; i++) {
            var c = "c" + i;
            objCodiciConsulenti[c] = arrayGiornateSchedulate[i].cod_consulente;
        }
        var objCodiciConsulenti2 = {};
        for (var i = 0; i < arrayGiornateSchedulate.length; i++) {
            var c = "d" + i;
            objCodiciConsulenti2[c] = arrayGiornateSchedulate[i].codiceConsulente;
        }

        _.merge(objCodiciConsulenti, objCodiciConsulenti2);

        var arrayfinale = [];
        for (var prop in objCodiciConsulenti) {
            var arr = _.where(arrayGiornateSchedulate, {
                codiceConsulente: objCodiciConsulenti[prop]
            });
            var obj = {};
            for (var i = 0; i < arr.length; i++) {
                for (var prop in arr[i]) {
                    if (!obj[prop]) {
                        obj[prop] = arr[i][prop]
                    } else {
                        if (prop.indexOf("gg") > -1 || prop.indexOf("nota") > -1 || prop.indexOf("req") > -1) {
                            var io = prop + "a";
                            while (obj[io]) {
                                io = io.concat("a");
                            }
                            obj[io] = arr[i][prop];
                        }
                    }
                }
            }
            if (!_.isEmpty(obj))
                arrayfinale.push(obj);
        }
        arrayfinale = _.reduce(arrayfinale, function (results, item) {
            return _.any(results, function (result) {
                return _.isEqual(result, item);
            }) ? results : results.concat([item]);
        }, []);

        console.log(arrayfinale);
        return arrayfinale;
    },

    svuotaTabella: function () {
        // **svuoto la tabella
        this.getView().byId("idSchedulazioniTableRequestModify").setVisible(true);
        this.pmSchedulingModel.setProperty("/schedulazioni", []);
        this.pmSchedulingModel.setProperty("/nRighe", 1);
    },

    removeStyleClassButton: function (b) {
        b.removeStyleClass("attention");
        b.removeStyleClass("travel");
        b.removeStyleClass("halfDay");
        b.removeStyleClass("wCons");
        b.removeStyleClass("fullDay");
        b.removeStyleClass("wNonCons");
        b.removeStyleClass("wCust");
        b.removeStyleClass("wNonCust");
        b.removeStyleClass("notConfirmed");
        b.removeStyleClass("bed");
        b.removeStyleClass("abroad");
    },

    addConsultantProposedPress: function (evt) {
        this.addConsultantPress(evt, "prop");
    },

    addConsultantPress: function (evt, prop) {
        if (!prop) {
            this.svuotaTabella();
            this.getView().byId("idSchedulazioniTableRequestModify").setVisible(false);
            this.getView().byId("idPeriodSelect").setEnabled(false);
            this.getView().byId("idProjectSelect").setEnabled(false);
            this.getView().byId("idNote").setEnabled(false);
            this.getView().byId("idProposeExchange").setEnabled(false);
            this.getView().byId("idCodConsulente").setValue("");
            this.getView().byId("idPeriodSelect").setValue("");
            this.getView().byId("idPeriodSelect").setSelectedKey("");
            this.getView().byId("idProjectSelect").setValue("");
            this.getView().byId("idProjectSelect").setSelectedKey("");
            this.enableModel.setProperty("/saveButton", false);
            this.enableModel.setProperty("/cancelButton", false);
            this.getView().byId("idProposeExchange").setSelected(false);
            this.requestModel.setProperty("/nomeConsulenteProposto", "");
            this.requestModel.setProperty("/codConsulenteProposto", "");
            this.visibleModel.setProperty("/consultantProposed", false);

            this.allConsultants = true;
        } else {
            this.allConsultants = false;
        }

        //        if (!this.addConsultantDialog)
        this.addConsultantDialog = sap.ui.xmlfragment("view.dialog.addConsultantForSchedModRequest", this);
        var page = sap.ui.getCore().byId(this.getView().getId());
        page.addDependent(this.addConsultantDialog);
        this.addConsultantDialog.setModel(this.uiModel, "ui");
        this.addConsultantDialog.setModel(this.getView().getModel("i18n"));
        //
        //        if (evt.getParameters().id === "schedulingModifyRequestEditId--chooseConsultantInput") {
        //            this.allConsultants = true;
        //        } else {
        //            this.allConsultants = false;
        //        }
        this.loadConsultants(this.allConsultants)
            .then(_.bind(function (result) {
                    this.addConsultantDialog.open();
                }, this),
                _.bind(function (err) {}, this));
    },

    afterOpenFilter: function () {
        this.consultantsList = sap.ui.getCore().getElementById("listS");
        this.consultantsList.removeSelections();
    },

    loadConsultants: function (allConsultants) {
        var defer = Q.defer();
        var fSuccess = function (result) {
            if (allConsultants) {
                for (var i = 0; i < result.items.length; i++) {
                    result.items[i].nomeCompletoConsulente = result.items[i].cognomeConsulente + " " + result.items[i].nomeConsulente;
                };
                this.addConsultantModel.setData(result);
            } else {
                result.items = _.uniq(result.items, 'codConsulenteStaff');
                for (var i = 0; i < result.items.length; i++) {
                    result.items[i].nomeCompletoConsulente = result.items[i].nomeConsulente + " " + result.items[i].cognomeConsulente;
                    result.items[i].codConsulente = result.items[i].codConsulenteStaff;
                };
                _.remove(result.items, {
                    'cognomeConsulente': ""
                });
                _.remove(result.items, {
                    'codConsulente': this.codConsultant
                });
                this.addConsultantModel.setData(result);
            }
            defer.resolve();
        };

        var fError = function (err) {
            sap.m.MessageToast("Consultant not added");
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var filterString = "";

        if (allConsultants) {
            model.Consultants.readConsultants(filterString)
                .then(fSuccess, fError);
        } else {
            var filtri = "?$filter=COD_PJM_AC eq '" + this.pm + "'";
            model.StaffCommesseView.readStaffComplete(filtri)
                .then(fSuccess, fError);
        }

        return defer.promise;
    },

    onAddConsultantDialogClose: function () {
        this.uiModel.setProperty("/valueSearch", "");
        var list = sap.ui.getCore().getElementById("listS");
        var binding = list.getBinding("items");
        binding.filter();
        this.addConsultantDialog.close();
        this.addConsultantDialog.destroy(true);
    },

    onAddConsultantDialogOK: function (evt) {
        var selectedConsultant = this.consultantsList.getSelectedItem();
        if (selectedConsultant) {
            if (this.allConsultants) {
                this.requestModel.setProperty("/nomeConsulente", selectedConsultant.getTitle());
                this.requestModel.setProperty("/codConsulente", selectedConsultant.getDescription());
                this.readPeriods();
            } else {
                this.requestModel.setProperty("/nomeConsulenteProposto", selectedConsultant.getTitle());
                this.requestModel.setProperty("/codConsulenteProposto", selectedConsultant.getDescription());
            }
        }
        this.uiModel.setProperty("/valueSearch", "");
        var list = sap.ui.getCore().getElementById("listS");
        list.removeSelections();
        var binding = list.getBinding("items");
        binding.filter();
        this.addConsultantDialog.close();
        this.addConsultantDialog.destroy(true);
    },
    /////****************Add New Consultant***********/////////////////////

    cancelModifyPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_CANCEL_MODIFY_CONSULTANT"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_CANCEL_MODIFY_TITLE_CONSULTANT"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmCancelPress, this)
        );
    },

    saveProjectPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_SAVE_CONSULTANT"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_SAVE_TITLE_CONSULTANT"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmSavePress, this)
        );
    },

    _confirmCancelPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            this._clearInputValue();
            this.router.navTo("modifyRequestsList");
        }
    },

    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var fError = function (err) {
                console.log("errore nel salvataggio");
                this.oDialog.setBusy(false);
            };

            var fSuccessAddSkill = function (result) {
                if (JSON.parse(result).status !== "OK") {
                    sap.m.MessageBox.alert(result, {
                        title: this._getLocaleText("errore salvataggio skill")
                    });
                }
            };

            var fSuccessAddLastSkill = function (result) {
                this.oDialog.setBusy(false);
                if (JSON.parse(result).status !== "OK") {
                    sap.m.MessageBox.alert(result, {
                        title: this._getLocaleText("errore salvataggio skill")
                    });
                } else {
                    this._clearInputValue();
                    this.oDialog.setBusy(false);
                    this.router.navTo("consultantList");
                }
            };

            var fSuccessAddConsultant = function (result) {
                var skillAggiunti = this.detailConsulente.getData().codSkillSelected;
                var consulenteSalvato = JSON.parse(result).results;
                for (var i = 0; i < skillAggiunti.length; i++) {
                    if (i !== skillAggiunti.length - 1) {
                        model.Skill.addSkillToConsultant(consulenteSalvato.codConsulente, skillAggiunti[i])
                            .then(fSuccessAddSkill, fError);
                    } else {
                        model.Skill.addSkillToConsultant(consulenteSalvato.codConsulente, skillAggiunti[i])
                            .then(fSuccessAddLastSkill, fError);
                    }
                }
                this._clearInputValue();
                this.oDialog.setBusy(false);
                this.router.navTo("consultantList");
            };

            var fSuccessUpdateConsultant = function () {
                this._clearInputValue();
                this.oDialog.setBusy(false);
                this.router.navTo("consultantList");
            };

            fSuccessUpdateConsultant = _.bind(fSuccessUpdateConsultant, this);
            fSuccessAddConsultant = _.bind(fSuccessAddConsultant, this);
            fSuccessAddLastSkill = _.bind(fSuccessAddLastSkill, this);
            fSuccessAddSkill = _.bind(fSuccessAddSkill, this);
            fError = _.bind(fError, this);
            var cognomeConsulente = this.getView().byId("idCognomeConsulente").getValue();
            var nomeConsulente = this.getView().byId("idNomeConsulente").getValue();
            var codProfiloConsulente = _.find(this.detailConsulente.getData().profiloConsulente, {
                'descrizioneProfiloConsulente': this.getView().byId("codProfiloConsulente").getProperty("value")
            }).COD_PROFILO;
            var codConsulenteBu = this.getView().byId("idCodConsulenteBu").getSelectedKey();
            var descBu = this.getView().byId("idCodConsulenteBu").getValue();
            var tipoAzienda = this.getView().byId("idTipoAzienda").getValue();
            var stato = this.getView().byId("idStatoConsulente").getValue();
            var note = this.getView().byId("idNote").getValue();
            var ggInterCompany = this.detailConsulente.getData().ggIntercompany;
            if (ggInterCompany === "") {
                ggInterCompany = 0;
            }
            var tariffaIntercompany = this.detailConsulente.getData().tariffaIntercompany;
            if (tariffaIntercompany === "") {
                tariffaIntercompany = 0;
            }
            var intercompany = this.detailConsulente.getData().intercompany;
            var partTime = this.detailConsulente.getData().partTime;
            var orePartTime = this.detailConsulente.getData().orePartTime;
            if (orePartTime === "") {
                orePartTime = 0;
            }
            this.oDialog.setBusy(true);
            if (this.state === "new") {
                model.Consultants.addConsultant(cognomeConsulente, nomeConsulente, codProfiloConsulente, codConsulenteBu, descBu, tipoAzienda, stato, note, ggInterCompany,
                        tariffaIntercompany, intercompany, partTime, orePartTime)
                    .then(fSuccessAddConsultant, fError);
            } else {
                var codConsulente = this.getView().byId("idCodConsulente").getValue();
                model.Consultants.updateConsultant(codConsulente, cognomeConsulente, nomeConsulente, codProfiloConsulente, codConsulenteBu, descBu, tipoAzienda, stato, note,
                        ggInterCompany, tariffaIntercompany, intercompany, partTime, orePartTime)
                    .then(fSuccessUpdateConsultant, fError);
            }
        }
    },

    handleOpen: function (oEvent) {
        this.oButton = oEvent.getSource();
        if (this.oButton.mProperties.icon !== "sap-icon://travel-itinerary") {
            // create action sheet only once
//            if (!this._actionSheet) {
            this.leggiTabella(this.ric);
            if(sap.ui.getCore().getElementById("actionSheet")) {
                sap.ui.getCore().getElementById("actionSheet").destroy(true);
            }
                this._actionSheet = sap.ui.xmlfragment(
                    "view.fragment.ActionSheet",
                    this
                );
                this.getView().addDependent(this._actionSheet);
//            }
            this._actionSheet.openBy(this.oButton);
        }
    },

    showButton: function (evt) {
        // mostro i bottoni delle note e del cancella
        // (ultimi due bottoni dell'actionSheet) solo se
        // ci sono già delle richieste 
        var b = this.oButton;
        var idButton = b.getId();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        var row = idButton.substring(idButton.indexOf("row") + 3, idButton.length);
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[row];
        var prop = "gg" + nButton;
        var prop2 = "gg" + nButton + "a";
        // nascondo le note e il cancella se sul giorno cliccato non c'è la commessa selezionata nella combobox
        var nascondiBottoniNoteCancella = false;
        var commessaComboBox = this.projectSelect.getSelectedItem().getText();
        // guardo che commesse ci sono in quel giorno
        if (dataModelConsultant[prop]) {
            if (dataModelConsultant[prop] !== commessaComboBox) {
                if (dataModelConsultant[prop2]) {
                    if (dataModelConsultant[prop2] !== commessaComboBox) {
                        nascondiBottoniNoteCancella = true;
                    }
                } else {
                    nascondiBottoniNoteCancella = true;
                }
            }
        }

        var numeroBottoni = evt.getSource().getAggregation("buttons").length;
        for (var i = 0; i < numeroBottoni; i++) {
            evt.getSource().getAggregation("buttons")[i].setVisible(true);
        }
        if (this.oButton.getIcon() !== "" && this.oButton.getIcon().indexOf("bed") < 0) {
            if ((dataModelConsultant[prop] && dataModelConsultant[prop] === "feri") || (dataModelConsultant[prop2] && dataModelConsultant[prop2] === "feri")) {
                for (var i = 0; i < numeroBottoni; i++) {
                    evt.getSource().getAggregation("buttons")[i].setVisible(false);
                }
                evt.getSource().getAggregation("buttons")[8].setVisible(true);
            } else {
                if (nascondiBottoniNoteCancella) {
                    evt.getSource().getAggregation("buttons")[numeroBottoni - 1].setVisible(false);
                    evt.getSource().getAggregation("buttons")[numeroBottoni - 2].setVisible(false);
                } else {
                    evt.getSource().getAggregation("buttons")[numeroBottoni - 1].setVisible(false);
                    evt.getSource().getAggregation("buttons")[numeroBottoni - 2].setVisible(true);
                }
            }
        } else if (this.oButton.getIcon().indexOf("bed") > 0) {
            for (var i = 0; i < numeroBottoni; i++) {
                evt.getSource().getAggregation("buttons")[i].setVisible(false);
            }
            evt.getSource().getAggregation("buttons")[1].setVisible(true);
        } else {
            evt.getSource().getAggregation("buttons")[numeroBottoni - 1].setVisible(false);
            evt.getSource().getAggregation("buttons")[numeroBottoni - 2].setVisible(false);
        }
    },

    afterClose: function (evt) {
        // nascondo i bottoni delle note e del cancella
        // (ultimi due bottoni dell'actionSheet)
        if (evt.getSource().getAggregation("buttons")) {
            var numeroBottoni = evt.getSource().getAggregation("buttons").length;
        }
        if (evt.getSource().getAggregation("buttons")[numeroBottoni - 1]) {
            evt.getSource().getAggregation("buttons")[numeroBottoni - 1].setVisible(false);
        }
        if (evt.getSource().getAggregation("buttons")[numeroBottoni - 2]) {
            evt.getSource().getAggregation("buttons")[numeroBottoni - 2].setVisible(false);
        }
    },

    actionButton: function (evt) {
        if (this.visibleModel.getProperty("/consultantProposed")) {
            if (this.getView().byId("idCodConsulenteProposto").getValue() === "") {
                sap.m.MessageToast.show(this._getLocaleText("SELECT_PROPOSED_CONSULTANT"));
                this.leggiTabella(this.ric);
                return;
            }
        }
        var del = false;
        var b = this.oButton;
        var idButton = b.getId();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        var row = idButton.substring(idButton.indexOf("row") + 3, idButton.length);
        var prop = "gg" + nButton;
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[row];
        var commessaComboBox = this.projectSelect.getSelectedItem().getText();
        if (dataModelConsultant[prop] !== commessaComboBox) {
            prop = prop + "a";
            var propNotaGiorno = "nota" + nButton + "a";
            var propReq = "req" + nButton + "a";
        } else {
            var propNotaGiorno = "nota" + nButton;
            var propReq = "req" + nButton;
        }
        //        this.noteModel.setProperty("/note", dataModelConsultant[propNotaGiorno]);
        this.oButton = "";
        var buttonPressed = evt.getSource();

        // prendo il numero della tabella per passare correttamente la data
        var table = this.getView().byId("idSchedulazioniTableRequestModify");
        var giornoMese = parseInt(table.getColumns()[nButton].getLabel().getProperty("text"));

        var dataToSend = {
            "codConsulente": "",
            "dataReqPeriodo": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "idCommessa": "",
            "codAttivitaRichiesta": "",
            "idPeriodoSchedulingRic": 0,
            "codPjm": "",
            "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "note": "",
            "statoReq": "CONFIRMED",
            "giorno": 0,
            "id_req": dataModelConsultant[propReq]
        };

        dataToSend.codConsulente = dataModelConsultant.codiceConsulente;
        dataToSend.codPjm = dataModelConsultant.codicePJM;
        dataToSend.idPeriodoSchedulingRic = this.getView().byId("idPeriodSelect").getSelectedKey();
        dataToSend.idCommessa = this.getView().byId("idProjectSelect").getSelectedKey();
        dataToSend.giorno = nButton;

        var jsonButtonModel = {
            halfDay: "1/2",
            fullDay: this.chosedCom,
            concConsultant: "yellow",
            concCustomer: "CC",
        };

        var richiesta = buttonPressed.getId();
        var table = this.getView().byId("idSchedulazioniTableRequestModify");

        var o = this.pmSchedulingModel.getData().schedulazioni[row];

        this.removeStyleClassButton(b);

        switch (richiesta) {
        case "Assign":
            //                o[prop] = jsonButtonModel.fullDay;
            dataToSend.codAttivitaRichiesta = "complete";
            break;
        case "halfDay":
            if (b.getIcon() === "./custom/img/half3.png" || b.getIcon() === "sap-icon://bed") {
                if (o[prop] && o[prop] === jsonButtonModel.fullDay) {
                    b.addStyleClass("fullDay");
                    //                        o[prop] = jsonButtonModel.fullDay;
                } else if (o[prop] && o[prop] !== jsonButtonModel.fullDay) {
                    prop = prop + "a";
                    //                        o[prop] = jsonButtonModel.fullDay;
                }
            } else {
                o[prop] = jsonButtonModel.fullDay;
                prop = prop + "a";
                if (o[prop]) {
                    o[prop] = "";
                }
            }
            dataToSend.codAttivitaRichiesta = richiesta;
            break;
        case "wCons":
            //                o[prop] = jsonButtonModel.fullDay;
            dataToSend.codAttivitaRichiesta = "customer-and-contacts";
            break;
        case "wNonCons":
            //                o[prop] = jsonButtonModel.fullDay;
            dataToSend.codAttivitaRichiesta = "not-customer-and-contacts";
            break;
        case "wCust":
            //                o[prop] = jsonButtonModel.fullDay;
            dataToSend.codAttivitaRichiesta = "collaborate";
            break;
        case "wNonCust":
            //                o[prop] = jsonButtonModel.fullDay;
            dataToSend.codAttivitaRichiesta = "not-collaborate";
            break;
        case "notConfirmed":
            //                o[prop] = jsonButtonModel.fullDay;
            dataToSend.codAttivitaRichiesta = "not-confirmed";
            break;
        case "abroad":
            //                o[prop] = jsonButtonModel.fullDay;
            dataToSend.codAttivitaRichiesta = richiesta;
            break;
        case "del":
            var icon = b.getIcon();
            var req = icon.substring(icon.lastIndexOf("/") + 1, icon.length);
            del = true;
            o[prop] = "";
            prop = prop + "a";
            if (o[prop]) {
                o[prop] = "";
            }
            if (req === "half3.png") {
                req = "halfDay";
            }

            dataToSend.codAttivitaRichiesta = req;
            this.requestModel.setProperty("/note", "RHIESTA CANCELLAZIONE") ;

//            var fSuccess = function () {
//                this.leggiTabella(this.ric);
//            };
//            var fError = function (err) {
//                sap.m.MessageToast.show("ERRORE");
//            };
//            fSuccess = _.bind(fSuccess, this);
//            fError = _.bind(fError, this);
//
//            model.reqToDelMaria.sendRequest(dataToSend)
//                .then(fSuccess, fError);
            break;
        }

        this.dataToSend = dataToSend;
        var idPrimoLabel = table.getColumns()[0].mAggregations.label.sId;
        var idPrimoGiornoLabel = table.getColumns()[1].mAggregations.label.sId;
        var indexPrimoLabel = parseInt(idPrimoLabel.split("label")[1]);
        var indexSelezionato = b.sId.split("col")[1];
        indexSelezionato = parseInt(indexSelezionato.split("-")[0]) + indexPrimoLabel;
        var idLabelSelezionato = "__label" + indexSelezionato;
        var giornoSelezionato = sap.ui.getCore().getElementById(idLabelSelezionato).mProperties.text.substring(0, 2);
        this.nomeConsulente = this.requestModel.getProperty("/nomeConsulente");
        this.codConsulente = this.requestModel.getProperty("/codConsulente");
        this.nomeCommessa = this.projectSelect.getSelectedItem().getText();
        this.codCommessa = this.projectSelect.getSelectedItem().getKey();
        //        this.codCommessa = this.requestModel.getProperty("/idProject");
        this.note = this.requestModel.getProperty("/note");
        this.giorno = nButton;
        this.idPeriod = dataToSend.idPeriodoSchedulingRic;
        this.codAttivitaRichiesta = dataToSend.codAttivitaRichiesta;
        var mesePartenzaPeriodo = parseInt(this.periodSelect.getSelectedItem().mProperties.text.split("-")[0].split("/")[1]);
        var annoPartenzaPeriodo = parseInt(this.periodSelect.getSelectedItem().mProperties.text.split("-")[0].split("/")[2]);
        var ultimoGiornoPeriodo = new Date(annoPartenzaPeriodo, mesePartenzaPeriodo, 0).getDate();
        var giornoPartenzaPeriodo = parseInt(this.periodSelect.getSelectedItem().mProperties.text.split("-")[0].split("/")[0]);
        var differenza = ultimoGiornoPeriodo - giornoPartenzaPeriodo + 1;
        if (parseInt(nButton) <= differenza) {
            var mese = mesePartenzaPeriodo;
            var anno = annoPartenzaPeriodo;
        } else {
            var mese = parseInt(this.periodSelect.getSelectedItem().mProperties.text.split("-")[1].split("/")[1]);
            var anno = parseInt(this.periodSelect.getSelectedItem().mProperties.text.split("-")[1].split("/")[2]);
        }
        var dataRichiestaModifica = new Date(anno, mese - 1, giornoSelezionato);
        this.dataRichiestaModificaDaMostrare = utils.Formatter.formatDate(dataRichiestaModifica);
        this.dataRichiestaModifica = utils.Formatter.dateToHana(this.dataRichiestaModificaDaMostrare);
        this.dataInserimentoModifica = utils.Formatter.formatDate(new Date());
        this.dataInserimentoModifica = utils.Formatter.dateToHana(this.dataInserimentoModifica);

        this.schedulazioniGiornaliere = [];
        var prop = "gg" + nButton;
        var propIcon = "ggIcon" + nButton;
        var propNota = "nota" + nButton;
        if (this.pmSchedulingModel.getProperty("/schedulazioni")[row][prop]) {
            var nomeCommessa = this.pmSchedulingModel.getProperty("/schedulazioni")[row][prop];
            var icona = this.pmSchedulingModel.getProperty("/schedulazioni")[row][propIcon];
            if (icona === "./custom/img/half3.png") {
                icona = "sap-icon://time-entry-request";
            };
            var nota = this.pmSchedulingModel.getProperty("/schedulazioni")[row][propNota];
            var codCommessaSel = _.find(this.requestModel.getProperty("/projects"), {
                'nomeCommessa': nomeCommessa
            });
            if (codCommessaSel) {
                var codCommessa = codCommessaSel.codCommessa;
            } else {
                sap.m.MessageToast.show("impossibile effettuare la richiesta");
                this.leggiTabella(this.ric);
                return;
            }
            var commessa = {
                'ric': nomeCommessa,
                'codCommessa': codCommessa,
                'icon': icona,
                'nota': nota
            };
            if (this.visibleModel.getProperty("/consultantProposed")) {
                this.commessa = commessa;
                var ricConProp = {};
                ricConProp.cod_consulente = this.getView().byId("idCodConsulenteProposto").getValue();
                ricConProp.idPeriodo = this.period;
                this.readSchedConsultantProposed(ricConProp)
                    .then(_.bind(function (result) {
                            var schedulingConsultantProposed = result;
                            console.log(schedulingConsultantProposed[0]);
                            var variabile = "gg" + this.giorno;
                            if (schedulingConsultantProposed[0][variabile]) {
                                //verifica se la commessa appartiene al PJM di login
                                this.commConsProp = schedulingConsultantProposed[0][variabile];
                                this.readProjects()
                                    .then(_.bind(function (result) {
                                            console.log(result);
                                            var trovataComm = _.find(result, {
                                                'nomeCommessa': this.commConsProp
                                            });
                                            
                                            if (trovataComm) {
                                                this.codCommConsProp = trovataComm.codCommessa;
                                                var prop2 = "gg" + nButton + "a";
                                                var propIcon2 = "ggIcon" + nButton + "a";
                                                var propNota2 = "nota" + nButton + "a";
                                                this.schedulazioniGiornaliere.push(this.commessa);
                                                if (this.pmSchedulingModel.getProperty("/schedulazioni")[row][prop2]) {
                                                    var nomeCommessa = this.pmSchedulingModel.getProperty("/schedulazioni")[row][prop2];
                                                    var icona = this.pmSchedulingModel.getProperty("/schedulazioni")[row][propIcon2];
                                                    if (icona === "./custom/img/half3.png") {
                                                        icona = "sap-icon://time-entry-request";
                                                    };
                                                    var nota = this.pmSchedulingModel.getProperty("/schedulazioni")[row][propNota2];
                                                    var codCommessa = _.find(this.requestModel.getProperty("/projects"), {
                                                        'nomeCommessa': nomeCommessa
                                                    }).codCommessa;
                                                    var commessa = {
                                                        'ric': nomeCommessa,
                                                        'codCommessa': codCommessa,
                                                        'icon': icona,
                                                        'nota': nota
                                                    };
                                                    this.schedulazioniGiornaliere.push(commessa);
                                                };

                                                if (this.schedulazioniGiornaliere.length < 2) {
                                                    if (this.requestModel.getProperty("/nomeConsulenteProposto")) {
                                                        this.comparsaMessageBoxSemplice();
                                                    }
                                                } else if (this.schedulazioniGiornaliere.length === 2 && richiesta === "halfDay") {
                                                    this.selezionaCommessa();
                                                } else if (this.schedulazioniGiornaliere.length === 2 && richiesta !== "halfDay") {
                                                    // non è possibile sostituire due mezze giornate con una commessa intera
                                                    sap.m.MessageBox.show(
                                                        this._getLocaleText("ERROR_SAVE_REQUEST"),
                                                        sap.m.MessageBox.Icon.ALLERT,
                                                        this._getLocaleText("ERROR_SAVE_TITLE_REQUEST")
                                                    );
                                                    this.leggiTabella(this.ric);
                                                };
                                            } else {
                                                sap.m.MessageToast.show("COMMESSA_ALTRO_PJM");
                                                return;
                                            }
                                        }, this),
                                        _.bind(function (err) {}, this));
                            } else {
                                this.codCommConsProp = "0000000000";
                                this.schedulazioniGiornaliere.push(this.commessa);
                                //                                this.codConsulenteProposto = this.requestModel.getProperty("/codConsulenteProposto");
                                var prop2 = "gg" + nButton + "a";
                                var propIcon2 = "ggIcon" + nButton + "a";
                                var propNota2 = "nota" + nButton + "a";
                                if (this.pmSchedulingModel.getProperty("/schedulazioni")[row][prop2]) {
                                    var nomeCommessa = this.pmSchedulingModel.getProperty("/schedulazioni")[row][prop2];
                                    var icona = this.pmSchedulingModel.getProperty("/schedulazioni")[row][propIcon2];
                                    if (icona === "./custom/img/half3.png") {
                                        icona = "sap-icon://time-entry-request";
                                    };
                                    var nota = this.pmSchedulingModel.getProperty("/schedulazioni")[row][propNota2];
                                    var codCommessa = _.find(this.requestModel.getProperty("/projects"), {
                                        'nomeCommessa': nomeCommessa
                                    }).codCommessa;
                                    var commessa = {
                                        'ric': nomeCommessa,
                                        'codCommessa': codCommessa,
                                        'icon': icona,
                                        'nota': nota
                                    };
                                    this.schedulazioniGiornaliere.push(commessa);
                                };

                                if (this.schedulazioniGiornaliere.length < 2) {
                                    if (this.requestModel.getProperty("/nomeConsulenteProposto")) {
                                        this.comparsaMessageBoxSemplice();
                                    }
                                } else if (this.schedulazioniGiornaliere.length === 2 && richiesta === "halfDay") {
                                    this.selezionaCommessa();
                                } else if (this.schedulazioniGiornaliere.length === 2 && richiesta !== "halfDay") {
                                    // non è possibile sostituire due mezze giornate con una commessa intera
                                    sap.m.MessageBox.show(
                                        this._getLocaleText("ERROR_SAVE_REQUEST"),
                                        sap.m.MessageBox.Icon.ALLERT,
                                        this._getLocaleText("ERROR_SAVE_TITLE_REQUEST")
                                    );
                                    this.leggiTabella(this.ric);
                                };
                                console.log("qui")
                            }

                        }, this),
                        _.bind(function (err) {}, this));
                //                return;
            } else {
                this.schedulazioniGiornaliere.push(commessa);
                var prop2 = "gg" + nButton + "a";
                var propIcon2 = "ggIcon" + nButton + "a";
                var propNota2 = "nota" + nButton + "a";
                if (this.pmSchedulingModel.getProperty("/schedulazioni")[row][prop2]) {
                    var nomeCommessa = this.pmSchedulingModel.getProperty("/schedulazioni")[row][prop2];
                    var icona = this.pmSchedulingModel.getProperty("/schedulazioni")[row][propIcon2];
                    if (icona === "./custom/img/half3.png") {
                        icona = "sap-icon://time-entry-request";
                    };
                    var nota = this.pmSchedulingModel.getProperty("/schedulazioni")[row][propNota2];
                    var codCommessa = _.find(this.requestModel.getProperty("/projects"), {
                        'nomeCommessa': nomeCommessa
                    }).codCommessa;
                    var commessa = {
                        'ric': nomeCommessa,
                        'codCommessa': codCommessa,
                        'icon': icona,
                        'nota': nota
                    };
                    this.schedulazioniGiornaliere.push(commessa);
                };

                if (this.schedulazioniGiornaliere.length < 2) {
                    if (this.requestModel.getProperty("/checkBox") !== true || !this.requestModel.getProperty("/nomeConsulenteProposto")) {
                        this.comparsaMessageBoxSemplice();
                    }
                } else if (this.schedulazioniGiornaliere.length === 2 && richiesta === "halfDay") {
                    this.selezionaCommessa();
                } else if (this.schedulazioniGiornaliere.length === 2 && richiesta !== "halfDay") {
                    // non è possibile sostituire due mezze giornate con una commessa intera
                    sap.m.MessageBox.show(
                        this._getLocaleText("ERROR_SAVE_REQUEST"),
                        sap.m.MessageBox.Icon.ALLERT,
                        this._getLocaleText("ERROR_SAVE_TITLE_REQUEST")
                    );
                    this.leggiTabella(this.ric);
                };
            }
        } else {
            var prop2 = "gg" + nButton + "a";
            var propIcon2 = "ggIcon" + nButton + "a";
            var propNota2 = "nota" + nButton + "a";
            if (this.pmSchedulingModel.getProperty("/schedulazioni")[row][prop2]) {
                var nomeCommessa = this.pmSchedulingModel.getProperty("/schedulazioni")[row][prop2];
                var icona = this.pmSchedulingModel.getProperty("/schedulazioni")[row][propIcon2];
                if (icona === "./custom/img/half3.png") {
                    icona = "sap-icon://time-entry-request";
                };
                var nota = this.pmSchedulingModel.getProperty("/schedulazioni")[row][propNota2];
                var codCommessa = _.find(this.requestModel.getProperty("/projects"), {
                    'nomeCommessa': nomeCommessa
                }).codCommessa;
                var commessa = {
                    'ric': nomeCommessa,
                    'codCommessa': codCommessa,
                    'icon': icona,
                    'nota': nota
                };
                this.schedulazioniGiornaliere.push(commessa);
            };

            if (this.schedulazioniGiornaliere.length < 2) {
                if (this.requestModel.getProperty("/checkBox") !== true || !this.requestModel.getProperty("/nomeConsulenteProposto")) {
                    this.comparsaMessageBoxSemplice();
                }
            } else if (this.schedulazioniGiornaliere.length === 2 && richiesta === "halfDay") {
                this.selezionaCommessa();
            } else if (this.schedulazioniGiornaliere.length === 2 && richiesta !== "halfDay") {
                // non è possibile sostituire due mezze giornate con una commessa intera
                sap.m.MessageBox.show(
                    this._getLocaleText("ERROR_SAVE_REQUEST"),
                    sap.m.MessageBox.Icon.ALLERT,
                    this._getLocaleText("ERROR_SAVE_TITLE_REQUEST")
                );
                this.leggiTabella(this.ric);
            };
        }

        //        if (!del) {
        //            this.saveRequest(dataToSend);
        //        };
    },

    onRequestDialogClose: function () {
        this.commesseDialog.close();
        this.commesseDialog.destroy(true);
        this.leggiTabella(this.ric);
    },

    onRequestDialogOK: function (evt) {
        //        console.log(evt);

        var requestList = sap.ui.getCore().getElementById("idRequestsList");
        var requestSelected = requestList.getSelectedItems();
        var req = requestSelected[0].getTitle();
        var icon = requestSelected[0].getIcon();

        var comm = _.find(this.requestModel.getData().projects, {
            'nomeCommessa': req
        })
        if (comm) {
            var codComm = comm.codCommessa;
        } else {
            var codComm = "0000000000";
        }

        this.commesseDialog.close();
        this.commesseDialog.destroy(true);
        //        this.leggiTabella(this.ric);
        this.comparsaMessageBoxSemplice();
    },

    selezionaCommessa: function () {
        //        if (!this.commesseDialog) {
        this.commesseDialog = sap.ui.xmlfragment("view.fragment.SelectProjectActionSheet", this);
        //            this.commesseDialog = sap.ui.xmlfragment("view.fragment.PmActionSheet", this);
        this.getView().addDependent(this.commesseDialog);
        //        };
        this.pmSchedulingModel.setProperty("/requests", this.schedulazioniGiornaliere);
        this.commesseDialog.setModel(this.pmSchedulingModel, "pmSchedulingModel");
        this.commesseDialog.open();
    },

    comparsaMessageBoxSemplice: function () {
        this.codConsulenteProposto = 0;
        this.codCommessaPartenza = this.schedulazioniGiornaliere[0] ? this.schedulazioniGiornaliere[0].codCommessa : "0000000000";
        sap.m.MessageBox.show(
            this._getLocaleText("CONFIRM_SAVE_REQUEST") + " " + this.nomeConsulente + " " +
            this._getLocaleText("CONFIRM_SAVE_REQUEST2") + " " + this.dataRichiestaModificaDaMostrare + " " +
            this._getLocaleText("CONFIRM_SAVE_REQUEST3") + " " + this.nomeCommessa + "?",
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_SAVE_TITLE_REQUEST"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this.onConfirmSaveRequest, this)
        );
    },

    colorItems: function () {
        var requestList = sap.ui.getCore().getElementById("idRequestsList");
        requestList.removeSelections();
        var requests = requestList.getItems();
        var richiestaMezzaGiornata = 0;
        for (var i = 0; i < requests.length; i++) {
            var id = requests[i].sId;
            var request = sap.ui.getCore().getElementById(id);
            var classiDialog = ["travelPm", "halfDayPm", "wConsPm", "travelPm", "abroadPm", "fullDayPm", "wNonConsPm", "wCustPm", "wNonCustPm", "notConfirmedPm", "bedPm"];
            for (var y = 0; y < classiDialog.length; y++) {
                request.removeStyleClass(classiDialog[y]);
            };
            if (request.getIcon() === "sap-icon://travel-itinerary") {
                var classe = "travelPm";
            } else if (request.getIcon() === "sap-icon://time-entry-request") {
                var classe = "halfDayPm";
                richiestaMezzaGiornata++;
            } else if (request.getIcon() === "sap-icon://customer-and-contacts") {
                var classe = "wConsPm";
            } else if (request.getIcon() === "sap-icon://globe") {
                var classe = "abroadPm";
            } else if (request.getIcon() === "sap-icon://complete") {
                var classe = "fullDayPm";
            } else if (request.getIcon() === "sap-icon://employee-rejections") {
                var classe = "wNonConsPm";
            } else if (request.getIcon() === "sap-icon://collaborate") {
                var classe = "wCustPm";
            } else if (request.getIcon() === "sap-icon://offsite-work") {
                var classe = "wNonCustPm";
            } else if (request.getIcon() === "sap-icon://role") {
                var classe = "notConfirmedPm";
            } else if (request.getIcon() === "sap-icon://bed") {
                var classe = "bedPm";
                richiestaMezzaGiornata++;
            };
            request.addStyleClass(classe);
            if (richiestaMezzaGiornata > 1 && !this.commesseDialog.isOpen()) {
                requestList.setMode("MultiSelect");
            } else {
                requestList.setMode("SingleSelectLeft");
            };
        };
    },

    onRequestPress: function (evt) {
        var requestList = sap.ui.getCore().getElementById("idRequestsList");
        if (requestList.getMode() === "MultiSelect") {
            var mezzeGiornate = [];
            var giornateIntere = [];
            var requestChoice = evt.getParameters().listItem;
            var icon = requestChoice.mProperties.icon;
            var req = requestChoice.mProperties.title;
            var selected = requestChoice.mProperties.selected;
            if (selected) {
                var requestsSelected = requestList.getSelectedItems();
                for (var i = 0; i < requestsSelected.length; i++) {
                    var iconaCiclica = requestsSelected[i].getIcon();
                    if (iconaCiclica === "sap-icon://time-entry-request" || iconaCiclica === "sap-icon://bed") {
                        mezzeGiornate.push(requestsSelected[i]);
                    } else {
                        giornateIntere.push(requestsSelected[i]);
                    };
                };
                if (mezzeGiornate.length > 2 || giornateIntere.length > 1 || (mezzeGiornate.length > 0 && giornateIntere.length > 0)) {
                    sap.m.MessageBox.alert(this._getLocaleText("TROPPE_RICHIESTE"), {
                        title: this._getLocaleText("ATTENTION")
                    });
                    var idRequestChoiche = requestChoice.sId;
                    requestList.setSelectedItemById(idRequestChoiche, false);
                };
            };
        };
    },

    onConfirmSaveRequest: function (evt) {
        if (evt === "YES") {
            var fSuccess = function (result) {
                this.oDialog.setBusy(false);
                if (this.visibleModel.getProperty("/consultantProposed")) {
                    //invio richiesta consulente proposto
                    
                    var fSuccessBis = function (result) {
                        // update idScambio primo consulente
                        
                        var fSuccessUpdate = function (result) {
                            console.log("OK");
                            this.router.navTo("modifyRequestsList");
                        };
                        
                        var fErrorUpdate = function (err) {
                            this.oDialog.setBusy(false);
                            sap.m.MessageBox.alert(err, {
                                title: this._getLocaleText("error")
                            });
                        };
                        
                        fSuccessUpdate = _.bind(fSuccessUpdate, this);
                        fErrorUpdate = _.bind(fErrorUpdate, this);
                        
                        console.log(JSON.parse(result));
                        this.idScambioConsProp = JSON.parse(result).idRecord;
                        model.SchedulingModifyRequests.updateIdScambio(this.idScambio, this.idScambioConsProp)
                            .then(fSuccessUpdate, fErrorUpdate)
                    }
                    
                    var fErrorBis = function (err) {
                        this.oDialog.setBusy(false);
                        sap.m.MessageBox.alert(err, {
                            title: this._getLocaleText("error")
                        });
                    }
                    
                    fSuccessBis = _.bind(fSuccessBis, this);
                    fErrorBis = _.bind(fErrorBis, this);
                    
                    this.idScambio = JSON.parse(result).idRecord;
                    model.SchedulingModifyRequests.sendRequest(this.idScambio,this.giorno, this.note, "PROPOSED", this.dataInserimentoModifica, this.pm, this.idPeriod, this.codAttivitaRichiesta, this.dataRichiestaModifica, this.getView().byId("idCodConsulenteProposto").getValue(), this.codCommessaPartenza, this.codCommConsProp)
                        .then(fSuccessBis, fErrorBis);
                } else {
                    this.router.navTo("modifyRequestsList");
                }
            };
            var fError = function (err) {
                this.oDialog.setBusy(false);
                sap.m.MessageBox.alert(err, {
                    title: this._getLocaleText("error")
                });
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            model.SchedulingModifyRequests.sendRequest(this.codConsulenteProposto, this.giorno, this.note, "PROPOSED", this.dataInserimentoModifica, this.pm, this.idPeriod, this.codAttivitaRichiesta, this.dataRichiestaModifica, this.codConsulente, this.codCommessa, this.codCommessaPartenza)
                .then(fSuccess, fError);
        } else {
            this.leggiTabella(this.ric);
        }
    },

    _clearInputValue: function () {
        this.getView().byId("chooseConsultantInput").setValue("");
        this.getView().byId("idCodConsulente").setValue("");
        this.getView().byId("idPeriodSelect").setValue("");
        this.getView().byId("idPeriodSelect").setSelectedKey("");
        this.getView().byId("idProjectSelect").setValue("");
        this.getView().byId("idProjectSelect").setSelectedKey("");
        this.getView().byId("idConsultantProposedInput").setValue("");
        this.getView().byId("idCodConsulenteProposto").setValue("");
        this.getView().byId("idNote").setValue("");
    },

    readSchedConsultantProposed: function (req) {
        var defer = Q.defer();

        var fSuccess = function (results) {
            console.log(results);
            var schedulazioni = this.uniqueElement(results);

            //            this.schedulingConsultantProposed = schedulazioni;
            //            return schedulazioni;

            jQuery.sap.delayedCall(500, this, function () {
                this.oDialogTable.setBusy(false);
            });
            defer.resolve(schedulazioni);
        };
        var fError = function (err) {
            console.log(err);
            this.oDialogTable.setBusy(false);
            sap.m.MessageToast.show(err);
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialogTable.setBusy(true);
        model.tabellaSchedulingOfficial.readTable(req)
            .then(fSuccess, fError);
        return defer.promise;
    },

    readProjects: function (close) {
        var defer = Q.defer();

        var fSuccess = function (result) {
            _.remove(result.items, {
                'statoCommessa': "CHIUSO"
            });
            result.items = _.where(result.items, {
                'codPjmAssociato': this.user.codice_consulente
            });

            defer.resolve(result.items);
            this.oDialogTable.setBusy(false);

        };
        var fError = function () {
            sap.m.MessageBox.alert("Errore in lettura periodo", {
                title: "Attenzione"
            });
            defer.reject(err);
            this.oDialogTable.setBusy(false);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialogTable.setBusy(true);
        model.Projects.readProjects()
            .then(fSuccess, fError);

        return defer.promise;
    },
});