jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.Projects");
jQuery.sap.require("model._ProjectsStatus");
jQuery.sap.require("model.BuList");
jQuery.sap.require("model.Consultants");

view.abstract.AbstractController.extend("view.ProjectModify", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.detailCommessa = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailCommessa, "detailCommessa");

        this.statoCommessa = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.statoCommessa, "statoCommessa");

        this.pjm = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pjm, "pjm");

        this.buModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.buModel, "buAssociate");


    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);

        var name = evt.getParameter("name");
        if (name !== "projectModify") {
            return;
        }

        this.oDialog = this.getView().byId("idProjectModify");

        this.state = evt.getParameters().arguments.state;

        var ferror = function (err) {
            this.oDialog.setBusy(false);
            sap.m.MessageBox.alert(err, {
                title: this._getLocaleText("error")
            });
        };

        var fsuccessStatus = function (result) {
            this.statoCommessa.setProperty("/status", result);
            var buComboBox = this.getView().byId("statoCommessa");
            if(this.state === "new") {
                buComboBox.setValue("APERTO");
            }
        };

        var fsuccessConsultants = function (result) {
        	_.remove(result, {stato: 'CHIUSO'});
            var pjmList = _.sortBy(result, 'cognomeConsulente');
            this.pjm.setProperty("/pjmCommessa", pjmList);
            this.oDialog.setBusy(false);
        };

        var fSuccessBu = function (result) {
            this.buModel.setData(result);
        };

        var stato = "APERTO";
        var getBuList = model.BuList.getBuListByStatus(stato);
//        var getBuList = model.BuList.getBuList();

        ferror = _.bind(ferror, this);
        fsuccessStatus = _.bind(fsuccessStatus, this);
        fsuccessConsultants = _.bind(fsuccessConsultants, this);
        fSuccessBu = _.bind(fSuccessBu, this);


        var req = {};
        req = "CP00000001";
//        req.COD_PROFILO_CONSULENTE = "CP00000001";
        this.oDialog.setBusy(true);
        model._ProjectsStatus.getStauts()
            .then(fsuccessStatus, ferror)
            .then(model.Consultants.getConsultants(req)
                .then(fsuccessConsultants, ferror))
            .then((getBuList)
                .then(fSuccessBu, ferror));

        if (this.state === "new") {
            this.detailCommessa.setProperty("/codCommessa", "");
            this.detailCommessa.setProperty("/nomeCommessa", "");
            this.detailCommessa.setProperty("/descrizioneCommessa", "");
            this.detailCommessa.setProperty("/codPjmAssociato", "");
            this.detailCommessa.setProperty("/codiceStaffCommessa", "");
            this.detailCommessa.setProperty("/statoCommessa", "");
            this.detailCommessa.setProperty("/nomeConsulente", "");
            this.detailCommessa.setProperty("/cognomeConsulente", "");
            this.detailCommessa.setProperty("/descrizione_bu", "");
            this.detailCommessa.setProperty("/cod_bu", "");
        } else {
            var commessa = JSON.parse(sessionStorage.getItem("selectedProject"));
            this.detailCommessa.setProperty("/codCommessa", commessa.codCommessa);
            this.detailCommessa.setProperty("/nomeCommessa", commessa.nomeCommessa);
            this.detailCommessa.setProperty("/descrizioneCommessa", commessa.descrizioneCommessa);
            this.detailCommessa.setProperty("/codPjmAssociato", commessa.codPjmAssociato);
            this.detailCommessa.setProperty("/codiceStaffCommessa", commessa.codiceStaffCommessa);
            this.detailCommessa.setProperty("/statoCommessa", commessa.statoCommessa);
            this.detailCommessa.setProperty("/nomeConsulente", commessa.nomeConsulente);
            this.detailCommessa.setProperty("/cognomeConsulente", commessa.cognomeConsulente);
            this.detailCommessa.setProperty("/cognomeNome", commessa.cognomeConsulente + " " + commessa.nomeConsulente);
            this.detailCommessa.setProperty("/descrizioneBu", commessa.descrizione_bu);
            this.detailCommessa.setProperty("/codiceBu", commessa.cod_bu);
        }
    },

    cancelModifyPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_CANCEL_MODIFY_PROJECT"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_CANCEL_MODIFY_TITLE_PROJECT"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmCancelPress, this)
        );
    },

    saveProjectPress: function () {
        if (this.detailCommessa.getProperty("/nomeCommessa") === "") {
            sap.m.MessageBox.show(this._getLocaleText("WRITE_PROJECT_NAME"),
                sap.m.MessageBox.Icon.ALLERT,
                this._getLocaleText("WRITE_PROJECT_NAME_TITLE")
            );
            return;
        }
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_SAVE_PROJECT"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_SAVE_PROJECT"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmSavePress, this)
        );
    },

    _confirmCancelPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {
            this._clearInputValue();
            this.router.navTo("projectList");
        }
    },

    _confirmSavePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return
        } else {

            var fSuccessAnag = function () { 
                this._clearInputValue();
                this.oDialog.setBusy(false);
                this.router.navTo("projectList");
            };
            var fSuccess = function () {
                //                this.readProjects();
                // funzione che cambia PJM in anag STAFF
                model.Projects.updateAnagStaff(this.detailCommessa.getProperty("/codCommessa"),codCons)
                    .then(fSuccessAnag, fError);
//                this._clearInputValue();
//                this.oDialog.setBusy(false);
//                this.router.navTo("projectList");
            };
            
            var fError = function () {
                console.log("errore nel salvataggio");
                this.oDialog.setBusy(false);
            };

            fSuccessAnag = _.bind(fSuccessAnag, this);
            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            var codCons = _.find(this.pjm.getData().pjmCommessa, {
                'nomeCognome': this.getView().byId("codPjmAssociato").getProperty("value")
            });
            //codCons = codCons.codConsulente;
            codCons = this.getView().byId("codPjmAssociato").getSelectedKey();

            if (this.state === "new") {
                this.oDialog.setBusy(true);
                model.Projects.addProject(this.getView().byId("nomeCommessa").getProperty("value"), this.getView().byId("descrizioneCommessa").getProperty("value"), codCons, "", this.getView().byId("statoCommessa").getProperty("value"), this.detailCommessa.getData().codiceBu)
                    .then(fSuccess, fError);
            } else {
                this.oDialog.setBusy(true);
                model.Projects.updateProject(this.detailCommessa.getProperty("/codCommessa"), this.getView().byId("nomeCommessa").getProperty("value"), this.getView().byId("descrizioneCommessa").getProperty("value"), codCons, this.detailCommessa.getProperty("/codiceStaffCommessa"), this.getView().byId("statoCommessa").getProperty("value"), this.detailCommessa.getData().codiceBu)
                    .then(fSuccess, fError);
            }
        }
    },

    _clearInputValue: function () {
        this.getView().byId("codCommessa").setValue("");
        this.getView().byId("nomeCommessa").setValue("");
        this.getView().byId("descrizioneCommessa").setValue("");
        this.getView().byId("statoCommessa").setSelectedKey("");
        this.getView().byId("codPjmAssociato").setSelectedKey("");
        this.getView().byId("buAssociata").setSelectedKey("");
    },




});