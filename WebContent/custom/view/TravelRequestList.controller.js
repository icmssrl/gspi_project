jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.TravelReq");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("utils.Formatter");

view.abstract.AbstractMasterController.extend("view.TravelRequestList", {

  onInit: function () {
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

    this.travelReqModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.travelReqModel, "travelReqModel");

    this.uiModel.setProperty("/searchProperty", ["nomeCognome"]);
    
    this.travelFilterModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.travelFilterModel, "travelFilterModel");    
  },

  handleRouteMatched: function (evt) {

    var name = evt.getParameter("name");

    if (name !== "travelRequestList") {
      return;
    }
    view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
      
    this.readTravelRequests();
    this.initializeFilter();

  },
    
    readTravelRequests: function(){
        fSuccess = function(result){
            console.log(result);
            //filtro per richieste non ancora confermate
            var notConfirmed = _.filter(result.items, {
                "confermato" : "0"
            });
            var eliminati;
            //filtro per richieste che sono meno vecchie di una settimana fa
            for(var i = 0; i < notConfirmed.length; i++){
                if(this.getOneWeek(notConfirmed[i].dataInserimento) === false){
                    eliminati = _.remove(notConfirmed, {
                        "dataInserimento": notConfirmed[i].dataInserimento
                    });
                    i = i-1;
                }
            };
            //assegno tutto al model
            for(var j = 0; j < notConfirmed.length; j++){
                if(!!notConfirmed[j].tipoSpesaCod && notConfirmed[j].tipoSpesaCod === "HTLF"){
                    notConfirmed[j].isHotel = true;
//                    notConfirmed[j].dataCheckIn = notConfirmed[j].dataCheckIn.substring(8) + "/" + notConfirmed[j].dataCheckIn.substring(5,7) + "/" + notConfirmed[j].dataCheckIn.substring(0,4);
//                    notConfirmed[j].dataCheckOut = notConfirmed[j].dataCheckOut.substring(8) + "/" + notConfirmed[j].dataCheckOut.substring(5,7) + "/" + notConfirmed[j].dataCheckOut.substring(0,4);
                }else{
                    notConfirmed[j].isHotel = false;
                }
                
            };
            this.travelReqModel.setProperty("/travelReq", notConfirmed);
            
        };
        
        fError = function(err){
            console.log(err);
        };
        
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        
        model.TravelReq.getTravelReq()
            .then(fSuccess, fError);
    },
    
    getOneWeek: function(date){
        var result = false;
        
        var mesi30 = ["04", "06", "09", "11"];
        var mesi31 = ["01", "03", "05", "07", "08", "10", "12"];
        var mesi28 = ["02"];
        //prima calcolo la data di collegamento 7 giorni all'indietro
        var today = new Date();
        var day = today.getDate();
        var year = today.getFullYear();
        var month = "";
        var mesePrecedente;
        if(today.getMonth() < 9 && today.getMonth() >= 1){
                month = today.getMonth() + 1;
                month =  "0" + month;
                mesePrecedente = today.getMonth();
                mesePrecedente = "0" + mesePrecedente;
            }else if(today.getMonth() === 0){
                month = today.getMonth() + 1;
                month =  "0" + month;
                mesePrecedente = "12"; 
            }else{
                month = today.getMonth() + 1;
                mesePrecedente = today.getMonth() ; 
            };
        var ggSettimanaFa;
        var meseSettimanaFa;
        var annoSettimanaFa;
        if(day > 7){
            ggSettimanaFa = day - 7;
            meseSettimanaFa = month;
            annoSettimanaFa = year;
        }else if(this.findInArray(mesePrecedente, mesi30)){
            ggMesePrecedente = 7-day;
            ggSettimanaFa = 30-ggMesePrecedente;
            meseSettimanaFa = mesePrecedente;
            annoSettimanaFa = year;
        }else if(this.findInArray(mesePrecedente, mesi31) && mesePrecedente !== "12"){
            ggMesePrecedente = 7-day;
            ggSettimanaFa = 31-ggMesePrecedente;
            meseSettimanaFa = mesePrecedente;
            annoSettimanaFa = year;
        }else if(this.findInArray(mesePrecedente, mesi31) && mesePrecedente === "12"){
            ggMesePrecedente = 7-day;
            ggSettimanaFa = 31-ggMesePrecedente;
            meseSettimanaFa = mesePrecedente;
            annoSettimanaFa = year-1;
        }else if(this.findInArray(mesePrecedente, mesi28)){
            ggMesePrecedente = 7-day;
            ggSettimanaFa = 28-ggMesePrecedente;
            meseSettimanaFa = mesePrecedente;
            annoSettimanaFa = year;
        }
        
        giornoSettimanaFa = annoSettimanaFa + "/" + meseSettimanaFa + "/" + ggSettimanaFa;
        
        //dopo vedo se la data inserimento della richiesta è dopo questa data
        
        if(parseInt(annoSettimanaFa) < parseInt(date.substring(0,4))){ //l'anno di una settimana fa è minore dell'anno in cui ho inserito la ric
            result = true;
        }else if(parseInt(meseSettimanaFa) < parseInt(date.substring(5,7))){
            result = true;

        }else if(parseInt(ggSettimanaFa) < parseInt(date.substring(8)) && parseInt(meseSettimanaFa) == parseInt(date.substring(5,7)) ){
            result = true;

        }
        return result;
        
    },
    
    onHomePress: function(){
        this.router.navTo("launchpad");
    },
        
    findInArray: function(elem, array){
        var result;
        for(var i = 0; i < array.length; i++){
            if (elem === array[i]){
                result = true;
                break;
            }else{
                result = false;
            }
        }
        return result;
    },
    
    onRequestSelect: function (evt) {
    var src = evt.getSource();
    var selectedItem = src.getSelectedItems()[0].getBindingContext("travelReqModel").getObject();
    sessionStorage.setItem("selectedTravelReq", JSON.stringify(selectedItem));
    this.router.navTo("travelRequestDetail", {
      detailId: selectedItem.idRichiesta
    });
  },

  
  //----------------Filter------------------------------------------------------------
  onFilterPress:function(evt)
  {
	  if(!this.filterDialog){
		  this.filterDialog = sap.ui.xmlfragment("view.dialog.travelFilterDialog", this);
		  var page = this.getView().byId("TravelRequestListId");
		  page.addDependent(this.filterDialog);
	  }
	
	  this.filterDialog.open();
  },
  onOKFilterPress: function(evt)
  {
	  
	  var fSuccess = function(res)
	  {
		  this.travelReqModel.setProperty("/travelReq", res.items);
		  this.travelReqModel.refresh();
		  this.filterFunction();
	  }
	  fSuccess = _.bind(fSuccess, this);
	  
	  var fError = function(err)
	  {
		  console.log("Error");
	  }
	  fError = _.bind(fError, this);
	  
	  var filterFunction = function()
	  {
		  var filters = [];
		  for(var prop in this.travelFilterData)
		  {
			  if(this.travelFilterData[prop].selected)
			  {
				 switch(prop)
				{
					 case "confermato":
						  if(this.travelFilterData[prop].value)
							{
								  filters.push(new sap.ui.model.Filter("confermato", sap.ui.model.FilterOperator.EQ, this.travelFilterData[prop].value));
							}
						  break;
					
					 case "dataInserimento":
						 if(this.travelFilterData[prop].fromDate !== null)
						{
							 filters.push(new sap.ui.model.Filter({path:prop,  
								 test : _.bind(function(value){
									 var dateVal = new Date(value);
									 var fromDate = new Date(this.travelFilterData[prop].fromDate);
									 if(dateVal.getFullYear() == fromDate.getFullYear())
									{
										 if(dateVal.getMonth() == fromDate.getMonth())
										{
											 return dateVal.getDate() >= fromDate.getDate();
										}
										 else
										{
											return dateVal.getMonth() > fromDate.getMonth(); 
										}
											 
									}
									 else
										 return dateVal.getFullYear() > fromDate.getFullYear();
									
							 }, this)}));
							 
						}
						 if(this.travelFilterData[prop].toDate !== null)
						{
							 filters.push(new sap.ui.model.Filter({path:prop,  
								 test : _.bind(function(value){
									 var dateVal = new Date(value);
									 var toDate = new Date(this.travelFilterData[prop].toDate);
									 if(dateVal.getFullYear() == toDate.getFullYear())
									{
										 if(dateVal.getMonth() == toDate.getMonth())
										{
											 return dateVal.getDate() <= toDate.getDate();
										}
										 else
										{
											return dateVal.getMonth() < toDate.getMonth(); 
										}
											 
									}
									 else
										 return dateVal.getFullYear() < toDate.getFullYear();
								 
							 }, this)}));
							 
						}
					}
				
			  	}
			
		  }
		  var list =  this.getView().byId("list");
		  
		  list.getBinding("items").filter([new sap.ui.model.Filter({filters:filters, and:true})]);
		  this.filterDialog.close();
	  }
	  this.filterFunction = _.bind(filterFunction, this);
	  
	  
	  model.TravelReq.getTravelReq()
      .then(fSuccess, fError);
	  
  },
  onResetFilterPress:function()
  {
	  
	  this.filterDialog.close();
	  this.initializeFilter();
	  var list =  this.getView().byId("list");
	  
	  list.getBinding("items").filter([]);
	  this.readTravelRequests();
  },
  onCloseFilterPress:function()
  {
	  this.filterDialog.close();
  },
  initializeFilter:function(evt)
  {
  							
	  this.travelFilterData = {
			  "confermato":
			  	{"selected":false,
				  "value":""
				  },
				"dataInserimento":
				{
					"selected":false,
					"fromDate":null,
					"toDate":null
				}
				
		  
	  };
	  this.travelFilterModel.setData(this.travelFilterData);
  },
  
  onConfirmedFilterStateSelect:function(evt)
  {
	  var src =evt.getSource();
	  var btn = src.getAggregation("buttons")[evt.getParameter("selectedIndex")];
	  switch(btn.getText()){
	  case model.i18n.getText("notConfirmedBtnTxt"):
		  this.travelFilterData.confermato.value="0";
	  	  break;
	  case model.i18n.getText("confirmedBtnTxt"):
		  this.travelFilterData.confermato.value="1";
	  	  break;
	  case model.i18n.getText("allBtnTxt"):
		  this.travelFilterData.confermato.value="";
	  	  break;
	  	  
	  	
	  }
	  this.travelFilterModel.refresh();
  },
  //--------------------------------------------------------------------------------------


});