jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.RequestTypes");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("sap.m.MessageToast");

view.abstract.AbstractMasterController.extend("view.RequestTypeList", {

  onInit: function () {
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    this.modelTipiRichiesta = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.modelTipiRichiesta, "modelTipiRichiesta");

    this.uiModel.setProperty("/searchProperty", ["descrizioneTipoRichiesta"]);
  },

  handleRouteMatched: function (evt) {

    var name = evt.getParameter("name");

    if (name !== "requestTypeList") {
      return;
    }
    view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
    this.getView().byId("list").removeSelections();
    this.oDialog = this.getView().byId("list");

    this.readRequestType();
  },

  readRequestType: function () {
    var fSuccess = function (result) {
      this.modelTipiRichiesta.setProperty("/tipiRichiesta", result.items);
      console.log(result);
      this.oDialog.setBusy(false);
    };
    var fError = function (err) {
      this.oDialog.setBusy(false);
      console.log(JSON.parse(err.response.body).error.message.value);
    };

    fSuccess = _.bind(fSuccess, this);
    fError = _.bind(fError, this);

    this.oDialog.setBusy(true);
    model.RequestTypes.readRequestTypes()
      .then(fSuccess, fError);
  },

  onLaunchpadPress: function () {
    this.router.navTo("launchpad");
    if (sessionStorage.getItem("selectedRequestType")) {
      sessionStorage.removeItem("selectedRequestType");
    };
  },

  onFilterPress: function () {
    var prova = [{
      "value": "APERTO"
        }, {
      "value": "CHIUSO"
        }];

    var page = this.getView().byId("listaTipoRichiesta");
    if (!this.dataDialog) {
      this.dataDialog = sap.ui.xmlfragment("view.dialog.filterDialogProjects", this);
    };

    this.tempModelData = new sap.ui.model.json.JSONModel();
    this.tempModelData.setData(prova);
    this.getView().setModel(this.tempModelData, "filter");
    page.addDependent(this.dataDialog);

    this.dataDialog.setBusy(true);
  },

  onFilterDialogClose: function () {
    this.dataDialog.setBusy(false);
  },

  onFilterDialogOK: function () {
    this.dataDialog.setBusy(false);

    this.dataDialog.destroy();
    delete(this.dataDialog);

    var filtro = new sap.ui.model.Filter('statoCommessa', sap.ui.model.FilterOperator.Contains, this.arrayFiltri[0])
    var table = this.getView().byId("list");
    var binding = table.getBinding("items");
    binding.filter(filtro);
  },

  onFilterDialogRipristino: function () {
    this.dataDialog.setBusy(false);

    this.dataDialog.destroy();
    delete(this.dataDialog);

    var table = this.getView().byId("list");
    var binding = table.getBinding("items");
    binding.filter();
  },

  filterPress: function (evt) {
    console.log(evt);
    var numeroFiltri = evt.getSource().getSelectedItems();
    var arrayFiltri = [];
    for (var i = 0; i < numeroFiltri.length; i++) {
      arrayFiltri.push(numeroFiltri[i].getProperty("title"));
    }
    this.arrayFiltri = arrayFiltri;
    console.log(arrayFiltri);
  },

  addRequestTypePress: function () {
    this.router.navTo("requestTypeModify", {
      state: "new"
    });
    if (sessionStorage.getItem("selectedRequestType")) {
      sessionStorage.removeItem("selectedRequestType");
    };
  },

  //    onRequestTypeSelect: function (evt) {
  //        var src = evt.getSource();
  //        var selectedItem = src.getBindingContext("modelTipiRichiesta").getObject();
  //        sessionStorage.setItem("selectedRequestType", JSON.stringify(selectedItem));
  //        this.router.navTo("requestTypeDetail", {
  //            detailId: selectedItem.codiceTipoRichiesta
  //        });
  //    },

  onRequestTypeSelectBis: function (evt) {
    var src = evt.getSource();
    var selectedItem = src.getSelectedItems()[0].getBindingContext("modelTipiRichiesta").getObject();
    sessionStorage.setItem("selectedRequestType", JSON.stringify(selectedItem));
    this.router.navTo("requestTypeDetail", {
      detailId: selectedItem.codiceTipoRichiesta
    });
  },

});