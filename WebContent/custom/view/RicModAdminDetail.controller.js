jQuery.sap.require("utils.Formatter");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("model.PeriodsModifyRequest");
jQuery.sap.require("model.tabellaSchedulingMaria");

view.abstract.AbstractController.extend("view.RicModAdminDetail", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.detaiRichiesteModifiche = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detaiRichiesteModifiche, "detaiRichiesteModifiche");

        this.detaiRichiesteModificheScambio = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detaiRichiesteModificheScambio, "detaiRichiesteModificheScambio");

        this.visibleScambio = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleScambio, "visibleScambio");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "ricModAdminDetail") {
            return;
        };
        this.oDialog = this.getView().byId("idApproveRequestDetail");

        this.requestDetail = JSON.parse(sessionStorage.getItem("selectedRequestApprove"));
        this.idReq = this.requestDetail.idReqModifica;

        this.detaiRichiesteModifiche.setData(this.requestDetail);
        this.visibleScambio.setProperty("/scambio", false);

        this.requestDetailScambio = JSON.parse(sessionStorage.getItem("selectedRequestApproveScambio"));
        if (this.requestDetailScambio) {
            this.idReqScambio = this.requestDetailScambio.idReqModifica;
            this.detaiRichiesteModificheScambio.setData(this.requestDetailScambio);
            this.visibleScambio.setProperty("/scambio", true);
        }
    },

    discardRequest: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_DISCARD_REQUEST"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_DISCARD_REQUEST_TITLE"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmDiscardPress, this)
        );
    },

    approveRequest: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_APPROVE_REQUEST"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_APPROVE_REQUEST_TITLE"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmApprovePress, this)
        );
    },

    _confirmDiscardPress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var fSuccessWriteFinal = function () {
                if (this.requestDetailScambio && this.giapassato) {
                    this.giapassato = false;
                    var idReq = this.idReqScambio;
                    var stato = "DELETED";
                    model.PeriodsModifyRequest.discardRequest(idReq, stato)
                        .then(fSuccess, fError);
                } else {
                    this.oDialog.setBusy(false);
                    this.router.navTo("ricModAdminList");
                }
            };

            var fSuccessProposed = function () {
                this.oDialog.setBusy(false);
                this.router.navTo("ricModAdminList");
            };

            var fErrorWriteFinal = function (err) {
                console.log("errore nel salvataggio tabella FINAL");
                var idReq = this.idReq;
                var stato = "PROPOSED";
                model.PeriodsModifyRequest.discardRequest(idReq, stato)
                    .then(fSuccessProposed, fError);
            };

            var fSuccess = function () {
                var results = {};
                results.idReq = this.requestDetail.idReqModifica;
                results.codConsulente = this.requestDetail.codiceConsulenteModifica;
                results.dataReqPeriodo = utils.Formatter.dateToHana(this.requestDetail.dataRichiestaModifica);
                results.idCommessa = this.requestDetail.codCommessaModifica;
                results.codAttivitaRichiesta = this.requestDetail.codAttivitaRichiestaModifica;
                results.idPeriodoSchedulingRic = this.requestDetail.idPeriodoSchedModifica;
                results.codPjm = this.requestDetail.codPjmModifica;
                results.dataInserimento = utils.Formatter.dateToHana(this.requestDetail.dataInserimentoModifica);
                results.note = "";
                results.giornoPeriodo = this.requestDetail.giornoPeriodoModifica;
                results.statoReq = "DELETED";

                model.PeriodsModifyRequest.writeSchedulingPeriodsFinal(results)
                    .then(fSuccessWriteFinal, fErrorWriteFinal);
            };
            var fError = function () {
                this.oDialog.setBusy(false);
                console.log("errore nel salvataggio tabella MODIFICA");
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            fSuccessWriteFinal = _.bind(fSuccessWriteFinal, this);
            fErrorWriteFinal = _.bind(fErrorWriteFinal, this);
            fSuccessProposed = _.bind(fSuccessProposed, this);

            this.oDialog.setBusy(true);
            var idReq = this.idReq;
            var stato = "DELETED";
            this.giapassato = true;
            model.PeriodsModifyRequest.discardRequest(idReq, stato)
                .then(fSuccess, fError);
        }
    },

    _confirmApprovePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var fSuccessWriteFinal = function () {
                if (this.requestDetailScambio && this.giapassatoConf) {
                    this.giapassatoConf = false;
                    var idReq = this.idReqScambio;
                    var stato = "CONFIRMED";
                    model.PeriodsModifyRequest.discardRequest(idReq, stato)
                        .then(fSuccess, fError);
                } else {
                    this.oDialog.setBusy(false);
                    this.router.navTo("ricModAdminList");
                }
            };

            var fSuccessProposed = function () {
                this.oDialog.setBusy(false);
                this.router.navTo("ricModAdminList");
            };

            var fErrorWriteFinal = function (err) {
                console.log("errore nel salvataggio tabella FINAL");
                var idReq = this.idReq;
                var stato = "PROPOSED";
                model.PeriodsModifyRequest.discardRequest(idReq, stato)
                    .then(fSuccessProposed, fError);
            };

            var fSuccess = function () {
                var results = {};
                results.idReq = this.requestDetail.idReqModifica;
                results.codConsulente = this.requestDetail.codiceConsulenteModifica;
                results.dataReqPeriodo = utils.Formatter.dateToHana(this.requestDetail.dataRichiestaModifica);
                results.idCommessa = this.requestDetail.codCommessaModifica;
                results.codAttivitaRichiesta = this.requestDetail.codAttivitaRichiestaModifica;
                results.idPeriodoSchedulingRic = this.requestDetail.idPeriodoSchedModifica;
                results.codPjm = this.requestDetail.codPjmModifica;
                results.dataInserimento = utils.Formatter.dateToHana(this.requestDetail.dataInserimentoModifica);
                results.note = "0";
                results.giornoPeriodo = this.requestDetail.giornoPeriodoModifica;
                results.statoReq = "CONFIRMED";

                model.PeriodsModifyRequest.writeSchedulingPeriodsFinal(results)
                    .then(fSuccessWriteFinal, fErrorWriteFinal);
            };
            var fError = function () {
                this.oDialog.setBusy(false);
                console.log("errore nel salvataggio tabella MODIFICA");
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            fSuccessWriteFinal = _.bind(fSuccessWriteFinal, this);
            fErrorWriteFinal = _.bind(fErrorWriteFinal, this);
            fSuccessProposed = _.bind(fSuccessProposed, this);

            this.oDialog.setBusy(true);
            var idReq = this.idReq;
            var stato = "CONFIRMED";
            this.giapassatoConf = true;
            model.PeriodsModifyRequest.discardRequest(idReq, stato)
                .then(fSuccess, fError);
        }
    },

});