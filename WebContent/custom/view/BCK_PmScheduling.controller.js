jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.ui.unified.MenuItem");

view.abstract.AbstractController.extend("view.PmScheduling", {
    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.pmSchedulingModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingModel, "pmSchedulingModel");

        this.pmSchedulingConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmSchedulingConsultantModel, "pmSchedulingConsultantModel");

        this.visibleModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.visibleModel, "visibleModel");

        this.noteModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.noteModel, "noteModel");

        this.switchModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.switchModel, "switchModel");

        this.addConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.addConsultantModel, "cModel");
        this.addConsultantModel.setSizeLimit(1000);

        this.genericConsultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.genericConsultantModel, "gModel");

        this.buModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.buModel, "buModel");

        this.userModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.userModel, "userModel");

        this.insertMultiModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.insertMultiModel, "insertMultiModel");

        this.multiModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.multiModel, "multiModel");
        this.multiModel.setProperty("/visibleSceltaCancella", false);

        this.periodModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.periodModel, "periodModel");

        this.pModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pModel, "pModel");

        this.excelModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.excelModel, "excelModel");

        this.skillModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.skillModel, "skillModel");

        this.consultantModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.consultantModel, "consultantModel");

        this.pmFilterModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.pmFilterModel, "pmFilterModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);

        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.userModel.setData(this.user);

        if (!this._checkRoute(evt, "pmScheduling")) {
            return;
        }

        var prefTitle = icms.Component.getMetadata().getConfig().settings.prefTitle;
        document.title = prefTitle + " _ PJM SCHED";

        this.oDialog = this.getView().byId("BlockLayout");
        this.oDialogTable = this.getView().byId("idSchedulazioniTable");
        this.undoButton = this.getView().byId("undo");
        this.getView().byId("idSchedulazioniTable").setVisible(false);
        this.getView().byId("idSelectCommessa").setEnabled(false);
        this.getView().byId("idSelectPriorita").setEnabled(false);
        this.getView().byId("idSelectPriorita2").setEnabled(false);
        this.visibleModel.setProperty("/addConsultantButton", false);
        this.visibleModel.setProperty("/closeProjectButton", false);
        this.visibleModel.setProperty("/enabledButton", false);
        this.visibleModel.setProperty("/filterButton", false);
        this.visibleModel.setProperty("/undoButton", false);
        this.visibleModel.setProperty("/uploadExcel", false);
        this.visibleModel.setProperty("/downloadExcel", false);
        this.switchModel.setProperty("/state", false);
        this.visibleModel.setProperty("/switch", false);
        this.uiModel.setProperty("/searchProperty", ["nomeCognome"]);
        this.uiModel.setProperty("/searchValue", "");
        if (sap.ui.getCore().getElementById("idListaConsulentiPjm")) {
            sap.ui.getCore().getElementById("idListaConsulentiPjm").removeSelections();
        }
        this.getView().byId("idSelectCommessa").setSelectedKey("");
        this.getView().byId("idSelectCommessa").setValue("");
        this.getView().byId("idSelectPriorita").setSelectedKey("");
        this.getView().byId("idSelectPriorita").setValue("");
        this.getView().byId("idSelectPriorita2").setSelectedKey("");
        this.getView().byId("idSelectPriorita2").setValue("");
//        this.getView().byId("filterForProject").setPressed(false);
        this.visibleModel.setProperty("/endReqDateForPeriodSelectedVisible", false);
        this.pmSchedulingModel.getData().giorniTotali = 0;

        this.getView().byId("idFiltroGiornateZero").setText(this._getLocaleText("NascondiConsulentiZeroGiornate"));
        this.getView().byId("idFiltroGiornateZero").setProperty("pressed", false);

        this.allConsultantsForProject = undefined;

        this.visibleModel.setProperty("/chargeButton", false);
        this.getView().byId("refresh").setEnabled(false);

        this.clearPasteCommesse();
        this.fromSaveLocal = true;

        this.downloadCommesseMultiple = false;

        this.oDialog.setBusy(true);
        this.svuotaLocalStorageUndo();
        this.readPeriods();

        //Inserted here filter lists Data in background//
        this._loadFilters();
        //************************************************//
    },

    // funzione per la navigazione alla lauchpad
    onLaunchpadPress: function () {
        model.persistence.Storage.session.remove("periodSelected");
        model.persistence.Storage.session.remove("period");
        this.onNavBackPress();
    },

    // Funzione caricamento periodi
    readPeriods: function () {
        console.time('readPeriods'); // funzione per verificare il tempo che impiega la funzione ->START
        var fSuccess = function (result) {
            var res = {};
            res.items = _.sortByOrder(result.items, ['annoPeriodo', 'mesePeriodo'], ['desc', 'desc']);
            var res13 = {};
            res13.items = [];
            for (var i = 0; i < 13; i++) {
                if (res.items[i]) {
                    res13.items.push(res.items[i]);
                }
            }
            this.console("Periods: " + res13.items.length, "success");
            this.console(res13.items);
            this.periodModel.setData(res13);
            this.getView().byId("idSelectPeriodo").setSelectedKey("");
            this.getView().byId("idSelectPeriodo").setValue("");
            console.timeEnd('readPeriods'); // funzione per verificare il tempo che impiega la funzione ->END
            var req = {};
            req.codPjm = this.user.codice_consulente;
            req.stato = "APERTO";
            this.readProjects(req);
        };
        var fError = function () {
            if (!this.oneTimeReadPeriods) {
                // funzione che ripete la chiamata di login nel caso in cui sia scaduta l'autenticazione al sistema
                this.oneTimeReadPeriods = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(this.readPeriods(), this));
            } else {
                this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'getPeriods'");
            }
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Periods().getPeriods()
            .then(fSuccess, fError);
    },

    // Funzione caricamento commesse
    readProjects: function (req) {
        console.time('readProjects'); // funzione per verificare il tempo che impiega la funzione ->START
        var fSuccess = function (result) {
            this.allCommesse = result.items;
            result.items = _.uniq(result.items, 'codCommessa');
            this.pmSchedulingModel.setProperty("/commesse", []);
            var commesseOrdinate = _.sortByOrder(result.items, [function (o) {
                return o.nomeCommessa.toLowerCase();
            }]);
            this.pmSchedulingModel.setProperty("/commesse", commesseOrdinate);
            this.console("Projects: " + commesseOrdinate.length, "success");
            this.console(commesseOrdinate);
            var comboCommessa = this.getView().byId("idSelectCommessa");
            if (comboCommessa) {
                comboCommessa.setValue(comboCommessa.getDefaultSelectedItem());
            }
            console.timeEnd('readProjects'); // funzione per verificare il tempo che impiega la funzione ->END
            this.readPriority();
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'projectsViewWithoutPriority'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Projects.readProjectsPerPJM(req)
            .then(fSuccess, fError);
    },

    // Funzione caricamento priorità
    readPriority: function () {
        console.time('readPriority'); // funzione per verificare il tempo che impiega la funzione ->START
        var fSuccess = function (result) {
            this.pmSchedulingModel.setProperty("/priorita", []);
            this.pmSchedulingModel.setProperty("/priorita", result);
            this.console("Priorities: " + result.items.length, "success");
            this.console(result.items);
            console.timeEnd('readPriority'); // funzione per verificare il tempo che impiega la funzione ->END
            this.readCommessePerPjmSet();
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'projectPrioritySet'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.Priorities.readPriorities()
            .then(fSuccess, fError);
    },

    // Funzione che carica le commsse, per estrarre le priorità della stessa per ogni periodo
    readProfiloCommessaPeriodo: function (period) {
        console.time('readProfiloCommessaPeriodo'); // funzione per verificare il tempo che impiega la funzione ->START
        var fSuccess = function (result) {
            this.profiloCommessaPeriodo = [];
            var commesse = this.pmSchedulingModel.getProperty("/commesse");
            for (var i = 0; i < commesse.length; i++) {
                this.profiloCommessaPeriodo.push(_.find(result.items, {
                    codCommessaPeriodo: commesse[i].codCommessa
                }));
            }
            this.console("ProfiloCommessaPeriodo: " + this.profiloCommessaPeriodo.length, "success");
            this.console(this.profiloCommessaPeriodo);
            console.timeEnd('readProfiloCommessaPeriodo'); // funzione per verificare il tempo che impiega la funzione ->END
            this.oDialog.setBusy(false);
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'profiloCommessaPeriodoSet'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.ProfiloCommessaPeriodo.readRecordByPeriod(period)
            .then(fSuccess, fError);
    },

    // Funzione caricamento di tutte le commesse di tutti i consulenti del PJM
    readCommessePerPjmSet: function () {
        console.time('readCommessePerPjmSet'); // funzione per verificare il tempo che impiega la funzione ->START
        var fSuccess = function (result) {
            var arrConsulente = [];
            for (var i = 0; i < result.items.length; i++) {
                if (!_.find(arrConsulente, {
                        cod: result.items[i].codConsulente
                    }))
                    arrConsulente.push({
                        cod: result.items[i].codConsulente
                    });
            }
            for (var t = 0; t < arrConsulente.length; t++) {
                arrConsulente[t].commesse = _.where(result.items, {
                    codConsulente: arrConsulente[t].cod,
                    stato: 'APERTO'
                });
            }
            this.commessePerConsulente = arrConsulente;
            this.oDialog.setBusy(false);
            console.timeEnd('readCommessePerPjmSet'); // funzione per verificare il tempo che impiega la funzione ->END
        };
        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'readCommessePerPjmSet'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var codPjm = this.user.codice_consulente;
        model.CommessePerStaffDelPjm.read(codPjm)
            .then(fSuccess, fError);
    },

    // funzione che, al cambio del periodo, nasconde alcuni tasti, svuota la tabella e le select
    // successivamente popola la tabella con il nuovo periodo selezionato
    onChangePeriod: function (pe) {
        console.time('onChangePeriod'); // funzione per verificare il tempo che impiega la funzione ->START
        this.filteredConsultants = undefined;
        this.oDialog.setBusy(true);
//        this.getView().byId("filterForProject").setPressed(false);
        this.visibleModel.setProperty("/addConsultantButton", false);
        this.visibleModel.setProperty("/closeProjectButton", false);
        this.visibleModel.setProperty("/filterButton", false);
        this.visibleModel.setProperty("/undoButton", false);
        this.visibleModel.setProperty("/downloadExcel", false);
        this.visibleModel.setProperty("/switch", false);
        this.switchModel.setProperty("/state", false);
        this.visibleModel.setProperty("/uploadExcel", false);
        this.pmSchedulingModel.setProperty("/giorniTotali", "0");
        this.svuotaSelect();
        this.svuotaTabella();
        this.getView().byId("refresh").setEnabled(false);

        var table = this.getView().byId("idSchedulazioniTable");

        this.pe = pe;
        var p = pe.getSource().getSelectedItem().getProperty("text").replace(/ /g, '');
        var period = pe.getSource().getProperty("selectedKey");
        this.period = parseInt(period);
        model.persistence.Storage.session.save("period", period);
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(period)
        });
        //**
        this.periodModel.setProperty("/endReqDateForPeriodSelected", periodSelect.dataFineRichieste);
        this.visibleModel.setProperty("/endReqDateForPeriodSelectedVisible", true);
        //**
        model.persistence.Storage.session.save("periodSelected", periodSelect);
        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();

        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = pInizio;
        var dataF = pFine;

        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

        // ** rendo visibili solo le colonne del periodo selezionato
        var columns = table.getColumns();
        for (var i = 0; i < columns.length; i++) {
            columns[i].setVisible(true);
        }
        for (var i = this.numColonne; i < columns.length - 1; i++) {
            columns[i].setVisible(false);
        }
        // **

        // ** metto i nomi dei giorni sulle colonne
        for (var i = 2; i < columns.length; i++) {
            columns[i - 1].getLabel().mProperties.text = this.arrayGiorni[i - 2];
        }
        // **
        this.pmSchedulingModel.setProperty("/days", this.arrayGiorni);

        this.getView().byId("idSelectCommessa").setEnabled(true);

        this.periodStatus = pe.getSource().getSelectedItem().getProperty("additionalText");

        jQuery.sap.delayedCall(500, this, function () {
            this.oDialog.setBusy(false);
        });
        this.visibleModel.setProperty("/chargeButton", false);
        console.timeEnd('onChangePeriod'); // funzione per verificare il tempo che impiega la funzione ->END
        this.readProfiloCommessaPeriodo(this.period);
    },
    // **

    // **svuoto la tabella
    svuotaTabella: function () {
        console.time('svuotaTabella'); // funzione per verificare il tempo che impiega la funzione ->START
        this.getView().byId("idSchedulazioniTable").setVisible(true);
        this.pmSchedulingModel.setProperty("/schedulazioni", []);
        this.pmSchedulingModel.setProperty("/nRighe", 1);
        this.pmSchedulingModel.getData().schedulazioni = [];
        console.timeEnd('svuotaTabella'); // funzione per verificare il tempo che impiega la funzione ->END
    },

    // **svuoto le select
    svuotaSelect: function (value) {
        console.time('svuotaSelect'); // funzione per verificare il tempo che impiega la funzione ->START
        if (!value) {
            this.getView().byId("idSelectCommessa").setEnabled(false);
            this.getView().byId("idSelectCommessa").setSelectedKey("");
            this.getView().byId("idSelectCommessa").setValue("");
        }
        this.getView().byId("idSelectPriorita").setEnabled(false);
        this.getView().byId("idSelectPriorita").setSelectedKey("");
        this.getView().byId("idSelectPriorita").setValue("");
        this.getView().byId("idSelectPriorita2").setEnabled(false);
        this.getView().byId("idSelectPriorita2").setSelectedKey("");
        this.getView().byId("idSelectPriorita2").setValue("");
        console.timeEnd('svuotaSelect'); // funzione per verificare il tempo che impiega la funzione ->END
    },

    // selezionando una commessa faccio apparire il tasto di caricamento
    onSelectionChangeCommessa: function (evt) {
        this.svuotaTabella();
        this.visibleModel.setProperty("/chargeButton", true);
        this.getView().byId("refresh").setEnabled(false);
        this.chosedCom = evt.getSource().getValue().split(" - ")[0];
        this.pmSchedulingModel.setProperty("/commessa", this.chosedCom);
    },

    // al premere del pulsante faccio nascondere lo stesso
    chargeProjects: function (evt) {
        this.visibleModel.setProperty("/chargeButton", false);
    },

    // funzione che si avvia in automatico premendo un qualsiasi punto dopo aver selezionato la commessa
    // vado a leggermi lo staffo della commessa selezionata
    onChangeCommessa: function (evt) {
        this.oDialog.setBusy(true);
        var comFind = _.find(this.pmSchedulingModel.getData().commesse, {
            nomeCommessa: this.chosedCom
        });
        if (comFind) {
            var commessaScelta = comFind.codCommessa;
            this.codCommessaSelezionata = comFind.codCommessa;
        } else {
            this._enterErrorFunction("alert", "Attenzione", "Commessa '" + this.chosedCom.toUpperCase() + "' non trovata");
            return;
        }
        var fSuccess = function (res) {
            this.currentStaff = _.find(res.items, {
                codCommessa: commessaScelta
            });
            if (!this.currentStaff) {
                this.currentStaff = _.find(this.pmSchedulingModel.getData().commesse, {
                    codCommessa: commessaScelta
                });
            }
            this.console("Staff commessa:", "success");
            this.console(this.currentStaff);
            this._onChangeCommessa(commessaScelta);
            this.clearPasteCommesse();
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore lettura XSJS 'letturaStaffCommessa.xsjs'");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.StaffCommesseView.readStaffCommessa()
            .then(fSuccess, fError);
        this.visibleModel.setProperty("/chargeButton", false);
    },

    // in base al periodo e alla commessa selezionati setto tutti i parametri indispensabili
    // lancio al funzione di lettura schedulazioni per popolare la tabella
    _onChangeCommessa: function (commessaP) {
        this.allConsultantsForProject = undefined;
        this.filteredConsultants = undefined;
//        this.getView().byId("filterForProject").setPressed(false);
        var commessaScelta = commessaP;
        var c = _.find(this.pmSchedulingModel.getData().commesse, {
            codCommessa: commessaScelta
        });
        var comm = model.persistence.Storage.session.save("commessa", c);

        var req = {};
        req.idCommessa = commessaScelta;
        req.idStaff = this.currentStaff.codStaff ? this.currentStaff.codStaff : this.currentStaff.codiceStaffCommessa;
        if (req.idStaff) {
            req.cod_consulente = (this.user).codice_consulente;
            req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
            req.nomeCommessa = this.chosedCom;
            this.ric = req;
            if (!this.currentStaff.codStaff) {
                sap.m.MessageToast.show("Non è presente alcun consulente per questo staff");
            }
        } else {
            this._enterErrorFunction("information", "Informazione", "Creare lo staff per questa commessa!");
            this.visibleModel.setProperty("/addConsultantButton", false);
            this.visibleModel.setProperty("/closeProjectButton", false);
            this.visibleModel.setProperty("/filterButton", false);
            this.visibleModel.setProperty("/undoButton", false);
            this.getView().byId("idSelectPriorita").setEnabled(false);
            this.getView().byId("idSelectPriorita2").setEnabled(false);
            this.visibleModel.setProperty("/enabledButton", false);
            return;
        }
        if (!this.periodStatus || this.periodStatus === "A") {
            this.visibleModel.setProperty("/switch", false);
        } else {
            this.visibleModel.setProperty("/switch", true);
        }
        this.switchModel.setProperty("/state", false);
        this.leggiTabella(req);
        this.visibleModel.setProperty("/visibleTable", true);
        var today = new Date();
        // imposto l'ora per far si che anke nel caso di giorno uguale rilevi che today è inferiore a dataRichiesta
        today.setHours(0);
        var dataRichiesta = new Date(model.persistence.Storage.session.get("periodSelected").reqEndDate);

        if (this.periodStatus === "A" && dataRichiesta > today) {
            this.getView().byId("idSelectPriorita").setEnabled(true);
            this.getView().byId("idSelectPriorita2").setEnabled(true);
        } else {
            this.getView().byId("idSelectPriorita").setEnabled(false);
            this.getView().byId("idSelectPriorita2").setEnabled(false);
        }
        this.getView().byId("addConsultantButton").setEnabled(true);
        this.getView().byId("addGenericConsultantButton").setEnabled(true);
        this.getView().byId("idFilterConsultant").setEnabled(true);

        var commessaSelezionata = this.getView().byId("idSelectCommessa");
        var commSel = commessaSelezionata.getValue();
        var commSelKey = commessaSelezionata.getSelectedKey();
        var nomeCommessa = _.find(this.pmSchedulingModel.getData().commesse, {
            'nomeCommessa': commSel
        });
        var periodoSelezionato = this.period;

        var commessaPerPeriodo = _.find(this.profiloCommessaPeriodo, {
            'idPeriodoSchedulingProf': parseInt(periodoSelezionato),
            'codCommessaPeriodo': commSelKey
        });

        // se le priorità non sono ancora state settate, le imposto a 0 entrambe
        var priorityOne, priorityTwo;
        if (commessaPerPeriodo) {
            var commessaGiusta = commessaPerPeriodo;
            priorityOne = _.find(this.pmSchedulingModel.getData().priorita.items, {
                'codPrioritaCommessa': commessaGiusta.codPrioritaA
            });

            priorityTwo = _.find(this.pmSchedulingModel.getData().priorita.items, {
                'codPrioritaCommessa': commessaGiusta.codPrioritaB
            });
            var stateTrasfert = commessaGiusta.trasferteEstere;
        }
        if (priorityOne) {
            this.getView().byId("idSelectPriorita").setSelectedKey(priorityOne.codPrioritaCommessa);
            this.getView().byId("idSelectPriorita").setValue(priorityOne.codPrioritaCommessa + " : " + priorityOne.descrPriorita);
        } else {
            this.getView().byId("idSelectPriorita").setSelectedKey("0");
            this.getView().byId("idSelectPriorita").setValue("0 : Nessuna");
        }
        if (priorityTwo) {
            this.getView().byId("idSelectPriorita2").setSelectedKey(priorityTwo.codPrioritaCommessa);
            this.getView().byId("idSelectPriorita2").setValue(priorityTwo.codPrioritaCommessa + " : " + priorityTwo.descrPriorita);
        } else {
            this.getView().byId("idSelectPriorita2").setSelectedKey("0");
            this.getView().byId("idSelectPriorita2").setValue("0 : Nessuna");
            this.switchChangeTransfer();
        }

        if (sap.ui.getCore().getElementById("idListaConsulentiPjm")) {
            sap.ui.getCore().getElementById("idListaConsulentiPjm").removeSelections();
        }
        this.oDialog.setBusy(false);
    },

    // funzione che cambia i valori delle priorità qualora venissero cambiate
    switchChangeTransfer: function () {
        this.oDialog.setBusy(true);
        var commessaSelezionata = this.getView().byId("idSelectCommessa");
        var periodoSelezionato = this.period;
        var commessaTrovata = _.find(this.profiloCommessaPeriodo, {
            'codCommessaPeriodo': commessaSelezionata.getSelectedKey(),
            'idPeriodoSchedulingProf': parseInt(periodoSelezionato)
        });

        var priorityA = this.getView().byId("idSelectPriorita").getSelectedKey();
        var priorityB = this.getView().byId("idSelectPriorita2").getSelectedKey();
        var idPeriod = parseInt(periodoSelezionato);
        var codiceStaffComm = _.find(this.pmSchedulingModel.getData().commesse, {
            'codCommessa': commessaSelezionata.getSelectedKey()
        }).codiceStaffCommessa;

        var fSuccess = function () {
            this.readProfiloCommessaPeriodo(this.period);
        };

        var fError = function () {
            this._enterErrorFunction("warning", "Attenzione - errore XSJS", "Errore lettura XSJS 'scritturaProfiloCommessaPeriodo.xsjs'");
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if (commessaTrovata) {
            model.ProfiloCommessaPeriodo.updateRecord(commessaSelezionata.getSelectedKey(), idPeriod, priorityA, priorityB)
                .then(fSuccess, fError);
        } else {
            var stato = 'CHIUSO';
            model.ProfiloCommessaPeriodo.newRecord(commessaSelezionata.getSelectedKey(), idPeriod, codiceStaffComm, stato, priorityA, priorityB)
                .then(fSuccess, fError);
        }
    },

    // in base alla richiesta passata, popolo la tabella con i consulenti e le giornate schedulate
    leggiTabella: function (req) {
        this.oDialogTable.setBusy(true);
        if (this.periodStatus === "A") {
            this.getView().byId("refresh").setEnabled(true);
        }
        var fSuccess = function (results) {
            var schedulazioni = results.resultOrdinato;
            for (var t = 0; t < schedulazioni.length; t++) {
                var commessaConsulente = _.find(_.find(this.commessePerConsulente, {
                    cod: schedulazioni[t].codiceConsulente
                }).commesse, {
                    nomeCommessa: this.chosedCom
                });
                if (commessaConsulente) {
                    schedulazioni[t].teamLeader = commessaConsulente.teamLeader;
                    schedulazioni[t].nomeSkill = commessaConsulente.nomeSkill;
                }
            }
            this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioni);
            this.allScheduling = this.pmSchedulingModel.getProperty("/schedulazioni");
            if (schedulazioni.length > 0 && schedulazioni.length <= 6) {
                this.pmSchedulingModel.setProperty("/nRighe", schedulazioni.length);
            } else if (schedulazioni.length > 6) {
                this.pmSchedulingModel.setProperty("/nRighe", 6);
            } else {
                this.pmSchedulingModel.setProperty("/nRighe", 1);
            }
            setTimeout(_.bind(this._onAfterRenderingTable, this));
        };

        var fSuccessAll = function (results) {
            var schedulazioni = results.resultOrdinato;
            this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioni);
            this.allScheduling = this.pmSchedulingModel.getProperty("/schedulazioni");
            if (schedulazioni.length > 0 && schedulazioni.length <= 6) {
                this.pmSchedulingModel.setProperty("/nRighe", schedulazioni.length);
            } else if (schedulazioni.length > 6) {
                this.pmSchedulingModel.setProperty("/nRighe", 6);
            } else {
                this.pmSchedulingModel.setProperty("/nRighe", 1);
            }
            setTimeout(_.bind(this._onAfterRenderingTable, this));
        };

        var fError = function (err) {
            this.console(err, "error");
            this._enterErrorFunction("error", "Errore", "Errore lettura tabella: " + err);
        };

        fSuccessAll = _.bind(fSuccessAll, this);
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var switchState = this.switchModel.getProperty("/state");
        if (switchState) {
            this.readAllEditedCommesse(req);
        } else {
            if (req.idStaff) {
                model.tabellaScheduling.readTable(req)
                    .then(fSuccess, fError);
            } else {
                model.tabellaScheduling.readPjmAllRequests(req)
                    .then(fSuccessAll, fError);
            }
        }
    },

    // funzione lanciata alla fine di tutto il caricamento
    // colora le celle in grigetto dei sabati e domeniche
    // somma e setta le giornate totali per consulenti e totale complessivo
    _onAfterRenderingTable: function (evt) {
        if (!this.oDialogTable.getBusy()) {
            this.oDialogTable.setBusy(true);
        }
        var table = this.getView().byId("idSchedulazioniTable");
        var rows = table.getRows();

        this._calcoloGiornateTotali();

        var today = new Date();
        // imposto l'ora per far si che anke nel caso di giorno uguale rilevi che today è inferiore a dataRichiesta
        today.setHours(0);
        var dataRichiesta = new Date(model.persistence.Storage.session.get("periodSelected").reqEndDate);

        if (this.periodStatus === "A" && dataRichiesta > today) {
            this.visibleModel.setProperty("/addConsultantButton", true);
            this.visibleModel.setProperty("/closeProjectButton", true);
            this.visibleModel.setProperty("/filterButton", true);
            this.visibleModel.setProperty("/enabledButton", true);
            this.visibleModel.setProperty("/uploadExcel", true);
        } else {
            this.visibleModel.setProperty("/addConsultantButton", false);
            this.visibleModel.setProperty("/closeProjectButton", false);
            this.visibleModel.setProperty("/filterButton", false);
            this.visibleModel.setProperty("/enabledButton", false);
            this.visibleModel.setProperty("/uploadExcel", false);
        }
        this.visibleModel.setProperty("/downloadExcel", true);

        setTimeout(_.bind(function () {
            table.rerender()
            setTimeout(_.bind(function () {
                var arrayCounter = [];
                for (var i = 0; i < rows.length; i++) {
                    var cellsPerRows = rows[i].getAggregation("cells");
                    var g = 0;
                    for (var j = 1; j < cellsPerRows.length - 1; j++) {
                        var cella = cellsPerRows[j];
                        if (_.find(this.weekendDays, _.bind(function (item) {
                                return (item == j);
                            }, this))) {
                            var column = $("#" + cella.getDomRef().id).parent().parent();
                            column.css('background-color', '#e5e5e5');
                        }
                    }
                }
                this.getView().byId("idFiltroGiornateZero").setText(this._getLocaleText("NascondiConsulentiZeroGiornate"));
                this.getView().byId("idFiltroGiornateZero").setProperty("pressed", false);
                this.oDialogTable.setBusy(false);
            }, this));
        }, this));
    },

    /* INIZIO - funzioni per l'aggiunta di un consulente allo staff */
    // funzione per aprire il Dialog per aggiungere i cosnulenti allo staff del progetto
    openAddConsultantDialog: function (evt) {
        if (!this.addConsultantDialog) {
            this.addConsultantDialog = sap.ui.xmlfragment("view.dialog.addConsultantDialog", this);
        }
        var page = this.getView().byId("schedulingPage");
        page.addDependent(this.addConsultantDialog);

        this.loadConsultants()
            .then(_.bind(function (result) {
                    this.addConsultantDialog.open();
                }, this),
                _.bind(function (err) {
                    this.pmSchedulingModel.refresh();
                }, this));
    },

    // carico tutti i consulenti appartenenti alla BU del PJM, rimuovendo quelli già presenti nello staff
    loadConsultants: function (params) {
        var defer = Q.defer();
        var fSuccess = function (result) {
            // rimuovo consulenti già presenti nello staff
            result.items = _.sortBy(result.items, 'cognomeConsulente');
            for (var i = 0; i < this.pmSchedulingModel.getData().schedulazioni.length; i++) {
                _.remove(result.items, {
                    codConsulente: this.pmSchedulingModel.getData().schedulazioni[i].codiceConsulente
                });
            }
            // fine
            this.addConsultantModel.setData(result);
            defer.resolve();
        };

        var fError = function (err) {
            sap.m.MessageToast("Consultant not added.");
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.filtroBu = [];
        this.filtroBu = !!params ? params : [model.persistence.Storage.session.get("user").codConsulenteBu];

        var filterString = "";
        if (this.filtroBu.length > 0) {
            filterString = "?$filter=";
            for (var i = 0; i < this.filtroBu.length; i++) {
                filterString += "COD_CONSULENTE_BU eq '" + this.filtroBu[i] + "'";
                if (i < (this.filtroBu.length - 1)) // Check if last
                {
                    filterString += " or ";
                }
            }
        }
        model.Consultants.readConsultants(filterString)
            .then(fSuccess, fError);

        return defer.promise;
    },

    // filtro di ricerca dei consulenti nel dialog 'addConsultantDialog'
    onSearchConsultant: function (oEvent) {
        var value = oEvent.getSource().getValue();
        var params = ["nomeConsulente", "cognomeConsulente", "codConsulente"];
        var filtersArr = [];
        for (var i = 0; i < params.length; i++) {
            filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
        }
        var oFilter = new sap.ui.model.Filter({
            filters: filtersArr,
            and: false
        });
        var ff = sap.ui.getCore().getElementById("listC");
        var oBinding = ff.getBinding("items");
        oBinding.filter([oFilter]);
    },

    // funzione che costrisce un array di elementi selezionati
    onSelectionChange: function (evt) {
        var oList = evt.getSource();
        this.aContexts = oList.getSelectedContexts(true);
    },

    // funzione lanciata dal comandi 'chiudi' del dialog (addConsultantDialog)
    // chiude il dialog, rimuove eventuali selezioni e svuoto la barra di ricerca
    onFilterDialogClose: function () {
        this.addConsultantDialog.close();
        var list = sap.ui.getCore().getElementById("listC");
        list.removeSelections();
        var oBinding = list.getBinding("items");
        oBinding.filter();
        sap.ui.getCore().getElementById("idSearchAddDialog").setValue("");
    },

    // funzione lanciata  dal comandi 'ok' del dialog (addConsultantDialog)
    // controllo che sia stato selezionato almeno un consulente
    // aggiungo il/i consulente/i selezionato/i
    // chiude il dialog, rimuove eventuali selezioni
    onFilterDialogOK: function (oEvent) {
        var consulentiSchedulati = _.clone(this.pmSchedulingModel.getData().schedulazioni);
        var consultants = [];
        if (this.aContexts && this.aContexts.length) {
            var fSuccess = function (result) {
                sap.m.MessageToast.show(this._getLocaleText("ADDED_CONSULTANT"));
                this.pmSchedulingModel.refresh();
                this.onFilterDialogClose();
                this.readCommessePerPjmSet();
                jQuery.sap.delayedCall(500, this, function () {
                    this.leggiTabella(this.ric);
                });
            };
            var fError = function (err) {
                sap.m.MessageToast.show(err);
                this.onFilterDialogClose();
            };
            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);
            var arrayReq = [];
            for (var i = 0; i < this.aContexts.length; i++) {
                var path = parseInt(this.aContexts[i].getPath().split("/")[2]);
                var req = {};
                var c = model.persistence.Storage.session.get("commessa");
                req.codStaff = c.codiceStaffCommessa;
                req.codConsulenteStaff = this.aContexts[i].getModel().getData().items[path].codConsulente;
                req.codCommessa = c.codCommessa;
                req.codPJMStaff = c.codPjmAssociato;
                arrayReq.push(req);
            }
            if (arrayReq.length > 0) {
                model.insertConsultantInStaff.insert(arrayReq)
                    .then(fSuccess, fError);
            }
        } else {
            sap.m.MessageToast.show(this._getLocaleText("NO_CONSULTANT_CHOSEN"));
        }
    },

    // apertura del filtro dal dialog 'addConsultantDialog'
    // carica tutte le BU presenti in DB
    onFilterPress: function () {
        this.selectBuDialog = sap.ui.xmlfragment("view.dialog.selectBuDialog", this);
        var page = this.getView().byId("schedulingPage");
        page.addDependent(this.selectBuDialog);

        var fSuccess = function (result) {
            this.buModel.setData(result.items);
            var listBu = sap.ui.getCore().getElementById("idListBu");
            listBu.removeSelections();
            var arrListBU = listBu.getItems();
            var buUserLogged = model.persistence.Storage.session.get("user").descBu;
            // seleziono la BU di partenza
            var buPjm = _.find(arrListBU, function (aa) {
                return aa.getProperty("title") === buUserLogged;
            });
            buPjm.setProperty("selected", true);
            //
            this.buContext = "codA";
            this.selectBuDialog.open();
        };

        var fError = function (err) {
            sap.m.MessageToast("error BU");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var stato = "APERTO";
        model.BuList.getBuListByStatus(stato)
            .then(fSuccess, fError);
    },

    // filtro di ricerca delle BU nel dialog 'selectBuDialog'
    handleSearchBuDialog: function (oEvent) {
        var value = oEvent.getParameter("newValue");
        var params = ["descrizioneBu", "codiceBu"];
        var filtersArr = [];
        for (var i = 0; i < params.length; i++) {
            filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
        }

        var oFilter = new sap.ui.model.Filter({
            filters: filtersArr,
            and: false
        });

        var list = sap.ui.getCore().byId("idListBu");
        var oBinding = list.getBinding("items");
        oBinding.filter([oFilter]);
    },

    // funzione che costrisce un array di elementi selezionati
    onSelectionBu: function (evt) {
        var oList = evt.getSource();
        this.buContext = oList.getSelectedContexts(true);
    },

    // funzione lanciata dal comandi 'chiudi' del dialog (selectBuDialog)
    // chiude il dialog, rimuove eventuali selezioni
    handleCloseBuDialog: function () {
        this.selectBuDialog.close();
        var list = sap.ui.getCore().byId("idListBu");
        var oBinding = list.getBinding("items");
        oBinding.filter();
        this.selectBuDialog.destroy(true);
    },

    // funzione lanciata  dal comandi 'ok' del dialog (selectBuDialog)
    // controllo che sia stato selezionato almeno una BU
    // carico i consulenti della/e BU selezionata/e
    // chiude il dialog, rimuove eventuali selezioni 
    handleConfirmBuDialog: function (oEvent) {
        if (this.buContext === "codA") {
            sap.m.MessageToast.show(this._getLocaleText("BU_NOT_CHANGED"));
            return;
        }
        if (this.buContext && this.buContext.length > 0) {
            var buArray = [];
            for (var i = 0; i < this.buContext.length; i++) {
                var path = parseInt(this.buContext[i].getPath().split("/")[1]);
                buArray.push(this.buContext[i].getModel().getData()[path].codiceBu);
            }

            this.loadConsultants(buArray)
                .then(_.bind(function (result) {
                    this.handleCloseBuDialog();
                    this.pmSchedulingModel.refresh();
                }, this));
        } else {
            sap.m.MessageToast.show(this._getLocaleText("NO_BU_CHOSEN"));
        }
    },
    /* FINE - funzioni per l'aggiunta di un consulente allo staff */

    /* INIZIO - funzioni per l'aggiunta di un consulente generico allo staff */
    // funzione per aprire il Dialog per aggiungere il consulente generico
    openGenericConsultant: function (evt) {
        this._genericConsultantDialog = sap.ui.xmlfragment("view.dialog.addGenericConsultant", this);
        var page = this.getView().byId("schedulingPage");
        this.getView().addDependent(this._genericConsultantDialog);

        this.loadGenericConsultants()
            .then(_.bind(function (result) {
                    this._genericConsultantDialog.open();
                    this.pmSchedulingModel.refresh();
                }, this),
                _.bind(function (err) {
                    this.pmSchedulingModel.refresh();
                }, this));
    },

    // carico tutti i consulenti generici
    loadGenericConsultants: function () {
        var defer = Q.defer();
        var fSuccess = function (result) {
            for (var i = 0; i < this.pmSchedulingModel.getProperty("/schedulazioni").length; i++) {
                _.remove(result.items, {
                    codConsulente: this.pmSchedulingModel.getProperty("/schedulazioni")[i].codiceConsulente
                });
            }
            this.genericConsultantModel.setData(result.items);
            this.console("Lista consulenti generici: " + result.items.length, "success");
            this.console(result.items);
            defer.resolve();
        };

        var fError = function (err) {
            sap.m.MessageToast(this._getLocaleText("SKILL_NOT_READ"));
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.getGenericConsSkills.getGenericConsSkills()
            .then(fSuccess, fError);
        return defer.promise;
    },

    // filtro di ricerca delle BU nel dialog 'addGenericConsultant'
    onSearchGenericConsultant: function (oEvent) {
        var value = oEvent.getSource().getValue();
        var params = ["nomeSkill", "descSkill"];
        var filtersArr = [];
        for (var i = 0; i < params.length; i++) {
            filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
        }
        var oFilter = new sap.ui.model.Filter({
            filters: filtersArr,
            and: false
        });
        var ff = sap.ui.getCore().getElementById("listGenCons");
        var oBinding = ff.getBinding("items");
        oBinding.filter([oFilter]);
    },

    // funzione lanciata dal comando 'chiudi' del dialog (addGenericConsultant)
    // chiude il dialog, rimuove eventuali selezioni
    onCancelGenericConsultant: function () {
        if (this._genericConsultantDialog) {
            this.pmSchedulingModel.refresh();
            this._genericConsultantDialog.close();
            this._genericConsultantDialog.destroy(true);
        }
    },

    // funzione che aggiunge alla tabella dei consulenti generici quello selezionato
    onAddGenericConsultant: function (oEvent) {
        var consulentiSchedulati = _.clone(this.pmSchedulingModel.getData().schedulazioni);
        var aContexts = sap.ui.getCore().getElementById("listGenCons").getSelectedItems();

        //        var fSuccess = function (result) {
        //            this.insertGenericInTable(result);
        //        };
        //
        //        var fError = function (err) {
        //            //sap.m.MessageToast.show(err);
        //            //this.onCancelGenericConsultant();
        //        };
        //
        //        fSuccess = _.bind(fSuccess, this);
        //        fError = _.bind(fError, this);  

        if (aContexts.length) {
            var arrayReq = [];
            for (var i = 0; i < aContexts.length; i++) {
                var req = {};
                var obj = aContexts[i];
                var findSkill = _.find(this.genericConsultantModel.getData(), {
                    'descSkill': obj.getProperty("description"),
                    'nomeSkill': obj.getProperty("title")
                });

                if (findSkill) {
                    var skillCons = findSkill.skill;
                    req.codSkill = skillCons;
                    req.nomeSkill = obj.getProperty("title");
                    req.descSkill = obj.getProperty("description");
                    req.codCons = findSkill.codConsulente
                    this.pmSchedulingModel.getData().schedulazioni.push(req);
                    arrayReq.push(req);
                    this.insertGenericInTable(arrayReq);
                } else {
                    sap.m.MessageToast.show("errore funzione 'onAddGenericConsultant'");
                    return;
                }
            }
        }
        //            if (arrayReq.length > 0) {
        //                model.insertGenericConsultant.insert(arrayReq)
        //                    .then(fSuccess, fError);
        //            }
        //        } else {
        //            sap.m.MessageToast.show(this._getLocaleText("NO_GENERIC_CONSULTANT_CHOSEN"));
        //        }
    },


    // funzione che aggiunge allo staff il consulente generico appena creato
    insertGenericInTable: function (input) {
        var fSuccess = function (result) {
            sap.m.MessageToast.show(this._getLocaleText("ADDED_CONSULTANT"));
            this.onCancelGenericConsultant();
            this.readCommessePerPjmSet();

            //chiamata alla tabella stato_consulente_periodo_draft
            var successo = function (resultConsPer) {
                this.console(resultConsPer);
                var risultatiQuestoConsulente = _.find(resultConsPer.items, {
                    'codConsulente': this.input[0].codCons
                });
                //se risultatiQuestoConsulente è vuoto o undefined allora inserisco in tabella
            };

            var errore = function (err) {
                sap.m.MessageToast.show(err);
            };

            successo = _.bind(successo, this);
            errore = _.bind(errore, this);

            var periodo = model.persistence.Storage.session.get("period");

            model.getStatoConsPeriodoDraft.getStatoConsPeriodoDraft(periodo)
                .then(successo, errore);

            jQuery.sap.delayedCall(500, this, function () {
                this.leggiTabella(this.ric);
            });
        };

        var fError = function (err) {
            sap.m.MessageToast.show(err);
            this.onCancelGenericConsultant();
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var req = {};
        var arrayReq = [];
        var obj = input;
        var codCons = input[0].codCons;
        var skill = input[0].codSkill;

        var c = model.persistence.Storage.session.get("commessa");
        req.codStaff = c.codiceStaffCommessa;
        req.codConsulenteStaff = codCons;
        req.codCommessa = c.codCommessa;
        req.codPJMStaff = c.codPjmAssociato;
        req.codSkill = skill;
        req.teamLeader = "NO";
        arrayReq.push(req);

        this.input = input;

        model.insertConsultantInStaff.insert(arrayReq)
            .then(fSuccess, fError);
    },
    /* FINE - funzioni per l'aggiunta di un consulente genrico allo staff */

    /* INIZIO - funzioni per selezionare uno o più consulenti presenti nello staff */
    // apro dialog 'filterConsultantsByPJM' e carico la lista di consulenti presenti nello staff
    onFilterConsultantsByPJMPress: function () {
        var page = this.getView().byId("schedulingPage");
        if (!this.pmDialog) {
            this.pmDialog = sap.ui.xmlfragment("view.dialog.filterConsultantsByPJM_", this);
            //            this.pmDialog = sap.ui.xmlfragment("view.dialog.filterDialogPJM_", this);
            page.addDependent(this.pmDialog);
        }
        this._loadFilterLists();

        var binding = sap.ui.getCore().getElementById("idListaConsulentiPjm").getBinding("items");
        if (binding) {
            binding.filter([]);
        }

        //        var buBinding = sap.ui.getCore().getElementById("idListaBuPjm").getBinding("items");
        //        if (buBinding) {
        //            buBinding.filter([]);
        //        }
        //
        //        var skillBinding = sap.ui.getCore().getElementById("idListaSkillPjm").getBinding("items");
        //        if (skillBinding) {
        //            skillBinding.filter([]);
        //        }

        this.uiModel.setProperty("/searchValue", "");
        this.uiModel.refresh();
        this.pmDialog.open();
    },

    /*************Filter Part**********************/

    /*****Functions To populate List Filter with all fields *******/
    _loadFilters: function () {
        var defer = Q.defer();

        var fSuccess = function (result) {
            this._loadSkills()
                .then(defer.resolve, fError);
        }
        fSuccess = _.bind(fSuccess, this);

        var fError = function (err) {
            sap.m.MessageToast.show(this._getLocaleText("errorLoadingFilterLists"));
            defer.reject(err);
        }
        fError = _.bind(fError, this);

        this._loadConsultants()
            .then(_.bind(function (res) {
                this._loadBuList()
                    .then(_.bind(fSuccess, this), _.bind(fError, this))

            }, this), fError)
        return defer.promise;
    },

    _loadConsultants: function () {
        var defer = Q.defer();
        var fConsultantSuccess = function (result) {
            //            var buUm = model.persistence.Storage.session.get("user").codConsulenteBu;
            var consultantsBu = result.items;
            this.consultantModel.setProperty("/results", consultantsBu);
            defer.resolve(consultantsBu);
        };
        var fConsultantError = function (err) {
            sap.m.MessageToast("error BU");
            defer.reject(err);
        };
        fConsultantSuccess = _.bind(fConsultantSuccess, this);
        fConsultantError = _.bind(fConsultantError, this);

        model.Consultants.readConsultants()
            .then(fConsultantSuccess, fConsultantError);

        return defer.promise;
    },

    _loadBuList: function () {
        var defer = Q.defer();

        var fSuccess = function (result) {
            console.log("-------Bu Load-----------");
            console.log(result);
            this.buList = {
                results: result.items
            };
            this.buModel.setData(this.buList);
            defer.resolve(result);
        }
        fSuccess = _.bind(fSuccess, this);

        var fError = function (err) {
            this.buList = {
                "results": []
            };
            this.buModel.setData(this.buList);
            sap.m.MessageToast.show(this._getLocaleText("errorLoadingBuList"));
            defer.reject(err);
        }
        fError = _.bind(fError, this);

        model.BuList.getBuListByStatus("APERTO")
            .then(fSuccess, fError)

        return defer.promise;
    },

    _loadSkills: function () {
        var defer = Q.defer();

        var fSuccess = function (result) {
            console.log("-------Skill Load-----------");
            console.log(result);
            this.skills = {
                results: result.items
            };
            this.skillModel.setData(this.skills);
            defer.resolve(result);
        }
        fSuccess = _.bind(fSuccess, this);

        var fError = function (err) {
            this.skills = {
                "results": []
            };
            this.skillModel.setData(this.skills);
            sap.m.MessageToast.show(this._getLocaleText("errorLoadingSkillList"));
            defer.reject(err);

        }
        fError = _.bind(fError, this);

        model.Skill.readSkills()
            .then(fSuccess, fError)

        return defer.promise;
    },
    /********************************************************/
    /********SearchField Management****************************/
    onFilterConsultantsByPJMSearch: function (evt) {
        this._onFilterPJMSearch(evt, "idListaConsulentiPjm", ["nomeConsulente", "cognomeConsulente", "codConsulente"]);
    },

    onFilterUMBuSearch: function (evt) {
        this._onFilterPJMSearch(evt, "idListaBuPjm", ["descrizioneBu", "codiceBu"]);
    },

    onFilterUMSkillSearch: function (evt) {
        this._onFilterPJMSearch(evt, "idListaSkillPjm", ["nomeSkill", "descSkill", "codSkill"]);
    },

    _onFilterPJMSearch: function (evt, listId, params) {
        var value = evt.getSource().getValue();
        var list = sap.ui.getCore().getElementById(listId);
        if (!list.getBinding("items").oList || list.getBinding("items").oList.length === 0)
            return;

        var temp = list.getBinding("items").oList[0]; // a template just to recover the data types
        var filtersArr = [];

        if (!_.isEmpty(params)) {
            if (!_.isArray(params)) {
                params = [params];
            }
            for (var i = 0; i < params.length; i++) {
                switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
                    case "undefined":
                        break;
                    case "string":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
                        break;
                    case "number":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
                        break;
                }
            }
            var filter = new sap.ui.model.Filter({
                filters: filtersArr,
                and: false
            });
            list.getBinding("items").filter(filter);
            return;
        }
        list.getBinding("items").filter();
    },
    /****************************************************************************/

    /**************Applying Filter****************************************/
    _loadFilterLists: function () {
        this.pmFilterModel.setProperty("/selectedTab", "consultants");
        this.pmFilterModel.setProperty("/allButtonVisible", true);
        this._setFilterProperty("/dayRange", "giorniScheduling", null, null);
        this._setFilterProperty("/consultants", "codiceConsulente", "consultantModel", "codConsulente");
        //        this._setFilterProperty("/buList", "cod_consulenteBu", "buModel", "codiceBu");
        this._setFilterProperty("/buList", "codBu", "buModel", "codiceBu");
        this._setFilterProperty("/skills", "nomeSkill", "skillModel", "nomeSkill");
        this.pmFilterModel.refresh();
    },

    _setFilterProperty: function (propertyName, key, model, modelKey) {
        //        var schedulations = this.allSchedulingForFilter;
        var schedulations = this.allScheduling;
        var schedulationItems = _.pluck(schedulations, key);

        if (propertyName.indexOf("/dayRange") >= 0) {
            //            var maxDay = _.max(schedulationItems) ? _.max(schedulationItems) : 31;
            var maxDay = _.max(schedulationItems) ? _.max(schedulationItems) : 0;
            if (!this.pmFilterModel.getProperty("/dayRange")) {
                this._initializeDayCountRange();
            } else {
                this.pmFilterModel.setProperty("/dayRange/dayMax", maxDay);
                var filterValue = parseFloat(this.pmFilterModel.getProperty("/dayRange").val);
                if (!filterValue)
                    filterValue = maxDay;
                this.pmFilterModel.setProperty("/dayRange/val", filterValue);
            }
        } else {
            var selectedItems = this._getFilterSelectedItems(propertyName);
            if (propertyName.indexOf("/skills") >= 0) {
                var skills = [];
                for (var i = 0; i < schedulationItems.length; i++) {
                    var singleSkills = schedulationItems[i].split(",");
                    singleSkills = _.map(singleSkills, function (item) {
                        return item.trim();
                    });
                    skills = skills.concat(singleSkills);
                }
                schedulationItems = skills;
            }
            var values;
            //            if (modelKey !== "codiceBu") {
            values = _.filter(this.getView().getModel(model).getProperty("/results"), _.bind(function (item) {
                return _.contains(schedulationItems, item[modelKey]);
            }, this));
            values = _.map(values, _.bind(function (val) {
                var selectedKeys = _.pluck(selectedItems, modelKey);
                val.selected = _.contains(selectedKeys, val[modelKey]);
                return val;
            }, this));
            //            } else {
            //                values = this.getView().getModel(model).getProperty("/results");
            //            }
            this.pmFilterModel.setProperty(propertyName, values);
        }
    },

    _initializeDayCountRange: function () {
        var schedulations = this.allScheduling;

        var schedulationItems = _.pluck(schedulations, "giorniScheduling");

        //        var maxDay = _.max(schedulationItems) ? parseFloat(_.max(schedulationItems)) : 31;
        var maxDay = _.max(schedulationItems) ? parseFloat(_.max(schedulationItems)) : 0;
        var dayRange = {
            "dayMin": 0,
            "dayMax": maxDay,
            "val": maxDay
        }
        this.pmFilterModel.setProperty("/dayRange", dayRange);
    },

    _getFilterSelectedItems: function (prop) {
        var items = this.pmFilterModel.getProperty(prop);
        if (prop == "/dayRange")
            return parseFloat(items.val);

        var selectedItems = [];
        if (items) {
            selectedItems = _.filter(items, function (item) {
                return item.selected;
            })
        }
        return selectedItems;
    },

    _setPropertyItems: function (propertyName, selectedBool) {
        var property = "/" + propertyName;
        var items = this.pmFilterModel.getProperty(property);

        if (property.indexOf("/dayRange") >= 0) {
            this._initializeDayCountRange();
            return;
        }

        items = _.map(items, function (obj) {
            obj.selected = selectedBool;
            return obj;
        });
        this.pmFilterModel.setProperty(property, items);
    },

    _checkAllSelectedFilterItems: function (propertyName) {
        var property = "/" + propertyName;
        var items = this.pmFilterModel.getProperty(property);
        var falseItem = _.find(items, function (item) {
            return !item.selected;
        })
        var result = falseItem ? false : true;

        return result;
    },

    _resetFilterSelectedItems: function () {
        delete this.ric.selectedConsultantsCode;
        for (var prop in this.pmFilterModel.getData()) {
            if (prop !== "allButtonVisible" && prop !== "allSelected")
                this._setPropertyItems(prop, false);
        }
        this.pmFilterModel.refresh();
    },

    _filterFunc: function (res) {
        //        var schedulations = this.allSchedulingForFilter;
        var schedulations = this.allScheduling;

        var selectedConsultants = this._getFilterSelectedItems("/consultants");
        var selectedConsultantsCodes = selectedConsultants && selectedConsultants.length > 0 ? _.pluck(selectedConsultants, 'codConsulente') : false;

        var selectedBUnits = this._getFilterSelectedItems("/buList");
        var selectedBUnitsCodes = selectedBUnits && selectedBUnits.length > 0 ? _.pluck(selectedBUnits, 'codiceBu') : false;

        var selectedSkills = this._getFilterSelectedItems("/skills");
        var selectedSkillsNames = selectedSkills && selectedSkills.length > 0 ? _.pluck(selectedSkills, 'nomeSkill') : false;

        var filteredSchedulations = _.filter(schedulations, _.bind(function (item) {
            var result = true;

            if (selectedConsultantsCodes) {
                result = _.contains(selectedConsultantsCodes, item.codiceConsulente);
            }
            if (!result)
                return false;

            if (selectedSkillsNames) {
                var itemSkills = item.nomeSkill.split(",");
                var found = false;

                for (var i = 0; i < itemSkills.length; i++) {
                    itemSkills[i] = itemSkills[i].trim();
                    found = _.contains(selectedSkillsNames, itemSkills[i]);
                    if (found)
                        break;
                }
                if (!found)
                    result = false;

            }
            if (!result)
                return false;

            if (selectedBUnitsCodes) {
                result = _.contains(selectedBUnitsCodes, item.cod_consulenteBu);
            }
            if (!result)
                return false;

            var itemDay = item.giorniScheduling;
            result = (itemDay >= this.pmFilterModel.getProperty("/dayRange/dayMin") && itemDay <= this.pmFilterModel.getProperty("/dayRange/val"));
            return result;

        }, this));

        this.pmSchedulingModel.setProperty("/schedulazioni", filteredSchedulations);
        if (filteredSchedulations.length > 0 && filteredSchedulations.length <= 6) {
            this.pmSchedulingModel.setProperty("/nRighe", filteredSchedulations.length);
        } else if (filteredSchedulations.length > 6) {
            this.pmSchedulingModel.setProperty("/nRighe", 6);
        } else {
            this.pmSchedulingModel.setProperty("/nRighe", 1);
        }
        setTimeout(_.bind(this._onAfterRenderingTable, this, this.oDialogTable));
    },

    onFilterUMDialogOK: function (evt) {
        utils.Busy.show("Ricaricamento Dati Real Time");
        var fSuccess = function () {
            utils.Busy.hide();
            this._filterFunc();
            this.umDialog.close();
        }
        fSuccess = _.bind(fSuccess, this);

        var req = _.clone(this.ric);

        var selectedConsultant = this._getFilterSelectedItems("/consultants");
        var selectedBunits = this._getFilterSelectedItems("/buList");
        var selectedSkills = this._getFilterSelectedItems("/skills");
        var selectedSchedulingDays = this._getFilterSelectedItems("/dayRange");
        if (selectedConsultant && selectedConsultant.length === 1 && selectedBunits.length == 0 && selectedSkills.length == 0) {
            req.selectedConsultantsCode = selectedConsultant[0].codConsulente;
            req.idStaff = "CSC0000031";
        }
        if (selectedConsultant && selectedConsultant.length > 1) {
            var arrayConsultants = [];
            for (var i = 0; i < selectedConsultant.length; i++) {
                arrayConsultants.push(selectedConsultant[i].codConsulente);
            }
            req.arrayConsultants = arrayConsultants;
        }
        if (!selectedConsultant || selectedConsultant.length !== 1) {
            if (selectedBunits.length !== 0) {
                var arrayBu = [];
                for (var i = 0; i < selectedBunits.length; i++) {
                    arrayBu.push(selectedBunits[i].codiceBu);
                }
                req.arrayBu = arrayBu;
            }
            if (selectedSkills.length !== 0) {
                var arraySkill = [];
                for (var i = 0; i < selectedSkills.length; i++) {
                    arraySkill.push(selectedSkills[i].nomeSkill);
                }
                req.arraySkill = arraySkill;
            }
            if (selectedSchedulingDays)
                req.selectedSchedulingDays = selectedSchedulingDays;
        }

        this.leggiTabella(req).then(fSuccess);
    },

    onFilterTabSelect: function (evt) {
        var tab = evt.getParameter("selectedKey");

        if (tab == "days") {
            sap.ui.getCore().byId("sliderId").setValue(this.pmFilterModel.getProperty("/dayRange/val"));
            this.pmFilterModel.setProperty("/allButtonVisible", false);
        } else {
            this.pmFilterModel.setProperty("/allSelected", this._checkAllSelectedFilterItems(tab));
            this.pmFilterModel.setProperty("/allButtonVisible", true);
        }
        this.pmFilterModel.refresh();
    },

    onSliderChange: function (evt) {
        var value = parseFloat(evt.getSource().getValue());
        this.pmFilterModel.setProperty("/dayRange/val", parseFloat(value));
    },

    onSliderLiveChange: function (evt) {
        var value = parseFloat(evt.getSource().getValue());
        this.pmFilterModel.setProperty("/dayRange/val", parseFloat(value));
    },
    /*************************************************/

    //    onFilterConsultantsByPJMPress: function () {
    //        var consultants = [];
    //        if (!this.allConsultantsForProject) {
    //            var schedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni");
    //            for (var i = 0; i < schedulazioni.length; i++) {
    //                if (schedulazioni[i].codiceConsulente.indexOf("CCG") === -1) {
    //                    var consultant = {
    //                        codConsulente: schedulazioni[i].codiceConsulente,
    //                        nomeCognome: schedulazioni[i].nomeConsulente
    //                    };
    //                    consultants.push(consultant);
    //                }
    //            }
    //            this.allConsultantsForProject = consultants;
    //        }
    //        var page = this.getView().byId("schedulingPage");
    //        if (!this.filterConsultantsByPJMDialog) {
    //            this.filterConsultantsByPJMDialog = sap.ui.xmlfragment("view.dialog.filterConsultantsByPJM_", this);
    //            page.addDependent(this.filterConsultantsByPJMDialog);
    //        }
    //
    //        if (this.commessaNelFiltro && this.commessaNelFiltro !== this.chosedCom) {
    //            var list = sap.ui.getCore().getElementById("idListaConsulentiPjm");
    //            list.removeSelections();
    //            var oBinding = list.getBinding("items");
    //            oBinding.filter();
    //            sap.ui.getCore().getElementById("idOnFilterConsultantsByPJMSearch").setValue("");
    //        }
    //
    //        this.pmSchedulingModel.setProperty("/consultants", this.allConsultantsForProject);
    //        var lista = sap.ui.getCore().getElementById("idListaConsulentiPjm");
    //        var selectedConsultants = lista.getSelectedItems();
    //        var selectAllButton = sap.ui.getCore().byId("selectAllButton");
    //        if (!!selectAllButton) {
    //            if (selectedConsultants.length === 0) {
    //                selectAllButton.setText(this._getLocaleText("SELECT_ALL"));
    //            } else {
    //                selectAllButton.setText(this._getLocaleText("DESELECT_ALL"));
    //            }
    //        }
    //        this.filterConsultantsByPJMDialog.open();
    //    },

    // filtro di ricerca dei consulenti nel dialog 'filterConsultantsByPJM'
    onFilterConsultantsByPJMSearch: function (evt) {
        var src = evt.getSource();
        this.searchValue = src.getValue();
        var searchProperty = ["codConsulente", "nomeCognome"];
        this.applyFilterConsultantsByPJM(this.searchValue, searchProperty);
    },

    // funzione concatenata al filtro di ricerca
    applyFilterConsultantsByPJM: function (value, params) {
        var list = sap.ui.getCore().getElementById("idListaConsulentiPjm");
        if (!list.getBinding("items").oList || list.getBinding("items").oList.length === 0)
            return;

        var temp = list.getBinding("items").oList[0]; // a template just to recover the data types
        var filtersArr = [];

        if (!_.isEmpty(params)) {
            if (!_.isArray(params)) {
                params = [params];
            }
            for (var i = 0; i < params.length; i++) {
                switch (typeof (utils.ObjectUtils.getValues(temp, params[i]))) {
                    case "undefined":
                        break;
                    case "string":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.Contains, value));
                        break;
                    case "number":
                        filtersArr.push(new sap.ui.model.Filter(params[i], sap.ui.model.FilterOperator.EQ, value));
                        break;
                }
            }
            var filter = new sap.ui.model.Filter({
                filters: filtersArr,
                and: false
            });
            list.getBinding("items").filter(filter);
            return;
        }
        list.getBinding("items").filter();
    },

    // funzione che costrisce un array di elementi selezionati
    onSelectionChangeFilterConsultants: function (evt) {
        var oList = evt.getSource();
        this.listaSelezioniFiltro = oList.getSelectedContexts(true);
    },

    // chiude il dialog, rimuove eventuali selezioni e filtri
    onFilterConsultantsByPJMDialogClose: function () {
        //        var list = sap.ui.getCore().getElementById("idListaConsulentiPjm");
        //        list.removeSelections();
        //        var oBinding = list.getBinding("items");
        //        oBinding.filter();
        //        sap.ui.getCore().getElementById("onFilterConsultantsByPJMSearch").setValue("");
        //        this.listaSelezioniFiltro = undefined;
        this.pmDialog.close();
    },

    // selezionato uno o piu consulenti, nasconde gli altri dalla tabella
    onFilterConsultantsByPJMDialogOK: function () {
        var selectedConsultant = _.sortByOrder(this._getFilterSelectedItems("/consultants"), 'cognomeConsulente');
        //        var selectedBunits = this._getFilterSelectedItems("/buList");
        //        var selectedSkills = this._getFilterSelectedItems("/skills");
        //        var selectedSchedulingDays = this._getFilterSelectedItems("/dayRange");
        //        
        // se si implementano gli altri campi di filtro questa parte va implementata perche non è stata terminata
        var schedulazioni = [];
        for (var j = 0; j < selectedConsultant.length; j++) {
            var schedulazione = _.find(this.allScheduling, {
                'codiceConsulente': selectedConsultant[j].codConsulente
            });
            if (schedulazione) {
                schedulazioni.push(schedulazione);
            }
        }
        this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioni);

        this.getView().byId("idFiltroGiornateZero").setText(this._getLocaleText("NascondiConsulentiZeroGiornate"));
        this.getView().byId("idFiltroGiornateZero").setProperty("pressed", false);

        //        this.filteredConsultants = undefined;
        //        var selectedConsultantsCode = [];
        //        if (this.listaSelezioniFiltro && this.listaSelezioniFiltro.length > 0) {
        //            for (var i = 0; i < this.listaSelezioniFiltro.length; i++) {
        //                var path = parseInt(this.listaSelezioniFiltro[i].getPath().split("/")[2]);
        //                var codConsulente = this.listaSelezioniFiltro[i].getModel().getData().consultants[path].codConsulente;
        //                selectedConsultantsCode.push(codConsulente);
        //            }
        //            var schedulazioni = [];
        //            for (var j = 0; j < selectedConsultantsCode.length; j++) {
        //                var schedulazione = _.find(this.allScheduling, {
        //                    'codiceConsulente': selectedConsultantsCode[j]
        //                });
        //                if (schedulazione) {
        //                    schedulazioni.push(schedulazione);
        //                }
        //            }
        //            this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioni);
        //            this.filteredConsultants = selectedConsultantsCode;
        //        } else {
        //            this.onFilterConsultantsByPJMDialogRipristino();
        //        }

        var lunghezzaSchedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni").length;
        if (lunghezzaSchedulazioni > 0 && lunghezzaSchedulazioni <= 6) {
            this.pmSchedulingModel.setProperty("/nRighe", lunghezzaSchedulazioni);
        } else if (lunghezzaSchedulazioni > 6) {
            this.pmSchedulingModel.setProperty("/nRighe", 6);
        } else {
            this.pmSchedulingModel.setProperty("/nRighe", 1);
        }
        this.pmSchedulingModel.refresh();
        this.pmDialog.close();
        //        this.commessaNelFiltro = this.getView().byId("idSelectCommessa").getValue();
        this.commessaNelFiltro = this.chosedCom;
        setTimeout(_.bind(this._onAfterRenderingTable, this));
    },

    // funzione che ripristina la tabella con tutti i consulenti
    onFilterConsultantsByPJMDialogRipristino: function () {
        this.filteredConsultants = undefined;
        this.pmSchedulingModel.setProperty("/schedulazioni", this.allScheduling);
        //        var lunghezzaSchedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni").length;
        //        sap.ui.getCore().getElementById("idListaConsulentiPjm").removeSelections();
        //        if (lunghezzaSchedulazioni > 0 && lunghezzaSchedulazioni <= 6) {
        //            this.pmSchedulingModel.setProperty("/nRighe", lunghezzaSchedulazioni);
        //        } else if (lunghezzaSchedulazioni > 6) {
        //            this.pmSchedulingModel.setProperty("/nRighe", 6);
        //        } else {
        //            this.pmSchedulingModel.setProperty("/nRighe", 1);
        //        }
        //        this.pmSchedulingModel.refresh();
        //        this.onFilterConsultantsByPJMDialogClose();
        //        setTimeout(_.bind(this._onAfterRenderingTable, this));
        //        this.listaSelezioniFiltro = undefined;

        //        sap.ui.getCore().getElementById("idListaConsulentiUM").removeSelections();
        var list = sap.ui.getCore().getElementById("idListaConsulentiPjm");
        list.removeSelections();
        var oBinding = list.getBinding("items");
        oBinding.filter();

        this.leggiTabella(this.ric);
        this.getView().byId("idFiltroGiornateZero").setText(this._getLocaleText("NascondiConsulentiZeroGiornate"));
        this.getView().byId("idFiltroGiornateZero").setProperty("pressed", false);
        this.pmDialog.close();
    },

    // funzione che seleziona o deselezioni tutti gli elementi della tab
    onFilterSelectAllItems: function (evt) {
        var src = evt.getSource();

        var tab = this.pmFilterModel.getProperty("/selectedTab");
        var text = src.getText();

        var op = !this.pmFilterModel.getProperty("/allSelected");

        this._setPropertyItems(tab, op);

        this.pmFilterModel.setProperty("/allSelected", this._checkAllSelectedFilterItems(tab));
        this.pmFilterModel.refresh();
    },
    //    onFilterSelectAllItems: function (evt) {
    //        var src = evt.getSource();
    //        var txtButton = this._getLocaleText(src.getText());
    //        var lista = sap.ui.getCore().getElementById("idListaConsulentiPjm");
    //        if (txtButton === "Seleziona tutto") {
    //            lista.selectAll();
    //            src.setText("Deseleziona tutto");
    //        } else {
    //            lista.removeSelections();
    //            src.setText("Seleziona tutto");
    //        }
    //        this.listaSelezioniFiltro = lista.getSelectedContexts(true);
    //    },
    /* FINE - funzioni per selezionare uno o più consulenti presenti nello staff */


    /* INIZIO - funzione visualizza tutti i consulenti con giornate schedulate diverso da 0 */
    onFilterGioranteZero: function (evt) {
        if (!evt.getSource().getProperty("pressed")) {
            evt.getSource().setProperty("pressed", false);
            this.leggiTabella(this.ric);
            this.getView().byId("idFiltroGiornateZero").setText(this._getLocaleText("NascondiConsulentiZeroGiornate"));
        } else {
            this.getView().byId("idFiltroGiornateZero").setText(this._getLocaleText("RipristinaConsulentiZeroGiornate"));
            var schedulazioniZero = _.filter(this.allScheduling, function (item) {
                if (item.giorniScheduling !== 0)
                    return item;
            });
            if (schedulazioniZero && _.size(schedulazioniZero) > 0) {
                this.pmSchedulingModel.setProperty("/schedulazioni", schedulazioniZero);

                if (schedulazioniZero.length > 0 && schedulazioniZero.length <= 6) {
                    this.pmSchedulingModel.setProperty("/nRighe", schedulazioniZero.length);
                } else if (schedulazioniZero.length > 6) {
                    this.pmSchedulingModel.setProperty("/nRighe", 6);
                } else {
                    this.pmSchedulingModel.setProperty("/nRighe", 1);
                }
            } else {
                this.pmSchedulingModel.setProperty("/schedulazioni", {});
                this.pmSchedulingModel.setProperty("/nRighe", 1);
                //                evt.getSource().setProperty("pressed", false);
            }
        }
    },
    /* FINE - funzione visualizza tutti i consulenti con giornate schedulate diverso da 0 */

    /* INIZIO - funzione visualizza tutti i consulenti o solo quelli dello staff selezionato */
    showAllConsultants: function (evt) {
        if (!evt.getSource().getProperty("pressed")) {
            this.viewAllConsultants = false;
            evt.getSource().setProperty("pressed", false);
            this.getView().byId("addConsultantButton").setEnabled(true);
            this.getView().byId("addGenericConsultantButton").setEnabled(true);
            this.getView().byId("idFilterConsultant").setEnabled(true);
            this.visibleModel.setProperty("/enabledButton", true);
            this.getView().byId("idDownloadExcel").setEnabled(true);
            this.getView().byId("idUploadExcel").setEnabled(true);
            this.leggiTabella(this.ric);
            return;
        }

        var req = {};
        req.cod_consulente = (this.user).codice_consulente;
        req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
        this.leggiTabella(req);
        this.visibleModel.setProperty("/enabledButton", false);
        this.getView().byId("addConsultantButton").setEnabled(false);
        this.getView().byId("addGenericConsultantButton").setEnabled(false);
        this.getView().byId("idFilterConsultant").setEnabled(false);
        this.getView().byId("idDownloadExcel").setEnabled(false);
        this.getView().byId("idUploadExcel").setEnabled(false);
        this.viewAllConsultants = true;
    },
    /* FINE - funzione visualizza tutti i consulenti o solo quelli dello staff selezionato */

    /* INIZIO - funzione di inserimento, modifica, cancellazione di richieste schedulazioni */
    // funzione lanciata alla pressione di una qualsiasi icona
    // trova il consulente e tutte le sue schedulazioni
    openMenu: function (evt) {
//        if (!this.getView().byId("filterForProject").getPressed() && this.visibleModel.getProperty("/enabledButton")) {
        if (this.visibleModel.getProperty("/enabledButton")) {
            this.oButton = evt.getSource();
            if (this.oButton.getSrc() !== "sap-icon://travel-itinerary") {
                this.idButton = this.oButton.getId();
                this.nButton = parseInt((this.idButton.split("-")[2]).substring(this.idButton.split("-")[2].indexOf("n") + 1));
                this.row = this.oButton.getBindingContext("pmSchedulingModel").getPath().split("/")[2];
                this.dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[this.row];
                var commesseQuestoConsulente = _.find(this.commessePerConsulente, {
                    cod: this.dataModelConsultant.codiceConsulente
                }).commesse;
                //                var commessaSelezionata = this.getView().byId("idSelectCommessa").getValue().split(" - ")[0];
                var commessaSelezionata = this.chosedCom;
                var cloneCommesseConsulente = _.clone(commesseQuestoConsulente);
                var commessaDaSpostare = _.remove(cloneCommesseConsulente, {
                    'nomeCommessa': commessaSelezionata
                });
                this.commesseQuestoConsulente = [];
                if (this.dataModelConsultant.codiceConsulente.indexOf("CCG") >= 0) {
                    this.commesseQuestoConsulente = commessaDaSpostare;
                } else {
                    if (commesseQuestoConsulente.length > 1)
                        cloneCommesseConsulente = _.sortBy(cloneCommesseConsulente, function (n) {
                            return n.nomeCommessa.toUpperCase();
                        });
                    cloneCommesseConsulente.unshift(commessaDaSpostare[0]);
                    this.commesseQuestoConsulente = _.clone(cloneCommesseConsulente);
                }
                this._openMenu();
            }
        }
    },

    // funzione che apre il menu 'menuButtonsPJM'
    _openMenu: function () {
        if (!this._menu) {
            this._menu = sap.ui.xmlfragment("view.fragment.menuButtonsPJM", this);
            this.getView().addDependent(this._menu);
        }
        var eDock = sap.ui.core.Popup.Dock;
        this._menu.setModel(this.visibleModel, "visibleModel");
        this._menu.open(undefined, this.oButton, eDock.BeginTop, eDock.BeginBottom, this.oButton);
        setTimeout(_.bind(this.nascondiItems, this));
    },

    // funzione che nasconde tutte le righe del menu che non servono per il consulente scelto
    nascondiItems: function () {
        // la sequente variabile serve per capire se mostrare poi tutte le richieste o solamente quella della mezza giornata
        var allRequests = true;
        var prop = "gg" + this.nButton;
        var propBis = "gg" + this.nButton + "a";
        var schedulazioneGiornaliera = this.dataModelConsultant[prop];
        var schedulazioneGiornalieraBis = this.dataModelConsultant[propBis];
        //        var commessaSelezionata = this.getView().byId("idSelectCommessa").getValue();
        var commessaSelezionata = this.chosedCom;

        if (schedulazioneGiornaliera) {
            if (schedulazioneGiornalieraBis || schedulazioneGiornaliera !== this.nomeCommessaFerie) {
                this.visibleModel.setProperty("/deleteVisible", true);
                this.visibleModel.setProperty("/copyVisible", true);
                this.visibleModel.setProperty("/noteVisible", true);
            }
        } else {
            this.visibleModel.setProperty("/deleteVisible", false);
            this.visibleModel.setProperty("/copyVisible", false);
            this.visibleModel.setProperty("/noteVisible", false);
        }
        if (schedulazioneGiornaliera === this.nomeCommessaFerie && !schedulazioneGiornalieraBis) {
            this.visibleModel.setProperty("/deleteVisible", false);
            this.visibleModel.setProperty("/copyVisible", false);
            this.visibleModel.setProperty("/noteVisible", false);
        }
        if (schedulazioneGiornaliera === this.nomeCommessaFerie || schedulazioneGiornalieraBis === this.nomeCommessaFerie) {
            // se una delle due commesse è mezza giornata di ferie allora cambio la variabile allRequest
            allRequests = false;
        }
        var schedulazioneInLocal = model.persistence.Storage.local.get("copy");
        if (schedulazioneInLocal) {
            var commessaInLocal = schedulazioneInLocal.commessa;
            var commessaAbilitata = _.find(this.commesseQuestoConsulente, {
                'codCommessa': commessaInLocal
            });
            if (commessaAbilitata && !schedulazioneGiornalieraBis) {
                if (schedulazioneGiornaliera !== this.nomeCommessaFerie) {
                    // se non ho ferie mostro l'incolla senza problemi
                    this.visibleModel.setProperty("/pasteVisible", true);
                } else {
                    // se invece ho ferie mostro l'incolla solamente se la commessa copiata precedentemente è una mezza giornata
                    if (schedulazioneInLocal.richiesta.indexOf("time-entry-request") >= 0) {
                        this.visibleModel.setProperty("/pasteVisible", true);
                    } else {
                        this.visibleModel.setProperty("/pasteVisible", false);
                    }
                }
            } else {
                this.visibleModel.setProperty("/pasteVisible", false);
            }
        } else {
            this.visibleModel.setProperty("/pasteVisible", false);
        }
        this.mostraCommesse(allRequests);
    },

    mostraCommesse: function () {
        var menuCommesse = sap.ui.getCore().getElementById("idMenuCommesse");
        menuCommesse.removeAllItems();
        for (var i = 0; i < this.commesseQuestoConsulente.length; i++) {
            var itemToAdd = new sap.ui.unified.MenuItem({
                text: this.commesseQuestoConsulente[i].nomeCommessa,
                select: _.bind(this.premiCommessa, this)
            });

            if (i === 1)
                itemToAdd.setStartsSection(true);

            // !! rimuovere commessa dove non schedulabile
            if (_.find(this.commesseOffsiteWork, {
                    comm: this.commesseQuestoConsulente[i].nomeCommessa
                })) {
                itemToAdd.setIcon("sap-icon://offsite-work");
                itemToAdd.setEnabled(false);
            }
            menuCommesse.addItem(itemToAdd);
        }
    },

    premiCommessa: function (evt) {
        var title = evt.getParameters().item.getProperty("text");
        var commessa = _.find(this.commesseQuestoConsulente, {
            'nomeCommessa': title
        });
        this.commessa = commessa.codCommessa;

        if (!this._menuAttvita) {
            this._menuAttvita = sap.ui.xmlfragment("view.fragment.MenuRichieste", this);
            this.getView().addDependent(this._menuAttvita);
        }
        this._menuAttvita.getAggregation("items")[0].setText(title);
        var eDock = sap.ui.core.Popup.Dock;
        this._menuAttvita.open(undefined, this.oButton, eDock.BeginTop, eDock.BeginBottom, this.oButton);

        setTimeout(_.bind(this._onAfterRenderingTable, this));
    },

    // funzione lanciata alla pressione di un bottone tra 'copia', 'incolla', 'taglia', 'cancella'
    handleMenuItemPress: function (evt) {
        var idButton = this.oButton.getId();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        var row = this.oButton.getBindingContext("pmSchedulingModel").getPath().split("/")[2];
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[parseInt(row)];
        var com = "gg" + nButton;
        var req = "req" + nButton;
        var ggIcon = "ggIcon" + nButton;
        var nota = "nota" + nButton;
        var comA = "gg" + nButton + "a";

        if (evt.getParameters().item.getText() === this._getLocaleText("COPY")) {
            if (dataModelConsultant[com] === this.nomeCommessaFerie || dataModelConsultant[comA] === this.nomeCommessaFerie) {
                if (dataModelConsultant[com] === this.nomeCommessaFerie) {
                    var codCommessaA = _.find(this.pmSchedulingModel.getData().commesse, {
                        nomeCommessa: dataModelConsultant[comA]
                    }).codCommessa;
                    var reqA = "req" + nButton + "a";
                    var ggIconA = "ggIcon" + nButton + "a";
                    var notaA = "nota" + nButton + "a";
                    var modelToSave = {
                        codCons: dataModelConsultant.codiceConsulente,
                        idReq: dataModelConsultant[reqA],
                        richiesta: dataModelConsultant[ggIconA],
                        note: dataModelConsultant[notaA] ? dataModelConsultant[notaA] : "",
                        commessa: codCommessaA,
                        nomeCommessa: dataModelConsultant[comA]
                    };
                } else {
                    var codCommessa = _.find(this.pmSchedulingModel.getData().commesse, {
                        nomeCommessa: dataModelConsultant[com]
                    }).codComessa;
                    var modelToSave = {
                        codCons: dataModelConsultant.codiceConsulente,
                        idReq: dataModelConsultant[req],
                        richiesta: dataModelConsultant[ggIcon],
                        note: dataModelConsultant[nota],
                        commessa: codCommessa,
                        nomeCommessa: dataModelConsultant[com]
                    };
                }
            } else {
                var codCommessa = _.find(this.pmSchedulingModel.getData().commesse, {
                    nomeCommessa: dataModelConsultant[com]
                }).codCommessa;
                var modelToSave = {
                    codCons: dataModelConsultant.codiceConsulente,
                    idReq: dataModelConsultant[req],
                    richiesta: dataModelConsultant[ggIcon],
                    note: dataModelConsultant[nota],
                    commessa: codCommessa,
                    nomeCommessa: dataModelConsultant[com]
                };
                if (dataModelConsultant[comA]) {
                    var codCommessaA = _.find(this.pmSchedulingModel.getData().commesse, {
                        nomeCommessa: dataModelConsultant[comA]
                    }).codCommessa;
                    var reqA = "req" + nButton + "a";
                    var ggIconA = "ggIcon" + nButton + "a";
                    var notaA = "nota" + nButton + "a";
                    var modelToSaveBis = {
                        codCons: dataModelConsultant.codiceConsulente,
                        idReq: dataModelConsultant[reqA],
                        richiesta: dataModelConsultant[ggIconA],
                        note: dataModelConsultant[notaA],
                        commessa: codCommessaA,
                        nomeCommessa: dataModelConsultant[comA]
                    };
                    model.persistence.Storage.local.save("copyBis", modelToSaveBis);

                    this.doppiaCancellazione = undefined;
                    this.doppiaNote = undefined;
                    this.doppiaCopia = [];
                    this.doppiaCopia.push({
                        ric: dataModelConsultant[com],
                        icon: dataModelConsultant[ggIcon],
                        nota: dataModelConsultant[nota]
                    }, {
                        ric: dataModelConsultant[comA],
                        icon: dataModelConsultant[ggIconA],
                        nota: dataModelConsultant[notaA]
                    });

                    this.commesseDialog = sap.ui.xmlfragment("view.fragment.SelectProjectActionSheet", this);
                    this.getView().addDependent(this.commesseDialog);
                    this.pmSchedulingModel.setProperty("/requests", this.doppiaCopia);
                    this.commesseDialog.setModel(this.pmSchedulingModel, "pmSchedulingModel");
                    this.commesseDialog.open();
                }
            }
            model.persistence.Storage.local.save("copy", modelToSave);
        } else if (evt.getParameters().item.getText() === this._getLocaleText("PASTE")) {
            if (model.persistence.Storage.local.get("copy")) {
                this.paste = true;
                var rich = model.persistence.Storage.local.get("copy");
                if (dataModelConsultant[com] && dataModelConsultant[ggIcon] === "sap-icon://time-entry-request" && rich.icon === "sap-icon://time-entry-request") {
                    this.propTemp = gga;
                }

                this.actionButton(evt, rich.richiesta);
            }
        } else {
            var ggIconA = "ggIcon" + nButton + "a";
            var notaA = "nota" + nButton + "a";
            if (evt.getParameters().item.getIcon() === "sap-icon://delete" && dataModelConsultant[comA]) {
                if (dataModelConsultant[com] === this.nomeCommessaFerie || dataModelConsultant[comA] === this.nomeCommessaFerie) {
                    this.actionButton(evt);
                } else {
                    this.doppiaNote = undefined;
                    this.doppiaCopia = undefined;
                    this.doppiaCancellazione = [];
                    this.doppiaCancellazione.push({
                        ric: dataModelConsultant[com],
                        icon: dataModelConsultant[ggIcon],
                        nota: dataModelConsultant[nota]
                    }, {
                        ric: dataModelConsultant[comA],
                        icon: dataModelConsultant[ggIconA],
                        nota: dataModelConsultant[notaA]
                    });
                    this.commesseDialog = sap.ui.xmlfragment("view.fragment.SelectProjectActionSheet", this);
                    this.getView().addDependent(this.commesseDialog);
                    this.pmSchedulingModel.setProperty("/requests", this.doppiaCancellazione);
                    this.commesseDialog.setModel(this.pmSchedulingModel, "pmSchedulingModel");
                    this.commesseDialog.open();
                }
            } else if (evt.getParameters().item.getIcon() === "sap-icon://notes" && dataModelConsultant[comA]) {
                if (dataModelConsultant[com] === this.nomeCommessaFerie || dataModelConsultant[comA] === this.nomeCommessaFerie) {
                    this.actionButton(evt);
                } else {
                    this.doppiaCancellazione = undefined;
                    this.doppiaCopia = undefined;
                    this.doppiaNote = [];
                    this.doppiaNote.push({
                        ric: dataModelConsultant[com],
                        icon: dataModelConsultant[ggIcon],
                        nota: dataModelConsultant[nota]
                    }, {
                        ric: dataModelConsultant[comA],
                        icon: dataModelConsultant[ggIconA],
                        nota: dataModelConsultant[notaA]
                    });
                    this.commesseDialog = sap.ui.xmlfragment("view.fragment.SelectProjectActionSheet", this);
                    this.getView().addDependent(this.commesseDialog);
                    this.pmSchedulingModel.setProperty("/requests", this.doppiaNote);
                    this.commesseDialog.setModel(this.pmSchedulingModel, "pmSchedulingModel");
                    this.commesseDialog.open();
                }
            } else if (evt.getParameters().item.getIcon()) {
                this.paste = false;
                this.actionButton(evt);
            } else {
                // entra in questo else se clicco per esempio sul nome della commessa e quindi mi aspetto che non succeda nulla
                return;
            }
        }
    },

    // funzione che chiude il dialog di scelta commessa nel caso di del o note
    onRequestDialogClose: function () {
        this.commesseDialog.close();
        this.commesseDialog.destroy(true);
        model.persistence.Storage.local.remove("copyBis");
    },

    // funzione che dopo ever scelto la commessa (nel caso di del o note) esegue il codice
    onRequestDialogOK: function (evt) {
        var requestList = sap.ui.getCore().getElementById("idRequestsList");
        var requestSelected = requestList.getSelectedItems();
        if (requestSelected.length > 0) {
            var req = requestSelected[0].getTitle();
            var icon = requestSelected[0].getIcon();
            this.tempRicToDel = {};
            this.tempRicToDel.req = req;
            this.tempRicToDel.icon = icon;

            if (this.doppiaCopia) {
                if (model.persistence.Storage.local.get("copy").nomeCommessa === this.tempRicToDel.req) {} else {
                    model.persistence.Storage.local.save("copy", model.persistence.Storage.local.get("copyBis"));
                }
                this.tempRicToDel = undefined;
                this.doppiaCopia = undefined;
            } else if (this.doppiaNote) {
                this.actionButton(undefined, "note");
            } else {
                this.actionButton();
            }
            this.onRequestDialogClose();
        }
    },

    // funzione che in base al tipo di richiesta, popola l'oggetto da scrivere, modificare, cancellare
    actionButton: function (evt, richiesta) {
        var del = false;
        var b = this.oButton;
        this.currentButton = b;
        var idButton = b.getId();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        var row = this.oButton.getBindingContext("pmSchedulingModel").getPath().split("/")[2];
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[parseInt(row)];
        var prop = "gg" + nButton;
        var propI = "ggIcon" + nButton;

        this.backupRowScheduling = dataModelConsultant;

        var tipoRichiesta = evt ? evt.getParameters().item.getText() : "";

        if (!richiesta) {
            var richiesta = "";
            switch (tipoRichiesta) {
                case this._getLocaleText("ASSIGN"):
                    richiesta = "Assign";
                    break;
                case this._getLocaleText("ASSIGN_HALF_DAY"):
                    richiesta = "halfDay";
                    break;
                case this._getLocaleText("C_WHIT_CONSULTANT"):
                    richiesta = "wCons";
                    break;
                case this._getLocaleText("NON_C_WHIT_CONSULTANT"):
                    richiesta = "wNonCons";
                    break;
                case this._getLocaleText("C_WHIT_CUSTOMER"):
                    richiesta = "wCust";
                    break;
                case this._getLocaleText("NON_C_WHIT_CUSTOMER"):
                    richiesta = "wNonCust";
                    break;
                case this._getLocaleText("NON_CONFIRMED_CUSTOMER"):
                    richiesta = "notConfirmed";
                    break;
                case this._getLocaleText("ABROAD"):
                    richiesta = "abroad";
                    break;
                case this._getLocaleText("NOTE"):
                    richiesta = "note";
                    break;
                default:
                    richiesta = "del";
                    break;
            }
        }
        var propNotaGiorno, propReq;
        var tempPropIcon = "ggIcon" + nButton;

        if ((dataModelConsultant[prop] && dataModelConsultant[prop] !== this.nomeCommessaSelezionata && (richiesta === "halfDay") && (dataModelConsultant[tempPropIcon] === "sap-icon://time-entry-request" || dataModelConsultant[tempPropIcon] === "sap-icon://bed")) || (this.tempRicToDel && dataModelConsultant[prop] !== this.tempRicToDel.req) || ((richiesta === 'note' || richiesta === "del") && dataModelConsultant[prop] === this.nomeCommessaFerie)) {
            prop = prop + "a";
            propNotaGiorno = "nota" + nButton + "a";
            propReq = "req" + nButton + "a";
        } else {
            propNotaGiorno = "nota" + nButton;
            propReq = "req" + nButton;
        }
        this.noteModel.setProperty("/note", dataModelConsultant[propNotaGiorno]);
        this.oButton = "";

        // prendo il numero della tabella per passare correttamente la data
        var table = this.getView().byId("idSchedulazioniTable");
        var giornoMese = parseInt(table.getColumns()[nButton].getLabel().getProperty("text"));
        if (giornoMese < 10) {
            giornoMese = "0" + giornoMese;
        }
        var anno = model.persistence.Storage.session.get("periodSelected").annoPeriodo;
        var periodoMese = model.persistence.Storage.session.get("periodSelected").mesePeriodo;
        if (parseInt(giornoMese) < 10 && nButton > 20) {
            if (parseInt(periodoMese) !== 12) {
                periodoMese = parseInt(periodoMese) + 1;
            } else {
                periodoMese = "1";
                anno = parseInt(anno) + 1;
            }
        }
        if (periodoMese < 10) {
            periodoMese = "0" + periodoMese;
        }

        var dataRichiesta = anno + "-" + periodoMese + "-" + giornoMese;
        if (!(new Date(dataRichiesta)).getYear()) {
            var lastDayOfMonth = new Date(parseInt(dataRichiesta.split("-")[0]), parseInt(dataRichiesta.split("-")[1]), 0);
            lastDayOfMonth = lastDayOfMonth.getDate();
            var mesePlus = parseInt(periodoMese) + 1;
            if (mesePlus < 10) {
                mesePlus = "0" + mesePlus;
            }
            var giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
            dataRichiesta = anno + "-" + mesePlus + "-" + giornoPlus;
        }

        var dataToSend = {
            "codConsulente": "",
            "dataReqPeriodo": dataRichiesta,
            "idCommessa": "",
            "codAttivitaRichiesta": "",
            "idPeriodoSchedulingRic": 0,
            "codPjm": "",
            "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "note": this.paste ? model.persistence.Storage.local.get("copy").note : "",
            "statoReq": "CONFIRMED",
            "giorno": 0,
            "id_req": dataModelConsultant[propReq] ? dataModelConsultant[propReq].toString() : ""
        };

        dataToSend.codConsulente = dataModelConsultant.codiceConsulente;
        dataToSend.codPjm = this.user.codice_consulente;
        dataToSend.idPeriodoSchedulingRic = this.period;
        if (this.commessa) {
            dataToSend.idCommessa = this.commessa;
            this.commessa = undefined;
        } else if (this.paste) {
            dataToSend.idCommessa = model.persistence.Storage.local.get("copy").commessa;
        } else {
            dataToSend.idCommessa = _.find(this.pmSchedulingModel.getData().commesse, {
                nomeCommessa: dataModelConsultant[prop]
            }).codCommessa;
        }
        dataToSend.giorno = nButton;

        var o = _.cloneDeep(this.pmSchedulingModel.getData().schedulazioni[row]);
        this.visibleModel.setProperty("/undoButton", true);
        switch (richiesta) {
            case "Assign":
            case "sap-icon://complete":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiesta = "complete";
                break;
            case "halfDay":
            case "sap-icon://time-entry-request":
                if (b.getSrc() === "sap-icon://time-entry-request" || b.getSrc() === "sap-icon://bed") {
                    if (o[prop] && o[prop] === this.chosedCom) {
                        o[prop] = this.chosedCom;
                        dataToSend.codAttivitaRichiesta = "complete";
                    } else if (o[prop] && o[prop] !== this.chosedCom) {
                        prop = prop + "a";
                        o[prop] = this.chosedCom;
                    }
                    dataToSend.codAttivitaRichiesta = "time-entry-request";
                } else {
                    o[prop] = this.chosedCom;
                    dataToSend.codAttivitaRichiesta = "time-entry-request";
                }
                break;
            case "wCons":
            case "sap-icon://customer-and-contacts":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiesta = "customer-and-contacts";
                break;
            case "wNonCons":
            case "sap-icon://employee-rejections":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiesta = "employee-rejections";
                break;
            case "wCust":
            case "sap-icon://collaborate":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiesta = "collaborate";
                break;
            case "wNonCust":
            case "sap-icon://offsite-work":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiesta = "offsite-work";
                break;
            case "notConfirmed":
            case "sap-icon://role":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiesta = "role";
                break;
            case "abroad":
            case "sap-icon://globe":
                o[prop] = this.chosedCom;
                dataToSend.codAttivitaRichiesta = "globe";
                break;
            case "del":
                var icon = b.getSrc();
                var req = icon.substring(icon.lastIndexOf("/") + 1, icon.length);
                del = true;
                dataToSend.codAttivitaRichiesta = req;
                this.tempRicToDel = undefined;
                this.dataToSave = dataToSend;
                this._delData(dataToSend, prop);
                break;
            case "note":
                this.onAddNote();
                this.dataForUpdate = dataToSend;
                del = true;
                if (!model.persistence.Storage.local.get("undoPJM_1")) {
                    this.visibleModel.setProperty("/undoButton", false);
                }
                break;
        }

        if (!del) {
            if (o[tempPropIcon] === "sap-icon://offsite-work" || dataToSend.codAttivitaRichiesta === "offsite-work") {
                dataToSend.id_req = "";
            }
            this.backupDataToSend = dataToSend;

            var fSuccess = function (results) {
                if (!!results.results && results.results.length > 0) {
                    var bed = _.where(results.results, {
                        COD_ATTIVITA_RICHIESTA: 'bed'
                    });
                    var offsite = _.where(results.results, {
                        COD_ATTIVITA_RICHIESTA: "offsite-work"
                    });
                    if (bed.length !== _.size(results.results) && offsite.length !== _.size(results.results)) {
                        this._openDialogConflicts(this.backupDataToSend.codConsulente, dataToSend);
                    } else {
                        this.saveRequest(dataToSend, prop);
                        return;
                    }
                } else {
                    this.saveRequest(dataToSend, prop);
                    return;
                }
            };
            var fError = function (err) {
                this.console(err, "error");
            };
            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            // se sto schedulando un consulente generico, non faccio il controllo per vedere se ci sono altre schedulazioni coincidenti
            if (dataToSend.codConsulente.indexOf("CCG") < 0) {
                this.checkForPreviousRequestForConsultant(idButton, row)
                    .then(fSuccess, fError);
            } else {
                this.saveRequest(dataToSend, prop);
            }
        }
    },

    // funzione che controlla se sono presenti in tabella altre richieste di altri PJM nel giorno selezionato
    checkForPreviousRequestForConsultant: function (idButton, rowId) {
        var defer = Q.defer();
        var nButton = (idButton.split("-")[2]).substring(idButton.split("-")[2].indexOf("n") + 1);
        var row = rowId;
        var prop = "gg" + nButton;
        var dataModelConsultant = this.pmSchedulingModel.getData().schedulazioni[row];
        var table = this.getView().byId("idSchedulazioniTable");
        var giornoMese = parseInt(table.getColumns()[nButton].getLabel().getProperty("text"));

        var dataToSend = {
            "idConsulente": "",
            "idPeriodo": 0,
            "idPJM": "",
            "idGiorno": 0
        };

        dataToSend.idConsulente = dataModelConsultant.codiceConsulente;
        this.consulenteSelezionato = dataToSend.idConsulente;
        dataToSend.idPJM = this.user.codice_consulente;
        dataToSend.idPeriodo = this.period;
        dataToSend.idGiorno = parseInt(nButton);

        var fSuccess = function (results) {
            results = JSON.parse(results);
            defer.resolve(results);
        };

        var fError = function (err) {
            this.console(err, "error");
            defer.reject(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.checkForPreviousRequestForConsultant(dataToSend, fSuccess, fError);
        return defer.promise;
    },

    // funzione chiamata in caso di conflitto: fa apparire un dialog di scelta
    _openDialogConflicts: function (codConsulente, dataToSend) {
        this.visibleModel.setProperty("/undoButton", false);
        sap.m.MessageBox.show(
            this._getLocaleText("SHOW_SCHEDULE_CONFLICTS_WITH_OTHER_PJM"), {
                icon: sap.m.MessageBox.Icon.INFORMATION,
                title: this._getLocaleText("SCHEDULE_CONFLICTS_TITLE"),
                actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                onClose: _.bind(function (oAction) {
                    if (oAction === "YES") {
                        var req = {};
                        var commessaScelta = model.persistence.Storage.session.get("commessa");
                        req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
                        req.codPjm = this.user.codice_consulente;
                        req.codConsulente = codConsulente;
                        this._leggiTabellaSchedulingConsulenti(req);
                    } else {
                        this.saveRequest(dataToSend);
                    }
                }, this),
            }
        );
    },

    // funzione che legge tutte le richieste del singolo consulente
    _leggiTabellaSchedulingConsulenti: function (req) {
        var fSuccess = function (results) {
            var schedulazioni = results.resultOrdinato;

            // rimuovo tutte le giornate dove il consulente non può essere schedulato
            var gg, icon;
            for (x = 1; x < 36; x++) {
                gg = "gg" + x;
                icon = "ggIcon" + x;

                var items = ""; //serie di "a" aggiunte alla proprietà
                while (schedulazioni[0][icon]) {
                    if (schedulazioni[0][icon] === "sap-icon://offsite-work") {
                        delete schedulazioni[0][icon];
                        delete schedulazioni[0][gg];
                    }
                    items = "a";
                    icon = icon + items;
                    gg = gg + items;
                }
            }

            this.pmSchedulingConsultantModel.setProperty("/schedulazioni", []);
            this.pmSchedulingConsultantModel.setProperty("/schedulazioni", schedulazioni);
            this.pmSchedulingConsultantModel.setProperty("/nRighe", 1);
            if (!this.schedConsultantDialog) {
                this.schedConsultantDialog = sap.ui.xmlfragment("view.dialog.viewConsultantScheduling", this);
                this.getView().addDependent(this.schedConsultantDialog);
            }
            var propCons = _.find(this.pmSchedulingModel.getProperty("/schedulazioni"), {
                codiceConsulente: schedulazioni[0].codiceConsulente
            });
            if (propCons && propCons.nomeConsulente) {
                this.schedConsultantDialog.setTitle(propCons.nomeConsulente);
                schedulazioni[0].nomeConsulente = propCons.nomeConsulente;
            } else {
                this.schedConsultantDialog.setTitle("");
            }
            this.oDialogTable.setBusy(false);
            this.schedConsultantDialog.open();
        };

        var fError = function (err) {
            this.oDialogTable.setBusy(false);
            sap.m.MessageToast.show("errore lettura tabella");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialogTable.setBusy(true);
        model.tabellaScheduling.readPjmSingleConsultant(req)
            .then(fSuccess, fError);
    },

    // funzione che chiude il dialog 'viewConsultantScheduling'
    onConsultantSchedulingDialogClose: function () {
        if (!!this.schedConsultantDialog) {
            this.schedConsultantDialog.close();
        }
        this.multipleRequest = undefined;
        this.backupDataToSend = undefined;
    },

    // funzione che invia la richiesta da salvare
    onConsultantSchedulingDialogOK: function () {
        if (this.multipleRequest) {
            this.multipleRequest = undefined;
            this.saveMultipleRequest(this.inserimentiBackup);
        } else {
            this.saveRequest(this.backupDataToSend);
        }
        this.onConsultantSchedulingDialogClose();
    },

    beforeCloseConsultantScheduling: function () {
        this.pmSchedulingConsultantModel.setProperty("/schedulazioni", []);
        this.pmSchedulingConsultantModel.setProperty("/nRighe", 0);
        this.pmSchedulingConsultantModel.refresh();
    },

    // funzione che elimina una richiesta di schedulazione
    _delData: function (dataToSend, prop) {
        this.richiestaToDel = dataToSend;
        this.propTemp = prop ? prop : "";
        var fSuccess = function (res) {
            if (this.fromSaveLocal) {
                this.saveToLocalForUndo("del", this.dataToSave);
                this.visibleModel.setProperty("/undoButton", true);
            }
            var elementoEliminato = JSON.parse(res).data;
            var questoConsulente = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                codiceConsulente: elementoEliminato.codConsulente
            });

            var commTempToDel = _.find(this.pmSchedulingModel.getData().commesse, {
                codCommessa: elementoEliminato.idCommessa
            }).nomeCommessa;
            var prop = this.propTemp.indexOf("a") > 0 ? "gg" + elementoEliminato.giorno + "a" : "gg" + elementoEliminato.giorno;

            var propA = prop + "a";
            if (questoConsulente[propA] && questoConsulente[propA] === commTempToDel) {
                this.propTemp = "gg1a";
                propA = propA + "a";
                prop = prop + "a";
            }

            var propConflict = this.propTemp.indexOf("a") > 0 ? "ggConflict" + elementoEliminato.giorno + "a" : "ggConflict" + elementoEliminato.giorno;
            var propIcon = this.propTemp.indexOf("a") > 0 ? "ggIcon" + elementoEliminato.giorno + "a" : "ggIcon" + elementoEliminato.giorno;
            var propReq = this.propTemp.indexOf("a") > 0 ? "ggReq" + elementoEliminato.giorno + "a" : "ggReq" + elementoEliminato.giorno;
            var propNota = this.propTemp.indexOf("a") > 0 ? "nota" + elementoEliminato.giorno + "a" : "nota" + elementoEliminato.giorno;
            var req = this.propTemp.indexOf("a") > 0 ? "req" + elementoEliminato.giorno + "a" : "req" + elementoEliminato.giorno;

            if (questoConsulente[propA]) {
                var propIconA = propIcon + "a";
                var propReqA = propReq + "a";
                var propNotaA = propNota + "a";
                var reqA = req + "a";
                var propConflictA = propConflict + "a";
                questoConsulente[prop] = questoConsulente[propA];
                questoConsulente[propIcon] = questoConsulente[propIconA];
                questoConsulente[propReq] = questoConsulente[propReqA];
                questoConsulente[propNota] = questoConsulente[propNotaA];
                questoConsulente[req] = questoConsulente[reqA];
                delete questoConsulente[propA];
                delete questoConsulente[propConflictA];
                delete questoConsulente[propIconA];
                delete questoConsulente[propReqA];
                delete questoConsulente[propNotaA];
                delete questoConsulente[reqA];
            } else {
                delete questoConsulente[prop];
                delete questoConsulente[propConflict];
                delete questoConsulente[propIcon];
                delete questoConsulente[propReq];
                delete questoConsulente[propNota];
                delete questoConsulente[req];
            }
            this._calcoloGiornate(questoConsulente);
            this.pmSchedulingModel.refresh();
            this.propTemp = undefined;
            if (this.undoButton.getBusy() === true) {
                jQuery.sap.delayedCall(500, this, function () {
                    this.undoButton.setBusy(false);
                });
            }
        };
        var fError = function (err) {
            sap.m.MessageToast.show("ERRORE");
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        model.reqToDel.sendRequest(dataToSend)
            .then(fSuccess, fError);
    },

    // funzione che scrive in tabella una richiesta
    saveRequest: function (req, prop) {
        var msgOk = "Richiesta inserita correttamente";
        var msgKo = "Errore in inserimento richiesta";
        this.dataToSave = req;
        this.propTemp = prop ? prop : (this.dataToSave ? "gg" + this.dataToSave.giorno : "");
        var fSuccess = function (ok) {
            sap.m.MessageToast.show(msgOk, {
                duration: 750,
            });
            if (this.fromSaveLocal) {
                this.dataToSave.id_req = JSON.parse(ok).retData;
                this.saveToLocalForUndo("insert", this.dataToSave);
                this.visibleModel.setProperty("/undoButton", true);
            }

            var questoConsulente = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                codiceConsulente: this.dataToSave.codConsulente
            });

            var tempIcon = "ggIcon" + this.dataToSave.giorno;
            if (questoConsulente[this.propTemp] && ((questoConsulente[tempIcon] === "sap-icon://time-entry-request" || questoConsulente[tempIcon] === "sap-icon://bed") && this.dataToSave.codAttivitaRichiesta ===
                    "time-entry-request" || (questoConsulente[tempIcon] === "sap-icon://offsite-work") || this.dataToSave.codAttivitaRichiesta === "offsite-work")) {
                this.propTemp = "gg1a";
            }
            var prop = this.propTemp.indexOf("a") > 0 ? "gg" + this.dataToSave.giorno + "a" : "gg" + this.dataToSave.giorno;
            var propConflict = this.propTemp.indexOf("a") > 0 ? "ggConflict" + this.dataToSave.giorno + "a" : "ggConflict" + this.dataToSave.giorno;
            var propIcon = this.propTemp.indexOf("a") > 0 ? "ggIcon" + this.dataToSave.giorno + "a" : "ggIcon" + this.dataToSave.giorno;
            var propReq = this.propTemp.indexOf("a") > 0 ? "ggReq" + this.dataToSave.giorno + "a" : "ggReq" + this.dataToSave.giorno;
            var propNota = this.propTemp.indexOf("a") > 0 ? "nota" + this.dataToSave.giorno + "a" : "nota" + this.dataToSave.giorno;
            var req = this.propTemp.indexOf("a") > 0 ? "req" + this.dataToSave.giorno + "a" : "req" + this.dataToSave.giorno;

            var icona = "sap-icon://" + this.dataToSave.codAttivitaRichiesta;

            if (questoConsulente[prop] === this.nomeCommessaFerie) {
                var propA = prop + "a";
                var propConflictA = propConflict + "a";
                var propIconA = propIcon + "a";
                var propReqA = propReq + "a";
                var propNotaA = propNota + "a";
                var reqA = req + "a";
                questoConsulente[propA] = _.find(this.pmSchedulingModel.getData().commesse, {
                    codCommessa: this.dataToSave.idCommessa
                }).nomeCommessa;
                questoConsulente[propConflictA] = undefined;
                questoConsulente[propIconA] = icona;
                questoConsulente[propReqA] = this.dataToSave.dataReqPeriodo;
                questoConsulente[propNotaA] = this.dataToSave.note;
                questoConsulente[reqA] = JSON.parse(ok).retData;
            } else {
                questoConsulente[prop] = _.find(this.pmSchedulingModel.getData().commesse, {
                    codCommessa: this.dataToSave.idCommessa
                }).nomeCommessa;
                questoConsulente[propConflict] = undefined;
                questoConsulente[propIcon] = icona;
                questoConsulente[propReq] = this.dataToSave.dataReqPeriodo;
                questoConsulente[propNota] = this.dataToSave.note;
                questoConsulente[req] = JSON.parse(ok).retData;

                var propTemp = prop + "a";
                if (questoConsulente[propTemp] && icona !== "sap-icon://time-entry-request") {
                    var propConflictTemp = propConflict + "a";
                    var propIconTemp = propIcon + "a";
                    var propReqTemp = propReq + "a";
                    var propNotaTemp = propNota + "a";
                    var reqTemp = req + "a";
                    delete questoConsulente[propTemp];
                    delete questoConsulente[propConflictTemp];
                    delete questoConsulente[propIconTemp];
                    delete questoConsulente[propReqTemp];
                    delete questoConsulente[propNotaTemp];
                    delete questoConsulente[reqTemp];
                }
            }
            this._calcoloGiornate(questoConsulente);

            this.fromSaveLocal = true;
            if (this.undoButton.getBusy() === true) {
                jQuery.sap.delayedCall(500, this, function () {
                    this.undoButton.setBusy(false);
                });
            }
        };
        var fError = function (err) {
            sap.m.MessageToast.show(JSON.parse(err.responseText).error);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.requestSave.sendRequest(req)
            .then(fSuccess, fError);
    },

    // funzione che calcola i giorni, e mezze giornate, della commessa in alto
    _calcoloGiornate: function (questoConsulente) {
        var giorniTemp = questoConsulente.giorniScheduling;
        var count = 0;
        for (var i = 1; i < 36; i++) {
            var propIn = "gg" + i;
            var propInIcon = "ggIcon" + i;
            var propInA = "gg" + i + "a";
            var propInIconA = "ggIcon" + i + "a";
            //            var commessaSelezionata = this.getView().byId("idSelectCommessa").getValue();
            var commessaSelezionata = this.chosedCom;
            if (questoConsulente[propIn] === commessaSelezionata || questoConsulente[propInA] === commessaSelezionata) {
                if (questoConsulente[propInIcon] === "sap-icon://time-entry-request" || questoConsulente[propInIconA] === "sap-icon://time-entry-request") {
                    count = count + 0.5;
                } else if (questoConsulente[propInIcon] !== "sap-icon://offsite-work" && questoConsulente[propInIconA] !== "sap-icon://offsite-work") {
                    count = count + 1;
                }
            }
        }
        questoConsulente.giorniScheduling = count;

        this.oDialogTable.setBusy(false);
        this.pmSchedulingModel.refresh();
        this._calcoloGiornateTotali();
    },

    _calcoloGiornateTotali: function () {
        var totalCount = 0;
        for (var i = 0; i < this.pmSchedulingModel.getProperty("/schedulazioni").length; i++) {
            totalCount = totalCount + this.pmSchedulingModel.getProperty("/schedulazioni")[i].giorniScheduling;
        }
        this.pmSchedulingModel.setProperty("/giorniTotali", totalCount);
    },

    // funzione che viene avviata all'apertura del dialog 'viewConsultantScheduling'
    afterOpenConsultantScheduling: function (evt) {
        var dialogPeriodInput = sap.ui.getCore().byId("idSelectPeriodoConsultantDialog");
        var viewPeriodValue = this.getView().byId("idSelectPeriodo").getValue();
        dialogPeriodInput.setValue(viewPeriodValue);

        /////**********************day names on the header columns*********************************////
        var table = sap.ui.getCore().byId("idSchedulazioniConsulenteTable");
        table.setBusy(true);
        var period = model.persistence.Storage.session.get("period");
        var periodSelect = _.find(this.periodModel.getData().items, {
            'idPeriod': parseInt(period)
        });

        var pInizio = periodSelect.startDate;
        var ggInizio = pInizio.getDate();
        var mInizio = pInizio.getMonth() + 1;
        var annoInizio = pInizio.getFullYear();

        var pFine = periodSelect.endDate;
        var ggFine = pFine.getDate();
        var mFine = pFine.getMonth() + 1;
        var annoFine = pFine.getFullYear();

        if (ggInizio < 9) {
            ggInizio = "0" + ggInizio;
        }
        if (mInizio < 9) {
            mInizio = "0" + mInizio;
        }
        if (ggFine < 9) {
            ggFine = "0" + ggFine;
        }
        if (mFine < 9) {
            mFine = "0" + mFine;
        }

        var dataI = pInizio;
        var dataF = pFine;
        //---------------------------------------------------------------------------------------
        this._calcoloGiorni(dataF, dataI, ggInizio, mInizio, ggFine, mFine, annoInizio, annoFine);

        // ** rendo visibili solo le colonne del periodo selezionato
        var columns = table.getColumns();
        for (var i = 0; i < columns.length; i++) {
            columns[i].setVisible(true);
        }
        for (var i = this.numColonne; i < columns.length - 1; i++) {
            columns[i].setVisible(false);
        }

        // ** metto i nomi dei giorni sulle colonne
        for (var i = 2; i < columns.length; i++) {
            if (columns[i - 1].getVisible()) {
                columns[i - 1].getLabel().setProperty("text", this.arrayGiorni[i - 2]);
            } else {
                continue;
            }
        }
        table.setBusy(false);

        jQuery.sap.delayedCall(500, this, function () {
            table.rerender();
            this._popolaTabellaSchedulazioneSingoloUtente();
        });
    },

    // funzione che popola correttamente la tabella 'viewConsultantScheduling'
    _popolaTabellaSchedulazioneSingoloUtente: function () {
        var table = sap.ui.getCore().byId("idSchedulazioniConsulenteTable");
        table.setBusy(true);
        var rows = table.getRows();
        var arrayCounter = [];
        var toCheck = "toCheck";
        for (var i = 0; i < rows.length; i++) {
            var cellsPerRows = rows[i].getAggregation("cells");
            var g = 0;
            for (var j = 1; j < cellsPerRows.length - 1; j++) {
                var cella = cellsPerRows[j];
                if (_.find(this.weekendDays, _.bind(function (item) {
                        return (item == j);
                    }, this))) {
                    var column = $("#" + cella.getDomRef().id).parent().parent();
                    column.css('background-color', '#e5e5e5');
                }
                var propGiornaliera = "gg" + j;

                var richiestaGiornaliera = this.pmSchedulingConsultantModel.getProperty("/schedulazioni")[0];
                if (richiestaGiornaliera) {
                    while (richiestaGiornaliera[propGiornaliera]) {
                        var iconProp = propGiornaliera.replace("gg", "ggIcon");
                        var commesseQuestoConsulente = _.find(this.commessePerConsulente, {
                            cod: this.dataModelConsultant.codiceConsulente
                        }).commesse;
                        var stato = "";
                        if (_.find(commesseQuestoConsulente, {
                                nomeCommessa: richiestaGiornaliera[propGiornaliera]
                            })) {
                            stato = "None";
                        } else {
                            stato = "Warning";
                        }
                        var txt = new sap.m.ObjectStatus({
                            text: richiestaGiornaliera[propGiornaliera],
                            icon: richiestaGiornaliera[iconProp],
                            state: stato
                        });
                        cella.addAggregation("items", txt);
                        propGiornaliera = propGiornaliera + "a";
                    }
                }
            }
        }
        table.setBusy(false);
    },

    // funzione che apre il dialog delle note 'noteDialog'
    onAddNote: function () {
        if (!this._noteDialog) {
            this._noteDialog = sap.ui.xmlfragment(
                "view.dialog.noteDialog",
                this
            );
            this.getView().addDependent(this._noteDialog);
        }
        this._noteDialog.open();
    },

    // funzione che viene lanciata alla chiusura del dialog delle note
    onAfterCloseNoteDialog: function () {
        this.noteModel.setProperty("/note", undefined);
    },

    // funzione che chiude il dialog delle note
    onCancelNoteDialogPress: function (evt) {
        if (this._noteDialog) {
            this._noteDialog.close();
        }
        if (!model.persistence.Storage.local.get("undoPJM_1")) {
            this.visibleModel.setProperty("/undoButton", false);
        }
    },

    // funzione che salva le note
    onSaveNoteDialogPress: function () {
        var fSuccess = function (res) {
            this.onCancelNoteDialogPress();
            sap.m.MessageToast.show(this._getLocaleText("NOTE_ADDED"));
            var schedulazioneConsulente = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                codiceConsulente: this.dataForUpdate.codConsulente
            });
            var prop = "nota" + this.dataForUpdate.giorno;
            var propReq = "req" + this.dataForUpdate.giorno;
            if (schedulazioneConsulente[propReq] !== parseInt(this.dataForUpdate.id_req)) {
                prop = "nota" + this.dataForUpdate.giorno + "a";
            }
            schedulazioneConsulente[prop] = this.dataForUpdate.note;
            // funzione salvataggio per undo
            this.pmSchedulingModel.refresh();
            this.dataForUpdate = "";
            this.notaConsultant = undefined;
        };

        var fError = function (err) {
            sap.m.messageToast.show(this._getLocaleText("ERROR_NOTE_UPDATE"));
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if (this.notaConsultant === this.noteModel.getData().note) {
            this.onCancelNoteDialogPress();
        } else {
            this.dataForUpdate.note = this.noteModel.getData().note ? this.noteModel.getData().note : "";
            model.requestSave.updateNote(this.dataForUpdate)
                .then(fSuccess, fError);
        }
    },

    // funzione per la modifica delle note
    onNotePress: function (evt) {
        var nota = evt.getSource().data("note");
        var jobName = evt.getSource().data("job");
        var job = jobName === this.nomeCommessaFerie ? "AA00000000" : _.find(this.getView().getModel("pmSchedulingModel").getProperty("/commesse"), {
            nomeCommessa: jobName
        });
        var consultant = evt.getSource().getBindingContext("pmSchedulingModel").getObject();
        this.notaConsultant = consultant[nota];
        this.noteModel.setProperty("/note", consultant[nota]);
        var codReq = nota.substring(4);
        var propReq = "req" + codReq;

        this.dataForUpdate = {
            "codConsulente": consultant.codiceConsulente,
            "dataReqPeriodo": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "idCommessa": job.codCommessa,
            "codAttivitaRichiesta": "",
            "idPeriodoSchedulingRic": this.period,
            "codPjm": this.user.codice_consulente,
            "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "note": "",
            "statoReq": "CONFIRMED",
            "giorno": codReq.indexOf("a") >= 0 ? parseInt(codReq.substr(0, codReq.length - 1)) : parseInt(codReq),
            "id_req": consultant[propReq] ? consultant[propReq].toString() : ""
        };
        if (jobName === this.nomeCommessaFerie) {
            this.noteModel.setProperty("/editable", false);
        } else {
            this.noteModel.setProperty("/editable", true);
        }
        this.onAddNote();
    },

    /* INIZIO funzioni per la gestione dell' 'annulla' */
    // funzione che svuola il localStorage dai file salvati
    svuotaLocalStorageUndo: function () {
        var modelLocal = model.persistence.Storage.local;
        for (var i = 1; i < 6; i++) {
            var nameToSave = "undoPJM_";
            nameToSave = nameToSave + i;
            if (modelLocal.get(nameToSave)) {
                modelLocal.remove(nameToSave);
            }
        }
    },

    // funzione che salva in localStorage le richieste
    saveToLocalForUndo: function (requestType, dataToSave) {
        var modelLocal = model.persistence.Storage.local;
        var a = {};
        a[requestType] = dataToSave;
        for (var i = 1; i < 6; i++) {
            var nameToSave = "undoPJM_";
            nameToSave = nameToSave + i;
            if (!modelLocal.get(nameToSave)) {
                modelLocal.save(nameToSave, a);
                return;
            } else if (i === 5) {
                modelLocal.save("undoPJM_1", modelLocal.get("undoPJM_2"));
                modelLocal.save("undoPJM_2", modelLocal.get("undoPJM_3"));
                modelLocal.save("undoPJM_3", modelLocal.get("undoPJM_4"));
                modelLocal.save("undoPJM_4", modelLocal.get("undoPJM_5"));
                modelLocal.save("undoPJM_5", a);
            }
        }
    },

    // funzione che ricarica la tabella
    reloadPress: function () {
        this.leggiTabella(this.ric);
        this.visibleModel.setProperty("/undoButton", false);
        this.clearPasteCommesse();
        this.svuotaLocalStorageUndo();
        this.fromSaveLocal = true;
    },

    // funzione lanciata alla pressione del tasto annulla
    undoPress: function () {
        this.undoButton.setBusy(true);
        var modelLocal = model.persistence.Storage.local;
        for (var i = 5; i > 0; i--) {
            var nameToSaveOR = "undoPJM_";
            var nameToSave = nameToSaveOR + i;
            if (modelLocal.get(nameToSave)) {
                var dataToRestore = modelLocal.get(nameToSave);
                if (modelLocal.get(nameToSave).del) {
                    this.fromSaveLocal = false;
                    var oggetto = modelLocal.get(nameToSave).del;
                    oggetto.id_req = "";
                    this.saveRequest(oggetto);
                } else {
                    this.fromSaveLocal = false;
                    this._delData(modelLocal.get(nameToSave).insert);
                    var nameToSaveInf = nameToSaveOR + (i - 1);
                }
                modelLocal.remove(nameToSave);
                jQuery.sap.delayedCall(500, this, function () {
                    this.fromSaveLocal = true;
                });
                if (i === 1) {
                    this.visibleModel.setProperty("/undoButton", false);
                }
                return;
            }
        }
    },
    /* FINE funzioni per la gestione dell' 'annulla' */

    /* INIZIO funzioni inserimento multiplo */
    // funzione che apre il dialog 'insertMultiDay'
    onPressMultiDay: function (evt) {
        this.multiDayDialog = sap.ui.xmlfragment("view.dialog.insertMultiDay", this);
        var page = this.getView().byId("schedulingPage");
        page.addDependent(this.multiDayDialog);

        var table = this.getView().byId("idSchedulazioniTable");
        var giornoPrimaColonna = table.getColumns()[1].getAggregation("label").getProperty("text");
        this.console(giornoPrimaColonna.split(" ")[1], "success");
        this.numPrimoGiornoMese = parseInt(giornoPrimaColonna.split(" ")[0]);
        // controllare se giornoPrimaColonna è LUN

        //////////////////////////////////////
        var id = evt.getSource().getId();
        var rowId = evt.getSource().getBindingContext("pmSchedulingModel").getPath().split("/")[2];
        this.console(rowId);

        this.schedCons = this.pmSchedulingModel.getData().schedulazioni[rowId];
        model.persistence.Storage.session.save("schedCons", this.schedCons);
        this.setDay();
        this.insertMultiModel.setProperty("/name", this.schedCons.nomeConsulente);

        this.attivitaScelta = undefined;
        this.selectLun = undefined;
        this.selectMar = undefined;
        this.selectMer = undefined;
        this.selectGio = undefined;
        this.selectVen = undefined;
        this.selectSab = undefined;
        this.selectDom = undefined;
        this.selectTutto = undefined;
        this.selectSoloCommessa = undefined;

        this.multiDayDialog.open();
    },

    // funzione che disabilita i giorni della settimana se trova giorni schedulati
    setDay: function () {
        var arrayTemp = [];
        for (var e = 1; e < 37; e++) {
            var gg = "gg" + e;
            if (this.schedCons[gg]) {
                arrayTemp.push(e);
            }
        }

        for (var q = 0; q < arrayTemp.length; q++) {
            for (var k = 1; k <= 7; k++) {
                for (var i = 0; i < 5; i++) {
                    if (arrayTemp[q] === k + (i * 7)) {
                        if (k === 1) {
                            sap.ui.getCore().getElementById("lun").setEnabled(false);
                            sap.ui.getCore().getElementById("lun").setProperty("selected", false);
                        } else if (k === 2) {
                            sap.ui.getCore().getElementById("mar").setEnabled(false);
                            sap.ui.getCore().getElementById("mar").setProperty("selected", false);
                        } else if (k === 3) {
                            sap.ui.getCore().getElementById("mer").setEnabled(false);
                            sap.ui.getCore().getElementById("mer").setProperty("selected", false);
                        } else if (k === 4) {
                            sap.ui.getCore().getElementById("gio").setEnabled(false);
                            sap.ui.getCore().getElementById("gio").setProperty("selected", false);
                        } else if (k === 5) {
                            sap.ui.getCore().getElementById("ven").setEnabled(false);
                            sap.ui.getCore().getElementById("ven").setProperty("selected", false);
                        } else if (k === 6) {
                            sap.ui.getCore().getElementById("sab").setEnabled(false);
                            sap.ui.getCore().getElementById("sab").setProperty("selected", false);
                        } else {
                            sap.ui.getCore().getElementById("dom").setEnabled(false);
                            sap.ui.getCore().getElementById("dom").setProperty("selected", false);
                        }
                    }
                }
            }
        }
        this.multiModel.setProperty("/visibleSceltaCancella", false);
    },

    // funzione che chiude il dialog e ripristina i valori di default
    onPressDialogMultiDayClose: function () {
        sap.ui.getCore().getElementById("lun").setSelected(false);
        sap.ui.getCore().getElementById("mar").setSelected(false);
        sap.ui.getCore().getElementById("mer").setSelected(false);
        sap.ui.getCore().getElementById("gio").setSelected(false);
        sap.ui.getCore().getElementById("ven").setSelected(false);
        sap.ui.getCore().getElementById("sab").setSelected(false);
        sap.ui.getCore().getElementById("dom").setSelected(false);

        this.multiDayDialog.close();
        this.multiDayDialog.destroy(true);
        model.persistence.Storage.session.remove("schedCons");
    },

    // funzione che conferma l'inserimento massivo 
    onPressDialogMultiDayOK: function () {
        if (!this.attivitaScelta) {
            sap.m.MessageToast.show(this._getLocaleText("SELECT_ATTIVITA"));
            return;
        }
        if (!this.selectLun && !this.selectMar && !this.selectMer && !this.selectGio && !this.selectVen && !this.selectSab && !this.selectDom) {
            sap.m.MessageToast.show(this._getLocaleText("SELECT_DAY"));
            return;
        }
        if (this.attivitaScelta === "del" && !this.selectTutto && !this.selectSoloCommessa) {
            sap.m.MessageToast.show(this._getLocaleText("SELECT_CHOICE"));
            return;
        }

        this.numPeriodoMese = parseInt(model.persistence.Storage.session.get("period"));
        var annoOR = model.persistence.Storage.session.get("periodSelected").annoPeriodo;
        var anno;
        var periodoMese = model.persistence.Storage.session.get("periodSelected").mesePeriodo;
        if (periodoMese < 10) {
            periodoMese = "0" + periodoMese;
        }
        var giorni = this.numColonne - 1;
        var inserimenti = [];

        var note = sap.ui.getCore().getElementById("idNoteMassivo").getValue();

        if (this.attivitaScelta !== "del") {
            var dataToSend = {
                "codConsulente": this.schedCons.codiceConsulente,
                "dataReqPeriodo": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
                "idCommessa": this.getView().byId("idSelectCommessa").getSelectedKey(),
                "codAttivitaRichiesta": this.attivitaScelta,
                "idPeriodoSchedulingRic": this.numPeriodoMese,
                "codPjm": this.user.codice_consulente,
                "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
                "note": note ? note : "",
                "statoReq": "CONFIRMED",
                "giorno": 0,
                "id_req": ""
            };

            var primoGG;
            var iteratore;
            var i = 0;
            var data;
            var lastDayOfMonth;
            var mesePlus;
            var giornoPlus;
            if (this.selectLun) {
                for (i = 0; i < giorni / 7; i++) {
                    primoGG = this.numPrimoGiornoMese + (i * 7);
                    if (primoGG < 10) {
                        primoGG = "0" + primoGG;
                    }
                    iteratore = _.clone(dataToSend);
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12) {
                            anno = parseInt(anno) + 1;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (1 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }
            if (this.selectMar) {
                for (i = 0; i < giorni / 7; i++) {
                    primoGG = this.numPrimoGiornoMese + 1 + (i * 7);
                    iteratore = _.clone(dataToSend);
                    if ((primoGG) < 10) {
                        primoGG = "0" + primoGG;
                    }
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12) {
                            anno = parseInt(anno) + 1;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (2 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }
            if (this.selectMer) {
                for (i = 0; i < giorni / 7; i++) {
                    primoGG = this.numPrimoGiornoMese + 2 + (i * 7);
                    iteratore = _.clone(dataToSend);
                    if (primoGG < 10) {
                        primoGG = "0" + primoGG;
                    }
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12) {
                            anno = parseInt(anno) + 1;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (3 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }
            if (this.selectGio) {
                for (i = 0; i < giorni / 7; i++) {
                    iteratore = _.clone(dataToSend);
                    primoGG = this.numPrimoGiornoMese + 3 + (i * 7);
                    if (primoGG < 10) {
                        primoGG = "0" + primoGG;
                    }
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12) {
                            anno = parseInt(anno) + 1;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (4 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }
            if (this.selectVen) {
                for (i = 0; i < giorni / 7; i++) {
                    iteratore = _.clone(dataToSend);
                    primoGG = this.numPrimoGiornoMese + 4 + (i * 7);
                    if (primoGG < 10) {
                        primoGG = "0" + primoGG;
                    }
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12) {
                            anno = parseInt(anno) + 1;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (5 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }
            if (this.selectSab) {
                for (i = 0; i < giorni / 7; i++) {
                    iteratore = _.clone(dataToSend);
                    primoGG = this.numPrimoGiornoMese + 5 + (i * 7);
                    if (primoGG < 10) {
                        primoGG = "0" + primoGG;
                    }
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12) {
                            anno = parseInt(anno) + 1;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (6 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }
            if (this.selectDom) {
                for (i = 0; i < giorni / 7; i++) {
                    iteratore = _.clone(dataToSend);
                    primoGG = this.numPrimoGiornoMese + 6 + (i * 7);
                    if (primoGG < 10) {
                        primoGG = "0" + primoGG;
                    }
                    anno = annoOR;
                    data = anno + "-" + periodoMese + "-" + (primoGG);
                    if (!(new Date(data)).getYear()) {
                        lastDayOfMonth = new Date(parseInt(data.split("-")[0]), parseInt(data.split("-")[1]), 0);
                        lastDayOfMonth = lastDayOfMonth.getDate();
                        mesePlus = periodoMese !== 12 ? parseInt(periodoMese) + 1 : 1;
                        if (mesePlus < 10) {
                            mesePlus = "0" + mesePlus;
                        }
                        if (parseInt(periodoMese) === 12 && !this.annoTrue) {
                            anno = parseInt(anno) + 1;
                            this.annoTrue = true;
                        }
                        giornoPlus = "0" + (parseInt(data.split("-")[2]) - lastDayOfMonth);
                        data = anno + "-" + mesePlus + "-" + giornoPlus;
                    } else {
                        data = (new Date(data)).getFullYear() + "-" + ((new Date(data)).getMonth() + 1) + "-" + (new Date(data)).getDate();
                    }
                    iteratore.dataReqPeriodo = data;
                    iteratore.giorno = (7 + (i * 7));
                    inserimenti.push(iteratore);
                }
            }

            var fControlloError = function (err) {
                sap.m.MessageToast.show(err);
            };

            var fControlloSuccess = function (result) {
                var scriviRichieste = true;
                for (var i = 0; i < result.length; i++) {
                    var schedulazione = _.find(inserimenti, {
                        'giorno': result[i].giornoPeriodo
                    });
                    if (schedulazione) {
                        scriviRichieste = false;
                        break;
                    }
                }
                if (scriviRichieste) {
                    this.saveMultipleRequest(inserimenti);
                } else {
                    sap.m.MessageBox.show(
                        this._getLocaleText("SHOW_SCHEDULE_CONFLICTS_WITH_OTHER_PJM"), {
                            icon: sap.m.MessageBox.Icon.INFORMATION,
                            title: this._getLocaleText("SCHEDULE_CONFLICTS_TITLE"),
                            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                            onClose: _.bind(function (oAction) {
                                if (oAction === "YES") {
                                    this.multipleRequest = true;
                                    var req = {};
                                    var commessaScelta = model.persistence.Storage.session.get("commessa");
                                    req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
                                    req.codPjm = this.user.codice_consulente;
                                    req.codConsulente = inserimenti[0].codConsulente;
                                    this.dataModelConsultant = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                                        codiceConsulente: inserimenti[0].codConsulente
                                    });
                                    this._leggiTabellaSchedulingConsulenti(req);
                                    if (!this.schedConsultantDialog) {
                                        this.schedConsultantDialog = sap.ui.xmlfragment("view.dialog.viewConsultantScheduling", this);
                                        this.getView().addDependent(this.schedConsultantDialog);
                                    }
                                    this.schedConsultantDialog.open();
                                    this.inserimentiBackup = inserimenti;
                                } else {
                                    this.saveMultipleRequest(inserimenti);
                                }
                            }, this),
                        }
                    );
                }
            };

            fControlloSuccess = _.bind(fControlloSuccess, this);
            fControlloError = _.bind(fControlloError, this);

            // faccio un controllo per vedere se sto per inserire delle richieste dove
            // già altri consulenti ne hanno messe per quello stesso consulente
            if (inserimenti.length > 0) {
                this.consulenteSelezionato = inserimenti[0].codConsulente;
                if (this.consulenteSelezionato.indexOf("CCG") < 0) {
                    model.tabellaScheduling.readSchedulazioni(inserimenti[0].codPjm, inserimenti[0].codConsulente, inserimenti[0].idPeriodoSchedulingRic)
                        .then(fControlloSuccess, fControlloError);
                } else {
                    this.saveMultipleRequest(inserimenti);
                }
            }
        } else {
            var arrayToRemove = [];
            var r = 0;
            var gg;
            var req;
            if (this.selectLun) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (1 + (r * 7));
                    req = "req" + (1 + (r * 7));

                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== this.nomeCommessaFerie) {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                //                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                if (this.schedCons[gg] === this.chosedCom) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }
            if (this.selectMar) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (2 + (r * 7));
                    req = "req" + (2 + (r * 7));
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== this.nomeCommessaFerie) {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                //                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                if (this.schedCons[gg] === this.chosedCom) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }
            if (this.selectMer) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (3 + (r * 7));
                    req = "req" + (3 + (r * 7));
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== this.nomeCommessaFerie) {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                //                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                if (this.schedCons[gg] === this.chosedCom) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }
            if (this.selectGio) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (4 + (r * 7));
                    req = "req" + (4 + (r * 7));
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== this.nomeCommessaFerie) {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                //                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                if (this.schedCons[gg] === this.chosedCom) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }
            if (this.selectVen) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (5 + (r * 7));
                    req = "req" + (5 + (r * 7));
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== this.nomeCommessaFerie) {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                //                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                if (this.schedCons[gg] === this.chosedCom) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }
            if (this.selectSab) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (6 + (r * 7));
                    req = "req" + (6 + (r * 7));
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== this.nomeCommessaFerie) {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                //                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                if (this.schedCons[gg] === this.chosedCom) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }
            if (this.selectDom) {
                for (r = 0; r < giorni / 7; r++) {
                    gg = "gg" + (7 + (r * 7));
                    req = "req" + (7 + (r * 7));
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (this.schedCons[req]) {
                        if (this.schedCons[req] && this.schedCons[gg] !== this.nomeCommessaFerie) {
                            if (!this.selectSoloCommessa) {
                                arrayToRemove.push(this.schedCons[req]);
                            } else {
                                //                                if (this.schedCons[gg] === this.getView().byId("idSelectCommessa").getValue()) {
                                if (this.schedCons[gg] === this.chosedCom) {
                                    arrayToRemove.push(this.schedCons[req]);
                                }
                            }
                        }
                        items = "a";
                        req = req + items;
                        gg = gg + items;
                    }
                }
            }

            var fSuccess = function (result) {
                sap.m.MessageToast.show(this._getLocaleText("CANCELLAZIONE_OK"));
                var res = JSON.parse(result).data;
                var idElementiEliminati = [];
                for (var t = 0; t < res.length; t++) {
                    idElementiEliminati.push({
                        id: res[t]
                    });
                }
                var questoConsulente = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                    codiceConsulente: this.schedCons.codiceConsulente
                });

                for (var i = 1; i < 37; i++) {
                    var req = "req" + i;
                    var reqA = "req" + i + "a";
                    if (this.schedCons[req]) {
                        if (_.find(idElementiEliminati, {
                                id: questoConsulente[req]
                            }) && _.find(idElementiEliminati, {
                                id: questoConsulente[reqA]
                            })) {
                            var prop = "gg" + i;
                            var propConflict = "ggConflict" + i;
                            var propIcon = "ggIcon" + i;
                            var propReq = "ggReq" + i;
                            var propNota = "nota" + i;

                            delete questoConsulente[prop];
                            delete questoConsulente[propConflict];
                            delete questoConsulente[propIcon];
                            delete questoConsulente[propReq];
                            delete questoConsulente[propNota];
                            delete questoConsulente[req];
                            var propA = "gg" + i + "a";
                            var propIconA = "ggIcon" + i + "a";
                            var propReqA = "ggReq" + i + "a";
                            var propNotaA = "nota" + i + "a";
                            var reqA = "req" + i + "a";
                            var propConflictA = "ggConflict" + i + "a";
                            delete questoConsulente[propA];
                            delete questoConsulente[propConflictA];
                            delete questoConsulente[propIconA];
                            delete questoConsulente[propReqA];
                            delete questoConsulente[propNotaA];
                            delete questoConsulente[reqA];
                        } else if (_.find(idElementiEliminati, {
                                id: questoConsulente[req]
                            })) {
                            var prop = "gg" + i;
                            var propConflict = "ggConflict" + i;
                            var propIcon = "ggIcon" + i;
                            var propReq = "ggReq" + i;
                            var propNota = "nota" + i;

                            delete questoConsulente[prop];
                            delete questoConsulente[propConflict];
                            delete questoConsulente[propIcon];
                            delete questoConsulente[propReq];
                            delete questoConsulente[propNota];
                            delete questoConsulente[req];
                        } else if (_.find(idElementiEliminati, {
                                id: questoConsulente[reqA]
                            })) {
                            var propA = "gg" + i + "a";
                            var propIconA = "ggIcon" + i + "a";
                            var propReqA = "ggReq" + i + "a";
                            var propNotaA = "nota" + i + "a";
                            var reqA = "req" + i + "a";
                            var propConflictA = "ggConflict" + i + "a";
                            delete questoConsulente[propA];
                            delete questoConsulente[propConflictA];
                            delete questoConsulente[propIconA];
                            delete questoConsulente[propReqA];
                            delete questoConsulente[propNotaA];
                            delete questoConsulente[reqA];
                        }
                    }
                }
                this._calcoloGiornate(questoConsulente);
                this.svuotaLocalStorageUndo();
                this.visibleModel.setProperty("/undoButton", false);
            };

            var fError = function (err) {
                sap.m.MessageToast.show(err);
            };

            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            if (arrayToRemove.length > 0) {
                model.reqToDel.delMulti(arrayToRemove)
                    .then(fSuccess, fError);
            } else {
                sap.m.MessageToast.show(this._getLocaleText("NIENTE_DA_CANCELLARE"));
            }
        }

        this.multiDayDialog.close();
        this.multiDayDialog.destroy(true);
        model.persistence.Storage.session.remove("schedCons");
    },

    // funzione di inserimento multiplo in tabella
    saveMultipleRequest: function (inserimenti, fromExcel) {
        this.inserimenti = inserimenti;
        var fSuccess = function (result) {
            sap.m.MessageToast.show(this._getLocaleText("SCRITTURA_OK"));

            if (fromExcel) {
                this.reloadPress();
                return;
            }
            var questoConsulente = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                codiceConsulente: this.inserimenti[0].codConsulente
            });

            for (var i = 0; i < this.inserimenti.length; i++) {
                var prop = "gg" + this.inserimenti[i].giorno;
                var propConflict = "ggConflict" + this.inserimenti[i].giorno;
                var propIcon = "ggIcon" + this.inserimenti[i].giorno;
                var propReq = "ggReq" + this.inserimenti[i].giorno;
                var propNota = "nota" + this.inserimenti[i].giorno;
                var req = "req" + this.inserimenti[i].giorno;

                var icona = "";
                icona = "sap-icon://" + this.inserimenti[i].codAttivitaRichiesta;

                questoConsulente[prop] = _.find(this.pmSchedulingModel.getData().commesse, {
                    codCommessa: this.inserimenti[i].idCommessa
                }).nomeCommessa;
                questoConsulente[propConflict] = undefined;
                questoConsulente[propIcon] = icona;
                questoConsulente[propReq] = this.inserimenti[i].dataReqPeriodo;
                questoConsulente[propNota] = this.inserimenti[i].note;

                var dataResult = JSON.parse(result).result;
                var reqId = _.find(dataResult, {
                    giorno: this.inserimenti[i].giorno
                }).id;
                questoConsulente[req] = reqId;
                this._calcoloGiornate(questoConsulente);
            }
            this.pmSchedulingModel.refresh();

            this.svuotaLocalStorageUndo();
            this.visibleModel.setProperty("/undoButton", false);
        };

        var fError = function (err) {
            sap.m.MessageToast.show(err);
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.requestSave.sendMultiRequest(inserimenti)
            .then(fSuccess, fError);
    },

    onPressAssign: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "complete";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressHalf: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "time-entry-request";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressConsu: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "customer-and-contacts";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressNoConsu: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "employee-rejections";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressCust: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "collaborate";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressNoCust: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "offsite-work";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressNoConfCust: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "role";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressAbr: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("delMulti").setProperty("pressed", false);
            this.attivitaScelta = "globe";
            this.setDay();
        } else {
            this.attivitaScelta = undefined;
        }
    },

    onPressDel: function (evt) {
        if (evt.getSource().getProperty("pressed")) {
            sap.ui.getCore().getElementById("AssignMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("halfDayMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonConsMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("wNonCustMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("notConfirmedMulti").setProperty("pressed", false);
            sap.ui.getCore().getElementById("abroadMulti").setProperty("pressed", false);
            this.attivitaScelta = "del";
            sap.ui.getCore().getElementById("lun").setEnabled(true);
            sap.ui.getCore().getElementById("mar").setEnabled(true);
            sap.ui.getCore().getElementById("mer").setEnabled(true);
            sap.ui.getCore().getElementById("gio").setEnabled(true);
            sap.ui.getCore().getElementById("ven").setEnabled(true);
            sap.ui.getCore().getElementById("sab").setEnabled(true);
            sap.ui.getCore().getElementById("dom").setEnabled(true);
            this.multiModel.setProperty("/visibleSceltaCancella", true);
        } else {
            this.attivitaScelta = undefined;
            this.multiModel.setProperty("/visibleSceltaCancella", false);
            this.setDay();
        }
    },

    onSelectLun: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectLun = true;
        } else {
            this.selectLun = false;
        }
    },

    onSelectMar: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectMar = true;
        } else {
            this.selectMar = false;
        }
    },

    onSelectMer: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectMer = true;
        } else {
            this.selectMer = false;
        }
    },

    onSelectGio: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectGio = true;
        } else {
            this.selectGio = false;
        }
    },

    onSelectVen: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectVen = true;
        } else {
            this.selectVen = false;
        }
    },

    onSelectSab: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectSab = true;
        } else {
            this.selectSab = false;
        }
    },

    onSelectDom: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectDom = true;
        } else {
            this.selectDom = false;
        }
    },

    onSelectTutto: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectTutto = true;
            if (sap.ui.getCore().getElementById("soloCommessa").getProperty("selected")) {
                sap.ui.getCore().getElementById("soloCommessa").setProperty("selected", false);
            }
        } else {
            this.selectTutto = false;
        }
    },

    onSelectSoloCommessa: function (evt) {
        if (evt.getSource().getProperty("selected")) {
            this.selectSoloCommessa = true;
            if (sap.ui.getCore().getElementById("tutto").getProperty("selected")) {
                sap.ui.getCore().getElementById("tutto").setProperty("selected", false);
            }
        } else {
            this.selectSoloCommessa = false;
        }
    },

    onPressDialogCopyFrom: function () {
        this.onPressDialogMultiDayClose();
        this.copyActivitiesDialog = sap.ui.xmlfragment("view.dialog.copyActivitiesDialog", this);
        var page = this.getView().byId("schedulingPage");
        page.addDependent(this.copyActivitiesDialog);
        var cloneModel = _.clone(this.pmSchedulingModel.getData().schedulazioni);
        _.remove(cloneModel, this.schedCons);
        _.remove(cloneModel, {
            'giorniScheduling': 0
        })
        this.multiModel.setData(cloneModel);
        this.multiModel.setProperty("/visibleSceltaCancellaCopyActivities", true);
        this.multiModel.setProperty("/visibleSceltaCancellaCopyActivitiesInv", false);
        this.copyActivitiesDialog.open();
    },

    onDialogCopyActivitiesClose: function () {
        this.copyActivitiesDialog.open();
        this.copyActivitiesDialog.destroy(true);
    },

    onDialogCopyActivitiesOK: function (evt) {
        if (!this.selectTutto && !this.selectSoloCommessa) {
            this.multiModel.setProperty("/visibleSceltaCancellaCopyActivities", false);
            this.multiModel.setProperty("/visibleSceltaCancellaCopyActivitiesInv", true);
        } else {
            var aContexts = sap.ui.getCore().getElementById("listaConsulenti").getSelectedItems();
            var consSelezionato = aContexts[0].getProperty("description");
            var allInfoConsSelezionato = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                'codiceConsulente': consSelezionato
            });
            var numPeriodoMese = parseInt(model.persistence.Storage.session.get("period"));
            var dataToSend = {
                "codConsulente": this.schedCons.codiceConsulente,
                "idPeriodoSchedulingRic": numPeriodoMese,
                "codPjm": this.user.codice_consulente,
                "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
                "note": "",
                "statoReq": "CONFIRMED",
                "id_req": ""
            };
            var inserimenti = [];

            for (var i = 1; i < 36; i++) {
                var iteratore = _.clone(dataToSend);
                var gg = "gg" + i;
                var ggIcon = "ggIcon" + i;
                var ggReq = "ggReq" + i;
                var ggNota = "nota" + i;
                if (allInfoConsSelezionato[gg] && !this.schedCons[gg]) {
                    var commessa = _.find(this.pmSchedulingModel.getData().commesse, {
                        'nomeCommessa': allInfoConsSelezionato[gg]
                    });
                    if (commessa !== undefined) {
                        var codCommessa = commessa.codCommessa;
                        var icona = allInfoConsSelezionato[ggIcon].split("//")[1];
                        iteratore.dataReqPeriodo = allInfoConsSelezionato[ggReq];
                        iteratore.note = allInfoConsSelezionato[ggNota];
                        iteratore.idCommessa = codCommessa;
                        iteratore.codAttivitaRichiesta = icona;
                        iteratore.giorno = i;

                        if (!this.selectSoloCommessa) {
                            inserimenti.push(iteratore);
                        } else {
                            //                            if (commessa.nomeCommessa === this.getView().byId("idSelectCommessa").getValue()) {
                            if (commessa.nomeCommessa === this.chosedCom) {
                                inserimenti.push(iteratore);
                            }
                        }

                    }
                }
            }

            var fControlloSuccess = function (result) {
                var scriviRichieste = true;
                for (var i = 0; i < result.length; i++) {
                    var schedulazione = _.find(inserimenti, {
                        'giorno': result[i].giornoPeriodo
                    });
                    if (schedulazione) {
                        scriviRichieste = false;
                        break;
                    }
                }
                if (scriviRichieste) {
                    this.saveMultipleRequest(inserimenti);
                } else {
                    sap.m.MessageBox.show(
                        this._getLocaleText("SHOW_SCHEDULE_CONFLICTS_WITH_OTHER_PJM"), {
                            icon: sap.m.MessageBox.Icon.INFORMATION,
                            title: this._getLocaleText("SCHEDULE_CONFLICTS_TITLE"),
                            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                            onClose: _.bind(function (oAction) {
                                if (oAction === "YES") {
                                    this.multipleRequest = true;
                                    var req = {};
                                    req.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));
                                    req.idPJM = this.user.codice_consulente;
                                    req.codConsulente = inserimenti[0].codConsulente;
                                    this.dataModelConsultant = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                                        codiceConsulente: inserimenti[0].codConsulente
                                    });
                                    this._leggiTabellaSchedulingConsulenti(req);
                                    if (!this.schedConsultantDialog) {
                                        this.schedConsultantDialog = sap.ui.xmlfragment("view.dialog.viewConsultantScheduling", this);
                                        this.getView().addDependent(this.schedConsultantDialog);
                                    }
                                    this.schedConsultantDialog.open();
                                    this.inserimentiBackup = inserimenti;
                                } else {
                                    this.saveMultipleRequest(inserimenti);
                                }
                            }, this),
                        }
                    );
                }
            };

            var fError = function (err) {
                sap.m.MessageToast.show(err);
            };

            fControlloSuccess = _.bind(fControlloSuccess, this);
            fError = _.bind(fError, this);

            // faccio un controllo per vedere se sto per inserire delle richieste dove
            // già altri consulenti ne hanno messe per quello stesso consulente
            if (inserimenti.length > 0) {
                this.consulenteSelezionato = inserimenti[0].codConsulente;
                if (this.consulenteSelezionato.indexOf("CCG") < 0) {
                    model.tabellaScheduling.readSchedulazioni(inserimenti[0].codPjm, inserimenti[0].codConsulente, inserimenti[0].idPeriodoSchedulingRic)
                        .then(fControlloSuccess, fError);
                } else {
                    this.saveMultipleRequest(inserimenti);
                }
            }
            this.copyActivitiesDialog.open();
            this.copyActivitiesDialog.destroy(true);
        }
    },
    /* FINE funzioni inserimento multiplo */

    /* DOWNOLAD EXCEL */
    onDownloadExcelPress: function (evt) {
        var oButton = evt.getSource();

        
        if (this.switchModel.getProperty("/state")) {
            this._onDownloadExcelPress();
        } else {
            this._scegliCommessa();
        }
        
//        if (!this._actionSheetExcel) {
//            this._actionSheetExcel = sap.ui.xmlfragment(
//                "view.fragment.ActionSheetExcel",
//                this
//            );
//            this.getView().addDependent(this._actionSheet);
//        }
//        this._actionSheetExcel.setModel(this.getView().getModel("i18n"), "i18n");
//        this._actionSheetExcel.setModel(this.switchModel, "switchModel");
//        this._actionSheetExcel.openBy(oButton);
    },

    _scegliCommessa: function () {
        if (!this._dialogCommessaExcel) {
            this._dialogCommessaExcel = sap.ui.xmlfragment(
                "view.dialog.selectCommessaExcelDialog",
                this
            );
            this.getView().addDependent(this._dialogCommessaExcel);
        }
        this._dialogCommessaExcel.setModel(this.getView().getModel("i18n"), "i18n");
        this._dialogCommessaExcel.open();
    },

    _onSelectionCommesse: function (evt) {
        var oList = evt.getSource();
        this.selezioneCommesse = oList.getSelectedContexts(true);
    },

    handleCancelSelectCommessaExcelDialog: function () {
        this._dialogCommessaExcel.close();
    },

    handleConfirmSelectCommessaExcelDialog: function () {
        if (this.selezioneCommesse && this.selezioneCommesse.length) {
            var arrayReq = [];
            for (var i = 0; i < this.selezioneCommesse.length; i++) {
                var path = parseInt(this.selezioneCommesse[i].getPath().split("/")[2]);
                arrayReq.push(this.selezioneCommesse[i].getModel().getData().commesse[path]);
            }

            var ric = {};
            ric.cod_consulente = (this.user).codice_consulente;
            ric.idPeriodo = parseInt(model.persistence.Storage.session.get("period"));

            var fSuccessAll = function (results) {
                var schedulazioni = results.resultOrdinato;
                var arr = [];
                for (var t = 0; t < arrayReq.length; t++) {
                    for (var i = 0; i < this.commessePerConsulente.length; i++) {
                        if ((_.find(this.commessePerConsulente[i].commesse, {
                                nomeCommessa: arrayReq[t].nomeCommessa
                            }))) {
                            arr.push(_.find(this.commessePerConsulente[i].commesse, {
                                nomeCommessa: arrayReq[t].nomeCommessa
                            }));
                        }
                    }
                }
                var group = _.groupBy(arr, 'codConsulente');
                var sched = [];
                for (var prop in group) {
                    var singularSched = _.find(schedulazioni, {
                        codiceConsulente: prop
                    });
                    if (singularSched) {
                        if (singularSched.codiceConsulente.indexOf("CCG") === 0) {
                            var genericConsultant = group[singularSched.codiceConsulente][0];
                            singularSched.nomeConsulente = this._getLocaleText("GENERIC_CONSULTANT") + ":\n" + genericConsultant.nomeSkill;
                        }
                        sched.push(singularSched);
                    }
                }
                this.excelModel.setProperty("/schedulazioni", sched);
                this.oDialogTable.setBusy(false);
                this._excelCommesseSelezionate(arrayReq);
            };

            var fError = function (err) {
                this.console(err, "error");
                this._enterErrorFunction("error", "Errore", "Errore lettura tabella: " + err);
            };

            fSuccessAll = _.bind(fSuccessAll, this);
            fError = _.bind(fError, this);

            this.oDialogTable.setBusy(true);
            model.tabellaScheduling.readPjmAllRequests(ric)
                .then(fSuccessAll, fError);
        } else {
            sap.m.MessageToast.show(this._getLocaleText("NO_PROJECT_CHOSEN"));
        }
        this._dialogCommessaExcel.getAggregation("content")[0].removeSelections();
        this._dialogCommessaExcel.close();
    },

    _excelCommesseSelezionate: function (commesse) {
        this.downloadCommesseMultiple = true;
        var columns = [];
        var item = [];
        var rows = [];
        var column = {};
        var days = this.pmSchedulingModel.getProperty("/days");
        var daysLength = days.length;
        var schedulazioni = this.excelModel.getProperty("/schedulazioni");
        var schedulazioniLength = schedulazioni.length;
        for (var i = -1; i < daysLength; i++) {
            if (i === -1) {
                column = {
                    "name": this._getLocaleText("CONSULENTI")
                };
            } else {
                column = {
                    "name": days[i]
                };
            }
            columns.push(column);
        }

        for (var j = 0; j < schedulazioniLength; j++) {
            var row = [];
            for (var i = -1; i < daysLength; i++) {
                var item = "";
                if (i === -1) {
                    var consultantName = schedulazioni[j].nomeConsulente;
                    // ho commentato la seguente riga per togliere le ore dal download dell'excel (jira n° 82)
                    //                    consultantName += ": " + schedulazioni[j].giorniScheduling;
                    item = consultantName;
                } else {
                    var index = i + 1;
                    var property = "gg" + index;
                    var propertyA = "gg" + index + "a";
                    var icon = "ggIcon" + index;
                    var iconA = "ggIcon" + index + "a";

                    var css = "";
                    switch (schedulazioni[j][icon]) {
                        case "sap-icon://complete":
                            css = "EXassign";
                            break;
                        case "sap-icon://time-entry-request":
                            css = "EXhalfDay";
                            break;
                        case "sap-icon://customer-and-contacts":
                            css = "EXwCons";
                            break;
                        case "sap-icon://employee-rejections":
                            css = "EXwNonCons";
                            break;
                        case "sap-icon://collaborate":
                            css = "EXwCust";
                            break;
                        case "sap-icon://offsite-work":
                            css = "EXwNonCust";
                            break;
                        case "sap-icon://role":
                            css = "EXnotConfirmed";
                            break;
                        case "sap-icon://globe":
                            css = "EXabroad";
                            break;
                    }

                    // la seguente proprietà mi serve per capire se nello stesso giorno un consulente ha due mezze giornate per la stessa commessa
                    var entratoInRapportino = false;
                    if (_.find(commesse, {
                            nomeCommessa: schedulazioni[j][property]
                        }) && schedulazioni[j][icon] !== "sap-icon://time-entry-request") {
                        item = schedulazioni[j][property] + "_" + css;
                    } else if (_.find(commesse, {
                            nomeCommessa: schedulazioni[j][property]
                        }) && schedulazioni[j][icon] === "sap-icon://time-entry-request") {
                        item = schedulazioni[j][property] + "_" + css;
                    }
                }
                row.push(item);
            }
            rows.push(row);
        }

        this.completeModel = new sap.ui.model.json.JSONModel();
        this.completeModel.setData({
            rows: rows,
            columns: columns
        });

        if (!this.excelDialog) {
            this.excelDialog = sap.ui.xmlfragment("view.dialog.excelDownload", this);
            this.getView().addDependent(this.excelDialog);
        }
        this.excelDialog.open();
    },

    _onDownloadExcelPress: function () {
        //        var commessaScelta = this.getView().byId("idSelectCommessa").getSelectedItem().mProperties.text;
        var commessaScelta = this.chosedCom;
        var columns = [];
        var item = [];
        var rows = [];
        var column = {};
        var days = this.pmSchedulingModel.getProperty("/days");
        var daysLength = days.length;
        var schedulazioni = this.pmSchedulingModel.getProperty("/schedulazioni");
        var schedulazioniLength = schedulazioni.length;
        for (var i = -1; i < daysLength; i++) {
            if (i === -1) {
                column = {
                    "name": this._getLocaleText("CONSULENTI")
                };
            } else {
                column = {
                    "name": days[i]
                };
            }
            columns.push(column);
        }

        for (var j = 0; j < schedulazioniLength; j++) {
            var row = [];
            for (var i = -1; i < daysLength; i++) {
                var item = "";
                if (i === -1) {
                    var consultantName;
                    // se è un consulente generico, allora mostro lo skill
                    if (schedulazioni[j].codiceConsulente.indexOf("CCG") >= 0) {
                        consultantName = this._getLocaleText("GENERIC_CONSULTANT") + ":\n" + schedulazioni[j].nomeSkill;
                    } else {
                        consultantName = schedulazioni[j].nomeConsulente;
                    }
                    // ho commentato la seguente riga per togliere le ore dal download dell'excel (jira n° 82)
                    //                    consultantName += ": " + schedulazioni[j].giorniScheduling;
                    item = consultantName;
                } else {
                    var index = i + 1;
                    var property = "gg" + index;
                    var propertyA = "gg" + index + "a";
                    var icon = "ggIcon" + index;
                    var iconA = "ggIcon" + index + "a";

                    var css = "";
                    switch (schedulazioni[j][icon]) {
                        case "sap-icon://complete":
                            css = "EXassign";
                            break;
                        case "sap-icon://time-entry-request":
                            css = "EXhalfDay";
                            break;
                        case "sap-icon://customer-and-contacts":
                            css = "EXwCons";
                            break;
                        case "sap-icon://employee-rejections":
                            css = "EXwNonCons";
                            break;
                        case "sap-icon://collaborate":
                            css = "EXwCust";
                            break;
                        case "sap-icon://offsite-work":
                            css = "EXwNonCust";
                            break;
                        case "sap-icon://role":
                            css = "EXnotConfirmed";
                            break;
                        case "sap-icon://globe":
                            css = "EXabroad";
                            break;
                    }

                    // la seguente proprietà mi serve per capire se nello stesso giorno un consulente ha due mezze giornate per la stessa commessa
                    var entratoInRapportino = false;
                    if (schedulazioni[j][property] === commessaScelta && schedulazioni[j][icon] !== "sap-icon://time-entry-request") {
                        item = "1_" + css;
                    } else if (schedulazioni[j][property] === commessaScelta && schedulazioni[j][icon] === "sap-icon://time-entry-request") {
                        item = "0.5_" + css;
                    }
                }
                row.push(item);
            }
            rows.push(row);
        }

        this.completeModel = new sap.ui.model.json.JSONModel();
        this.completeModel.setData({
            rows: rows,
            columns: columns
        });

        if (!this.excelDialog) {
            this.excelDialog = sap.ui.xmlfragment("view.dialog.excelDownload", this);
            this.getView().addDependent(this.excelDialog);
        }
        this.excelDialog.open();
    },

    afterExcelDialog: function () {
        var activityForm = sap.ui.getCore().getElementById("idExcelDialog");
        if (this.completeList) {
            activityForm.removeContent(this.completeList);
            this.completeList.destroy();
        }
        this.completeList = new sap.m.List({
            id: "listaSchedulazioni",
            showSeparators: "All"
        });

        var columns = this.completeModel.getData().columns;

        for (var i = 0; i < columns.length; i++) {
            var minWidth = "15px";
            if (i === 0) {
                minWidth = "50px";
            }
            this.completeList.addColumn(new sap.m.Column({
                width: minWidth,
                hAlign: "Left",
                vAlign: "Middle",
                header: new sap.m.Label({
                    text: columns[i].name,
                    design: sap.m.LabelDesign.Bold
                })
            }));
        }

        var rows = this.completeModel.getData().rows;
        for (var j = 0; j < rows.length; j++) {
            var item = new sap.m.ColumnListItem({
                width: "100%"
            });
            for (var k = 0; k < rows[j].length; k++) {
                if (rows[j][k] !== "") {
                    var tx1 = rows[j][k].split("_")[0];
                    var tx2 = rows[j][k].split("_")[1];
                    item.addCell(new sap.m.Text({
                        text: tx1
                    }));
                    item.getCells()[k].addStyleClass(tx2);
                } else {
                    item.addCell(new sap.m.Text({
                        text: rows[j][k]
                    }));
                }
            }
            this.completeList.addItem(item);
        }
        activityForm.addContent(this.completeList);

        if (this.legend) {
            activityForm.removeContent(this.legend);
            this.legend.destroy();
        }
        this.legend = new sap.m.List({
            id: "legendaSchedulazioni",
            showSeparators: "All"
        });

        for (var i = 0; i < 8; i++) {
            var minWidth = "12.50%";
            this.legend.addColumn(new sap.m.Column({
                width: minWidth,
                hAlign: "Center",
                vAlign: "Middle",
            }));
        }

        var item = new sap.m.ColumnListItem({
            width: "100%"
        });
        item.addCell(new sap.m.Text({
            text: "Assegnata"
        }));
        item.getCells()[0].addStyleClass("EXassign");
        item.addCell(new sap.m.Text({
            text: "Mezza giornata"
        }));
        item.getCells()[1].addStyleClass("EXhalfDay");
        item.addCell(new sap.m.Text({
            text: "Concomitanza"
        }));
        item.getCells()[2].addStyleClass("EXwCons");
        item.addCell(new sap.m.Text({
            text: "Non concomitanza"
        }));
        item.getCells()[3].addStyleClass("EXwNonCons");

        item.addCell(new sap.m.Text({
            text: "Fissata"
        }));
        item.getCells()[4].addStyleClass("EXwCust");
        item.addCell(new sap.m.Text({
            text: "Non schedulabile"
        }));
        item.getCells()[5].addStyleClass("EXwNonCust");
        item.addCell(new sap.m.Text({
            text: "Non confermata"
        }));
        item.getCells()[6].addStyleClass("EXnotConfirmed");
        item.addCell(new sap.m.Text({
            text: "Estero"
        }));
        item.getCells()[7].addStyleClass("EXabroad");
        this.legend.addItem(item);
        activityForm.addContent(this.legend);
    },

    onDownloadDialogPress: function (oEvent) {
        var oButton = oEvent.getSource();
        // create action sheet only once
        if (!this._fileExtensionActionSheet) {
            this._fileExtensionActionSheet = sap.ui.xmlfragment(
                "view.fragment.ChooseFileExtension",
                this
            );
            this.getView().addDependent(this._fileExtensionActionSheet);
        }
        this._fileExtensionActionSheet.openBy(oButton);
    },

    handleFileExtensionChoice: function (evt) {
        var oButton = evt.getSource();
        var text = oButton.getText();
        switch (text) {
            case ".csv":
                this.onDownloadDialogCSVPress();
                break;
            case ".html":
                this.createHTMLTable(".html");
                break;
            case ".xls":
                this.createHTMLTable(".xls");
                break;
        }
    },

    onDownloadDialogCSVPress: function () {
        //        var commessaScelta = this.getView().byId("idSelectCommessa").getSelectedItem().mProperties.text;
        var commessaScelta = this.chosedCom;
        this.oDialog.setBusy(true);
        var tableId = this.completeList;
        var oModel = this.completeModel;
        utils.exportCsv.exportToExcel(tableId, oModel, "Scheduling " + commessaScelta);
        this.downloadCommesseMultiple = false;
        this.excelDialog.close();
        this.oDialog.setBusy(false);
    },

    createHTMLTable: function (extension) {
        //        var commessaScelta = this.getView().byId("idSelectCommessa").getSelectedItem().mProperties.text;
        var commessaScelta = this.chosedCom;
        var nameFileExcel = commessaScelta;
        if (this.downloadCommesseMultiple) {
            var period = _.find(this.periodModel.getData().items, {
                idPeriod: this.period
            });
            var mese = "";
            switch (period.mesePeriodo) {
                case (1):
                    mese = "Gennaio";
                    break;
                case (2):
                    mese = "Febbraio";
                    break;
                case (3):
                    mese = "Marzo";
                    break;
                case (4):
                    mese = "Aprile";
                    break;
                case (5):
                    mese = "Maggio";
                    break;
                case (6):
                    mese = "Giugno";
                    break;
                case (7):
                    mese = "Luglio";
                    break;
                case (8):
                    mese = "Agosto";
                    break;
                case (9):
                    mese = "Settembre";
                    break;
                case (10):
                    mese = "Ottobre";
                    break;
                case (11):
                    mese = "Novembre";
                    break;
                case (12):
                    mese = "Dicembre";
                    break;
            }
            nameFileExcel = mese + period.annoPeriodo;
        }
        
        if (this.selezioneCommesse.length === 1) {
            var path = parseInt(this.selezioneCommesse[0].getPath().split("/")[2]);
            var commessa = this.selezioneCommesse[0].getModel().getData().commesse[path];
            var nomeCommessa = commessa.nomeCommessa;
            var cognomePjm = commessa.cognomeConsulente.replace(/\s/g, '');
            nameFileExcel = cognomePjm + "_" + nomeCommessa + "_" + nameFileExcel;
        } else {
            var cognomePjm = this.userModel.getData().cognomeConsulente.replace(/\s/g, '');
            nameFileExcel = cognomePjm + "_N_" + nameFileExcel;
        }
        
        var tab_text = "<table width='100%' border='2px'><tr bgcolor='#87AFC6'>";
        var textRange;
        var j = 0;
        var tab = document.getElementById('listaSchedulazioni-listUl'); // id of table

        for (j = 0; j < tab.rows.length; j++) {
            var tabHeader = tab.rows[j];
            var childrens;
            var children;

            childrens = tabHeader.childNodes;
            this.numeroColonne = childrens.length;
            if (tabHeader.id === "listaSchedulazioni-tblHeader") {
                for (var i = 0; i < childrens.length; i++) {
                    if (childrens[i].id === "listaSchedulazioni-tblHead0") {
                        childrens[i].style.width = "150px";
                        childrens[i].style.columnWidth = "150px";
                        childrens[i].attributes.style = "width: 150px: text-align: left";
                    } else {
                        childrens[i].style.width = "50px";
                        childrens[i].style.columnWidth = "50px";
                        childrens[i].attributes.style = "width: 50px: text-align: left";
                    }
                }
            } else {
                for (var i = 0; i < childrens.length - 1; i++) {
                    var xx = childrens[i].getElementsByTagName("span")[0];
                    if (xx.textContent !== "" && i !== 0) {
                        var classes = xx.getAttribute("class").split(" ");
                        var classeIteressata = classes[0];

                        var css = "";
                        switch (classeIteressata) {
                            case "EXassign":
                                css = "mediumpurple";
                                break;
                            case "EXhalfDay":
                                css = "orange";
                                break;
                            case "EXwCons":
                                css = "yellow";
                                break;
                            case "EXwNonCons":
                                css = "chartreuse";
                                break;
                            case "EXwCust":
                                css = "red";
                                break;
                            case "EXwNonCust":
                                css = "lightgrey";
                                break;
                            case "EXnotConfirmed":
                                css = "dodgerblue";
                                break;
                            case "EXabroad":
                                css = "lightskyblue";
                                break;
                        }
                        childrens[i].bgColor = css;
                    }
                }
            }
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
        }

        var tabLegenda = document.getElementById('legendaSchedulazioni-listUl');
        var tabHeader = tabLegenda.rows[1];
        var rigaInteressata = tabHeader.childNodes;
        for (var i = 0; i < rigaInteressata.length - 1; i++) {
            var xx = rigaInteressata[i].getElementsByTagName("span")[0];
            if (xx.textContent !== "") {
                var testo = xx.textContent;
                var css = "";
                switch (testo) {
                    case "Assegnata":
                        css = "mediumpurple";
                        break;
                    case "Mezza giornata":
                        css = "orange";
                        break;
                    case "Concomitanza":
                        css = "yellow";
                        break;
                    case "Non concomitanza":
                        css = "chartreuse";
                        break;
                    case "Fissata":
                        css = "red";
                        break;
                    case "Non schedulabile":
                        css = "lightgrey";
                        break;
                    case "Non confermata":
                        css = "dodgerblue";
                        break;
                    case "Estero":
                        css = "lightskyblue";
                        break;
                }
                rigaInteressata[i].bgColor = css;
            }
        }

        var st = tabLegenda.rows[1].innerHTML
        var str = "";
        var ff = "";
        for (var i = 0; i < st.split("</td>").length - 2; i++) {
            str = st.split("</td>")[i] + "</td>";
            ff = ff.concat(str)
        }

        var colspan = parseInt(this.numeroColonne / 8);
        tab_text = tab_text + "<tr><td colspan='" + this.numeroColonne + " border='0'></td></tr><tr><td><span>Legenda</span></td>" + ff.replace(/<td /g, "<td colspan='" + colspan + "' ") + "</tr>";

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
        {
            iframetxtArea1.document.open("txt/html", "replace");
            iframetxtArea1.document.write(tab_text);
            iframetxtArea1.document.close();
            iframetxtArea1.focus();
            sa = iframetxtArea1.document.execCommand("SaveAs", true, nameFileExcel + extension);
            return (sa);
            this.onCloseDialogPress();

        } else {
            var filename;
            //            var nameFileExcel = commessaScelta;
            switch (extension) {
                case ".html":
                    filename = nameFileExcel + extension;
                    this.saveAsHTML(tab_text, filename);
                    break;
                case ".xls":
                    filename = nameFileExcel;
                    this.saveAsXLS(tab_text, filename);
                    break;
            }
        }
    },

    saveAsXLS: function (uri, filename) {
        var link = document.createElement('a');
        if (typeof link.download === 'string') {
            var file = new Blob([uri], {
                type: "application/vnd.ms-excel"
            });
            document.body.appendChild(link); // Firefox requires the link to be in the body
            link.download = filename;
            link.href = URL.createObjectURL(file);
            link.click();
            setTimeout(function () {
                document.body.removeChild(link); /*remove the link when done*/
            }, 2000);
        } else {
            location.replace(uri);
        }
        this.onCloseDialogPress();
    },

    saveAsHTML: function (uri, filename) {
        var link = document.createElement('a');
        if (typeof link.download === 'string') {
            var file = new Blob([uri], {
                type: "application/vnd.ms-excel"
            });
            document.body.appendChild(link); // Firefox requires the link to be in the body
            link.download = filename;
            link.href = URL.createObjectURL(file);
            link.click();
            setTimeout(function () {
                document.body.removeChild(link); /*remove the link when done*/
            }, 2000);
        } else {
            location.replace(uri);
        }
        this.onCloseDialogPress();
    },

    onCloseDialogPress: function () {
        this.downloadCommesseMultiple = true;
        this.excelDialog.close();
    },

    /* INIZIO - funzione di caricamento schedulzioni da CSV */
    // funzione lanciata per caricare un file excel
    onUploadExcelPress: function () {
        sap.m.MessageBox.show(
            this._getLocaleText("TEXT_CONFIRM_UPLOAD"), {
                icon: sap.m.MessageBox.Icon.INFORMATION,
                title: this._getLocaleText("TITLE_CONFIRM_UPLOAD"),
                actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                onClose: _.bind(function (oAction) {
                    if (oAction === "YES") {
                        this.uploadDialog = sap.ui.xmlfragment("view.dialog.uploadExcel", this);
                        var page = this.getView().byId("schedulingPage");
                        page.addDependent(this.uploadDialog);
                        this.uploadDialog.open();
                    } else {
                        return;
                    }
                }, this),
            }
        );
    },

    handleTypeMissmatch: function (evt) {
        var aFileTypes = evt.getSource().getFileType();
        var typeSelect = evt.getParameter("fileType");
        sap.m.MessageToast.show("Il file di tipo '." + typeSelect + "' non è ammesso. Scegli un file di tipo: '" + aFileTypes + "'");
    },

    handleUploadPress: function (evt) {
        var fileUploader = sap.ui.getCore().getElementById("idFileUploader");
        if (!fileUploader.getValue()) {
            sap.m.MessageToast.show("Selezionare un file");
            return;
        }
        this.handleClosePress();
    },

    handleClosePress: function () {
        this.uploadDialog.close();
        this.uploadDialog.destroy(true);
    },

    handleValueChange: function (oEvent) {
        var fileUploader = sap.ui.getCore().getElementById("idFileUploader");
        if (!fileUploader.getValue()) {
            sap.m.MessageToast.show("Selezionare un file");
            return;
        }

        var loadHandler = function (event) {
            var csv = event.target.result;
            processData(csv);
        }

        var processData = function (csv) {
            var allTextLines = csv.split(/\r\n|\n/);
            var lineaIntestazione = allTextLines[0].split(';');
            this.lineaIntestazione = lineaIntestazione;

            if (lineaIntestazione[0] === "PROGETTO" && lineaIntestazione[1] === "COGNOME_PJM" && lineaIntestazione[2] === "NOME_PJM" && lineaIntestazione[3] === "COGNOME_CONSULENTE" && lineaIntestazione[4] === "NOME_CONSULENTE") {
                var dataInit = JSON.parse(sessionStorage.getItem("periodSelected")).dataInizio;
                var dataEnd = JSON.parse(sessionStorage.getItem("periodSelected")).dataFine;
                if (parseInt(dataInit.split("/")[0]) === parseInt(lineaIntestazione[5]) && parseInt(dataEnd.split("/")[0]) === parseInt(lineaIntestazione[lineaIntestazione.length - 1])) {
                    var lines = [];
                    for (var i = 1; i < allTextLines.length; i++) {
                        var data = allTextLines[i].split(';');
                        var tarr = [];
                        for (var j = 0; j < data.length; j++) {
                            if (data[j] === "") {
                                continue;
                            }
                            tarr.push(data[j]);
                        }
                        if (tarr.length > 0) {
                            lines.push(tarr);
                        }
                    }
                    this.insertSchedulingFromExcel(lines);
                } else {
                    // errore intestazione giorni
                    sap.m.MessageBox.information("Nella prima riga, le colonne relative ai giorni di schedulazioni non sono corrette", {
                        title: "ERRORE FILE",
                    });
                    this.console("intestazione giorni non corretto", "error");
                    this.handleClosePress();
                    return;
                }
            } else {
                // errore intestazione
                sap.m.MessageBox.information("La riga dell'intestazione non è corretta", {
                    title: "ERRORE FILE",
                });
                this.console("intestazione tabella errata", "error");
                this.handleClosePress();
                return;
            }
        }

        var errorHandler = function (evt) {
            this.console("IMPOSSIBILE LEGGERE IL FILE", "error");
        }

        loadHandler = _.bind(loadHandler, this);
        processData = _.bind(processData, this);
        errorHandlerd = _.bind(errorHandler, this);

        var file = oEvent.getParameter("files")[0];

        var reader = new FileReader();
        reader.readAsText(file);
        reader.onload = loadHandler;
        reader.onerror = errorHandler;
    },

    insertSchedulingFromExcel: function (records) {
        var schedulazioni = this.pmSchedulingModel.getData().schedulazioni;
        if (schedulazioni.length < records.length) {
            // staff nn corretto
            this.console("staff non corretto", "error");
            this.handleClosePress();
            return;
        }
        var dataToSend = {
            "codConsulente": "",
            "dataReqPeriodo": "",
            "idCommessa": this.getView().byId("idSelectCommessa").getSelectedKey(),
            "codAttivitaRichiesta": "",
            "idPeriodoSchedulingRic": parseInt(JSON.parse(sessionStorage.getItem("period"))),
            "codPjm": this.user.codice_consulente,
            "dataInserimento": (new Date()).toISOString().substring(0, (new Date()).toISOString().indexOf("T")),
            "note": "",
            "statoReq": "CONFIRMED",
            "giorno": 0,
            "id_req": ""
        };
        var inserimenti = [];
        for (var i = 0; i < records.length; i++) {
            //            if (records[i][0] !== this.getView().byId("idSelectCommessa").getValue()) {
            if (records[i][0] !== this.chosedCom) {
                // errore commessa
                this.console("commessa selezionata diversa da commessa in caricamento", "error");
                this.handleClosePress();
                return;
            } else {
                var cognome = records[i][3];
                if (cognome.split(" ").length === 1) {
                    var cognomeTrasf = cognome.substr(0, 1).toUpperCase() + cognome.substr(1, cognome.length).toLowerCase();
                } else if (cognome.split(" ").length === 2) {
                    var cognomeTrasf = "";
                    cognomeTrasf = (cognome.split(" ")[0]).substr(0, 1).toUpperCase() + (cognome.split(" ")[0]).substr(1, (cognome.split(" ")[0]).length).toLowerCase();
                    cognomeTrasf = cognomeTrasf.concat(" ");
                    cognomeTrasf = cognomeTrasf.concat((cognome.split(" ")[1]).substr(0, 1).toUpperCase() + (cognome.split(" ")[1]).substr(1, (cognome.split(" ")[1]).length).toLowerCase());
                }
                var nome = records[i][4];
                if (nome.split(" ").length === 1) {
                    var nomeTrasf = nome.substr(0, 1).toUpperCase() + nome.substr(1, nome.length).toLowerCase();
                } else {
                    var nomeTrasf = "";
                    nomeTrasf = (nome.split(" ")[0]).substr(0, 1).toUpperCase() + (nome.split(" ")[0]).substr(1, (nome.split(" ")[0]).length).toLowerCase();
                    nomeTrasf = nomeTrasf.concat(" ");
                    nomeTrasf = nomeTrasf.concat((nome.split(" ")[1]).substr(0, 1).toUpperCase() + (nome.split(" ")[1]).substr(1, (nome.split(" ")[1]).length).toLowerCase());
                }
                var nomeConsulente = cognomeTrasf + " " + nomeTrasf;
                var trovatoConsulente = _.find(this.pmSchedulingModel.getData().schedulazioni, {
                    nomeConsulente: nomeConsulente
                });
                if (trovatoConsulente) {
                    var rich = "";
                    for (var t = 5; t < (records[i].length - 1); t++) {
                        var iteratore = _.clone(dataToSend);
                        var numRich = records[i][t].split("#")[0];
                        switch (numRich) {
                            case "1":
                                // Assegnata
                                rich = "complete";
                                break;
                            case "2":
                                // Assegnata mezza giornata
                                rich = "time-entry-request";
                                break;
                            case "3":
                                // Concomitanza consulente
                                rich = "customer-and-contacts";
                                break;
                            case "4":
                                // Fissata
                                rich = "collaborate";
                                break;
                            case "5":
                                // Estero
                                rich = "globe";
                                break;
                            case "6":
                                // Non concomitanza consulente   
                                rich = "employee-rejections";
                                break;
                            case "7":
                                // Non chedulabile
                                rich = "offsite-work";
                                break;
                            case "8":
                                // Non confermato cliente
                                rich = "role";
                                break;
                            case "0":
                                rich = "";
                                break;
                            default:
                                rich = "";
                        }
                        if (rich !== "") {
                            iteratore.codConsulente = trovatoConsulente.codiceConsulente;
                            iteratore.codAttivitaRichiesta = rich;
                            var giornoTestata = this.lineaIntestazione[t];
                            iteratore.giorno = t - 4;
                            if (records[i][t].split("#")[1]) {
                                iteratore.note = records[i][t].split("#")[1];
                            }
                            var dataInit = JSON.parse(sessionStorage.getItem("periodSelected")).dataInizio;
                            var periodoMese = dataInit.split("/")[1];
                            var periodoAnno = dataInit.split("/")[2];

                            if (parseInt(giornoTestata) < 10 && t > 20) {
                                if (parseInt(periodoMese) !== 12) {
                                    periodoMese = parseInt(periodoMese) + 1;
                                } else {
                                    periodoMese = "1";
                                    periodoAnno = parseInt(periodoAnno) + 1;
                                }
                            }
                            var dataReq = periodoAnno + "/" + periodoMese + "/" + giornoTestata;
                            iteratore.dataReqPeriodo = dataReq;
                            var ggProp = "gg" + iteratore.giorno;
                            var ggIconProp = "ggIcon" + iteratore.giorno;
                            if (trovatoConsulente[ggProp] === this.nomeCommessaFerie && (trovatoConsulente[ggIconProp] === "sap-icon://travel-itinerary" || (trovatoConsulente[ggIconProp] === "sap-icon://bed" && iteratore.codAttivitaRichiesta !== "time-entry-request"))) {
                                continue;
                            }
                            inserimenti.push(iteratore);
                        } else {
                            // 
                        }
                    }
                } else {
                    // errore nome consulente
                    this.console("errore nome consulente", "error");
                    return;
                }
            }
        }
        var arrayRemove = [];
        for (var i = 0; i < schedulazioni.length; i++) {
            for (var w = 1; w < 36; w++) {
                var reqProp = "req" + w;
                var ggProp = "gg" + w;
                var items = ""; //serie di "a" aggiunte alla proprietà
                while (schedulazioni[i][reqProp]) {
                    //                    if (schedulazioni[i][reqProp] && schedulazioni[i][ggProp] !== this.nomeCommessaFerie && schedulazioni[i][ggProp] === this.getView().byId("idSelectCommessa").getValue()) {
                    if (schedulazioni[i][reqProp] && schedulazioni[i][ggProp] !== this.nomeCommessaFerie && schedulazioni[i][ggProp] === this.chosedCom) {
                        arrayRemove.push(schedulazioni[i][reqProp]);
                    }
                    items = "a";
                    reqProp = reqProp + items;
                    ggProp = ggProp + items;
                }
            }
        }

        var fSuccess = function () {
            this.saveMultipleRequest(inserimenti, "excel");
        };

        var fError = function (err) {
            sap.m.MessageToast.show(err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        if (arrayRemove.length > 0) {
            model.reqToDel.delMulti(arrayRemove)
                .then(fSuccess, fError);
        } else {
            this.saveMultipleRequest(inserimenti, "excel");
        }
        this.handleClosePress();
    },
    /* FINE - funzione di caricamento schedulzioni da CSV */

    // funzione per modificare lo switch che cambia il servizio da chiamare per popolare la tabella
    onSwitchChange: function () {
        this.leggiTabella(this.ric);
    },

    readAllEditedCommesse: function (req) {
        this.oDialogTable.setBusy(true);
        var fSuccess = function (results) {
            var schedulazioni = this._uniqueElement(results);
            var ggTot = 0;
            for (var q = 0; q < schedulazioni.length; q++) {
                var ob = schedulazioni[q];
                ob.giorniScheduling = 0;
                for (var w = 1; w < 36; w++) {
                    var prp = "gg" + w;
                    if (ob[prp] && ob[prp] === this.chosedCom) {
                        ob.giorniScheduling++;
                    }
                }
                ggTot = ggTot + ob.giorniScheduling;
            }
            this.pmSchedulingModel.setProperty("/giorniTotali", ggTot);

            for (var t = 0; t < schedulazioni.length; t++) {
                var commessaConsulente = _.find(_.find(this.commessePerConsulente, {
                    cod: schedulazioni[t].codiceConsulente
                }).commesse, {
                    nomeCommessa: this.chosedCom
                });
                schedulazioni[t].teamLeader = commessaConsulente.teamLeader;
                schedulazioni[t].nomeSkill = commessaConsulente.nomeSkill;
            }

            var schedOrdinato = _.sortBy(schedulazioni, 'nomeConsulente');
            this.pmSchedulingModel.setProperty("/schedulazioni", schedOrdinato);
            this.allScheduling = this.pmSchedulingModel.getProperty("/schedulazioni");
            if (schedOrdinato.length > 0 && schedOrdinato.length <= 7) {
                this.pmSchedulingModel.setProperty("/nRighe", schedOrdinato.length);
            } else if (schedOrdinato.length > 7) {
                this.pmSchedulingModel.setProperty("/nRighe", 7);
            } else {
                this.pmSchedulingModel.setProperty("/nRighe", 1);
            }
            setTimeout(_.bind(this._onAfterRenderingTable, this));
        };

        var fError = function (err) {
            console.log(err);
            this.oDialogTable.setBusy(false);
            this.visibleModel.setProperty("/closeProjectButton", false);
            this._enterErrorFunction("error", "Errore", "Errore lettura tabella: " + err);
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        var fSuccessStaff = function (res) {
            model.tabellaSchedulingModify.readTable(req, res, this.chosedCom, this.allCommesse)
                .then(fSuccess, fError);
        };

        var fErrorStaff = function (err) {
            this.oDialogTable.setBusy(false);
            this._enterErrorFunction("warning", "Attenzione - errore oData", "Errore lettura oData 'anagStaffViewSet'");
        };

        fSuccessStaff = _.bind(fSuccessStaff, this);
        fErrorStaff = _.bind(fErrorStaff, this);

        model.tabellaSchedulingModify.readStaffPerCommessaPJM(req)
            .then(fSuccessStaff, fErrorStaff);
    },

    // funzione che mi construisce un array ordinato
    // legge gli skill dei consulenti estratti dalla lettura
    _uniqueElement: function (result) {
        var arrayfinale = result[0];
        var arrayGiornateSchedulate = result[0];

        var objCodiciConsulenti = {};
        for (var i = 0; i < arrayGiornateSchedulate.length; i++) {
            var c = "c" + i;
            objCodiciConsulenti[c] = arrayGiornateSchedulate[i].codiceConsulente;
        }
        var objCodiciConsulenti2 = {};
        for (var i = 0; i < arrayGiornateSchedulate.length; i++) {
            var c = "d" + i;
            objCodiciConsulenti2[c] = arrayGiornateSchedulate[i].codiceConsulente;
        }
        _.merge(objCodiciConsulenti, objCodiciConsulenti2);

        arrayfinale = [];
        for (var prop in objCodiciConsulenti) {
            var arr = _.where(arrayGiornateSchedulate, {
                codiceConsulente: objCodiciConsulenti[prop]
            });
            var obj = {};
            for (var i = 0; i < arr.length; i++) {
                obj.nomeConsulente = arr[i].nomeConsulente;
                obj.codiceConsulente = arr[i].codiceConsulente;
                obj.codicePJM = arr[i].codicePJM;
                var objProps = _.keys(arr[i]);
                var ggProp = _.find(objProps, function (item) {
                    return (item.indexOf("gg") > -1 && item.indexOf("Icon") < 0 && item.indexOf("Conflict") < 0 && item.indexOf("Req") < 0 && item.indexOf("Ins") < 0);
                });
                if (ggProp) {
                    var gg = ggProp.substring(2); //id del giorno nel periodo
                    // ggVal è il nome della proprietà "gg"+ la serie di "a" dipendente dalla quantità di attività in quello stesso giorno
                    var ggVal = ggProp;
                    var items = ""; //serie di "a" aggiunte alla proprietà
                    while (obj[ggVal]) {
                        items = "a";
                        ggVal = ggVal + items;
                    }
                    obj[ggVal] = arr[i][ggProp];
                    obj["nota" + ggVal.substring(2)] = arr[i]["nota" + gg];
                    obj["req" + ggVal.substring(2)] = arr[i]["req" + gg];
                    obj["ggReq" + ggVal.substring(2)] = arr[i]["ggReq" + gg];
                    obj["ggIcon" + ggVal.substring(2)] = arr[i]["ggIcon" + gg];
                    obj["ggConflict" + ggVal.substring(2)] = arr[i]["ggConflict" + gg];
                    obj["ggIns" + ggVal.substring(2)] = arr[i]["ggIns" + gg];
                }
            }
            if (!_.isEmpty(obj))
                arrayfinale.push(obj);
        }
        arrayfinale = _.reduce(arrayfinale, function (results, item) {
            return _.any(results, function (result) {
                return _.isEqual(result, item);
            }) ? results : results.concat([item]);
        }, []);

        var arrCodConsulenti = [];
        for (var aa = 0; aa < arrayfinale.length; aa++) {
            for (var prop in arrayfinale[aa]) {
                if (prop.indexOf("codiceConsulente") > -1) {
                    arrCodConsulenti.push((arrayfinale[aa])[prop]);
                }
            }
        }
        return arrayfinale;
    },

    clearPasteCommesse: function () {
        if (model.persistence.Storage.local.get("copy")) {
            model.persistence.Storage.local.remove("copy");
        }
        if (model.persistence.Storage.local.get("copyBis")) {
            model.persistence.Storage.local.remove("copyBis");
        }
        this.paste = false;
    }
});
