jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.WBS");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("sap.m.MessageToast");

view.abstract.AbstractMasterController.extend("view.SettingsWBSList", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.modelWBS = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelWBS, "modelWBS");
        
        this.statoModel = new sap.ui.model.json.JSONModel();

        this.uiModel.setProperty("/searchProperty", ["descCommessaHana"]);
    },

    handleRouteMatched: function (evt) {
        var name = evt.getParameter("name");
        this.name = name;

        if (name !== "settingsWBSList") {
            return;
        }
        view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
        this.getView().byId("list").removeSelections();
        
        this.oDialogList = this.getView().byId("list");

        if (model.persistence.Storage.session.get("selectedWBS")) {
            model.persistence.Storage.session.remove("selectedWBS");
        }

        var listWBS = this.getView().byId("list");
        var binding = listWBS.getBinding("items");
        binding.filter();

        this.readWBS();
    },

    readWBS: function (stato) {
        var fSuccess = function (result) {
            var res = _.sortByOrder(result[0].items, ['descCommessaHana']);
            this.modelWBS.setData(res);
            this.console("WBS", "success");
            this.console(result);
            this.oDialogList.setBusy(false);
        };
        var fError = function (err) {
            if (!this.oneTime) {
                this.oneTime = true;
                model.odata.chiamateOdata.chiamataForLoginToHana(fSuccess, fError)
                    .then(_.bind(this.readWBS(stato), this));
            }
            this.oDialogList.setBusy(false);
//            this.console(JSON.parse(err.response.body).error.message.value, "error");
            this.console(err.response.statusText, "error");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialogList.setBusy(true);
        var cid = sessionStorage.getItem("cid");
        model.WBS.readWbsSapHana(cid, stato)
            .then(fSuccess, fError);
    },

    onLaunchpadPress: function () {
        this.router.navTo("launchpad");
        if (model.persistence.Storage.session.get("selectedWBS")) {
            model.persistence.Storage.session.remove("selectedWBS");
        }
    },

    addWBSPress: function () {
        this.router.navTo("settingsWBSModify", {
            state: "new"
        });
        if (sessionStorage.getItem("selectedWBS")) {
            sessionStorage.removeItem("selectedWBS");
        }
    },

    onWBSSelect: function (evt) {
        var src = evt.getSource();
        if (src.getSelectedItems()[0]) {
            var selectedItem = src.getSelectedItems()[0].getBindingContext("modelWBS").getObject();
            sessionStorage.setItem("selectedWBS", JSON.stringify(selectedItem));
            this.router.navTo("settingsWBSDetail", {
                codCommessaHana: selectedItem.codCommessaHana,
                codCommessaSap: selectedItem.codCommessaSap,
                codAttivitaSap: selectedItem.codAttivitaSap
            });
        }
    },
    
    onFilterPress: function () {
        var stato = [{
            "value": "ATTIVO"
        }, {
            "value": "CANCELLATO"
        }];
        this.statoModel.setData(stato);
        this.getView().setModel(this.statoModel, "filter");

        var page = this.getView().byId("listaWBS");
        this.filterDialog = sap.ui.xmlfragment("view.dialog.filterDialogWBS", this);
        page.addDependent(this.filterDialog);
        
        this.filterDialog.open();
    },
    
    onFilterDialogClose: function() {
        this.filterDialog.close();
        this.filterDialog.destroy(true);
    },

    onFilterDialogOK: function() {
        var value = sap.ui.getCore().getElementById("listaFiltroStato").getSelectedItems()[0].getTitle();
        this.readWBS(value);
        this.filterDialog.close();
        this.filterDialog.destroy(true);
    },

});