jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.UserLogin");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("sap.m.MessageToast");

view.abstract.AbstractMasterController.extend("view.UserLoginList", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.modelConsulentiLogin = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.modelConsulentiLogin, "modelConsulentiLogin");

        this.adminType = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.adminType, "adminType");

        this.uiModel.setProperty("/searchProperty", ["cognomeConsulente"]);
    },

    handleRouteMatched: function (evt) {


        var name = evt.getParameter("name");
        this.name = name;

        if (name !== "userLoginList") {
            return;
        }
        if (sessionStorage.getItem("listaConsulentiLogin")) {
            sessionStorage.removeItem("listaConsulentiLogin");
        }
        view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
        this.getView().byId("list").removeSelections();
        this.oDialog = this.getView().byId("list");

        if (model.persistence.Storage.session.get("selectedConsultantLogin")) {
            model.persistence.Storage.session.remove("selectedConsultantLogin");
        }
        this.userAlias = model.persistence.Storage.session.get("GSPI_User_Logged").usernameConsulente;

        var listConsultants = this.getView().byId("list");
        var binding = listConsultants.getBinding("items");
        binding.filter();

        this.consultantType = model.persistence.Storage.session.get("user").descrizioneProfiloConsulente;
        if (this.consultantType !== "SuperUser") {
            this.adminType.setProperty("/admin", false);
        } else {
            this.adminType.setProperty("/admin", true);
        }

        this.readConsultantsLogin();
    },

    readConsultantsLogin: function () {
        var fSuccess = function (result) {
            this.modelConsulentiLogin.setProperty("/userLogin", result.items);
            this.console("usersLogin", "success");
            this.console(result);
            if (this.oneTime) {
                this.oneTime = undefined;
            }
            this.oDialog.setBusy(false);
        };
        var fError = function (err) {
            this.oDialog.setBusy(false);
            this.console(JSON.parse(err.response.body).error.message.value, "error");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        if (this.adminType.getProperty("/admin")) {
            model.UserLogin.read()
                .then(fSuccess, fError);
        } else {
            model.UserLogin.read(this.userAlias)
                .then(fSuccess, fError);
        }
    },

    onLaunchpadPress: function () {
        this.router.navTo("launchpad");
        if (model.persistence.Storage.session.get("selectedConsultantLogin")) {
            model.persistence.Storage.session.remove("selectedConsultantLogin");
        }
    },

    onFilterPress: function () {
        if (this.name === "consultantDetail" || this.name === "consultantModify") {
            this.router.navTo("consultantList");
        }
        if (sap.ui.getCore().getElementById("idSegmentedButton")) {
            sap.ui.getCore().getElementById("idSegmentedButton").setSelectedButton("statusButton");
        }
        this.modelFragmentConsultant.setProperty("/status", true);
        this.modelFragmentConsultant.setProperty("/bu", false);

        var stato = [{
            "value": "Aperto"
        }, {
            "value": "Chiuso"
        }];
        this.tempModelData.setData(stato);
        this.getView().setModel(this.tempModelData, "filter");

        var page = this.getView().byId("listaConsulenti");
        if (!this.consultantDialog) {
            this.consultantDialog = sap.ui.xmlfragment("view.dialog.filterDialogConsultants", this);
        }
        page.addDependent(this.consultantDialog);

        var fSuccess = function (result) {
            this.buModel.setData(result);
            this.consultantDialog.open();
        };
        var fError = function (err) {
            sap.m.MessageToast("error BU");
        };
        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);
        model.BusinessUnit.readBu()
            .then(fSuccess, fError);
    },

    getSelectButton: function (evt) {
        var buttonPressed = evt.getSource().getSelectedButton();
        if (buttonPressed === "statusButton") {
            this.modelFragmentConsultant.setProperty("/status", true);
            this.modelFragmentConsultant.setProperty("/bu", false);
        } else if (buttonPressed === "businessUnitButton") {
            this.modelFragmentConsultant.setProperty("/status", false);
            this.modelFragmentConsultant.setProperty("/bu", true);
        }
    },

    onFilterDialogClose: function () {
        this.consultantDialog.close();
    },

    onFilterDialogOK: function (evt) {
        this.consultantDialog.close();

        var filtersArr = [];
        var filtroBu = sap.ui.getCore().getElementById("idListBuConsultants").getSelectedItems();

        for (var i = 0; i < filtroBu.length; i++) {
            filtersArr.push(new sap.ui.model.Filter('descBu', sap.ui.model.FilterOperator.Contains, filtroBu[i].getProperty("title")));
        }
        var filtroOK = new sap.ui.model.Filter({
            filters: filtersArr,
            and: false
        });
        if (this.arrayFiltri) {
            var filtroStato = new sap.ui.model.Filter('stato', sap.ui.model.FilterOperator.Contains, this.arrayFiltri[0]);
            filtersArr.push(filtroStato)
            var filtroOK = new sap.ui.model.Filter({
                filters: filtersArr,
                and: true
            });
        }

        var list = this.getView().byId("list");
        var binding = list.getBinding("items");
        binding.filter(filtroOK);
        this.console("filtri selezionati", "confirm");
        this.console(filtersArr);
    },

    onFilterDialogRipristino: function () {
        this.consultantDialog.close();
        sap.ui.getCore().getElementById("listaFiltroStato").removeSelections();
        this.arrayFiltri = undefined;
        sap.ui.getCore().getElementById("idListBuConsultants").removeSelections();
        var listConsultants = this.getView().byId("list");
        var binding = listConsultants.getBinding("items");
        binding.filter();
    },

    filterPress: function (evt) {
        var numeroFiltri = evt.getSource().getSelectedItems();
        var arrayFiltri = [];
        for (var i = 0; i < numeroFiltri.length; i++) {
            arrayFiltri.push((numeroFiltri[i].getProperty("title").toUpperCase()));
        }
        this.arrayFiltri = arrayFiltri;
        //        this.console(arrayFiltri);
    },

    addConsultantPress: function () {
        this.router.navTo("userLoginModify", {
            state: "new"
        });
        if (sessionStorage.getItem("selectedConsultantLogin")) {
            sessionStorage.removeItem("selectedConsultantLogin");
        }
        sessionStorage.setItem("listaConsulentiLogin", JSON.stringify(this.modelConsulentiLogin.getProperty("/userLogin")));
    },

    onConsultantSelectBis: function (evt) {
        var src = evt.getSource();
        var selectedItem = src.getSelectedItems()[0].getBindingContext("modelConsulentiLogin").getObject();
        sessionStorage.setItem("selectedConsultantLogin", JSON.stringify(selectedItem));
        this.router.navTo("userLoginDetail", {
            alias: selectedItem.alias
        });
    },

});