jQuery.sap.require("view.abstract.AbstractController");

view.abstract.AbstractController.extend("view.Empty", {

    onInit: function () {
        this.router = sap.ui.core.UIComponent.getRouterFor(this);
        this.router.attachRoutePatternMatched(this.handleRouteMatched, this);

    },

    handleRouteMatched: function (evt) {
        var name = evt.getParameter("name");

        if (name !== "empty") {
            return;
        }
    },
//    onLogoutPress: function(){
//        this.router.navTo("login");
//    }
});
