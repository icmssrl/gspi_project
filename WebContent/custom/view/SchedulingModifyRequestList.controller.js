jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.SchedulingModifyRequests");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("sap.m.MessageToast");

view.abstract.AbstractController.extend("view.SchedulingModifyRequestList", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

        this.reqModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.reqModel, "reqModel");

        this.tempModelData = new sap.ui.model.json.JSONModel();

        this.uiModel.setProperty("/searchProperty", ["nomeCommessaModifica", "cognomeConsulenteModifica"]);
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);

        var name = evt.getParameter("name");
        this.name = name;
        if (name !== "modifyRequestsList") {
            return;
        };
        this.getView().byId("list").removeSelections();

        if (model.persistence.Storage.session.get("selectedModifyRequest")) {
            model.persistence.Storage.session.remove("selectedModifyRequest");
        }
        if (model.persistence.Storage.session.get("modifyRequestDetailScambio")) {
            model.persistence.Storage.session.remove("modifyRequestDetailScambio");
        }

        this.oDialog = this.getView().byId("list");
        this.user = model.persistence.Storage.session.get("GSPI_User_Logged");
        this.readModifyRequests("PROPOSED");
    },

    readModifyRequests: function (stato) {
        var fSuccess = function (result) {
            var ordinaPerDataRichiesta = _.sortBy(result.items, function (o) {
                return utils.Formatter.dateToHana(o.dataRichiestaModifica);
            });
            this.reqModel.setProperty("/items", ordinaPerDataRichiesta);
            this.oDialog.setBusy(false);
        };

        var fError = function () {
            this.oDialog.setBusy(false);
            console.log("errore in lettura della lista richieste di modifiche scheduling");
        };

        fSuccess = _.bind(fSuccess, this);
        fError = _.bind(fError, this);

        this.oDialog.setBusy(true);
        model.SchedulingModifyRequests.readModifyRequests(this.user.codice_consulente, stato)
            .then(fSuccess, fError);
    },

    onLaunchpadPress: function () {
        this.router.navTo("launchpad");
        if (model.persistence.Storage.session.get("selectedModifyRequest")) {
            model.persistence.Storage.session.remove("selectedModifyRequest");
        };
    },

    onFilterPress: function () {
        if (this.name === "modifyRequestDetail" || this.name === "modifyRequestModify") {
            this.router.navTo("modifyRequestsList");
        }

        var stato = [{
            "value": "PROPOSED"
        }, {
            "value": "CONFIRMED"
        }, {
            "value": "DELETED"
        }, {
            "value": "ALL"
        }];
        this.tempModelData.setData(stato);

        this.getView().setModel(this.tempModelData, "filter");

        var page = this.getView().byId("schedulingModifyRequestListPage");
        //        if (!this.filterApproveRequestDialog) {
        this.filterApproveRequestDialog = sap.ui.xmlfragment("view.dialog.filterApproveRequestDialog", this);
        //        }
        
        jQuery.sap.delayedCall(100, this, function () {
            page.addDependent(this.filterApproveRequestDialog);
            this.filterApproveRequestDialog.open();
        });
//        page.addDependent(this.filterApproveRequestDialog);
//        this.filterApproveRequestDialog.open();
    },

    onFilterDialogClose: function () {
        this.filterApproveRequestDialog.close();
        this.filterApproveRequestDialog.destroy(true);
    },

    onFilterDialogOK: function () {
        var filtroStato = sap.ui.getCore().getElementById("listaFiltroStato").getSelectedItems();
        filtroStato = filtroStato[0].getProperty("title");
        switch (filtroStato) {
            case "Proposta":
                filtroStato = "PROPOSED";
                break;
            case "Confermata":
                filtroStato = "CONFIRMED";
                break;
            case "Respinta":
                filtroStato = "DELETED";
                break;
            case "Tutti":
                filtroStato = "ALL";
                break;
            }
       
        this.filterApproveRequestDialog.close();
        this.filterApproveRequestDialog.destroy(true);
        this.readModifyRequests(filtroStato);
    },

    addNewModifySchedulingRequest: function () {
        this.router.navTo("modifyRequestEdit", {
            state: "new"
        });
        if (sessionStorage.getItem("selectedModifyRequest")) {
            sessionStorage.removeItem("selectedModifyRequest");
        }
        if (model.persistence.Storage.session.get("modifyRequestDetailScambio")) {
            model.persistence.Storage.session.remove("modifyRequestDetailScambio");
        }
    },

    onSingleItemPress: function (evt) {
        var src = evt.getSource();
        var selectedItem = src.getSelectedItems()[0].getBindingContext("reqModel").getObject();
        sessionStorage.setItem("selectedModifyRequest", JSON.stringify(selectedItem));
        if (selectedItem.idScambio !== "") {
            var consulenteScambio = _.find(this.reqModel.getData().items, {
                'idReqModifica': selectedItem.idScambio
            });
            sessionStorage.setItem("modifyRequestDetailScambio", JSON.stringify(consulenteScambio));
        } else {
            if (model.persistence.Storage.session.get("modifyRequestDetailScambio")) {
                model.persistence.Storage.session.remove("modifyRequestDetailScambio");
            }
        }
        this.router.navTo("modifyRequestDetail", {
            detailId: selectedItem.idReqModifica
        });
    },
    
    goToPjmSched: function () {
        this.router.navTo("pmSchedulingDraft");
    },
});