jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.TravelReq");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("utils.Formatter");

view.abstract.AbstractMasterController.extend("view.TravelRequestDetail", {

  onInit: function () {
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

    this.travelReqDetailModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.travelReqDetailModel, "travelReqDetailModel");

    //this.uiModel.setProperty("/searchProperty", ["nomeCognome"]);
  },

  handleRouteMatched: function (evt) {

    var name = evt.getParameter("name");

    if (name !== "travelRequestDetail") {
      return;
    }
    view.abstract.AbstractMasterController.prototype.handleRouteMatched.apply(this, arguments);
      
    var travelReq = JSON.parse(sessionStorage.getItem("selectedTravelReq"));
    this.travelReqDetailModel.setProperty("/nomeCognome", travelReq.nomeCognome);
    this.travelReqDetailModel.setProperty("/societa", travelReq.societa);
    this.travelReqDetailModel.setProperty("/cliente", travelReq.cliente);
    this.travelReqDetailModel.setProperty("/commessa", travelReq.commessa);
    this.travelReqDetailModel.setProperty("/attivita", travelReq.attivita);
    this.travelReqDetailModel.setProperty("/elementoWbs", travelReq.elementoWbs);
      if(travelReq.rifatturabile === "1"){
          this.getView().byId("idCheckRifatturabile").setSelected(true);
      }else{
          this.getView().byId("idCheckRifatturabile").setSelected(false);
      }
    this.travelReqDetailModel.setProperty("/tipoSpesaCod", travelReq.tipoSpesaCod);
    this.travelReqDetailModel.setProperty("/tipoSpesaDesc", travelReq.tipoSpesaDesc);

    if(travelReq.tipoSpesaCod === "HTLF"){
        this.travelReqDetailModel.setProperty("/visibleHotel", true);
        this.travelReqDetailModel.setProperty("/visibleTrasporto", false);
    }else if(travelReq.tipoSpesaCod === "AIRF" || travelReq.tipoSpesaCod === "CARF" || travelReq.tipoSpesaCod === "TRNF"){
        this.travelReqDetailModel.setProperty("/visibleTrasporto", true);
        this.travelReqDetailModel.setProperty("/visibleHotel", false);
    }else{
        this.travelReqDetailModel.setProperty("/visibleTrasporto", false);
        this.travelReqDetailModel.setProperty("/visibleHotel", false);
    }
      
    this.travelReqDetailModel.setProperty("/dataCheckIn", utils.Formatter.invertDate(travelReq.dataCheckIn));
    this.travelReqDetailModel.setProperty("/dataCheckOut", utils.Formatter.invertDate(travelReq.dataCheckOut));
    this.travelReqDetailModel.setProperty("/noNotti", travelReq.noNotti);
    this.travelReqDetailModel.setProperty("/luogo", travelReq.luogo);

    this.travelReqDetailModel.setProperty("/dettagliTrasporto", travelReq.dettagliTrasporto);
    this.travelReqDetailModel.setProperty("/richiesteSpeciali", travelReq.richiesteSpeciali);
      if(travelReq.confermato === "0"){
          this.travelReqDetailModel.setProperty("/confermato", false);
      }else{
          this.travelReqDetailModel.setProperty("/confermato", true);
      }
    this.travelReqDetailModel.setProperty("/noteConferma", travelReq.noteConferma);
//      var dataInserimento = travelReq.dataInserimento.substring(8) + "/" + travelReq.dataInserimento.substring(5,7) + "/" + travelReq.dataInserimento.substring(0,4);
//    this.travelReqDetailModel.setProperty("/dataInserimento", dataInserimento);
      this.travelReqDetailModel.setProperty("/dataInserimento", utils.Formatter.invertDate(travelReq.dataInserimento));
    
      
    
    

  },
    
    modifyReqPress: function(){
        var travelReq = JSON.parse(sessionStorage.getItem("selectedTravelReq"));
        this.router.navTo("travelRequestModify", {
            detailId: travelReq.idRichiesta
        });
    },
    


});