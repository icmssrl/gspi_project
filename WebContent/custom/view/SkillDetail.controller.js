jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.Consultants");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageBox");

view.abstract.AbstractController.extend("view.SkillDetail", {

    onInit: function () {
        view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
        this.detailSkillModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.detailSkillModel, "detailSkillModel");
    },

    handleRouteMatched: function (evt) {
        view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
        var name = evt.getParameter("name");

        if (name !== "skillDetail") {
            return;
        };
        this.oDialog = this.getView().byId("idSkillDetail");

        var skill = JSON.parse(sessionStorage.getItem("selectedSkill"));
        this.detailSkillModel.setProperty("/codSkill", skill.codSkill);
        this.detailSkillModel.setProperty("/nomeSkill", skill.nomeSkill);
        this.detailSkillModel.setProperty("/descrizioneSkill", skill.descSkill);
    },

    modifySkillPress: function () {
        this.router.navTo("skillModify", {
            state: "modify",
            detailId: this.detailSkillModel.getProperty("/codSkill")
        });
    },

    deleteSkillPress: function () {
        sap.m.MessageBox.show(this._getLocaleText("CONFIRM_DELETE_SKILL"),
            sap.m.MessageBox.Icon.ALLERT,
            this._getLocaleText("CONFIRM_DELETE_TITLE_SKILL"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
            _.bind(this._confirmDeletePress, this)
        );
    },

    _confirmDeletePress: function (evt) {
        if (evt === sap.m.MessageBox.Action.NO) {
            return;
        } else {
            var fSuccess = function () {
                this.oDialog.setBusy(false);
                this.router.navTo("skillList");
            };
            var fError = function () {
                this.oDialog.setBusy(false);
                console.log("errore nel salvataggio");
            };
            fSuccess = _.bind(fSuccess, this);
            fError = _.bind(fError, this);

            this.oDialog.setBusy(true);
            model.Skill.deleteSkill(this.detailSkillModel.getProperty("/codSkill"))
                .then(fSuccess, fError);
        };
    }
});
