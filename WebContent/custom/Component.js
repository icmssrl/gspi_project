jQuery.sap.declare("icms.Component");
jQuery.sap.require("icms.MyRouter");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("sap.m.MessageBox");

sap.ui.core.UIComponent.extend("icms.Component", {
    metadata: {
        config: {
            resourceBundle: "custom/i18n/text.properties",

            //puntati dalla classe settings.core [custom/settings]
            settings: {
                lang: "IT",
                isMock: false,
                debug: true,

                serverSAP: "http://wardev.icms.it/sap/opu/odata/SAP/ZFTR_SRV/",
                //                client: "sap-client=200",
                serverUrlMenuTendina: "http://wardev.icms.it/sap/opu/odata/SAP/ZFIORI_SRV/",
                logoffUrl: "http://wardev.icms.it/sap/public/bc/icf/logoff",

                //SVILUPPO
                client: "sap-client=100",
                //                prefTitle: "GSPI - SVIL",
                //                serverUrl: "http://gspidev.icms.it:8000/GSPI/odata/odata.xsodata/",
                //                serverUrlServices: "http://gspidev.icms.it:8000/GSPI/services/",
                //                serverUrlServicesNew: "http://wardev.icms.it/GSPI/servicesNew/",
                //                tokUsr: "SYSTEM",
                //                tokPsw: "Saphdb_40$",

                // ambiente TEST
                //                client: "sap-client=200",
                prefTitle: "GSPI - QLT",
                dnsServer: "https://wardev.icms.it",
                serverUrl: "/GSPI/odata/odata.xsodata/",
                serverUrlServices: "/GSPI/services/",
                serverUrlServicesNew: "/GSPI/servicesNew/",
                tokUsr: "SYSTEM",
                tokPsw: "Saphdb_00$"

                // ambiente PROD
                //                 client: "sap-client=100",
                //                 prefTitle: "GSPI",
                //                                	dnsServer: "https://war.icms.it",
                //                 serverUrl: "/GSPI/odata/odata.xsodata/",
                //                 serverUrlServices: "/GSPI/services/",
                //                                	serverUrlServicesNew: "/GSPI/servicesNew/",
                //                 tokUsr: "SYSTEM",
                //                 tokPsw: "Saphip_00$"

            },

            //rootView: 'view.App'
        },
        includes: [ //importare css di custom e lib
            "css/custom.css",
            "libs/lodash.js",
            "libs/q.js",
            "libs/xlsx.core.min.js",
            "model/odata/chiamateOdata.js"
        ],
        dependencies: {
            libs: [
                "sap.m",
                "sap.ui.layout"
            ]
        },

        routing: {
            config: {
                viewType: "XML",
                viewPath: "view",
                clearTarget: false,
                transition: "slide",
            },

            routes: [{
                    name: "login",
                    pattern: "",
                    view: "Login",
                    viewId: "loginId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "pmScheduling",
                    pattern: "pmScheduling",
                    view: "PmScheduling",
                    viewId: "pmSchedulingId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "pmModifyRequest",
                    pattern: "pmModifyRequest",
                    view: "PmModifyRequest",
                    viewId: "PmModifyRequestId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "seePjmScheduling",
                    pattern: "seePjmScheduling",
                    view: "SeePjmScheduling",
                    viewId: "SeePjmSchedulingId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "pmSchedulingDraft",
                    pattern: "pmSchedulingDraft",
                    view: "PmSchedulingDraft",
                    viewId: "pmSchedulingDraftId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "holidaysScheduling",
                    pattern: "holidaysScheduling",
                    view: "HolidaysScheduling",
                    viewId: "HolidaysSchedulingId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "umScheduling",
                    pattern: "umScheduling",
                    view: "UmScheduling",
                    viewId: "UmSchedulingId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "umSchedulingWrite",
                    pattern: "umSchedulingWrite",
                    view: "UmSchedulingWrite",
                    viewId: "UmSchedulingWriteId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "analisiCapacitaRisorse",
                    pattern: "analisiCapacitaRisorse",
                    view: "AnalisiCapacitaRisorse",
                    viewId: "AnalisiCapacitaRisorseId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "analisiDisponibilitaRisorse",
                    pattern: "analisiDisponibilitaRisorse",
                    view: "AnalisiDisponibilitaRisorse",
                    viewId: "AnalisiDisponibilitaRisorseId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "administratorScheduling",
                    pattern: "administratorScheduling",
                    view: "AdministratorScheduling",
                    viewId: "AdministratorSchedulingId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "finalSchedulingAll",
                    pattern: "finalSchedulingAll",
                    view: "FinalSchedulingAll",
                    viewId: "FinalSchedulingAllId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "administratorSchedulingWrite",
                    pattern: "administratorSchedulingWrite",
                    view: "AdministratorSchedulingWrite",
                    viewId: "AdministratorSchedulingWriteId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "launchpad",
                    pattern: "launchpad",
                    view: "Launchpad",
                    viewId: "launchpadId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "changePassword",
                    pattern: "changePassword",
                    view: "ChangePassword",
                    viewId: "changePasswordId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "newUser",
                    pattern: "user/new",
                    view: "UserCreate",
                    viewId: "newUserId",
                    targetAggregation: "pages",
                    targetControl: "app"

                },
                {
                    name: "officialScheduling",
                    pattern: "officialScheduling",
                    view: "OfficialScheduling",
                    viewId: "officialSchedulingId",
                    targetAggregation: "pages",
                    targetControl: "app"

                },
                {
                    name: "administratorSchedulingReadOnly",
                    pattern: "finalReadOnly",
                    view: "AdministratorSchedulingReadOnly",
                    viewId: "AdministratorSchedulingReadOnlyId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },

                {
                    name: "vacationRequestsList",
                    pattern: "vacationRequestsList",
                    view: "VacationRequestsList",
                    viewId: "VacationRequestsListRequestId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "vacationRequestEdit",
                    pattern: "vacationRequestEdit/{state}/:detailId:",
                    view: "VacationRequestEdit",
                    viewId: "VacationRequestEditId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    name: "tableReportConsultant",
                    pattern: "tableReportConsultant/{codConsulente}",
                    view: "TableReportConsultant",
                    viewId: "TableReportConsultantId",
                    targetAggregation: "pages",
                    targetControl: "app"
                },
                {
                    pattern: "split",
                    name: "splitApp",
                    view: "SplitApp",
                    viewType: "JS",
                    targetControl: "app",
                    targetAggregation: "pages",
                    subroutes: [{
                            name: "projectList",
                            pattern: "projectList",
                            view: "ProjectList",
                            viewId: "ProjectListId",
                            targetAggregation: "masterPages",
                            targetControl: "splitApp",
                            subroutes: [{
                                    name: "projectDetail",
                                    pattern: "projectDetail/{detailId}",
                                    view: "ProjectDetail",
                                    viewId: "ProjectDetailId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "projectModify",
                                    pattern: "projectModify/{state}/:detailId:",
                                    view: "ProjectModify",
                                    viewId: "ProjectModifyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "projectList",
                                    pattern: "projectList",
                                    view: "Empty",
                                    viewId: "EmptyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                }
                            ]
                        },
                        {
                            name: "consultantList",
                            pattern: "consultantList",
                            view: "ConsultantList",
                            viewId: "ConsultantListId",
                            targetAggregation: "masterPages",
                            targetControl: "splitApp",
                            subroutes: [{
                                    name: "consultantDetail",
                                    pattern: "consultantDetail/{detailId}",
                                    view: "ConsultantDetail",
                                    viewId: "ConsultantDetailId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "consultantModify",
                                    pattern: "consultantModify/{state}/:detailId:",
                                    view: "ConsultantModify",
                                    viewId: "ConsultantModifyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "consultantList",
                                    pattern: "consultantList",
                                    view: "Empty",
                                    viewId: "EmptyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                }
                            ]
                        },
                        {
                            name: "userLoginList",
                            pattern: "userLoginList",
                            view: "UserLoginList",
                            viewId: "UserLoginListId",
                            targetAggregation: "masterPages",
                            targetControl: "splitApp",
                            subroutes: [{
                                    name: "userLoginDetail",
                                    pattern: "userLoginDetail/{alias}",
                                    view: "UserLoginDetail",
                                    viewId: "UserLoginDetailId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "userLoginModify",
                                    pattern: "userLoginModify/{state}/:alias:",
                                    view: "UserLoginModify",
                                    viewId: "UserLoginModifyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "userLoginList",
                                    pattern: "userLoginList",
                                    view: "Empty",
                                    viewId: "EmptyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                }
                            ]
                        },
                        {
                            name: "priorityList",
                            pattern: "priorityList",
                            view: "PriorityList",
                            viewId: "PriorityListId",
                            targetAggregation: "masterPages",
                            targetControl: "splitApp",
                            subroutes: [{
                                    name: "priorityDetail",
                                    pattern: "priorityDetail/{detailId}",
                                    view: "PriorityDetail",
                                    viewId: "PriorityDetailId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "priorityModify",
                                    pattern: "priorityModify/{state}/:detailId:",
                                    view: "PriorityModify",
                                    viewId: "PriorityModifyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "priorityList",
                                    pattern: "priorityList",
                                    view: "Empty",
                                    viewId: "EmptyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                }
                            ]
                        },
                        {
                            name: "requestTypeList",
                            pattern: "requestTypeList",
                            view: "RequestTypeList",
                            viewId: "RequestTypeListId",
                            targetAggregation: "masterPages",
                            targetControl: "splitApp",
                            subroutes: [{
                                    name: "requestTypeDetail",
                                    pattern: "requestTypeDetail/{detailId}",
                                    view: "RequestTypeDetail",
                                    viewId: "RequestTypeDetailId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "requestTypeModify",
                                    pattern: "requestTypeModify/{state}/:detailId:",
                                    view: "RequestTypeModify",
                                    viewId: "RequestTypeModifyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "requestTypeList",
                                    pattern: "requestTypeList",
                                    view: "Empty",
                                    viewId: "EmptyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                }
                            ]
                        },
                        {
                            name: "periodList",
                            pattern: "periodList",
                            view: "ListaPeriodi",
                            viewId: "ListaPeriodiId",
                            targetAggregation: "masterPages",
                            targetControl: "splitApp",
                            subroutes: [{
                                    name: "periodDetail",
                                    pattern: "periodDetail/{year}/{detailId}",
                                    view: "DetailPeriodi",
                                    viewId: "detailPeriodiId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "periodModify",
                                    pattern: "periodModify/edit/{state}/:year:/:detailId:",
                                    view: "PeriodModify",
                                    viewId: "periodModifyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "periodList",
                                    pattern: "periodList",
                                    view: "Empty",
                                    viewId: "EmptyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                }
                            ]
                        },
                        {

                            name: "businessUnitList",
                            pattern: "businessUnitList",
                            view: "BusinessUnitList",
                            viewId: "BusinessUnitListId",
                            targetAggregation: "masterPages",
                            targetControl: "splitApp",
                            subroutes: [{
                                    name: "businessUnitDetail",
                                    pattern: "businessUnitDetail/{detailId}",
                                    view: "BusinessUnitDetail",
                                    viewId: "BusinessUnitDetailId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "businessUnitModify",
                                    pattern: "businessUnitModify/{state}/:detailId:",
                                    view: "BusinessUnitModify",
                                    viewId: "BusinessUnitModifyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "businessUnitList",
                                    pattern: "businessUnitList",
                                    view: "Empty",
                                    viewId: "EmptyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                }
                            ]
                        },
                        {
                            name: "anagStaffList",
                            pattern: "anagStaffList",
                            view: "AnagStaffList",
                            viewId: "AnagStaffListId",
                            targetAggregation: "masterPages",
                            targetControl: "splitApp",
                            subroutes: [{
                                    name: "anagStaffDetail",
                                    pattern: "anagStaffDetail/{detailId}",
                                    view: "AnagStaffDetail",
                                    viewId: "AnagStaffDetailId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "anagStaffList",
                                    pattern: "anagStaffList",
                                    view: "Empty",
                                    viewId: "EmptyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                }
                            ]

                        },
                        {

                            name: "skillList",
                            pattern: "skillList",
                            view: "SkillList",
                            viewId: "SkillListId",
                            targetAggregation: "masterPages",
                            targetControl: "splitApp",
                            subroutes: [{
                                    name: "skillDetail",
                                    pattern: "skillDetail/{detailId}",
                                    view: "SkillDetail",
                                    viewId: "SkillDetailId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "skillModify",
                                    pattern: "skillModify/{state}/:detailId:",
                                    view: "SkillModify",
                                    viewId: "SkillModifyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "skillList",
                                    pattern: "skillList",
                                    view: "Empty",
                                    viewId: "EmptyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                }
                            ]
                        },
                        {
                            name: "modifyRequestsList",
                            pattern: "modifyRequestsList",
                            view: "SchedulingModifyRequestList",
                            viewId: "schedulingModifyRequestId",
                            targetAggregation: "masterPages",
                            targetControl: "splitApp",
                            subroutes: [{
                                    name: "modifyRequestDetail",
                                    pattern: "modifyRequestDetail/{detailId}",
                                    view: "SchedulingModifyRequestDetail",
                                    viewId: "schedulingModifyRequestDetailId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "modifyRequestEdit",
                                    pattern: "modifyRequestEdit/{state}/:detailId:",
                                    view: "SchedulingModifyRequestEdit",
                                    viewId: "schedulingModifyRequestEditId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "modifyRequestsList",
                                    pattern: "modifyRequestsList",
                                    view: "Empty",
                                    viewId: "EmptyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                }
                            ]
                        },

                        //                        {
                        //                            name: "vacationRequestsList",
                        //                            pattern: "vacationRequestsList",
                        //                            view: "VacationRequestsList",
                        //                            viewId: "VacationRequestsListRequestId",
                        //                            targetAggregation: "masterPages",
                        //                            targetControl: "splitApp",
                        //                            subroutes: [
                        //                                {
                        //                                    name: "vacationRequestDetail",
                        //                                    pattern: "vacationRequestDetail/{detailId}",
                        //                                    view: "VacationRequestDetail",
                        //                                    viewId: "VacationRequestDetailId",
                        //                                    targetAggregation: "detailPages",
                        //                                    targetControl: "splitApp"
                        //                                },
                        //                                {
                        //                                    name: "vacationRequestEdit",
                        //                                    pattern: "vacationRequestEdit/{state}/:detailId:",
                        //                                    view: "VacationRequestEdit",
                        //                                    viewId: "VacationRequestEditId",
                        //                                    targetAggregation: "detailPages",
                        //                                    targetControl: "splitApp"
                        //                                },
                        //                                {
                        //                                    name: "vacationRequestsList",
                        //                                    pattern: "vacationRequestsList",
                        //                                    view: "Empty",
                        //                                    viewId: "EmptyId",
                        //                                    targetAggregation: "detailPages",
                        //                                    targetControl: "splitApp"
                        //								}
                        //                            ]
                        //                        },
                        {
                            name: "ricModAdminList",
                            pattern: "ricModAdminList",
                            view: "RicModAdminList",
                            viewId: "RicModAdminListId",
                            targetAggregation: "masterPages",
                            targetControl: "splitApp",
                            subroutes: [{
                                    name: "ricModAdminDetail",
                                    pattern: "ricModAdminDetail/{detailId}",
                                    view: "RicModAdminDetail",
                                    viewId: "RicModAdminDetailId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "ricModAdminList",
                                    pattern: "ricModAdminList",
                                    view: "Empty",
                                    viewId: "EmptyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                }
                            ]
                        },
                        {
                            name: "travelRequestList",
                            pattern: "travelRequestList",
                            view: "TravelRequestList",
                            viewId: "TravelRequestListId",
                            targetAggregation: "masterPages",
                            targetControl: "splitApp",
                            subroutes: [{
                                    name: "travelRequestDetail",
                                    pattern: "travelRequestDetail/{detailId}",
                                    view: "TravelRequestDetail",
                                    viewId: "TravelRequestDetailId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "travelRequestList",
                                    pattern: "travelRequestList",
                                    view: "Empty",
                                    viewId: "EmptyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "travelRequestModify",
                                    pattern: "travelRequestModify/{detailId}",
                                    view: "TravelRequestModify",
                                    viewId: "TravelRequestModifyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                            ]
                        },
                        {
                            name: "settingsWBSList",
                            pattern: "settingsWBSList",
                            view: "SettingsWBSList",
                            viewId: "SettingsWBSListId",
                            targetAggregation: "masterPages",
                            targetControl: "splitApp",
                            subroutes: [{
                                    name: "settingsWBSDetail",
                                    pattern: "settingsWBSDetail/{codCommessaHana}/{codCommessaSap}/{codAttivitaSap}",
                                    view: "SettingsWBSDetail",
                                    viewId: "SettingsWBSDetailId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "settingsWBSList",
                                    pattern: "settingsWBSList",
                                    view: "Empty",
                                    viewId: "EmptyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                                {
                                    name: "settingsWBSModify",
                                    pattern: "settingsWBSModify/{state}/:codCommessaHana:/:codCommessaSap:/:codAttivitaSap:",
                                    view: "SettingsWBSModify",
                                    viewId: "SettingsWBSModifyId",
                                    targetAggregation: "detailPages",
                                    targetControl: "splitApp"
                                },
                            ]
                        },

                    ]
                }
            ]
        }
    },

    /**
     * !!! The steps in here are sequence dependent !!!
     */
    init: function () {
        //il ns icms va cambiato a seconda del nome app e nome cliente
        jQuery.sap.registerModulePath("icms", "custom");
        jQuery.sap.registerModulePath("view", "custom/view");
        jQuery.sap.registerModulePath("model", "custom/model");
        jQuery.sap.registerModulePath("utils", "custom/utils");

        // require utils's file
        jQuery.sap.require("icms.utils.Formatter");
        jQuery.sap.require("icms.utils.ParseDate");
        // require model's file
        jQuery.sap.require("icms.model._ConsultantsStatus");
        jQuery.sap.require("icms.model._CompanyType");
        jQuery.sap.require("icms.model.Login");
        jQuery.sap.require("icms.model.i18n");
        jQuery.sap.require("icms.model.cidOdata");
        jQuery.sap.require("icms.model.userInfo");
        jQuery.sap.require("icms.model.User");
        jQuery.sap.require("icms.model.UserLogin");
        jQuery.sap.require("icms.model.persistence.Storage");
        jQuery.sap.require("icms.model.persistence.Serializer");
        jQuery.sap.require("icms.model.odata.chiamateOdata");
        jQuery.sap.require("icms.model.filters.Filter");
        jQuery.sap.require("icms.model.Consultants");
        jQuery.sap.require("icms.model.Periods");
        jQuery.sap.require("icms.model.Priorities");
        jQuery.sap.require("icms.model.Projects");
        jQuery.sap.require("icms.model.BuList");
        jQuery.sap.require("icms.model.Skill");
        jQuery.sap.require("icms.model.insertConsultantInStaff");
        jQuery.sap.require("icms.model.tabellaScheduling");
        jQuery.sap.require("icms.model.requestSave");
        jQuery.sap.require("icms.model.reqToDel");
        jQuery.sap.require("icms.model.ProfiloCommessaPeriodo");
        jQuery.sap.require("icms.model.StaffCommesseView");
        jQuery.sap.require("icms.model.getStatoConsPeriodoDraft");
        jQuery.sap.require("icms.model.getGenericConsSkills");
        jQuery.sap.require("icms.model.CommessePerStaffDelPjm");
        jQuery.sap.require("icms.model.Tiles");
        jQuery.sap.require("icms.model.PeriodsModifyRequest");
        jQuery.sap.require("icms.model.TravelReq");
        jQuery.sap.require("icms.model.BusinessUnit");
        jQuery.sap.require("icms.model.UpdateAnagStaffGenerico");
        jQuery.sap.require("icms.model.Profiles");
        jQuery.sap.require("icms.model.tabellaSchedulingUM");
        jQuery.sap.require("icms.model.requestSaveToDraft");
        jQuery.sap.require("icms.model.CommessePerStaffDelUm");
        jQuery.sap.require("icms.model.TabellaConsulentePeriodoDraft");
        jQuery.sap.require("icms.model.SchedulingModifyRequests");
        jQuery.sap.require("icms.model.tabellaSchedulingAdmin");
        jQuery.sap.require("icms.model.tabellaSchedulingDraft");
        jQuery.sap.require("icms.model.requestSaveToFinal");
        jQuery.sap.require("icms.model.tabellaSchedulingPjmReadFinal");
        jQuery.sap.require("icms.model.getAnalisiCapacita");
        jQuery.sap.require("icms.model.getAnalisiDisponibilita");
        jQuery.sap.require("icms.model.tabellaSchedulingModify");
        jQuery.sap.require("icms.model.PjmByConsultant");
        jQuery.sap.require("icms.model.SendMail");
        jQuery.sap.require("icms.model.tabellaSchedulingFinalIcms");

        ////////////////////////////////////

        // 2. call overridden init (calls createContent)
        sap.ui.core.UIComponent.prototype.init.apply(this, arguments);

        var oI18nModel = new sap.ui.model.resource.ResourceModel({
            bundleUrl: [this.getMetadata().getConfig().resourceBundle]
        });
        this.setModel(oI18nModel, "i18n");
        if (sessionStorage.getItem("language")) {
            sap.ui.getCore().getConfiguration().setLanguage(sessionStorage.getItem("language"));
        }
        //sap.ui.getCore().setModel(oI18nModel, "i18n");

        // 3a. monkey patch the router
        jQuery.sap.require("sap.m.routing.RouteMatchedHandler");
        var router = this.getRouter();
        router.myNavBack = icms.MyRouter.myNavBack;
        router.myNavToWithoutHash = icms.MyRouter.myNavToWithoutHash;
        icms.MyRouter.router = router;

        this.routeHandler = new sap.m.routing.RouteMatchedHandler(router);
        router.initialize();
        // NO CACHE
        $.ajaxSetup({
            cache: false
        });

        // set device model
        var oDeviceModel = new sap.ui.model.json.JSONModel({
            isTouch: sap.ui.Device.support.touch,
            isNoTouch: !sap.ui.Device.support.touch,
            isPhone: sap.ui.Device.system.phone,
            isNoPhone: !sap.ui.Device.system.phone,
            listMode: (sap.ui.Device.system.phone) ? "None" : "SingleSelectMaster",
            listItemType: (sap.ui.Device.system.phone) ? "Active" : "Inactive"
        });
        oDeviceModel.setDefaultBindingMode("OneWay");
        this.setModel(oDeviceModel, "device");

        var appStatusModel = new sap.ui.model.json.JSONModel();
        appStatusModel.setProperty("/navBackVisible", false);
        this.setModel(appStatusModel, "appStatus");
    },

    destroy: function () {
        if (this.routeHandler) {
            this.routeHandler.destroy();
        }

        // call overridden destroy
        sap.ui.core.UIComponent.prototype.destroy.apply(this, arguments);
    },

    createContent: function () {
        // create root view
        var oView = sap.ui.view({
            id: "mainApp",
            viewName: "view.App",
            type: "JS",
            viewData: {
                component: this
            }
        });

        return oView;
    },

});